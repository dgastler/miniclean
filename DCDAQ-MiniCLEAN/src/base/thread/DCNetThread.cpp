#include <DCNetThread.h>

DCNetThread::DCNetThread()
{  
  SetType("DCNetThread");
  SetName("DCNetThread");
  parent = NULL;
  SocketFD = BadPipeFD;      
  MaxNetErrorCount = MAXNETERRORS;
  BadPacketTimeout = BADPACKETTIMEOUT;
  DataIn = DataOut = TimeStepDataIn = TimeStepDataOut = 0;
  PacketsIn = PacketsOut = TimeStepPacketsIn = TimeStepPacketsOut = 0;
  In = Out = NULL;
  lastUpdate = time(NULL);
}
DCNetThread::~DCNetThread()
{
  //Add the shutdown packet if it hasn't already been sent. 
  if(IsRunning())
    {
      AddShutdownPacket();
    }
  //Set up a timed wait for this to go through. 
  //If it hasn't in N seconds, bail!
  time_t shutdownStart,shutdownEnd;
  shutdownStart = shutdownEnd = time(NULL);
  shutdownEnd += 3; //Wait for 3 seconds for this to go through
  while(IsRunning() && 
	(ShutdownManager.GetSize() != 0)  && 
	(difftime(shutdownEnd,shutdownStart) >= 0))
    {
      //Sleep for 1/10 a second
      usleep(100000);
    }
  In->Shutdown();
  Out->Shutdown();
  if(In != NULL)
    {
      delete In; In = NULL;
    }
  if(Out == NULL)
    {
      delete Out; Out = NULL;
    }
  ShutdownConnection();
  if(SocketFD != BadPipeFD)
    {
      CloseSockFD(SocketFD);
      SocketFD = BadPipeFD;
    }
}

bool DCNetThread::SetupConnection(int &_SocketFD,DCMessage::DCAddress local,DCMessage::DCAddress remote)
{
  //==============================
  //Assign network variables
  //==============================      
  SocketFD = _SocketFD;
  std::stringstream ss;
  if(local.IsBlank())
    LocalAddr = GetLocalAddress(SocketFD);  
  else
    {
      LocalAddr = local;
    }
  ss << "Configuring DCNetThread with local address: " << LocalAddr.GetAddrStr();
  printf("%s\n",ss.str().c_str()); ss.str("");
  if(remote.IsBlank())
    RemoteAddr = GetRemoteAddress(SocketFD);
  else
    {
      RemoteAddr = remote;
    }
  ss << "Configuring DCNetThread with remote address: " << RemoteAddr.GetAddrStr();
  printf("%s\n",ss.str().c_str());

  //Make sure the socket has non-blocking off
  if(!SetNonBlocking(SocketFD,false))
    {
      return false;
    }

  //Add the socket fd to our read set
  if(SocketFD != BadPipeFD)
    {
      AddReadFD(SocketFD);
      SetupSelect();
    }

  //If the In and Out managers work and the socket is setup
  //set the Ready status to be true
  if((In != NULL) && (Out != NULL) && (SocketFD != BadPipeFD))
    {
      Ready = true;
      Loop = true;
    }
  _SocketFD = BadPipeFD;
  return true;
}

bool DCNetThread::SetupPacketManager(xmlNode * _PacketManagerNode)
{
  //Find In and Out pakcet manager nodes.
  xmlNode *  inPacketManagerNode = FindSubNode(_PacketManagerNode,"IN");
  if(inPacketManagerNode == NULL){return(false);}

  xmlNode * outPacketManagerNode = FindSubNode(_PacketManagerNode,"OUT");
  if(outPacketManagerNode == NULL){return(false);}


  //==============================
  //packet manager setup.
  //==============================      
  char * packetManagerName = new char[100];


  //Create In packet manager
  if(In) //Check for an existing manager
    {
      printf("Warning:   Deleting existing inPacketManager.\n");
      delete In;
    }
  sprintf(packetManagerName,"FD%02dIN",SocketFD); //name for thread
  In = new DCNetPacketManager(packetManagerName); 
  if(In == NULL) //Check that new worked
    {
      printf("Error:   Unable to allocate inPacketManager.\n");
      return(false);
    }
  if(!In->AllocateMemory(inPacketManagerNode)) 
    //Check that memory was allocated correctly
    {
      printf("Error:   Unable to allocate inPacketManager memory.\n");
      return(false);
    }      
  



  //Create Out packet manager
  if(Out) //Check for an existing manager
    {
      printf("Warning:   Deleting existing outPacketManager.\n");
      delete Out;
    }
  sprintf(packetManagerName,"FD%02dOUT",SocketFD); //name for thread
  Out = new DCNetPacketManager(packetManagerName);
  if(Out == NULL) //Check that new worked
    {
      printf("Error:   Unable to allocate outPacketManager.\n");
      return(false);
    }
  if(!Out->AllocateMemory(outPacketManagerNode))
    //Check that memory was allocated correctly
    {
      printf("Error:   Unable to allocate outPacketManager memory.\n");
      return(false);
    }      
  //Add the outgoing packet manager's FD to our thread's readset.
  AddReadFD(Out->GetFullFDs()[0]);    

  //Clean up our char array
  delete [] packetManagerName;
  

  //Set MainLoop to listen to the ShutdownManager;
  AddReadFD(ShutdownManager.GetFDs()[0]);

  //Set select to listen to our packetManager's FDs
  SetupSelect();
  
  //If the In and Out managers work and the socket is setup
  //set the Ready status to be true
  if((In != NULL) && (Out != NULL) && (SocketFD != BadPipeFD))
    {
      Ready = true;
      Loop = true;
    }
  return(true);
}

void DCNetThread::CheckForBadConnection(bool force)
{
  //Get Current time and add it to our deque
  time_t now = time(NULL);
  NetErrors.push_back(now);
  //Set oldest time to care about.
  time_t timeCut = now - BadPacketTimeout;

  //Loop over NetErrors and remove any times
  //we dont' care about.
  while(NetErrors.size())
    {
      if(NetErrors.front() < timeCut)
	{
	  //Remove old times
	  NetErrors.pop_front();
	}
      else
	{
	  //break when we get to current times
	  break;
	}
    }
  //Return true if there are too many errors
  if((NetErrors.size() > MaxNetErrorCount)||force)
    {
      DCMessage::Message message;

      DCMessage::SingleINT64 errorData;
      sprintf(errorData.text,"Bad connection");
      errorData.i = SocketFD;

      message.SetType(DCMessage::ERROR);      
      message.SetDataStruct(errorData);
      SendMessageOut(message);

      //stop listeing to the broken connection
      ShutdownConnection();
      //RemoveReadFD(SocketFD);
      //RemoveReadFD(Out->GetFullFDs()[0]);
      //RemoveReadFD(ShutdownManager.GetFDs()[0]);
      //SetupSelect();

    }  
}

void DCNetThread::ProcessTimeout()
{
  //Get current time
  now = time(NULL);  
  //check if we should be updating our parent.
  if((now - lastUpdate) > GetUpdateTime())
    {
      //Send a message with network transfer stats
      SendMessages(now,lastUpdate);
      lastUpdate = now;
    }
}

void DCNetThread::MainLoop()
{
  //================================
  //Process a shutdown message
  //================================
  if(IsReadReady(ShutdownManager.GetFDs()[0]))
    {
      SendShutdownPacket();
    }

  //================================
  //Process Out message
  //================================
  if(IsReadReady(Out->GetFullFDs()[0]))
    {
      SendPacket();
    }
  //================================
  //Process In message
  //================================
  if(IsReadReady(SocketFD))
    {
      ListenForPacket();
    }
}

void DCNetThread::SendMessages(time_t now,time_t lastUpdate)
{
  DCMessage::Message message;
  StatMessage::StatRate data;
  DCtime_t dt = now - lastUpdate;

  char netID[] = "NET_00";
  uint8_t netIDNumber = 0;
  if(GetLocalAddr().GetAddr()->sa_family == AF_INET)
    {
      if( (netIDNumber = ntohl(((struct sockaddr_in*)GetLocalAddr().GetAddr())->sin_addr.s_addr)&0xFF) <= 6)
	sprintf(netID,"NET_%02u",netIDNumber);
    }
  SubID::Type subIDType = SubID::GetType(netID);

  //PacketsOut message
  data.sBase.type = StatMessage::PACKET_RATE;
  data.count = TimeStepPacketsOut;
  data.units = Units::NUMBER;
  data.sInfo.interval = dt;
  data.sInfo.time = now;
  data.sInfo.subID = subIDType;
  data.sInfo.level = Level::OUT;
  message.SetData(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message);

  PacketsOut += TimeStepPacketsOut;
  TimeStepPacketsOut = 0;

  //PacketsIn message
  data.sBase.type = StatMessage::PACKET_RATE;
  data.count = TimeStepPacketsIn;
  data.units = Units::NUMBER;
  data.sInfo.interval = dt;
  data.sInfo.time = now;
  data.sInfo.subID = subIDType;
  data.sInfo.level = Level::IN;
  message.SetData(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message);

  PacketsIn += TimeStepPacketsIn;
  TimeStepPacketsIn = 0;

  //DatasOut message
  data.sBase.type = StatMessage::DATA_RATE;
  data.count = TimeStepDataOut;
  data.units = Units::BYTES;
  data.sInfo.interval = dt;
  data.sInfo.time = now;
  data.sInfo.subID = subIDType;
  data.sInfo.level = Level::OUT;
  message.SetData(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message);

  DataOut += TimeStepDataOut;
  TimeStepDataOut = 0;

  //DatasIn message
  data.sBase.type = StatMessage::DATA_RATE;
  data.count = TimeStepDataIn;
  data.units = Units::BYTES;
  data.sInfo.interval = dt;
  data.sInfo.time = now;
  data.sInfo.subID = subIDType;
  data.sInfo.level = Level::IN;
  message.SetData(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message);

  DataIn += TimeStepDataIn;
  TimeStepDataIn = 0;
}

inline void DCNetThread::SendPacket()
{
  DCNetPacket * packet;
  int32_t memManagerIndex;
  
  //Get a packet
  Out->GetFull(memManagerIndex,false);
  if(memManagerIndex >= 0)
    {
      packet = Out->GetPacket(memManagerIndex);
      //send packet
      int sendReturn = packet->SendPacket(SocketFD);
      if(sendReturn > 0)
	{
	  //Everything is ok
	  Out->AddFree(memManagerIndex); //Add the freed packet to the free queue     
	  TimeStepPacketsOut++;
	  TimeStepDataOut += sendReturn;
	}
      else if(sendReturn == 0)
	{		  
	  //Data didn't send due to a timeout.
	  //Add the current packet back to the queue.
	  Out->AddFull(memManagerIndex);
	}
      else
	{
	  //A real error has occured.
	  
	  //Data didn't send due to a timeout.
	  //Add the current packet back to the queue.
	  Out->AddFull(memManagerIndex);
	  
	  if(sendReturn == -1)
	    {
	      if((errno == EPIPE)||
		 (errno == ENXIO))
		{
		  ShutdownConnection();
		}
	    }
		 

	  //Check if we should consider the connection a failure.
	  CheckForBadConnection(true);	      
	}
    }  
}

inline void DCNetThread::ListenForPacket()
{
  DCNetPacket * packet;
  int32_t memManagerIndex;
  //Get a packet
  In->GetFree(memManagerIndex ,false);
  if(memManagerIndex >= 0)
    {
      packet = In->GetPacket(memManagerIndex);
      //listen for a  packet
      int errorReturn = 0;
      int32_t listenReturn = packet->ListenForPacket(SocketFD,errorReturn);
      if(listenReturn > 0)
	{
	  if(packet->GetType() != DCNetPacket::SHUTDOWN)
	    {
	      //Everything worked.
	      In->AddFull(memManagerIndex);
	      TimeStepPacketsIn++;
	      TimeStepDataIn+=listenReturn;
	    }
	  else
	    {
	      PrintWarning("Got shutdown packet\n");
	      fprintf(stderr,"DCNetThread (%ld) got a shutdown packet from (%s)\n",
		      syscall(SYS_gettid),
		      RemoteAddr.GetStr().c_str());	      

	      DCMessage::Message message;
	      
//	      DCMessage::SingleINT64 errorData;
//	      sprintf(errorData.text,"SHUTDOWN");
//	      errorData.i = SocketFD;
//	      
//	      message.SetType(DCMessage::ERROR);      
//	      message.SetDataStruct(errorData);

	      message.SetType(DCMessage::NETWORK);
	      NetworkStatus::Type status = NetworkStatus::DISCONNECTED;
	      message.SetData(status);

	      SendMessageOut(message);
	      
	      //stop listeing to the broken connection
	      ShutdownConnection();	      
	    }
	}
      else
	{
	  //Error
	  //return the packet to the free queue
	  In->AddFree(memManagerIndex);	  
	  if(listenReturn == DCNetPacket::PACKET_EOF)
	    {
	      //This means the connection is dead
	      //stop listeing to the broken connection
	      ShutdownConnection();
	    }
	  else if(listenReturn == DCNetPacket::PACKET_TOO_LARGE)
	    {
	      //PrintError("Error ListenForPacket:   Packet too large!\n");
	      fprintf(stderr,"Error ListenForPacket:   Packet too large!\n");
	    }
	  else if(listenReturn == DCNetPacket::PACKET_DATA_READ_SIZE)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: Bad data read size. %d\n",errorReturn);
	      //	      PrintError(buffer);
	      fprintf(stderr,buffer);
	      delete [] buffer;
	    }
	  else if(listenReturn == DCNetPacket::PACKET_BAD_HEADER)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: Bad header word. 0x%X\n",errorReturn);
	      //	      PrintError(buffer);
	      fprintf(stderr,buffer);
	      delete [] buffer;
	    }
	  else if(listenReturn == DCNetPacket::PACKET_HEADER_READ_SIZE)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: Packet header read wrong size (%dbytes)!\n",errorReturn);
	      //	      PrintError(buffer);
	      fprintf(stderr,buffer);
	      delete [] buffer;
	    }
	  else if(listenReturn == DCNetPacket::PACKET_READ_ERROR)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: %s\n",strerror(errorReturn));
	      //	      PrintError(buffer);
	      fprintf(stderr,buffer);
	      delete [] buffer;
	      //We need to stop listening to this connection
	      CheckForBadConnection(true);
	    }
	  
	  if(errorReturn > 0)
	    {
	      fprintf(stderr,"read gave errno (%d) %s\n",
		      errorReturn,
		      strerror(errorReturn));
	      ShutdownConnection();
	    }

	  //Check if the connection is effectively dead.
	  CheckForBadConnection();
	}
    }	  	   
}

bool DCNetThread::ProcessMessage(DCMessage::Message &message)
{
  //We should wait for a message because select told us we have a message.
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      //Stop listening to network connection
      if(SocketFD != BadPipeFD)
	{
	  RemoveReadFD(SocketFD);
	}
      if(Out->GetFullFDs()[0] != BadPipeFD) 
	{
	  RemoveReadFD(Out->GetFullFDs()[0]);
	}
      SetupSelect();
      AddShutdownPacket();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //stop listeing to the broken connection
      RemoveReadFD(SocketFD);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      if(SocketFD != BadPipeFD)
	{
	  AddReadFD(SocketFD);
	  SetupSelect();
	}
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DCNetThread::PrintStatus()
{
  printf(" InPacketManager:");
  if(In != NULL)
    {      
      In->PrintStatus(3);
    }
  else
    {
      printf(" NULL\n");
    }
  printf(" OutPacketManager:");
  if(Out != NULL)
    {      
      Out->PrintStatus(3);
    }
  else
    {
      printf(" NULL\n");
    }
}

void DCNetThread::AddShutdownPacket()
{
  //Stop listening to network connection
  if(SocketFD != BadPipeFD)
    {
      shutdown(SocketFD,SHUT_RD);
      RemoveReadFD(SocketFD);
    }
  if(Out->GetFullFDs()[0] != BadPipeFD)
    {
      RemoveReadFD(Out->GetFullFDs()[0]);
    }
  SetupSelect();
  
  //Shutdown packet manager queues
  In->Shutdown();
  Out->Shutdown();       

  //Build a shutdown packet and pass it to the shutdown manager
  //this will cause the thread to look at it. 
  //When mainloop wakes up for this queue, it will send the packet 
  //and then shutdown this thread
  DCNetPacket packet(0); //Size does not matter as long as it is above the header size.
  packet.SetType(DCNetPacket::SHUTDOWN);
  ShutdownManager.Add(packet);
}
void DCNetThread::SendShutdownPacket()
{ 
  //Send out the packet
  //We don't error check because we are shutting down anyways.
  DCNetPacket shutdownPacket(0);
  ShutdownManager.Get(shutdownPacket,true);
  fprintf(stderr,"DCNetThread (%ld) has sent a shutdown packet (to %s)\n",
	  syscall(SYS_gettid),
	  RemoteAddr.GetStr().c_str());
  if(SocketFD != BadPipeFD)
    shutdownPacket.SendPacket(SocketFD);
  //Make the thread stop
  ShutdownConnection();
}

bool DCNetThread::IsLocalAddr(DCMessage::DCAddress const &addr)
{
  return (LocalAddr.IsEqualAddr(addr));
}
bool DCNetThread::IsRemoteAddr(DCMessage::DCAddress const & addr)
{
  return (RemoteAddr.IsEqualAddr(addr));
}

void DCNetThread::ShutdownConnection()
{
  if(SocketFD != BadPipeFD)
    {
      RemoveReadFD(SocketFD);
      if(Out != NULL)
	RemoveReadFD(Out->GetFullFDs()[0]);
      RemoveReadFD(ShutdownManager.GetFDs()[0]);
      SetupSelect();
      //      CloseSockFD(SocketFD);
      //      SocketFD = BadPipeFD;
    }
  if(Out != NULL)
    Out->Shutdown();
  if(In != NULL)
    In->Shutdown();
  Loop = false;
  Running = false;
}
