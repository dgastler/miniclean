#ifdef __RATDS__
#include <DRGetter.h>

void DRGetter::MainLoop()
{
  //Check for a new connection
  CheckForNewConnection();

  //Check for new data from the DCNetThread
  if(DRSender.IsRunning())
    //We have a connection
    {
      if(IsReadReady(DRSender.GetFullPacketFD()))
	{
	  ProcessPacket();
	}
      //Check for a new message
      if(IsReadReady(DRSender.GetMessageFD()))
	{
	  ProcessChildMessage();
	}
    }

  //Check the local DecisionQueue
  if(IsReadReady(DecisionQueue.GetFDs()[0]))
    {
      ProcessDecision();
    }

}

void DRGetter::ProcessTimeout()
{
  //Update the current time
  currentTime = time(NULL);
  //Do statistics stuff


  //==============================================================
  //Network connection stuff
  //==============================================================
  //Catch a timeout if we don't have a DCNetThread
  //  if((DRSender.thread == NULL) && (DRSender.GetSocketFD() != -1))
  if(!DRSender.IsRunning())
    {
      DRSender.Shutdown();
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(DRSender.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }  
}

bool DRGetter::ProcessMessage(DCMessage::Message & message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      DeleteConnection();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      
      //Since this thread always needs to be running to notice network
      //connections, we don't pause anything.  
      RemoveReadFD(DecisionQueue.GetFDs()[0]);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      AddReadFD(DecisionQueue.GetFDs()[0]);
      SetupSelect();
      //We are always running
    }
  else
    {
      ret = false;
    }
  return(ret); 
}
#endif
