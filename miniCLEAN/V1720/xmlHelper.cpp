#include "xmlHelper.h"

char * GetNodeText(xmlNode * baseNode)
{
  xmlNode * curNode = NULL;
  char * xmlText = NULL;
  for(curNode = baseNode->children;curNode;curNode = curNode->next)
    {
      //If this node is a text node and it contains a valid string
      //and that string isn't just an EOL. 
      if((curNode->type == XML_TEXT_NODE) &&
	 (curNode->content != NULL) &&
	 (curNode->content[0] != '\n'))
	//      if(curNode->type == XML_TEXT_NODE)
	{
	  xmlText = (char*) curNode->content;
	  break;
	}
    }
  return(xmlText);
}

xmlNode * FindIthSubNode(xmlNode * baseNode, char * Name,int i)
{
  //Takes in an xmlNode and a Node name (char array) and searches
  //for a sub-node with that node name.
  //If found returns a pointer to that node.
  //else returns NULL;
  int foundNodeNumber = 0;
  xmlNode * curNode = NULL;
  xmlNode * retNode = NULL;
  //Loop over all sub-nodes.
  for(curNode = baseNode->children; curNode; curNode = curNode->next)
    {
      //If this is an element node then check if it has the correct name.
      if((curNode->type == XML_ELEMENT_NODE) && (xmlStrcmp(curNode->name,(xmlChar *) Name) == 0))
	{
	  //If this is the ith found Node with name Name stop
	  if(foundNodeNumber == i)
	    {
	      //set return node ot current node and break from loop.
	      retNode = curNode;
	      break;
	    }
	  else
	    {
	      //Look for the next Node with name Name
	      foundNodeNumber++;
	    }
	}
    }
  return(retNode);
}
xmlNode * FindSubNode(xmlNode * baseNode, char * Name)
{
  //Call FindIthSubNode with index 0
  return(FindIthSubNode(baseNode,Name,0));
  
}

int       NumberOfSubNodes(xmlNode * baseNode)
{
  int numberOfSubNodes = 0;
  xmlNode * curNode = NULL;
  //loop over all sub-nodes
  for(curNode = baseNode->children; curNode; curNode = curNode->next)
    {
      //If this is an element sub-node add one to numberOfSubNodes.
      if(curNode->type == XML_ELEMENT_NODE)
	{
	  numberOfSubNodes++;
	}
    }
  return(numberOfSubNodes);
}
int       NumberOfNamedSubNodes(xmlNode * baseNode,char * Name)
{
  int numberOfSubNodes = 0;
  xmlNode * curNode = NULL;
  //loop over all sub-nodes
  for(curNode = baseNode->children; curNode; curNode = curNode->next)
    {
      //If this is an element sub-node add one to numberOfSubNodes.
      if((curNode->type == XML_ELEMENT_NODE) && !xmlStrcmp(curNode->name,(xmlChar * )Name))
	{
	  numberOfSubNodes++;
	}
    }
  return(numberOfSubNodes);
}

xmlNode * AddNewSubNode(xmlNode * baseNode,char * Name)
{
  xmlNode * newNode = NULL;
  newNode = xmlNewChild(baseNode,NULL,(xmlChar*)Name,NULL);
  return(newNode);
}
