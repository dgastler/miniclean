#include <DCThread.h>

static size_t MessagePriority(DCMessage::Message & message)
{
  size_t ret = 0;
  DCMessage::DCMessageType type = message.GetType();
  switch (type)
    {
    case DCMessage::STOP:
    case DCMessage::END_OF_THREAD:
    case DCMessage::GO:
    case DCMessage::PAUSE:
      ret = 4;
      break;
    case DCMessage::LAUNCH:
    case DCMessage::LAUNCH_RESPONSE:
    case DCMessage::COMMAND:
    case DCMessage::COMMAND_RESPONSE:
    case DCMessage::NETWORK:
      ret = 3;
      break;
    case DCMessage::CAN_NOT_ROUTE:
    case DCMessage::REGISTER_TYPE:
    case DCMessage::UNREGISTER_TYPE:
    case DCMessage::REGISTER_ADDR:
    case DCMessage::UNREGISTER_ADDR:
    case DCMessage::RUN_NUMBER:
    case DCMessage::RUN_NUMBER_RESPONSE:
      ret = 2;
      break;
    case DCMessage::ERROR:
      ret = 1;
      break;
    case DCMessage::BLANK:
    case DCMessage::WARNING:
    case DCMessage::STATISTIC:
    case DCMessage::HISTOGRAM:
    case DCMessage::TEXT:
    case DCMessage::RUN_INFO:
    case DCMessage::GET_STATS:
    default:
      ret = 0;
      break;
    }
  return ret;
}


DCThread::DCThread():MessagesIn(MessagePriority),MessagesOut(MessagePriority)
{
  //Thread/run loop control
  Ready = false;
  Running = false;
  Loop = false;
  
  //Book keeping values
  RunNumber = -1;
  ThreadID = 0;      
  ThreadResults = NULL;      
  ThreadName = "unassigned";
  ThreadType = "unassigned";
  UpdateTime = 60;              //Seconds
    
  //Select parameters
  FD_ZERO(&ReadSet);            //Empty
  FD_ZERO(&WriteSet);           //Empty
  FD_ZERO(&ExceptionSet);       //Empty
  DefaultSelectTimeout.tv_sec = UpdateTime;   //seconds
  DefaultSelectTimeout.tv_usec = 0;
  SelectTimeout = DefaultSelectTimeout;

  //Set the bad message type in the IN/OUT queues
  badDCDAQMessage.SetType(DCMessage::BLANK);
  MessagesIn.Setup(badDCDAQMessage);
  MessagesOut.Setup(badDCDAQMessage);

  //Setup select
  SetupSelect();

}

/*
  This is the function that is the primary loop of the DCThread thread.
  It can run in two different ways.   
    1. SelectRunningMode(true)
       This is how almost all threads should run, so if you don't know what
       this means, ONLY USE THIS MODE!
       In this mode you write MainLoop() that is called when a non-DCDAQ message queue
       file descriptor has been opened for reading or writing. 
       After MainLoop() finishes, this code goes back to blocking on select.

    2. SelectRunningMode(false)
       You better know what you are doing and why you are using this mode. 
       This is for when you want total control of all thread flow in MainLoop.
       YOU have to manage the incomming DCDAQ message queue yourself when
       Loop is true.   
       This is needed when you are effectively polling some resouce. 
       (ie reading out the WFDs over the VME bus.)
       Again, YOU must manage the incomming DCDAQ message queue and respond
       to it's messages appropriately. 

  Running loop handles basic DCDAQ messaging for start/pause/stop and calls the
  MainLoop() function to perform the inherited class's job.
 */
void *DCThread::Run(void * /*Arg*/)
{
  struct sigaction sa;
  sa.sa_handler = thread_signal_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGSEGV,&sa, NULL);
  sigaction(SIGPIPE,&sa, NULL);
  sigaction(SIGUSR2,&sa, NULL);

  //Add a pthread cleanup function to properly stop this thread
  //in case we have to force cancel it (pthread_cancel)
  pthread_cleanup_push(end_thread,this);
  threadTID=syscall(SYS_gettid);
  //Running loop... I wrote this so long ago.
  while(Running)
    {
      //===================================================================
      //===================DCThread running loop===========================
      //===================Just listening for messages from DCDAQ==========
      //===================No thread specific code may be running!=========
      //===================================================================

      //If this is a thread that isn't running by default (Loop = true), then we should
      //clear any select FDs it may want to listen to. 
      if(!Loop)
	{
	  ClearSelect();   
	}
      
      //Block on select for something to happen.
      SelectTimeout = DefaultSelectTimeout;  //select will modify SelectTimeout.
      //reset read/write/exception sets
      retReadSet = ReadSet;                  
      retWriteSet = WriteSet;                
      retExceptionSet = ExceptionSet;        
      //block in select
      int selectReturn = select(MaxFDPlusOne,
				&retReadSet,
				&retWriteSet,
				&retExceptionSet,
				&SelectTimeout);            
      if(selectReturn > 0) //We can use one of our FDs
	{
	  //Check for an incomming message from DCDAQ
	  if(IsReadReady(GetMessageInFDs()[0]))	    
	    {
	      DCMessage::Message message =  GetMessageIn();
	      //Pass the message to the derived class's ProcessMessage
	      //if it returns false, then it didn't understand the message
	      //and we should call the DCThread version of ProcessMessage
	      if(!ProcessMessage(message))
		{
		  DCThread::ProcessMessage(message);
		}
	    }
	}
      else if(selectReturn == 0)
	{
	  ProcessTimeout();
	}
      else
	{
	  if(!selectErrorIgnore->IgnoreSelectError(errno))
	    {
	      std::string warningString("received select error(");
	      warningString += strerror(errno);
	      warningString += ") in running loop.\n";
	      PrintWarning(warningString.c_str());
	      //printf("Warning(Running):  %s:%s received select errror(%s).\n",
	      //	     ThreadType.c_str(),
	      //	     ThreadName.c_str(),
	      //	     strerror(errno));	  
	    }
	}

      while(Loop) 
	{
	  //===================================================================
	  //===================Specific thread main loop=======================
	  //===================================================================
	  //reset read/write/exception sets and reset select's timeout
	  SelectTimeout = DefaultSelectTimeout;  
	  retReadSet = ReadSet;                  
	  retWriteSet = WriteSet;                
	  retExceptionSet = ExceptionSet;        
	  //Block on select for something to happen.
	  int selectReturn = select(MaxFDPlusOne,
				    &retReadSet,
				    &retWriteSet,
				    &retExceptionSet,
				    &SelectTimeout);            
	  if(selectReturn > 0) //We can use one of our FDs
	    {
	      //Check for an incomming message from DCDAQ
	      if(IsReadReady(GetMessageInFDs()[0]))
		{
		  DCMessage::Message message =  GetMessageIn();
		  //Pass the message to the derived class's ProcessMessage
		  //if it returns false, then it didn't understand the message
		  //and we should call the DCThread version of ProcessMessage
		  if(!ProcessMessage(message))
		    {
		      DCThread::ProcessMessage(message);
		    }
		  
		  //Check if we should be updating DCDAQ
		  ProcessTimeout();
		}
	      //For anything else, call MainLoop.
	      else
		{
		  //Run the thread's main loop
		  MainLoop();
		  //Check if we should be updating DCDAQ
		  ProcessTimeout();
		}
	    }
	  else if(selectReturn == 0)
	    {


	      //Check if we should be updating DCDAQ
	      ProcessTimeout();
	    }	  
	  else
	    {	      
	      if(!selectErrorIgnore->IgnoreSelectError(errno))
		{
		  std::string warningString("received select error(");
		  warningString += strerror(errno);
		  warningString += ") in main loop.\n";
		  PrintWarning(warningString.c_str());
//		  printf("Warning(MainLoop):  %s:%s received select errror(%s).\n",
//			 ThreadType.c_str(),
//			 ThreadName.c_str(),
//			 strerror(errno));
		}	      
	      if(errno == EBADF)
		{
		  for(int i = 0;i < MaxFDPlusOne;i++)
		    {		      
		      if(FD_ISSET(i,&ReadSet))
			{
			  printf("   %s Read Selecting on FD: %u:%u\n",
				 GetName().c_str(),i,MaxFDPlusOne);
			  if(fcntl(i,F_GETFD) == -1)
			    {
			      printf("   %s Removing Bad Read FD %u from select\n",
				     GetName().c_str(),i);			 
			      RemoveReadFD(i);
			      SetupSelect();
			    }
			}
		      if(FD_ISSET(i,&WriteSet))
			{
			  printf("   %s Write Selecting on FD: %u:%u\n",
				 GetName().c_str(),i,MaxFDPlusOne);
			  if(fcntl(i,F_GETFD) == -1)
			    {
			      printf("   %s Removing Bad Write FD %u from select\n",
				     GetName().c_str(),i);			 
			      RemoveWriteFD(i);
			      SetupSelect();
			    }
			}
		      if(FD_ISSET(i,&ExceptionSet))
			{
			  printf("   %s Exception Selecting on FD: %u:%u\n",
				 GetName().c_str(),i,MaxFDPlusOne);
			  if(fcntl(i,F_GETFD) == -1)
			    {
			      printf("   %s Removing Bad Exception FD %u from select\n",
				     GetName().c_str(),i);			 
			      RemoveExceptionFD(i);
			      SetupSelect();
			    }
			}
		    }
		  //Don't do this!
		  //		  Loop = false;
		  //		  Running = false;
		}
	    }
	  //===================================================================
	  //===================Specific thread main loop end===================
	  //===================================================================	  
	} //MainLoop      
      //===================================================================
      //===================DCThread running loop end=======================
      //===================================================================
    }
 
  
  //Send out END_OF_THREAD message via
  //clearing the pthread cleanup function 
  //and calling the EndOfThread function
  pthread_cleanup_pop(true); 
  return NULL;
}
bool DCThread::Start(void * /*Arg*/)
{
  //If things aren't setup don't make the thread.
  if(!Ready)
    {
      printf("Warning: Can not create thread %s(%s) because thread wasn't setup yet.\n",GetName().c_str(),GetType().c_str());
      return(false);
    }
  if(Running)
    {
      printf("Warning: Can not creat thread because thread is already running.\n");
      return(false);
    }
  //Need Running to be true before pthread_create
  Running = true;
  if(pthread_create(&ThreadID,NULL,start_thread,this))
    {
      fprintf(stderr,"Error creating consumer thread!\n");
      Running = false;
    }

  return(true);
}

void * DCThread::End(bool Wait)
{
  ThreadResults = NULL;
  MessagesOut.ShutdownWait();
  MessagesIn.ShutdownWait();
  //Request that the thread end
  if(IsRunning())
    {
      //Shutdown thread
      //      pthread_cancel(ThreadID);
      if(Wait)
	{
	  pthread_join(ThreadID,&ThreadResults);
	}
    }
  return(ThreadResults);
}

DCMessage::Message DCThread::GetMessageOut()
{
  DCMessage::Message ret;
  ret.SetType(DCMessage::BLANK);
  MessagesOut.Get(ret,true);
  return ret;
}
DCMessage::Message DCThread::GetMessageIn()
{
  DCMessage::Message ret; 
  MessagesIn.Get(ret,true);
  return(ret);
}
bool DCThread::SendMessageIn(DCMessage::Message &message) 
{
  if(Running)
    return(MessagesIn.Add(message,true));
  return false;
}

bool DCThread::SendMessageOut(DCMessage::Message &message)
{
  //Adding source information (default remote address is set by DCMessage)
  message.SetSource(GetName());
  //Add a message to the queue.
  return(MessagesOut.Add(message,true));
}

bool DCThread::ForwardMessageOut(DCMessage::Message &message)
{
  //Add a message to the queue. (do not molest the message)
  return(MessagesOut.Add(message,true));
}

/*
  Function used to add a file descriptor to the fd vector.
  This has the added benefit that it won't duplicate an existing entry.  
 */
void DCThread::AddToVectorWODup(int value,std::vector<int> &vec)
{
  if(value >= 0)
    {
      bool found = false;
      //Search vec for value
      for(unsigned int i = 0;((!found)&&(i < vec.size()));i++)
	{if(vec[i] == value) {found = true;}}
      if(!found) //if value was not found add it to vec
	{vec.push_back(value);}
    }
}
/*
  Function used to remove an FD from our fd vectors.
 */
void DCThread::RemoveFromVector(int value,std::vector<int> &vec)
{
  std::vector<int>::iterator it = vec.begin();
  //iterate through the vector until we reach the end or find "value"
  for(;it != vec.end();it++)
    {if((*it) == value){break;}}
  //If "it" isn't vec.end(), then we found val and we should remove it. 
  if(it != vec.end())
    {vec.erase(it);}
}

/*
  ClearSelect(): This functions removes all fds from the list select 
  listen's to.  It then calls SetupSelect to make these changes take
  effect.   SetupSelect will add back in the InMessageQueue's fd.
 */
void DCThread::ClearSelect()
{
  //Clear all fds from our select fd lists
  ReadFDs.clear();       
  WriteFDs.clear();
  ExceptionFDs.clear();
  //Call setup select to have these changes take effect.
  //InMessageQueue's fd will be added in SetupSelect()
  SetupSelect();
}

/*
  SetupSelect(): This function sets up all of the values needed for select 
  to properly work. It also makes sure that the read message queue's 
  fd is ALWAYS listened to. 
 */
void DCThread::SetupSelect()
{
  //Clear all FDs from Read/Write/Exception sets.
  FD_ZERO(&ReadSet);
  FD_ZERO(&WriteSet);
  FD_ZERO(&ExceptionSet);

  //  Add MessageInPipeFD (we ALWAYS listen to this!)
  AddReadFD(GetMessageInFDs()[0]);

  //Zero out so that we can find the new maximum FD
  MaxFDPlusOne = 0;

  //Add all FDs in ReadFDs to ReadSet
  for(unsigned int i = 0; i < ReadFDs.size();i++)
    {
      FD_SET(ReadFDs[i],&ReadSet);
      if(ReadFDs[i] > MaxFDPlusOne)
	{MaxFDPlusOne = ReadFDs[i];}
    }
  //Add all FDs in WriteFDs to WriteSet
  for(unsigned int i = 0; i < WriteFDs.size();i++)
    {
      FD_SET(WriteFDs[i],&WriteSet);
      if(WriteFDs[i] > MaxFDPlusOne)
	{MaxFDPlusOne = WriteFDs[i];}
    }
  //Add all FDs in ExceptionFDs to ExceptionSet
  for(unsigned int i = 0; i < ExceptionFDs.size();i++)
    {
      FD_SET(ExceptionFDs[i],&ExceptionSet);
      if(ExceptionFDs[i] > MaxFDPlusOne)
	{MaxFDPlusOne = ExceptionFDs[i];}
    }
  
  //Add one to the max file descriptor
  MaxFDPlusOne++;
 
}


void DCThread::SetSelectTimeout(long seconds,long useconds)
{
  DefaultSelectTimeout.tv_sec = seconds;
  DefaultSelectTimeout.tv_usec = useconds;
  SelectTimeout = DefaultSelectTimeout;
}

void DCThread::PrintError(const std::string & text)
{
  PrintError(text.c_str());
}
void DCThread::PrintError(const char * text)
{  
  if(IsRunning())
    {
      DCMessage::Message errorMessage;
      errorMessage.SetType(DCMessage::ERROR);
      errorMessage.SetDestination();
      
      DCMessage::SingleUINT64 errorData;
      sprintf(errorData.text,"%s\n",
	      text);
      errorMessage.SetDataStruct(errorData);
      SendMessageOut(errorMessage);
    }
  else
    {
      time_t timeTemp = time(NULL);
      fprintf(stderr,"Error(%s):%s\n",GetTime(&timeTemp).c_str(),text);
    }
}
void DCThread::PrintWarning(const char * text)
{
  PrintWarning(std::string(text));
}
void DCThread::PrintWarning(const std::string &text)
{
  if(IsRunning())
    {
      DCMessage::Message message;
      message.SetDestination();
      message.SetType(DCMessage::WARNING);
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(text);
      message.SetData(&(data[0]),data.size());
      SendMessageOut(message);
    }
  else
    {
      time_t timeTemp = time(NULL);
      fprintf(stdout,"Warning(%s):%s\n",GetTime(&timeTemp).c_str(),text.c_str());
    }
}
void DCThread::Print(const char * text)
{
  Print(std::string(text));
}
void DCThread::Print(const std::string &text)
{
  if(IsRunning())
    {
      DCMessage::Message message;
      message.SetDestination();
      message.SetType(DCMessage::TEXT);
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(text);
      message.SetData(&(data[0]),data.size());
      SendMessageOut(message);
    }
  else
    {
      time_t timeTemp = time(NULL);
      fprintf(stdout,"Print(%s):%s\n",GetTime(&timeTemp).c_str(),text.c_str());
    }
}
void DCThread::SendStop(bool PrintStackTrace)
{
  if(PrintStackTrace == true)
    {
      //Print the current stack trace
      print_trace();
    }
  //Shutdown DCDAQ
  DCMessage::Message message;
  message.SetType(DCMessage::STOP);
  SendMessageOut(message);

  //Pause this thread.
  message.SetType(DCMessage::PAUSE);
  SendMessageIn(message);
  //Clear the select set
  //  ClearSelect();
  //Resetup select
  //  SetupSelect();
  //Loop = false;
  //Running = false;
}

//Basic message processor
bool DCThread::ProcessMessage(DCMessage::Message &message)
{
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      if(Ready)
	Loop = true;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      Loop = false;
    }
  else if(message.GetType() == DCMessage::RUN_NUMBER)
    {
      uint32_t dataTemp;      
      if(message.GetData(dataTemp))
	{
	  SetRunNumber(dataTemp);
	
	  DCMessage::Message responseMesg;
	  responseMesg.SetType(DCMessage::RUN_NUMBER_RESPONSE);
	  responseMesg.SetDestination(message.GetSource());
	  SendMessageOut(responseMesg);
	}
    }
  else if(message.GetType() == DCMessage::RUN_NUMBER_RESPONSE)
    {
      //just let it drop
    }
  else
    {
      printf("Warning:    %s unknown DCDAQ message type %s from %s.\n",
	     GetName().c_str(),
	     GetTypeName(message.GetType()),
	     message.GetSource().GetStr().c_str());
      return false;
    }
  return true;
}

void DCThread::PrintStatus()
{
  std::stringstream status;//("is ");
  status << "(tid: " << threadTID <<")" 
	 << " is ";
  if(Ready)
  status << "";
  else
    status << "not ";
  status << "ready, ";
  if(Running)
  status << "";
  else
    status << "not ";
  status << "running and ";
  if(Loop)
  status << "";
  else
    status << "not ";
  status << "looping.";
  status << " Mesg(In/Out) <";
  status << MessagesIn.GetSize();
  status << "/";
  status << MessagesOut.GetSize();
  status << ">";

//  sprintf("Thread: %s:%s is %sready, %srunning and %slooping\n",
//	  GetType().c_str(),
//	  GetName().c_str(),
//	  (Ready)?"":"not ",
//	  (Running)?"":"not ",
//	  (Loop)?"":"not ");
  Print(status.str().c_str());
}

void DCThread::SetDefaultSelectTimeout(long int tv_sec, long int tv_usec)
{
  DefaultSelectTimeout.tv_sec  = tv_sec;
  DefaultSelectTimeout.tv_usec = tv_usec;
}

void DCThread::ClearIncomingMessages(DCMessage::DCMessageType type)
{
  if(type == DCMessage::COUNT)
    MessagesIn.Clear();
  else
    {
      size_t count = MessagesIn.GetSize();
      for(size_t i = 0; i < count;i++)
	{
	  DCMessage::Message message;
	  MessagesIn.Get(message,true);
	  if(message.GetType() != type)
	    MessagesIn.Add(message,true);
	}
    }
}

void DCThread::SendStartMessage()
{
  DCMessage::Message message;
  message.SetType(DCMessage::GO);
  MessagesIn.Add(message,true);
}
