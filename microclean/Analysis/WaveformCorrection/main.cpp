#include <iostream>
#include <math.h>
#include <iomanip>

#include "TFile.h"
#include "TBranch.h"
#include "TTree.h"
#include "TH1F.h"
#include "TMinuit.h"
#include "TF1.h"

#include "Event.h"
#include "TextArray.h"
#include "xmlParser.h"

#include <fstream.h>

using namespace std;

volatile bool stoprun = false;


Float_t * RawWave = 0;
Float_t * SummedWave = 0;
Float_t * CorrectedWave = 0;
Int_t     Imax;
Float_t   Sigma;

void signal_stoprun(int sig_num) //ctrl-c and alarm handler
{
  stoprun = true;
}

void swap(Float_t *, Float_t *);
void quicksort(Float_t *, int, int);

/* function used for swapping values of two variables */
void swap(Float_t *a, Float_t *b)
{
  Float_t c = *a;

  *a = *b;
  *b = c;
}

/* nums[] - the array whose elements are to be sorted
 * first - index of the first element to be sorted
 * last - index of the last element to be sorted */
void quicksort(Float_t * nums, int first, int last)
{
  int i;
  Float_t  pivot;
  int left;
  int right;
  int passes = last - first;
  Float_t mid;

  /* Do not run if there is only one element */
  if (passes > 0)
  {
    /* The following if check and the next loop
     * set the lowest element at the beginning
     * and the greatest at the end of the array. */
    if (nums[first] > nums[last])
      swap(&nums[first], &nums[last]);

    if (passes > 1)
    {
      for (i = first + 1; i < last; i++)
      {
        if (nums[i] < nums[first])
          swap(&nums[i], &nums[first]);
        else if (nums[i] > nums[last])
          swap(&nums[i], &nums[last]);
      }

      if (passes > 2)
      {
        mid = (nums[first] + nums[last]) / 2;
        pivot = nums[first + 1];

        /* The following loop sets the value of pivot element. */
        for (i = first + 2; i < last; i++)
        {
          if (fabs(nums[i] - mid) < fabs(pivot - mid))
            pivot = nums[i];
        }

        left = first + 1;
        right = last - 1;

        /* After following loop (one quicksort pass),
         * "right" shows the location of pivot element... */
        while (left < right)
        {
          while (nums[left] < pivot)
            left++;

          while (nums[right] > pivot)
            right--;

          if (left < right)
            swap(&nums[left], &nums[right]);

          if (nums[left] == pivot && nums[right] == pivot)
            left++;
        }

        /* ...and that's why the quicksort is used recursively
         * on array nums from point "first + 1" to "right - 1" and
         * from point "right + 1" to "last - 1". */
        quicksort(nums, first + 1, right - 1);
        quicksort(nums, right + 1, last - 1);
      }
    }
  }
}

Float_t median(Float_t* A, Int_t size)
{
  quicksort(A,0,size - 1);
  Float_t value = -1;
  if(0x1&size == 0x1)
    {
      value = A[(size - 1)/2];
      
    }
  else
    {
      value = (A[(size/2)] + A[(size/2)-1])/2.0;
    }
  return (value);
}


void fcn(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  Double_t parameter = 1.0 + par[0];
  CorrectedWave[0] = RawWave[0];
  for(int i = 0; i < Imax - 1;i++)
    {
      CorrectedWave[i+1] = CorrectedWave[i] - RawWave[i] + Double_t(RawWave[i+1])*parameter;
    }
  f = median(CorrectedWave + (Imax - 1000),1000);
  f *= f;
  //  cerr << "@ " << par[0] << " "  << f << endl;
}







Float_t MergeWaveforms(Int_t ChannelSize, Float_t * Ya,Float_t *Yb,Float_t Y_Offset)
{
  Float_t start = 0;
  Float_t end = 0;
  Float_t deltaT = 0;
  Float_t trip = 200-Y_Offset;
  //  for(int i = 0; i < ChannelSize;i++)
  //Find position of channel a hitting 200-Y_Offset on the way up and down.
  for(int i = 1; i < ChannelSize;i++)
    {
      if((Ya[i] > trip)&&(start == 0))
	{
	  start = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) + i - 1;
	}
      if((Ya[i] < trip)&&(start !=0))
	{
	  end = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) +i - 1;
	  i = ChannelSize;
	}
      
    }
  
  deltaT = end - start;
  
  //5;
  //Find B channel MAXes
  Int_t ChannelB_MAX = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Yb[i] >= Yb[ChannelB_MAX])
	{
	  ChannelB_MAX = i;
	}
      if((i == 2*ChannelB_MAX)&&(Yb[ChannelB_MAX] > 5))
	break;
    }
  
  Int_t Tf = ChannelB_MAX;
  Int_t Ti = Tf-1;
  
  //6;
  while( Tf - Ti < deltaT )
    {
      if(((Tf >= 0) && (Tf < ChannelSize)) && ((Ti >= 0) && (Ti < ChannelSize)))
      //      if ( Yb[Tf] > Yb[Ti] )
	{
	  if(Yb[Tf] > Yb[Ti])
	    {
	      Tf++;
	    }
	  else
	    {
	      Ti--;
	    }
	}
      else
	{
	  break;
	  Ti = 0xefffffff;
	  Tf = 0xefffffff;
	}
    }

	
  Float_t shift;
  shift = (start - Ti + end - Tf)/2.0;
  
  Float_t Gain = 10;
  //7;
  for(int i = 0;i < ChannelSize;i++)
    {
      //      if((i + shift) > 0 && (i + shift) < ChannelSize)
      if((i - (int)shift) > 0 && (i - (int)shift) < ChannelSize)
	{
	  if(Ya[i] >= 255 - ceil(Y_Offset) && Gain*Yb[i-(int)shift] > Ya[i])
	    {
	      Ya[i] = Gain * Yb[i-(int)shift];
	    }
	}
      
    }
  
  return(shift);
}

int main(int argc, char **argv)
{
  struct sigaction userStop;
  sigset_t      mask_set; // Mask out *all signals in the signal handlers
  sigfillset(&mask_set);
  sigdelset(&mask_set,SIGINT);
  memset(&userStop,0,sizeof(userStop));
  userStop.sa_handler = signal_stoprun;
  userStop.sa_mask = mask_set;
  sigaction(SIGINT, &userStop,NULL);
  if(argc < 5)
    {
      cout << argv[0] << " InFile_base Ni Nf Outfile" << endl;
      return(0);
    }

  Double_t PMT0_V = 0;
  Double_t PMT1_V = 0;

  TFile   *    InFile = 0;
  TTree   *    tree = 0;
  TTree   *    TextTree = 0;
  Event   *    event = 0;
  TextArray *  RootText = 0;
  Char_t  *    filename = new Char_t[200];
  Char_t  *    filebase = argv[1];
  Int_t        ni = atoi(argv[2]);
  Int_t        nf = atoi(argv[3]);


  TH1F *PMT0 = new TH1F("PMT0","PMT0 Corr. Parameter",800,-0.00002,0.00006);
  TH1F *PMT1 = new TH1F("PMT1","PMT1 Corr. Parameter",800,-0.00002,0.00006);

  

  for(int filenumber = ni;filenumber <= nf; filenumber++)
    {
      sprintf(filename,"%s%i.root",filebase,filenumber);
      cout << "Opening file: " << filename << endl;
      InFile                 = new TFile(filename,"READ","",9);
      tree                   = (TTree*) InFile->Get("tree");
      event                  = 0;
      TextTree               = (TTree*) InFile->Get("TextTree");
      RootText               = 0;
      Int_t NEvents          = tree->GetEntries();
      tree->SetBranchAddress("Event branch",&event);
      tree->GetEntry(0);

      TextTree->SetBranchAddress("Run Notes",&RootText);
      TextTree->GetEntry(1);
      Char_t * text = RootText->GetArray();
      XMLNode xMainNode = XMLNode::parseString(text,"DAQ");
      
      PMT0_V = atof(xMainNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",0).getAttribute("PMTVoltage"));
      PMT1_V = atof(xMainNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",2).getAttribute("PMTVoltage"));
      free(text);

      Int_t ChannelSize                    = event->GetSize(0);
      Float_t * Channel1                   = new Float_t[ChannelSize];
      Float_t * Channel1b                  = new Float_t[ChannelSize];
      Float_t * Channel2                   = new Float_t[ChannelSize];
      Float_t * Channel2b                  = new Float_t[ChannelSize];
      CorrectedWave                        = new Float_t[ChannelSize];

      for(int EventN = 0; EventN < NEvents; EventN++)
	{
	  if(stoprun)
	    {
	      break;
	    }
	  tree->GetEntry(EventN);
	  
	  bool FixGain[2]                      = {false,false};
	  
	  Float_t Channel1_Offset              = 0;
	  Float_t Channel1b_Offset             = 0;
	  Float_t Channel2_Offset              = 0;
	  Float_t Channel2b_Offset             = 0;
	  
	  Float_t Channel1_OffsetSquared       = 0;
	  Float_t Channel1b_OffsetSquared      = 0;
	  Float_t Channel2_OffsetSquared       = 0;
	  Float_t Channel2b_OffsetSquared      = 0;
		  
	  Float_t Sigma_Channel1_Offset        = 0;
	  Float_t Sigma_Channel1b_Offset       = 0;
	  Float_t Sigma_Channel2_Offset        = 0;
	  Float_t Sigma_Channel2b_Offset       = 0;

	  //Read in Waveform and check if it needs to be merged with the second
	  // gain scale.
	  
	  for(int i = 0; i < 1000;i++)
	    {
	      Channel1_Offset  += event->GetPoint(0,i);
	      Channel1b_Offset += event->GetPoint(1,i);
	      Channel2_Offset  += event->GetPoint(2,i);
	      Channel2b_Offset += event->GetPoint(3,i);
	   
	      Channel1_OffsetSquared  += event->GetPoint(0,i)*event->GetPoint(0,i);
	      Channel1b_OffsetSquared  += event->GetPoint(1,i)*event->GetPoint(1,i);
	      Channel2_OffsetSquared  += event->GetPoint(2,i)*event->GetPoint(2,i);
	      Channel2b_OffsetSquared  += event->GetPoint(3,i)*event->GetPoint(3,i);
	    }

	  Sigma_Channel1_Offset    = sqrt((Channel1_OffsetSquared - Channel1_Offset*Channel1_Offset/1000)/(1000-1));
	  Sigma_Channel1b_Offset   = sqrt((Channel1b_OffsetSquared - Channel1b_Offset*Channel1b_Offset/1000)/(1000-1));
	  Sigma_Channel2_Offset    = sqrt((Channel2_OffsetSquared - Channel2_Offset*Channel2_Offset/1000)/(1000-1));
	  Sigma_Channel2b_Offset   = sqrt((Channel2b_OffsetSquared - Channel2b_Offset*Channel2b_Offset/1000)/(1000-1));
	  
	  Channel1_Offset  /= 1000.0;
	  Channel1b_Offset /= 1000.0;
	  Channel2_Offset  /= 1000.0;
	  Channel2b_Offset /= 1000.0;

	  for(int i = 0; i < ChannelSize;i++)
	    {
	      Channel1[i] = event->GetPoint(0,i);
	      Channel1b[i] = event->GetPoint(1,i);
	      Channel2[i] = event->GetPoint(2,i);
	      Channel2b[i] = event->GetPoint(3,i);
	      
	      if(Channel1[i] >= 254)  //==255)
		{
		  FixGain[0] = true;
		}
	      if(Channel2[i] >= 254)
		{
		  FixGain[1] = true;
		}
	    
	      Channel1[i]  -= Channel1_Offset;
	      Channel1b[i] -= Channel1b_Offset;
	      Channel2[i]  -= Channel2_Offset;
	      Channel2b[i] -= Channel2b_Offset;
	    }

	  if(FixGain[0])
	    {
	      MergeWaveforms(ChannelSize,Channel1,Channel1b,Channel1_Offset);
	    }
	  if(FixGain[1])
	    {
	      MergeWaveforms(ChannelSize,Channel2,Channel2b,Channel2_Offset);
	    }


	  cout << "# " << EventN << endl;
	  TMinuit *gMinuit;
	  Double_t arglist[10];
	  Int_t ierflg = 0;
	  //Set starting values, bounds, and step sizes for parameters
	  //	  Double_t Rnd = random()/RAND_MAX;
	  static Double_t vstart[1] = {0};//0.00004*Rnd };
	  static Double_t step[1]   = {0.0000001};
	  static Double_t minvals[1]= {0.00000};
	  static Double_t maxvals[1]= {0.00000};
	  Double_t a,aerr;

	  //Fit Channel1
	  gMinuit = new TMinuit(1);
	  Imax = ChannelSize - 500;
	  RawWave = Channel1;
	  Sigma = Channel1_Offset;
	  gMinuit->SetFCN(fcn);
	  gMinuit->mnparm(0,"a",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
	  //Set output print level
	  // -1 : no output
	  //  0 : minimum output
	  //  1 : standard output
	  //  2 : additional output (intermediate results)
	  //  3 : maximum output (progress of minimizations)
	  gMinuit->SetPrintLevel(-1);
	  //Set error definition
	  // 1   : Chi square
	  // 0.5 : Negative log likelihood
	  gMinuit->SetErrorDef(1);
	  //Minimization strategy
	  // 0 : fewer function calls (less reliable)
	  // 1 : standard
	  // 2 : try to improve minimum (slower)
	  arglist[0] = 1;
	  gMinuit->mnexcm("SET STR", arglist, 1, ierflg);
	  //Call MIGRAD with 5000 iterations maximum
	  arglist[0] = 10000;     //max number of iterations
	  arglist[1] = 0.0000001;    //tolerance
	  gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
	  gMinuit->GetParameter(0,a,aerr);
	  PMT0->Fill(a);
	  delete gMinuit;
	  
	  
	  //Fit Channel2
	  gMinuit = new TMinuit(1);
	  Imax = ChannelSize - 500;
	  RawWave = Channel2;
	  Sigma = Channel2_Offset;
	  gMinuit->SetFCN(fcn);
	  gMinuit->mnparm(0,"a",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
	  //Set output print level
	  // -1 : no output
	  //  0 : minimum output
	  //  1 : standard output
	  //  2 : additional output (intermediate results)
	  //  3 : maximum output (progress of minimizations)
	  gMinuit->SetPrintLevel(-1);
	  //Set error definition
	  // 1   : Chi square
	  // 0.5 : Negative log likelihood
	  gMinuit->SetErrorDef(1);
	  //Minimization strategy
	  // 0 : fewer function calls (less reliable)
	  // 1 : standard
	  // 2 : try to improve minimum (slower)
	  arglist[0] = 1;
	  gMinuit->mnexcm("SET STR", arglist, 1, ierflg);
	  //Call MIGRAD with 5000 iterations maximum
	  arglist[0] = 10000;     //max number of iterations
	  arglist[1] = 0.0000001;    //tolerance
	  gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
	  gMinuit->GetParameter(0,a,aerr);
	  PMT1->Fill(a);
	  delete gMinuit;
	}
      delete [] Channel1;                   
      delete [] Channel1b;                  
      delete [] Channel2;                   
      delete [] Channel2b;                  
      delete [] CorrectedWave;              
      cout << "Closing file : " << filename << endl;
      InFile->Close("R");
      delete InFile;
    }
  TFile * OutFile = new TFile(argv[4],"RECREATE");
  
  Int_t PMT0Max = PMT0->GetMaximumBin();
  Int_t PMT0L = 0;
  Int_t PMT0H = 0;
  Int_t PMT1Max = PMT1->GetMaximumBin();
  Int_t PMT1L = 0;
  Int_t PMT1H = 0;
  for(int i = PMT0Max; i < PMT0->GetNbinsX();i++)
    {
      if(2.0*PMT0->GetBinContent(i) < PMT0->GetBinContent(PMT0Max))
	{
	  PMT0H = i;
	}
    }
  for(int i = PMT0Max; i >0;i--)
    {
      if(2.0*PMT0->GetBinContent(i) < PMT0->GetBinContent(PMT0Max))
	{
	  PMT0L = i;
	}
    }
  
  for(int i = PMT1Max; i < PMT1->GetNbinsX();i++)
    {
      if(2.0*PMT1->GetBinContent(i) < PMT1->GetBinContent(PMT1Max))
	{
	  PMT1H = i;
	}
    }
  for(int i = PMT1Max; i >0;i--)
    {
      if(2.0*PMT1->GetBinContent(i) < PMT1->GetBinContent(PMT1Max))
	{
	  PMT1L = i;
	}
    }
  
  TF1 * f0 = new TF1("f0","gaus",
		     PMT0->GetBinCenter(PMT0L),
		     PMT0->GetBinCenter(PMT0H));
  TF1 * f1 = new TF1("f1","gaus",
		     PMT1->GetBinCenter(PMT1L),
		     PMT1->GetBinCenter(PMT1H));

  f0->SetParameters(PMT0->GetMaximum(),
		    PMT0Max,
		    PMT0->GetBinCenter(PMT0H) - PMT0->GetBinCenter(PMT0Max));

  f1->SetParameters(PMT1->GetMaximum(),
		    PMT1Max,
		    PMT1->GetBinCenter(PMT1H) - PMT1->GetBinCenter(PMT1Max));

  PMT0->Fit(f0,"R");
  PMT1->Fit(f1,"R");

  PMT0->Write();
  PMT1->Write();
  OutFile->Write();
  OutFile->Close();

  ofstream OutFile1("PMT0.dat",ios::app);
  OutFile1 << PMT0_V << " " << f0->GetParameter(1) << endl;
  OutFile1.close();
  ofstream OutFile2("PMT1.dat",ios::app);
  OutFile2 << PMT1_V << " " << f1->GetParameter(1) << endl;
  OutFile2.close();

  return(0);
}
