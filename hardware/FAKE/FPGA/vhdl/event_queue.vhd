--This memory can store up to 51200 words of data. That data is written over the 
--high-level, register-oriented, IPBUS abstraction but it is read out continuously
--as long as RD_EN is set high. The data in DOUT is subsequently serialized and sent
--out over LVDS pairs in the top-level module.
--Boston University CLEAN project--Conor DuBois.


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity event_queue is Port(
	CLKS : in STD_LOGIC;
	CLKR : in STD_LOGIC;
	DEBUG : out STD_LOGIC_VECTOR(7 downto 0);
	RESET : in STD_LOGIC;
	CLKA : in STD_LOGIC;
	CLKB : in STD_LOGIC;
	DIN : in STD_LOGIC_VECTOR(31 downto 0);
	WR_EN : in STD_LOGIC;
	EN : in STD_LOGIC_VECTOR(99 downto 0);
	WADDR : in STD_LOGIC_VECTOR(8 downto 0);
	RD_EN : in STD_LOGIC;
	RD_ADDR : out unsigned(6 downto 0);
	RD_CONF : in STD_LOGIC;
	RD_ACK : out STD_LOGIC;
	ERROR_INFO : out unsigned(31 downto 0);
	FAILED : out STD_LOGIC;
	IMPATIENCE : in unsigned(32 downto 0);
	DOUT2 : out STD_LOGIC_VECTOR(15 downto 0);
	DOUT : out STD_LOGIC_VECTOR(95 downto 0));
end event_queue;

architecture Behavioral of event_queue is
	type multiple is ARRAY(99 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
	signal douts : multiple;
	signal dout1 : STD_LOGIC_VECTOR(31 downto 0);
	signal time_stamp : unsigned(31 downto 0);
	signal raddr : unsigned(8 downto 0) := '0' & x"00";
	signal raddr1 : unsigned(6 downto 0) := "0000000";
	signal clk1_to50 : STD_LOGIC;
	signal clk1_to125 : STD_LOGIC;
	signal clkr_to50 : STD_LOGIC;
	signal clockee50 : unsigned(31 downto 0) := x"00000000";
	signal clockee125 : unsigned(31 downto 0) := x"00000000";
	type state_type is (st_wait1, st_wait2, st_ready, st_pause1, st_pause0, st_pause5,
		st_store, st_send, st_wait3, st_pause2, st_pause3, st_pause4, st_getconf, st_pause10);
	signal state : state_type := st_wait3;
	type five_cycle_storage is ARRAY(4 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
	signal most_significance : five_cycle_storage;
	signal mid_significance : five_cycle_storage;
	signal subtraction : unsigned(31 downto 0) := x"00000000";
	signal subtraction2 : unsigned(31 downto 0) := x"00000000";
	signal parity : STD_LOGIC := '0';
	type three_cycle_storage is ARRAY(2 downto 0) of unsigned(31 downto 0);
	signal future_mask : three_cycle_storage;
	signal edge_check : STD_LOGIC_VECTOR(2 downto 0);
	signal pre_fail : STD_LOGIC;
	
	component synch is Port(
		CLK : in STD_LOGIC;
		RST : in STD_LOGIC;
		S : in STD_LOGIC;
		Y : out STD_LOGIC);
	end component;
begin
	synch1: synch port map(
		CLK => CLKB,
		RST => '0',
		S => CLKS,
		Y => clk1_to50);
	
	synch2: synch port map(
		CLK => CLKA,
		RST => '0',
		S => CLKS,
		Y => clk1_to125);
	
	synch3: synch port map(
		CLK => CLKB,
		RST => '0',
		S => CLKR,
		Y => clkr_to50);

	synch4: synch port map(
		CLK => CLKA,
		RST => '0',
		S => pre_fail,
		Y => FAILED);

	dout1 <= douts(to_integer(raddr1));
	RD_ADDR <= raddr1;
	DEBUG(7) <= RD_EN;
	DEBUG(6 downto 0) <= STD_LOGIC_VECTOR(raddr1);
	ERROR_INFO(27 downto 0) <= parity & subtraction(26 downto 0);
	
	process(CLKA)
	begin
		if CLKA'event and CLKA = '1' then
			if RESET = '1' then
				clockee125 <= x"00000000";
				subtraction <= x"00000000";
			elsif CLKR = '1' then
				clockee125 <= x"00000000";
			elsif clk1_to125 = '1' then
				clockee125 <= clockee125 + x"00000001";
			elsif RD_EN = '0' then
				subtraction <= x"00000000";
			elsif state = st_ready then
				subtraction <= clockee125 - time_stamp;
			end if;
		end if;
	end process;

	process(CLKB)
	begin
		if CLKB'event and CLKB = '1' then
--			if RESET = '1' or clkr_to50 = '1' then
--				clockee50 <= x"00000000";
--			elsif clk1_to50 = '1' then
--				clockee50 <= clockee50 + x"00000001";
--			end if;
			if RESET = '1' then
				clockee50 <= x"00000000";
				subtraction2 <= x"00000000";
			elsif clkr_to50 = '1' then
				clockee50 <= x"00000000";
			elsif clk1_to50 = '1' then
				clockee50 <= clockee50 + x"00000001";
			elsif RD_EN = '0' then
				subtraction2 <= x"00000000";
			elsif (state = st_pause5) or (state = st_ready) then
				subtraction2 <= time_stamp - clockee50;
			end if;
			edge_check <= edge_check(1 downto 0) & IMPATIENCE(32);
			future_mask(1 downto 0) <= future_mask(0) & IMPATIENCE(31 downto 0);
			if (edge_check = "000") or (edge_check = "111") then
				future_mask(2) <= future_mask(1);--avoid transition problems
			end if;
			
			if RESET = '1' or RD_EN = '0' then
				DOUT2(2 downto 0) <= "000";
				RD_ACK <= '0';
				state <= st_wait3;--RD_EN = 0 functions as reset rather than pause
				if state = st_wait3 then
					raddr1 <= "0000000";
				end if;
				raddr <= '0' & x"00";
				DOUT <= (others => '0');
				ERROR_INFO(31 downto 28) <= x"0";
			else
				case state is
				when st_getconf =>
					RD_ACK <= '0';
					if RD_CONF = '1' then
						state <= st_pause5;
						time_stamp <= unsigned(dout1);
					else
						state <= st_getconf;
					end if;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_pause5 =>
					RD_ACK <= '0';
					state <= st_ready;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_ready =>
					RD_ACK <= '0';
					ERROR_INFO(31 downto 28) <= RD_CONF & raddr1(2 downto 0);
--					if time_stamp = clockee50 then
--						state <= st_pause0;--start data output when timestamp reached
--					else
--						state <= st_ready;
--					end if;
					if subtraction2 = x"00000000" then
						state <= st_pause0;--start data output when timestamp reached
					elsif (subtraction2 and future_mask(2)) /= x"00000000" then
						state <= st_pause10;--timestamp too far in the future--skip it
						pre_fail <= '1';
					else
						state <= st_ready;
					end if;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_pause10 =>
					RD_ACK <= '1';
					state <= st_pause2;--skip sending and go straight to acknowledging
					pre_fail <= '0';
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_pause0 =>
					DOUT2(2 downto 0) <= "111";
					RD_ACK <= '0';
					state <= st_pause1;
					raddr <= raddr + ('0' & x"01");
					DOUT <= (others => '0');
				when st_pause1 =>
					RD_ACK <= '0';
					state <= st_store;
					raddr <= raddr + ('0' & x"01");
					DOUT <= (others => '0');
				when st_store =>
					RD_ACK <= '0';
					--put data in storage, move on to st_send after 10 cycles
					if raddr < ('0' & x"07") then
						most_significance <= most_significance(3 downto 0) & dout1;
						state <= st_store;
					else
						DOUT2(8 downto 3) <= most_significance(4)(5 downto 0);
						mid_significance <= mid_significance(3 downto 0) & dout1;
						if raddr = ('0' & x"0B") then
							state <= st_send;
						else
							state <= st_store;
						end if;
					end if;
					raddr <= raddr + ('0' & x"01");
					DOUT <= (others => '0');
				when st_send =>
					RD_ACK <= '0';
					if raddr = ('1' & x"FF") then
						state <= st_wait1;
						raddr <= '0' & x"00";
					else
						state <= st_send;
						raddr <= raddr + ('0' & x"01");
					end if;
					--use up stored data then move to purely lsb
					DOUT2(15 downto 13) <= most_significance(4)(0) &
						mid_significance(4)(0) & dout1(0);--same as opamp 0
					DOUT2(12 downto 9) <= STD_LOGIC_VECTOR(raddr(3 downto 0));
					DOUT(95 downto 64) <= most_significance(4);
					DOUT(63 downto 32) <= mid_significance(4);
					DOUT(31 downto 0) <= dout1;
					most_significance <= most_significance(3 downto 0) & x"00000000";
					mid_significance <= mid_significance(3 downto 0) & x"00000000";
				when st_wait1 =>
					RD_ACK <= '1';
					state <= st_pause2;
					raddr <= '0' & x"00";
					DOUT(95 downto 32) <= (others => '0');
					DOUT(31 downto 0) <= dout1;
				when st_pause2 =>
					RD_ACK <= '1';
					state <= st_pause3;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_pause3 => 
					RD_ACK <= '1';
					state <= st_pause4;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_pause4 =>
					RD_ACK <= '0';
					state <= st_wait2;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_wait2 =>--just to be safe, add an extra wait state...
					RD_ACK <= '0';--...since RD_ACK is sampled in multiple clock domains
					state <= st_wait3;
					if raddr1 = "1100011" then--wrap back to 0 after block ram 99
						raddr1 <= "0000000";
						parity <= not parity;
					else
						raddr1 <= raddr1 + "0000001";
					end if;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				when st_wait3 =>
					DOUT2(2 downto 0) <= "000";
					RD_ACK <= '0';
					state <= st_getconf;
					raddr <= '0' & x"00";
					DOUT <= (others => '0');
				end case;
			end if;
		end if;
	end process;

	memories: for i in 0 to 99 generate
		memory0to99: RAMB16BWER generic map(
			DATA_WIDTH_A => 36,
			DATA_WIDTH_B => 36,
			WRITE_MODE_A => "READ_FIRST",
			WRITE_MODE_B => "READ_FIRST") port map(
			CLKA => CLKA,
			WEA(0) => WR_EN,
			WEA(1) => WR_EN,
			WEA(2) => WR_EN,
			WEA(3) => WR_EN,
			ENA => EN(i),
			REGCEA => '0',
			ADDRA(13 downto 5) => WADDR,
			ADDRA(4 downto 0) => "00000",
			DIA => DIN,
			DIPA => x"0",
			DOA => open,
			DOPA => open,
			RSTA => RESET,
			CLKB => CLKB,
			WEB => x"0",
			ENB => '1',
			REGCEB => '0',
			ADDRB(13 downto 5) => STD_LOGIC_VECTOR(raddr),
			ADDRB(4 downto 0) => "00000",
			DIB => x"00000000",
			DIPB => x"0",
			DOB => douts(i),
			DOPB => open,
			RSTB => RESET);
	end generate;
end Behavioral;

