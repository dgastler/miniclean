#include <V1720_XML.h>


V1720_XML::V1720_XML(DCMySQL * conn,int runNumber)
  : Thread_XML(conn,runNumber)
{
}

bool V1720_XML::Generate_XML(xmlNode * threadsNode,
			     std::string pcName,
			     std::string threadType,
			     std::string threadName,
			     xmlNode * responseNode)
{
  std::stringstream query;

  bool ret = Thread_XML::Generate_XML(threadsNode,pcName,threadType,threadName,responseNode);


  //==============================================================================================
  //Get list of wfds for this thread
  //==============================================================================================  
  query << "select BoardID,BdType,BdNum,VMEBaseAddress,Link,VMEControl,ChannelsConfig,MemConfig,PostSamples,FrontPanel,TriggerSource,TriggerOutput,AcquisitionControl,MonitorMode,InternalDelay,CanStart,CustomSize from Settings_WFDs" 
	<< " where thread_name='" << threadName << "'"
	<< " and pc_name='" << pcName << "'"  
	<< " and run_number=" << runNumber
	<< " order by BoardID";
  enum {BOARD_ID=0,BD_TYPE,BD_NUM,VMEBASE_ADDRESS,LINK,VME_CONTROL,
	CHANNELS_CONFIG,MEM_CONFIG,POST_SAMPLES,FRONT_PANEL,
	TRIGGER_SOURCE,TRIGGER_OUTPUT,ACQUISITION_CONTROL,MONITOR_MODE,INTERNAL_DELAY,CAN_START,CUSTOM_SIZE};
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  std::vector<std::vector<std::string> > wfdData;
  DCMySQLConnection->GetTable(wfdData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  //==============================================================================================
  //Get list of pmts for those WFDs
  //==============================================================================================  
  query << "select wfd_id,pmt_id,wfd_pos,pc_name,baseline,presamples,zs_thresh_edge_trig,zs_thresh_val,zs_nsamp_nlbk,zs_nsamp_nlfwd,hitthresh,nsamples,dac_offset,channeltype from Settings_PMTs"
	<< " where (";
  for(size_t iWFD = 0; iWFD < wfdData.size() ;iWFD++)
    {
      query << "wfd_id=" << wfdData[iWFD][BOARD_ID];
      if(iWFD < (wfdData.size()-1))
	{
	  query << " or ";
	}
    }
  query << ") and run_number=" << runNumber << " order by wfd_id;";
  enum{WFD_ID=0,PMT_ID,WFD_POS,PC_NAME,BASELINE,PRESAMPLES,ZS_THRESH_EDGE,
       ZS_THRESH_VAL,ZS_NSAMP_NLBK,ZS_NSAMP_NLFWD,HIT_THRESH,NSAMPLES,DAC_OFFSET,CHANNEL_TYPE};
  std::vector<std::vector<std::string> > pmtData;
  
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  DCMySQLConnection->GetTable(pmtData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());



  //Create the WFDs node
  xmlNode *wfdsNode=xmlNewChild(threadNode,NULL,(xmlChar *)"WFDS",NULL);

  for(size_t iWFD = 0; iWFD < wfdData.size();iWFD++)
    {
      //Build the WFD node
      xmlNode * wfdNode = xmlNewChild(wfdsNode,NULL,(xmlChar *)"WFD",NULL);
      //Build the Comm XML block
      xmlNode * commNode = xmlNewChild(wfdNode,NULL,(xmlChar*)"Comm",NULL);
      xmlNewTextChild(commNode,NULL,(xmlChar*) "BdType",(xmlChar*) wfdData[iWFD][BD_TYPE].c_str());
      xmlNewTextChild(commNode,NULL,(xmlChar*) "Link",(xmlChar*) wfdData[iWFD][LINK].c_str());
      xmlNewTextChild(commNode,NULL,(xmlChar*) "BdNum",(xmlChar*) wfdData[iWFD][BD_NUM].c_str());
      xmlNewTextChild(commNode,NULL,(xmlChar*) "VMEBaseAddress",(xmlChar*) wfdData[iWFD][VMEBASE_ADDRESS].c_str());
      //Build the Build the board block
      xmlNode * boardNode = xmlNewChild(wfdNode,NULL,(xmlChar*)"Board",NULL);
      if(!boost::algorithm::iequals(wfdData[iWFD][CAN_START],"NULL"))
	{
	  xmlNewTextChild(boardNode,NULL,(xmlChar*) "CAN_START",(xmlChar*) wfdData[iWFD][CAN_START].c_str());
	}
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "BoardID",(xmlChar*) wfdData[iWFD][BOARD_ID].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "VMEControl",(xmlChar*) wfdData[iWFD][VME_CONTROL].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "ChannelsConfig",(xmlChar*) wfdData[iWFD][CHANNELS_CONFIG].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "MemConfig",(xmlChar*) wfdData[iWFD][MEM_CONFIG].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "PostSamples",(xmlChar*) wfdData[iWFD][POST_SAMPLES].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "FrontPanel",(xmlChar*) wfdData[iWFD][FRONT_PANEL].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "TriggerSource",(xmlChar*) wfdData[iWFD][TRIGGER_SOURCE].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "TriggerOutput",(xmlChar*) wfdData[iWFD][TRIGGER_OUTPUT].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "AcquisitionControl",(xmlChar*) wfdData[iWFD][ACQUISITION_CONTROL].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "MonitorMode",(xmlChar*) wfdData[iWFD][MONITOR_MODE].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "InternalDelay",(xmlChar*) wfdData[iWFD][INTERNAL_DELAY].c_str());
      xmlNewTextChild(boardNode,NULL,(xmlChar*) "CustomSize",(xmlChar*) wfdData[iWFD][CUSTOM_SIZE].c_str());
      //This is a little inefficient, but will have to do for now. 
      for(size_t iPMT = 0; iPMT < pmtData.size();iPMT++)
	{
	  if(boost::algorithm::iequals(pmtData[iPMT][WFD_ID],
				       wfdData[iWFD][BOARD_ID]) )
	    {
	      char zs_text[]="0x12345678";
	      xmlNode * channelNode = xmlNewChild(boardNode,NULL,(xmlChar*)"CHANNEL",NULL);
	      xmlNewTextChild(channelNode,NULL,(xmlChar*) "ChannelID",(xmlChar*) pmtData[iPMT][WFD_POS].c_str());
	      
	      uint32_t zs_thresh= strtoul(pmtData[iPMT][ZS_THRESH_VAL].c_str(),NULL,0);
	      zs_thresh |= (strtoul(pmtData[iPMT][ZS_THRESH_EDGE].c_str(),NULL,0) << 31);
	      sprintf(zs_text,"0x%08X",zs_thresh);	      
	      xmlNewTextChild(channelNode,NULL,(xmlChar*) "ZS_THRESH",(xmlChar*) zs_text);

	      xmlNewTextChild(channelNode,NULL,(xmlChar*) "Threshold",(xmlChar*) pmtData[iPMT][HIT_THRESH].c_str());
	      xmlNewTextChild(channelNode,NULL,(xmlChar*) "NSamples",(xmlChar*) pmtData[iPMT][NSAMPLES].c_str());
	      xmlNewTextChild(channelNode,NULL,(xmlChar*) "DAC",(xmlChar*) pmtData[iPMT][DAC_OFFSET].c_str());

	      uint32_t zs_nsamp = strtoul(pmtData[iPMT][ZS_NSAMP_NLBK].c_str(),NULL,0)<<16;	      
	      zs_nsamp |= strtoul(pmtData[iPMT][ZS_NSAMP_NLFWD].c_str(),NULL,0);	      	      
	      sprintf(zs_text,"0x%08X",zs_nsamp);
	      xmlNewTextChild(channelNode,NULL,(xmlChar*) "ZS_NSAMP",(xmlChar*) zs_text);    
	     }
	}
    }
  return ret;
}
