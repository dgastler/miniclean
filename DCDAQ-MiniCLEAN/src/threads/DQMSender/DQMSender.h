#ifndef __DQMSender__
#define __DQMSender__

#include <DCThread.h>
#include <RATCopyQueue.h>
#include <xmlHelper.h>
#include <RAT/DS/Root.hh>
#include <cstdio>
#include <zmq.hpp>
#include <zmq.h>
#include <RAT/DS/DCDAQ_DQMStructs.hh>
#include <sstream>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>


#include <netHelper.h>
#include <map>
#include <sys/time.h>
#include <RAT/DS/DCDAQ_RAWDataStructs.hh>
#include "WriteTime.h"
#include "RAT/DS/DCDAQ_EventStreamHelper.hh"
#include <boost/algorithm/string.hpp>
#define PERM_FILE   (S_IRUSR | S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH| S_IWOTH)

class DQMSender : public DCThread 
{
 public:
  DQMSender(std::string Name);
  ~DQMSender()
    {
         };
  virtual bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  virtual void PrintStatus(){
    std::stringstream status;
    pid_t tid=syscall(SYS_gettid);
    status<<"id "<<tid;
    Print(status.str().c_str());
    DCThread::PrintStatus();
  }
  virtual void ThreadCleanUp()
{


 
 daqSocketDistro->close();
 delete daqSocketDistro;
 daqServerPull->close();
 delete daqServerPull;
 daqServerReq->close();
 delete daqServerReq;
 map<uint16_t,zmq::socket_t*>::iterator zmq_iter;
 while((zmq_iter=writezmqMap.begin()) != writezmqMap.end())
	{

	  //send stop message to all
	  zmq::message_t message(sizeof(int));
	  int msg=ZMQ::MSGSTOP;
	  memcpy(message.data(),&msg,sizeof(int));
	  zmq_iter->second->send(message,ZMQ_NOBLOCK);
	  zmq_iter->second->close();

	  delete zmq_iter->second;
	    
	  zmq_iter->second=NULL;
	    
	  writezmqMap.erase(zmq_iter);
	}
      
      delete context;
}

 
  map<uint16_t,zmq::socket_t*> writezmqMap;
 
  int portStart;
  int portCurrentInc;
  int zmqfd;
  int zmqfdReq;
  int zmqfdPull;
  vector<int> availPorts;
  zmq::context_t * context;
  zmq::socket_t * daqSocketDistro;
  zmq::socket_t * daqServerReq;
  zmq::socket_t * daqServerPull;
  ClientMessage::header head;
  
 
  //  timeval timeV;
 
};


#endif
