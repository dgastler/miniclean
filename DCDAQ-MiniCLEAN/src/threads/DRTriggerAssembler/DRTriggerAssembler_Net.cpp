#include "DRTriggerAssembler.h"


int DRTriggerAssembler::InitializeTriggerBox()
{
  time_t startTime = time(NULL);
  SetRunStartTime(startTime);
  clock_high = startTime*125000000 >> 32;
  //fprintf(stderr,"VENATOR clock high: %u\n",clock_high);
  bzero(packetBytes, 50);
  packet[2] = htonl(clock_low);
  packet[3] = htonl(clock_high);
  packet[4] = htonl(next_event);
  packet[5] = htonl((((uint32_t)trigger_active_time) << 12) | (((uint32_t)trigger_mask) << 4) 
			  | (use_external_clock << 2) | enable_triggers);
  packet[6] = htonl((((uint32_t)trigger_offset) << 16) | gate_duration);
  packet[7] = htonl(GetRunNumber());
  //  fprintf(stderr,"enable_triggers: %d , packet word: 0x%08X\n",enable_triggers,packet[5]);
  return write_registers(6, 1);
}


int DRTriggerAssembler::write_registers(uint16_t words, uint32_t address)
{
  packet[0] = htonl((((uint32_t)packetID)<<24) | (((uint32_t)words)<<8) | WRITE_COMMAND);
  packet[1] = htonl(address);

  uint8_t * packet_ptr = (uint8_t*) packet;
  int packet_size = 4*(words+2);

  for(size_t i = 0; i < 10; i++)
    {
      int ret_size = sendto(sockfd,packet_ptr,packet_size,0,(struct sockaddr *)&servaddr,sizeof(servaddr));
      if((ret_size >= 0) && (ret_size != packet_size))
	{
	  packet_size -= ret_size;
	  packet_ptr += ret_size;	  
	}      
      else if(ret_size == packet_size)
	{
	  break;
	}
      
      //      sendto(sockfd,packetBytes,4*(words+2),0,(struct sockaddr *)&servaddr,sizeof(servaddr));
    }

  //Setup file descriptor sets for the board response.  
  fd_set waitReadSet;
  FD_ZERO(&waitReadSet);
  FD_SET(sockfd,&waitReadSet);
  //Set timeout for the board's response
  struct timeval selectTimeout = {1,500000};  //Wait 1s 100ms for a response
  int selectRet = select(sockfd+1,&waitReadSet,NULL,NULL,&selectTimeout);
  //If this is positive, then there is data for recvfrom to read
  if(selectRet > 0)
    return recvfrom(sockfd,packetBytes,200,0,NULL,NULL);  
  //If there is no data after 100ms, just return -1 and move on with life.
  return -1;
}

int DRTriggerAssembler::read_registers(uint16_t words, uint32_t address, uint8_t hidden)
{
  //Setup file descriptor sets for the board response.  
  fd_set waitReadSet;
  fd_set waitWriteSet;
  //Set timeout for the board's response
  struct timeval selectTimeout = {0,100000};  //Wait 100ms for a response


  int n = 0;
  bzero(packetBytes, 50);
  packet[0] = htonl((((uint32_t)packetID)<<24) | (((uint32_t)words)<<8) | READ_COMMAND);
  packet[1] = htonl(address);

  //Check if we can write to the socket
  FD_ZERO(&waitWriteSet);
  FD_SET(sockfd,&waitWriteSet);
  int selectRet = select(sockfd+1,NULL,&waitWriteSet,NULL,&selectTimeout);
  if(selectRet > 0)    
    {
      sendto(sockfd,packetBytes,8,0,(struct sockaddr *)&servaddr,sizeof(servaddr));
    }
  else
    {
      return -1;
    }
  //Do the reading
  do
    {
      FD_ZERO(&waitReadSet);
      FD_SET(sockfd,&waitReadSet);
      selectTimeout.tv_sec = 0;  //Wait 100ms for a response
      selectTimeout.tv_usec = 100000;
      selectRet = select(sockfd+1,&waitReadSet,NULL,NULL,&selectTimeout);
      //If this is positive, then there is data for recvfrom to read
      if(selectRet > 0)
	{
	  n = recvfrom(sockfd,packetBytes,1500,0,NULL,NULL);
	  if(n == -1)
	    {
	      break;
	    }
	  if(!hidden)
	    {
	      sEvent event;
	      memset(&event,0,sizeof(event));
	      for(int index = 1; index < (n >> 2); index += 1)
		{
		  switch(parsing_position)
		    {
		    case 0:
		      event.timeStamp = ntohl(packet[index]);
		      break;
		    case 1:
		      event.timeStamp |= (((uint64_t)ntohl(packet[index])) << 32);
		      break;
		    case 2:
		      event.triggerCounterNibbles = ntohl(packet[index]);
		      break;
		    default:
		      event.eventID = ntohl(packet[index]);
		      event.has_been_blocked = false;
		      event_queue.Add(event);
		      break;
		    }
		  parsing_position = (parsing_position + 1) & 3;
		}
	    }
	}
      else if(selectRet == 0)
	{
	  if(!Loop)
	    {
	      return -1;
	    }
	}
      else
	{
	  return -1;
	}
    } while(packetBytes[3] == 0x1D);
  return n;
}
