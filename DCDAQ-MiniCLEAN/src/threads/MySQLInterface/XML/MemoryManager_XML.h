#ifndef MEMORYMANAGER_XML_H
#define MEMORYMANAGER_XML_H

#include <libxml/tree.h>
#include <libxml/parser.h>

#include <vector>
#include <string>
#include <sstream>

#include <boost/algorithm/string.hpp>

#include <DCMySQL.h>


class MemoryManager_XML
{
 public:
  MemoryManager_XML(DCMySQL * conn,int runNumber);

  //===========================
  //General Generate_MM_XML Function
  //===========================
  virtual bool Generate_XML(xmlNode * memManagersNode,
			    std::string pcName,
			    std::string mmType,
			    std::string mmName);


 protected:
  DCMySQL * DCMySQLConnection;
  int runNumber;

  xmlNode * memManagerNode;
 private:
  bool FillData(const std::string & pcName,
		const std::string & memManagerType,
		const std::string & memManagerName,
		std::vector<std::vector<std::string> > &memManagerTemplate,
		std::vector<std::vector<std::string> > &memManagerData,
		std::vector<std::vector<std::string> > &memManagerDefaults,
		std::vector<std::vector<std::string> > &memManagerRequired);
    };
#endif
