#include <FEPCRATManager.h>

FEPCRATManager::FEPCRATManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("FEPCRATManager"));
  //Make sure our memory vector is empty
  Memory.clear();
  ShutdownStatus = false;
  badValue = -2;
  emptyValue = -1;
}

FEPCRATManager::~FEPCRATManager()
{
  Deallocate();
}

void FEPCRATManager::Deallocate()
{
  if(!LockObject())
    {
      fprintf(stderr,"Lock error on deallocation\n");
      abort();
    }
  //Clear our containers
  Free.Clear(true);
  UnSent.Clear(true);
  StorageEvent.Clear(true);
  AssembledEvent.Clear(true);
  Send.Clear(true);
  BadEvent.Clear(true); 
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //      delete FEPC event (check if it's not NULL)
      if(Memory.back().ev)
	{
	  delete Memory.back().ev;
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
  UnlockObject();
}

void FEPCRATManager::WakeUp()
{
  //child objects
  Free.WakeUp();
  UnSent.WakeUp();
  Send.WakeUp();
  BadEvent.WakeUp();
  
  AssembledEvent.WakeUp();
  StorageEvent.WakeUp();
}
void FEPCRATManager::Shutdown()
{
  //Set to shutdown mode.   Now we can't wait on mutex locks
  //Child objects
  Free.ShutdownWait();
  UnSent.ShutdownWait();
  Send.ShutdownWait();
  BadEvent.ShutdownWait();
  
  AssembledEvent.ShutdownWait();
  StorageEvent.ShutdownWait();

  ShutdownStatus = true;
  
  //call wakeup to force everyone out of a mutex condition wait.
  WakeUp();
}

bool FEPCRATManager::AllocateMemory(xmlNode * setupNode)
{
  if(!LockObject())
    {
      fprintf(stderr,"Lock error on allocation\n");
      abort();
    }
  bool ret = true;
  
  int NumFEPCBlocksERR = GetXMLValue(setupNode,"NUMFEPCBLOCKS","MANAGER",NumFEPCBlocks);
  if(NumFEPCBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMFEPCBLOCKS in FEPCRATManager.\n");
      ret = false;
    }
  int StorageEventSizeERR = GetXMLValue(setupNode,"STORAGEEVENTSIZE","MANAGER",StorageEventSize);
  if(StorageEventSizeERR != 0)
    {
      fprintf(stderr,"Error reading StorageEventSize in FEPCRATManager.\n");
      ret = false;
    }
  int AssembledEventSizeERR = GetXMLValue(setupNode,"ASSEMBLEDEVENTSIZE","MANAGER",AssembledEventSize);
  if(AssembledEventSizeERR != 0)
    {
      fprintf(stderr,"Error reading AssembledEventSize in FEPCRATManager.\n");
      ret = false;
    }

  //Allocate the needed RAT FEPC blocks
  if(ret)
    {
      for(uint32_t iFEPCBlock = 0; iFEPCBlock < NumFEPCBlocks;iFEPCBlock++)
	{
	  FEPCBlock evBlock;	  
	  Memory.push_back(evBlock);
	  Memory.back().ev = new RAT::DS::EV;	  
	  int32_t index = Memory.size()-1;
	  Free.Add(index);
	  if(Memory.back().ev == NULL)
	    {
	      ret = false;
	      break;
	    }
	}     
    }
  //Since we have just setup this MM we should set shutdown status to false
  ShutdownStatus =false;


  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash 
      StorageEvent.Setup(StorageEventSize,emptyValue,badValue);
      AssembledEvent.Setup(AssembledEventSize,emptyValue,badValue);
    }

  //Setup Queues
  Free.Setup(badValue);
  UnSent.Setup(badValue);
  Send.Setup(badValue);
  BadEvent::sBadEvent badEventValue;
  badEventValue.eventID = -1;
  badEventValue.index = badValue;
  badEventValue.errorCode = BadEvent::NO_ERROR;
  BadEvent.Setup(badEventValue);

  UnlockObject();
  return(ret);
}

void FEPCRATManager::PrintStatus(int indent)
{
  printf("\n");
  if(indent > 0)
    {
      for(int i = 0;i < indent;i++)
	{
	  printf(" ");
	}
    }
# if __WORDSIZE == 64
  char printCommand[] = "FEPCRATManager\n     Size: %04lu\n     Free: %04lu\n     Active Assembled: %04lu(%04lu)\n     Active Storage: %04lu(%04lu)\n     UnSent: %04lu\n     ToSend: %04lu\n     BadEvent: %04lu\n";
# else
  char printCommand[] = "FEPCRATManager\n     Size: %04u\n     Free: %04u\n     Active Assembled: %04u(%04u)\n     Active Storage: %04u(%04u)\n     UnSent: %04u\n     ToSend: %04u\n     BadEvent: %04u\n";
#endif
  printf(printCommand,
	 Memory.size(),
	 Free.GetSize(),
	 AssembledEvent.GetActiveEntryCount(),
	 AssembledEvent.GetSize(),
	 StorageEvent.GetActiveEntryCount(),
	 StorageEvent.GetSize(),
	 UnSent.GetSize(),
	 Send.GetSize(),
	 BadEvent.GetSize());

}




