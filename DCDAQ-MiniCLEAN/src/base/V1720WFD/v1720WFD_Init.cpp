#ifdef __CAENLIBS__

#include "v1720WFD.h"
#include "xmlHelper.h"

int v1720::LoadXMLConfig(xmlNode * WFDNode)
{
  int ret = 0;
  //==============================================================================
  //Find the connection xml node.
  //==============================================================================
  if(FindSubNode(WFDNode,"Comm") == NULL)
    {
      ret++;
      fprintf(stderr,"WFD Comm XML lookup failed.\n");
      return(ret);
    }
  //Found the XML Node, passing it to CommInit to setup CONET link.
  ret += CommInit(FindSubNode(WFDNode,"Comm"));
  //If the comm setup is correct
  if(ret == 0)
    {
      //==============================================================================
      //Find the Board xml node.
      //==============================================================================
      if(FindSubNode(WFDNode,"Board") == NULL)
	{
	  ret++;
	  fprintf(stderr,"WFD Board XML lookup failed.\n");
	  return(ret);
	}
      //Found the XML Node, passing it to BoardInit to setup this V1720
      ret += BoardInit(FindSubNode(WFDNode,"Board"));
    }
  if(ret == 0)
    {
      //==============================================================================
      //Read other commands and process them
      //==============================================================================
      if(FindSubNode(WFDNode,"ExtraCommands") != NULL)
	{
	  ProcessCommands(FindSubNode(WFDNode,"ExtraCommands"));
	}
    }


  printf("\tV1720 Board %u\n",BdNum);

  return ret;
}


















int v1720::CommInit(xmlNode * commNode)
{
  int ret = 0;
  //==============================================================================
  //Look for this V1720's XML Link ID
  //==============================================================================
  ret += GetXMLValue(commNode,"BdType","  Comm",BdName);
  if(ret > 0)
    {
      return ret ;
    }
  else
    {
      if(boost::iequals(BdName,"PCI"))
	{
	  BdType=CAEN_DGTZ_PCI_OpticalLink;
	}
      else if(boost::iequals(BdName,"PCIE"))
	{
	  BdType=CAEN_DGTZ_PCIE_OpticalLink;
	}
      else if(boost::iequals(BdName,"USB"))
	{
	  BdType=CAEN_DGTZ_USB;
	}
      else
	{
	  fprintf(stderr,"Bad board type %s\n",BdName.c_str());
	  return ++ret;
	}
    }

  //==============================================================================
  //Look for this V1720's XML Link ID
  //==============================================================================
  ret += GetXMLValue(commNode,"Link","  Comm",Link);
  if(ret > 0)
    {
      return(ret);
    }

  //==============================================================================
  //Look for this V1720's XML BdNum
  //==============================================================================
  ret += GetXMLValue(commNode,"BdNum","  Comm",BdNum);
  if(ret > 0)
    {
      return(ret);
    }
  
  //==============================================================================
  //Look for this V1720's XML VME address
  //==============================================================================
  ret += GetXMLValue(commNode,"VMEBaseAddress","  Comm",VMEBaseAddress);
  if(ret > 0)
    {
      return(ret);
    }
  //==============================================================================
  //Setup and check the CONET connection
  //==============================================================================
  CAEN_DGTZ_ErrorCode CAENret;
  fprintf(stderr,"%d %d %d 0x%08X\n",BdType,Link,BdNum,VMEBaseAddress);
  //  CAENret = CAEN_DGTZ_OpenDigitizer(BdType,Link,BdNum,VMEBaseAddress,&Handle);  
  CAENret = CAEN_DGTZ_OpenDigitizer(BdType,Link,BdNum,0,&Handle);  
  if(CAENret != CAEN_DGTZ_Success)
    {
      //      char * CAENerr = new char[1000];
      //      CAENComm_DecodeError( CAENret,CAENerr); 
      fprintf(stderr,"CAENComm lib returned: %d\n",CAENret);
      //      delete [] CAENerr;
    }
  else
    {
      fprintf(stdout,"CONET connection made for link %d.%d\n",Link,BdNum);
      //      CAENVME_DeviceReset(Handle);
    }
  ret+=CAENret;
  return ret;
}

















int v1720::BoardInit(xmlNode *boardNode)
{
  int ret = 0;
  int CAENret = 0;
  uint32_t RegisterReturn;
  RegisterReturn = 0;


  //==============================================================================
  //Apply a software reset and clear to the V1720
  //==============================================================================
  ret += CAEN_DGTZ_Reset(Handle);
  if(ret!=0)
    printf("Failed to reset board\n");
  usleep(1000);
  //==============================================================================
  //Check that there is a valid board at this location.
  //==============================================================================
  //read the ROCFPGA date/version 
  CAENret = CAEN_DGTZ_GetInfo(Handle,&boardInfo);
  if(CAENret == 0)
    {
      xmlNode * newNode = AddNewSubNode(boardNode,"ROCFPGA");
      char * buffer = new char[100];
      sprintf(buffer,"%s",
	      boardInfo.ROC_FirmwareRel);
      printf("\t\tROCFPGA firmware: %s\n",
	     boardInfo.ROC_FirmwareRel);
      xmlNodeSetContent(newNode,(xmlChar*)buffer);
      ROCFirmware = atof(boardInfo.ROC_FirmwareRel);
    }
  ret+=CAENret;
  if(ret != 0)
    {
      return ret ;
    }
  

  //==============================================================================
  //Look for the BoardID setting.
  //==============================================================================
  //Check if the value exists in the XML
  xmlNode * boardID_Node = FindSubNode(boardNode,"BoardID");
  if(boardID_Node != NULL)
    //Value exists
    {
      ret += GetXMLValue(boardNode,"BoardID","  Board",BoardID);
      //Write value and then do a read-back check
      ret += SetRegister(VMEBaseAddress + 0xEF08,BoardID,"BoardID");
      if(ret != 0)
	{
	  printf("Failed in getting BoardID\n");
	  return(ret);
	}
    }  
  else
    {
      //Get the value from the board (GEO for VME64X, something random from any other board)
      CAENret = CAEN_DGTZ_ReadRegister(Handle,VMEBaseAddress+0xEF08,&BoardID);
      if(CAENret != 0)
	return(CAENret);
    }
  


  //==============================================================================
  //Look for the VMEControl settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"VMEControl","  Board",VMEControl);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check.
  ret += SetRegister(VMEBaseAddress+ 0xEF00,VMEControl,"VMEControl");


  //==============================================================================
  //Look for the ChannelsConfig settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"ChannelsConfig","  Board",ChannelsConfig);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8000,ChannelsConfig,"ChannelsConfig");


  //==============================================================================
  //Look for the MemConfig settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"MemConfig","  Board",MemConfig);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x800C,MemConfig,"MemConfig");



  //==============================================================================
  //Look for the CustomSize settings.
  //==============================================================================
  //Get value from XML
  if(FindSubNode(boardNode,"CustomSize") != NULL)
    {
      ret += GetXMLValue(boardNode,"CustomSize","  Board",CustomSize);
      if(ret != 0)
	{
	  return(ret);
	}
      //Write value and then do a read-back check
      ret += SetRegister(VMEBaseAddress + 0x8020,CustomSize,"CustomSize");
    }

  //==============================================================================
  //Look for the PostSamples settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"PostSamples","  Board",PostSamples);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8114,PostSamples,"PostSamples");

  //==============================================================================
  //Look for the WFDStart settings.
  //==============================================================================
  //Get value from XML
  if(FindSubNode(boardNode,"CAN_START") == NULL)
    sendStart = false;
  else
    {
      sendStart = true;
      if(ROCFirmware >= 3.4)
	{
	  //To follow the CAEN example, a can-start WFD turns off trigger ins
	  //Write to undocument CAEN register ( I hate that)
	  //ret += SetRegister(VMEBaseAddress + 0x817C,1);
	}
    }

  //Look for the Front panel I/O settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"FrontPanel","  Board",FrontPanelControl);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x811C,FrontPanelControl,"FrontPanel");

  //==============================================================================
  //Look for the TriggerSource settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"TriggerSource","  Board",TriggerSource);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x810C,TriggerSource,"TriggerSource");

  //==============================================================================
  //Look for the TriggerOutput settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"TriggerOutput","  Board",TriggerOutput);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8110,TriggerOutput,"TriggerOutput");

  //==============================================================================
  //Look for the Acquisition control  settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"AcquisitionControl","  Board",AcquisitionControl);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8100,AcquisitionControl,"AcquisitionControl");

  //==============================================================================
  //Look for the Monitor Mode  settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"MonitorMode","  Board",MonitorMode);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8144,MonitorMode,"MonitorMode");

  //==============================================================================
  //Look for the Internal delay value.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"InternalDelay","  Board",InternalDelay);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8170,InternalDelay,"InternalDelay");
    
  //==============================================================================
  //Setup each V1720 channel
  //==============================================================================
  int numberOfChannels = NumberOfNamedSubNodes(boardNode,"Channel");
  for(int ichan = 0; ichan < numberOfChannels;ichan++)
    {
      //==========================================================================
      //Find the ichanth Channel xml node.
      //==========================================================================
      if(FindIthSubNode(boardNode,"Channel",ichan) == NULL)
	{
	  ret++;
	  fprintf(stderr,"    Channel  XML lookup failed.\n");
	  return(ret);
	}
      //Found the XML Node, passing it to BoardInit to setup this V1720
      ret += ChannelInit(FindIthSubNode(boardNode,"Channel",ichan));
    }

  //==============================================================================
  //Setup channel mask
  //==============================================================================
  //Calculate the channel mask;
  ChannelMask = GetChannelMask();
  //write channel mask to WFD
  CAENret = SetRegister(VMEBaseAddress + 0x8120,ChannelMask,"ChannelMask");
  //Add channel mask to the output XML string.
  if(CAENret == 0)
    {
      xmlNode * newNode = AddNewSubNode(boardNode,"ChannelMask");
      char * buffer = new char[100];
      sprintf(buffer,"0x%08X",ChannelMask);
      xmlNodeSetContent(newNode,(xmlChar*)buffer);
    }
  ret+=CAENret;  


  //==============================================================================
  //Use this information to setup EventSize.
  //Event size in bytes.
  //==============================================================================
  //           ( # channels      *Maximum Size)                      Header size
  uint32_t memorySize = 0;
  CAENret = CAEN_DGTZ_ReadRegister(Handle,VMEBaseAddress+0x8140,&memorySize);
  memorySize = 0xFF&(memorySize >> 8); //MBytes per channel
  memorySize = memorySize >> 1;
  EventSize = (memorySize*(numberOfChannels * 1024 * 1024) >> (MemConfig + 1)) + 0x4; //32bit words
  //  EventSize = EventSize << 2; //Convert 32bit words to bytes



  
  //==============================================================================
  //Check if everything has been setup correctly.
  //==============================================================================
  if(ret == 0)
    {
      Ready = true;
    }
  else
    {
      Ready = false;
    }
  return(ret);
}



void v1720::ProcessCommands(xmlNode * commandNode)
{
  //Loop over all the extra commands and put them in the ExtraCommands map
  int numberOfCommands = NumberOfNamedSubNodes(commandNode,"Command");
  int errors = 0;
  for(int i = 0; i < numberOfCommands;i++)
    {
      xmlNode * subNode = NULL;
      if((subNode = FindIthSubNode(commandNode,"Command",i)) != NULL)
	{
	  errors += ReadCommand(subNode);
	}
    }
  if( errors == 0)
    {
      ExecuteCommands();
    }
}


int v1720::ReadCommand(xmlNode * command)
{
  int error = 0;
  //Get the register
  sCommand tempCommand;  
  tempCommand.write = false;
  error += GetXMLValue(command,"Reg","    Command",tempCommand.reg);
  error += GetXMLValue(command,"IO","    Command",tempCommand.io);
  if(boost::iequals(tempCommand.io,"o") || 
     boost::iequals(tempCommand.io,"w") ||
     boost::iequals(tempCommand.io,"out") ||
     boost::iequals(tempCommand.io,"write"))
    {
      tempCommand.write = true;
      error += GetXMLValue(command,"VAL","    Command",tempCommand.val);
    }
  if(error == 0)
    ExtraCommands.push_back(tempCommand);
  return error;
}

void v1720::ExecuteCommands()
{
  std::vector<sCommand>::iterator it;
  for(it = ExtraCommands.begin();
      it != ExtraCommands.end();
      it++)
    {
      //Write command
      if(it->write)
	{
	  //	  a32d32w(it->reg,it->val);
	  SetRegister(it->reg,it->val);
	}
      //Read command
      else
	{
	  //	  a32d32r(it->reg,it->val);	  
	  ReadRegister(it->reg,it->val);
	  printf("V1720 read @ 0x%08X gave 0x%08X\n",it->reg,it->val);
	}
    }
}











int v1720::ChannelInit(xmlNode *channelNode)
{
  int ret = 0;
  //  v1720channel * Channel = &(Channels[Channels.size()-1]);
  v1720channel channel;
  int channelOffset = 0x0;
  //  uint32_t registerReturn = 0;

  //==============================================================================
  //Setup this channel's channel ID
  //==============================================================================  
  ret += GetXMLValue(channelNode,"ChannelID","    Channel",channel.ChannelID);
  if(ret != 0)
    {
      return(ret);
    }
  //Using channel ID set the channel specific VME offset.
  channelOffset = 0x100*channel.ChannelID;

  //==============================================================================
  //Setup this channel's ZS_THRESH
  //==============================================================================  
  ret += GetXMLValue(channelNode,"ZS_THRESH","    Channel",channel.ZS_THRESH);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1024 + channelOffset,channel.ZS_THRESH,"channel.ZS_THRESH");

  //==============================================================================
  //Setup this channel's ZS_NSAMP
  //==============================================================================  
  ret += GetXMLValue(channelNode,"ZS_NSAMP","    Channel",channel.ZS_NSAMP);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1028 + channelOffset,channel.ZS_NSAMP,"channel.ZS_NSAMP");
  
  //==============================================================================
  //Setup this channel's Threshold
  //==============================================================================  
  ret += GetXMLValue(channelNode,"Threshold","    Channel",channel.Threshold);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1080 + channelOffset,channel.Threshold,"channel.Threshold");

  //==============================================================================
  //Setup this channel's NSamples
  //==============================================================================  
  ret += GetXMLValue(channelNode,"NSamples","    Channel",channel.NSamples);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1084 + channelOffset,channel.NSamples,"channel.NSamples");

  //==============================================================================
  //Setup this channel's DAC
  //==============================================================================  
  ret += GetXMLValue(channelNode,"DAC","    Channel",channel.DAC);
  if(ret != 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1098 + channelOffset,channel.DAC,"channel.DAC");

  //==============================================================================
  //Read the AMCFPGA version/date
  //==============================================================================  
  CAEN_DGTZ_BoardInfo_t boardInfo;
  int CAENret = CAEN_DGTZ_GetInfo(Handle,&boardInfo);
  //  int  CAENret = a32d32r(VMEBaseAddress+0x108c +channelOffset,registerReturn);
  if(CAENret == 0)
    {
      xmlNode * newNode = AddNewSubNode(channelNode,"AMCFPGA");
      //char * buffer = new char[100];
      printf("\t\t\tAMCFPGA chan %d firmware: %s\n",
	     channelOffset,
	     boardInfo.AMC_FirmwareRel
	     );
      xmlNodeSetContent(newNode,(xmlChar*)boardInfo.AMC_FirmwareRel);
    }
  ret+=CAENret;
  
  Channels.push_back(channel);

  if(ret!=0)
    {
      printf("Failed in setting channel %u\n",channel.ChannelID);
    }
  return(ret);
}

uint32_t v1720::GetChannelMask()
{
  //Initial mask is all channels off
  uint32_t channelMask = 0x0;
  unsigned int numberOfChannels = Channels.size();
  for(unsigned int iChan = 0;iChan<numberOfChannels;iChan++)
    {
      //Add this channel to the channel mask.
      //Enable channel by setting it's bit (7-0) to one.
      channelMask += (0x1 << Channels[iChan].ChannelID);
    }
  return(channelMask);
}

#endif
