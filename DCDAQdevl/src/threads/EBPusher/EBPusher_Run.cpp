#include <EBPusher.h>

void EBPusher::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(EBAssembler.fdSocket) || IsWriteReady(EBAssembler.fdSocket))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(EBAssembler.fdSocket);
      RemoveWriteFD(EBAssembler.fdSocket);      
      SetupSelect();

      //If the connect check fails
      if(!EBAssembler.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %lds\n",EBAssembler.remoteIP.c_str(),GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  EBAssembler.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      SendStop();
	    }	  
	}
    }

  


  //Check for a message from our DCNetThread
  if((EBAssembler.thread != NULL)  && IsReadReady(EBAssembler.thread->GetMessageOutFDs()[0]))
    {
      ProcessNetThreadMessage();
    }
  if((EBAssembler.thread != NULL) && IsReadReady(FEPC_mem->GetToSendFDs()[0]))
    {      
      ProcessEvent();
    }
}

bool EBPusher::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Remove the FEPCRATManager from select
      RemoveReadFD(FEPC_mem->GetToSendFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the WFD/EV block's free queue's FDs with select if
      //we have a network connection.
      if(EBAssembler.thread != NULL)
	{
	  AddReadFD(FEPC_mem->GetToSendFDs()[0]);      
	}
      else
	{
	  PrintWarning("No network connection! Not listening to the FEPC memory structure!");
	}
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}

void EBPusher::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      float timeStep = difftime(currentTime,lastTime);
      DCMessage::Message message;
      DCMessage::TimedCount Count;
      sprintf(Count.text,"Events read");
      Count.count = timeStepProcessedEvents;
      Count.dt = timeStep;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      timeStepProcessedEvents = 0;

      //Setup next time
      lastTime = currentTime;
    }

  //Catch a timeout if we don't have a DCNetThread
  if((EBAssembler.thread == NULL) && (EBAssembler.fdSocket != -1))
    {
      if(EBAssembler.fdSocket != BadPipeFD)
	{
	  close(EBAssembler.fdSocket);
	  EBAssembler.fdSocket = BadPipeFD;
	}
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(EBAssembler.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }
}

