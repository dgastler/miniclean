#include <FEPCRATManager.h>

bool FEPCRATManager::GetUnSent(int32_t &index)
{
  bool ret = false;
  ret = UnSent.Get(index);
  return ret ;
}

int64_t FEPCRATManager::ReleaseStorageEvent(int32_t &index,
					    uint64_t &password)
{
  int64_t ret = returnDCVectorHash::BAD_MUTEX;
  //Update the storage event
  ret = StorageEvent.UpdateEntry(GetFEPCBlock(index)->eventID,
 				 index,
 				 password);
  if(ret == returnDCVectorHash::OK)
    {
      ret = StorageEvent.ClearPassword(GetFEPCBlock(index)->eventID,
				       password);
    }
  return ret;
}

int64_t FEPCRATManager::GetStorageEvent(int64_t eventID,
					int32_t &index,
					uint64_t &password)
{
  //Get the entry at Event
  return StorageEvent.GetEntry(eventID,index,password);  
}


int64_t FEPCRATManager::ClearStorageEvent(int64_t eventID,
					  uint64_t password)
{
  return StorageEvent.ClearEntry(eventID,password);
}
