#ifdef __RATDS__

#include <FEPCRATManager.h>

bool FEPCRATManager::GetSend(int32_t &index)
{
  bool ret = false;
  ret = Send.Get(index);
  return ret;
}

bool FEPCRATManager::AddFree(int32_t &index)
{
  bool ret = false;
  //Clear the event
  RATEVBlock * ptr = GetFEPCBlock(index);
  std::map<enum TimeStampCode::Code,TH1F*>::iterator it;
  for(it = hTimeStamp.begin(); it != hTimeStamp.end();it++)
    {
      it->second->Fill(ptr->TimeDiff(it->first,ptr->timeStampReference));
    }

  if(ptr)
    ptr->Clear(TimeStampCode::FE_CLEAR);
  ret = Free.Add(index);
  //signal so that if anyone is in cond_wait in Get() they wake up
  return ret;
}
#endif
