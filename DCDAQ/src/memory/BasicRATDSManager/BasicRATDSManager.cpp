#include <BasicRATDSManager.h>

BasicRATDSManager::BasicRATDSManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("BasicRATDSManager"));
  //Make sure our memory vector is empty
  Memory.clear();
}

BasicRATDSManager::~BasicRATDSManager()
{
  Deallocate();
}

void BasicRATDSManager::Deallocate()
{
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //delete DS event (check if it's not NULL)
      if(Memory.back().array)
	{
	  delete [] Memory.back().array; 
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
}

bool BasicRATDSManager::AllocateMemory(xmlNode * setupNode)
{
  bool ret = true;
  uint32_t NumDS;
  int NumDSERR = GetXMLValue(setupNode,"NUMDS","MANAGER",NumDS);
  if(NumDSERR != 0)
    {
      fprintf(stderr,"Error reading NUMDS in BasicRATDSManager.\n");
      ret = false;
    }

  uint32_t NumBlocks;
  int NumBlocksERR = GetXMLValue(setupNode,"NUMBLOCKS","MANAGER",NumBlocks);
  if(NumBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMBLOCKS in BasicRATDSManager.\n");
      ret = false;
    }

  if(ret)
    {
      for(uint32_t iBlock = 0; iBlock < NumBlocks;iBlock++)
	{
	  DSBlock dsBlock;
	  //Allocate a new DS
	  dsBlock.array = new RAT::DS::Root[NumDS];
	  if(dsBlock.array == NULL)
	    {
	      ret = false;
	      dsBlock.allocatedSize = 0;	      
	    }
	  else
	    {
	      dsBlock.allocatedSize = NumDS;
	      dsBlock.usedSize = 0;
	      //Add it's pointer to the Memory vector.
	      Memory.push_back(dsBlock);
	      //Add it's index in the Memory vector to the free DS vector.
	      int32_t index = Memory.size()-1;
	      AddFree(index);
	    }
	}
    }
  return(ret);
}
