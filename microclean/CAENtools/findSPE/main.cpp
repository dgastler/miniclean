//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//Class includes
#include "PEvent.h"
#include "TextArray.h"
#include "xmlParser.h"

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TGraph.h"
#include "TLine.h"

using namespace std;

Float_t Volts = 0;
Float_t Seconds = 0;
Float_t C = 0;

Float_t FitSPE(TH1D * h1)
{
  Float_t SPE = 0;

  Int_t Cut0 = 0;
  Int_t Cut1 = 0;
  for(int i = h1->GetMaximumBin(); i >0 ;i--)
    {
      if(h1->GetBinContent(i) < 0.7*h1->GetMaximum())
	{
	  Cut0 = i;
	  break;
	}
    }
  for(int i = h1->GetMaximumBin(); i < h1->GetNbinsX();i++)
    {
      if(h1->GetBinContent(i) < 0.7*h1->GetMaximum())
	{
	  Cut1 = i;
	  break;
	}
    }
  if(fabs(h1->GetMaximumBin() - Cut0) > fabs(h1->GetMaximumBin() - Cut1))
    {
      Cut0 = 2*h1->GetMaximumBin() - Cut1;
    }
  else
    {
      Cut1 = 2*h1->GetMaximumBin() - Cut0;
    }
  
  
  
  
  TF1 * myfit = new TF1("myfit","pol4");
  h1->Fit(myfit,"F","",
	  h1->GetXaxis()->GetBinCenter(Cut0),
	  h1->GetXaxis()->GetBinCenter(Cut1));
  Double_t SPEmin;
  Double_t SPEmax;
    SPEmin = myfit->GetXmin();
  SPEmax = myfit->GetXmax();
  SPE = myfit->GetMaximumX(SPEmin,SPEmax);
  while(SPE < (SPEmin + 0.05*(SPEmax - SPEmin)))
    {
      SPEmin = SPEmin + 0.05*(SPEmax - SPEmin);
      SPE = myfit->GetMaximumX(SPEmin,SPEmax);
    }
  

}

//Find Single photo electron by sliding through projections of
//the SPE integral vs. SPE height 2d histogram. 
Float_t FindSPE(TH2F * h2,TH1D * &h1,Float_t &HeightCut,char * Name,TH1D * g1,Float_t Voltage)
{
  Int_t Range = 0;

  //Project on to the Y axis to get the SPE pulse height histogram.
  //Set range to serach through to be the point where an exponential fit is down by a factor of 1/e
  TH1D * YProjection = (TH1D*) h2->ProjectionY("YProjection");
  TF1 * Expo = new TF1("Expo",
		       "expo",
		       YProjection->GetYaxis()->GetXmin(),
		       YProjection->GetYaxis()->GetXmax());
  YProjection->Fit(Expo);
  //Bin where the exponential has fallen to 1/e.
  Range = YProjection->GetXaxis()->FindBin(-1.0 * Expo->GetParameter(0)/Expo->GetParameter(1));
  

  Int_t N = Range;
  //holds voltage cut values
  Float_t *x = new Float_t[N];
  //Holds charge maximum values
  Float_t *y = new Float_t[N];
  //holds weighted change of charge max values.
  Float_t *dy = new Float_t[N];
  Float_t yMax = 0;
  Int_t xMax= 0;
  Float_t SPE = 0;

  

  x[0] = 0;
  dy[0] = 0;
  y[0] = 0;
  x[1] = 0;
  dy[1] = 0;
  y[1] = 0;

  g1->SetBinContent(1,0);
  
  // Watch how the maximum peak of the historam shifts.
  // Look for the first big jump in this value. 
  // that should be the SPE peak. 
  for(int i = 1; i < N; i++)
    {
      //Project to charge axis using all bins above the ith voltage bin.
      h1 = h2->ProjectionX(Name,i,h2->GetNbinsY());
      //Get the voltage cut's Volt value.
      x[i] = h2->GetYaxis()->GetBinCenter(i);
      //Get the maximum of resulting histogram
      y[i] = h1->GetMaximumBin();
      //Calculate dy for this point. Weight by how far along we are in i.
      //This helps remove big jumps at much higher voltages due to low statistic high charge hits. 
      if(i -1 == 0)
	dy[i] = 0;
      else
	dy[i] = (y[i] - y[i-1])/(i - 1);
      g1->SetBinContent(i+1,dy[i]);
      delete h1;
      if(yMax < dy[i])
	{
	  yMax = dy[i];
	  xMax = i;
	}
    }

  Float_t Multiple = 0.9;
  if(Voltage < 1300)
    Multiple = 0.5;
  //Find the jump in dy
  for(int i = 2; i < N;i++)
    {
      if(dy[i] >= Multiple* yMax)
	{
	  xMax = i;
	  break;
	}
    }
  
  //make final projection for human viewing. 
  //  h1 = h2->ProjectionX("h1",xMax,h2->GetNbinsY());
  h1 = h2->ProjectionX(Name,xMax,h2->GetNbinsY());

  ///The rest of this function does the actual fit to the charge spectrum and 
  //finds the SPE.

  HeightCut = h2->GetYaxis()->GetBinCenter(xMax);

  SPE = FitSPE(h1);

  g1->Draw("APL");

  //  delete dmyfit;
  //  delete ddmyfit;
  
  delete x;
  delete y;
  delete dy;
  //  delete myfit;

  return(SPE);
}



int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      cout << argv[0] << " inFile" << endl;
      return(0);
    }

  TFile *InFile = new TFile(argv[1],"UPDATE");
  TTree *TextTree = (TTree*) InFile->Get("TextTree");
  TextArray *RootText = 0;
  TextTree->SetBranchAddress("Run Notes",&RootText);
  TextTree->GetEntry(1);
  char * TempPointer = RootText->GetArray();

  XMLNode DAQNode=XMLNode::parseString(TempPointer,"DAQ");

  Int_t CurrentPMT = 0;
  int NameSize = 100;
  char * Name = new char[NameSize];
  //  char * Channel = new char[NameSize];
  //  char * End = new char[NameSize];
  Bool_t End = false;
  do
    {
      //Find voltage.
      //      sprintf(Channel,"%d",CurrentPMT);
      Float_t Voltage = 0;
      for(Int_t WFD =0; WFD < DAQNode.getChildNode("Setup").nChildNode();WFD++)
	{
	  XMLNode WFDNode = DAQNode.getChildNode("Setup").getChildNode("WFD",WFD);
	  for(Int_t Channel = 0; Channel < WFDNode.nChildNode();Channel++)
	    {
	      XMLNode ChannelNode = WFDNode.getChildNode("Channel",Channel);
	      std::string Read(ChannelNode.getAttribute("PMTID"));
	      std::string Nothing("");
	      if((Read != Nothing) && (atoi(ChannelNode.getAttribute("PMTID")) == CurrentPMT))
		{
		  Voltage = atof(ChannelNode.getAttribute("PMTVoltage"));
		}
	    }
	}
      //IF this channel is in the File. 
      //If a pmt is missing then I guess this will stop at that PMT.

      //First check if this is the second pass
      sprintf(Name,"PMT%dSPE_2",CurrentPMT);
      if(!InFile->Get(Name))
	{
	  //This isn't the second pass so set Name to the first pass.
	  sprintf(Name,"PMT%dSPE",CurrentPMT);
	}
      else
	{
	  //second pass, lets do what we need.
	  printf("%s\n",Name);
	  //Get 2-d histogram
	  TH2F * SPE2D = (TH2F *) InFile->Get(Name);
	  sprintf(Name,"%s_SPE_Fit",SPE2D->GetName());
	  TH1D * SPE = SPE2D->ProjectionX(Name);
	  Float_t SPEintegral = FitSPE(SPE);
	  sprintf(Name,"PMT: %03d Voltage: %08fV SPE: %08EC\n",CurrentPMT,Voltage,SPEintegral);
	  //	  InFile->Write();
	  SPE->Write();
	}
      //check if this is the first pass
      if(!InFile->Get(Name))
	{
	  //This isn't the first pass, this channel isn't used.
	  End = true;
	}
      else
	{
	  printf("%s\n",Name);
	  //this is the first pass, 
	  //Get 2-d histogram
	  TH2F * SPE2D = (TH2F *) InFile->Get(Name);
	  //Make the name for the 2-d histograms voltage cut histogram
	  sprintf(Name,"%s_CUTPMT%d",SPE2D->GetName(),CurrentPMT);
	  TH1D * SPECut = new TH1D(Name,Name,40,0,40);
	  //Pointer to the SPE histogram
	  TH1D * SPE = 0;
	  Float_t HeightCut = 0;
	  //Nameof the SPE histogram(sent to FindSPE)
	  sprintf(Name,"%s_SPE_Fit",SPE2D->GetName());
	  Float_t SPEintegral = FindSPE(SPE2D,SPE,HeightCut,Name,SPECut,Voltage);
	  TLine * HeightCutLine = new TLine(SPE2D->GetXaxis()->GetXmin(),HeightCut,SPE2D->GetXaxis()->GetXmax(),HeightCut);
	  HeightCutLine->SetLineColor(kRed);
	  SPE2D->GetListOfFunctions()->AddFirst(HeightCutLine);
	  //	  sprintf(Name,"PMT%dSPE;1",CurrentPMT);
	  sprintf(Name,"%s;1",SPE2D->GetName());
	  InFile->Delete(Name);
	  //	  SPE2D->Write();
	  sprintf(Name,"PMT: %03d Voltage: %08fV SPE: %08EC Height Cut: %fV",CurrentPMT,Voltage,SPEintegral,HeightCut);
	  printf("%s\n",Name);
	  SPE->SetTitle(Name);
	  //	  sprintf(Name,"PMT%dSPE_Fit",CurrentPMT);
	  //	  SPE->Write(Name);
	  SPE2D->Write();
	  SPE->Write();
	  SPECut->Write();
	  CurrentPMT++;
	  //	  InFile->Write();
	}
    }while(!End);
    
  delete [] Name;


  InFile->Close();
  
  return(0);
}
