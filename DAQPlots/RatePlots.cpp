#include "RatePlots.h"

void MakeRatePlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grTotalRate;
  TGraph * grZLEWaveRate;
  TGraph * grPrmpTotRate;
  
  ss << "select time,value from sc_sens_DMRRTRAW where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grTotalRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_DMRRTCZQ where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grZLEWaveRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_DMRRTCPT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grPrmpTotRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);
  
  grTotalRate  ->SetMarkerColor(kGreen);
  grPrmpTotRate->SetMarkerColor(kBlue);
  grZLEWaveRate->SetMarkerColor(kRed);

  grTotalRate  ->SetMarkerStyle(33);
  grPrmpTotRate->SetMarkerStyle(33);
  grZLEWaveRate->SetMarkerStyle(33);

  grTotalRate->GetYaxis()->SetRangeUser(0,2500);
  grTotalRate->Draw("AP");
  grPrmpTotRate->Draw("P");
  grZLEWaveRate->Draw("P");

  TLegend * leg = new TLegend(0.15,0.6,0.55,0.85);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  leg->AddEntry(grTotalRate,"Total rate","P");
  leg->AddEntry(grPrmpTotRate,"Prompt/Total Q rate","P");
  leg->AddEntry(grZLEWaveRate,"ZLE Waveform rate","P");
  leg->SetBorderSize(0);
  leg->Draw("SAME");
  //  c1->Update();
  c1->Print("rate.pdf");
  

  delete grTotalRate;
  delete grPrmpTotRate;
  delete grZLEWaveRate;
  delete leg;
  delete c1;
}

void MakeRatePlotV2(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grTotalRate;
  TGraph * grZLEIRate;
  TGraph * grPTRate;
  TGraph * grZLEWRate;


  ss << "select time,value from sc_sens_DMRRTRAW where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grTotalRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_DMRRTCZI where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grZLEIRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_DMRRTCPT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grPTRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_DMRRTCZQ where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grZLEWRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");


  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);  
  TPad * padRate = new TPad("padRate","Rate",0.0,0.0,1.0,1.0);
  padRate->SetFillColor(kWhite);

  padRate->SetBorderSize(0);
  padRate->SetBorderMode(0);

  padRate->SetRightMargin(padRate->GetRightMargin()*0.3);
  padRate->SetLeftMargin(padRate->GetLeftMargin()*1.1);

  padRate->Draw();

  grTotalRate->SetMarkerStyle(33);
  grTotalRate->SetMarkerColor(kBlack);
  grZLEIRate->SetMarkerStyle(33);
  grZLEIRate->SetMarkerColor(kRed);
  grZLEWRate->SetMarkerStyle(33);
  grZLEWRate->SetMarkerColor(kBlue);
  grPTRate->SetMarkerStyle(33);
  grPTRate->SetMarkerColor(kViolet);

  grTotalRate->SetFillColor(kBlack);
  grZLEIRate ->SetFillColor(kRed);
  grZLEWRate ->SetFillColor(kBlue);
  grPTRate   ->SetFillColor(kViolet);

  grTotalRate->SetLineColor(kBlack);
  grZLEIRate ->SetLineColor(kRed);
  grZLEWRate ->SetLineColor(kBlue);
  grPTRate   ->SetLineColor(kViolet);



  //padRate->cd()->SetLogy();  
  padRate->cd();  

  //  grTotalRate->GetYaxis()->SetRangeUser(-10,2000);
  grTotalRate->GetYaxis()->SetRangeUser(0,2000);
  grTotalRate->GetYaxis()->SetLabelSize(grTotalRate->GetYaxis()->GetLabelSize());
  grTotalRate->GetYaxis()->SetTitleSize(grTotalRate->GetYaxis()->GetTitleSize()*1.3);
  grTotalRate->GetYaxis()->SetTitleOffset(grTotalRate->GetYaxis()->GetTitleOffset());
  grTotalRate->GetYaxis()->SetTitle("Event Rate (hz)");
  grTotalRate->GetXaxis()->SetTitle("Run Time (hours)");
  grTotalRate->Draw("AL");

  grPTRate->Draw("L");
  grZLEWRate->Draw("L");
  grZLEIRate->Draw("L");

  TLegend * leg = new TLegend(0.55,0.3,0.9,0.7);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  leg->SetBorderSize(1);
  leg->SetShadowColor(0);
  leg->SetFillColor(0);
  leg->SetHeader("^{39}Ar reduction level rates");
  leg->AddEntry(grTotalRate,"Total Event Rate","l");
  leg->AddEntry(grPTRate,"Prompt/Total Rdx-level","l");
  leg->AddEntry(grZLEWRate,"ZLE Waveforms Rdx-level","l");
  leg->AddEntry(grZLEIRate,"ZLE Integrals Rdx-level","l");
  leg->Draw("SAME");

  c1->Print("FullRates.pdf");
  

  delete c1;
}

