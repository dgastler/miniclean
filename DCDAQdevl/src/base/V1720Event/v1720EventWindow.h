#ifndef __V1720_EVENT_WINDOW__
#define __V1720_EVENT_WINDOW__

#include <iostream>
#include <stdint.h> //uint32_t
#include <vector>
#include <cstdio>

class v1720EventWindow
{
 public:
  v1720EventWindow();
  ~v1720EventWindow();
  void Clear();

  //Get the ith sample in this window
  uint16_t GetSample(uint32_t i);
  //Get the number of samples in this window
  uint32_t GetSampleCount(); 
  //Get the time offset of this data
  uint32_t GetTimeOffset(){return(timeOffset);}


  //Give people the raw data if they want it.  Gives back pointer and size
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);}; 
  
  
  void Setup(uint32_t * _rawData,uint32_t _size,bool _twoPack, uint32_t _timeOffset);
 private:
  bool twoPack;
  //This class NEVER owns this data!
  //It is either under the control of the v1720Event class
  //or external code.
  uint32_t * rawData;
  uint32_t size;

  //Parsed data
  uint32_t timeOffset;

  //Parsing code
  uint16_t GetSample20pk(uint32_t i);
  uint16_t GetSample25pk(uint32_t i);
};

#endif
