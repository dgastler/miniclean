#include <DCMessageProcessor.h>

bool DCMessageProcessor::TypeRoute(DCMessage::Message & message)
{      
  //Iterator for type lookup.
  std::map<uint16_t,DCMessage::DCAddress>::iterator itMessageTypeToAddressMap;
  
  //Check if the destination still exists
  bool validRoute = false;
  
  //Search for a mapping between type ID and a thread name
  itMessageTypeToAddressMap = messageTypeToAddressMap.find(message.GetType());
  if(itMessageTypeToAddressMap != messageTypeToAddressMap.end())
    {
      validRoute = true;
      //Check if this is a local address and if it is, check that it exists
      if(itMessageTypeToAddressMap->second.IsDefaultAddr())
	{
	  if(FindThread(itMessageTypeToAddressMap->second.GetName()) == NULL)
	    {	  
	      validRoute = false;
	    }
	}
      //Check that this remote address's local route exists
      else
	{
	  if(RemoteRoutesForAddress(itMessageTypeToAddressMap->second).empty())
	    {
	      validRoute =false;
	    }
	}

      //Update the message if the route exists and remove the route if it doesn't
      if(validRoute)
	{
	  //Update the message destination address to the one in the Addr.	 
	  message.SetDestination(itMessageTypeToAddressMap->second);
	}
      else
	{	  
	  //Remove the type of route
	  RemoveThreadTypeRoute(itMessageTypeToAddressMap->second.GetName());

	  DCMessage::Message warning;
	  std::stringstream ss;
	  ss << "Can not find routed to thread (" 
	     << itMessageTypeToAddressMap->second.GetName() <<  ") for "
	     << DCMessage::GetTypeName(message.GetType())
	     <<" type. Removing the route and reprocessing Message";
	  warning.SetSource("DCDAQ");
	  warning.SetDestination();
	  warning.SetType(DCMessage::TEXT);
	  DCMessage::DCString dcString;
	  std::vector<uint8_t> data = dcString.SetData(ss.str());
	  warning.SetData(&(data[0]),data.size());
	  ProcessMessage(warning);
	}
    }
  return validRoute;
}

bool DCMessageProcessor::TypeFunction(DCMessage::Message &message, void (DCMessageProcessor::* &func )(DCMessage::Message &, bool &))
{
  bool foundRoute = false;
  //Iterator for out command lookup.
  std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,bool &)>::iterator itMessageTypeToFunctionMap;
  //Search for the correct command function for this message type.
  itMessageTypeToFunctionMap = messageTypeToFunctionMap.find(message.GetType());
  if(itMessageTypeToFunctionMap != messageTypeToFunctionMap.end())
    {
      //Pass off the message, the thread that sent it and the DCDAQ loop
      //running bool to the command function
      func = itMessageTypeToFunctionMap->second;
      foundRoute = true;
    }
  return foundRoute;   
}

bool DCMessageProcessor::RouteMessageType(DCMessage::Message message)
{
  bool runReturn = true;
  
  //A function pointer
  void (DCMessageProcessor::* func)(DCMessage::Message &, bool &);

  //=================================================================
  //Check for a type route to a thread
  //=================================================================
  if(TypeRoute(message))
    {
      runReturn = ProcessMessage(message);      
    }
  //=================================================================
  //Check for a function mapped from a type
  //=================================================================
  else if(TypeFunction(message,func))
    {      
      (*this.*(func))(message,runReturn);
    }
  //=================================================================
  //We couldn't figure out what to do with this message.
  //=================================================================
  else
    {	  	  
      DCMessage::Message warning;
      std::stringstream ss;
      ss << "Can not find a function for "
	 << DCMessage::GetTypeName(message.GetType())
	 <<" type.";
      warning.SetSource("DCDAQ");
      warning.SetDestination();
      warning.SetType(DCMessage::TEXT);
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(ss.str());
      warning.SetData(&(data[0]),data.size());
      ProcessMessage(warning);
    }
  return runReturn;
}

void DCMessageProcessor::PrintTypeThreadRouteTables()
{
  //Build a streamer to put the list in.
  std::stringstream ss;
  //       1234567890123456789012345678901234567890123456789
  ss << "\n-------------------------------\n";
  ss << "  #  Type              Thread\n";
  ss << "-------------------------------\n";


  std::map<uint16_t,DCMessage::DCAddress>::iterator it;
  for(it = messageTypeToAddressMap.begin(); 
      it != messageTypeToAddressMap.end();
      it++)
    {
      //Get name of type
      std::string type(DCMessage::GetTypeName(DCMessage::DCMessageType(it->first)));
      //Add Type # and name
      ss << " ";
      if(it->first < 10)
	ss << " ";
      ss << it->first << "  " << type << std::string(24-type.size()-7,' ');
      //Add the local thread handling this route
      ss << it->second.GetStr() << std::endl;
    }
  ss << "-------------------------------\n";
  //Print
  DCMessage::Message message;
  message.SetDestination();
  message.SetType(DCMessage::TEXT);
  DCMessage::DCString dcString;
  std::vector<uint8_t> data = dcString.SetData(ss.str());
  message.SetData(&(data[0]),data.size());
  ProcessMessage(message);
}

void DCMessageProcessor::PrintTypeFunctionRouteTables()
{
  //Build a streamer to put the list in.
  std::stringstream ss;
  //       1234567890123456789012345678901234567890123456789
  ss << "\n-----------------------------------\n";
  ss << "  #  Type              Function\n";
  ss << "-----------------------------------\n";

  std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,bool &)>::iterator it;
  for(it = messageTypeToFunctionMap.begin(); 
      it != messageTypeToFunctionMap.end();
      it++)
    {
      //Get name of type
      std::string type(DCMessage::GetTypeName(DCMessage::DCMessageType(it->first)));
      //Add Type # and name
      ss << " ";
      if(it->first < 10)
	ss << " ";
      ss << it->first << "  " << type << std::string(24-type.size()-6,' ');
      //Add the local thread handling this route
      char buffer[201]; buffer[sizeof(buffer)-1] = NULL;
      ss << GetFunctionName(buffer,sizeof(buffer)-1,it->second) << std::endl;
    }
  ss << "-----------------------------------\n";
  //Print
  DCMessage::Message message;
  message.SetDestination();
  message.SetType(DCMessage::TEXT);
  DCMessage::DCString dcString;
  std::vector<uint8_t> data = dcString.SetData(ss.str());
  message.SetData(&(data[0]),data.size());
  ProcessMessage(message);
}
