#ifndef __CONNECTION__
#define __CONNECTION__

#include <string>
#include <DCNetThread.h>
#include <NetworkStatus.h>

//Structure to hold connection info
class Connection
{ 
 public:
  Connection();
  ~Connection();
  void Clear();
  static const int badFD;

  bool RouteToLocal(DCMessage::DCAddress const & addr);
  bool RouteToRemote(DCMessage::DCAddress const & addr);

  //========================================
  //Connection client side functions
  //========================================
  //Start trying to connect to a remote server
  int GetSocketFD(){return fdSocket;};
  bool SetupRemoteAddr(xmlNode * SetupNode);
  bool AsyncConnectStart(int type = SOCK_STREAM,int protocol = 0);
  //Check if the remote server attempt worked. 
  bool AsyncConnectCheck();
  bool SetupDCNetThread(xmlNode * managerNode);

  //========================================
  //Connection server side functions
  //========================================
  bool SetupDCNetThread(int & _fd,
  			DCMessage::DCAddress const & addr,
  			uint32_t _port,xmlNode * managerNode);

  //SetRetry() and Retry() are used to wait a DCThread timeout
  //before trying another connection.  The countdown is 2 to 
  //account for the checking of ProcessTimeout() after calling
  //MainLoop() in DCThread
  void SetRetry(int countdown = 2);
  bool Retry();
  //Stops and deletes netthread.  Closes network socket.
  void Shutdown();
  bool IsRunning(){if(thread != NULL){return thread->IsRunning();}return false;};
  
  //DCNetThread interface
  int GetFullPacketFD();
  int GetMessageFD();
  
  DCNetPacketManager * GetInPacketManager();
  DCNetPacketManager * GetOutPacketManager();

  DCMessage::Message GetMessageOut();

  DCMessage::DCAddress GetRemoteAddress();
  DCMessage::DCAddress GetLocalAddress();
  void PrintStatus(){if(thread!=NULL)thread->PrintStatus();}

  
 private:

  uint32_t              port;

  //Cast as what you need ie struct sockaddr_in, struct sockaddr_in6, struct sockaddr_un based on SocketFamily
  DCMessage::DCAddress  remoteAddr;
  int                   fdSocket;
  DCNetThread *         thread;
  
  int                   retryCountDown;
  bool                  retry;

  void PrintError(const char * str){fprintf(stderr,"Connection error: %s\n",str);};
  void PrintWarning(const char * str){printf("Connection warning: %s\n",str);};
  void DestructiveCopy(Connection & obj);
  //Do not impliment these.
  Connection(Connection & obj);
  Connection & operator=(Connection & rhs);
};

#endif
