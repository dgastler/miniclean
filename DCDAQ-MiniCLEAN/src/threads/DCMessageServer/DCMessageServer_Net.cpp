#include <DCMessageServer.h>

bool DCMessageServer::Setup(xmlNode * SetupNode,
			    std::vector<MemoryManager*> &/*MemManager*/)
{  
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  //================================
  //Parse data for server config and expected clients.
  //================================
  if(server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.GetSocketFD());
      SetupSelect();
      Ready=true;
    }
  else
    {
      return false;
    }
  return true;
}



bool DCMessageServer::ProcessNewConnection()
{
  //create a DCNetThread for this 
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().GetFullPacketFD());
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().GetMessageFD());            
      printf("DCMessage connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().GetRemoteAddress().GetAddrStr().c_str());
      SetupSelect();

      //================================================
      //Register this connection with DCMessageProcessor
      //================================================

      //Data structure that holds the routing information
      DCMessage::DCRoute routeData;
      DCMessage::DCAddress dcAddr = server.Back().GetRemoteAddress();
      if(dcAddr.GetAddr()->sa_family == AF_LOCAL)
	{
	  dcAddr=server.GetListenAddr();
	}

      std::vector<uint8_t> data = routeData.SetData(dcAddr,
						    GetName());	
      //message of REGISTER_ADDR type to tell the DCMessageProcessor to route
      //messages with this address to this thread
      DCMessage::Message message;
      message.SetType(DCMessage::REGISTER_ADDR);
      message.SetDestination();
      message.SetData(&data[0],data.size());
      SendMessageOut(message);


    }
 else
   {
     return false;
   }
  return true;
}

bool DCMessageServer::SendMessage(size_t iConn,DCMessage::Message &message)
{
  //Get the data for this packet
  std::vector<uint8_t> stream;
  //get an vector of data for this message to send.
  uint streamSize = message.StreamMessage(stream);
  if(streamSize < 0)
    return(false);
  uint8_t * dataPtr  = &(stream[0]);	  
  int packetNumber = 0;
  //Send loop over the data until all of it is sent
  while(streamSize >0)
    {
      //Get a free packet index
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      server[iConn].GetOutPacketManager()->GetFree(iDCPacket,true);
      //Get the DCNetPacket for that index
      DCNetPacket * packet = server[iConn].GetOutPacketManager()->GetPacket(iDCPacket);
      //Fail if our packet is bad
      if(packet == NULL)
	{
	  return(false);
	}
      //Fill packet
      if(streamSize > packet->GetMaxDataSize())
	{
	  packet->SetType(DCNetPacket::DCMESSAGE_PARTIAL);
	  packet->SetData(dataPtr,packet->GetMaxDataSize(),packetNumber);
	  dataPtr+=packet->GetMaxDataSize();
	  streamSize-=packet->GetMaxDataSize();
	  packetNumber++;
	}
      else
	{
	  packet->SetType(DCNetPacket::DCMESSAGE);
	  packet->SetData(dataPtr,streamSize,packetNumber);
	  dataPtr+=streamSize;
	  streamSize-=streamSize;
	  packetNumber++;
	}
      //Send off packet
      server[iConn].GetOutPacketManager()->AddFull(iDCPacket);
      packet = NULL;
    }
  return true ;
}

void DCMessageServer::ProcessChildMessage(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
  DCMessage::Message message = server[iConn].GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      DeleteConnection(iConn);

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::DISCONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      DCMessage::Message shutdownMessage;      
      connectMessage.SetType(DCMessage::STOP);
      connectMessage.SetDestination("DCDAQ");
      SendMessageOut(connectMessage);

      //      SendStop(false);
      Loop=false;
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  else if(message.GetType() == DCMessage::NETWORK)
    {
      ForwardMessageOut(message);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      //SendMessageOut(message,true);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}

void DCMessageServer::DeleteConnection(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
 
  //Remove the incoming packet manager fds from list
  RemoveReadFD(server[iConn].GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(server[iConn].GetMessageFD());          

  SetupSelect();
  server.DeleteConnection(iConn);
}

DCMessage::Message DCMessageServer::ReadRemoteDCMessage(size_t iConn)
{
  DCMessage::Message blankMessage;
  std::vector<uint8_t> messageData;
  int packetCount = 1;
  //Loop over all the packets that make up this DCMessage
  for(int iPacket = 0; iPacket < packetCount; iPacket++)
    {
      int32_t iDCPacket = FreeFullQueue::BADVALUE;    
      //Try to get a full in packet index
      server[iConn].GetInPacketManager()->GetFull(iDCPacket,true);
      //Get the associated packet
      DCNetPacket * packet = server[iConn].GetInPacketManager()->GetPacket(iDCPacket);
      //Fail if the packet is bad
      if(packet == NULL)
	{
	  PrintWarning("Bad net packet.\n");
	  return blankMessage;
	}
     
      //Now we have a valid packet
      
      //Check it's type (if it is a partial packet increase packetCount;
      if(packet->GetType() == DCNetPacket::DCMESSAGE_PARTIAL)
	{
	  packetCount++;
	}
      else if(packet->GetType() != DCNetPacket::DCMESSAGE)
	{	  
	  PrintWarning("Bad incoming packet\n");
	  //return packet as free and return
	  server[iConn].GetInPacketManager()->AddFree(iDCPacket);
	  return blankMessage;
	}
      
      //Now get the data from the packet and append it to the message data vector
      unsigned int existingMessageDataSize = messageData.size();
      //Resize our vector to hold the new data
      messageData.resize(existingMessageDataSize + packet->GetSize());
      //Check that the packet has a valid data pointer
      if(packet->GetData() == NULL)
	{
	  PrintWarning("Bad packet data\n");
	  //return packet as free and return
	  server[iConn].GetInPacketManager()->AddFree(iDCPacket);
	  return blankMessage;
	}
      //Copy in the new data
      memcpy(&messageData[existingMessageDataSize],packet->GetData(),packet->GetSize());
      //return our packet
      server[iConn].GetInPacketManager()->AddFree(iDCPacket);
    }
  DCMessage::Message message;
  message.SetMessage(&messageData[0],messageData.size());
  return message;
}
