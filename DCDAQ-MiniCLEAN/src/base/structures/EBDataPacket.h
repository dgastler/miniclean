#ifndef __EBDATAPACKET__
#define __EBDATAPACKET__
#include <RAT/DS/ReductionLevel.hh>

namespace EBPacket
{
  struct Event
  {
    //From trigger box
    uint32_t eventID;
    uint64_t eventTime;
    uint32_t eventTriggerCounts;
    uint16_t eventTriggerPattern;

    uint8_t  dataType;    //Reductionlevel definitions
    uint8_t  boardCount;   //if zero, then we have a summary event with two integral blocks
    
  };

  struct Board
  {
    uint32_t header[4];
    uint8_t  channelPattern;
  };
  struct Channel
  {
    uint8_t dataBlockCount;
  }; 

  struct PromptTotalBlock
  {
    //Prompt integral (ADC sum)
    float prompt;
    //total integral (ADC sum)
    float total;
  };

  struct IntegralBlock
  {
    float integral;
    uint16_t startTime;  //clock ticks
    uint8_t width;      //clock ticks
  };  
  struct WaveformBlock
  {
    uint32_t startTime; //clock ticks
    uint32_t dataBytes; //bytes of data after this struct that contains waveform
                        //data
  };
}
#endif
