#ifndef __THREADEND__
#define __THREADEND__

#include <pthread.h>
#include <DCThread.h>

void  end_thread(void * _pointer);

#endif
