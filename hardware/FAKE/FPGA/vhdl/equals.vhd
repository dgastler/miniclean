library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity equals is Port( 
	A : in  STD_LOGIC_VECTOR(6 downto 0);
	B : in  STD_LOGIC_VECTOR(6 downto 0);
	O : out  STD_LOGIC);
end equals;

architecture Behavioral of equals is
begin
	O <= '1' when A = B else '0';
end Behavioral;
