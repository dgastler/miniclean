#include <RATParse.h>

void RATParse::ProcessOneEvent(v1720Event & V1720Event,RAT::DS::Root * DS)
{
  //Make sure there is an ev structure and if there isn't allocated one.
  RAT::DS::EV * RATEvent;
  if(DS->GetEVCount() == 0)
    {
      DS->AddNewEV();
    }
  RATEvent = DS->GetEV(0);
  //Set IDs
  DS->SetRunID(GetRunNumber());
  //Event ID set by writer thread
  
  //Loop over the WFD channels.
  unsigned int numberOfChannels = V1720Event.GetChannelCount();
  for(unsigned int iChannel = 0; iChannel < numberOfChannels;iChannel++)
    {
      //Allocate a new PMT if there aren't enough
      if(iChannel >= (unsigned int) RATEvent->GetPMTCount())
	{
	  RATEvent->AddNewPMT();
	}
      //RAT::DS::PMT.
      RAT::DS::PMT * RATPMT = RATEvent->GetPMT(iChannel);
      //Get the parsed channel structure.
      v1720EventChannel currentParsedChannel = V1720Event.GetChannel(iChannel);
      RATPMT->SetID(currentParsedChannel.GetChannelID());
      RATPMT->SetPMTType(1);
      //Get this PMT's raw event.
      //RAT::DS::Raw * RATRaw = RATPMT->AddRaw();
      RAT::DS::Raw * RATRaw = RATPMT->GetRaw();

      //Set the window header.
      WindowHeaderVector.clear(); //clears, but does not deallocate
      //Set the header for this PMT
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(0));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(1));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(2));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(3));
      RATRaw->SetHeader(WindowHeaderVector);

      //Clear the raw waveforms already in this PMT.
      //      RATRaw->ClearRaw();      
      //Loop over the zero suppressed windows.
      unsigned int numberOfWindows = currentParsedChannel.GetWindowCount();
      for(unsigned int iWindow = 0; iWindow < numberOfWindows;iWindow++)
	{
	  RAT::DS::RawWaveform * RATRawWaveform = NULL;
	  //	  RATRawWaveform = RATRaw->AddNewRawWaveform();
	  if(iWindow >= ((unsigned int) RATRaw->GetRawWaveformCount()))
	    RATRawWaveform = RATRaw->AddNewRawWaveform();
	  else
	    RATRawWaveform = RATRaw->GetRawWaveform(iWindow);
	  
	  //Get the current parsed window in this channel
	  v1720EventWindow currentParsedWindow = currentParsedChannel.GetWindow(iWindow);

	  //For each window show the window's time offset to the event time stamp
	  RATRawWaveform->SetTimeOffset(currentParsedWindow.GetTimeOffset());
	  //Set the data samples for this window
	  WindowDataVector.clear();//clears, but does not deallocate
	  uint32_t windowSize = currentParsedWindow.GetSampleCount();
	  //Unpack the samples from this window and enter them in to the RAT window
	  for(uint32_t iSample = 0;iSample < windowSize;iSample++)
	    {
	      WindowDataVector.push_back(currentParsedWindow.GetSample(iSample));
	    }
	  RATRawWaveform->SetSamples(WindowDataVector);
	}//Window/RAW loop
    }//Channel loop
}
