--------------------------------------------------------------------------------
-- Copyright (c) 1995-2008 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 10.1.03
--  \   \         Application : sch2vhdl
--  /   /         Filename : pport.vhf
-- /___/   /\     Timestamp : 12/01/2009 16:00:42
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: /opt/Xilinx/10.1/ISE/bin/lin/unwrapped/sch2vhdl -intstyle ise -family spartan2e -flat -suppress -w /home/clean/vhdl/pport/pport.sch pport.vhf
--Design Name: pport
--Device: spartan2e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OBUF8_MXILINX_pport is
   port ( I : in    std_logic_vector (7 downto 0); 
          O : out   std_logic_vector (7 downto 0));
end OBUF8_MXILINX_pport;

architecture BEHAVIORAL of OBUF8_MXILINX_pport is
   attribute BOX_TYPE   : string ;
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
begin
   I_36_30 : OBUF
      port map (I=>I(0),
                O=>O(0));
   
   I_36_31 : OBUF
      port map (I=>I(1),
                O=>O(1));
   
   I_36_32 : OBUF
      port map (I=>I(2),
                O=>O(2));
   
   I_36_33 : OBUF
      port map (I=>I(3),
                O=>O(3));
   
   I_36_34 : OBUF
      port map (I=>I(7),
                O=>O(7));
   
   I_36_35 : OBUF
      port map (I=>I(6),
                O=>O(6));
   
   I_36_36 : OBUF
      port map (I=>I(5),
                O=>O(5));
   
   I_36_37 : OBUF
      port map (I=>I(4),
                O=>O(4));
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FTCE_MXILINX_pport is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          T   : in    std_logic; 
          Q   : out   std_logic);
end FTCE_MXILINX_pport;

architecture BEHAVIORAL of FTCE_MXILINX_pport is
   attribute BOX_TYPE   : string ;
   attribute INIT       : string ;
   attribute RLOC       : string ;
   signal TQ      : std_logic;
   signal Q_DUMMY : std_logic;
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component FDCE
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute INIT of FDCE : component is "0";
   attribute BOX_TYPE of FDCE : component is "BLACK_BOX";
   
   attribute RLOC of I_36_35 : label is "R0C0.S0";
begin
   Q <= Q_DUMMY;
   I_36_32 : XOR2
      port map (I0=>T,
                I1=>Q_DUMMY,
                O=>TQ);
   
   I_36_35 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ,
                Q=>Q_DUMMY);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity CB2CE_MXILINX_pport is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          CEO : out   std_logic; 
          Q0  : out   std_logic; 
          Q1  : out   std_logic; 
          TC  : out   std_logic);
end CB2CE_MXILINX_pport;

architecture BEHAVIORAL of CB2CE_MXILINX_pport is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_1   : std_logic;
   signal Q0_DUMMY : std_logic;
   signal Q1_DUMMY : std_logic;
   signal TC_DUMMY : std_logic;
   component FTCE_MXILINX_pport
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             T   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   attribute HU_SET of I_Q0 : label is "I_Q0_0";
   attribute HU_SET of I_Q1 : label is "I_Q1_1";
begin
   Q0 <= Q0_DUMMY;
   Q1 <= Q1_DUMMY;
   TC <= TC_DUMMY;
   I_Q0 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>XLXN_1,
                Q=>Q0_DUMMY);
   
   I_Q1 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>Q0_DUMMY,
                Q=>Q1_DUMMY);
   
   I_36_37 : AND2
      port map (I0=>Q1_DUMMY,
                I1=>Q0_DUMMY,
                O=>TC_DUMMY);
   
   I_36_47 : VCC
      port map (P=>XLXN_1);
   
   I_36_52 : AND2
      port map (I0=>CE,
                I1=>TC_DUMMY,
                O=>CEO);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity CB16CE_MXILINX_pport is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          CEO : out   std_logic; 
          Q   : out   std_logic_vector (15 downto 0); 
          TC  : out   std_logic);
end CB16CE_MXILINX_pport;

architecture BEHAVIORAL of CB16CE_MXILINX_pport is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal T2       : std_logic;
   signal T3       : std_logic;
   signal T4       : std_logic;
   signal T5       : std_logic;
   signal T6       : std_logic;
   signal T7       : std_logic;
   signal T8       : std_logic;
   signal T9       : std_logic;
   signal T10      : std_logic;
   signal T11      : std_logic;
   signal T12      : std_logic;
   signal T13      : std_logic;
   signal T14      : std_logic;
   signal T15      : std_logic;
   signal XLXN_1   : std_logic;
   signal Q_DUMMY  : std_logic_vector (15 downto 0);
   signal TC_DUMMY : std_logic;
   component FTCE_MXILINX_pport
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             T   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component AND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4 : component is "BLACK_BOX";
   
   component AND5
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND5 : component is "BLACK_BOX";
   
   attribute HU_SET of I_Q0 : label is "I_Q0_3";
   attribute HU_SET of I_Q1 : label is "I_Q1_2";
   attribute HU_SET of I_Q2 : label is "I_Q2_5";
   attribute HU_SET of I_Q3 : label is "I_Q3_4";
   attribute HU_SET of I_Q4 : label is "I_Q4_9";
   attribute HU_SET of I_Q5 : label is "I_Q5_8";
   attribute HU_SET of I_Q6 : label is "I_Q6_7";
   attribute HU_SET of I_Q7 : label is "I_Q7_6";
   attribute HU_SET of I_Q8 : label is "I_Q8_10";
   attribute HU_SET of I_Q9 : label is "I_Q9_11";
   attribute HU_SET of I_Q10 : label is "I_Q10_12";
   attribute HU_SET of I_Q11 : label is "I_Q11_13";
   attribute HU_SET of I_Q12 : label is "I_Q12_14";
   attribute HU_SET of I_Q13 : label is "I_Q13_15";
   attribute HU_SET of I_Q14 : label is "I_Q14_16";
   attribute HU_SET of I_Q15 : label is "I_Q15_17";
begin
   Q(15 downto 0) <= Q_DUMMY(15 downto 0);
   TC <= TC_DUMMY;
   I_Q0 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>XLXN_1,
                Q=>Q_DUMMY(0));
   
   I_Q1 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>Q_DUMMY(0),
                Q=>Q_DUMMY(1));
   
   I_Q2 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T2,
                Q=>Q_DUMMY(2));
   
   I_Q3 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T3,
                Q=>Q_DUMMY(3));
   
   I_Q4 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T4,
                Q=>Q_DUMMY(4));
   
   I_Q5 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T5,
                Q=>Q_DUMMY(5));
   
   I_Q6 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T6,
                Q=>Q_DUMMY(6));
   
   I_Q7 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T7,
                Q=>Q_DUMMY(7));
   
   I_Q8 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T8,
                Q=>Q_DUMMY(8));
   
   I_Q9 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T9,
                Q=>Q_DUMMY(9));
   
   I_Q10 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T10,
                Q=>Q_DUMMY(10));
   
   I_Q11 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T11,
                Q=>Q_DUMMY(11));
   
   I_Q12 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T12,
                Q=>Q_DUMMY(12));
   
   I_Q13 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T13,
                Q=>Q_DUMMY(13));
   
   I_Q14 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T14,
                Q=>Q_DUMMY(14));
   
   I_Q15 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T15,
                Q=>Q_DUMMY(15));
   
   I_36_3 : AND3
      port map (I0=>Q_DUMMY(2),
                I1=>Q_DUMMY(1),
                I2=>Q_DUMMY(0),
                O=>T3);
   
   I_36_4 : AND2
      port map (I0=>Q_DUMMY(1),
                I1=>Q_DUMMY(0),
                O=>T2);
   
   I_36_9 : VCC
      port map (P=>XLXN_1);
   
   I_36_10 : AND4
      port map (I0=>Q_DUMMY(3),
                I1=>Q_DUMMY(2),
                I2=>Q_DUMMY(1),
                I3=>Q_DUMMY(0),
                O=>T4);
   
   I_36_14 : AND5
      port map (I0=>Q_DUMMY(7),
                I1=>Q_DUMMY(6),
                I2=>Q_DUMMY(5),
                I3=>Q_DUMMY(4),
                I4=>T4,
                O=>T8);
   
   I_36_15 : AND2
      port map (I0=>Q_DUMMY(4),
                I1=>T4,
                O=>T5);
   
   I_36_19 : AND3
      port map (I0=>Q_DUMMY(5),
                I1=>Q_DUMMY(4),
                I2=>T4,
                O=>T6);
   
   I_36_21 : AND4
      port map (I0=>Q_DUMMY(6),
                I1=>Q_DUMMY(5),
                I2=>Q_DUMMY(4),
                I3=>T4,
                O=>T7);
   
   I_36_22 : AND5
      port map (I0=>Q_DUMMY(15),
                I1=>Q_DUMMY(14),
                I2=>Q_DUMMY(13),
                I3=>Q_DUMMY(12),
                I4=>T12,
                O=>TC_DUMMY);
   
   I_36_23 : AND2
      port map (I0=>Q_DUMMY(12),
                I1=>T12,
                O=>T13);
   
   I_36_24 : AND3
      port map (I0=>Q_DUMMY(13),
                I1=>Q_DUMMY(12),
                I2=>T12,
                O=>T14);
   
   I_36_25 : AND4
      port map (I0=>Q_DUMMY(14),
                I1=>Q_DUMMY(13),
                I2=>Q_DUMMY(12),
                I3=>T12,
                O=>T15);
   
   I_36_26 : AND4
      port map (I0=>Q_DUMMY(10),
                I1=>Q_DUMMY(9),
                I2=>Q_DUMMY(8),
                I3=>T8,
                O=>T11);
   
   I_36_27 : AND3
      port map (I0=>Q_DUMMY(9),
                I1=>Q_DUMMY(8),
                I2=>T8,
                O=>T10);
   
   I_36_28 : AND2
      port map (I0=>Q_DUMMY(8),
                I1=>T8,
                O=>T9);
   
   I_36_29 : AND5
      port map (I0=>Q_DUMMY(11),
                I1=>Q_DUMMY(10),
                I2=>Q_DUMMY(9),
                I3=>Q_DUMMY(8),
                I4=>T8,
                O=>T12);
   
   I_36_54 : AND2
      port map (I0=>CE,
                I1=>TC_DUMMY,
                O=>CEO);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ADD16_MXILINX_pport is
   port ( A   : in    std_logic_vector (15 downto 0); 
          B   : in    std_logic_vector (15 downto 0); 
          CI  : in    std_logic; 
          CO  : out   std_logic; 
          OFL : out   std_logic; 
          S   : out   std_logic_vector (15 downto 0));
end ADD16_MXILINX_pport;

architecture BEHAVIORAL of ADD16_MXILINX_pport is
   attribute BOX_TYPE   : string ;
   attribute RLOC       : string ;
   signal C0       : std_logic;
   signal C1       : std_logic;
   signal C2       : std_logic;
   signal C3       : std_logic;
   signal C4       : std_logic;
   signal C5       : std_logic;
   signal C6       : std_logic;
   signal C7       : std_logic;
   signal C8       : std_logic;
   signal C9       : std_logic;
   signal C10      : std_logic;
   signal C11      : std_logic;
   signal C12      : std_logic;
   signal C13      : std_logic;
   signal C14      : std_logic;
   signal C14O     : std_logic;
   signal dummy    : std_logic;
   signal I0       : std_logic;
   signal I1       : std_logic;
   signal I2       : std_logic;
   signal I3       : std_logic;
   signal I4       : std_logic;
   signal I5       : std_logic;
   signal I6       : std_logic;
   signal I7       : std_logic;
   signal I8       : std_logic;
   signal I9       : std_logic;
   signal I10      : std_logic;
   signal I11      : std_logic;
   signal I12      : std_logic;
   signal I13      : std_logic;
   signal I14      : std_logic;
   signal I15      : std_logic;
   signal CO_DUMMY : std_logic;
   component FMAP
      port ( I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : in    std_logic);
   end component;
   attribute BOX_TYPE of FMAP : component is "BLACK_BOX";
   
   component MUXCY_L
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_L : component is "BLACK_BOX";
   
   component MUXCY
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY : component is "BLACK_BOX";
   
   component XORCY
      port ( CI : in    std_logic; 
             LI : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XORCY : component is "BLACK_BOX";
   
   component MUXCY_D
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_D : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   attribute RLOC of I_36_16 : label is "R3C0.S1";
   attribute RLOC of I_36_17 : label is "R3C0.S1";
   attribute RLOC of I_36_18 : label is "R2C0.S1";
   attribute RLOC of I_36_19 : label is "R2C0.S1";
   attribute RLOC of I_36_20 : label is "R1C0.S1";
   attribute RLOC of I_36_21 : label is "R1C0.S1";
   attribute RLOC of I_36_22 : label is "R0C0.S1";
   attribute RLOC of I_36_23 : label is "R0C0.S1";
   attribute RLOC of I_36_55 : label is "R3C0.S1";
   attribute RLOC of I_36_58 : label is "R2C0.S1";
   attribute RLOC of I_36_62 : label is "R2C0.S1";
   attribute RLOC of I_36_63 : label is "R1C0.S1";
   attribute RLOC of I_36_64 : label is "R0C0.S1";
   attribute RLOC of I_36_107 : label is "R0C0.S1";
   attribute RLOC of I_36_110 : label is "R1C0.S1";
   attribute RLOC of I_36_111 : label is "R3C0.S1";
   attribute RLOC of I_36_248 : label is "R4C0.S1";
   attribute RLOC of I_36_249 : label is "R4C0.S1";
   attribute RLOC of I_36_250 : label is "R5C0.S1";
   attribute RLOC of I_36_251 : label is "R5C0.S1";
   attribute RLOC of I_36_252 : label is "R6C0.S1";
   attribute RLOC of I_36_253 : label is "R6C0.S1";
   attribute RLOC of I_36_254 : label is "R7C0.S1";
   attribute RLOC of I_36_255 : label is "R7C0.S1";
   attribute RLOC of I_36_272 : label is "R7C0.S1";
   attribute RLOC of I_36_275 : label is "R7C0.S1";
   attribute RLOC of I_36_279 : label is "R6C0.S1";
   attribute RLOC of I_36_283 : label is "R6C0.S1";
   attribute RLOC of I_36_287 : label is "R5C0.S1";
   attribute RLOC of I_36_291 : label is "R5C0.S1";
   attribute RLOC of I_36_295 : label is "R4C0.S1";
   attribute RLOC of I_36_299 : label is "R4C0.S1";
begin
   CO <= CO_DUMMY;
   I_36_16 : FMAP
      port map (I1=>A(8),
                I2=>B(8),
                I3=>dummy,
                I4=>dummy,
                O=>I8);
   
   I_36_17 : FMAP
      port map (I1=>A(9),
                I2=>B(9),
                I3=>dummy,
                I4=>dummy,
                O=>I9);
   
   I_36_18 : FMAP
      port map (I1=>A(10),
                I2=>B(10),
                I3=>dummy,
                I4=>dummy,
                O=>I10);
   
   I_36_19 : FMAP
      port map (I1=>A(11),
                I2=>B(11),
                I3=>dummy,
                I4=>dummy,
                O=>I11);
   
   I_36_20 : FMAP
      port map (I1=>A(12),
                I2=>B(12),
                I3=>dummy,
                I4=>dummy,
                O=>I12);
   
   I_36_21 : FMAP
      port map (I1=>A(13),
                I2=>B(13),
                I3=>dummy,
                I4=>dummy,
                O=>I13);
   
   I_36_22 : FMAP
      port map (I1=>A(14),
                I2=>B(14),
                I3=>dummy,
                I4=>dummy,
                O=>I14);
   
   I_36_23 : FMAP
      port map (I1=>A(15),
                I2=>B(15),
                I3=>dummy,
                I4=>dummy,
                O=>I15);
   
   I_36_55 : MUXCY_L
      port map (CI=>C8,
                DI=>A(9),
                S=>I9,
                LO=>C9);
   
   I_36_58 : MUXCY_L
      port map (CI=>C10,
                DI=>A(11),
                S=>I11,
                LO=>C11);
   
   I_36_62 : MUXCY_L
      port map (CI=>C9,
                DI=>A(10),
                S=>I10,
                LO=>C10);
   
   I_36_63 : MUXCY_L
      port map (CI=>C11,
                DI=>A(12),
                S=>I12,
                LO=>C12);
   
   I_36_64 : MUXCY
      port map (CI=>C14,
                DI=>A(15),
                S=>I15,
                O=>CO_DUMMY);
   
   I_36_73 : XORCY
      port map (CI=>C7,
                LI=>I8,
                O=>S(8));
   
   I_36_74 : XORCY
      port map (CI=>C8,
                LI=>I9,
                O=>S(9));
   
   I_36_75 : XORCY
      port map (CI=>C10,
                LI=>I11,
                O=>S(11));
   
   I_36_76 : XORCY
      port map (CI=>C9,
                LI=>I10,
                O=>S(10));
   
   I_36_77 : XORCY
      port map (CI=>C12,
                LI=>I13,
                O=>S(13));
   
   I_36_78 : XORCY
      port map (CI=>C11,
                LI=>I12,
                O=>S(12));
   
   I_36_80 : XORCY
      port map (CI=>C14,
                LI=>I15,
                O=>S(15));
   
   I_36_81 : XORCY
      port map (CI=>C13,
                LI=>I14,
                O=>S(14));
   
   I_36_107 : MUXCY_D
      port map (CI=>C13,
                DI=>A(14),
                S=>I14,
                LO=>C14,
                O=>C14O);
   
   I_36_110 : MUXCY_L
      port map (CI=>C12,
                DI=>A(13),
                S=>I13,
                LO=>C13);
   
   I_36_111 : MUXCY_L
      port map (CI=>C7,
                DI=>A(8),
                S=>I8,
                LO=>C8);
   
   I_36_226 : XORCY
      port map (CI=>CI,
                LI=>I0,
                O=>S(0));
   
   I_36_227 : XORCY
      port map (CI=>C0,
                LI=>I1,
                O=>S(1));
   
   I_36_228 : XORCY
      port map (CI=>C2,
                LI=>I3,
                O=>S(3));
   
   I_36_229 : XORCY
      port map (CI=>C1,
                LI=>I2,
                O=>S(2));
   
   I_36_230 : XORCY
      port map (CI=>C4,
                LI=>I5,
                O=>S(5));
   
   I_36_231 : XORCY
      port map (CI=>C3,
                LI=>I4,
                O=>S(4));
   
   I_36_233 : XORCY
      port map (CI=>C6,
                LI=>I7,
                O=>S(7));
   
   I_36_234 : XORCY
      port map (CI=>C5,
                LI=>I6,
                O=>S(6));
   
   I_36_248 : MUXCY_L
      port map (CI=>C6,
                DI=>A(7),
                S=>I7,
                LO=>C7);
   
   I_36_249 : MUXCY_L
      port map (CI=>C5,
                DI=>A(6),
                S=>I6,
                LO=>C6);
   
   I_36_250 : MUXCY_L
      port map (CI=>C4,
                DI=>A(5),
                S=>I5,
                LO=>C5);
   
   I_36_251 : MUXCY_L
      port map (CI=>C3,
                DI=>A(4),
                S=>I4,
                LO=>C4);
   
   I_36_252 : MUXCY_L
      port map (CI=>C2,
                DI=>A(3),
                S=>I3,
                LO=>C3);
   
   I_36_253 : MUXCY_L
      port map (CI=>C1,
                DI=>A(2),
                S=>I2,
                LO=>C2);
   
   I_36_254 : MUXCY_L
      port map (CI=>C0,
                DI=>A(1),
                S=>I1,
                LO=>C1);
   
   I_36_255 : MUXCY_L
      port map (CI=>CI,
                DI=>A(0),
                S=>I0,
                LO=>C0);
   
   I_36_272 : FMAP
      port map (I1=>A(1),
                I2=>B(1),
                I3=>dummy,
                I4=>dummy,
                O=>I1);
   
   I_36_275 : FMAP
      port map (I1=>A(0),
                I2=>B(0),
                I3=>dummy,
                I4=>dummy,
                O=>I0);
   
   I_36_279 : FMAP
      port map (I1=>A(2),
                I2=>B(2),
                I3=>dummy,
                I4=>dummy,
                O=>I2);
   
   I_36_283 : FMAP
      port map (I1=>A(3),
                I2=>B(3),
                I3=>dummy,
                I4=>dummy,
                O=>I3);
   
   I_36_287 : FMAP
      port map (I1=>A(4),
                I2=>B(4),
                I3=>dummy,
                I4=>dummy,
                O=>I4);
   
   I_36_291 : FMAP
      port map (I1=>A(5),
                I2=>B(5),
                I3=>dummy,
                I4=>dummy,
                O=>I5);
   
   I_36_295 : FMAP
      port map (I1=>A(6),
                I2=>B(6),
                I3=>dummy,
                I4=>dummy,
                O=>I6);
   
   I_36_299 : FMAP
      port map (I1=>A(7),
                I2=>B(7),
                I3=>dummy,
                I4=>dummy,
                O=>I7);
   
   I_36_354 : XOR2
      port map (I0=>A(0),
                I1=>B(0),
                O=>I0);
   
   I_36_355 : XOR2
      port map (I0=>A(1),
                I1=>B(1),
                O=>I1);
   
   I_36_356 : XOR2
      port map (I0=>A(2),
                I1=>B(2),
                O=>I2);
   
   I_36_357 : XOR2
      port map (I0=>A(3),
                I1=>B(3),
                O=>I3);
   
   I_36_358 : XOR2
      port map (I0=>A(4),
                I1=>B(4),
                O=>I4);
   
   I_36_359 : XOR2
      port map (I0=>A(5),
                I1=>B(5),
                O=>I5);
   
   I_36_360 : XOR2
      port map (I0=>A(6),
                I1=>B(6),
                O=>I6);
   
   I_36_361 : XOR2
      port map (I0=>A(7),
                I1=>B(7),
                O=>I7);
   
   I_36_362 : XOR2
      port map (I0=>A(8),
                I1=>B(8),
                O=>I8);
   
   I_36_363 : XOR2
      port map (I0=>A(9),
                I1=>B(9),
                O=>I9);
   
   I_36_364 : XOR2
      port map (I0=>A(10),
                I1=>B(10),
                O=>I10);
   
   I_36_365 : XOR2
      port map (I0=>A(11),
                I1=>B(11),
                O=>I11);
   
   I_36_366 : XOR2
      port map (I0=>A(12),
                I1=>B(12),
                O=>I12);
   
   I_36_367 : XOR2
      port map (I0=>A(13),
                I1=>B(13),
                O=>I13);
   
   I_36_368 : XOR2
      port map (I0=>A(14),
                I1=>B(14),
                O=>I14);
   
   I_36_369 : XOR2
      port map (I0=>A(15),
                I1=>B(15),
                O=>I15);
   
   I_36_375 : XOR2
      port map (I0=>C14O,
                I1=>CO_DUMMY,
                O=>OFL);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity CB4CE_MXILINX_pport is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          CEO : out   std_logic; 
          Q0  : out   std_logic; 
          Q1  : out   std_logic; 
          Q2  : out   std_logic; 
          Q3  : out   std_logic; 
          TC  : out   std_logic);
end CB4CE_MXILINX_pport;

architecture BEHAVIORAL of CB4CE_MXILINX_pport is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal T2       : std_logic;
   signal T3       : std_logic;
   signal XLXN_1   : std_logic;
   signal Q0_DUMMY : std_logic;
   signal Q1_DUMMY : std_logic;
   signal Q2_DUMMY : std_logic;
   signal Q3_DUMMY : std_logic;
   signal TC_DUMMY : std_logic;
   component FTCE_MXILINX_pport
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             T   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   
   component AND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   attribute HU_SET of I_Q0 : label is "I_Q0_18";
   attribute HU_SET of I_Q1 : label is "I_Q1_19";
   attribute HU_SET of I_Q2 : label is "I_Q2_20";
   attribute HU_SET of I_Q3 : label is "I_Q3_21";
begin
   Q0 <= Q0_DUMMY;
   Q1 <= Q1_DUMMY;
   Q2 <= Q2_DUMMY;
   Q3 <= Q3_DUMMY;
   TC <= TC_DUMMY;
   I_Q0 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>XLXN_1,
                Q=>Q0_DUMMY);
   
   I_Q1 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>Q0_DUMMY,
                Q=>Q1_DUMMY);
   
   I_Q2 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T2,
                Q=>Q2_DUMMY);
   
   I_Q3 : FTCE_MXILINX_pport
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                T=>T3,
                Q=>Q3_DUMMY);
   
   I_36_31 : AND4
      port map (I0=>Q3_DUMMY,
                I1=>Q2_DUMMY,
                I2=>Q1_DUMMY,
                I3=>Q0_DUMMY,
                O=>TC_DUMMY);
   
   I_36_32 : AND3
      port map (I0=>Q2_DUMMY,
                I1=>Q1_DUMMY,
                I2=>Q0_DUMMY,
                O=>T3);
   
   I_36_33 : AND2
      port map (I0=>Q1_DUMMY,
                I1=>Q0_DUMMY,
                O=>T2);
   
   I_36_58 : VCC
      port map (P=>XLXN_1);
   
   I_36_67 : AND2
      port map (I0=>CE,
                I1=>TC_DUMMY,
                O=>CEO);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity COMP16_MXILINX_pport is
   port ( A  : in    std_logic_vector (15 downto 0); 
          B  : in    std_logic_vector (15 downto 0); 
          EQ : out   std_logic);
end COMP16_MXILINX_pport;

architecture BEHAVIORAL of COMP16_MXILINX_pport is
   attribute BOX_TYPE   : string ;
   signal ABCF : std_logic;
   signal AB0  : std_logic;
   signal AB1  : std_logic;
   signal AB2  : std_logic;
   signal AB3  : std_logic;
   signal AB4  : std_logic;
   signal AB5  : std_logic;
   signal AB6  : std_logic;
   signal AB7  : std_logic;
   signal AB8  : std_logic;
   signal AB8B : std_logic;
   signal AB9  : std_logic;
   signal AB03 : std_logic;
   signal AB10 : std_logic;
   signal AB11 : std_logic;
   signal AB12 : std_logic;
   signal AB13 : std_logic;
   signal AB14 : std_logic;
   signal AB15 : std_logic;
   signal AB47 : std_logic;
   component AND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4 : component is "BLACK_BOX";
   
   component XNOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XNOR2 : component is "BLACK_BOX";
   
begin
   I_36_31 : AND4
      port map (I0=>ABCF,
                I1=>AB8B,
                I2=>AB47,
                I3=>AB03,
                O=>EQ);
   
   I_36_40 : XNOR2
      port map (I0=>B(1),
                I1=>A(1),
                O=>AB1);
   
   I_36_41 : XNOR2
      port map (I0=>B(0),
                I1=>A(0),
                O=>AB0);
   
   I_36_54 : XNOR2
      port map (I0=>B(2),
                I1=>A(2),
                O=>AB2);
   
   I_36_55 : XNOR2
      port map (I0=>B(3),
                I1=>A(3),
                O=>AB3);
   
   I_36_56 : XNOR2
      port map (I0=>B(4),
                I1=>A(4),
                O=>AB4);
   
   I_36_57 : XNOR2
      port map (I0=>B(5),
                I1=>A(5),
                O=>AB5);
   
   I_36_58 : XNOR2
      port map (I0=>B(6),
                I1=>A(6),
                O=>AB6);
   
   I_36_59 : XNOR2
      port map (I0=>B(7),
                I1=>A(7),
                O=>AB7);
   
   I_36_60 : XNOR2
      port map (I0=>B(8),
                I1=>A(8),
                O=>AB8);
   
   I_36_61 : XNOR2
      port map (I0=>B(9),
                I1=>A(9),
                O=>AB9);
   
   I_36_62 : XNOR2
      port map (I0=>B(10),
                I1=>A(10),
                O=>AB10);
   
   I_36_63 : XNOR2
      port map (I0=>B(11),
                I1=>A(11),
                O=>AB11);
   
   I_36_64 : XNOR2
      port map (I0=>B(12),
                I1=>A(12),
                O=>AB12);
   
   I_36_65 : XNOR2
      port map (I0=>B(13),
                I1=>A(13),
                O=>AB13);
   
   I_36_66 : XNOR2
      port map (I0=>B(14),
                I1=>A(14),
                O=>AB14);
   
   I_36_67 : XNOR2
      port map (I0=>B(15),
                I1=>A(15),
                O=>AB15);
   
   I_36_68 : AND4
      port map (I0=>AB7,
                I1=>AB6,
                I2=>AB5,
                I3=>AB4,
                O=>AB47);
   
   I_36_69 : AND4
      port map (I0=>AB11,
                I1=>AB10,
                I2=>AB9,
                I3=>AB8,
                O=>AB8B);
   
   I_36_70 : AND4
      port map (I0=>AB15,
                I1=>AB14,
                I2=>AB13,
                I3=>AB12,
                O=>ABCF);
   
   I_36_71 : AND4
      port map (I0=>AB3,
                I1=>AB2,
                I2=>AB1,
                I3=>AB0,
                O=>AB03);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OR8_MXILINX_pport is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          I4 : in    std_logic; 
          I5 : in    std_logic; 
          I6 : in    std_logic; 
          I7 : in    std_logic; 
          O  : out   std_logic);
end OR8_MXILINX_pport;

architecture BEHAVIORAL of OR8_MXILINX_pport is
   attribute BOX_TYPE   : string ;
   attribute RLOC       : string ;
   signal dummy   : std_logic;
   signal S0      : std_logic;
   signal S1      : std_logic;
   signal O_DUMMY : std_logic;
   component FMAP
      port ( I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : in    std_logic);
   end component;
   attribute BOX_TYPE of FMAP : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
   attribute RLOC of I_36_91 : label is "R0C0.S0";
   attribute RLOC of I_36_116 : label is "R0C0.S1";
   attribute RLOC of I_36_117 : label is "R0C0.S1";
begin
   O <= O_DUMMY;
   I_36_91 : FMAP
      port map (I1=>S0,
                I2=>S1,
                I3=>dummy,
                I4=>dummy,
                O=>O_DUMMY);
   
   I_36_94 : OR2
      port map (I0=>S0,
                I1=>S1,
                O=>O_DUMMY);
   
   I_36_95 : OR4
      port map (I0=>I4,
                I1=>I5,
                I2=>I6,
                I3=>I7,
                O=>S1);
   
   I_36_112 : OR4
      port map (I0=>I0,
                I1=>I1,
                I2=>I2,
                I3=>I3,
                O=>S0);
   
   I_36_116 : FMAP
      port map (I1=>I0,
                I2=>I1,
                I3=>I2,
                I4=>I3,
                O=>S0);
   
   I_36_117 : FMAP
      port map (I1=>I4,
                I2=>I5,
                I3=>I6,
                I4=>I7,
                O=>S1);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity COMPM16_MXILINX_pport is
   port ( A  : in    std_logic_vector (15 downto 0); 
          B  : in    std_logic_vector (15 downto 0); 
          GT : out   std_logic; 
          LT : out   std_logic);
end COMPM16_MXILINX_pport;

architecture BEHAVIORAL of COMPM16_MXILINX_pport is
   attribute BOX_TYPE   : string ;
   attribute HU_SET     : string ;
   signal EQ_1    : std_logic;
   signal EQ_3    : std_logic;
   signal EQ_5    : std_logic;
   signal EQ_7    : std_logic;
   signal EQ_9    : std_logic;
   signal EQ_11   : std_logic;
   signal EQ_13   : std_logic;
   signal EQ_15   : std_logic;
   signal EQ2_3   : std_logic;
   signal EQ4_5   : std_logic;
   signal EQ6_7   : std_logic;
   signal EQ8_9   : std_logic;
   signal EQ8_15  : std_logic;
   signal EQ10_11 : std_logic;
   signal EQ12_13 : std_logic;
   signal EQ14_15 : std_logic;
   signal GE0_1   : std_logic;
   signal GE2_3   : std_logic;
   signal GE4_5   : std_logic;
   signal GE6_7   : std_logic;
   signal GE8_9   : std_logic;
   signal GE10_11 : std_logic;
   signal GE12_13 : std_logic;
   signal GE14_15 : std_logic;
   signal GTA     : std_logic;
   signal GTB     : std_logic;
   signal GTC     : std_logic;
   signal GTD     : std_logic;
   signal GTE     : std_logic;
   signal GTF     : std_logic;
   signal GTG     : std_logic;
   signal GTH     : std_logic;
   signal GT_1    : std_logic;
   signal GT_3    : std_logic;
   signal GT_5    : std_logic;
   signal GT_7    : std_logic;
   signal GT_9    : std_logic;
   signal GT_11   : std_logic;
   signal GT_13   : std_logic;
   signal GT_15   : std_logic;
   signal GT0_1   : std_logic;
   signal GT2_3   : std_logic;
   signal GT4_5   : std_logic;
   signal GT6_7   : std_logic;
   signal GT8_9   : std_logic;
   signal GT10_11 : std_logic;
   signal GT12_13 : std_logic;
   signal LE0_1   : std_logic;
   signal LE2_3   : std_logic;
   signal LE4_5   : std_logic;
   signal LE6_7   : std_logic;
   signal LE8_9   : std_logic;
   signal LE10_11 : std_logic;
   signal LE12_13 : std_logic;
   signal LE14_15 : std_logic;
   signal LTA     : std_logic;
   signal LTB     : std_logic;
   signal LTC     : std_logic;
   signal LTD     : std_logic;
   signal LTE     : std_logic;
   signal LTF     : std_logic;
   signal LTG     : std_logic;
   signal LTH     : std_logic;
   signal LT_1    : std_logic;
   signal LT_3    : std_logic;
   signal LT_5    : std_logic;
   signal LT_7    : std_logic;
   signal LT_9    : std_logic;
   signal LT_11   : std_logic;
   signal LT_13   : std_logic;
   signal LT_15   : std_logic;
   signal LT0_1   : std_logic;
   signal LT2_3   : std_logic;
   signal LT4_5   : std_logic;
   signal LT6_7   : std_logic;
   signal LT8_9   : std_logic;
   signal LT10_11 : std_logic;
   signal LT12_13 : std_logic;
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
   component XNOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XNOR2 : component is "BLACK_BOX";
   
   component AND3B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component AND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4 : component is "BLACK_BOX";
   
   component AND5
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND5 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component OR8_MXILINX_pport
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             I5 : in    std_logic; 
             I6 : in    std_logic; 
             I7 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   attribute HU_SET of I_36_124 : label is "I_36_124_22";
   attribute HU_SET of I_36_125 : label is "I_36_125_23";
begin
   I_36_9 : AND2B1
      port map (I0=>A(7),
                I1=>B(7),
                O=>LT_7);
   
   I_36_10 : NOR2
      port map (I0=>GT6_7,
                I1=>LT6_7,
                O=>EQ6_7);
   
   I_36_11 : AND2B1
      port map (I0=>B(7),
                I1=>A(7),
                O=>GT_7);
   
   I_36_12 : XNOR2
      port map (I0=>A(7),
                I1=>B(7),
                O=>EQ_7);
   
   I_36_13 : AND3B1
      port map (I0=>B(6),
                I1=>EQ_7,
                I2=>A(6),
                O=>GE6_7);
   
   I_36_14 : AND3B1
      port map (I0=>A(6),
                I1=>EQ_7,
                I2=>B(6),
                O=>LE6_7);
   
   I_36_15 : OR2
      port map (I0=>GT_7,
                I1=>GE6_7,
                O=>GT6_7);
   
   I_36_16 : OR2
      port map (I0=>LT_7,
                I1=>LE6_7,
                O=>LT6_7);
   
   I_36_22 : AND2
      port map (I0=>EQ8_15,
                I1=>LT6_7,
                O=>LTD);
   
   I_36_23 : AND2
      port map (I0=>GT6_7,
                I1=>EQ8_15,
                O=>GTD);
   
   I_36_24 : AND4
      port map (I0=>GT2_3,
                I1=>EQ4_5,
                I2=>EQ6_7,
                I3=>EQ8_15,
                O=>GTB);
   
   I_36_25 : AND4
      port map (I0=>EQ8_15,
                I1=>EQ6_7,
                I2=>EQ4_5,
                I3=>LT2_3,
                O=>LTB);
   
   I_36_35 : NOR2
      port map (I0=>GT2_3,
                I1=>LT2_3,
                O=>EQ2_3);
   
   I_36_36 : AND2B1
      port map (I0=>B(3),
                I1=>A(3),
                O=>GT_3);
   
   I_36_37 : AND2B1
      port map (I0=>A(1),
                I1=>B(1),
                O=>LT_1);
   
   I_36_38 : XNOR2
      port map (I0=>A(3),
                I1=>B(3),
                O=>EQ_3);
   
   I_36_39 : AND2B1
      port map (I0=>B(1),
                I1=>A(1),
                O=>GT_1);
   
   I_36_40 : AND3B1
      port map (I0=>B(2),
                I1=>EQ_3,
                I2=>A(2),
                O=>GE2_3);
   
   I_36_41 : AND3B1
      port map (I0=>A(2),
                I1=>EQ_3,
                I2=>B(2),
                O=>LE2_3);
   
   I_36_42 : OR2
      port map (I0=>GT_1,
                I1=>GE0_1,
                O=>GT0_1);
   
   I_36_43 : OR2
      port map (I0=>GT_3,
                I1=>GE2_3,
                O=>GT2_3);
   
   I_36_44 : OR2
      port map (I0=>LT_3,
                I1=>LE2_3,
                O=>LT2_3);
   
   I_36_50 : OR2
      port map (I0=>LT_1,
                I1=>LE0_1,
                O=>LT0_1);
   
   I_36_51 : XNOR2
      port map (I0=>A(1),
                I1=>B(1),
                O=>EQ_1);
   
   I_36_52 : AND3B1
      port map (I0=>A(0),
                I1=>EQ_1,
                I2=>B(0),
                O=>LE0_1);
   
   I_36_53 : AND5
      port map (I0=>EQ8_15,
                I1=>EQ6_7,
                I2=>EQ4_5,
                I3=>EQ2_3,
                I4=>LT0_1,
                O=>LTA);
   
   I_36_54 : AND3B1
      port map (I0=>B(0),
                I1=>EQ_1,
                I2=>A(0),
                O=>GE0_1);
   
   I_36_55 : AND5
      port map (I0=>GT0_1,
                I1=>EQ2_3,
                I2=>EQ4_5,
                I3=>EQ6_7,
                I4=>EQ8_15,
                O=>GTA);
   
   I_36_57 : AND3
      port map (I0=>GT4_5,
                I1=>EQ6_7,
                I2=>EQ8_15,
                O=>GTC);
   
   I_36_58 : AND3
      port map (I0=>EQ8_15,
                I1=>EQ6_7,
                I2=>LT4_5,
                O=>LTC);
   
   I_36_59 : NOR2
      port map (I0=>GT10_11,
                I1=>LT10_11,
                O=>EQ10_11);
   
   I_36_60 : AND3
      port map (I0=>EQ14_15,
                I1=>EQ12_13,
                I2=>LT10_11,
                O=>LTF);
   
   I_36_61 : AND2
      port map (I0=>GT12_13,
                I1=>EQ14_15,
                O=>GTG);
   
   I_36_62 : AND2
      port map (I0=>EQ14_15,
                I1=>LT12_13,
                O=>LTG);
   
   I_36_63 : AND4
      port map (I0=>EQ14_15,
                I1=>EQ12_13,
                I2=>EQ10_11,
                I3=>LT8_9,
                O=>LTE);
   
   I_36_64 : AND4
      port map (I0=>GT8_9,
                I1=>EQ10_11,
                I2=>EQ12_13,
                I3=>EQ14_15,
                O=>GTE);
   
   I_36_65 : AND3
      port map (I0=>GT10_11,
                I1=>EQ12_13,
                I2=>EQ14_15,
                O=>GTF);
   
   I_36_66 : OR2
      port map (I0=>LT_15,
                I1=>LE14_15,
                O=>LTH);
   
   I_36_67 : OR2
      port map (I0=>GT_15,
                I1=>GE14_15,
                O=>GTH);
   
   I_36_68 : OR2
      port map (I0=>GT_13,
                I1=>GE12_13,
                O=>GT12_13);
   
   I_36_69 : AND3B1
      port map (I0=>A(12),
                I1=>EQ_13,
                I2=>B(12),
                O=>LE12_13);
   
   I_36_70 : AND3B1
      port map (I0=>B(12),
                I1=>EQ_13,
                I2=>A(12),
                O=>GE12_13);
   
   I_36_71 : AND3B1
      port map (I0=>A(14),
                I1=>EQ_15,
                I2=>B(14),
                O=>LE14_15);
   
   I_36_72 : AND3B1
      port map (I0=>B(14),
                I1=>EQ_15,
                I2=>A(14),
                O=>GE14_15);
   
   I_36_73 : XNOR2
      port map (I0=>A(13),
                I1=>B(13),
                O=>EQ_13);
   
   I_36_74 : AND2B1
      port map (I0=>B(13),
                I1=>A(13),
                O=>GT_13);
   
   I_36_75 : XNOR2
      port map (I0=>A(15),
                I1=>B(15),
                O=>EQ_15);
   
   I_36_76 : AND2B1
      port map (I0=>A(13),
                I1=>B(13),
                O=>LT_13);
   
   I_36_77 : AND2B1
      port map (I0=>B(15),
                I1=>A(15),
                O=>GT_15);
   
   I_36_78 : OR2
      port map (I0=>LT_13,
                I1=>LE12_13,
                O=>LT12_13);
   
   I_36_79 : NOR2
      port map (I0=>GTH,
                I1=>LTH,
                O=>EQ14_15);
   
   I_36_80 : AND2B1
      port map (I0=>A(11),
                I1=>B(11),
                O=>LT_11);
   
   I_36_81 : OR2
      port map (I0=>LT_9,
                I1=>LE8_9,
                O=>LT8_9);
   
   I_36_82 : AND2B1
      port map (I0=>B(11),
                I1=>A(11),
                O=>GT_11);
   
   I_36_83 : AND2B1
      port map (I0=>A(9),
                I1=>B(9),
                O=>LT_9);
   
   I_36_84 : XNOR2
      port map (I0=>A(11),
                I1=>B(11),
                O=>EQ_11);
   
   I_36_85 : AND2B1
      port map (I0=>B(9),
                I1=>A(9),
                O=>GT_9);
   
   I_36_86 : XNOR2
      port map (I0=>A(9),
                I1=>B(9),
                O=>EQ_9);
   
   I_36_87 : AND3B1
      port map (I0=>B(10),
                I1=>EQ_11,
                I2=>A(10),
                O=>GE10_11);
   
   I_36_88 : AND3B1
      port map (I0=>A(10),
                I1=>EQ_11,
                I2=>B(10),
                O=>LE10_11);
   
   I_36_89 : AND3B1
      port map (I0=>B(8),
                I1=>EQ_9,
                I2=>A(8),
                O=>GE8_9);
   
   I_36_90 : AND3B1
      port map (I0=>A(8),
                I1=>EQ_9,
                I2=>B(8),
                O=>LE8_9);
   
   I_36_91 : OR2
      port map (I0=>GT_9,
                I1=>GE8_9,
                O=>GT8_9);
   
   I_36_92 : OR2
      port map (I0=>GT_11,
                I1=>GE10_11,
                O=>GT10_11);
   
   I_36_93 : OR2
      port map (I0=>LT_11,
                I1=>LE10_11,
                O=>LT10_11);
   
   I_36_94 : NOR2
      port map (I0=>GT12_13,
                I1=>LT12_13,
                O=>EQ12_13);
   
   I_36_95 : AND2B1
      port map (I0=>A(15),
                I1=>B(15),
                O=>LT_15);
   
   I_36_96 : NOR2
      port map (I0=>GT4_5,
                I1=>LT4_5,
                O=>EQ4_5);
   
   I_36_97 : OR2
      port map (I0=>LT_5,
                I1=>LE4_5,
                O=>LT4_5);
   
   I_36_98 : AND2B1
      port map (I0=>A(5),
                I1=>B(5),
                O=>LT_5);
   
   I_36_99 : AND2B1
      port map (I0=>B(5),
                I1=>A(5),
                O=>GT_5);
   
   I_36_100 : XNOR2
      port map (I0=>A(5),
                I1=>B(5),
                O=>EQ_5);
   
   I_36_101 : AND3B1
      port map (I0=>B(4),
                I1=>EQ_5,
                I2=>A(4),
                O=>GE4_5);
   
   I_36_102 : AND3B1
      port map (I0=>A(4),
                I1=>EQ_5,
                I2=>B(4),
                O=>LE4_5);
   
   I_36_103 : OR2
      port map (I0=>GT_5,
                I1=>GE4_5,
                O=>GT4_5);
   
   I_36_111 : NOR2
      port map (I0=>GT8_9,
                I1=>LT8_9,
                O=>EQ8_9);
   
   I_36_114 : AND2B1
      port map (I0=>A(3),
                I1=>B(3),
                O=>LT_3);
   
   I_36_124 : OR8_MXILINX_pport
      port map (I0=>LTH,
                I1=>LTG,
                I2=>LTF,
                I3=>LTE,
                I4=>LTD,
                I5=>LTC,
                I6=>LTB,
                I7=>LTA,
                O=>LT);
   
   I_36_125 : OR8_MXILINX_pport
      port map (I0=>GTD,
                I1=>GTC,
                I2=>GTB,
                I3=>GTA,
                I4=>GTH,
                I5=>GTG,
                I6=>GTF,
                I7=>GTE,
                O=>GT);
   
   I_36_162 : AND4
      port map (I0=>EQ14_15,
                I1=>EQ12_13,
                I2=>EQ10_11,
                I3=>EQ8_9,
                O=>EQ8_15);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity pport is
   port ( CLOCK       : in    std_logic; 
          FreqInput0  : in    std_logic; 
          FreqInput1  : in    std_logic; 
          FreqInput2  : in    std_logic; 
          FreqInput3  : in    std_logic; 
          FreqInput4  : in    std_logic; 
          FreqInput5  : in    std_logic; 
          FreqInput6  : in    std_logic; 
          PulseCount0 : in    std_logic; 
          PulseCount1 : in    std_logic; 
          PulseCount2 : in    std_logic; 
          PulseCount3 : in    std_logic; 
          PulseCount4 : in    std_logic; 
          LED         : out   std_logic_vector (7 downto 0); 
          PULSE       : out   std_logic; 
          TRIG        : out   std_logic);
end pport;

architecture BEHAVIORAL of pport is
   attribute BOX_TYPE   : string ;
   attribute INIT       : string ;
   attribute HU_SET     : string ;
   signal Add16         : std_logic_vector (15 downto 0);
   signal FreqCount     : std_logic_vector (15 downto 0);
   signal PULSECOUNTBus : std_logic_vector (15 downto 0);
   signal XLXN_14       : std_logic;
   signal XLXN_32       : std_logic;
   signal XLXN_121      : std_logic;
   signal XLXN_128      : std_logic;
   signal XLXN_142      : std_logic_vector (15 downto 0);
   signal XLXN_152      : std_logic;
   signal XLXN_153      : std_logic_vector (15 downto 0);
   signal XLXN_210      : std_logic;
   signal XLXN_211      : std_logic;
   signal XLXN_217      : std_logic;
   signal XLXN_226      : std_logic_vector (15 downto 0);
   signal XLXN_227      : std_logic_vector (15 downto 0);
   signal XLXN_245      : std_logic;
   signal XLXN_254      : std_logic;
   signal XLXN_257      : std_logic;
   signal XLXN_260      : std_logic;
   signal XLXN_264      : std_logic;
   signal XLXN_266      : std_logic;
   signal XLXN_267      : std_logic;
   signal XLXN_268      : std_logic;
   signal XLXN_269      : std_logic;
   signal XLXN_270      : std_logic;
   signal XLXN_271      : std_logic;
   component IBUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of IBUFG : component is "BLACK_BOX";
   
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component FDC
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute INIT of FDC : component is "0";
   attribute BOX_TYPE of FDC : component is "BLACK_BOX";
   
   component CB16CE_MXILINX_pport
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             CEO : out   std_logic; 
             Q   : out   std_logic_vector (15 downto 0); 
             TC  : out   std_logic);
   end component;
   
   component COMPM16_MXILINX_pport
      port ( A  : in    std_logic_vector (15 downto 0); 
             B  : in    std_logic_vector (15 downto 0); 
             GT : out   std_logic; 
             LT : out   std_logic);
   end component;
   
   component COMP16_MXILINX_pport
      port ( A  : in    std_logic_vector (15 downto 0); 
             B  : in    std_logic_vector (15 downto 0); 
             EQ : out   std_logic);
   end component;
   
   component CB4CE_MXILINX_pport
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             CEO : out   std_logic; 
             Q0  : out   std_logic; 
             Q1  : out   std_logic; 
             Q2  : out   std_logic; 
             Q3  : out   std_logic; 
             TC  : out   std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component ADD16_MXILINX_pport
      port ( A   : in    std_logic_vector (15 downto 0); 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OBUF8_MXILINX_pport
      port ( I : in    std_logic_vector (7 downto 0); 
             O : out   std_logic_vector (7 downto 0));
   end component;
   
   component CB2CE_MXILINX_pport
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             CEO : out   std_logic; 
             Q0  : out   std_logic; 
             Q1  : out   std_logic; 
             TC  : out   std_logic);
   end component;
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_59 : label is "XLXI_59_24";
   attribute HU_SET of XLXI_61 : label is "XLXI_61_25";
   attribute HU_SET of XLXI_62 : label is "XLXI_62_26";
   attribute HU_SET of XLXI_63 : label is "XLXI_63_27";
   attribute HU_SET of XLXI_64 : label is "XLXI_64_28";
   attribute HU_SET of XLXI_66 : label is "XLXI_66_29";
   attribute HU_SET of XLXI_138 : label is "XLXI_138_32";
   attribute HU_SET of XLXI_141 : label is "XLXI_141_30";
   attribute HU_SET of XLXI_143 : label is "XLXI_143_31";
begin
   FreqCount(6 downto 0) <= b"0000000";
   FreqCount(15 downto 14) <= b"00";
   PULSECOUNTBus(7 downto 5) <= b"000";
   XLXN_142(15 downto 0) <= x"0000";
   XLXN_153(15 downto 0) <= x"03E8";
   XLXI_7 : IBUFG
      port map (I=>CLOCK,
                O=>XLXN_254);
   
   XLXI_8 : OBUF
      port map (I=>XLXN_128,
                O=>PULSE);
   
   XLXI_11 : VCC
      port map (P=>XLXN_14);
   
   XLXI_19 : VCC
      port map (P=>XLXN_32);
   
   XLXI_42 : OBUF
      port map (I=>XLXN_121,
                O=>TRIG);
   
   XLXI_50 : FDC
      port map (C=>XLXN_254,
                CLR=>XLXN_264,
                D=>XLXN_267,
                Q=>XLXN_128);
   
   XLXI_59 : CB16CE_MXILINX_pport
      port map (C=>XLXN_267,
                CE=>XLXN_14,
                CLR=>XLXN_268,
                CEO=>open,
                Q(15 downto 0)=>XLXN_226(15 downto 0),
                TC=>open);
   
   XLXI_61 : COMPM16_MXILINX_pport
      port map (A(15 downto 0)=>PULSECOUNTBus(15 downto 0),
                B(15 downto 0)=>XLXN_226(15 downto 0),
                GT=>open,
                LT=>XLXN_264);
   
   XLXI_62 : COMP16_MXILINX_pport
      port map (A(15 downto 0)=>XLXN_227(15 downto 0),
                B(15 downto 0)=>Add16(15 downto 0),
                EQ=>XLXN_268);
   
   XLXI_63 : COMP16_MXILINX_pport
      port map (A(15 downto 0)=>XLXN_226(15 downto 0),
                B(15 downto 0)=>XLXN_142(15 downto 0),
                EQ=>XLXN_121);
   
   XLXI_64 : CB4CE_MXILINX_pport
      port map (C=>XLXN_254,
                CE=>XLXN_32,
                CLR=>XLXN_152,
                CEO=>open,
                Q0=>XLXN_260,
                Q1=>XLXN_271,
                Q2=>open,
                Q3=>open,
                TC=>open);
   
   XLXI_65 : GND
      port map (G=>XLXN_152);
   
   XLXI_66 : ADD16_MXILINX_pport
      port map (A(15 downto 0)=>FreqCount(15 downto 0),
                B(15 downto 0)=>XLXN_153(15 downto 0),
                CI=>XLXN_245,
                CO=>open,
                OFL=>open,
                S(15 downto 0)=>Add16(15 downto 0));
   
   XLXI_69 : GND
      port map (G=>XLXN_245);
   
   XLXI_91 : IBUF
      port map (I=>PulseCount0,
                O=>PULSECOUNTBus(0));
   
   XLXI_92 : IBUF
      port map (I=>PulseCount1,
                O=>XLXN_217);
   
   XLXI_102 : IBUF
      port map (I=>PulseCount4,
                O=>PULSECOUNTBus(3));
   
   XLXI_103 : IBUF
      port map (I=>PulseCount3,
                O=>XLXN_210);
   
   XLXI_104 : IBUF
      port map (I=>PulseCount2,
                O=>XLXN_211);
   
   XLXI_106 : INV
      port map (I=>XLXN_210,
                O=>PULSECOUNTBus(4));
   
   XLXI_107 : INV
      port map (I=>XLXN_211,
                O=>PULSECOUNTBus(2));
   
   XLXI_125 : IBUF
      port map (I=>FreqInput0,
                O=>FreqCount(7));
   
   XLXI_126 : IBUF
      port map (I=>FreqInput1,
                O=>FreqCount(8));
   
   XLXI_127 : IBUF
      port map (I=>FreqInput2,
                O=>FreqCount(9));
   
   XLXI_128 : IBUF
      port map (I=>FreqInput3,
                O=>FreqCount(10));
   
   XLXI_129 : IBUF
      port map (I=>FreqInput4,
                O=>FreqCount(11));
   
   XLXI_130 : IBUF
      port map (I=>FreqInput5,
                O=>FreqCount(12));
   
   XLXI_137 : IBUF
      port map (I=>FreqInput6,
                O=>FreqCount(13));
   
   XLXI_138 : OBUF8_MXILINX_pport
      port map (I(7 downto 0)=>PULSECOUNTBus(7 downto 0),
                O(7 downto 0)=>LED(7 downto 0));
   
   XLXI_140 : INV
      port map (I=>XLXN_217,
                O=>PULSECOUNTBus(1));
   
   XLXI_141 : CB16CE_MXILINX_pport
      port map (C=>XLXN_260,
                CE=>XLXN_270,
                CLR=>XLXN_268,
                CEO=>open,
                Q(15 downto 0)=>XLXN_227(15 downto 0),
                TC=>open);
   
   XLXI_142 : VCC
      port map (P=>XLXN_270);
   
   XLXI_143 : CB2CE_MXILINX_pport
      port map (C=>XLXN_271,
                CE=>XLXN_270,
                CLR=>XLXN_257,
                CEO=>open,
                Q0=>XLXN_266,
                Q1=>XLXN_267,
                TC=>open);
   
   XLXI_144 : AND2
      port map (I0=>XLXN_267,
                I1=>XLXN_266,
                O=>XLXN_269);
   
   XLXI_145 : OR2
      port map (I0=>XLXN_269,
                I1=>XLXN_268,
                O=>XLXN_257);
   
end BEHAVIORAL;


