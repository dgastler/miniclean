#include <HardwareTrigger.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[HardwareTrigger::COUNT] =
  {
    "VETO",
    "EVNT",
    "COMP",
    "BUTN",
    "CAL0",
    "CAL1",
    "CAL2",
    "PERI"
  };

const char * HardwareTrigger::GetTypeName(HardwareTrigger::Type type)
{
  if(type < HardwareTrigger::COUNT)
    return TypeName[type];
  return NULL;
}

HardwareTrigger::Type HardwareTrigger::GetType(const char * name)
{
  std::string searchString(name);
  HardwareTrigger::Type ret = HardwareTrigger::Type(0);
  for(HardwareTrigger::Type type = HardwareTrigger::Type(0);
      type < HardwareTrigger::COUNT; 
      type = HardwareTrigger::Type(((int)type)+1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }
  return ret;
}
