#ifndef __FEPCRATMANAGER__
#define __FEPCRATMANAGER__


#include <MemoryManager.h>
#include <FEPC.h>
#include <xmlHelper.h>
#include <EVBlock.h>
#include "RAT/DS/EV.hh"

class FEPCRatManager : public MemoryManager , public FEPC
{
 public:
  FEPCRatManager(std::string Name);
  ~FEPCRatManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  EVBlock * GetEVBlock(uint32_t iEV) 
    {
      if(iEV < Memory.size())
	{
	  return(&(Memory[iEV]));
      }
    return(NULL);
  }
 private: 
  uint32_t NumEV;
  uint32_t VectorHashSize;
  std::vector<EVBlock> Memory;
  void Deallocate();
};

#endif
