#ifndef __DCMESSAGEPROCESSOR__
#define __DCMESSAGEPROCESSOR__


#include <DCLauncher.h>
#include <DCThread.h>
#include <DCMessage.h>

#include <timeHelper.h>

class DCMessageProcessor
{
 public:
  DCMessageProcessor(DCLauncher * _Launcher);
  ~DCMessageProcessor();
  //Find and process all the DCMessages from the threads.
  //Return will update the run status of the DAQ
  bool ProcessMessage(int selectReturn,
		      fd_set ReadSet,
		      fd_set WriteSet,
		      fd_set ExceptionSet);

  bool ProcessMessage(DCMessage::Message message);
 private:
  DCLauncher * Launcher;
  
  //Message processing functions

  void Stop(DCMessage::Message & message,DCThread * thread,bool & run);
  void Go(DCMessage::Message & message,DCThread * thread,bool & run);
  void Pause(DCMessage::Message & message,DCThread * thread,bool & run);
  void ReadOutSize(DCMessage::Message & message,DCThread * thread,bool & run);
  void ReadOutNumber(DCMessage::Message & message,DCThread * thread,bool & run);
  void ReadOutEvents(DCMessage::Message & message,DCThread * thread,bool & run);
  void RATError_POE(DCMessage::Message & message,DCThread * thread,bool & run);
  void EventError_POE(DCMessage::Message & message,DCThread * thread,bool & run);
  void BlocksRead(DCMessage::Message & message,DCThread * thread,bool & run);
  void PulseHits(DCMessage::Message & message,DCThread * thread,bool & run);
  void EventsRead(DCMessage::Message & message,DCThread * thread,bool & run);
  void DataIn(DCMessage::Message & message,DCThread * thread,bool & run);
  void DataOut(DCMessage::Message & message,DCThread * thread,bool & run);
  void Launch(DCMessage::Message & message,DCThread * thread,bool & run);
  void RunNumber(DCMessage::Message & message,DCThread * thread,bool & run);


  //Map of message types to 
  std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,DCThread *,bool &)> typeMap;
  
};

#endif
