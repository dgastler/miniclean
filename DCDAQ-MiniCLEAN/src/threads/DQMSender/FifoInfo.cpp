#include "FifoInfo.h"

FifoInfo::FifoInfo()
{
  pid=-1;
  fd=-1;
  name=NULL;
  status=true;
}

void FifoInfo::Clear()
{
  pid=-1;
  fd=-1;
  name=NULL;
  status=true;
}
