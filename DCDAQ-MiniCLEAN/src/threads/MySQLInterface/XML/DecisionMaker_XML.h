#ifndef DECISIONMAKER_XML_H
#define DECISIONMAKER_XML_H

#include <Thread_XML.h>
#include <math.h>

class DecisionMaker_XML: public Thread_XML
{
 public:
  DecisionMaker_XML(DCMySQL * conn,int runNumber);
  virtual bool Generate_XML(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responsesNode);

 private:
  bool GetHardwareTrigger(std::string threadName);
  bool GetDRSettings(xmlNode* drSetttingsNode);
  bool GetPMTS(xmlNode * drSettingsNode,std::string pcName);
  
};

#endif
