#ifdef __RATDS__
#ifndef __FEPCBADEVENT__
#define __FEPCBADEVENT__
/*
  Thread description
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <FEPCRATManager.h>
#include <StatMessage.h>

#include <map>

class FEPCBadEvent : public DCThread 
{
 public:
  FEPCBadEvent(std::string Name);
  ~FEPCBadEvent(){};

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);  

  virtual void MainLoop();
 private:

  //==============================================================
  //DCThread 
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);  
  virtual void ProcessTimeout();

  //==============================================================
  //Memory manager interface
  //==============================================================
  FEPCRATManager * mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  uint32_t badEventCount;
  
  std::map<int8_t,uint32_t> stateCount;
  std::map<Level::Type,uint32_t> errorCodeCount;  
  std::deque<time_t> badEventTimes;

  uint32_t badEventWindow;
  uint32_t badEventThresh;
  bool shutdownCheck;
};
#endif
#endif
