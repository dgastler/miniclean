#include <DecisionMaker_XML.h>

DecisionMaker_XML::DecisionMaker_XML(DCMySQL * conn,int runNumber)
  : Thread_XML(conn,runNumber)
{
}

bool DecisionMaker_XML::Generate_XML(xmlNode * threadsNode,
				     std::string pcName,
				     std::string threadType,
				     std::string threadName,
				     xmlNode * responsesNode)
{
  bool ret = Thread_XML::Generate_XML(threadsNode,
				      pcName,
				      threadType,
				      threadName,
				      responsesNode);

  GetHardwareTrigger(threadName); 
  xmlNode *drSettingsNode=xmlNewChild(threadNode,NULL,
				      (xmlChar *)"DRSettings",NULL);
  ret |= GetDRSettings(drSettingsNode);
  ret |= GetPMTS(drSettingsNode,pcName);
  return ret;
}

bool DecisionMaker_XML::GetHardwareTrigger(std::string threadName)
{
  bool ret = true;
  std::stringstream query;
  query << "SELECT Hardware_Trigger,ReductionLevel,Software_Trigger from SThread_DecisionMaker where thread_name='"
	<< threadName << "' AND run_number=" << runNumber;
  enum{HARDWARE_TRIGGER=0,REDUCTION_LEVEL,SOFTWARE_TRIGGER};
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  std::vector<std::vector<std::string> > hwTrigger;
  DCMySQLConnection->GetTable(hwTrigger);
  DCMySQLConnection->FreeResult();
  
  for(size_t iTrig = 0; iTrig < hwTrigger.size();iTrig++)
    {
      if( (!boost::algorithm::iequals(hwTrigger[iTrig][HARDWARE_TRIGGER],"NULL")) &&
	  (!boost::algorithm::iequals(hwTrigger[iTrig][REDUCTION_LEVEL],"NULL")))
	{
	  xmlNode * hwTriggerNode = xmlNewChild(threadNode,NULL,
						(xmlChar*) hwTrigger[iTrig][HARDWARE_TRIGGER].c_str(),
						NULL);
	  xmlNewTextChild(hwTriggerNode,NULL,
			  (xmlChar*) "LEVEL",
			  (xmlChar*) hwTrigger[iTrig][REDUCTION_LEVEL].c_str());
	  if(!boost::algorithm::iequals(hwTrigger[iTrig][SOFTWARE_TRIGGER],"NULL"))
	    {
	      xmlNewTextChild(hwTriggerNode,NULL,(xmlChar*) "SoftwareTrigger",NULL);
	    }						      						      
	}
    }
  return ret;
}

bool DecisionMaker_XML::GetDRSettings(xmlNode * drsettingsNode)
{
  std::stringstream drSettingsQuery;
  drSettingsQuery << "SELECT * FROM Settings_DataReduction WHERE run_number=" << runNumber;

  if(!DCMySQLConnection->RunQuery(drSettingsQuery.str()))
    {
      return false;
    }
  std::vector<std::vector<std::string> > drSettings;
  std::vector<std::string> drSettingsFields;
  DCMySQLConnection->GetTable(drSettings);
  DCMySQLConnection->GetFieldNames(drSettingsFields);
  DCMySQLConnection->FreeResult();

  for(size_t iRow= 0; iRow < drSettings.size();iRow++)
    {
      for(size_t iField = 1;iField<drSettingsFields.size();iField++)
	{
	  if(!boost::algorithm::iequals(drSettings[iRow][iField],"NULL"))
	    {
	      xmlNewTextChild(drsettingsNode,NULL,
			      BAD_CAST (drSettingsFields[iField]).c_str(),
			      BAD_CAST (drSettings[iRow][iField]).c_str());
	    }
	}
    }

  return true;
}

//Maybe this is not quite the right function name

bool DecisionMaker_XML::GetPMTS(xmlNode * node,std::string pcName)
{
  std::stringstream query;
  query << "SELECT zs_nsamp_nlbk,zs_nsamp_nlfwd,channel_type from Settings_PMTs where run_number=" << runNumber;
  enum {ZS_NLBK,ZS_NLFWD,TYPE};
  std::vector<std::vector<std::string> > pmtData;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }
  DCMySQLConnection->GetTable(pmtData);
  DCMySQLConnection->FreeResult();
  for(size_t iRow = 0; iRow < pmtData.size();iRow++)
    {
      xmlNewTextChild(node,NULL,
		      (xmlChar*) "zlePresamples",
		      (xmlChar*) pmtData[iRow][ZS_NLBK].c_str());
     xmlNewTextChild(node,NULL,
		      (xmlChar*) "zlePostsamples",
		      (xmlChar*) pmtData[iRow][ZS_NLBK].c_str());
     xmlNewTextChild(node,NULL,
		      (xmlChar*) "channelType",
		      (xmlChar*) pmtData[iRow][TYPE].c_str());      
    }
  return true;
}
