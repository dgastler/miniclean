/*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/

/*Waveform.h is the header file for the Waveform class and the sZLEWindow struct used in CLEANViewer. The source code for this class is contained in the Waveform.cpp file*/

//C++ Libraries
#include <iostream>
#include <string>
#include <algorithm>
#include <math.h>
#include <vector>
#include <fstream>
#include <cstdlib>

#ifndef __WAVEFORM__
#define __WAVEFORM__

typedef struct sZLEwindow{
  std::vector <uint16_t> ADCs, Samples, CalibSamples;
  std::vector <double> QCalib;
}sZLEwindow;

typedef struct sTransformZLEwindow{
  std::vector <double> ADCs, Samples ;
}sTranformZLEwindow;

typedef struct sTransformCalibwindow{
  std::vector<double> QCalib, CalibSamples;
}sTransformCalibwindow;

class Waveform
{
 private:
  //bool to set debug mode on/off
  bool                   DEBUG;

  //min and max variables
  double                 maxADC, minADC, maxQCalib,minQCalib;
  int                    maxSample, minSample, maxCalibSample, minCalibSample;

  //Associated PMT ID for RawBlock waveform
  int                    ID;

  //vectors for raw and transformed ZLE windows
  std::vector <sZLEwindow> zleWindows;
  std::vector <sTransformZLEwindow> transformZLEWindows;
  std::vector <sTransformCalibwindow> transformCalibWindows;

  //Total Charge
  double QTotal;
  
  //private function for finding the mins and maxes for a waveform
  void                   FindMaxADC();
  void                   FindMinADC();
  void                   FindMaxSample(bool calib);
  void                   FindMinSample(bool calib);
  void                   FindMaxQCalib();
  void                   FindMinQCalib();

 public:
  //all functions for the Waveform class are in Waveform.cpp
  
  //Waveform constructors and destructor
  Waveform(sZLEwindow Raw,bool _DebugSwitch);
  Waveform(sZLEwindow Raw,bool _DebugSwitch, int id);
  ~Waveform();
  void Process();
  //functions for seting waveform variables
  void                    SetID(int id);
  void                    SetQTotal(int qTotal);
  //functions for returning waveform variables
  int                     GetID();
  double                  GetMinSample();
  double                  GetMaxSample();
  double                  GetMinSample(bool calib);
  double                  GetMaxSample(bool calib);
  double                  GetMinADC();
  double                  GetMaxADC();
  double                  GetQTotal();
  double                  GetMaxQCalib();
  double                  GetMinQCalib();
  std::vector <sZLEwindow> GetWindows();
  std::vector <sTransformZLEwindow> GetTransform();
  std::vector <sTransformCalibwindow> GetCalibTransform();
 
  //function for adding  a ZLE window into the waveform
  void                    AddZLE(sZLEwindow Raw);

  bool                    InsertZLE(sZLEwindow window, size_t index);
  //Transform Function
  void                    TransformRaw(int canvasMinX, 
				       int canvasMaxX,
				       int nPlots,
				       int whichPMT);
  void                    TransformRaw(int canvasMinX, 
				       int canvasMaxX,
				       int canvasMinY,
				       int canvasMaxY,
				       int nPlots,
				       int whichPMT);
  void                    TransformCalib(int canvasMinX, 
				       int canvasMaxX,
				       int nPlots,
				       int whichPMT);
  void                    TransformCalib(int canvasMinX, 
				       int canvasMaxX,
				       double canvasMinY,
				       double canvasMaxY,
				       int nPlots,
				       int whichPMT);   

 

};

#endif
