#ifndef __RUNINFO__
#define __RUNINFO__

namespace RunInfo
{
  struct sRunInfo_001
  {
    uint32_t size_word;
    uint32_t version;
    
    //Main run information
    int32_t run_number;
    uint32_t run_time;
    size_t xml_size;
    char * run_xml;
    size_t text_size;
    char * run_text;
    uint8_t run_type;
    
    //Convenience 
    uint32_t run_presamples;
    uint32_t run_start_prompt;
    uint32_t run_end_prompt;

   
    
    sRunInfo_001()
    {
      size_word = 0x55000000;
      version = 0;
      xml_size = 0;
      text_size = 0;
      run_xml = NULL;
      run_text = NULL;
      run_number = -2;
      run_type = -1;
      run_time = 0;
      run_presamples = 0;
      run_start_prompt = 0;
      run_end_prompt = 0;
    }

    std::vector<uint8_t> StreamData()
    {
      int pos = 0;
      std::vector<uint8_t> ret;
  
      ret.resize(sizeof(size_word));
      memcpy(&ret[pos],&size_word,sizeof(size_word));
      pos += sizeof(size_word); 

      ret.resize(ret.size()+sizeof(version));
      memcpy(&ret[pos],&version,sizeof(version));
      pos += sizeof(version);
  
      ret.resize(ret.size()+sizeof(run_number));
      memcpy(&ret[pos],&run_number,sizeof(run_number));
      pos += sizeof(run_number);
 
      ret.resize(ret.size()+sizeof(run_time));
      memcpy(&ret[pos],&run_time,sizeof(run_time));
      pos += sizeof(run_time);
  
      ret.resize(ret.size()+sizeof(xml_size));  
      memcpy(&ret[pos],&xml_size,sizeof(xml_size));   
      pos += sizeof(xml_size);
  
      ret.resize(ret.size()+xml_size);
      memcpy(&ret[pos],run_xml,xml_size);
      pos += sizeof(run_xml);
    
      ret.resize(ret.size()+sizeof(text_size));
      memcpy(&ret[pos],&text_size,sizeof(text_size));
      pos += sizeof(text_size);

      ret.resize(ret.size()+text_size);
      memcpy(&ret[pos],run_text,text_size);
      pos += sizeof(run_text);

      ret.resize(ret.size()+sizeof(run_presamples));
      memcpy(&ret[pos],&run_presamples,sizeof(run_presamples));
      pos += sizeof(run_presamples);
   
      ret.resize(ret.size()+sizeof(run_start_prompt));
      memcpy(&ret[pos],&run_start_prompt,sizeof(run_start_prompt));
      pos += sizeof(run_start_prompt);  
  
      ret.resize(ret.size()+sizeof(run_end_prompt));
      memcpy(&ret[pos],&run_end_prompt,sizeof(run_end_prompt));
      pos += sizeof(run_end_prompt);

      return(ret);
    }

    uint32_t * ReadData(uint32_t * data)
    {
      uint8_t * localPointer;
      localPointer = (uint8_t *)(data);
      
      memcpy(&size_word,localPointer,sizeof(size_word));
      localPointer += sizeof(size_word);

      memcpy(&version,localPointer,sizeof(version));
      localPointer += sizeof(version);

      memcpy(&run_number,localPointer,sizeof(run_number));
      localPointer += sizeof(run_number);

      memcpy(&run_time,localPointer,sizeof(run_time));
      localPointer += sizeof(run_time);

      memcpy(&xml_size,localPointer,sizeof(xml_size));
      localPointer += sizeof(xml_size);

      memcpy(&run_xml,localPointer,xml_size);
      localPointer += xml_size;

      memcpy(&text_size,localPointer,sizeof(text_size));
      localPointer += sizeof(text_size);

      memcpy(&run_text,localPointer,text_size);
      localPointer += text_size;
      
      memcpy(&run_type,localPointer,sizeof(run_type));
      localPointer += sizeof(run_type);

      memcpy(&run_presamples,localPointer,sizeof(run_presamples));
      localPointer += sizeof(run_presamples);

      memcpy(&run_start_prompt,localPointer,sizeof(run_start_prompt));
      localPointer += sizeof(run_start_prompt);

      memcpy(&run_end_prompt,localPointer,sizeof(run_end_prompt));
      localPointer += sizeof(run_end_prompt);

      data += (size_word & 0xFFFFFF)/sizeof(uint32_t);
      return(data);
    };
  

  };

 
}

#endif
