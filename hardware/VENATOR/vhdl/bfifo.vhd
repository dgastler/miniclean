-- A 16 byte fifo with independent WR and RD clock domains.
-- Set WR_EN high for one cycle of WR_CLK to cause a value to be written into the FIFO. If
-- FULL was high during that period, it means that the memory was already full and your value
-- was discarded. Set RD_EN high for one cycle of RD_CLK to change the value of DOUT. If 
-- EMPTY goes high immediately after RD_EN set, it means that there are no new values left in
-- the memory to display. EMPTY will subsequently remain high until the next time there are 
-- values available in the memory and three clock cycles have gone by (at which point, the
-- data will update regardless of whether RD_EN is still set high or not).
-- Boston University Electronics Design Facility--Conor DuBois

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bfifo is Port(
	RESET : in STD_LOGIC;
	WR_CLK : in STD_LOGIC;
	DIN : in STD_LOGIC_VECTOR(8 downto 0);
	WR_EN : in STD_LOGIC;
	RD_CLK : in STD_LOGIC;
	RD_EN : in STD_LOGIC;
	DOUT : out STD_LOGIC_VECTOR(8 downto 0);
	EMPTY : out STD_LOGIC;
	FULL : out STD_LOGIC);
end bfifo;

architecture Behavioral of bfifo is
	type mem is array (15 downto 0) of STD_LOGIC_VECTOR(8 downto 0);
	signal memory : mem;
	signal wraddress : unsigned(4 downto 0) := "00000";
	signal rdaddress : unsigned(4 downto 0) := "11111";
	signal iempty : STD_LOGIC_VECTOR(1 downto 0) := "11";
	signal ifull : STD_LOGIC_VECTOR(1 downto 0) := "00";
	signal fextra : STD_LOGIC_VECTOR(1 downto 0) := "00";
	signal eextra : STD_LOGIC_VECTOR(1 downto 0) := "11";
	signal wr_buf : STD_LOGIC := '0';
	signal din_buf : STD_LOGIC_VECTOR(8 downto 0) := "000000000";
	signal counter : unsigned(1 downto 0);
	constant max : unsigned(1 downto 0) := "11";
	constant maxish : unsigned(1 downto 0) := "10";
begin
	--the empty and full flags are created by examining the memory pointers
	--and determining whether rdaddress has caught up or has been lapped
	iempty(0) <= '1' when rdaddress = wraddress  or rdaddress + "00001" = wraddress else '0';
	ifull(0)  <= '1' when rdaddress = (not wraddress(4) & wraddress(3 downto 0)) else '0';
	--to access the memory at the correct times while still being synchronous to
	--both RD_CLK and WR_CLK, we essentially need to know the future state of iempty(1)
	--and ifull(1); as it turns out, it's good enough to predict the future more than 
	--50% of the time: as fextra(1) and eextra(1) will attest
	eextra(0) <= '1' when eextra(1) = '0' and (rdaddress + "00010") = wraddress else '0';
	fextra(0) <= '1' when fextra(1) = '0' and (rdaddress - "00001") = 
		(not wraddress(4) & wraddress(3 downto 0)) else '0';
	FULL <= ifull(1) or fextra(1);
	EMPTY <= '0' when counter = max else '1';
	--first word should "fall through"
	DOUT <= memory(to_integer(rdaddress(3 downto 0)));

	process(WR_CLK, RESET)
	begin
		if RESET = '1' then
			ifull(1) <= '0';
			fextra(1) <= '0';
			wr_buf <= '0';
			din_buf <= '0' & x"00";
			wraddress <= "00000";
			memory(0) <= '0' & x"00";
			memory(1) <= '0' & x"00";
			memory(2) <= '0' & x"00";
			memory(3) <= '0' & x"00";
			memory(4) <= '0' & x"00";
			memory(5) <= '0' & x"00";
			memory(6) <= '0' & x"00";
			memory(7) <= '0' & x"00";
			memory(8) <= '0' & x"00";
			memory(9) <= '0' & x"00";
			memory(10) <= '0' & x"00";
			memory(11) <= '0' & x"00";
			memory(12) <= '0' & x"00";
			memory(13) <= '0' & x"00";
			memory(14) <= '0' & x"00";
			memory(15) <= '0' & x"00";
		elsif WR_CLK'event and WR_CLK = '1' then
			ifull(1) <= ifull(0);
			fextra(1) <= fextra(0);
			wr_buf <= WR_EN;
			din_buf <= DIN;
			if wr_buf = '1' and ifull(1) = '0' and fextra(1) = '0' then
				--write a value to the FIFO when enable high and space available
				memory(to_integer(wraddress(3 downto 0))) <= din_buf;
				wraddress <= wraddress + x"1";
			end if;
		end if;
	end process;

	process(RD_CLK, RESET)
	begin
		if RESET = '1' then
			iempty(1) <= '1';
			eextra(1) <= '1';
			rdaddress <= "11111";
		elsif RD_CLK'event and RD_CLK = '1' then
			iempty(1) <= iempty(0);
			eextra(1) <= eextra(0);
			if iempty(1) = '0' and eextra(1) = '0' then
				if counter = max then
					if RD_EN = '1' then
						--increment the pointer on read operations
						rdaddress <= rdaddress + "00001";
					else
						rdaddress <= rdaddress;
					end if;
					counter <= max;--EMPTY never high after data has changed
				elsif counter = maxish then
					rdaddress <= rdaddress + "00001";
					counter <= max;
				else
					rdaddress <= rdaddress;
					counter <= counter + "01";
				end if;
			else
				rdaddress <= rdaddress;
				if RD_EN = '1' then
					--data is not changing even though client wishes it to
					counter <= "00";
				else
					counter <= counter;
				end if;
			end if;
		end if;
	end process;
end Behavioral;

