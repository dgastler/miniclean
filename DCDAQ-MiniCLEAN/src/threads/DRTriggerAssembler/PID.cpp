/**********************************************************************
* Discrete PID controller implementation.
* Modified to consider the multiplicative separation of the target and measured
* values rather than their additive distance.
* author              Atmel Corporation: http://www.atmel.com
*****************************************************************************/

#include "PID.h"

// Create a PID loop.
PID::PID(uint8_t p_factor, uint8_t i_factor, uint8_t d_factor)
{
   // Start values for PID controller
   sumError = 1.0;
   first_time = true;
   // Tuning constants for PID loop
   P_Factor = (double)p_factor;
   I_Factor = (double)i_factor;
   D_Factor = (double)d_factor;
}


/* PID control algorithm.
*  Calculates output from setpoint, process value and PID status.
*  Output will always be a positive multiplicative factor
*  param setPoint  Desired value.
*  param processValue  Measured value.
*/
double PID::update(uint16_t setPoint, uint16_t processValue)
{
   double error, difference;

   //Calculate Pterm
   error = ((double)setPoint)/((double)processValue);

   //Calculate Iterm
   sumError *= error;
   if(sumError > 10.0)//limit integral runaway
      sumError = 10.0;
   else if(sumError < .01)
      sumError = .01;

   //Calculate Dterm
   if(first_time)
      difference = 1.0;
   else
      difference = ((double)lastProcessValue)/((double)processValue);

   first_time = false;
   lastProcessValue = processValue;

   return (P_Factor*error + I_Factor*sumError + D_Factor*difference)/(P_Factor + I_Factor + D_Factor);
}

/* Resets the integrator.
*  Calling this function will reset the integrator in the PID regulator.
*/
void PID::reset_integrator()
{
   sumError = 1.0;
}

