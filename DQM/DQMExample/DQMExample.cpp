#include <zmq.hpp>
#include <EventStreamHelper.h>
#include <DQMStructs.h>
#include <RAT/DS/EV.hh>
//This code is intended as a tutorial/example of how to use zmq sockets and EventStreamHelper to stream events from DCDAQ.  The main things to understand are how to use zmq sockets, StreamBuffToEV (which does exactly what it says, it's in EventStreamHelper for reference) and the structs in DQMStructs.h (most important! these include the ZMQ::Request struct and the ZMQ::REQ______ enum for example, as well as the StreamFilter struct, which is not explicitly mentioned in this code but is very important, is the struct which controls what events DCDAQ sends to what clients)
// Written 2-19-2013 By Chris Kachulis
bool Running;
zmq::context_t *context;
zmq::socket_t *daqSocketRequest;
zmq::socket_t  *daqSocketData;
RAT::DS::EV * ratEV;
bool SetUpSocket();
bool GetSocketMessage();

int main()
{
  //use one context throughout for all sockets
  context=new zmq::context_t(1);
  //Begin by setting up socket for communication with DAQ
  if(!SetUpSocket())
    return 0;

  Running=true;
  ratEV=NULL;
  //this is mainloop
  while(Running)
    {
      //first we need to check if there are any messages to be read
      int zmq_events;
      size_t zmq_events_size=sizeof(zmq_events);
      daqSocketData->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
      
      if(zmq_events & ZMQ_POLLIN)
	{
	  //This means there is stuff to read
	  Running=GetSocketMessage();

	  if(Running)
	    {
	      //If we got something, let's print something
	      fprintf(stderr,"We got Event # %i from DCDAQ \n",ratEV->GetEventID());

	    }

	}
    }
  //Finished running, time to shutdown
  daqSocketRequest->close();
  delete daqSocketRequest;
  daqSocketData->close();
  delete daqSocketData;
  delete context;
  delete ratEV;
  
  return 0;

}

bool SetUpSocket()
{
  //Sets up Socket to recieve messages/data from DCDAQ.  First must make socket to reques port assignment from DCDAQ, the setup socket on assigned port.  You would also send other streaming requests to daqSocketRequest after setup (adjusting your ZMQ::Request accordingly).  See CLEANViewer::SendFilterRequest()and similar code nearby in CLEANViewer_Signals.cpp for example.

  //first make daqSocketRequest.  This socket connects to Reply socket on DCDAQ and requests a port to connect to for data
  daqSocketRequest=new zmq::socket_t(*context,ZMQ_REQ);
  daqSocketRequest->connect("tcp://localhost:20000"); //give adress and port of DCDAQ request port, set in xml for DQMSender
  //create are request port message, 
  ZMQ::Request * zmq_req=new ZMQ::Request();
  zmq_req->req=ZMQ::REQPORT;
  
  //create a zmq message to mem copy our request port to (zmq sockets send/recv zmq messages.  the ZMQ::Request is a struct which DCDAQ can understand, and in this case reads to see that we are asking for a port)
  zmq::message_t message(sizeof(ZMQ::Request));
  memcpy(message.data(),zmq_req,sizeof(ZMQ::Request));

  //send the message
  daqSocketRequest->send(message);
  //Cleanup, delete request
  delete zmq_req;

  //now create zmq message to recieve response (response will be assigned port number)
  zmq::message_t response;
  daqSocketRequest->recv(&response); //will block here until DCDAQ is ready and replies

  ZMQ::MSG msg;
  memcpy(&msg,response.data(),sizeof(ZMQ::MSG));
  
  if(msg!=ZMQ::MSGRECVDGOOD)
    {
      //Message we sent wasn't good, let's see what was wrong
      if(msg==ZMQ::MSGWRNGVERSION)
	{
	  fprintf(stderr,"Wrong ZMQ Version.  Please svn update DCDAQ, and recompile DCDAQ and CLEANViewer \n");
	  return false;
	}
      else if(msg==ZMQ::MSGBADMAGIC)
	{
	  fprintf(stderr,"BAD Magic word sent to DCDAQ \n");
	  return false;
	}
      else
	{

	  fprintf(stderr,"ZMQ Messaging error \n");
	  return false;
	}
    }

  //otherwise, following data is dataPort
  int dataPort;
  memcpy(&dataPort,(uint8_t*)response.data()+sizeof(ZMQ::MSG),sizeof(int));
 
  //create string for address of socket
  char * dataEndpoint=new char[100];
  sprintf(dataEndpoint,"tcp://localhost:%i",dataPort); //again local host replaced by host of DCDAQ session

  //now create daqSocketData.  This is the socket where the evs (or whatever) are actually transferred
  daqSocketData=new zmq::socket_t(*context,ZMQ_PULL);
  daqSocketData->connect(dataEndpoint);
  delete dataEndpoint;

  return true;
}


bool GetSocketMessage()
{
  //Gets message/data from data Socket.  depending on message type this is either a Stop or and Event.  Other types of messages should only be sent to client as responses to requests.
  zmq::message_t message;
  daqSocketData->recv(&message);

  //the message will begin with an int specifying the type of message
  int msg_type;
  memcpy(&msg_type,message.data(),sizeof(int));
  
  if(msg_type==ZMQ::MSGSTOP)
    {
      //DCDAQ is shutting down, so you should too
      return false;

    }
  else if(msg_type==ZMQ::MSGEVENT)
    {
      //DCDAQ Sent us an event.  clear out ratEV and prepare
      delete ratEV;
      ratEV=new RAT::DS::EV();
      size_t data_size=message.size()-sizeof(int); //everything in the message except that first int
      uint8_t* message_data=new uint8_t[data_size];
      memcpy(message_data,(uint8_t*)message.data()+sizeof(int),data_size);

      //convert buffer into ev
      StreamBufftoEV(message_data,ratEV);
      return true;
    }
  else
    {
      //this shouldn't happen.this would be problem
      return false;
    }

  
}





