#include <PacketBuilder.h>

bool PacketBuilder::CheckBoardSize_CHAN_ZLE_INTEGRAL(RATEVBlock * event,RAT::DS::Board * board)
{
  //Calculate the size for this RAT::DS::Board in EBPacket structures
  uint32_t boardSize = sizeof(EBPacket::Board);
  int channelCount = board->GetChannelCount();
  for(int iChannel = 0; iChannel < channelCount;iChannel++)
    {
      boardSize += sizeof(EBPacket::Channel);      
      RAT::DS::Channel * channel = board->GetChannel(iChannel);
      int rawBlockCount = channel->GetRawBlockCount();
      for(int iRawBlock = 0; iRawBlock < rawBlockCount; iRawBlock++)
	{
	  boardSize += sizeof(EBPacket::IntegralBlock);
	}
    }

  //See if we have room
  if((boardSize > dataSizeLeft) || packet==NULL)
    {
      return CheckPacket(boardSize,event);
      //return true;
    }
  return true;
}

void PacketBuilder::AddChannel_CHAN_ZLE_INTEGRAL(RAT::DS::Channel * channel, int16_t /*PMTID*/)
{
  //Add channel
  EBPacket::Channel * ebChannel = (EBPacket::Channel *) currentPtr;
  currentPtr += sizeof(EBPacket::Channel);
  dataSizeLeft -= sizeof(EBPacket::Channel);
  ebChannel->dataBlockCount = channel->GetRawBlockCount();
  for(int iRawBlock = 0; iRawBlock < ebChannel->dataBlockCount;iRawBlock++)
    {
        //Add channel
      EBPacket::IntegralBlock * ebIntegral = (EBPacket::IntegralBlock *) currentPtr;
      currentPtr += sizeof(EBPacket::IntegralBlock);
      dataSizeLeft -= sizeof(EBPacket::IntegralBlock);
      RAT::DS::RawBlock * rawBlock = channel->GetRawBlock(iRawBlock);
      ebIntegral->integral    = rawBlock->GetIntegral();
      ebIntegral->startTime   = rawBlock->GetOffset();
      ebIntegral->width       = rawBlock->GetWidth();      
    }
}
