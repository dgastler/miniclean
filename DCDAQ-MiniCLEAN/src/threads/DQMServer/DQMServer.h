#ifndef __DQMServer__
#define __DQMServer__

#include <DCThread.h>
#include <RATCopyQueue.h>
#include <xmlHelper.h>
#include <RAT/DS/Root.hh>
#include <cstdio>
#include <zmq.hpp>
#include <zmq.h>
#include <RAT/DS/DCDAQ_DQMStructs.hh>
#include <sstream>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>


#include <netHelper.h>
#include <map>
#include <sys/time.h>
#include <RAT/DS/DCDAQ_RAWDataStructs.hh>
#include "WriteTime.h"
#include "RAT/DS/DCDAQ_EventStreamHelper.hh"
#include <boost/algorithm/string.hpp>
#define PERM_FILE   (S_IRUSR | S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH| S_IWOTH)

class DQMServer : public DCThread 
{
 public:
  DQMServer(std::string Name);
  ~DQMServer()
    {
         };
  virtual bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  virtual void PrintStatus(){
    std::stringstream status;
    pid_t tid=syscall(SYS_gettid);
    status<<"id "<<tid;
    Print(status.str().c_str());
    DCThread::PrintStatus();
  }
  virtual void ThreadCleanUp()
{
  //send stop message to all
  zmq::message_t message(sizeof(int));
  int msg=ZMQ::MSGSTOP;
  memcpy(message.data(),&msg,sizeof(int));
  daqServerPush->send(message,ZMQ_NOBLOCK);
  daqServerPush->close();
  delete daqServerPush;
  daqServerPush=NULL;
  daqServerRep->close();
  delete daqServerRep;
  daqServerRep=NULL;
  delete context;
}

  RATCopyQueue * Mem;
  EventToPorts * eventPorts;
  RAT::DS::EV * Event;

 
  int portPush;
  int portRep;
  int zmqfdPush;
  int zmqfdRep;
  zmq::context_t * context;
  zmq::socket_t * daqServerPush;
  zmq::socket_t * daqServerRep;
  ClientMessage::header head;

 
  //  timeval timeV;
 
};


#endif
