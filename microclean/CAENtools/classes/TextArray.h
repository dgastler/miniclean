#ifndef ROOT_TextArray
#define ROOT_TextArray

#include <TObject.h>
#include <iostream>

using namespace std;

class TextArray : public TObject
{

public:
  TextArray(){IsValid = false; N = 0; array=0;};
  TextArray(Int_t n,Char_t * text){IsValid = false;FillArray(n,text);};
  void FillArray(Int_t n, Char_t * text);
  //  Char_t GetChar(Int_t i){if(i>0 && i < N) {return(array[i]);}return(-1);};
  Char_t *GetArray(){return(array);};

  ~TextArray(){N = 0;array=0;};//DeleteArray();};

private:
  Bool_t IsValid;
  Int_t N;
  Char_t * array; //[N]

  //  void DeleteArray();
  void AddFill(Int_t n, Char_t *text);
  ClassDef(TextArray,1) 
};

#endif
