#ifdef __RATDS__
#ifndef __DREBBRIDGE__
#define __DREBBRIDGE__
/*
  Thread description
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <DRPCRatManager.h>
#include <RATDiskBlockManager.h>

#include <map>

class DREBBridge : public DCThread 
{
 public:
  DREBBridge(std::string Name);
  ~DREBBridge(){};

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);  

  virtual void MainLoop();
 private:

  //==============================================================
  //DCThread 
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);  
  virtual void ProcessTimeout();

  //==============================================================
  //Memory manager interface
  //==============================================================
  DRPCRATManager * DRPCmem;
  RATDiskBlockManager * RATDiskmem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  uint32_t bridgeCount;
  
};
#endif
#endif
