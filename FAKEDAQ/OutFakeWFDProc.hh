#ifndef __OUTFAKEWFDProc___
#define __OUTFAKEWFDProc___

#include <RAT/Processor.hh>
#include <RAT/DS/Run.hh>
#include <RAT/Log.hh>

#include <DAQTestData.h>
#include <DAQFile.h>
#include <FakeWFD.h>

#include <vector>
#include <string>
#include <map>

namespace RAT 
{
  class OutFakeWFDProc : public Processor 
  {
  public:
    static int run_num;
    OutFakeWFDProc();
    virtual ~OutFakeWFDProc();
    
    virtual void SetS(std::string param, std::string value);
    
    virtual void SetI(std::string param, int value);
    
    virtual Processor::Result DSEvent(DS::Root *ds);

  protected:
    std::string default_filename;
    std::string filename;

    std::map< std::string , std::map<int, std::vector<int> > > PMTID;
    
    DAQTestData * fakeDataBuilder;
    DAQTestData * SetupDataBuilder();
    
    uint32_t writeSize;
  };

} // namespace RAT

#endif
