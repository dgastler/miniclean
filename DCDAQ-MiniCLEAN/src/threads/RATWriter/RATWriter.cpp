#ifdef __RATDS__
#include <RATWriter.h>

RATWriter::RATWriter(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATWriter"));

  //EBPC memorymanager
  EBPC_mem = NULL;
  
  //statistics
  lastTime = time(NULL);
  currentTime = time(NULL);
  
  timeStepProcessedEvents = 0;

  SubRunNumber = 1; //Starting subrun ID
  SubRunNumberMax = 999; //Maximum number of sub runs.
  SubRunMaxEvents = 1000; //Default events per subrun.
  
  EventNumber = 1; //Starting event number
  SubEventNumber = 1; //Starting sub-event number

  fileReady = false;
  OutFile = NULL;
}

RATWriter::~RATWriter()

{
  CloseFile();
}

bool RATWriter::Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager)
{
 
  //================================
  //Get the EBPC memory manager
  //================================
  if(!FindMemoryManager(EBPC_mem,SetupNode,MemManager))
    {
      return(false);
    }
  //Get the autosave value for the root file (Bytes)
  if(FindSubNode(SetupNode,"AUTOSAVE") == NULL)
    {
      Autosave = 1024*1024; //Default autosave size (Bytes)
      PrintWarning("Using default autosave value");
    }
  else    
    GetXMLValue(SetupNode,"AUTOSAVE",GetName().c_str(),Autosave);

  //Get the subrun event count value for the root files
  if(FindSubNode(SetupNode,"SUBRUNEVENTS") == NULL)
    {
      SubRunMaxEvents = 10000;
      PrintWarning("Using default value for subrun event count");
    }
  else
    GetXMLValue(SetupNode,"SUBRUNEVENTS",GetName().c_str(),SubRunMaxEvents);
  
  
  //Get the max events value for this run. 
  if(FindSubNode(SetupNode,"EVENTS") == NULL)
    {
      //      MaxRunEvents = 100000; //Default max number of events.
      MaxRunEvents = 0; //Default max number of events.
      PrintWarning("Using default value for total run events");
    }
  else
    GetXMLValue(SetupNode,"EVENTS",GetName().c_str(),MaxRunEvents);

  //Get the compression level for this root file
  if(FindSubNode(SetupNode,"COMPRESSION") == NULL)
    {
      CompressionLevel = 2; //Default max number of events.
      PrintWarning("Using default value for compression level");
    }
  else
    GetXMLValue(SetupNode,"COMPRESSION",GetName().c_str(),CompressionLevel);

  //Get the buffer size for the branches
  if(FindSubNode(SetupNode,"BRANCHBUFFER") == NULL)
    {
      BranchBufferSize = 32000; //Default max number of events.
      PrintWarning("Using default value for branch buffer size");
    }
  else
    GetXMLValue(SetupNode,"BRANCHBUFFER",GetName().c_str(),BranchBufferSize);

  //Get the output directory
  if(FindSubNode(SetupNode,"OUTPUTDIRECTORY") == NULL)
    {
      outputDirectory = "";
      PrintWarning("Writing output to current directory");
    }
  else
    {
      GetXMLValue(SetupNode,"OUTPUTDIRECTORY",GetName().c_str(),outputDirectory);
      char * lastchar = &outputDirectory[outputDirectory.size()-1];
      std::string test = "/";
      if(strcmp(lastchar,test.c_str())!=0)
	{
	  outputDirectory.append("/");
	}
    }

  //Get the name of this RATWriter thread
  GetXMLValue(SetupNode,"NAME",GetName().c_str(),ThreadName);

  Ready = true;
  return(true);
}
#endif
