#ifdef __RATDS__
#include <EBPCAssembler.h>

void EBPCAssembler::DeleteConnection(size_t iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
 

  //Remove the incoming packet manager fds from list
  RemoveReadFD(server[iConn].GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(server[iConn].GetMessageFD());          

  SetupSelect();
  server.DeleteConnection(iConn);
}

bool EBPCAssembler::ProcessNewConnection()
{
  //create a DCNetThread for this 
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().GetFullPacketFD());
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().GetMessageFD());      
      printf("EBPC connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().GetRemoteAddress().GetAddrStr().c_str());
      SetupSelect();
    }
 else
   {
     return false;
   }
  return true;
}
#endif
