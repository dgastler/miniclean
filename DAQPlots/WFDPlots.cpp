#include "WFDPlots.h"

void MakeReadOutRatePlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  Color_t plotColors[] = {1,2,3,4,5,6,7};
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  const int PlotCount = 4;
  TGraph * grRORate[PlotCount];
  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);
  //  TLegend * leg = new TLegend(0.15,0.6,0.55,0.85);
  TLegend * leg = new TLegend(0.6,0.15,0.85,0.55);
  leg->SetBorderSize(0);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");

  double maxHeight = 0;
  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      //Grab data from the SQL tables and build a TGraph
      ss << "select time,value from sc_sens_FE" << iGr+1 << "RORTWFD_0" << iGr+1 << " where time > "
	 << start << " and time <" << end;
      GetXYData(conn,ss.str(),time, val);
      ReZeroTimeVector(val,0,0.001);
      ReZeroTimeVector(time,start,1.0/3600);
      grRORate[iGr] = new TGraph(time.size(),&time[0],&val[0]);      
      ss.str("");

      //Set TGraph attributes
      grRORate[iGr]->SetLineColor(plotColors[iGr]);
      grRORate[iGr]->SetMarkerColor(plotColors[iGr]);
      grRORate[iGr]->SetMarkerStyle(33);
      grRORate[iGr]->SetMinimum(0);
      grRORate[iGr]->GetYaxis()->SetTitle("WFD Read-out rate (khz)");
      grRORate[iGr]->GetXaxis()->SetTitle("Run time (hr)");
      ss << "WFD " << iGr <<" RO rate(khz)";
      leg->AddEntry(grRORate[iGr],ss.str().c_str(),"PL");
      ss.str("");
      if(maxHeight < FindTGraphMax(grRORate[iGr]))
	{
	  maxHeight = FindTGraphMax(grRORate[iGr]);
	}
    }

  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      grRORate[iGr]->GetYaxis()->SetRangeUser(0,maxHeight*1.1);
      if(!iGr)
	{
	  grRORate[iGr]->Draw("APL");
	}
      else
	{
	  grRORate[iGr]->Draw("PLSAME");
	}
    }
  leg->Draw("SAME");
  c1->Print("WFD_RO_Rate.pdf");
  delete c1;
  for(int i = 0; i < PlotCount;i++)
    {
      delete grRORate[i];
    }  
}
void MakeDataRatePlot (MYSQL * conn,int runNumber,time_t start,time_t end)
{
  Color_t plotColors[] = {1,2,3,4,5,6,7};
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  const int PlotCount = 4;
  TGraph * grDataRate[PlotCount];
  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);
  TLegend * leg = new TLegend(0.6,0.15,0.85,0.55);
  leg->SetBorderSize(0);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  double maxHeight = 0;
  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      //Grab data from the SQL tables and build a TGraph
      ss << "select time,value from sc_sens_FE" << iGr+1 << "DRTWFD_0" << iGr+1 << " where time > "
	 << start << " and time <" << end;
      GetXYData(conn,ss.str(),time, val);
      ReZeroTimeVector(val,0,1.0/(1024*1024.0));
      ReZeroTimeVector(time,start,1.0/3600);
      grDataRate[iGr] = new TGraph(time.size(),&time[0],&val[0]);      
      ss.str("");

      //Set TGraph attributes
      grDataRate[iGr]->SetLineColor(plotColors[iGr]);
      grDataRate[iGr]->SetMarkerColor(plotColors[iGr]);
      grDataRate[iGr]->SetMarkerStyle(33);
      grDataRate[iGr]->SetMinimum(0);
      grDataRate[iGr]->GetYaxis()->SetTitle("WFD data rate (MBps)");
      grDataRate[iGr]->GetXaxis()->SetTitle("Run time (hr)");

      if(maxHeight < FindTGraphMax(grDataRate[iGr]))
	{
	  maxHeight = FindTGraphMax(grDataRate[iGr]);
	}
      ss << "WFD " << iGr <<" data rate(MBps)";
      leg->AddEntry(grDataRate[iGr],ss.str().c_str(),"PL");
      ss.str("");
    }
  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      grDataRate[iGr]->GetYaxis()->SetRangeUser(0,maxHeight*1.1);
      if(!iGr)
	{
	  grDataRate[iGr]->Draw("APL");
	}
      else
	{
	  grDataRate[iGr]->Draw("PLSAME");
	}
    }
  leg->Draw("SAME");
  c1->Print("WFD_Data_Rate.pdf");
  delete c1;
  for(int i = 0; i < PlotCount;i++)
    {
      delete grDataRate[i];
    }
}
void MakeOccupancyPlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  Color_t plotColors[] = {1,2,3,4,5,6,7};
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  const int PlotCount = 4;
  TGraph * grOccupancy[PlotCount];
  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);
  TLegend * leg = new TLegend(0.6,0.15,0.85,0.55);
  leg->SetBorderSize(0);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  double maxHeight = 0;
  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      //Grab data from the SQL tables and build a TGraph
      ss << "select time,value from sc_sens_FE" << iGr+1 << "OCCWFD_0" << iGr+1 << " where time > "
	 << start << " and time <" << end;
      GetXYData(conn,ss.str(),time, val);
      ReZeroTimeVector(time,start,1.0/3600);
      grOccupancy[iGr] = new TGraph(time.size(),&time[0],&val[0]);      
      ss.str("");

      //Set TGraph attributes
      grOccupancy[iGr]->SetLineColor(plotColors[iGr]);
      grOccupancy[iGr]->SetMarkerColor(plotColors[iGr]);
      grOccupancy[iGr]->SetMarkerStyle(33);
      grOccupancy[iGr]->SetMinimum(0);
      grOccupancy[iGr]->GetYaxis()->SetTitle("WFD Occupancy");
      grOccupancy[iGr]->GetXaxis()->SetTitle("Run time (hr)");

      if(maxHeight < FindTGraphMax(grOccupancy[iGr]))
	{
	  maxHeight = FindTGraphMax(grOccupancy[iGr]);
	}
      ss << "WFD " << iGr <<" occupancy";
      leg->AddEntry(grOccupancy[iGr],ss.str().c_str(),"PL");
      ss.str("");
    }
  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      grOccupancy[iGr]->GetYaxis()->SetRangeUser(0,maxHeight*1.1);
      if(!iGr)
	{
	  grOccupancy[iGr]->Draw("APL");
	}
      else
	{
	  grOccupancy[iGr]->Draw("PLSAME");
	}
    }
  leg->Draw("SAME");
  c1->Print("WFD_Occupancy.pdf");
  delete c1;
  for(int i = 0; i < PlotCount;i++)
    {
      delete grOccupancy[i];
    }
}
void MakeEventRatePlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  Color_t plotColors[] = {1,2,3,4,5,6,7};
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  const int PlotCount = 4;
  TGraph * grEventRate[PlotCount];
  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);
  TLegend * leg = new TLegend(0.6,0.15,0.85,0.55);
  leg->SetBorderSize(0);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");

  double maxHeight = 0;
  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      //Grab data from the SQL tables and build a TGraph
      ss << "select time,value from sc_sens_FE" << iGr+1 << "ERTWFD_0" << iGr+1 << " where time > "
	 << start << " and time <" << end;
      GetXYData(conn,ss.str(),time, val);
      ReZeroTimeVector(val,0,0.001);
      ReZeroTimeVector(time,start,1.0/3600);
      grEventRate[iGr] = new TGraph(time.size(),&time[0],&val[0]);      
      ss.str("");

      //Set TGraph attributes
      grEventRate[iGr]->SetLineColor(plotColors[iGr]);
      grEventRate[iGr]->SetMarkerColor(plotColors[iGr]);
      grEventRate[iGr]->SetMarkerStyle(33);
      grEventRate[iGr]->SetMinimum(0);
      grEventRate[iGr]->GetYaxis()->SetTitle("WFD event rate (khz)");
      grEventRate[iGr]->GetXaxis()->SetTitle("Run time (hr)");
      ss << "WFD " << iGr <<" event rate(khz)";
      leg->AddEntry(grEventRate[iGr],ss.str().c_str(),"PL");
      ss.str("");
      if(maxHeight < FindTGraphMax(grEventRate[iGr]))
	{
	  maxHeight = FindTGraphMax(grEventRate[iGr]);
	}
    }

  for(int iGr = 0; iGr < PlotCount; iGr++)
    {
      grEventRate[iGr]->GetYaxis()->SetRangeUser(0,maxHeight*1.1);
      if(!iGr)
	{
	  grEventRate[iGr]->Draw("APL");
	}
      else
	{
	  grEventRate[iGr]->Draw("PLSAME");
	}
    }
  leg->Draw("SAME");
  c1->Print("WFD_Event_Rate.pdf");
  delete c1;
  for(int i = 0; i < PlotCount;i++)
    {
      delete grEventRate[i];
    }  
}
