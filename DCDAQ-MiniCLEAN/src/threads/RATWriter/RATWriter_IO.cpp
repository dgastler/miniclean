#ifdef __RATDS__
#include <RATWriter.h>

bool RATWriter::OpenFile(uint32_t runNumber)
{
  bool ret = true;

  //Check for the special case of being out of subruns
  if(SubRunNumber > SubRunNumberMax)
    {
      DCMessage::Message message;      
      //Send a message back to the master and stop the inner run loop
      printf("Max sub runs reached. Shutting down DAQ\n");
      message.SetType(DCMessage::STOP);
      //      SendMessageOut(message,true);
      SendMessageOut(message);
      return(false);
    }

  const size_t bufferSize =200;
  char buffer[bufferSize];

  //Construct the current filename
  snprintf(buffer,bufferSize,
	   "%srun_%06d_%05d.root",
	   outputDirectory.c_str(),
	   runNumber,
	   SubRunNumber); 
  OutFile = TFile::Open(buffer,"RECREATE","",CompressionLevel);
  if(!OutFile)
    {
      ret = false;
    }
  else
    {
      //Store the new filename
      OutFileName.assign(buffer);
      std::string newBuffer("Opening file: ");
      newBuffer += OutFileName;
      PrintWarning(newBuffer);

      Tree = new TTree("T", "RAT Tree"); // create a new TTree


      //Make a branch in the TTree for the branchDS
      RAT::DS::Root * TemporaryBranch = new RAT::DS::Root;
      Tree->Branch("ds", TemporaryBranch->ClassName(), &BranchDS, BranchBufferSize, 99);
      delete TemporaryBranch;
      Tree->SetAutoSave(Autosave);

      //Set the subevent number to 1
      SubEventNumber = 1;
      ret = true;
    }
  if(ret)
    {
      failedFileOpens = 0;
    }
  return ret;
}

bool RATWriter::CloseFile()
{
  bool ret = false;
  if(OutFile)
    {  
      const size_t bufferSize = 200;
      char buffer[bufferSize];
      snprintf(buffer,bufferSize,"Closing file: %s\n",OutFileName.c_str());
      PrintWarning(buffer);

      OutFile->cd();  //Make sure we are in the correct ROOT directory
      //Write trees
      Tree->Write();
      //Write files and close
      OutFile->Write();
      OutFile->Close();
      //Reset our pointers (we can't delete these, but I don't knwo why)
      Tree = NULL;
      delete OutFile;
      OutFile = NULL;
      ret = true;
    }
  return ret;
}

#endif
