#include "v1720Event.h"

v1720Event::v1720Event()
{
  rawData = 0;
  size = 0;
  header = 0;
  ZLE = false;
  twoPack = false;  
}

v1720Event::~v1720Event()
{
  Channels.clear();
  rawData = 0;
  size = 0;
  header = 0;
  ZLE = false;
  twoPack = false;  
}

char * v1720Event::ProcessEvent(char * _rawData,uint32_t _rawSizeMax,bool _twoPack)
{
  char * ret = NULL;
  Channels.clear();
  //Recast the input char data to uint32_t
  rawData = (uint32_t *) _rawData;
  rawSizeMax = _rawSizeMax >> 2; //divide by four since the input is a char pointer and we now have a uint32_t
  size = _rawSizeMax >> 2; //divide by four since the input is a char pointer and we now have a uint32_t
  twoPack = _twoPack;
  uint32_t position = 0;


  if(ProcessHeader() == 0)  
    //process the event header and proceed if it returns 0
    {
      //move past the header
      position+=4;
      //loop over all of the possible eight channels and
      //add a v1720EventChannel if that channel was digitized.
      for(uint8_t channelID = 0; ((channelID < 8) && (position < size)) ; channelID++)
	{
	  //	  if(channelMask & (0x1 << channelID))
	  if(0x1 & ( channelMask >> channelID))
	    {
	      if(ZLE)
		{
		  //Process ZLE event
		  ProcessZLE(channelID,position);
		}
	      else
		{
		  //Process a full waveform event.
		  ProcessFULL(channelID,position);
		}			       
	    }
	}
      //Finished processing channels, now we need to see if we are at the last
      //event for this rawData array. To do this we check if position is still 
      //less than rawSizeMax.  If there is more data to process return the next
      //pointer.  If there isn't data return NULL
      if(position < rawSizeMax)
	{
	  ret = (char *) (rawData + position);
	}
      else
	{
	  ret = NULL;
	}
    }
  
  return(ret);
}

int v1720Event::ProcessHeader()
{
  int ret = 0;
  //check if the size is big enought for there to be a header(4xuint32_t = 16bytes)
  if(rawSizeMax > 4)
    {
      //Size big enough for a header.
      //Recast rawd data to uint32_t and assign the header.
      header = rawData;
      //Check if the header is formatted correctly by looking 
      //for 0xA******* in the header.
      if((header[0]&0xF0000000) == 0xA0000000)
	{
	  //The format of this header can be found in the v1720's manual
	  //Look up the bit offsets/masks there.

	  //set this event's size
	  size = header[0]&0xFFFFFF;
	  
	  //set board ID (bits 31-27) of header[2];
	  boardID = header[1] >> 27;
	  
	  //Set ZLE bool
	  ZLE = 0x1 & (header[1] >> 24);
	  
	  //Get the pattern on the V1720's front panel
	  IOPattern = 0xFFFF&(header[1] >> 8);
	  
	  //Get the channel mask
	  channelMask = 0xFF&(header[1]);
	  //Count the bits of the channel mask to find the number of channels.
	  numberOfChannels = BitSum(channelMask);
	  
	  //get the CAEN event number for this event
	  eventNumber = (header[2])&0xFFFFFFF;

	  //get the CAEN time for this event
	  baseTime = header[3];
	}
      else
	{
	  fprintf(stderr,"ERROR: Incorrect header value.\n");
	  ret++;
	}
    }
  else
    {
      fprintf(stderr,"ERROR:rawData is too small to be an event! %d bytes.\n",size);
      ret++;
    }
  return(ret);
}

int v1720Event::ProcessZLE(uint8_t channelID, uint32_t &position)
{
  int ret = 0;
  //Find out how big this channel is in 32bit words
  //subtract one for the size header.
  uint32_t channelSize = rawData[position]-1;
  v1720EventChannel channel;
  //Process this channel.  The data is of size channel size
  //and it starts at rawData + position +1.
  channel.ProcessChannel((rawData + position + 1),
			 channelSize,
			 ZLE,
			 twoPack,
			 channelID);
  //Add this channel to the event.
  Channels.push_back(channel);
  //Move to the next channel position.
  position+= channelSize+1;
  return(ret);
}
int v1720Event::ProcessFULL(uint8_t channelID, uint32_t &position)
{
  int ret = 0;
  //Find out how big this channel is in 32bit words
  uint32_t channelSize = (size-4)/(numberOfChannels);
  v1720EventChannel channel;
  //Process this channel.  The data is of size channel size
  //and it starts at rawData32 + position32.
  channel.ProcessChannel((rawData + position),
			 channelSize,
			 ZLE,
			 twoPack,
			 channelID);
  //Add this channel to the event.
  Channels.push_back(channel);
  //Move to the next channel position.
  position+= channelSize;
  return(ret);
}

uint8_t v1720Event::BitSum(uint32_t data)
{
  uint8_t ret = 0;
  for(int bit = 0; bit < 32;bit++)
    {
      ret+= 0x1 & (data >> bit);
    }
  return(ret);
}
