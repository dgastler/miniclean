#ifndef __DCVECTORENTRY__
#define __DCVECTORENTRY__

#include <stdint.h>

//==========================================================================
//==========================================================================
//DCVectorEntry
//==========================================================================
//==========================================================================
//A structure to store the contents of the vector hash
template<class T>
class DCVectorEntry
{
public:
  DCVectorEntry(T _Obj_default,
		int64_t _key_default,
		uint64_t _blank_password)
  {
    Obj_default = _Obj_default;
    key_default = _key_default;
    blank_password = _blank_password;
    Clear();
  }
  DCVectorEntry(const DCVectorEntry & rhs)
    {Copy(rhs);}
  DCVectorEntry & operator=(const DCVectorEntry & rhs)
    {Copy(rhs);return(*this);}
  T Obj;   //Object being stored
  int64_t key;  //Key used to set this object
  uint64_t password;  //the password for this key
  int64_t passwordBlocks;  //number of times the password has stopped someone. (reset on clearing of password)
  bool IsValid()
  {
    return (key != key_default);
  }

  void ClearPassword()
  {
    password = blank_password;
    passwordBlocks = 0;
  }
  bool ExistPassword()
  {return(password != blank_password);}
  void Clear()
  {
    Obj = Obj_default;
    key = key_default;
    ClearPassword();
  };
  void Copy(const DCVectorEntry & rhs)
  {
    Obj  = rhs.Obj;
    key  = rhs.key;
    password = rhs.password;
    passwordBlocks = rhs.passwordBlocks;
    
    Obj_default    = rhs.Obj_default;
    key_default    = rhs.key_default;
    blank_password = rhs.blank_password;
  }
  size_t PrintEntry(char * text)
  {
    size_t ret = sprintf(text,"Obj: %i   Key: %"PRIi64"   Password: %"PRIi64"  Blocks: %"PRIi64"\n",
			 Obj,key,password,passwordBlocks);
    return ret;
  }
private:
  T Obj_default;
  int64_t key_default;
  uint64_t blank_password;
};
#endif
