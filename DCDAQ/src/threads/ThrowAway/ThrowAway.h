#ifndef __THROWAWAY__
#define __THROWAWAY__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>


class ThrowAway : public DCThread 
{
 public:
  ThrowAway(std::string Name);
  ~ThrowAway()
    {
    };
  virtual bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  //Update control
  time_t last;
  time_t current;

  //Statistics
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;

  // Memory access
  int32_t blockIndex;
  MemoryBlock block;
  MemoryBlockManager * Mem; 

};


#endif
