#ifdef __RATDS__

#include <FEPCRATManager.h>

void FEPCRATManager::AddTimeStampHistogram(enum TimeStampCode::Code code)
{
  //build binning logarithmically 
  const int nBins = 500;
  double xBins[2*nBins];
  double rangeMax = 100;
  double rangeMin = 1E-5;
  //Negative bins and positive at the same time
  for(int i = 0; i < nBins;i++)
    {      
      //negative
      xBins[nBins-i-1] = -1*exp(log(rangeMin) + i*(log(rangeMax) - log(rangeMin))/double(nBins));
      //positive
      xBins[i+nBins] =    exp(log(rangeMin) + i*(log(rangeMax) - log(rangeMin))/double(nBins));
    }
  hTimeStamp[code] = new TH1F(TimeStampCode::GetTimeStampName(code),
			      TimeStampCode::GetTimeStampName(code),
			      2*nBins-1,xBins);
  //			      4000000,-100,100);
		  
}

void FEPCRATManager::PrintTimeStampHistograms(std::string filename)
{
  TFile * outFile = TFile::Open(filename.c_str(),"RECREATE");
  std::map<enum TimeStampCode::Code,TH1F*>::iterator it;
  for(it = hTimeStamp.begin(); it != hTimeStamp.end();it++)
    {
      outFile->Add(it->second);
      outFile->Write();
      delete it->second;
    }
  hTimeStamp.clear();
  delete outFile;
}

FEPCRATManager::FEPCRATManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("FEPCRATManager"));
  //Make sure our memory vector is empty
  Memory.clear();
  ShutdownStatus = false;
  badValue = -2;
  emptyValue = -1;
  
  AddTimeStampHistogram(TimeStampCode::FE_CLEAR);
  AddTimeStampHistogram(TimeStampCode::FE_RATAssembler_START);
  AddTimeStampHistogram(TimeStampCode::FE_RATAssembler_END);
  AddTimeStampHistogram(TimeStampCode::FE_DRPusher);
  AddTimeStampHistogram(TimeStampCode::FE_DRGetter);
  AddTimeStampHistogram(TimeStampCode::FE_BadEvent);
  Free.SetPrintStuffTrue();
}

FEPCRATManager::~FEPCRATManager()
{
  Deallocate();
  // char * nameBuffer = new char[100];
  // sprintf(nameBuffer,"FEPCRatManager_%d.root",GetRunNumber());
  // PrintTimeStampHistograms(nameBuffer);
  // delete [] nameBuffer;
  PrintTimeStampHistograms("FEPCRatManager.root");
}

void FEPCRATManager::Deallocate()
{
  //Clear our containers
  Free.Clear(true);
  UnSent.Clear(true);
  StorageEvent.Clear(true);
  AssembledEvent.Clear(true);
  Send.Clear(true);
  BadEvent.Clear(true); 
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //      delete FEPC event (check if it's not NULL)
      if(Memory.back().ev)
	{
	  delete Memory.back().ev;
	  Memory.back().ev = NULL;
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
}

void FEPCRATManager::WakeUp()
{
  //child objects
  Free.WakeUp();
  UnSent.WakeUp();
  Send.WakeUp();
  BadEvent.WakeUp();
  
  AssembledEvent.WakeUp();
  StorageEvent.WakeUp();
}
void FEPCRATManager::Shutdown()
{
  //Set to shutdown mode.   Now we can't wait on mutex locks
  //Child objects
  Free.ShutdownWait();
  UnSent.ShutdownWait();
  Send.ShutdownWait();
  BadEvent.ShutdownWait();
  
  AssembledEvent.ShutdownWait();
  StorageEvent.ShutdownWait();

  ShutdownStatus = true;
  
  //call wakeup to force everyone out of a mutex condition wait.
  WakeUp();
}

bool FEPCRATManager::AllocateMemory(xmlNode * setupNode)
{
  bool ret = true;
  
  int NumBlocksERR = GetXMLValue(setupNode,"NUMBLOCKS","MANAGER",NumBlocks);
  if(NumBlocksERR != 0)
    {
      Print("Error reading NUMFEPCBLOCKS in FEPCRATManager.\n");
      ret = false;
    }
  int StorageEventSizeERR = GetXMLValue(setupNode,"STORAGEEVENTSIZE","MANAGER",StorageEventSize);
  if(StorageEventSizeERR != 0)
    {
      Print("Error reading StorageEventSize in FEPCRATManager.\n");
      ret = false;
    }
  int AssembledEventSizeERR = GetXMLValue(setupNode,"ASSEMBLEDEVENTSIZE","MANAGER",AssembledEventSize);
  if(AssembledEventSizeERR != 0)
    {
      Print("Error reading AssembledEventSize in FEPCRATManager.\n");
      ret = false;
    }

  //Allocate the needed RAT FEPC blocks
  if(ret)
    {
      for(uint32_t iBlock = 0; iBlock < NumBlocks;iBlock++)
	{
	  RATEVBlock evBlock(TimeStampCode::FE_CLEAR);	  
	  Memory.push_back(evBlock);
	  Memory.back().ev = new RAT::DS::EV;	  
	  Memory.back().timeStampReference = TimeStampCode::FE_RATAssembler_START;
	  int32_t index = Memory.size()-1;
	  Free.Add(index);
	  if(Memory.back().ev == NULL)
	    {
	      ret = false;
	      break;
	    }
	}     
    }
  //Since we have just setup this MM we should set shutdown status to false
  ShutdownStatus =false;


  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash 
      StorageEvent.Setup(StorageEventSize,emptyValue,badValue);
      AssembledEvent.Setup(AssembledEventSize,emptyValue,badValue);
    }

  //Setup Queues
  Free.Setup(badValue);
  UnSent.Setup(badValue);
  Send.Setup(badValue);
  BadEvent::sBadEvent badEventValue;
  badEventValue.eventKey = -1;
  badEventValue.index = badValue;
  badEventValue.errorCode = Level::BLANK;
  BadEvent.Setup(badEventValue);

  return ret;
}

void FEPCRATManager::PrintStatus(int indent, int verbosity)
{

  StatMessage::StatFEPCMM sStat;
  sStat.sBase.type = StatMessage::FEPC;
  sStat.sInfo.time = time(NULL);
  sStat.sInfo.subID = SubID::BLANK;
  sStat.sInfo.level = Level::BLANK;
  sStat.allocatedSize = Memory.size();
  sStat.assembled.size = AssembledEvent.GetSize();
  sStat.assembled.active = AssembledEvent.GetActiveEntryCount();
  sStat.storage.size = StorageEvent.GetSize();
  sStat.storage.active = StorageEvent.GetActiveEntryCount();
  sStat.free.size = Free.GetSize();
  sStat.free.addCount = Free.GetAddCount();
  sStat.unSent.size = UnSent.GetSize();
  sStat.unSent.addCount = UnSent.GetAddCount();
  sStat.send.size = Send.GetSize();
  sStat.send.addCount = Send.GetAddCount();
  sStat.badEvent.size = BadEvent.GetSize();
  sStat.badEvent.addCount = BadEvent.GetAddCount();

  DCMessage::Message message;
  message.SetType(DCMessage::STATISTIC);
  message.SetData(sStat);
  SendMessageOut(message);

#ifdef VERBOSE_MEMORY  
  //Old printing for the moment. 
  char * textBuffer = new char[5000];
  sprintf(textBuffer,"\n");
  if(indent > 0)
    {
      for(int i = 0;i < indent;i++)
	{
	  sprintf(textBuffer," ");
	}
    }
  char printCommand[] = "FEPCRATManager\n     Size: %04zu\n     Free: %04zu     (%12zu)\n     Active Assembled: %04zu(%04zu)\n     Active Storage: %04zu(%04zu)\n     UnSent: %04zu     (%12zu)\n     ToSend: %04zu     (%12zu)\n     BadEvent: %04zu     (%12zu)\n";
  sprintf(textBuffer, printCommand,
	  sStat.allocatedSize,
	  sStat.free.size,
	  sStat.free.addCount,
	  sStat.assembled.active,
	  sStat.assembled.size,
	  sStat.storage.active,
	  sStat.storage.size,
	  sStat.unSent.size,
	  sStat.unSent.addCount,
	  sStat.send.size,
	  sStat.send.addCount,
	  sStat.badEvent.size,
	  sStat.badEvent.addCount);
  Print(textBuffer);
  delete [] textBuffer;
  if(verbosity >= 2)
    {
      PrintVectorHash();
    }
#endif  
  //  DCMessage::DCTObject rootObj;
//  std::vector<uint8_t> dataTemp;
//  time_t currentTime = time(NULL);
//  std::map<enum TimeStampCode::Code,TH1F*>::iterator it;
//  for(it = hTimeStamp.begin(); it != hTimeStamp.end();it++)
//    {      
//      dataTemp = rootObj.SetData((TObject * )(it->second),
//				 DCMessage::DCTObject::H1F);
//      message.SetDestination();
//      message.SetType(DCMessage::HISTOGRAM);
//      message.SetData(&(dataTemp[0]),dataTemp.size());
//      message.SetTime(currentTime);
//      SendMessageOut(message);
//      it->second->Reset();
//    }
}

void FEPCRATManager::PrintVectorHash()
{
  char * textBuffer = new char[5000000];
  AssembledEvent.PrintDCVector(textBuffer);
  Print(textBuffer);
  StorageEvent.PrintDCVector(textBuffer);
  Print(textBuffer);
  delete [] textBuffer;

}



#endif
