#include <v1720Readout.h>

bool v1720Readout::Setup(xmlNode * SetupNode,
			 std::map<std::string,MemoryManager*> &MemManager)
{
  //=================================================
  //Find the MemoryManager we need
  //=================================================
  //get the memory manager name from the XML string
  std::string memoryManagerName;  
  if(FindSubNode(SetupNode,"MEMORYMANAGER") == NULL)
    {
      PrintError("No memorymanager thread");
      return(false);
    }  
  else
    {
      GetXMLValue(SetupNode,"MEMORYMANAGER",GetName().c_str(),memoryManagerName);
    }
  //Find the mememory manager with name memeoryManagerName
  if(MemManager.find(memoryManagerName) == MemManager.end())
    {
      char * buffer = new char[100];
      sprintf(buffer,"Memory manager %s not found",memoryManagerName.c_str());
      PrintError(buffer);
      delete [] buffer;
      return(false);      
    }
  //Get the memory block manager
  Mem = (MemoryBlockManager *) (MemManager[memoryManagerName]);

  //=================================================
  //Find readout settings
  //=================================================

  //Find max readouts(CAEN events) into a block before we pass it off
  if(FindSubNode(SetupNode,"MaxReadoutsPerBlock") == NULL)
    {
      MaxReadoutsPerBlock = 100;
    }
  else
    {
      GetXMLValue(SetupNode,"MaxReadoutsPerBlock","THREAD",MaxReadoutsPerBlock);
    }

  //Find minimum free space in a block before we just pass it off
  if(FindSubNode(SetupNode,"MinimumFreeBufferSpace") == NULL)
    {
      MinimumFreeBufferSpace = 0x20010 * 2;
    }
  else
    {
      GetXMLValue(SetupNode,"MinimumFreeBufferSpace","THREAD",MaxReadoutsPerBlock);
    }


  //=================================================
  //Find the name of this v1720Readout's config file
  //=================================================
  //File containing WFD settings
  std::string setupFilename;
  if(FindSubNode(SetupNode,"SETUPFILE") == NULL)
    {
      PrintError("No WFD setup file specified");
      return(false);
    }
  else
    {
      GetXMLValue(SetupNode,"SETUPFILE","THREAD",setupFilename);  
    }
  
  //Make string for this WFD's settings
  if(LoadFile(setupFilename.c_str(),WFDSetup) == false)
    {
      char * buffer = new char[100];
      sprintf(buffer,"Bad WFD settings file %s.",setupFilename.c_str());
      PrintError(buffer);
      delete [] buffer;
      return(false);
    }

  //Loop over the WFDSetup to create the needed
  //XML Docs
  //XML BaseNodes
  //WFD
  XMLDoc = xmlReadMemory(WFDSetup.c_str(),
			 WFDSetup.size(),
			 NULL,
			 NULL,
			 0);
  if(XMLDoc == NULL)
    {
      PrintError("Bad XML memory parse.");
      return(false);
    }  
  xmlNode * baseNode = xmlDocGetRootElement(XMLDoc);
  uint32_t nWFDs = NumberOfNamedSubNodes(baseNode,"WFD");
  for(unsigned int iWFD = 0; iWFD < nWFDs;iWFD++)
    {
      xmlNode * tempNode = FindIthSubNode(baseNode,"WFD",iWFD);
      BaseXMLNodes.push_back(tempNode);
      WFD.push_back(new v1720());
      WFD.back()->LoadXMLConfig(BaseXMLNodes.back());
      
      WFDEventCount.push_back(0);

      WFDReadoutNumber.push_back(0);         
      WFDTimeStep_ReadoutNumber.push_back(0);

      WFDReadoutSize.push_back(0);           
      WFDTimeStep_ReadoutSize.push_back(0);  
    }

  currentTime = time(NULL);
  lastTime = time(NULL);
  Ready = true;
  return(true);  
}
