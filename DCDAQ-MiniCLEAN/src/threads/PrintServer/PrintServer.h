#ifndef __PRINTSERVER__
#define __PRINTSERVER__
/*

 */

#include <DCThread.h>
#include <xmlHelper.h>
#include <StatMessage.h>

#include <math.h> //fabs()

class PrintServer : public DCThread 
{
 public:
  PrintServer(std::string Name);
  ~PrintServer();

  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  
  virtual void MainLoop(){};
 private:
  virtual bool ProcessMessage(DCMessage::Message &message);  
  virtual void ThreadCleanUp();
  struct sRoute
  {
    enum DCMessage::DCMessageType type;
    DCMessage::DCAddress route;
  };
  std::vector<sRoute> routes;

  std::map<DCMessage::DCMessageType,void (PrintServer::*)(DCMessage::Message & message)> typeMap;
  void PrintError(DCMessage::Message &message);
  void PrintStats(DCMessage::Message &message);
  void PrintText (DCMessage::Message &message);
};
#endif
