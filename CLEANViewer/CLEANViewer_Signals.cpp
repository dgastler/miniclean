#include "CLEANViewer.h"

//roots way of automatically haneling signals from TGObjects
Bool_t CLEANViewer::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::ProcessMessage(Long_t msg, Long_t parm1, Long_t)\n");
  
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {
    case kCM_BUTTON:
      if (parm1 == 2007)
	guiToolBar->GetButton(2008)->SetState(kButtonUp);
      if (parm1 == 2005)
	CloseWindow();
      if (parm1 == 2003)
	{
	  zoomAdjusted = kFALSE;
	  PlotChannel();
	}
      if (parm1 == 2002)
	FileMenu(3);
      break;
    case kCM_MENU:
      if (parm1 == 11)
	{					
	  zoomAdjusted = kFALSE;
	  PlotChannel();
	}
      if (parm1 == 12)
	{
	  if (gridLinesY)
	    {
	      gridLinesY = kFALSE;
	      popupGridLines->UnCheckEntry(13);
	    }
	  else
	    {
	      gridLinesY = kTRUE;
	      popupGridLines->CheckEntry(13);
	    }
	}
      if (parm1 == 13)
	{	  
	}
      break;
    default:
      break;  
    }
    break;
  default:
    break;
  }
  return kTRUE;
}

//Next event button causes the event number entry widget to increase by one
void CLEANViewer::NextEventPressed()
{



  if(DEBUG) fprintf(stderr,"CLEANViewer::NextEvent()\n");

  currentEvent = eventNumberEntry->GetNumberEntry()->GetIntNumber();
  currentEvent++;
  eventNumberEntry->SetNumber(currentEvent);
  if(eventNumberEntry3D)  eventNumberEntry3D->SetNumber(currentEvent);
  if(DAQdat)
    {

      if(sizeBlockRead==dBlock->dataSize)
	{ //End of memBlock, need to load new one
	 
	  
	  bufferMemNumber++;
	  if((bufferMemNumber+1)>(int)memBlockStartVec.size())
	    {//Store starting position of memBlock;
	      fpos_t Pos;
	      fgetpos(InFile,&Pos);
	      memBlockStartVec.push_back(Pos);
	    }
	
	  ReadMemBlock(cBlock);
      
	  if(cBlock->uncompressedSize>0)
	    {
	      DecompressMemBlock(cBlock,dBlock);
	    }
	  else
	    {
	      if(dBlock->allocatedSize<cBlock->dataSize)
		{
		  delete (dBlock->buffer);
		  dBlock->buffer=new uint32_t[cBlock->dataSize];
		}
	      memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
	      dBlock->dataSize=cBlock->dataSize;
	    }


	  DAQBufferPtr=dBlock->buffer;
	  uint32_t * initPtr = DAQBufferPtr;
	  DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
	  sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	}

     

      ratDS->PruneEV();
      ratEV=ratDS->AddNewEV();
      int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
      if(size<0) CloseWindow();
      
      DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
      sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     

 
      bufferEvtNumber+=1;
     
      if(NumberEV(kTRUE)<0)
	{
	  NextEventPressed();
	  return;
	}
     
    }

  else
    {
      eventProgress->SetPosition(currentEvent);
      if(NumberEV(kTRUE)<0)
	{
	  NextEventPressed();
	  return;
	}
      

      eventProgress->ShowPosition();
    }
  PlotChannel();
  //enable or disable next and previous event buttons depending if there are no more events before or after current event
  if(currentEvent == firstEvent)
    {
      prevEvent->SetEnabled(kFALSE);
      if(prevEvent3D)prevEvent3D->SetEnabled(kFALSE);
      popupRAT->DisableEntry(22);
    }
  else
    {
      prevEvent->SetEnabled(kTRUE);
      if(prevEvent3D)prevEvent3D->SetEnabled(kTRUE);
      popupRAT->EnableEntry(22);
    }
  if(currentEvent == nEvents)
    {
      nextEvent->SetEnabled(kFALSE);
      if(nextEvent3D) nextEvent3D->SetEnabled(kFALSE);
      popupRAT->DisableEntry(21);
    }
  else
    {
      nextEvent->SetEnabled(kTRUE);
      if(nextEvent3D) nextEvent3D->SetEnabled(kTRUE);
      popupRAT->EnableEntry(21);
    }
}

//Prev event button causes the event number entry widget to decrease by one
void CLEANViewer::PrevEventPressed()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::PrevEvent()\n");

  currentEvent = eventNumberEntry->GetNumberEntry()->GetIntNumber();
  currentEvent--;
  eventNumberEntry->SetNumber(currentEvent);
  if(eventNumberEntry3D)   eventNumberEntry3D->SetNumber(currentEvent);
  if(DAQdat)
    {

      int evtNum;
     
      fsetpos(InFile,&memBlockStartVec[bufferMemNumber]);
      //cBlock = new MemoryBlock();
    
    
      ReadMemBlock(cBlock);
      
      if(cBlock->uncompressedSize>0)
	{
	  DecompressMemBlock(cBlock,dBlock);
	}

      else
	{
	  if(dBlock->allocatedSize<cBlock->dataSize)
	    {
	      delete (dBlock->buffer);
	      dBlock->buffer=new uint32_t[cBlock->dataSize];
	    }
	  memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
	  dBlock->dataSize=cBlock->dataSize;
	}


      DAQBufferPtr=dBlock->buffer;
      uint32_t * initPtr = DAQBufferPtr;
      DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
      sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
      ratDS->PruneEV();
      ratEV=ratDS->AddNewEV();
      int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
      if(size<0) CloseWindow();
      DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
      sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
         
      // DAQBufferPtr+=size;
      //sizeBlockRead+=size;
    
      evtNum=ratEV->GetEventID();
      //rewind to beginning of memBlock containing event we are looking for
      while(currentEvent<evtNum)
	{
	  bufferMemNumber--;
	  fsetpos(InFile,&memBlockStartVec[bufferMemNumber]);
	  
    
	  ReadMemBlock(cBlock);
      
	  if(cBlock->uncompressedSize>0)
	    {
	      DecompressMemBlock(cBlock,dBlock);
	    }

	  else
	    {
	      if(dBlock->allocatedSize<cBlock->dataSize)
		{
		  delete (dBlock->buffer);
		  dBlock->buffer=new uint32_t[cBlock->dataSize];
		}
	      memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
	      dBlock->dataSize=cBlock->dataSize;
	    }
	  DAQBufferPtr=dBlock->buffer;
	  uint32_t * initPtr = DAQBufferPtr;
	  DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
	  sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	  
	  ratDS->PruneEV();
	  ratEV=ratDS->AddNewEV();
	  int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
	  if(size<0) CloseWindow();
	  DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	  sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     
	  //DAQBufferPtr+=size;
	  //sizeBlockRead+=size;
    
	  evtNum=ratEV->GetEventID();

	}

     
      int numToRead=currentEvent-evtNum;
      for(int iEVT=0;iEVT<numToRead;iEVT++)
	{
	  ratDS->PruneEV();
	  ratEV=ratDS->AddNewEV();
	  int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
	  if(size<0) CloseWindow();
	  DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	  sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     
	  // DAQBufferPtr+=size;
	  //sizeBlockRead+=size;
	  
	}
      bufferEvtNumber=currentEvent+1;
      /*if(ratEV->GetEventID() != currentEvent)
	{
	  
	  fprintf(stderr,"Error in reading back events, %i,%i \n",ratEV->GetEventID(),(int)currentEvent);
	  CloseWindow();
	  }*/
     
     
      if(NumberEV(kTRUE)<0)
	{
	  PrevEventPressed();
	  return;
	}

    }
  else
    {
      eventProgress->SetPosition(currentEvent);
      if(NumberEV(kTRUE)<0)
	{
	  PrevEventPressed();
	  return;
	}
      eventProgress->ShowPosition();
    }
  PlotChannel();
  //enable/disable next and previous event buttons depending if there are no more events before or after current event
  if(currentEvent == firstEvent)
    {
      prevEvent->SetEnabled(kFALSE);
      popupRAT->DisableEntry(22);
    }
  else
    {
      prevEvent->SetEnabled(kTRUE);
      popupRAT->EnableEntry(22);
    }
  if(currentEvent == nEvents)
    {
      if(!DAQdat)
	{
	  nextEvent->SetEnabled(kFALSE);
	  popupRAT->DisableEntry(21);
	}
      else
	{
	  nextEvent->SetEnabled(kTRUE);
	  popupRAT->EnableEntry(21);
	}
    }
  else
    {
      nextEvent->SetEnabled(kTRUE);
      popupRAT->EnableEntry(21);
    }
}

void CLEANViewer::highLight()
{
  DrawEventDisplay3D();
}
void CLEANViewer::pmtSelectChanged3D()
{
  fprintf(stderr,"Changed\n");
  selectPMT=pmtSelectEntry3D->GetNumberEntry()->GetIntNumber();
  if(highLightPMTButton->IsDown()) DrawEventDisplay3D();
}
//function handles when the event entry widgets changes
void CLEANViewer::EventChange3D()
{
  eventNumberEntry->SetNumber(eventNumberEntry3D->GetNumberEntry()->GetIntNumber());
  EventChange();
}
void CLEANViewer::EventChange()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::EventChange()\n");
  
  Long64_t oldEvent = currentEvent;
  currentEvent = eventNumberEntry->GetNumberEntry()->GetIntNumber();
  if(eventNumberEntry3D)
    {
      eventNumberEntry->SetNumber(currentEvent);
    }
  if(DAQdat)
    {
      if (currentEvent<bufferEvtNumber)
	{

	  int evtNum;
	  fsetpos(InFile,&memBlockStartVec[bufferMemNumber]);
	  

	  ReadMemBlock(cBlock);
      
	  if(cBlock->uncompressedSize>0)
	    {
	      DecompressMemBlock(cBlock,dBlock);
	    }
	  else
	    {
	      if(dBlock->allocatedSize<cBlock->dataSize)
		{
		  delete (dBlock->buffer);
		  dBlock->buffer=new uint32_t[cBlock->dataSize];
		}
	      memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
	      dBlock->dataSize=cBlock->dataSize;
	    }


	  DAQBufferPtr=dBlock->buffer;
	  uint32_t * initPtr = DAQBufferPtr;
	  DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
	  sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	  ratDS->PruneEV();
	  ratEV=ratDS->AddNewEV();
	  int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
	  if(size<0) CloseWindow();
	  DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	  sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     
	  // DAQBufferPtr+=size;
	  // sizeBlockRead+=size;
    
	  evtNum=ratEV->GetEventID();
	  //rewind to beginning of memBlock containing event we are looking for
	  while(currentEvent<evtNum)
	    {
	     
	      bufferMemNumber--;
	      fsetpos(InFile,&memBlockStartVec[bufferMemNumber]);

    
	      ReadMemBlock(cBlock);
      
	      if(cBlock->uncompressedSize>0)
		{
		  DecompressMemBlock(cBlock,dBlock);
		}


	      else
		{
		  if(dBlock->allocatedSize<cBlock->dataSize)
		    {
		      delete (dBlock->buffer);
		      dBlock->buffer=new uint32_t[cBlock->dataSize];
		    }
		  memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
		  dBlock->dataSize=cBlock->dataSize;
		}
	      DAQBufferPtr=dBlock->buffer;
	      uint32_t * initPtr = DAQBufferPtr;
	      DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
	      sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	      ratDS->PruneEV();
	      ratEV=ratDS->AddNewEV();
	      int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
	      if(size<0) CloseWindow();
	      DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	      sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     
	      //  DAQBufferPtr+=size;
	      // sizeBlockRead+=size;
    
	      evtNum=ratEV->GetEventID();

	    }

     
	  int numToRead=currentEvent-evtNum;
	  for(int iEVT=0;iEVT<numToRead;iEVT++)
	    {
	      ratDS->PruneEV();
	      ratEV=ratDS->AddNewEV();
	      int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
	      if(size<0) CloseWindow();
	      
	      DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	      sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     
	      //DAQBufferPtr+=size;
	      //sizeBlockRead+=size;
	      
	    }
	  bufferEvtNumber=currentEvent+1;
	  /*	  if(ratEV->GetEventID() != currentEvent)
	    {
	      fprintf(stderr,"Error in reading back events %i,%i \n",ratEV->GetEventID(),(int)currentEvent);
	      CloseWindow();
	      }*/
	  NumberEV(kTRUE);
	 


	}
      else
	{
	  int numberToRead=currentEvent-bufferEvtNumber+1;
	 
	  for(int iEVT=0;iEVT<numberToRead;iEVT++)
	    {
	      if(sizeBlockRead==dBlock->dataSize)
		{
		  
		  //End of memBlock, need to load new one
		  bufferMemNumber++;
		  if((bufferMemNumber+1)>(int)memBlockStartVec.size())
		    {//Store starting position of memBlock;
		      fpos_t Pos;
		      fgetpos(InFile,&Pos);
		      memBlockStartVec.push_back(Pos);
		    }
	
		  ReadMemBlock(cBlock);
		 
		  
		  if(cBlock->uncompressedSize>0)
		    {
		      DecompressMemBlock(cBlock,dBlock);
		    }

		 
		  else
		    {
		      if(dBlock->allocatedSize<cBlock->dataSize)
			{
			  delete (dBlock->buffer);
			  dBlock->buffer=new uint32_t[cBlock->dataSize];
			}
		      memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
		      dBlock->dataSize=cBlock->dataSize;
		    }
		  DAQBufferPtr=dBlock->buffer;
		  uint32_t * initPtr = DAQBufferPtr;
		  DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
		  sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
		}



	      ratDS->PruneEV();
	      ratEV=ratDS->AddNewEV();
	      int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ratEV);
	      if(size<0) CloseWindow();
	      DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	      sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));
     
	      // DAQBufferPtr+=size;
      
	      //	      sizeBlockRead+=size;

 
	      bufferEvtNumber+=1;
	     

	     



	    }

	   NumberEV(kFALSE);
	}

      

    }
  
  else
    {
      if (currentEvent > oldEvent)
	NumberEV(kFALSE);
      if (currentEvent < oldEvent)
	NumberEV(kTRUE);
      eventProgress->SetPosition(currentEvent);
      eventProgress->ShowPosition();
    }
  //DrawEventDisplay3D();
  PlotChannel();
 
  if(currentEvent == firstEvent)
    prevEvent->SetEnabled(kFALSE);
  else
    prevEvent->SetEnabled(kTRUE);
  if(currentEvent == nEvents)
    nextEvent->SetEnabled(kFALSE);
  else
    nextEvent->SetEnabled(kTRUE);
    
 
}

//function deals with when a pmt has been checked 
void CLEANViewer::PlotChannel()
{

  if(DEBUG) fprintf(stderr,"CLEANViewer::PlotChannel()\n"); 

  PlotIDsVector.clear();

  rawTCanvas->Clear();
  rawECanvas->Clear();
  calibTCanvas->Clear();
  

  SumIncluded=false;

  bool kPMTOrder;
  
  //free histogram memory, safe to do because always allocated in LoadChannels 
  
  delete PromptChannelsHist;
  delete LateChannelsHist;
  if(calibrated)
    {
      delete PromptCalibChannelsHist;
      delete LateCalibChannelsHist;
    }

  PromptChannelsHist=NULL;
  LateChannelsHist=NULL;
  PromptCalibChannelsHist=NULL;
  LateCalibChannelsHist=NULL;

  
    
  currentTabnumber=selectionTab->GetCurrent();
  if(drawPromptTotal)
    {

      //Make histograms containing just the active channels
      PromptChannelsHist = new TH1F;
      LateChannelsHist = new TH1F;
      
      PromptCalibChannelsHist=new TH1F;
      LateCalibChannelsHist=new TH1F;

     


      Int_t activeChannels = 0;
      int iLabel=0;


      for(int iChannel = 0; iChannel < PromptHist->GetNbinsX(); iChannel++)
	{
	  if(pmtCheckInfoVector[iChannel].CheckButton->IsOn())
	    {
	      if(SupressEmpty)
		{
		  if((PromptHist->GetBinContent(ChannelIDOrderMap[pmtCheckInfoVector[iChannel].PMTID])+LateHist->GetBinContent(ChannelIDOrderMap[pmtCheckInfoVector[iChannel].PMTID]))==0)
		    {
		      continue;
		    }
		}
	      PlotIDs TempPlotIDs;
	      
	      TempPlotIDs.ChannelID=pmtCheckInfoVector[iChannel].PMTID;
	      TempPlotIDs.WFDID=pmtCheckInfoVector[iChannel].Board;
	      TempPlotIDs.InputID=pmtCheckInfoVector[iChannel].Channel;
	      PlotIDsVector.push_back(TempPlotIDs);
	      
	    }
		
	}
     
      if(!orderByBoard) 
	{
	  sort(PlotIDsVector.begin(),PlotIDsVector.end(),OrderbyChannel);
	}
      else
	{
	  sort(PlotIDsVector.begin(),PlotIDsVector.end(),OrderbyBoard);
	}
       if(summedWaveformButton->IsDown())
	{
	  SumIncluded=true;
	  PlotIDs TempPlotIDs;
	  TempPlotIDs.ChannelID=-100;
	  TempPlotIDs.WFDID=-100;
	  TempPlotIDs.InputID=-100;
	  PlotIDsVector.push_back(TempPlotIDs);
	}
     
       activeChannels=PlotIDsVector.size();

      for(unsigned int iChannel=0;iChannel<PlotIDsVector.size();iChannel++)
	{
	  iLabel+=1;
	  char iChannelStr[100];
	  if(PlotIDsVector[iChannel].ChannelID<0)
	    {
	      sprintf(iChannelStr,"Summed");
	    }
	  else
	    {
	      if(!labelByBoard)
		sprintf(iChannelStr,"PMT %i",PlotIDsVector[iChannel].ChannelID);
	      else
		sprintf(iChannelStr,"WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);
	    }
	  //*ChannelsHist is the histogram we will draw, includes only channels checked off, *Hist is the histogram of all the channels (there may be a better way of doing this, like just set *ChannelsHist value to ratEV->GetPrompt(/Total)Q
	  PromptChannelsHist->Fill(iChannelStr,
				   -1*(PromptHist->GetBinContent(ChannelIDOrderMap[PlotIDsVector[iChannel].ChannelID])));
	 
	  PromptChannelsHist->GetXaxis()->SetBinLabel(iLabel,iChannelStr);
	  PromptChannelsHist->GetYaxis()->SetLabelSize(0);
	  LateChannelsHist->Fill(iChannelStr,
				 LateHist->GetBinContent(ChannelIDOrderMap[PlotIDsVector[iChannel].ChannelID]));
	  LateChannelsHist->GetXaxis()->SetBinLabel(iLabel,iChannelStr);
	 
	  if(activeChannels <=50)
	    {
	      LateChannelsHist->GetXaxis()->SetLabelSize(0.020);
	      PromptChannelsHist->GetXaxis()->SetLabelSize(0.020);
	   
	      LateCalibChannelsHist->GetXaxis()->SetLabelSize(0.020);
	      PromptCalibChannelsHist->GetXaxis()->SetLabelSize(0.020);
	    }
	  else if(activeChannels <=64)
	    {
	      LateChannelsHist->GetXaxis()->SetLabelSize(0.015);
	      PromptChannelsHist->GetXaxis()->SetLabelSize(0.015);
	   
	      LateCalibChannelsHist->GetXaxis()->SetLabelSize(0.015);
	      PromptCalibChannelsHist->GetXaxis()->SetLabelSize(0.015);
	    }

	    else 
	    {
	      LateChannelsHist->GetXaxis()->SetLabelSize(0.01);
	      PromptChannelsHist->GetXaxis()->SetLabelSize(0.01);
	      LateCalibChannelsHist->GetXaxis()->SetLabelSize(0.01);
	      PromptCalibChannelsHist->GetXaxis()->SetLabelSize(0.01);
	      }
	  
	  if(calibrated)
	    {
	      PromptCalibChannelsHist->Fill(iChannelStr,
					    -1*(PromptCalibHist->GetBinContent(ChannelIDOrderMap[PlotIDsVector[iChannel].ChannelID])));
	      PromptCalibChannelsHist->GetXaxis()->SetBinLabel(iLabel,iChannelStr);
	      PromptCalibChannelsHist->GetYaxis()->SetLabelSize(0);
	     
		      
	      LateCalibChannelsHist->Fill(iChannelStr,
					  LateCalibHist->GetBinContent(ChannelIDOrderMap[PlotIDsVector[iChannel].ChannelID]));
	      LateCalibChannelsHist->GetXaxis()->SetBinLabel(iLabel,iChannelStr);
	    }
	      


	}
    
      if(DEBUG) fprintf(stderr,"About to DrawPromptTo\n");
      DrawPromptTotalHist(activeChannels);
      if(DEBUG) fprintf(stderr,"Just did DrawPromptTo()\n");
    }
  
  else
    { 


      //get all the RAT pointers sorted out for each pmt that has been checked
      //uses the pmt IDs to relate check buttons to actual pmts
      for (UInt_t iCheck = 0;
	   iCheck < pmtCheckInfoVector.size();
	   iCheck++)
	{
	  if (pmtCheckInfoVector[iCheck].CheckButton->IsOn())
	    {
	

	      if(SupressEmpty)
		{
		  //Check to see if any data there
		  
		  for(UInt_t iWaveform=0;iWaveform<WaveformVector.size();iWaveform++)
		    {
		      if(WaveformVector[iWaveform].GetID()==(int)pmtCheckInfoVector[iCheck].PMTID)
			{
			  
			  if(WaveformVector[iWaveform].GetWindows().size()>0)
			    {
			      
			      PlotIDs TempPlotIDs;
			      
			      TempPlotIDs.ChannelID=pmtCheckInfoVector[iCheck].PMTID;
			      TempPlotIDs.WFDID=pmtCheckInfoVector[iCheck].Board;
			      TempPlotIDs.InputID=pmtCheckInfoVector[iCheck].Channel;
			      PlotIDsVector.push_back(TempPlotIDs);
	
			    }
			  break;
			  
			}
		      
		    }
	
		}
	      else
		{

		  PlotIDs TempPlotIDs;
 
		  TempPlotIDs.ChannelID=pmtCheckInfoVector[iCheck].PMTID;
		  TempPlotIDs.WFDID=pmtCheckInfoVector[iCheck].Board;
		  TempPlotIDs.InputID=pmtCheckInfoVector[iCheck].Channel;
		  PlotIDsVector.push_back(TempPlotIDs);
	
		}
     			       
	    }
	}
      if(PlotIDsVector.size()==0)
	{
	  return;
	}
      if(orderByBoard) 
	{
	  kPMTOrder=false;
	}
      else
	{
	  kPMTOrder=true;
	}

	  
      if(kPMTOrder) 
	{
	  sort(PlotIDsVector.begin(),PlotIDsVector.end(),OrderbyChannel);
	}
      else
	{
	  sort(PlotIDsVector.begin(),PlotIDsVector.end(),OrderbyBoard);
	}
      if(DEBUG) fprintf(stderr,"ABout to DrawPolyLine()\n");
      //DrawPolyLine() contained in CLEANViewer_Draw.cpp
      if(summedWaveformButton->IsDown())
	{
	  SumIncluded=true;
	  PlotIDs TempPlotIDs;
	  TempPlotIDs.ChannelID=-100;
	  TempPlotIDs.WFDID=-100;
	  TempPlotIDs.InputID=-100;
	  PlotIDsVector.push_back(TempPlotIDs);
	}
      DrawPolyLine();


      if(DEBUG) fprintf(stderr,"Just did DrawPolyLine()\n");
    }
  //update those canvases or they will remain blank
  if(DEBUG) fprintf(stderr,"About to rTCanvas->Update()\n");
  rawTCanvas->Update();
  rawTCanvas->Modified();
  if(DEBUG) fprintf(stderr,"Just did rTCanvas->Update()\n");

  calibTCanvas->Update();
  calibTCanvas->Modified();

  if(DEBUG) fprintf(stderr,"About to histogramTCanvas->Update()\n");
  histogramTCanvas->Update();
  histogramTCanvas->Modified();
  if(DEBUG) fprintf(stderr,"Just did histogramTCanvas->Update()\n");
}

bool CLEANViewer::OrderbyChannel(const PlotIDs &a,const PlotIDs &b)
{
  /*
  if(a.ChannelID<0 && b.ChannelID>-1)
    return false;
  if(a.ChannelID>-1 && b.ChannelID<0)
    return true;
*/
  return a.ChannelID<b.ChannelID;

}

bool CLEANViewer::OrderbyBoard(const PlotIDs &a,const PlotIDs &b)
{
  return (8*a.WFDID+a.InputID<8*b.WFDID+b.InputID);

}

bool CLEANViewer::OrderButtonsbyBoard(const pmtCheckInfo &a,const pmtCheckInfo &b)
{
  return (8*a.Board+a.Channel<8*b.Board+b.Channel);

}



bool CLEANViewer::OppOrderbyChannel(const PlotIDs &a,const PlotIDs &b)
{
  return a.ChannelID>b.ChannelID;

}

bool CLEANViewer::OppOrderbyBoard(const PlotIDs &a,const PlotIDs &b)
{
  return (8*a.WFDID+a.InputID>8*b.WFDID+b.InputID);

}

//function handles when the double slider changes positions
void CLEANViewer::XAxisChange()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::XAxisChange()\n");

  if(reductionLevel!=ReductionLevel::CHAN_PROMPT_TOTAL)
    {

      PolyLineZoomAdjust();

      rawTCanvas->Update();
      rawTCanvas->Modified();

      calibTCanvas->Update();
      calibTCanvas->Modified();

      histogramTCanvas->Update();
      histogramTCanvas->Modified();
    }
}

//function handles when the double slider changes positions
void CLEANViewer::ZoomFull()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::ZoomFull()\n");

  zoomSlider->SetPosition(0, 40000);


  PolyLineZoomAdjust();

  rawTCanvas->Update();
  rawTCanvas->Modified();

  calibTCanvas->Update();
  calibTCanvas->Modified();

  histogramTCanvas->Update();
  histogramTCanvas->Modified();
}

void CLEANViewer::CheckAllPMTs()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::CheckAllPMTs()\n");

  int pmtID;
  for (UInt_t iCheck = 0; iCheck < pmtCheckInfoVector.size(); iCheck++)
    {
     
      pmtID=pmtCheckInfoVector[iCheck].PMTID;
     
      pmtCheckInfoVector[iCheck].CheckButton->SetState(kButtonDown);
      pmtCheckInfoVector[iCheck].CheckOrderButton->SetState(kButtonDown);
     
      //    pmtCheckInfoVector[iCheck].isDown=true;
      if(pmtID<0)
	{
	  pmtCheckInfoVector[iCheck].CheckButton->SetEnabled(kFALSE);
	}

      if(int(iCheck)<ratEV->GetPMTCount())
	pmtCheckInfoVector[iCheck].CheckOrderButton->SetState(kButtonDown);
    }

  for(UInt_t iBoard=0;iBoard<wfdGroupVector.size();iBoard++)
    {
      wfdGroupVector[iBoard]->GetButton(iBoard)->SetState(kButtonDown);
    }
 
  PlotChannel();

  //DrawEventDisplay3D();
  //displayTabTCanvas->Update();
  //displayTabTCanvas->Modified();
  //displayTCanvas->Update();
  //displayTCanvas->Modified();
}

void CLEANViewer::UncheckAllPMTs()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::CheckAllPMTs()\n");
 
  int pmtID;
  for (UInt_t iCheck = 0; iCheck < pmtCheckInfoVector.size(); iCheck++)
    {

      pmtID=pmtCheckInfoVector[iCheck].PMTID;
      if(pmtID!=-1)
	{
	  pmtCheckInfoVector[iCheck].CheckButton->SetState(kButtonUp);
	  pmtCheckInfoVector[iCheck].CheckOrderButton->SetState(kButtonDown);
	 
	}
       if(pmtID<0)
	{
	  pmtCheckInfoVector[iCheck].CheckButton->SetEnabled(kFALSE);
	}


      if(int(iCheck)<ratEV->GetPMTCount())
	pmtCheckInfoVector[iCheck].CheckOrderButton->SetState(kButtonUp);
    }

  for(UInt_t iBoard=0;iBoard<wfdGroupVector.size();iBoard++)
    {
      wfdGroupVector[iBoard]->GetButton(iBoard)->SetState(kButtonUp);
    }
  PlotChannel();
  //DrawEventDisplay3D();
  //displayTabTCanvas->Update();
  //displayTabTCanvas->Modified();
  // displayTCanvas->Update();
  //displayTCanvas->Modified();

  rawECanvas->Clear();
  calECanvas->Clear();
}

void CLEANViewer::FileMenu(Int_t id)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::FileMenu(Int_t id)\n");
  if (id ==1)
    {
      ResetFile();
      TGFileInfo* fileInfo=new TGFileInfo;
      new TGFileDialog(gClient->GetRoot(),gClient->GetRoot(),kFDOpen,fileInfo);
      delete dataFile;
      dataFile=new char[100];
      sprintf(dataFile,fileInfo->fFilename);
      delete fileInfo;
      LoadFile(dataFile);
      MapSubwindows();
      Layout();
      MapWindow();
      //if(ZMQ)
      CheckAllPMTs();
      FirstRead=true;
    }
  if (id == 6)
    CloseWindow();
  if (id >29 and id<34)
    {

      TGFileInfo * fileInfo=new TGFileInfo;
      new TGFileDialog(gClient->GetDefaultRoot(),gClient->GetDefaultRoot(),kFDSave,fileInfo);
      SaveCanvas(fileInfo->fFilename,id);
      delete fileInfo;
    }

}

void CLEANViewer::ViewMenu(Int_t id)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::ViewMenu(Int_t id)\n");

  if (id == 11)
    {					
      zoomAdjusted = kFALSE;
      PlotChannel();
    }
  if (id == 12)
    {
      if (gridLinesX)
	{
	  gridLinesX=kFALSE;
	  popupGridLines->UnCheckEntry(12);
	  PlotChannel();
	}
      else
	{
	  gridLinesX=kTRUE;
	  popupGridLines->CheckEntry(12);
	  PlotChannel();
	}
    }
  if (id == 13)
    {
      if (gridLinesY)
	{
	  gridLinesY=kFALSE;
	  popupGridLines->UnCheckEntry(13);
	  PlotChannel();
	}
      else
	{
	  gridLinesY=kTRUE;
	  popupGridLines->CheckEntry(13);
	  PlotChannel();
	}
    }
  if(id==14)
    {
      if(commonScale)
	{
	  commonScale=kFALSE;
	  popupView->UnCheckEntry(14);
	  PlotChannel();
	}
      else
	{
	  commonScale=kTRUE;
	  popupView->CheckEntry(14);
	  PlotChannel();
	}
    }
  if (id == 15)
    ZoomFull();
  if (id == 16)
    {
      if(drawSeperator)
	{
	  drawSeperator=kFALSE;
	  popupView->UnCheckEntry(16);
	  PlotChannel();
	}
      else
	{
	  drawSeperator=kTRUE;
	  popupView->CheckEntry(16);
	  PlotChannel();
	}
    }
  if (id == 161)
    {
      SupressEmpty=!SupressEmpty;
      if(SupressEmpty)
	{
	  popupView->CheckEntry(161);
	}
      else
	{
	  popupView->UnCheckEntry(161);
	}
      PlotChannel();
    }

  if(id==17)
    {
      
      orderByBoard=true;
      popupOrdering->CheckEntry(17);
      popupOrdering->UnCheckEntry(18);
      PlotChannel();
    }

  if(id==18)
    {
      orderByBoard=false;
      popupOrdering->CheckEntry(18);
      popupOrdering->UnCheckEntry(17);
      PlotChannel();

    }

  if(id==19)
    {
      labelByBoard=true;
      popupLabeling->CheckEntry(19);
      popupLabeling->UnCheckEntry(20);
      PlotChannel();

    }

  if(id==20)
    {
      labelByBoard=false;
      popupLabeling->CheckEntry(20);
      popupLabeling->UnCheckEntry(19);
      PlotChannel();

    }

  if(id==21)
    {
      if(AutoScaling)
	{
	  popupView->UnCheckEntry(21);
	}
      else
	{
	  popupView->CheckEntry(21);
	}
      AutoScaling=(!AutoScaling);
      //DrawEventDisplay3D();
      //displayTCanvas->Update();
      //displayTCanvas->Modified();
      
    }

  if(id==22)
    {
      if(AvgDraw)
	popupView->UnCheckEntry(22);
      else
	popupView->CheckEntry(22);
      
      AvgDraw=(!AvgDraw);
      //DrawEventDisplay3D();
      //displayTCanvas->Update();
      //displayTCanvas->Modified();
    }

  if(id==23)
    {
      popupUnsnap->DisableEntry(23);
      rawTCanvas->SetEditable(kTRUE);
      rawTCanvas->Clear();
      rawECanvas->Clear();
      rawTCanvas->Update();
      rawTCanvas->Modified();
      

      guiTab->SetEnabled(0,kFALSE);
    

      winRaw=new TGMainFrame(gClient->GetRoot(),800,700);
      winRaw->SetWindowName("Raw Wavemform");
      if(tabRawWin) delete tabRawWin;
      tabRawWin=new TGCompositeFrame(winRaw,800,700);
      winRaw->AddFrame(tabRawWin,tabLayout);
      if(rawECanWin) delete rawECanWin;
      rawECanWin = new TRootEmbeddedCanvas("Raw Waveform", tabRawWin,800,700);

      tabRawWin->AddFrame(rawECanWin,tabLayout);
      rawTCanvas = rawECanWin->GetCanvas();

      PlotChannel();
      rawTCanvas->SetEditable(kFALSE);
  
      rawTCanvas->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
                                  "CLEANViewer", this,
                                  "RawCanvasClicked(Int_t,Int_t,Int_t,TObject*");

      winRaw->Connect("CloseWindow()","CLEANViewer",this,"RawWindowClosed()");
      // winRaw->DontCallClose();
      winRaw->MapSubwindows();
      winRaw->MapWindow();
      
    }

  if(id==24)
    {

      popupUnsnap->DisableEntry(24);
      calibTCanvas->SetEditable(kTRUE);
      calibTCanvas->Clear();
      calECanvas->Clear();
      calibTCanvas->Update();
      calibTCanvas->Modified();
      

      guiTab->SetEnabled(1,kFALSE);
    
    
      winCalib=new TGMainFrame(gClient->GetRoot(),800,700);
      winCalib->SetWindowName("Calibrated Waveform");
      if(tabCalibWin) delete tabCalibWin;
      tabCalibWin=new TGCompositeFrame(winCalib,800,700);
     
      winCalib->AddFrame(tabCalibWin,tabLayout);
      if(calibECanWin) delete calibECanWin;
      TRootEmbeddedCanvas * calibECanWin = new TRootEmbeddedCanvas("Calibrated", tabCalibWin,800,700);

      tabCalibWin->AddFrame(calibECanWin,tabLayout);
      calibTCanvas = calibECanWin->GetCanvas();

      PlotChannel();
      calibTCanvas->SetEditable(kFALSE);
  
      calibTCanvas->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
                                  "CLEANViewer", this,
                                  "CalibCanvasClicked(Int_t,Int_t,Int_t,TObject*");

      winCalib->Connect("CloseWindow()","CLEANViewer",this,"CalibWindowClosed()");
      
      winCalib->MapSubwindows();
      winCalib->MapWindow();
     
    }



  if(id==25)
    {

      popupUnsnap->DisableEntry(25);
      histogramTCanvas->SetEditable(kTRUE);
      histogramTCanvas->Clear();
      histogramECanvas->Clear();
      histogramTCanvas->Update();
      histogramTCanvas->Modified();
      

      guiTab->SetEnabled(2,kFALSE);
    
      if(winHist) delete winHist;
      winHist=new TGMainFrame(gClient->GetRoot(),800,700);
      winHist->SetWindowName("Histogram");
      if(frameHist) delete frameHist;
      frameHist=new TGCompositeFrame(winHist,800,700);
      winHist->AddFrame(frameHist,tabLayout);
      if(histECanWin) delete histECanWin;
      histECanWin = new TRootEmbeddedCanvas("Histogram", frameHist,800,700);
      //calibECanWin->SetAutoFit();
      frameHist->AddFrame(histECanWin,tabLayout);
      histogramTCanvas = histECanWin->GetCanvas();

      DrawHistogram();
      histogramTCanvas->SetEditable(kFALSE);
  
     
      winHist->Connect("CloseWindow()","CLEANViewer",this,"HistWindowClosed()");
      winHist->DontCallClose();
      winHist->MapSubwindows();
      winHist->MapWindow();
     
    }

  /*
  if(id==25)
    {
      man->ClearPhysicalNodes();
      delete man;
      man=NULL;
      //      delete displayTView;
      //displayTView=NULL;
      popupUnsnap->DisableEntry(25);
      if(man) 
	{
	  delete man;
	  }
      //displayTCanvas->SetEditable(kTRUE);
      //displayTCanvas->Clear();
      //displayECanvas->Clear();
      //displayTCanvas->Update();
      //displayTCanvas->Modified();
      

      
      guiTab->SetEnabled(2,kFALSE);
    
    
      TGMainFrame * winDisplay=new TGMainFrame(gClient->GetRoot(),800,700);
      winDisplay->SetWindowName("Event Display");
      TGCompositeFrame * tabDisplayWin=new TGCompositeFrame(winDisplay,800,700);
      winDisplay->AddFrame(tabDisplayWin,tabLayout);
      TRootEmbeddedCanvas * displayECanWin = new TRootEmbeddedCanvas("Display", tabDisplayWin,800,700);
      //calibECanWin->SetAutoFit();
      tabDisplayWin->AddFrame(displayECanWin,tabLayout);
      displayTCanvas = displayECanWin->GetCanvas();

      DrawEventDisplay3D();
      displayTCanvas->SetEditable(kTRUE);
  
     

      winDisplay->Connect("CloseWindow()","CLEANViewer",this,"DisplayWindowClosed()");
      
      winDisplay->MapSubwindows();
      winDisplay->MapWindow();
     
    }*/

  if(id==26)
    {
      if(Draw3D)
	{
	  popupView->UnCheckEntry(26);
	}
      else
	popupView->CheckEntry(26);

      Draw3D=!Draw3D;
      DrawEventDisplay3D();
      //displayTCanvas->Update();
      //displayTCanvas->Modified();



    }
}


void CLEANViewer::RATMenu(Int_t id)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::RATMenu(Int_t id)\n");

  if (id == 21)
    NextEventPressed();
  if (id == 22)
    PrevEventPressed();
  if (id == 23)
    UncheckAllPMTs();
  if (id == 24) 
    CheckAllPMTs();
}

void CLEANViewer::EVMenu(Int_t id)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::EVMenu(Int_t id)\n");

  ratEV = ratDS->GetEV(id-3000);

  DrawEventDisplay3D();

  BuildMaps();
  
  LoadChannels();

  PlotChannel();

  Int_t entry = 3000;
  while (popupEV->GetEntry(entry) != 0)
    {
      popupEV->UnCheckEntry(entry);
      entry++;
    }
  popupEV->CheckEntry(id);
}

void CLEANViewer::WFDMenu(Int_t id)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::WFDMenu(Int_t id)\n");
  
  Int_t boardID = 0;

  if((id-80) < 0)
    boardID = id-60;
  else
    boardID = id-80;

 


  if((id-boardID)==60) 
    {
      wfdGroupVector[boardID]->GetButton(boardID)->SetState(kButtonDown);
      NChannelsClickedOnBoardVector[boardID]=8;
    }

  else  
    {
      wfdGroupVector[boardID]->GetButton(boardID)->SetState(kButtonUp);
      NChannelsClickedOnBoardVector[boardID]=0;
    }

  for (unsigned int iPMT=0;iPMT<pmtCheckInfoVector.size();iPMT++)
    {
     
      if (boardID==pmtCheckInfoVector[iPMT].Board)
	{
	  if((id-boardID) == 60)
	    {
	      pmtCheckInfoVector[iPMT].CheckButton->SetState(kButtonDown);
	      pmtCheckInfoVector[iPMT].CheckOrderButton->SetState(kButtonDown);
	    }
	  else
	    {
	      pmtCheckInfoVector[iPMT].CheckButton->SetState(kButtonUp);
	      pmtCheckInfoVector[iPMT].CheckOrderButton->SetState(kButtonUp);
	    }

	 
	  // DrawEventDisplay3D();
	    
	}
	
    }
  PlotChannel();
  //displayTabTCanvas->Update();
  //displayTabTCanvas->Modified();
}

void CLEANViewer::DQMMenu(Int_t id)
{
 
  if(id>10 and id<100)
    {
      filterNumSelected=id-11;
      FilterWindow();
    }
  if(id>100 and id<399)
    {
      filterNumSelected=id-101;
      
      RemoveFilter(filterNumSelected);

    }
  
  if(id==1)
    {
      AddNewFilter();
    }

  if(id==2)
    {
      StreamStop();
     
    }

 if(id==3)
   {
     StreamGo(); 
   }
 if(id>3 and (id-4)<(int)ratEVBufferVector.size())
   {
    
     ratDS->PruneEV();
     ratEV=new RAT::DS::EV(ratEVBufferVector[id-4]);
     ratDS->AddEV(ratEV);

     NumberEV(kTRUE);
     PlotChannel();
   }
 if(id==400)
   {
     LocalFilterWindow();
   }
}

void CLEANViewer::ColorScaleSelected(Int_t id)
{
  if(id!=colorScaleState)
    {
      if(id)colorScaleMaxEntry->SetState(kTRUE);
      else colorScaleMaxEntry->SetState(kFALSE);
      if(id==1) 
	{
	  colorUnitsText->SetText("pC");
	  colorScaleMax=50;
	}
      else if(id==2)
	{
	  colorUnitsText->SetText("ns");
	  colorScaleMax=5000;
	}
      colorScaleMaxEntry->SetNumber(colorScaleMax);
      InfoFrame->Layout();
      InfoFrame->MapSubwindows();
      colorScaleState=id;
      DrawEventDisplay3D();
    }
 
}

void CLEANViewer::ColorScaleChange()
{
  colorScaleMax=colorScaleMaxEntry->GetNumberEntry()->GetNumber();
  DrawEventDisplay3D();
}

void CLEANViewer::SizeScaleSelected(Int_t id)
{
  if(id!=sizeScaleState)
    {
      if(id)sizeScaleMaxEntry->SetState(kTRUE);
      else sizeScaleMaxEntry->SetState(kFALSE);
      sizeScaleState=id;
      
      DrawEventDisplay3D();
    }

}

void CLEANViewer::SizeScaleChange()
{
  sizeScaleMax=sizeScaleMaxEntry->GetNumberEntry()->GetNumber();
  DrawEventDisplay3D();
}


void CLEANViewer::ThreeDMenu(Int_t id)
{
  if(id==11)
    {
      popup3DSize->CheckEntry(11);
      popup3DSize->UnCheckEntry(12);
      sizeScaleState=0;
    }
  if(id==12)
    {
      popup3DSize->UnCheckEntry(11);
      popup3DSize->CheckEntry(12);
      sizeScaleState=1;
   
    }
  if(id==21)
    {
      popup3DColor->CheckEntry(21);
      popup3DColor->UnCheckEntry(22);
      colorScaleState=0;
    }
  if(id==22)
    {
      popup3DColor->UnCheckEntry(21);
      popup3DColor->CheckEntry(22);
      colorScaleState=1;
   
    }
  DrawEventDisplay3D();


}
void CLEANViewer::RemoveFilter(int32_t filterNum)
{
  for(size_t i=0;i<zmqEventFilterVec.size();i++)
    {
      popupFilters->DeleteEntry(i+11);
      popupRemoveFilters->DeleteEntry(i+101);
    }
  zmqEventFilterVec.erase(zmqEventFilterVec.begin()+filterNum);
  filterNames.erase(filterNames.begin()+filterNum);
  
  for(size_t i=0;i<zmqEventFilterVec.size();i++)
    {
      popupFilters->AddEntry(filterNames[i].c_str(),11+i);
      popupRemoveFilters->AddEntry(filterNames[i].c_str(),101+i);
    }

  ZMQ::Request * zmqReq=new ZMQ::Request();
  zmqReq->req=ZMQ::REQREMOVEFILTER;
  zmqReq->port=zmqPort;
  zmqReq->filterNum=filterNum;
  zmq::message_t messg(sizeof(ZMQ::Request)+sizeof(StreamFilter));

  memcpy(messg.data(),zmqReq,sizeof(ZMQ::Request));
  memcpy((uint8_t*)messg.data()+sizeof(ZMQ::Request),zmqEventFilterVec[filterNum],sizeof(StreamFilter));

  daqSocketRequest->send(messg);
  
  zmq::message_t response;


  daqSocketRequest->recv(&response);
  
  ZMQ::MessageHead MssgHead;
  memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
  uint8_t msg=MssgHead.mssg;
  if(msg!=ZMQ::MSGRECVDGOOD)
    {
      if(msg==ZMQ::MSGWRNGVERSION)
	{
	  fprintf(stderr,"Wrong ZMQ Version.  Please svn update RAT, and recompile RAT and CLEANViewer.  If this does not fix the problem, please contact someone in charge of DCDAQ. \n");
	  CloseWindow();
	}
      else if(msg==ZMQ::MSGBADMAGIC)
	{
	  fprintf(stderr,"Bad Magic word sent to DCDAQ. Shutting Down \n");
	  CloseWindow();
	}
      else if (msg==ZMQ::MSGBADPORT)
	{
	  fprintf(stderr,"Forgot to set port in message to DCDAQ.  Shutting down \n");
	  CloseWindow();
	}
      else
	{

	  fprintf(stderr,"ZMQ Messaging error \n");
	  CloseWindow();
	}
    }
  delete zmqReq;
}
void CLEANViewer::AddNewFilter()
{
  zmqEventFilterVec.push_back(new StreamFilter());
  char nameBuf[100];
  sprintf(nameBuf,"Filter %zu",zmqEventFilterVec.size()-1);
  filterNames.push_back(nameBuf);
  zmqEventFilterVec.back()->period=2;
  zmqEventFilterVec.back()->ReducLevels=0;

  popupFilters->AddEntry(nameBuf,zmqEventFilterVec.size()+10);
  popupRemoveFilters->AddEntry(nameBuf,zmqEventFilterVec.size()+100);
  filterNumSelected=zmqEventFilterVec.size()-1;
  FilterWindow();
  
}



void CLEANViewer::FilterWindow()
{
 
  if(!winFilter)
    {
      for(size_t i=0;i<zmqEventFilterVec.size();i++)
	popupDQM->DisableEntry(11+i);

      winFilter=new TGMainFrame(gClient->GetRoot(),250,600);
      winFilter->SetWindowName("DQM Filters");
      winFilterFrame=new TGVerticalFrame(winFilter,250,600,kFixedWidth | kFixedHeight);
      winFilter->AddFrame(winFilterFrame,tabLayout);
      char * title=new char;
      sprintf(title,"Period %i s",zmqEventFilterVec[filterNumSelected]->period);
      zmqPeriodGroup=new TGGroupFrame(winFilterFrame,title);
      delete title;
      zmqPeriodGroup->SetTitlePos(TGGroupFrame::kLeft);
      winFilterFrame->AddFrame(zmqPeriodGroup,new TGLayoutHints(kLHintsTop | 
								kLHintsCenterX |
								kLHintsExpandX,
								10,10,10,10));
      zmqPeriodFrame=new TGVerticalFrame(zmqPeriodGroup,250,25);
      zmqPeriodGroup->AddFrame(zmqPeriodFrame,tabLayout);
      zmqPeriodNumberEntry = new TGNumberEntry(zmqPeriodFrame, 0, 15, 7000,
					       TGNumberFormat::kNESInteger,
					       TGNumberFormat::kNEANonNegative,
					       TGNumberFormat::kNELLimitMin,
					       0);
      zmqPeriodNumberEntry->SetIntNumber(zmqEventFilterVec[filterNumSelected]->period);
      zmqPeriodFrame->AddFrame(zmqPeriodNumberEntry,tabLayout);
 
  
      zmqReductionLevelGroup=new TGGroupFrame(winFilterFrame,"Reduction Level");
      zmqReductionLevelGroup->SetTitlePos(TGGroupFrame::kLeft);
      winFilterFrame->AddFrame(zmqReductionLevelGroup,new TGLayoutHints(kLHintsTop | 
									kLHintsCenterX |
									kLHintsExpandX,
									10,10,10,10));
      zmqReductionLevelFrame=new TGHorizontalFrame(zmqReductionLevelGroup,250,25);
      zmqReductionLevelGroup->AddFrame(zmqReductionLevelFrame,tabLayout);

      zmqReductionLabelFrame=new TGVerticalFrame(zmqReductionLevelFrame);
      zmqReductionLevelFrame->AddFrame(zmqReductionLabelFrame,tabLayout);
  
      zmqReductionCheckFrame=new TGVerticalFrame(zmqReductionLevelFrame);
      zmqReductionLevelFrame->AddFrame(zmqReductionCheckFrame);
      char TempLabelBuff[100];
 
 
      for(ReductionLevel::Type iReductionLevel=ReductionLevel::Type(0);iReductionLevel<ReductionLevel::COUNT;iReductionLevel=ReductionLevel::Type((int)iReductionLevel+1))
	{
     
	  sprintf(TempLabelBuff,ReductionLevel::GetReductionLevelName(iReductionLevel));
	  TGLabel * TempLabel=new TGLabel(zmqReductionLabelFrame,TempLabelBuff);
	  vectorReductionLabels.push_back(TempLabel);
	  zmqReductionLabelFrame->AddFrame(vectorReductionLabels.back(),tabLayout);

	  TGCheckButton * TempCheckButton=new TGCheckButton(zmqReductionCheckFrame);
	  vectorReductionChecks.push_back(TempCheckButton);
	  if(zmqEventFilterVec[filterNumSelected]->ReducLevels & (1<<iReductionLevel))
	    {
	      vectorReductionChecks.back()->SetState(kButtonDown);
	      vectorReductionLabels.back()->SetTextColor(0x006600);
	    }
	  else
	    {
	      vectorReductionChecks.back()->SetState(kButtonUp);
	      vectorReductionLabels.back()->SetTextColor(0xff0000);
	      
	    }
	  zmqReductionCheckFrame->AddFrame(vectorReductionChecks.back(),tabLayout);
	}
 


      zmqHardwareTrigGroup=new TGGroupFrame(winFilterFrame,"Hardware Trigger");
      zmqHardwareTrigGroup->SetTitlePos(TGGroupFrame::kLeft);
      winFilterFrame->AddFrame(zmqHardwareTrigGroup,new TGLayoutHints(kLHintsTop | 
									kLHintsCenterX |
									kLHintsExpandX,
									10,10,10,10));
      zmqHardwareTrigFrame=new TGHorizontalFrame(zmqHardwareTrigGroup,250,25);
      zmqHardwareTrigGroup->AddFrame(zmqHardwareTrigFrame,tabLayout);

      zmqHardwareLabelFrame=new TGVerticalFrame(zmqHardwareTrigFrame);
      zmqHardwareTrigFrame->AddFrame(zmqHardwareLabelFrame,tabLayout);
  
      zmqHardwareCheckFrame=new TGVerticalFrame(zmqHardwareTrigFrame);
      zmqHardwareTrigFrame->AddFrame(zmqHardwareCheckFrame);

 
 
      for(int i=0;i<8;i++)
	{
	  TGLabel * TempLabel;
	  switch(i)
	    {
	    case 0:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Veto");
	      break;
	    case 1:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Hitsum");
	      break;
	    case 2:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"FPGA");
	      break;
	    case 3:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Software");
	      break;
	    case 4:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Cal 1");
	      break;
	    case 5:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Cal 2");
	      break;
	    case 6:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Cal 3");
	      break;
	    case 7:
	      TempLabel=new TGLabel(zmqHardwareLabelFrame,"Periodic");
	      break;
	      
	      }
     
	 
	  
	  vectorHardwareLabels.push_back(TempLabel);
	  zmqHardwareLabelFrame->AddFrame(vectorHardwareLabels.back(),tabLayout);

	  TGCheckButton * TempCheckButton=new TGCheckButton(zmqHardwareCheckFrame);
	  vectorHardwareChecks.push_back(TempCheckButton);
	  if(zmqEventFilterVec[filterNumSelected]->HardwareTrig & (1<<i))
	    {
	      vectorHardwareChecks.back()->SetState(kButtonDown);
	      vectorHardwareLabels.back()->SetTextColor(0x006600);
	    }
	  else
	    {
	      vectorHardwareChecks.back()->SetState(kButtonUp);
	      vectorHardwareLabels.back()->SetTextColor(0xff0000);
	    }
	  zmqHardwareCheckFrame->AddFrame(vectorHardwareChecks.back(),tabLayout);
	}
 

      winFilter->Connect("CloseWindow()","CLEANViewer",this,"FilterWindowClosed()");

      winFilter->DontCallClose();
      sendButton = new TGTextButton(winFilterFrame, 
				    "   &Send >>   ", 
				    7001);

      winFilterFrame->AddFrame(sendButton, new TGLayoutHints(kLHintsTop | kLHintsCenterX));
      sendButton->Connect("Pressed()","CLEANViewer",this,"SendFilterFromWindow()");
      winFilter->Layout();
      winFilter->MapSubwindows();
      winFilter->MapWindow();
      }
 
}


void CLEANViewer::LocalFilterWindow()
{
  
  if(!winFilterLocal)
    {

      popupDQM->DisableEntry(400);
      winFilterLocal=new TGMainFrame(gClient->GetRoot(),250,600);
      winFilterLocal->SetWindowName("Local Filter");
      
      winFilterFrameLocal=new TGVerticalFrame(winFilterLocal,250,600,kFixedWidth | kFixedHeight);
      winFilterLocal->AddFrame(winFilterFrameLocal,tabLayout);
     
      
  
      LocalReductionLevelGroup=new TGGroupFrame(winFilterFrameLocal,"Reduction Level");
      LocalReductionLevelGroup->SetTitlePos(TGGroupFrame::kLeft);
      winFilterFrameLocal->AddFrame(LocalReductionLevelGroup,new TGLayoutHints(kLHintsTop | 
									kLHintsCenterX |
									kLHintsExpandX,
									10,10,10,10));
      
      LocalReductionLevelFrame=new TGHorizontalFrame(LocalReductionLevelGroup,250,25);
      LocalReductionLevelGroup->AddFrame(LocalReductionLevelFrame,tabLayout);

      LocalReductionLabelFrame=new TGVerticalFrame(LocalReductionLevelFrame);
      LocalReductionLevelFrame->AddFrame(LocalReductionLabelFrame,tabLayout);
  
      LocalReductionCheckFrame=new TGVerticalFrame(LocalReductionLevelFrame);
      LocalReductionLevelFrame->AddFrame(LocalReductionCheckFrame);
      
      char TempLabelBuff[100];
 
 
      for(ReductionLevel::Type iReductionLevel=ReductionLevel::Type(0);iReductionLevel<ReductionLevel::COUNT;iReductionLevel=ReductionLevel::Type((int)iReductionLevel+1))
	{
     
	  sprintf(TempLabelBuff,ReductionLevel::GetReductionLevelName(iReductionLevel));
	  TGLabel * TempLabel=new TGLabel(LocalReductionLabelFrame,TempLabelBuff);
	  LocalvectorReductionLabels.push_back(TempLabel);
	  LocalReductionLabelFrame->AddFrame(LocalvectorReductionLabels.back(),tabLayout);

	  TGCheckButton * TempCheckButton=new TGCheckButton(LocalReductionCheckFrame);
	  LocalvectorReductionChecks.push_back(TempCheckButton);
	  if(LocalFilter->ReducLevels & (1<<iReductionLevel))
	    {
	      LocalvectorReductionChecks.back()->SetState(kButtonDown);
	      LocalvectorReductionLabels.back()->SetTextColor(0x006600);
	    }
	  else
	    {
	      LocalvectorReductionChecks.back()->SetState(kButtonUp);
	      LocalvectorReductionLabels.back()->SetTextColor(0xff0000);
	      
	    }
	  LocalReductionCheckFrame->AddFrame(LocalvectorReductionChecks.back(),tabLayout);
	}
      


      LocalHardwareTrigGroup=new TGGroupFrame(winFilterFrameLocal,"Hardware Trigger");
      LocalHardwareTrigGroup->SetTitlePos(TGGroupFrame::kLeft);
      winFilterFrameLocal->AddFrame(LocalHardwareTrigGroup,new TGLayoutHints(kLHintsTop | 
									kLHintsCenterX |
									kLHintsExpandX,
									10,10,10,10));
      LocalHardwareTrigFrame=new TGHorizontalFrame(LocalHardwareTrigGroup,250,25);
      LocalHardwareTrigGroup->AddFrame(LocalHardwareTrigFrame,tabLayout);

      LocalHardwareLabelFrame=new TGVerticalFrame(LocalHardwareTrigFrame);
      LocalHardwareTrigFrame->AddFrame(LocalHardwareLabelFrame,tabLayout);
  
      LocalHardwareCheckFrame=new TGVerticalFrame(LocalHardwareTrigFrame);
      LocalHardwareTrigFrame->AddFrame(LocalHardwareCheckFrame);

      
 
      for(int i=0;i<8;i++)
	{
	  TGLabel * TempLabel;
	  switch(i)
	    {
	    case 0:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Veto");
	      break;
	    case 1:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Hitsum");
	      break;
	    case 2:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"FPGA");
	      break;
	    case 3:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Software");
	      break;
	    case 4:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Cal 1");
	      break;
	    case 5:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Cal 2");
	      break;
	    case 6:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Cal 3");
	      break;
	    case 7:
	      TempLabel=new TGLabel(LocalHardwareLabelFrame,"Periodic");
	      break;
	      
	      }
     
	 
	  
	  LocalvectorHardwareLabels.push_back(TempLabel);
	  LocalHardwareLabelFrame->AddFrame(LocalvectorHardwareLabels.back(),tabLayout);

	  TGCheckButton * TempCheckButton=new TGCheckButton(LocalHardwareCheckFrame);
	  LocalvectorHardwareChecks.push_back(TempCheckButton);
	  if(LocalFilter->HardwareTrig & (1<<i))
	    {
	      LocalvectorHardwareChecks.back()->SetState(kButtonDown);
	      LocalvectorHardwareLabels.back()->SetTextColor(0x006600);
	    }
	  else
	    {
	      LocalvectorHardwareChecks.back()->SetState(kButtonUp);
	      LocalvectorHardwareLabels.back()->SetTextColor(0xff0000);
	    }
	  LocalHardwareCheckFrame->AddFrame(LocalvectorHardwareChecks.back(),tabLayout);
	}
      

      char title[100];
      sprintf(title,"Charge Cut > %.1f pC",LocalFilter->TotalQ);
      LocalChargeGroup=new TGGroupFrame(winFilterFrameLocal,title);
      
      
      LocalChargeGroup->SetTitlePos(TGGroupFrame::kLeft);
      winFilterFrameLocal->AddFrame(LocalChargeGroup,new TGLayoutHints(kLHintsTop | 
								kLHintsCenterX |
								kLHintsExpandX,
								10,10,10,10));
      LocalChargeFrame=new TGVerticalFrame(LocalChargeGroup,250,25);
      LocalChargeGroup->AddFrame(LocalChargeFrame,tabLayout);
      LocalChargeEntry = new TGNumberEntry(LocalChargeFrame, 0, 15, 7002,
					       TGNumberFormat::kNESReal,
					       TGNumberFormat::kNEANonNegative,
					       TGNumberFormat::kNELLimitMin,
					       0);
      LocalChargeEntry->SetNumber(LocalFilter->TotalQ);
      LocalChargeFrame->AddFrame(LocalChargeEntry,tabLayout);
      
 
      
      winFilterLocal->Connect("CloseWindow()","CLEANViewer",this,"LocalFilterWindowClosed()");
      
      winFilterLocal->DontCallClose();
      
      LocalsendButton = new TGTextButton(winFilterFrameLocal, 
				    "   &Set >>   ", 
				    7003);

      winFilterFrameLocal->AddFrame(LocalsendButton, new TGLayoutHints(kLHintsTop | kLHintsCenterX));
      LocalsendButton->Connect("Pressed()","CLEANViewer",this,"SetLocalFilter()");
      
      winFilterLocal->Layout();
      winFilterLocal->MapSubwindows();
      winFilterLocal->MapWindow();
      }
 
}

/*
void CLEANViewer::SleepWindow()
{
  if(!winSleep)
    {
      winSleep=new TGMainFrame(gClient->GetRoot(),400,600);
      winSleep->SetWindowName("DCDAQ Hasn't seent anything in a while...");
      canSleep=new TRootEmbeddedCanvas("SleepCanvas",winSleep,400,600);
      winSleep->AddFrame(canSleep, tabLayout);
      canSleep->GetCanvas()->cd();
      imageSleep=TImage::Open("sleeptest.jpg");
      imageSleep->Draw();
      winSleep->Connect("CloseWindow()","CLEANViewer",this,"SleepWindowClosed()");
      winSleep->Layout();
      winSleep->MapSubwindows();
      winSleep->MapWindow();
    }

    }*/


void CLEANViewer::SetNewAlarm()
{
  alarm(0);
  alarm(timeOutFactor*zmqEventFilterVec.back()->period);
}
  
void CLEANViewer::SendFilterFromWindow()
{
  SetFilter(filterNumSelected);
  SendFilterRequest(filterNumSelected);
  //popupDQM->EnableEntry(1);
  //fprintf(stderr,"about to fwc \n");
 
  //FilterWindowClosed();
  
  //winFilter->CloseWindow();
  //winFilter->Emit("CloseWindow()");
  //FilterWindowClosed();
  
}

void CLEANViewer::SetLocalFilter()
{

  


  LocalFilter->ReducLevels=0;
  for(int iRL=0;iRL<ReductionLevel::COUNT;iRL++)
    {
      if(LocalvectorReductionChecks[iRL]->IsDown())
	{
	  LocalFilter->ReducLevels=LocalFilter->ReducLevels | 1<<iRL;
	}
    }
  LocalFilter->HardwareTrig=0;
  for(int iHT=0;iHT<8;iHT++)
    {
      if(LocalvectorHardwareChecks[iHT]->IsDown())
	{
	  LocalFilter->HardwareTrig=LocalFilter->HardwareTrig | 1<<iHT;
	}
    }
  LocalFilter->TotalQ=LocalChargeEntry->GetNumber();
  if(winFilterLocal)
    {

      for(int iRL=0;iRL<ReductionLevel::COUNT;iRL++)
	{
	  if(LocalvectorReductionChecks[iRL]->IsDown())
	    {
	      LocalvectorReductionLabels[iRL]->SetTextColor(0x006600);
	    }
	  else
	    {
	      LocalvectorReductionLabels[iRL]->SetTextColor(0xff0000);
	    }
	}

      for(int iHT=0;iHT<8;iHT++)
	{
	  if(LocalvectorHardwareChecks[iHT]->IsDown())
	    {
	      LocalvectorHardwareLabels[iHT]->SetTextColor(0x006600);
	    }
	  else
	    {
	      LocalvectorHardwareLabels[iHT]->SetTextColor(0xff0000);
	    }
	}

      char title[100];
      sprintf(title,"Charge Cut > %.1f pC",LocalFilter->TotalQ);
      LocalChargeGroup->SetTitle(title);
      


      winFilterLocal->Layout();
      winFilterLocal->MapSubwindows();
      winFilterLocal->MapWindow();
	  
    }

 
     

}

void CLEANViewer::SetFilter(int32_t filterNum)
{

  

  zmqEventFilterVec[filterNum]->period=zmqPeriodNumberEntry->GetIntNumber();
  zmqEventFilterVec[filterNum]->ReducLevels=0;
  for(int iRL=0;iRL<ReductionLevel::COUNT;iRL++)
    {
      if(vectorReductionChecks[iRL]->IsDown())
	{
	  zmqEventFilterVec[filterNum]->ReducLevels=zmqEventFilterVec[filterNum]->ReducLevels | 1<<iRL;
	}
    }
  zmqEventFilterVec[filterNum]->HardwareTrig=0;
  for(int iHT=0;iHT<8;iHT++)
    {
      if(vectorHardwareChecks[iHT]->IsDown())
	{
	  zmqEventFilterVec[filterNum]->HardwareTrig=zmqEventFilterVec[filterNum]->HardwareTrig | 1<<iHT;
	}
    }
  alarm(0); //clear old alarm
  alarm(timeOutFactor*zmqEventFilterVec[filterNum]->period);
 
     

}
void CLEANViewer::SendFilterRequest(int32_t filterNum)
{
  

  if(ZMQ)
    {
 
      ZMQ::Request * zmqReq=new ZMQ::Request();
      zmqReq->req=ZMQ::REQFILTER;
      zmqReq->port=zmqPort;
      zmqReq->filterNum=filterNum;
      zmq::message_t messg(sizeof(ZMQ::Request)+sizeof(StreamFilter));

      memcpy(messg.data(),zmqReq,sizeof(ZMQ::Request));
      memcpy((uint8_t*)messg.data()+sizeof(ZMQ::Request),zmqEventFilterVec[filterNum],sizeof(StreamFilter));

      daqSocketRequest->send(messg);
      
      zmq::message_t response;


      daqSocketRequest->recv(&response);
      
      ZMQ::MessageHead MssgHead;
      memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
      uint8_t msg=MssgHead.mssg;
      if(msg!=ZMQ::MSGRECVDGOOD)
	{
	  if(msg==ZMQ::MSGWRNGVERSION)
	    {
	      fprintf(stderr,"Wrong ZMQ Version.  Please svn update RAT, and recompile RAT and CLEANViewer.  If this does not fix the problem, please contact someone in charge of DCDAQ. \n");
	      CloseWindow();
	    }
	  else if(msg==ZMQ::MSGBADMAGIC)
	    {
	      fprintf(stderr,"Bad Magic word sent to DCDAQ. Shutting Down \n");
	      CloseWindow();
	    }
	  else if (msg==ZMQ::MSGBADPORT)
	    {
	      fprintf(stderr,"Forgot to set port in message to DCDAQ.  Shutting down \n");
	      CloseWindow();
	    }
	  else
	    {

	      fprintf(stderr,"ZMQ Messaging error \n");
	      CloseWindow();
	    }
	}
      
      if(winFilter)
	{
	  char * title=new char;
	  sprintf(title,"Period %i s",zmqEventFilterVec[filterNum]->period);
	  zmqPeriodGroup->SetTitle(title);
	  delete title;
	  for(int iRL=0;iRL<ReductionLevel::COUNT;iRL++)
	    {
	      if(vectorReductionChecks[iRL]->IsDown())
		{
		  vectorReductionLabels[iRL]->SetTextColor(0x006600);
		}
	      else
		{
		  vectorReductionLabels[iRL]->SetTextColor(0xff0000);
		}
	    }

	  for(int iHT=0;iHT<8;iHT++)
	    {
	      if(vectorHardwareChecks[iHT]->IsDown())
		{
		  vectorHardwareLabels[iHT]->SetTextColor(0x006600);
		}
	      else
		{
		  vectorHardwareLabels[iHT]->SetTextColor(0xff0000);
		}
	    }
	  winFilter->Layout();
	  winFilter->MapSubwindows();
	  winFilter->MapWindow();
	}
      alarm(timeOutFactor*zmqEventFilterVec[filterNum]->period);
      delete zmqReq;
    }


}



void CLEANViewer::StreamGo()
{
 


  popupDQM->DisableEntry(3);
 
  for(size_t i=0;i<ratEVBufferVector.size();i++)
    {
      popupBuffer->DisableEntry(i+4);
    }
  //send go signal to DCDAQ
  guiToolBar->GetButton(2007)->SetEnabled();    
  if(ZMQ)
    {
      if(InfoFrame)
	{
	  StartButton3D->SetEnabled(kFALSE);
	  StopButton3D->SetEnabled();
	}

      alarm(0);
      alarm(timeOutFactor*zmqEventFilterVec.back()->period);
      ZMQ::Request * zmq_req=new ZMQ::Request;
      zmq_req->req=ZMQ::REQGO;
      zmq_req->port=zmqPort;
	
      zmq::message_t message(sizeof(ZMQ::Request));
      memcpy(message.data(),zmq_req,sizeof(ZMQ::Request));
      daqSocketRequest->send(message);

      zmq::message_t response;

      daqSocketRequest->recv(&response);
      ZMQ::MessageHead MssgHead;
      memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
      uint8_t msg=MssgHead.mssg;
      if(msg!=ZMQ::MSGRECVDGOOD)
	{
	  if(msg==ZMQ::MSGWRNGVERSION)
	    {
	      fprintf(stderr,"Wrong ZMQ Version.  Please svn update RAT, and recompile RAT and CLEANViewer.  If this does not fix the problem, please contact someonw in charge of DCDAQ. \n");
	      CloseWindow();
	    }
	   else if(msg==ZMQ::MSGBADMAGIC)
	    {
	      fprintf(stderr,"Bad Magic word sent to DCDAQ. Shutting Down \n");
	      CloseWindow();
	    }
	  else if (msg==ZMQ::MSGBADPORT)
	    {
	      fprintf(stderr,"Forgot to set port in message to DCDAQ.  Shutting down \n");
	      CloseWindow();
	    }
	  else
	    {

	      fprintf(stderr,"ZMQ Messaging error \n");
	      CloseWindow();
	    }
	}

      delete zmq_req;
    }
  popupDQM->EnableEntry(2);
  guiToolBar->GetButton(2008)->SetEnabled(kFALSE);
 


}

void CLEANViewer::StreamStop()
{


  popupDQM->DisableEntry(2);
 
  guiToolBar->GetButton(2008)->SetEnabled();
  if(ZMQ)
    {
      if(InfoFrame)
	{
	  StartButton3D->SetEnabled();
	  StopButton3D->SetEnabled(kFALSE);
	}
      alarm(0);
      
      ZMQ::Request * zmq_req=new ZMQ::Request;
      zmq_req->req=ZMQ::REQPAUSE;
      zmq_req->port=zmqPort;
	 
      zmq::message_t message(sizeof(ZMQ::Request));
      memcpy(message.data(),zmq_req,sizeof(ZMQ::Request));
     
      daqSocketRequest->send(message);

      zmq::message_t response;
     
      daqSocketRequest->recv(&response);
      ZMQ::MessageHead MssgHead;
      memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
      uint8_t msg=MssgHead.mssg;
      if(msg!=ZMQ::MSGRECVDGOOD)
	{
	  if(msg==ZMQ::MSGWRNGVERSION)
	    {
	      fprintf(stderr,"Wrong ZMQ Version.  Please svn update RAT, and recompile RAT and CLEANViewer.  If this does not fix the problem, please contact someone in charge of DCDAQ. \n");
	      CloseWindow();
	    }
	   else if(msg==ZMQ::MSGBADMAGIC)
	    {
	      fprintf(stderr,"Bad Magic word sent to DCDAQ. Shutting Down \n");
	      CloseWindow();
	    }
	  else if (msg==ZMQ::MSGBADPORT)
	    {
	      fprintf(stderr,"Forgot to set port in message to DCDAQ.  Shutting down \n");
	      CloseWindow();
	    }
	  else
	    {

	      fprintf(stderr,"ZMQ Messaging error \n");
	      CloseWindow();
	    }
	}
      delete zmq_req;
    }
  popupDQM->EnableEntry(3);
  for(size_t i=0;i<ratEVBufferVector.size();i++)
    {
      popupBuffer->EnableEntry(i+4);

    }
  guiToolBar->GetButton(2007)->SetEnabled(kFALSE);
 


}

void CLEANViewer::PlotMenu(Int_t id)
{


}


//functions to make sure same check buttons are pushed on each of the selection canvases

void CLEANViewer::pmtOrderButtonClicked(Int_t id)
{ 
  int vecIndex=idGroupVectorMap[id];
  int vecOrderIndex=idOrderGroupVectorMap[id];
  pmtGroupVector[vecIndex]->GetButton(id)->SetState(pmtOrderGroupVector[vecOrderIndex]->GetButton(id)->GetState());

  if(pmtGroupVector[vecIndex]->GetButton(id)->GetState()==kButtonDown)
    NChannelsClickedOnBoardVector[vecIndex]+=1;
  else
    NChannelsClickedOnBoardVector[vecIndex]-=1;

  int wfd=vecIndex;
  /* bool wfdFull=true;
  for(UInt_t iCheckButton=0;iCheckButton<8;iCheckButton++)
    {
    
      if(!pmtGroupVector[vecIndex]->GetButton(idsOnBoardVector[vecIndex][iCheckButton])->IsDown())
	wfdFull=false;
	

	}*/
  if(NChannelsClickedOnBoardVector[vecIndex]==8) wfdGroupVector[vecIndex]->GetButton(wfd)->SetState(kButtonDown);
  else wfdGroupVector[vecIndex]->GetButton(wfd)->SetState(kButtonUp);

  PlotChannel();
  //DrawEventDisplay3D();
  //displayTCanvas->Update();
  //displayTCanvas->Modified();
  //displayTabTCanvas->Update();
  //displayTCanvas->Modified();
  //displayTabTCanvas->Modified();
}

void CLEANViewer::pmtButtonClicked(Int_t id)
{
 
  int vecIndex=idGroupVectorMap[id];
  int vecOrderIndex=idOrderGroupVectorMap[id];
 
 
  pmtOrderGroupVector[vecOrderIndex]->GetButton(id)->SetState(pmtGroupVector[vecIndex]->GetButton(id)->GetState());

 
  int wfd=vecIndex;

  if(pmtGroupVector[vecIndex]->GetButton(id)->GetState()==kButtonDown)
    NChannelsClickedOnBoardVector[vecIndex]+=1;
  else
    NChannelsClickedOnBoardVector[vecIndex]-=1;

  /*  bool wfdFull=true;
  for(UInt_t iCheckButton=0;iCheckButton<8;iCheckButton++)
    {

      if(!pmtGroupVector[vecIndex]->GetButton(idsOnBoardVector[vecIndex][iCheckButton])->IsDown())
	wfdFull=false;


	}*/

  if(NChannelsClickedOnBoardVector[vecIndex]==8) wfdGroupVector[vecIndex]->GetButton(wfd)->SetState(kButtonDown);
  else wfdGroupVector[vecIndex]->GetButton(wfd)->SetState(kButtonUp);
 
 
  PlotChannel();
 
  //DrawEventDisplay3D();
 
  //displayTCanvas->Update();
  //displayTabTCanvas->Update();
  //displayTCanvas->Modified();
  //displayTabTCanvas->Modified();
}

void CLEANViewer::PMTClicked(Int_t event,Int_t x,Int_t y,TObject* select)
{

  int pmtID;
  if(select->InheritsFrom(TText::Class()))
    {
      gPad->SetCursor(kPointer);
    }



  if(event!=11) 
    {
      
      return;
    }
  

  if(select->InheritsFrom(TText::Class()))
    {

      pmtID=select->GetUniqueID();
            
      int vecIndex=idGroupVectorMap[pmtID];
      if(pmtGroupVector[vecIndex]->GetButton(pmtID)->GetState()==kButtonDown)
	pmtGroupVector[vecIndex]->GetButton(pmtID)->SetState(kButtonUp);
      else
	pmtGroupVector[vecIndex]->GetButton(pmtID)->SetState(kButtonDown);
	 
      pmtButtonClicked(pmtID);
    }


}

void CLEANViewer::MouseOver3DViewer(TObject * obj, UInt_t state)
{
  
  if(obj && obj->GetUniqueID()<10000)
    {
  
      if(v)
	{
	  InfoTextPad->cd();
	  InfoTextPad->Clear();
	  if(InfoTopText)
	    {
	      delete InfoTopText;
	      InfoTopText=NULL;
	    }
	  InfoTopText=new TPaveText(.005,.995,.995,.005);


	  char InfoBuff[100];
	  sprintf(InfoBuff,"Data Source: %s",dataFile);
	  InfoTopText->AddText(InfoBuff);
       
	  sprintf(InfoBuff,"Run ID: %i",ratDS->GetRunID());
	  InfoTopText->AddText(InfoBuff);
	  sprintf(InfoBuff,"Event ID: %i",ratEV->GetEventID());
	  InfoTopText->AddText(InfoBuff);
	  sprintf(InfoBuff,"Trig Pattern: %#x",ratEV->GetTriggerPattern());
	  InfoTopText->AddText(InfoBuff);
	  sprintf(InfoBuff,"Reduction Level: %i",ratEV->GetReductionLevel());
	  InfoTopText->AddText(InfoBuff);
	  sprintf(InfoBuff,"Total Q in Event: %.2f pC",ratEV->GetTotalQ());
	  InfoTopText->AddText(InfoBuff);



      

       
	  sprintf(InfoBuff,"---------------MOUSE OVER--------------");
	  InfoTopText->AddText(InfoBuff);
	  //find wfd, channel, etc
	  int PMTID;
	  if(obj->GetUniqueID()<1000)
	    {
	      PMTID=(int)obj->GetUniqueID();
	    }
	  else
	    {
	      PMTID=(int)obj->GetUniqueID()-1000;
	    }
	  int i;
	  for(i=0;i<ratEV->GetPMTCount();i++)
	    {
	      if(ratEV->GetPMT(i)->GetChannelID()==PMTID)
		{
		  break;
		}
	    }
	  if(i<ratEV->GetPMTCount())
	    {

	      sprintf(InfoBuff,"PMT ID :%i",PMTID);
	      InfoTopText->AddText(InfoBuff);
	      sprintf(InfoBuff,"WFD : %i",OrderedBoards[ratEV->GetPMT(i)->GetBoardNumber()].GetID());//sprintf(InfoBuff,"WFD : %i",ratEV->GetBoard(ratEV->GetPMT(i)->GetBoardNumber())->GetID());
	      InfoTopText->AddText(InfoBuff);
	      sprintf(InfoBuff,"Input : %i",ratEV->GetPMT(i)->GetInputNumber());
	      InfoTopText->AddText(InfoBuff);
	      sprintf(InfoBuff,"Total Q : %.2f pC",ratEV->GetPMT(i)->GetTotalQ());
	      InfoTopText->AddText(InfoBuff);
	    }
	  InfoTopText->SetTextAlign();
	  InfoTopText->SetFillColor(0);
	  InfoTopText->SetBorderSize(0);
	  InfoTopText->SetTextSize(0.075);
	 
	  InfoTopText->Draw();
	  if(ratEV->GetReductionLevel()==ReductionLevel::CHAN_ZLE_WAVEFORM)
	    {
	      if(i<ratEV->GetPMTCount() and ratEV->GetPMT(i)->GetBlockCount()>0)
		{
		  DrawMiniWaveform(PMTID);
		}
	      else
		{
		  InfoPlotPad->Clear();
		}
	    }
	  if(ratEV->GetReductionLevel()==ReductionLevel::CHAN_PROMPT_TOTAL)
	    {
	      if(i<ratEV->GetPMTCount() and ratEV->GetPMT(i)->GetTotalQ()>0)
		{
		  DrawMiniWaveform(PMTID);
		}
	      else
		{
		  InfoPlotPad->Clear();
		}
	    }

	  InfoCanvas->cd();
	  InfoCanvas->Update();
	  InfoCanvas->Modified();








	}
    }
  else
    {
      CreateInfoWindow();
      
    }
  

}


/*
void CLEANViewer::PMTDisplayClicked(Int_t event,Int_t x,Int_t y,TObject* select)
{

  int pmtID;
  if(select->InheritsFrom(TArc::Class()))
    {     

      
      gPad->SetCursor(kPointer);

      if(calibrated)
	{

	  if((cursorOverPMT!=select->GetUniqueID()-1000)&(select->GetUniqueID()>999))
	    {

	      pmtID=select->GetUniqueID()-1000-PMTIDStart;
	      double x[5]={0,.4,.4,0,0};
	      double y[5]={0,0,.15,.15,0};
	      double xwf[5]={.7,.9,.9,.7,.7};
	      double ywf[5]={.05,.05,.15,.15,.05};
	      TPolyLine *Eraser=new TPolyLine(5,x,y);
	      TPolyLine *EraserWaveform=new TPolyLine(5,xwf,ywf);
	      Eraser->SetFillColor(0);
	      EraserWaveform->SetFillColor(0);
	  
	      displayTCanvas->SetEditable(kTRUE);
	      displayTCanvas->cd();
	      Eraser->Draw("f");
	      EraserWaveform->Draw("f");
	      EraserWaveform->Draw();
	      char labelBuff[25];

	      sprintf(labelBuff,"PMT %i, Charge %4.2f pC",pmtID,ratEV->GetPMT(pmtID)->GetTotalQ());
	      chargeValueText=new TText(.1,.1,labelBuff);
	      chargeValueText->SetTextSize(.02);
	      chargeValueText->Draw();
	      char labelWFDBuff[25];
	      sprintf(labelWFDBuff,"WFD %i, Channel %i",ratEV->GetPMT(pmtID)->GetBoardNumber(),ratEV->GetPMT(pmtID)->GetInputNumber());
	      TText *wfdChannelText=new TText(.1,.05,labelWFDBuff);
	      wfdChannelText->SetTextSize(.02);
	      wfdChannelText->Draw();

	      DrawMiniWaveform(pmtID);

	      displayTCanvas->SetEditable(kFALSE);
	      displayTCanvas->Update();
	      displayTCanvas->Modified();
	      cursorOverPMT=select->GetUniqueID()-1000;
	    }
	  gPad->SetCursor(kPointer);
	}
    }
	

    
  else
    {
      if(cursorOverPMT!=9999)
	{
	  double x[5]={0,.4,.4,0,0};
	  double y[5]={0,0,.15,.15,0};
	  double xwf[5]={.7,.9,.9,.7,.7};
	  double ywf[5]={.05,.05,.15,.15,.05};
	  TPolyLine *Eraser=new TPolyLine(5,x,y);
	  TPolyLine *EraserWaveform=new TPolyLine(5,xwf,ywf);
	  Eraser->SetFillColor(0);
	  EraserWaveform->SetFillColor(0);
	  
	  displayTCanvas->SetEditable(kTRUE);
	  displayTCanvas->cd();
	  Eraser->Draw("f");
	  EraserWaveform->Draw("f");
	  EraserWaveform->Draw();
	  displayTCanvas->SetEditable(kFALSE);
	  displayTCanvas->Update();
	  displayTCanvas->Modified();
	 
	  cursorOverPMT=9999;
	}
	    
      
    }
    


 
  if((event!=11)&(event!=13)) 
    {
      
      return;
    }
  
  if(select->InheritsFrom(TArc::Class()))
    {
      if(event==11)
	{
      
	  pmtID=select->GetUniqueID();
      
	  int vecIndex=idGroupVectorMap[pmtID];

	 
	 
	  if(pmtGroupVector[vecIndex]->GetButton(pmtID)->GetState()==kButtonDown)
	    pmtGroupVector[vecIndex]->GetButton(pmtID)->SetState(kButtonUp);
	  else
	    pmtGroupVector[vecIndex]->GetButton(pmtID)->SetState(kButtonDown);
	 
	  pmtButtonClicked(pmtID);


	}
       if(event==13)
	{
	  pmtID=select->GetUniqueID()-1000-PMTIDStart;
	  CreateCenteredArrays(pmtID) ;
	  DrawEventDisplay3D();
	  displayTCanvas->Update();
	  displayTCanvas->Modified();
	  }
    }


}
*/
void CLEANViewer::wfdButtonClicked(Int_t id)
{
  int wfd=id;
  int pmtID;
 
  
  sort(pmtCheckInfoVector.begin(),pmtCheckInfoVector.end(),OrderButtonsbyBoard);

  if(wfdGroupVector[wfd]->GetButton(wfd)->GetState()==kButtonDown)
    NChannelsClickedOnBoardVector[wfd]=8;
  else
    NChannelsClickedOnBoardVector[wfd]=0;

  for(int iPMT=8*wfd;iPMT<8*wfd+8;iPMT++)
    {
  
     
      pmtID=pmtCheckInfoVector[iPMT].PMTID;

      pmtCheckInfoVector[iPMT].CheckButton->SetState(wfdGroupVector[wfd]->GetButton(wfd)->GetState());
      pmtCheckInfoVector[iPMT].CheckOrderButton->SetState(wfdGroupVector[wfd]->GetButton(wfd)->GetState());

      //DrawEventDisplay3D();
	
	
      
      

    }

  PlotChannel();
  //displayTabTCanvas->Update();
  //displayTabTCanvas->Modified();
  //displayTCanvas->Update();
  //displayTCanvas->Modified();

}

void CLEANViewer::cursorSelectChecked()
{
  if(cursorSelectCheckButton->IsDown())
    AxisSelecting=true;
  else
    AxisSelecting=false;
}

void CLEANViewer::RawCanvasClicked(Int_t event,Int_t x,Int_t y,TObject* select)
{
  if(reductionLevel!=ReductionLevel::CHAN_PROMPT_TOTAL)
    {
      if(AxisSelecting)
	{
	  gPad->RecursiveRemove(selectEndLine);

	  if(event==1)
	    {

	      if(x<int(rawTCanvas->GetWw()*0.15))
		selectXStart=int(rawTCanvas->GetWw()*0.15);
	      else if(x>int(rawTCanvas->GetWw()*0.95))
		selectXStart=int(rawTCanvas->GetWw()*0.95);
	      else selectXStart=x;
	    }
  
	  if(event==21)
	    {
	      gPad->RecursiveRemove(selectStartLine);
	      double xLineStart;
	      double xLineEnd;
	      int xEnd;
	      if(x<int(rawTCanvas->GetWw()*0.15))
		xEnd=int(rawTCanvas->GetWw()*0.15);
	      else if(x>int(rawTCanvas->GetWw()*0.95))
		xEnd=int(rawTCanvas->GetWw()*0.95);
	      else xEnd=x;


	      xLineStart=(selectXStart-.15*float(rawTCanvas->GetWw()))/float(rawTCanvas->GetWw())+.15;
	      selectStartLine=new TLine(xLineStart,.1,xLineStart,.9);
	      xLineEnd=(xEnd-.15*float(rawTCanvas->GetWw()))/float(rawTCanvas->GetWw())+.15;
	      selectEndLine=new TLine(xLineEnd,.1,xLineEnd,.9);
      
	      selectStartLine->SetLineStyle(2);
	      selectEndLine->SetLineStyle(2);
	      rawTCanvas->SetEditable(kTRUE);
 

	      rawTCanvas->cd();


	      selectEndLine->Draw();
	      selectStartLine->Draw();
	
	      rawTCanvas->SetEditable(kFALSE);

     
	      rawTCanvas->Update();
	      rawTCanvas->Modified();

	    }
      

	  if(event==11)
	    {

	      gPad->RecursiveRemove(selectStartLine);
	      if((y<0)||(x<0)||(x>(int)rawTCanvas->GetWw())||(y>(int)rawTCanvas->GetWh())||(x==selectXStart))
		{
		  rawTCanvas->Update();
		  rawTCanvas->Modified();
		  return;
		}

	      int xMin;
	      int xMax;
	      if(x<selectXStart)
		{
	  
		  if(x<(int)(rawTCanvas->GetWw()*.15))
		    xMin=(int)(rawTCanvas->GetWw()*.15);
		  else if(x>(int)(rawTCanvas->GetWw()*.95))
		    xMin=(int)(rawTCanvas->GetWw()*.95);
		  else xMin=x;
	  
		  xMax=selectXStart;
		}
	      else
		{
		  xMin=selectXStart;

		  if(x<int(rawTCanvas->GetWw()*.15))
		    xMax=(int)(rawTCanvas->GetWw()*.15);
		  else if(x>(int)(rawTCanvas->GetWw()*.95))
		    xMax=(int)(rawTCanvas->GetWw()*.95);
		  else xMax=x;
		}
      
	      double zoomMin;
	      double zoomMax;


	      zoomMin=(zoomSlider->GetMaxPosition()-zoomSlider->GetMinPosition())*float(xMin-rawTCanvas->GetWw()*.15)/float(rawTCanvas->GetWw()*.8)+zoomSlider->GetMinPosition();
	      zoomMax=(zoomSlider->GetMaxPosition()-zoomSlider->GetMinPosition())*float(xMax-rawTCanvas->GetWw()*.15)/float(rawTCanvas->GetWw()*.8)+zoomSlider->GetMinPosition();
	      



	      zoomSlider->SetPosition(zoomMin,zoomMax);
     
	      zoomAdjusted=kTRUE;

	      PolyLineZoomAdjust();


	      rawTCanvas->Update();
	      rawTCanvas->Modified();
      
	      calibTCanvas->Update();
	      calibTCanvas->Modified();
	  

	    }
	}
    }
}

void CLEANViewer::CalibCanvasClicked(Int_t event,Int_t x,Int_t y,TObject* select)
{
  if(reductionLevel!=ReductionLevel::CHAN_PROMPT_TOTAL)
    {
      
      if(AxisSelecting)
	{
	  gPad->RecursiveRemove(selectEndLine);

	  if(event==1)
	    {

	      if(x<int(calibTCanvas->GetWw()*0.15))
		selectXStart=int(calibTCanvas->GetWw()*0.15);
	      else if(x>int(calibTCanvas->GetWw()*0.95))
		selectXStart=int(calibTCanvas->GetWw()*0.95);
	      else selectXStart=x;
	    }
  
	  if(event==21)
	    {
	      gPad->RecursiveRemove(selectStartLine);
	      double xLineStart;
	      double xLineEnd;
	      int xEnd;
	      if(x<int(calibTCanvas->GetWw()*0.15))
		xEnd=int(calibTCanvas->GetWw()*0.15);
	      else if(x>int(calibTCanvas->GetWw()*0.95))
		xEnd=int(calibTCanvas->GetWw()*0.95);
	      else xEnd=x;


	      xLineStart=(selectXStart-.15*float(calibTCanvas->GetWw()))/float(calibTCanvas->GetWw())+.15;
	      selectStartLine=new TLine(xLineStart,.1,xLineStart,.9);
	      xLineEnd=(xEnd-.15*float(calibTCanvas->GetWw()))/float(calibTCanvas->GetWw())+.15;
	      selectEndLine=new TLine(xLineEnd,.1,xLineEnd,.9);
      
	      selectStartLine->SetLineStyle(2);
	      selectEndLine->SetLineStyle(2);
	      calibTCanvas->SetEditable(kTRUE);
 

	      calibTCanvas->cd();


	      selectEndLine->Draw();
	      selectStartLine->Draw();
	
	      calibTCanvas->SetEditable(kFALSE);

     
	      calibTCanvas->Update();
	      calibTCanvas->Modified();

	    }
      

	  if(event==11)
	    {

	      gPad->RecursiveRemove(selectStartLine);
	      if((y<0)||(x<0)||(x>(int)calibTCanvas->GetWw())||(y>(int)calibTCanvas->GetWh())||(x==selectXStart))
		{
		  calibTCanvas->Update();
		  calibTCanvas->Modified();
		  return;
		}

	      int xMin;
	      int xMax;
	      if(x<selectXStart)
		{
	  
		  if(x<(int)(calibTCanvas->GetWw()*.15))
		    xMin=(int)(calibTCanvas->GetWw()*.15);
		  else if(x>(int)(calibTCanvas->GetWw()*.95))
		    xMin=(int)(calibTCanvas->GetWw()*.95);
		  else xMin=x;
	  
		  xMax=selectXStart;
		}
	      else
		{
		  xMin=selectXStart;

		  if(x<int(calibTCanvas->GetWw()*.15))
		    xMax=(int)(calibTCanvas->GetWw()*.15);
		  else if(x>(int)(calibTCanvas->GetWw()*.95))
		    xMax=(int)(calibTCanvas->GetWw()*.95);
		  else xMax=x;
		}
      
	      double zoomMin;
	      double zoomMax;


	      zoomMin=(zoomSlider->GetMaxPosition()-zoomSlider->GetMinPosition())*float(xMin-calibTCanvas->GetWw()*.15)/float(calibTCanvas->GetWw()*.8)+zoomSlider->GetMinPosition();
	      zoomMax=(zoomSlider->GetMaxPosition()-zoomSlider->GetMinPosition())*float(xMax-calibTCanvas->GetWw()*.15)/float(calibTCanvas->GetWw()*.8)+zoomSlider->GetMinPosition();
	      



	      zoomSlider->SetPosition(zoomMin,zoomMax);
     
	      zoomAdjusted=kTRUE;

	      PolyLineZoomAdjust();


	      rawTCanvas->Update();
	      rawTCanvas->Modified();
      
	      calibTCanvas->Update();
	      calibTCanvas->Modified();
	  

	    }
	}
    }
}

void CLEANViewer::SaveCanvas(char * path,int id)
{  switch(id)
    {
    case 30:
      rawTCanvas->Print(path);
      break;
    case 31:
      calibTCanvas->Print(path);
      break;
    case 33:
      histogramTCanvas->Print(path);
      break;
    }
}




void CLEANViewer::ZMQReadNewEvent()
{ 
  zmqhandler->Remove();
 
  //check to see if we really have anything

  int zmq_events;
  size_t zmq_events_size=sizeof(zmq_events);
  //int success;
  daqSocketData->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
    
  fprintf(stderr,"New Event\n");
  if(zmq_events & ZMQ_POLLIN)
    {
      fprintf(stderr,"New Event real\n");
      //if(winSleep)
      //SleepWindowClosed();
 
      alarm(0);
      //we've got something!!

      zmq::message_t message;
      daqSocketData->recv(&message);
      

      ZMQ::MessageHead MssgHead;
      memcpy(&MssgHead,message.data(),sizeof(ZMQ::MessageHead));
      uint8_t msg_type=MssgHead.mssg;
      if(msg_type==ZMQ::MSGSTOP)
	{
	  alarm(0);
	  CloseWindow();
	}
      else if(msg_type==ZMQ::MSGEVENT)
	{
	  //cleanup ratEV
	  if(ratEV)
	    {
	      ratDS->PruneEV();
	    }
	  ratDS->SetRunID(MssgHead.runNumber);
	  ratEV=ratDS->AddNewEV();
	  size_t message_size=message.size()-sizeof(ZMQ::MessageHead);
	  uint8_t* message_data=new uint8_t[message_size];
	  memcpy(message_data,(uint8_t*)message.data()+sizeof(ZMQ::MessageHead),message_size);

	  int32_t sizeEV=StreamBufftoEV(message_data,ratEV);
	  for(size_t i=0;i<ratEVBufferVector.size();i++)
	    {
	      popupBuffer->DeleteEntry(4+i);
	    }
	  bufferSize=bufferSizeNumberEntry->GetIntNumber();
	  if(ratEVBufferVector.size()>=bufferSize)
	    {
	      ratEVBufferVector.erase(ratEVBufferVector.begin());
	    }
	  ratEVBufferVector.push_back(*ratEV);
	  char entryLabel[100];
	  for(size_t i=0;i<ratEVBufferVector.size();i++)
	    {
	      sprintf(entryLabel,"%i",ratEVBufferVector[i].GetEventID());
	      popupBuffer->AddEntry(entryLabel,i+4);
	      popupBuffer->DisableEntry(i+4);
	     
	    }
	  fprintf(stderr,"%i\n",ratEV->GetBoardCount());
	  if(sizeEV<0) CloseWindow();
	  delete message_data;
	  if((size_t)sizeEV != message_size)
	    {
	      fprintf(stderr,"Error with socket streaming \n");
	      gApplication->Terminate();
	      return;
	    }

	  if(!FirstRead)
	    {

	      BuildMaps();
	      SetChannelLocations();
	  
	      //Get the Number of EV for combo box
	      NumberEV(kFALSE);




	      //initial filling of PMT checkboxes vector
	      FillPmtVector();

	      MapSubwindows();
	      Layout();
	      SetWindowName("CLEAN Event Viewer");
	      SetIconName("CLEAN Event Viewer");
	      MapWindow();

	      CheckAllPMTs();
	      FirstRead=true;
	    }
	  else
	    NumberEV(kTRUE);
      
	  PlotChannel ();

	  //clear socket in case too slow and pile up
	  int Cntr=0;
	  daqSocketData->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
	  while(zmq_events & ZMQ_POLLIN)
	    {
	      Cntr++;
	      zmq::message_t message;
	      daqSocketData->recv(&message);
	      ZMQ::MessageHead MssgHead;
	      memcpy(&MssgHead,message.data(),sizeof(ZMQ::MessageHead));
	      uint8_t msg_type=MssgHead.mssg;
	      if(msg_type==ZMQ::MSGSTOP)
		{
		  alarm(0);
		  CloseWindow();
		}
	      daqSocketData->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
	    }

      
      
	}
      else
	fprintf(stderr,"Bad message from socket \n");

      alarm(timeOutFactor*zmqEventFilterVec.back()->period);
    }

  fprintf(stderr,"Complete\n");
   

  zmqhandler->Add();



}

void CLEANViewer::ResetFile()
{
  delete ratReader;
  ratReader=NULL;
  SummedChargeVector.clear();
  pmtCheckInfoVector.clear();
  NChannelsClickedOnBoardVector.clear();
  /*for(int i=0;i<wfdMenuVector.size();i++)
    {
      fprintf(stderr,"%i\n",i);
      popupWFD->DeleteEntry(i);
    }*/
  popupWFD->Clear();
  pmtGroup->Cleanup();
  wfdMenuVector.clear();
  pmtGroupVector.clear();
  pmtCheckInfoVector.clear();
  idGroupVectorMap.clear();
  pmtIDsVector.clear();
  idsOnBoardVector.clear();
  wfdGroupVector.clear();
  pmtOrderGroupVector.clear();
  idOrderGroupVectorMap.clear();

  calibrated=false;
  drawPromptTotal=false;
  DAQdat=false;
  

}

void CLEANViewer::RawWindowClosed()
{
  rawTCanvas->Disconnect();
  /*  delete winRaw;
  winRaw=NULL;
  delete tabRawWin;
  tabRawWin=NULL;
  delete rawECanWin;
  rawECanWin=NULL;
  */

  popupUnsnap->EnableEntry(23);
  rawTCanvas=rawECanvas->GetCanvas();

  PlotChannel();

  guiTab->SetEnabled(0);

}

void CLEANViewer::HistWindowClosed()
{
  delete winHist;
  winHist=NULL;
  delete frameHist;
  frameHist=NULL;
  delete histECanWin;
  histECanWin=NULL;
  popupUnsnap->EnableEntry(25);
  histogramTCanvas=histogramECanvas->GetCanvas();
  DrawHistogram();
  guiTab->SetEnabled(2);

}
/*
void CLEANViewer:: InfoWindowClosed()
{
  delete InfoWindow;
  InfoWindow=NULL;
  delete InfoFrame;
  InfoFrame=NULL;
  delete InfoECanvas;
  InfoECanvas=NULL;
  delete InfoTopText;
  InfoTopText=NULL;
  delete InfoMidText;
  InfoMidText=NULL;
  //the following is not a memory leak because destructor of canvas deletes all subpads, even though I'm the one who called new so I should also be deleting, this doesn't seem to be the way root works it 
  InfoPlotPad=NULL;
  InfoTextPad=NULL;

}*/
void CLEANViewer::Viewer3DClosed()
{
  v=NULL;
  InfoFrame=NULL;
  InfoECanvas=NULL;
  InfoTextPad=NULL;
  InfoPlotPad=NULL;
  eventNumberEntry3D=NULL;
  eventGroup3D=NULL;
  eventHorizontal3D=NULL;
  prevEvent3D=NULL;
  nextEvent3D=NULL;
  highLightPMTButton=NULL;
  /*  if(ColorScaleWin)
    {
      ColorScaleWin->Emit("Closed()");
    }
  if(InfoWindow)
    {

      InfoWindow->Emit("CloseWindow()");
      }*/
}


  
void CLEANViewer::ColorScaleClosed()
{
  delete ColorScaleWin;
  ColorScaleWin=NULL;
  delete colorScaleFrame;
  delete colorScaleECan;
}

void CLEANViewer::CalibWindowClosed()
{/*
  delete winCalib;
  winCalib=NULL;
  delete tabCalibWin;
  tabCalibWin=NULL;
  delete calibECanWin;
  calibECanWin=NULL;
 */
  calibTCanvas->Disconnect();
  popupUnsnap->EnableEntry(24);
  calibTCanvas=calECanvas->GetCanvas();
  PlotChannel();
  guiTab->SetEnabled(1);

}

/*
void CLEANViewer::DisplayWindowClosed()
{
  popupUnsnap->EnableEntry(25);
  displayTCanvas=displayECanvas->GetCanvas();
  DrawEventDisplay3D();
  guiTab->SetEnabled(2);

  }*/
/*
void CLEANViewer::SleepWindowClosed()
{
  delete winSleep;
  winSleep=NULL;
  delete canSleep;
  canSleep=NULL;
  delete imageSleep;
  imageSleep=NULL;

}*/
void CLEANViewer::FilterWindowClosed()
{
 
  popupDQM->EnableEntry(1);

  delete zmqPeriodNumberEntry;
  zmqPeriodNumberEntry=NULL;

  delete sendButton;
  sendButton=NULL;
 
  delete winFilter;
  winFilter=NULL;
  
  delete winFilterFrame;
  winFilterFrame=NULL;
  delete zmqPeriodGroup;
  zmqPeriodGroup=NULL;
  delete zmqReductionLevelGroup;
  zmqReductionLevelGroup=NULL;
  delete zmqPeriodFrame;
  zmqPeriodFrame=NULL;
  delete zmqReductionLevelFrame;
  zmqReductionLevelFrame=NULL;
  delete zmqReductionLabelFrame;
  zmqReductionLabelFrame=NULL;
  delete zmqReductionCheckFrame;
  zmqReductionCheckFrame=NULL;
  vectorReductionLabels.clear();
  vectorReductionChecks.clear();
  delete zmqHardwareTrigGroup;
  zmqHardwareTrigGroup=NULL;
  delete zmqHardwareTrigFrame;
  zmqHardwareTrigFrame=NULL;
  delete zmqHardwareLabelFrame;
  zmqHardwareLabelFrame=NULL;
  delete zmqHardwareCheckFrame;
  zmqHardwareCheckFrame=NULL;
  vectorHardwareLabels.clear();
  vectorHardwareChecks.clear();


}

void CLEANViewer::LocalFilterWindowClosed()
{
 
  popupDQM->EnableEntry(400);



  delete LocalsendButton;
  LocalsendButton=NULL;
 
  delete winFilterLocal;
  winFilterLocal=NULL;
  
  delete winFilterFrameLocal;
  winFilterFrameLocal=NULL;
  delete LocalReductionLevelGroup;
  LocalReductionLevelGroup=NULL;
  delete LocalReductionLevelFrame;
  LocalReductionLevelFrame=NULL;
  delete LocalReductionLabelFrame;
  LocalReductionLabelFrame=NULL;
  delete LocalReductionCheckFrame;
  LocalReductionCheckFrame=NULL;
  LocalvectorReductionLabels.clear();
  LocalvectorReductionChecks.clear();
  delete LocalHardwareTrigGroup;
  LocalHardwareTrigGroup=NULL;
  delete LocalHardwareTrigFrame;
  LocalHardwareTrigFrame=NULL;
  delete LocalHardwareLabelFrame;
  LocalHardwareLabelFrame=NULL;
  delete LocalHardwareCheckFrame;
  LocalHardwareCheckFrame=NULL;
  LocalvectorHardwareLabels.clear();
  LocalvectorHardwareChecks.clear();
  delete LocalChargeGroup;
  LocalChargeGroup=NULL;
  delete LocalChargeFrame;
  LocalChargeFrame=NULL;
  delete LocalChargeEntry;
  LocalChargeEntry=NULL;

}




void CLEANViewer::Exit()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::Exit()\n");
  gApplication->Terminate(0);
}

//prevents seg fault when x button closes window
void CLEANViewer::CloseWindow()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::CloseWindow()\n"); 
  //Catch the close window.
  Exit();
}


//void CLEANViewer::CloseZMQ()
