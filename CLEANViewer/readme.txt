+---------------+
|  CLEANViewer  |
+---------------+

CLEANViewer event viewing software developed by Scott Stackley, under the direction of Dan Gastler and Ed Kearns for viewing monte carlo and DCDAQ events for the MiniCLEAN dark matter detector.

Current Version: alpha 2.0.1
Version Description: CLEANViewer alpha 2.0.1 is a unfinished working version of the event viewer capable of limited viewing of individual raw waveforms with multiple ZLE windows. Plotting of calibrated waveforms as well and statistics histograms is unavailable. Many GUI widgets have been included for future implementation including inactive menu/toolbar entries for unavailable capabilities, and unused plotting tabs. 

Bugs and Fixes are recorded on the B.U. superK wiki: hep.bu.edu/superkwiki/Bugs/Fixes

Please update this readme.txt as updates of CLEANViewer occur.

--------
Contents
--------

[1]  Install
     1.1 - Prerequisites
     1.2 - Build Steps
[2]  Running CLEANViewer
     2.1 -  Command Line Arguments
     2.2 -  (Quick Guide) Event Viewing
     2.3 -  Event Select
     2.4 -  PMT Select
     2.5 -  Raw Waveform Tab
     2.6 -  Calibrated Waveform Tab
     2.7 -  Event Display Tab
     2.8 -  X-Axis Adjust
     2.9 -  Toolbar Buttons
     2.10 -  Menu bar
     2.11 -  Using Debug Mode
[3]  Source Code
     3.1 -  Locating Functions
     3.2 -  GUI Widget ID Numbers
     3.3 -  CLEANViewer Class
     	 3.3.1 -  Function Descriptions/Locations
	 3.3.2 -  Caution
     3.4 -  Waveform Class
     	 3.4.1 -  Class Purpose
	 3.4.2 -  Function Descriptions/Locations
[4]  Appendix



------------
[1]  Install
------------

**1.1 - Prerequisites
      * RAT
      * ROOT 5.34.xx, be sure to add --enable-python --enable-roofit --enable-minuit2 --enable-opengl when you run the ROOT ./configure script
      * geant4
      * zmq 2.2.0 (or later, should hopefully be backwards compatible)
      * zmq.hpp, theses are zmq.hpp bindings available at https://github.com/zeromq/cppzmq
      * binutils
      * tclap
      * on Mac OS X, be sure you have the updated Xcode, and install Command Line Tools (in Preferences,Downlads,Components)

**1.2 - Build Steps
      * source the env.sh (env.csh) in rat
      * source the geant4.sh (geant4.csh) in geant4/bin
      * source the thisroot.sh (thisroot.csh) in root/bin
      * be sure that you have built the most updated version of rat (if you aren't sure, go to rat directory and "scons -c;scons"
      * the above the steps must be done in each session that you rebuild, so if you plan on editing the code and rebuilding a lot, it's probably a good idea to write a little shell script to do the above each time you need
      * make
      * if you are using osx, you may get some warnings like "warning: unused parameter 'shape'" and "warning: unused parameter 't'".  Don't worry about those.

-----------------------
[2] Running CLEANViewer
-----------------------


**2.1 -  Command Line Arguments

USAGE:
	./CLEANViewer [-D] [-f <file name>] [-Z <host:port> [-R #reductionlevel] [-p #period]][--] [--version] [-h] <file name>

Where:

	-D, --Debug
	    Switch for Debug Mode for CLEANViewer

	-f <file name>, --DataFileFlag <file name>
	    Initial RAT data file loaded into CLEANViewer. Overwrites the 'no flag' data file argument if both are used.  

	--, --ignore_rest
	    Ignores the rest of the labeled arguments following this flag.

	--version
	    Displays version information and exits.

	-h, --help
	    Displays usage information and exits.

	<file name>
	    Initial RAT data file loaded into CLEANViewer (no flag). Overwritten by DataFileFlag (-f) if both are used.

	-Z <host:port>
	    Using DQM mode.  Host is running DCDAQ, port is which port on DCDAQ to connect to to request events (set by DCDAQ)
	-R #reductionlevels
	    When running in DQM mode, tells DCDAQ to only send events of particular reduction level.  multiple reduction levels can be set, each with own -R flag.  
	-p #period
	    When running in DQM mode, tells DCDAQ to send events no faster than the specified period.  depending on how frequently the events are occuring, they may be sent less frequently than requested.  This is a lower limit on period.
	
	-s <ratServer:port>

**2.2 -  (Quick Guide) Event Viewing

      The quickest way to load a file into CLEANViewer is to include the file as a command line argument like so, 
    $./CLEANViewer /path_to_file/file_name
    This will load the file up in CLEANViewer.  It is suggested to get the full benifit of CLEANViewer to use data that has passed through the rat CalibrateWaveformProc.  This means either you must run you're data through the rat processor before opening it with CLEANViewer, or you should attach CLEANViewer to a ratserver.  The ratserver should be doing minimal analysis, just calibrate and (optionally) fitcentroid is a good amount.  To attach CLEANViewer to the ratserver add -s <ratServer:port> like so:
     $./CLEANViewer /path_to_file/file_name -s neutrino.bu.edu:7770
    You need to set up your ratserver seprately, this is very easy, see mac/MiniCLEAN/server.mac in rat for an example

    CLEANViewer can also be run in shift mode.  To do this replace the file name with the DCDAQ host and port number from which events will be streamed and use a -Z flag.  When running in shift mode it is suggested to attach to a ratserver to get the most out of CLEANViewer, like above.  So:
     $./CLEANViewer -Z cleanpc06.snolab.ca:9999 -s neutrino.bu.edu:7770
  		    
**2.3 -  Event Select

      The Event Select group contains 4 widgets for navigating through the events of the loaded RAT file; number entry, 'Prev Event' button, 'Next Event>>' button, and progress bar.
      The number entry displays the number of the current event being viewed. It also allows for the user to change between events by either typing the event number directly into the number entry or using the up/down arrows to the right of the text field. The 'Prev Event' and 'Next Event>>' buttons perform as expected and are an alternative to the up/down arrows included with the number entry widget. The buttons were added for their convenient size.
      Along with the number entry, that displays the actual event number, the progress bar helps determine the relative position of the current event amongst all the events in the file.


**2.4 -  PMT Selection

      Below the Event Select group is the main area for selecting PMTs to be drawn.  There are two buttons to 'Check All PMTs' and 'Uncheck All PMTs', whose operation should be obvious.  Below are three tabs for different selection modes.  'Channel Select' allows you to select by wfd/channel.  These channel numbers correspond to what channel a particular pmt is wired into on its WFD.  The 'PMT Select' tab allows for selection based on the PMT ID number.  This ID numbering system is related to the positions of the waveguides attached to each pmt in the detector, but does not necessarily correspond to the ordering of the WFDs and channels.  The 'Display Select' tab shows a small geometric image of the layout of each pmt's layout, and includes the PMT ID numbers.  When a pmt is selected in this tab it is colored in red.  PMTs can also be selected in the 'Event Display' tab, where they are outlined in black when selected.  All selection techniques are linked together, so they should all always show the same pmts selected.  However, it can sometimes take a few seconds for all the selection to record everywhere, particularly when many PMTs have been selected one right after the other, so be patient.    
      Often a RAT event will contain a pointer/pointers to PMT(s) that contain zero rawBlocks. These PMTs will have active check boxes but no poly line will be drawn (however there will be a space on the canvas where the polyline might have been drawn).


**2.5 -  Raw Waveform Tab

      The Raw Waveform tab contains the TCanvas where the rawBlocks from the checked PMTs are drawn as TPolylines. Also drawn to the canvas are the PMT labels, the file name, run ID, event ID, trigger pattern (hex), software trigger word (hex), fPrompt, qTotal, and QPE. The x-axis is in units of sample numbers (4 ns samples), the y-axis is in units of ADC counts. After 50 PMT boxes have been checked the PMT labels on the plot disappear due to overcrowding.
      The canvas can be saved to file by clicking on either the 'save canvas' button on the toolbar or the 'save canvas' File menu entry. Axis controls are described in the following section.

**2.6 - Calibrated Waveform Tab

      The Calibrated Waveform Tab shows the same information as the Raw Wveform Tab, except that the y-axis has been calibrated to voltage instead of to ADC Counts.

**2.7 - Event Display Tab

      The Event Display tab shows the amount of charge in each pmt distributed geometrically by their positions in the detector.  PMTs can be selected from this view, and when the cursor is placed over a pmt, information about that pmt appears on the canvas.  This image can also be centered on any pmt by right clicking on the choosen pmt and choosing 'Center On This PMT' fron the context menu.


**2.8 -  X-Axis Adjust

      Poorly named, the X-Axis Adjust group controls the x-axis of the waveforms.
      The double slider controls the range and scale of the x-axis. The scale on the slider goes from Sample Number 0 to Sample Number 10000. When a PMT is checked the scale for the x-axis is automatically set to fit all the ZLE windows for every PMT checked. By moving the slider bar from left to right you can adjust the range of the x-axis, by moving the edges of the slider you can increase or decrease the range of the x-axis. Practice makes perfect! To automatically set the x-axis range to 0-10000 simply press the 'View Full Waveform' button. Once the x-axis has been changed it will remain at those settings until the 'Reset Canvas' button (on the toolbar) has been pushed.
      If the 'Adjust With Cursor' check box is selected then you can also shrink the range of the x-axis by selecting the range you would like to zoom in on in the 'Raw Waveform' or 'Calibrated Waveform' canvas.  Dotted lines will appear and slide with your mouse to show what range you are selecting.  Note that you can only shrink the range with this method.  In order to expand the range back, you must either click the 'Reset Canvas' button or use the double slider.  


**2.9 -  Toolbar Buttons

      The toolbar contains 7 buttons, only 3 of which are enabled. The three buttons enabled are the 'save canvas', 'reset canvas', and 'Exit CLEANViewer' buttons. The 'save canvas' button brings up a dialog box that allows a user to save the canvas as a number of different file formats. The 'Reset Canvas' button redraws all the plots as though the zoom had never been adjusted.


**2.10 -  Menu Bar

      There are five menu bar pull down menus that can be accessed. Most of these pull down menus have entries that are either disabled or have redundant functionality. The only pull down menus that have original functionality that cannot be performed by some other widget are the 'EV Select' and 'WFD Select' pull downs. 
      If a selected event has multiple EVs, the first EV will automatically be loaded and those waveforms will be visible when PMTs are check. To change between EVs the user must access the 'EV Select' pull down menu. The selected EV will appear with a check mark on the left of the menu entry. Simply clicking on an EV without a check mark will cause that EV to be loaded and the check mark will move to its entry.
      The 'WFD Select' pull down allows the user to check/uncheck all the PMTs of a single WFD at once.

**2.11 -  Using Debug Mode

      CLEANViewer can be run in a debug mode that will cause a printout of every function called by the program as the functions are being called. To use the debug mode CLEANViewer must be run with the -D command line flag. The printout will look something of the format;

CLEANViewer::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, Char_t * DataFile)\n

Each debug printout tells the function called, the class that function exists in, and the arguments required of that function. Only use debug mode when necessary.



---------------
[3] Source Code
---------------


**3.1 -  Locating Functions

      To locate the correct file for the source code of a function refer to the header files for CLEANViewer. There the functions are broken up into groupings with comments above each group telling what file those functions are located in. The following is an example of all the functions located in CLEANViewer_GUISetup.cpp, as shown in CLEANViewer.h:

  /*CLEANViewer_GUISetup.cpp*/
  //functions for initial setup gui layout
  void SetMenu();
  void SetToolBar();
  void FillPmtVector();
  void SetTabs();
  void SetGroupFrames();

For the Waveform Class, all functions are located in Waveform.cpp.





**3.3 -  CLEANViewer Class


--3.3.1 -  Function Descriptions/Locations (INCOMPLETE)

CLEANViewer (const TGWindow *p, UInt_t w, UInt_t h, const char * _DataFile, Bool_t _DebugSwitch)

Location: CLEANViewer.cpp

Description: CLEANViewer constructor; p is the pointer to the main GUI frame, w is the width of the main GUI frame, h is the height of the main GUI frame, _DataFile is the name of the RAT data file loaded from the command line arguments, _DebugSwitch is the switch from the command line arguments that turns on/off debug mode.
--------------------

~CLEANViewer()

Location: CLEANViewer.cpp

Description: CLEANViewer desctructor
--------------------

void SetMenu()

Location: CLEANViewer_GUISetup.cpp

Description: Initial assignment and drawing of pointers for the Menu Bar, including the TGMenuBar, TGPopMenu, and pop up menu entries.
--------------------

void SetToolBar()

Location: CLEANViewer_GUISetup.cpp

Description: Initial assignment and drawing of pointers for the TGToolBar and all of its TGPictureButtons.
--------------------

void FillPmtVector()

Location: CLEANViewer_GUISetup.cpp

Description: After a file, even and EV have been loaded all of the active PMTs are loaded for that event and the container pmtCheckVector is filled with TGCheckButtons for the corresponding active PMTs.
--------------------

void SetTabs()

Location: CLEANViewer_GUISetup.cpp

Description: Initial assignment and drawing of pointers for the tabs 'Raw Waveform', 'Calibrated Waveform', Event Display', and 'Histograms' as well as the widgets in the group frame 'X Axis Adjust'.
--------------------

void SetGroupFrame()

Location: CLEANViewer_GUISetup.cpp

Description: Initial assignment and drawing of pointers for the group frames 'Event Select' and 'PMT Select'. This does not include the assignment and drawing of the PMT check boxes which occurs in the function FillPmtVector().
--------------------

Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t)

Location: CLEANViewer_Signals.cpp

Description: Used for processing event messages, is only applicable for events from the tool bar. All other events use signal/slot methods.
--------------------

void NextEventPressed()

Location: CLEANViewer_Signals.cpp

Description: Slot for when the Next Event Button (or menu entry) is pressed.
--------------------

void PrevEventPressed()

Location: CLEANViewer_Signals.cpp

Description: Slot for when the Previous Event Button (or menu entry) is pressed.
--------------------

void EventChanged()

Location: CLEANViewer_Signals.cpp

Description: Handles changing events, either direction, so that only valid events are loaded and settings for the next/prev event buttons reflect the current event.
--------------------

void XAxisChange()

Location: CLEANViewer_Signals.cpp

Description: Accordingly changes settings for axis zoom and grid lines. Calls PolyLineZoomAdjust() for redrawing waveforms.
--------------------

void ZoomFull()

Location: CLEANViewer_Signals.cpp

Description: Causes the x-axis to range from 0-10000 upon pushing the 'View Full Waveform' button. Calls PolyLineZoomAdjust() for redrawing waveforms.
--------------------

void CheckAllPMTs()

Location: CLEANViewer_Signals.cpp

Description: Checks all PMT check boxes at once. Calls PlotChannel() to draw all the waveforms.
--------------------

void UncheckAllPMTs()

Location: CLEANViewer_Signals.cpp

Description: Unchecks all PMT check boxes. Supposedly it clears the canvas, however it doesn't show until a new PMT box has been checked.
--------------------

void BoardCheck(Event_t* event)

Location: CLEANViewer_Signals.cpp

Description: Checks all the PMTs for a single board. Currently not implemented.
--------------------

void FileMenu(Int_t id)

Location: CLEANViewer_Signals.cpp

Description: Processes GUI events caused by the File Menu including the 'Save Canvas' entry and the 'Exit CLEAN Viewer' entry. Int_t id is the widget id of the entry clicked.
--------------------

void ViewMenu(Int_t id)

Location: CLEANViewer_Signals.cpp

Description: Processes GUI events caused by the View menu including the 'Reset Canvas', 'Show Grid Lines', and 'View Full Waveform' entries. Int_t id is the widget id of the entry clicked.
--------------------

void RATMenu(Int_t id)

Location: CLEANViewer_Signals.cpp

Description: Processes GUI events caused by the RAT menu including the 'Prev/Next Event' entries and the 'Check/Uncheck All PMTs' entries. Int_t id is the widget id of the entry clicked.
--------------------

void EVMenu(Int_t id)

Location: CLEANViewer_Signals.cpp

Description: Processes GUI events cuased by the EV menu, currently this only includes loading different EV for a current RAT event. Int_t id is the widget id of the entry clicked.
--------------------

void WFDMenu(Int_t id)

Location: CLEANViewer_Signals.cpp

Description: Processes GUI events cuased by the WFD menu, currently this only includes checking/unchecking all the PMTs of an individual WFD. Int_t id is the widget id of the entry clicked.
--------------------

void SaveCanvas()

Location: CLEANViewer_Signals.cpp

Description: allocates and draws/maps pointers associated with the save canvas dialog window. This function is called by clicking on the 'Save Canvas' tool bar button or clicking on the 'Save Canvas' entry in the File menu.
--------------------

void CancelSave()

Location: CLEANViewer_Signals.cpp

Description: Handles cleaning up pointers for the save canvas dialog box.
--------------------

void CloseWindow()

Location: CLEANViewer_Signals.cpp

Description: Closes the main GUI window.
--------------------

void Exit()

Location: CLEANViewer_Signals.cpp

Description: Exits CLEANViewer.
--------------------

void LoadFile(const char * DataFile)

Location: CLEANViewer_RAT.cpp

Description: Loads the RAT file from the DataFile argument (DataFile is most likely assigned from a command line argument) and assigns the DSReader pointer ratReader. This function also sets the initial current event to event 0 and calls NumberEV(kFALSE) and FillPmtVector().
--------------------

void NumberEV(Bool_t Prev)

Location: CLEANViewer_RAT.cpp

Description: if there are no EV in the event a new event is found with EV. Adds entries to the EV menu and sets the initial EV to EV 0. Also calls the function LoadChannels().
--------------------

void EventCheck(Bool_t Prev)

Location: CLEANViewer_RAT.cpp

Description: Checks that by changing the event that events below the lowest valid event and events above the highest valid event aren't loaded.
--------------------

void LoadChannels()

Location: CLEANViewer_RAT.cpp

Description: Enables/Disables PMT check boxes depending on if those PMTs are active for the current event. This Function DOES NOT allocate the pointers for the check boxes, that is done in the function FillPmtVector().
--------------------

void DrawHistogram()

Location: CLEANViewer_Draw.cpp

Description: Plots histograms in the Histograms tab. Currently this function is not implemented.
--------------------

void DrawPolyLine()

Location: CLEANViewer_Draw.cpp

Description: This Function handles the drawing of the TPolylines for the channel waveforms. This includes finding the min and max values of each axis for each channel, making the TPolyLine pointers and filling them with the correct data, and finally drawing to the correct TCanvas (rawTCanvas).
--------------------

void PolyLineZoomAdjust()

Location: CLEANViewer_Draw.cpp

Description: This Function handles the drawing of the TPolylines for the channel waveforms if an event was caused by the group frame 'X Axis Adjust'. This includes finding the min and max values of each axis for each channel, making the TPolyLine pointers and filling them with the correct data, and finally drawing to the correct TCanvas (rawTCanvas).
--------------------


--3.3.2 -  Caution

	Altering the source code can be very tricky and simple changes can cause large errors with the program. Remember that the latest working version can always be checked out from the SVN repository and no to commit any changes until you are certain that your changes have not caused any breaks.


**3.4 -  Waveform Class


--3.4.1 - Class Purpose

	The Waveform class was constructed for the purpose of have a convenient storage space for all of the RawBlocks of an event. The class stores raw data, transformed data, channel ID, and min and max vales. This creates an unnecessary black box filled with the functions that make it possible to draw multiple PolyLines of multiple ZLE windows for multiple channels all on the same canvas.


--3.4.2 - Function Descriptions/Locations (INCOMPLETE)

void FindMaxADC()

Location: Waveform.cpp

Description: Private function for finding the max ADC of all the ZLE windows for a single channel.
--------------------

void FindMinADC()

Location: Waveform.cpp

Description: Private function for finding the min ADC of all the ZLE windows for a single channel.
--------------------

void FindMaxSample()

Location: Waveform.cpp

Description: Private function for finding the max sample number of all the ZLE windows for a single channel.
--------------------

void FindMinSample()

Location: Waveform.cpp

Description: Private function for finding the min sample number of all the ZLE windows for a single channel.
--------------------

Waveform(sZLEwindow Raw, bool _DebugSwitch)
Waveform(sZLEwindow Raw, bool _DebugSwithc, int id)

Location: Waveform.cpp

Description: Overloaded constructor for the Waveform class. Can include an argument for the channel Id. 
--------------------

~Waveform()

Location: Waveform.cpp

Description: Default destructor for the waveform class.
--------------------

void SetID(int id)

Location: Waveform.cpp

Description: Sets the channel ID for an instance of the Waveform class.
--------------------

int GetID()

Location: Waveform.cpp

Description: Returns the channel ID for the referenced instance of the waveform class.
--------------------

double GetMinSample()

Location: Waveform.cpp

Description: Returns the min sample number (for all ZLE windows of the same channel) for the referenced instance of the waveform class.
--------------------

double GetMaxSample()

Location: Waveform.cpp

Description: Returns the max sample number (for all ZLE windows of the same channel) for the referenced instance of the waveform class.
--------------------

double GetMinADC()

Location: Waveform.cpp

Description: Returns the min ADC count (for all ZLE windows of the same channel) for the referenced instance of the waveform class.
--------------------

double GetMaxADC()

Location: Waveform.cpp

Description: Returns the max ADC count (for all ZLE windows of the same channel) for the referenced instance of the waveform class.
--------------------

vector <sTransformZLEwindow> GetTransform()

Location: Waveform.cpp

Description: Returns a vector, of structs sTransformZLEwindow, of transformed raw data. The function TransformRaw(...) must be called for this vector to be filled.
--------------------

void AddZLE(sZLEwindow Raw)

Location: Waveform.cpp

Description: This functions allows another sZLEwindow struct to be added to a Waveform class instance after that Waveform has been declared.
--------------------

void TransformRaw(int canvasMinX, int canvasMaxX, int nPlots, int whichPMT)
void TransformRaw(int canvasMinX, int canvasMaxX, int canvasMinY, int canvasMaxY, int nPlots, int whichPMT)

Location: Waveform.cpp

Description: Overloaded function for transforming the ZLE window structs (sZLEwindow) of a single channel into the correct coordinates for the TCanvas and TPolyLine. This Function fills the vector of sTransformZLEwindow structs. If a canvasMinY/canvasMaxY is included as an argument than those values will be used to determine the Y scale of every channel, otherwise each channel will individually scaled to it's min/max ADC count.
--------------------



------------
[4] Appendix
------------

Handle with care, good luck.
