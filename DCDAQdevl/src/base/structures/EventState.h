#ifndef __EVENTSTATE__
#define __EVENTSTATE__
//For state.
namespace EventState 
{
  enum
  {
    READY = 1,
    UNFINISHED = 2,
    BAD_WFD_EVENTID = 3,
    TOO_MANY_WFDS = 4      
  };
}
#endif
