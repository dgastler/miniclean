#ifndef __DRDATAPACKET__
#define __DRDATAPACKET__

namespace DRPacket
{
  struct Event
  {
    uint32_t ID;
    uint8_t ChannelCount;
  };
  struct Channel
  {
    uint8_t ID;
    uint8_t RawIntegralCount;
  };
  
  struct RawIntegral
  {
    //    double integral;     //ADC sum
    uint16_t    integral;     //ADC sum
    //    uint32_t startTime;  //clock ticks
    uint16_t startTime; //clock ticks
    uint8_t width; //clock ticks
    //uint32_t width;      //clock ticks
    //    double baseline;     //ADC
    //    double baselineRMS;  //ADC
  };
}
#endif
