#ifndef __DRGETTER__
#define __DRGETTER__
/*
  This thread connects the DRPC and listens for event reduction decisions.
  Then passes the reduced data to the EBPusher to be sent to the EBPC.
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <Connection.h>
#include <DRDecisionPacket.h>

#include <FEPCRATManager.h>
#include <DCQueue.h>

class DRGetter : public DCThread 
{
 public:
  //==============================================================
  //DRGetter.cpp
  //==============================================================
  DRGetter(std::string Name);
  ~DRGetter();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  void PrintStatus();
  //==============================================================
  //DRGetter_Run.cpp
  //==============================================================
  virtual void MainLoop();
 private:

  //==============================================================
  //DRGetter_Run.cpp
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message); 
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(); 

  //==============================================================
  //DRGetter_Process.cpp
  //==============================================================
  void ProcessPacket();    
  void ProcessDecision();
  void MoveToBad(int64_t eventID,int32_t errorCode);

  //==============================================================
  //DRGetter_Net.cpp
  //==============================================================
  bool ProcessNewConnection();
  void DeleteConnection();
  void ProcessNetThreadMessage();
  bool StartNewConnection();  

  //==============================================================
  //Network Connection data members
  //==============================================================
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  Connection DRSender;

  //==============================================================
  //Memory manager interface
  //==============================================================
  FEPCRATManager * FEPC_mem;

  //==============================================================
  //Local queues
  //==============================================================
  DCQueue<EventDecision,PipeSelect,NotLockedObject> DecisionQueue;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
};
#endif
