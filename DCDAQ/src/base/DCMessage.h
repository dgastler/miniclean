#ifndef __DCMESSAGE__
#define __DCMESSAGE__

#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <string.h> //for memcpy

//for address types
#include <arpa/inet.h>

namespace DCMessage 
{
  enum 
  {
    //Blank
    BLANKMESSAGE,
    
    //V1720Producer
    READOUTSIZE,
    READOUTNUMBER,
    READOUTEVENTS,
    
    //READERs
    BLOCKSREAD,
    EVENTSREAD,
    BADEVENTSREAD,
    
    //Universal
    STOP,
    PAUSE,
    GO,

    //SpeedTest
    PULSEHITS,
    
    //RAT reader
    MAX_SUBRUNS,
    RATERROR_POE,
    EVENTERROR_POE,
    MAX_TIME,
    MAX_EVENTS,

    //Network
    BADCONNECTION,
    PACKETSOUT,
    PACKETSIN,
    DATAOUT,
    DATAIN,

    //DCLauncher
    LAUNCH,
    LAUNCHFAIL,
    
    //DCThread basics
    RUN_NUMBER
  };
  
  
  //===================================
  //Useful struct types for parsing message data
  //===================================
  //DCtime_t
  typedef uint64_t DCtime_t;
  //WFD readout size
  typedef struct {int ID;uint32_t bytes;float dt;} WFDTrans;
  //WFD readouts
  typedef struct {int ID;uint32_t readouts;float dt;} WFDROs;
  //WFD Events
  typedef struct {int ID;uint32_t events;float dt;} WFDEv;
  
  //Pulser pulse count and frequency number
  typedef struct {int n;int f;} PulseFreq;
  
  //TimedCount
  typedef struct {uint32_t count;float dt;} TimedCount;

  //single uint64_t
  typedef struct {uint64_t i;} SingleUINT64;
  //single int
  typedef struct {int i;} SingleINT;

  //This class exists to allow message passing with data 
  //of any size and type.   It is up to you to know what
  //you are putting in it and what you are getting back.
  //If you are using C STYLE STRINGS you can't forget to
  //address the null termination of your string. 
  class Message
  {
  public:
    //===================================
    //Constructors
    //===================================
    Message(){Clear();};
    Message(const Message &message){CopyObj(message);};
    Message & operator=(const Message &rhs){CopyObj(rhs);return *this;};

    //===================================
    //User interfaces
    //===================================

    //Message type
    void SetType(uint16_t _type){type = _type; SetTime(time(NULL));};
    uint16_t GetType(){return(type);}
    
    //Set source and destination (AF == -1 means local)
    //(AF == -2 means broadcast)
    //(AF == AF_INET means a specific IPV4 address)
    //(AF == AF_INET6 means a specific IPV6 address)
    void SetSource(std::string name,
		   uint8_t * addr,
		   int32_t AFin);
    void GetSource(std::string & name,
		   std::vector<uint8_t> & addr, 
		   int32_t &AFin);


    void SetDestination(std::string name,
			uint8_t * addr,
			int32_t AFin);

    void GetDestination(std::string & name,
			std::vector<uint8_t> & addr, 
			int32_t &AFin);


    //Message time stamp
    void SetTime(time_t _timeStamp){timeStamp = _timeStamp;};
    DCtime_t GetTime(){return(timeStamp);};

    //Message data
    void SetData(const void * inData,size_t bytes){Set(inData,bytes,Data);};
    //Fill a passed vector with the data
    void GetData(std::vector<uint8_t> &returnData){returnData = Data;};
    
    int StreamMessage(std::vector<uint8_t> &stream);
    
    
    void SetMessage(uint8_t * streamSource,unsigned int size);
    
  private:
    //===================================
    //Constructor clear function
    //===================================
    void Clear();
    //===================================
    //Copy function
    //===================================
    void CopyObj(const Message &rhs);
    //===================================
    //Set internal data
    //===================================
    void Set(const void * in,size_t bytes,std::vector<uint8_t> & store);
    void Add(const void * in, size_t bytes,std::vector<uint8_t> & store);
    //===================================
    //Data members
    //===================================
    uint16_t type;
    DCtime_t timeStamp;
    std::vector<uint8_t> Data;

    std::string DestinationName;
    std::vector<uint8_t> DestinationAddr;
    int32_t DestinationAF;
    
    std::string SourceName;
    std::vector<uint8_t> SourceAddr;
    int32_t SourceAF;
  };
};
#endif
