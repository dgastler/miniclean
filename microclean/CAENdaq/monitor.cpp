// @(#)root/test:$Name:  $:$Id: guiviewer.cxx,v 1.10 2003/08/06 09:42:57 brun Exp $
// Author: Brett Viren   04/15/2001

// guiviewer.cxx: GUI test program showing TGTableLayout widget manager,
// embedded ROOT canvases, axis and sliders.
// To run it do: make guiviewer; guiviewer

#if !defined( __CINT__) || defined (__MAKECINT__)

#ifndef ROOT_TGDockableFrame
#include "TGDockableFrame.h"
#endif
#ifndef ROOT_TGMenu
#include "TGMenu.h"
#endif
#ifndef ROOT_TGMdiDecorFrame
#include "TGMdiDecorFrame.h"
#endif
#ifndef ROOT_TG3DLine
#include "TG3DLine.h"
#endif
#ifndef ROOT_TGMdiFrame
#include "TGMdiFrame.h"
#endif
#ifndef ROOT_TGMdiMainFrame
#include "TGMdiMainFrame.h"
#endif
#ifndef ROOT_TGuiBldHintsButton
#include "TGuiBldHintsButton.h"
#endif
#ifndef ROOT_TGMdiMenu
#include "TGMdiMenu.h"
#endif
#ifndef ROOT_TGListBox
#include "TGListBox.h"
#endif
#ifndef ROOT_TGNumberEntry
#include "TGNumberEntry.h"
#endif
#ifndef ROOT_TGScrollBar
#include "TGScrollBar.h"
#endif
#ifndef ROOT_TGuiBldHintsEditor
#include "TGuiBldHintsEditor.h"
#endif
#ifndef ROOT_TRootBrowser
#include "TRootBrowser.h"
#endif
#ifndef ROOT_TGFileDialog
#include "TGFileDialog.h"
#endif
#ifndef ROOT_TGShutter
#include "TGShutter.h"
#endif
#ifndef ROOT_TGButtonGroup
#include "TGButtonGroup.h"
#endif
#ifndef ROOT_TGCanvas
#include "TGCanvas.h"
#endif
#ifndef ROOT_TGFSContainer
#include "TGFSContainer.h"
#endif
#ifndef ROOT_TGuiBldEditor
#include "TGuiBldEditor.h"
#endif
#ifndef ROOT_TGColorSelect
#include "TGColorSelect.h"
#endif
#ifndef ROOT_TGButton
#include "TGButton.h"
#endif
#ifndef ROOT_TGSplitter
#include "TGSplitter.h"
#endif
#ifndef ROOT_TGFSComboBox
#include "TGFSComboBox.h"
#endif
#ifndef ROOT_TGLabel
#include "TGLabel.h"
#endif
#ifndef ROOT_TGMsgBox
#include "TGMsgBox.h"
#endif
#ifndef ROOT_TRootGuiBuilder
#include "TRootGuiBuilder.h"
#endif
#ifndef ROOT_TGTab
#include "TGTab.h"
#endif
#ifndef ROOT_TGListView
#include "TGListView.h"
#endif
#ifndef ROOT_TGStatusBar
#include "TGStatusBar.h"
#endif
#ifndef ROOT_TGListTree
#include "TGListTree.h"
#endif
#ifndef ROOT_TGToolTip
#include "TGToolTip.h"
#endif
#ifndef ROOT_TGToolBar
#include "TGToolBar.h"
#endif
#ifndef ROOT_TRootEmbeddedCanvas
#include "TRootEmbeddedCanvas.h"
#endif
#ifndef ROOT_TCanvas
#include "TCanvas.h"
#endif
#ifndef ROOT_TGuiBldDragManager
#include "TGuiBldDragManager.h"
#endif


#include "Riostream.h"

#endif


#include "monitor.h"

#include "Event.h"

#include <sys/stat.h>
#include <fcntl.h>

#define FILE  "/dev/shm/lock"


bool lock()
{
  Int_t fd = open(FILE, (O_WRONLY | O_CREAT | O_EXCL) ,0);
  if(fd == -1)
    return(false);
  close(fd);
  return(true);
}
void unlock()
{
  unlink(FILE);
}


void setStyle(TStyle* theStyle)
{ 
  theStyle->SetFrameBorderMode(0);
  theStyle->SetCanvasBorderMode(0);
  theStyle->SetPadBorderMode(0);
  theStyle->SetPadColor(0);
  theStyle->SetCanvasColor(kWhite);
  theStyle->SetTitleColor(kBlack);
  theStyle->SetTitleBorderSize(0);
  theStyle->SetStatColor(kWhite);
  theStyle->SetTitleFillColor(kWhite);
  //  theStyle->SetFillColor(kWhite);
 
  theStyle->SetPaperSize(20,26);
  theStyle->SetPadTopMargin(0.1);
  theStyle->SetPadRightMargin(0.08);
  theStyle->SetPadBottomMargin(0.16);
  theStyle->SetPadLeftMargin(0.12);

 
  // do not display any of the standard histogram decorations
  theStyle->SetOptTitle(1);
  theStyle->SetOptStat(0);
  theStyle->SetOptFit(1);
 
  // put tick marks on top and RHS of plots
  theStyle->SetPadTickX(1);
  theStyle->SetPadTickY(1);

  theStyle->SetTitleW(1.0);
  //  theStyle->SetTitleAlign(22);
  theStyle->SetTitleFontSize(.08);
 
  theStyle->SetPalette(1);
  return;
}


//static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

Viewer::Viewer(const TGWindow *p,Char_t * filename,char *optionsi) : TGMainFrame(p,500,500)
{
  FileName = filename;
  options = optionsi;
  event = 0;

  ArraysSet = false;
  Size = 0;
  FileNumber = 0;

  voltsLow = 1.0/255.0;
  voltsHigh = 10.0/255.0;

  
  //Make root pretty
  TStyle *myStyle = new TStyle("Me","My Style");
  setStyle(myStyle);
  gROOT->SetStyle("Me");

  HistNumber = 0;
  
  TogglePlots = false;
  TB = new TH1F("TB","Top-Bottom Ratio",100,-1.2,1.2);
  PMT0 = new TH1F("PMT0","PMT0 Charge",100,0,1e-8);
  PMT0->GetXaxis()->SetTitle("(C)");
  PMT1 = new TH1F("PMT1","PMT1 Charge",100,0,1e-8);
  PMT1->GetXaxis()->SetTitle("(C)");
  Total = new TH1F("Total","Total Charge",100,0,1e-8);
  Total->GetXaxis()->SetTitle("(C)");
  fPrompt = new TH1F("fPrompt","fPrompt",50,0,1);
  ChargeVSfPrompt = new TH2F("ChargeVSfPrompt","Charge vs fPrompt",100,0,1e-8,50,0,1);
  ChargeVSfPrompt->GetXaxis()->SetTitle("(C)");
  SPE_IvsH0 = new TH2F("SPE_IvsH0","SPE Integral vs Pulse Height",300,0,300*(1/255.0)*2E-9*(1/50.0),300,0,300*(1/255.0)); 
  SPE_IvsH0->GetXaxis()->SetTitle("Charge(C)");  
  SPE_IvsH0->GetYaxis()->SetTitle("Pulse Height(V)");
  SPE_IvsH1 = new TH2F("SPE_IvsH1","SPE Integral vs Pulse Height",300,0,300*(1/255.0)*2E-9*(1/50.0),300,0,300*(1/255.0));
  SPE_IvsH1->GetXaxis()->SetTitle("Charge(C)");  
  SPE_IvsH1->GetYaxis()->SetTitle("Pulse Height(V)");
  //SPE0 = new TH1D("SPE0","PMT0 SPE",300,0,150*(1/255.0)*2E-9*(1/50.0));
  //SPE0->GetXaxis()->SetTitle("Charge(C)");
  //SPE1 = new TH1D("SPE1","PMT1 SPE",300,0,150*(1/255.0)*2E-9*(1/50.0));
  //SPE1->GetXaxis()->SetTitle("Charge(C)");
  SPE0 = 0;
  SPE1 = 0;
  f0 = 0;
  f1 = 0;

  //Set GUI Parameters
  w                 = 1024;
  h                 = 768;
  border            = 10;
  buffer            = 10;
  PercentData     = 0.9;
  
  WaveformWidth     = Int_t ((w - border -border -buffer*2)*0.4);// width - border on each side - buffer between plots
  WaveformHeight    = Int_t (((h - border - border)*PercentData)*0.25 - buffer) ;
  WaveformHeightSpecial = Int_t (((h - border - border)*(1.0 - PercentData)));
  WaveformX         = w - border - WaveformWidth;
  WaveformY0        = border;
  WaveformY1        = WaveformY0 + buffer + WaveformHeight;
  WaveformY2        = WaveformY1 + buffer + WaveformHeight;
  WaveformY3        = WaveformY2 + buffer + WaveformHeight;
  WaveformY4        = WaveformY3 + buffer + WaveformHeight;
    
  HistWidth         = Int_t ((w - border - border - buffer*2)*0.3) ;
  HistHeight        = Int_t (((h - border - border)*PercentData)*0.5 - buffer);
  HistX0            = border;
  HistX1            = HistX0 + HistWidth + buffer;
  HistY0            = border;
  HistY1            = HistY0 + HistHeight + buffer;
    
  HorzWidth         = HistWidth ;//2 * HistWidth  + buffer;
  HorzHeight        = Int_t (((h - border - border)*(1.0 - PercentData)));
  HorzX             = border;
  HorzY             = HistY1 + HistHeight + buffer;
  
  TextWidth         = HistWidth;//WaveformWidth;
  TextHeight        = Int_t (((h - border - border)*(1.0 - PercentData)) * 0.33);
  
  TextX             = HistX1;//WaveformX;
  TextY0            = WaveformY3 + WaveformHeight + buffer;
  TextY1            = TextY0 + TextHeight;
  TextY2            = TextY1 + TextHeight;


  
  // composite frame
  fCompositeFrame = new TGCompositeFrame(this,w,h,kVerticalFrame);
  fCompositeFrame->SetLayoutBroken(kTRUE);
  

  //Histograms

  // embedded canvas
  fRootEmbeddedCanvas01 = new TRootEmbeddedCanvas(0,fCompositeFrame,HistWidth,HistHeight);
  wfRootEmbeddedCanvas01 = fRootEmbeddedCanvas01->GetCanvasWindowId();
  c01 = new TCanvas("c01", 10, 10, wfRootEmbeddedCanvas01);
  fRootEmbeddedCanvas01->AdoptCanvas(c01);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas01, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas01->MoveResize(HistX0,HistY0,HistWidth,HistHeight);
  
  // embedded canvas
  fRootEmbeddedCanvas02 = new TRootEmbeddedCanvas(0,fCompositeFrame,HistWidth,HistHeight);
  wfRootEmbeddedCanvas02 = fRootEmbeddedCanvas02->GetCanvasWindowId();
  c02 = new TCanvas("c02", 10, 10, wfRootEmbeddedCanvas02);
  fRootEmbeddedCanvas02->AdoptCanvas(c02);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas02, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas02->MoveResize(HistX1,HistY0,HistWidth,HistHeight);
  
  // embedded canvas
  fRootEmbeddedCanvas03 = new TRootEmbeddedCanvas(0,fCompositeFrame,HistWidth,HistHeight);
  wfRootEmbeddedCanvas03 = fRootEmbeddedCanvas03->GetCanvasWindowId();
  c03 = new TCanvas("c03", 10, 10, wfRootEmbeddedCanvas03);
  fRootEmbeddedCanvas03->AdoptCanvas(c03);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas03, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas03->MoveResize(HistX0,HistY1,HistWidth,HistHeight);
  
  // embedded canvas
  fRootEmbeddedCanvas04 = new TRootEmbeddedCanvas(0,fCompositeFrame,HistWidth,HistHeight);
  wfRootEmbeddedCanvas04 = fRootEmbeddedCanvas04->GetCanvasWindowId();
  c04 = new TCanvas("c04", 10, 10, wfRootEmbeddedCanvas04);
  fRootEmbeddedCanvas04->AdoptCanvas(c04);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas04, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas04->MoveResize(HistX1,HistY1,HistWidth,HistHeight);

  //Waveform plots


  // embedded canvas
  fRootEmbeddedCanvas05 = new TRootEmbeddedCanvas(0,fCompositeFrame,WaveformWidth,WaveformHeight);
  wfRootEmbeddedCanvas05 = fRootEmbeddedCanvas05->GetCanvasWindowId();
  c05 = new TCanvas("c05", WaveformWidth, WaveformHeight, wfRootEmbeddedCanvas05);
  fRootEmbeddedCanvas05->AdoptCanvas(c05);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas05, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas05->MoveResize(WaveformX,WaveformY0,WaveformWidth,WaveformHeight);

  // embedded canvas
  fRootEmbeddedCanvas06 = new TRootEmbeddedCanvas(0,fCompositeFrame,WaveformWidth,WaveformHeight);
  wfRootEmbeddedCanvas06 = fRootEmbeddedCanvas06->GetCanvasWindowId();
  c06 = new TCanvas("c06", WaveformWidth, WaveformHeight, wfRootEmbeddedCanvas06);
  fRootEmbeddedCanvas06->AdoptCanvas(c06);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas06, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas06->MoveResize(WaveformX,WaveformY1,WaveformWidth,WaveformHeight);
  
  // embedded canvas
  fRootEmbeddedCanvas07 = new TRootEmbeddedCanvas(0,fCompositeFrame,WaveformWidth,WaveformHeight);
  wfRootEmbeddedCanvas07 = fRootEmbeddedCanvas07->GetCanvasWindowId();
  c07 = new TCanvas("c07", WaveformWidth, WaveformHeight, wfRootEmbeddedCanvas07);
  fRootEmbeddedCanvas07->AdoptCanvas(c07);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas07, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas07->MoveResize(WaveformX,WaveformY2,WaveformWidth,WaveformHeight);
  
  // embedded canvas
  fRootEmbeddedCanvas08 = new TRootEmbeddedCanvas(0,fCompositeFrame,WaveformWidth,WaveformHeight);
  wfRootEmbeddedCanvas08 = fRootEmbeddedCanvas08->GetCanvasWindowId();
  c08 = new TCanvas("c08", WaveformWidth, WaveformHeight, wfRootEmbeddedCanvas08);
  fRootEmbeddedCanvas08->AdoptCanvas(c08);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas08, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas08->MoveResize(WaveformX,WaveformY3,WaveformWidth,WaveformHeight);

  // embedded canvas
  fRootEmbeddedCanvas09 = new TRootEmbeddedCanvas(0,fCompositeFrame,WaveformWidth,WaveformHeightSpecial);
  wfRootEmbeddedCanvas09 = fRootEmbeddedCanvas09->GetCanvasWindowId();
  c09 = new TCanvas("c09", WaveformWidth, WaveformHeightSpecial, wfRootEmbeddedCanvas09);
  fRootEmbeddedCanvas09->AdoptCanvas(c09);
  fCompositeFrame->AddFrame(fRootEmbeddedCanvas09, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fRootEmbeddedCanvas09->MoveResize(WaveformX,WaveformY4,WaveformWidth,WaveformHeightSpecial);

  // horizontal frame and Buttons
  
  fHorizontalFrame01 = new TGHorizontalFrame(fCompositeFrame,HorzY,HorzX,kHorizontalFrame | kSunkenFrame);
  TGTextButton * reset = new TGTextButton(fHorizontalFrame01,"Reset");
  reset->Connect("Clicked()","Viewer",this,"Reset()");
  fHorizontalFrame01->AddFrame(reset,new TGLayoutHints(kLHintsCenterX,5,5,3,4));
  //  fHorizontalFrame01->AddFrame(reset);
  
  TGTextButton * exit  = new TGTextButton(fHorizontalFrame01,"Exit");
  exit->Connect( "Clicked()","Viewer",this,"Exit()");
  fHorizontalFrame01->AddFrame(exit,new TGLayoutHints(kLHintsCenterX,5,5,3,4));
  TGTextButton * start  = new TGTextButton(fHorizontalFrame01,"Start");
  start->Connect( "Clicked()","Viewer",this,"Start()");
  fHorizontalFrame01->AddFrame(start,new TGLayoutHints(kLHintsCenterX,5,5,3,4));
  TGTextButton * stop  = new TGTextButton(fHorizontalFrame01,"Stop");
  stop->Connect( "Clicked()","Viewer",this,"Stop()");
  fHorizontalFrame01->AddFrame(stop,new TGLayoutHints(kLHintsCenterX,5,5,3,4));
  TGTextButton * toggle  = new TGTextButton(fHorizontalFrame01,"Toggle Plots");
  toggle->Connect( "Clicked()","Viewer",this,"Toggle()");
  fHorizontalFrame01->AddFrame(toggle,new TGLayoutHints(kLHintsCenterX,5,5,3,4));
  
  fCompositeFrame->AddFrame(fHorizontalFrame01, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fHorizontalFrame01->MoveResize(HorzX,HorzY,HorzWidth,HorzHeight);

  
  // Text Boxes

  EventText = new char[100];
  HistText = new char[100];
  TimeText = new char[100];
  sprintf(EventText," Event:        %07d Run:   %07d",0,0);
  sprintf(TimeText, " Event Time: ");
  sprintf(HistText, " Hist Entries: %07d",0);
  
  fLabel01 = new TGLabel(fCompositeFrame,EventText,TGLabel::GetDefaultGC()(),TGLabel::GetDefaultFontStruct(),kSunkenFrame);
  fLabel01->SetTextJustify(kTextLeft);
  fCompositeFrame->AddFrame(fLabel01, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabel01->MoveResize(TextX,TextY0,TextWidth,TextHeight);
  fLabel02 = new TGLabel(fCompositeFrame,TimeText,TGLabel::GetDefaultGC()(),TGLabel::GetDefaultFontStruct(),kSunkenFrame);
  fLabel02->SetTextJustify(kTextLeft);
  fCompositeFrame->AddFrame(fLabel02, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabel02->MoveResize(TextX,TextY1,TextWidth,TextHeight);
  fLabel03 = new TGLabel(fCompositeFrame,HistText,TGLabel::GetDefaultGC()(),TGLabel::GetDefaultFontStruct(),kSunkenFrame);
  fLabel03->SetTextJustify(kTextLeft);
  fCompositeFrame->AddFrame(fLabel03, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabel03->MoveResize(TextX,TextY2,TextWidth,TextHeight);


  timer = new TTimer();
  timer->Connect("Timeout()","Viewer",this,"Plot()");


  AddFrame(fCompositeFrame, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  fCompositeFrame->MoveResize(0,0,w,h);
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
  Resize(w,h);  

  Start();
  SetWindowName("DAQ Monitor");
}

Viewer::~Viewer()
{
  //should exit be called here? 
  // should cleanup be overloaded and also called by exit? 
  Exit();
  Cleanup();
}

void Viewer::CloseWindow()
{
  Exit();
}

void Viewer::Toggle()
{
  TogglePlots = !TogglePlots;
}

void Viewer::Exit()
{
  Stop();
  delete TB;
  delete PMT0;
  delete PMT1;
  delete Total;
  delete fPrompt;
  delete ChargeVSfPrompt;
  delete SPE_IvsH0;
  delete SPE_IvsH1;
  if(SPE0 != 0)
    delete SPE0;
  if(SPE1 != 0)
    delete SPE1;

  if(ArraysSet)
    {
      delete Waveform0;
      delete Waveform1;
      delete Waveform2;
      delete Waveform3;
      delete Waveform4;
      delete Chan0;
      delete Chan1;
      delete Chan2;
      delete Chan3;
      delete Chan4;
      //      delete ChanX;
    }
  gApplication->Terminate(0);
}

void Viewer::Reset()
{
  HistNumber = 0;
  FileNumber = 0;
  TB->Reset();
  PMT0->Reset();
  PMT1->Reset(); 
  Total->Reset();
  fPrompt->Reset();
  ChargeVSfPrompt->Reset();
  SPE_IvsH0->Reset();
  SPE_IvsH1->Reset();
  SPE0->Reset();
  SPE1->Reset();
}

void Viewer::Start()
{
  //two seconds
  timer->Start(2000,kFALSE);
}
void Viewer::Stop()
{
  timer->Stop();
}

void  Viewer::Plot()
{
  Bool_t Channel4 = false;
  //Generate new file name
  sprintf(FileName,"/dev/shm/DAQtemp.root");

  //Check if the file is ok to read.
  if(lock())
    {
      Int_t fd = open(FileName,O_RDONLY);
      if(fd != -1)
	{
	  //No file lock, so it's ok to read. 
	  close(fd);
	  DataFile = new TFile(FileName,"READ");
	  //Check if that file exists.
	  if(DataFile->IsZombie())
	    {
	      cerr << "BadFile: " << FileName << "! The DAQ is producing corrupted files!"<< endl;
	      EverythingIsOk = false;
	      delete DataFile;
	    }
	  else
	    //everything is working.  Read the event. 
	    {
	      EverythingIsOk = true;
	      ReadAttempts = 0;
	      tree = (TTree*) DataFile->Get("tree");
	      tree->SetBranchAddress("Event branch",&event);
	      tree->GetEntry(tree->GetEntries()-1);
	      //TempEvent = (Event *) event->Clone();
	      
	      
	      
	      if(!ArraysSet)
		{
		  Size = event->GetSize(0);
		  if(Size < 1000)
		    {
		      cerr << "Waveform Size below 1000" << endl;
		      Exit();
		    }
		  Waveform0 = new TGraph(Size);
		  Waveform1 = new TGraph(Size);
		  Waveform2 = new TGraph(Size);
		  Waveform3 = new TGraph(Size);
		  Waveform4 = new TGraph(Size);
		  
		  ArraysSet = true;
		  Chan0 = new Float_t[Size];
		  Chan1 = new Float_t[Size];
		  Chan2 = new Float_t[Size];
		  Chan3 = new Float_t[Size];
		  Chan4 = new Float_t[Size];
		  //		  ChanX = new Float_t[Size];
		}
	      
	      
	      
	      Float_t PMT0SC = 0;
	      Float_t PMT1SC = 0;
	      Float_t PMT0FC = 0;
	      Float_t PMT1FC = 0;
	      Float_t PMT0C = 0;
	      Float_t PMT1C = 0;
	      Float_t PMTC = 0;
	      Float_t Offset0 = 0;
	      Float_t Offset1 = 0;
	      Float_t Offset2 = 0;
	      Float_t Offset3 = 0;
	      Float_t Offset4 = 0;


	      // John instructs Dan to stop choosing coding over women.
	      if(event->NRaw() > 4)
		{
		  Channel4 = true;
		}

	      
	      Int_t EventNumber = 0;
	      Int_t RunNumber = 0;
	      
	      EventNumber = event->GetHeader()->GetEvtNum();
	      RunNumber = event->GetHeader()->GetRun();
	      
	      //for(int i = 0; i < 1000; i++)
	      //	{
	      //	  Offset0 += event->GetPoint(0,i);
	      //	  Offset1 += event->GetPoint(1,i);
	      //	  Offset2 += event->GetPoint(2,i);
	      //	  Offset3 += event->GetPoint(3,i);
	      //	  if(Channel4)
	      //	    {
	      //	      Offset4 += event->GetPoint(4,i);
	      //	    }
	      //	}
	      //Offset0 /= 1000;
	      //Offset1 /= 1000;
	      //Offset2 /= 1000;
	      //Offset3 /= 1000;
	      //Offset4 /= 1000;


	      Offset0 = Offset(event,0,1000,0);
	      Offset1 = Offset(event,1,1000,0);
	      Offset2 = Offset(event,2,1000,0);
	      Offset3 = Offset(event,3,1000,0);
	      if(Channel4)
		{
		  Offset4 = Offset(event,4,1000,0);
		}
	      
	      Bool_t FixGain[2] = {false,false};
	      
	      FixGain[0] = CopyWaveform(event,0,Chan0,Size,Offset0);
	      CopyWaveform(event,1,Chan1,Size,Offset1);
	      FixGain[1] = CopyWaveform(event,2,Chan2,Size,Offset2);
	      CopyWaveform(event,3,Chan3,Size,Offset3);
	      if(Channel4)
		{
		  CopyWaveform(event,4,Chan4,Size,Offset4);
		}

	      //// this is to add the date stamp after "Event Time: " in TimeText string.
	      event->GetHeader()->GetDate(TimeText+13);
	      TimeText[43] = '\0';
	      
	      //	  delete event;
	      
	      delete tree;
	      DataFile->Close();
	      delete DataFile;

	      //Set size of the array in our TGraphs
	      Waveform0->Set(Size);
	      Waveform1->Set(Size);
	      Waveform2->Set(Size);
	      Waveform3->Set(Size);
	      Waveform4->Set(Size);

	      

	      for(int i = 0 ; i < Size;i++)
	      	{
//	      	  Waveform0->SetPoint(i,i*2e-9,(event->GetPoint(0,i) - Offset0)*voltsLow);
//	      	  Waveform1->SetPoint(i,i*2e-9,(event->GetPoint(1,i) - Offset1)*voltsHigh);
//	      	  Waveform2->SetPoint(i,i*2e-9,(event->GetPoint(2,i) - Offset2)*voltsLow);
//	      	  Waveform3->SetPoint(i,i*2e-9,(event->GetPoint(3,i) - Offset3)*voltsHigh);
		  Waveform0->SetPoint(i,i*2e-9,(Chan0[i])*voltsLow); 
		  Waveform1->SetPoint(i,i*2e-9,(Chan1[i])*voltsHigh);
		  Waveform2->SetPoint(i,i*2e-9,(Chan2[i])*voltsLow); 
		  Waveform3->SetPoint(i,i*2e-9,(Chan3[i])*voltsHigh);
		  
	      	  //Chan0[i] = (event->GetPoint(0,i) - Offset0)*voltsLow;
	      	  //Chan1[i] = (event->GetPoint(1,i) - Offset1)*voltsHigh;
	      	  //Chan2[i] = (event->GetPoint(2,i) - Offset2)*voltsLow;
	      	  //Chan3[i] = (event->GetPoint(3,i) - Offset3)*voltsHigh;
	      	  if(Channel4)
	      	    {
		      //	      	      Waveform4->SetPoint(i,i*2e-9,(event->GetPoint(4,i) - Offset4)*voltsLow);
	      	      Waveform4->SetPoint(i,i*2e-9,(Chan4[i])*voltsLow);
	      	      //Chan4[i] = (event->GetPoint(4,i) - Offset4)*voltsLow;
	      	    }
	      //	  
	      //	  PMT0C += event->GetPoint(0,i) - Offset0;
	      //	  PMT1C += event->GetPoint(2,i) - Offset2;
	      //	  if(i < 2200)
	      //	    {
	      //	      PMT0FC += event->GetPoint(0,i) - Offset0;
	      //	      PMT1FC += event->GetPoint(2,i) - Offset2;
	      //	    }
	      //	  //		  ChanX[i] = i*2e-9;
	      	}


	      if(FixGain[0])
		{
		  MergeWaveforms(Size,Chan0,Chan1,Offset0,10.0);
		}
	      if(FixGain[1])
		{
		  MergeWaveforms(Size,Chan2,Chan3,Offset2,10.0);
		}
	      
	      Bool_t SubEvent = false;
	      Int_t Peak[2] = {0,0};
	      Peak[0] = Find_Waveform_Start(3000,Chan0);
	      Peak[1] = Find_Waveform_Start(3000,Chan2);
	      Float_t MaxHeights[2] = {0,0};
	      MaxHeights[0] = Chan0[Find_Waveform_Maximum(3000,Chan0)];
	      MaxHeights[2] = Chan2[Find_Waveform_Maximum(3000,Chan2)];
	      
	      //Lets see if there is a second event in the waveform.
	      if((Chan0[Find_Waveform_Maximum(Size-3000,Chan0+3000)+3000] > 0.4*MaxHeights[0]) &&
		 (Chan2[Find_Waveform_Maximum(Size-3000,Chan2+3000)+3000] > 0.4*MaxHeights[1]))
		{
		  if (fabs((Find_Waveform_Start(Size-3000,Chan0+3000)+3000) - (Find_Waveform_Start(Size-3000,Chan2+3000)+3000)) < 10)
		    {
		      SubEvent = true;
		      //SubPeak[0] = (Find_Waveform_Start(ChannelSize-3000,Channel1+3000)+3000);
		      //SubPeak[1] = (Find_Waveform_Start(ChannelSize-3000,Channel2+3000)+3000);
		      //pevent->SubDelay(0,SubPeak[0]);
		      //pevent->SubDelay(1,SubPeak[1]);
		    }
		}
	      
	      
	      Int_t WindowEnds[4] = {-10,45,2500,7500};
	      
	      PMT0FC = Integrate(Chan0,Peak[0] + WindowEnds[0], Peak[0] + WindowEnds[1],0,2,25,5);
	      PMT1FC = Integrate(Chan1,Peak[1] + WindowEnds[0], Peak[1] + WindowEnds[1],0,2,25,5);
	      PMT0SC = Integrate(Chan0,Peak[0] + WindowEnds[1], Peak[0] + WindowEnds[2],0,2,25,5);
	      PMT1SC = Integrate(Chan1,Peak[1] + WindowEnds[1], Peak[1] + WindowEnds[2],0,2,25,5);

	      if(!SubEvent)
		{
		  Integrate(Chan0,Peak[0] + WindowEnds[2], Peak[0] + WindowEnds[3],SPE_IvsH0,2,25,5);
		  Integrate(Chan1,Peak[1] + WindowEnds[2], Peak[1] + WindowEnds[3],SPE_IvsH1,2,25,5);
		}
	      
	      PMT0C = PMT0SC + PMT0FC;
	      PMT1C = PMT1SC + PMT1FC;
	      PMTC = PMT0C + PMT1C;

	      
	      fPrompt->Fill((PMT0FC + PMT1FC)/(PMTC));
	      ChargeVSfPrompt->Fill(PMTC,(PMT0FC + PMT1FC)/(PMTC));
	      PMT0->Fill(PMT0C);
	      PMT1->Fill(PMT1C);
	      Total->Fill(PMTC);
	      TB->Fill( (PMT0C - PMT1C)/(PMTC));
	      
	      //	      delete SPE0;
	      //	      delete SPE1;
	      if(TogglePlots)
		{
		  if(SPE0 !=0)
		    delete SPE0;
		  if(SPE1 !=0)
		    delete SPE1;
		  SPE0 = SPE_IvsH0->ProjectionX("SPE0");	
		  SPE0->SetTitle("SPE PMT0");
		  SPE0->GetXaxis()->SetTitle("Charge(C)");
		  SPE1 = SPE_IvsH1->ProjectionX("SPE1");
		  SPE1->SetTitle("SPE PMT1");
		  SPE1->GetXaxis()->SetTitle("Charge(C)");
		  SPE0->GetListOfFunctions()->Clear();
		  SPE1->GetListOfFunctions()->Clear();
		  if(f0 != 0)
		    delete f0;
		  if(f1 != 0)
		    delete f1;
		  f0 = SPEfit((TH1F*)SPE0,"f0");
		  //		  delete f1;
		  f1 = SPEfit((TH1F*)SPE1,"f1");
		  //		  delete f2;
		}
	      //function = SPEFit(SPE0,"function");
	      //	      delete function;
	      //function = SPEFit(SPE1,"function1");
	      //	      delete function;
	      
	      HistNumber++;
	      

	      
	      c01->Clear();
	      if(TogglePlots)
		{
		  fPrompt->Draw();
		}
	      else
		{
		  //Find a good range for this plot.   Same for other lines like this.
		  PMT0->GetXaxis()->SetRangeUser(0,PMT0->GetBinCenter(FindPlotMaxBin(PMT0)));
		  PMT0->Draw();
		}
	      c02->Clear();
	      if(TogglePlots)
		{
		  ChargeVSfPrompt->GetXaxis()->SetRangeUser(0,ChargeVSfPrompt->GetBinCenter(FindPlotMaxBinX(ChargeVSfPrompt)));
		  ChargeVSfPrompt->Draw();
		}
	      else
		{
		  PMT1->GetXaxis()->SetRangeUser(0,PMT1->GetBinCenter(FindPlotMaxBin(PMT1)));
		  PMT1->Draw();
		}
	      c03->Clear();
	      if(TogglePlots)
		{
		  SPE0->GetXaxis()->SetRangeUser(0,SPE0->GetBinCenter(FindPlotMaxBin(SPE0)));
		  SPE0->SetMaximum(SPE0->GetMaximum()*1.05);
		  SPE0->Draw();
		  //		  delete f0;
		}
	      else
		{
		  Total->GetXaxis()->SetRangeUser(0,Total->GetBinCenter(FindPlotMaxBin(Total)));
		  Total->Draw();
		}
	      c04->Clear();
	      if(TogglePlots)
		{
		  SPE1->GetXaxis()->SetRangeUser(0,SPE1->GetBinCenter(FindPlotMaxBin(SPE1)));
		  SPE1->SetMaximum(SPE1->GetMaximum()*1.05);
		  SPE1->Draw();
		  //		  delete f1;
		}
	      else
		{
		  TB->Draw();
		}
	      
	      c05->Clear();
	      //	      Waveform0->DrawGraph(Size,ChanX,Chan0,"AL");
	      Waveform0->Draw("AL");
	      c06->Clear();
	      //	      Waveform1->DrawGraph(Size,ChanX,Chan1,"AL");
	      Waveform1->Draw("AL");
	      c07->Clear();
	      //	      Waveform2->DrawGraph(Size,ChanX,Chan2,"AL");
	      Waveform2->Draw("AL");
	      c08->Clear();
	      //	      Waveform3->DrawGraph(Size,ChanX,Chan3,"AL");
	      Waveform3->Draw("AL");
	      c09->Clear();
	      if(Channel4)
		{
		  //		  Waveform4->DrawGraph(Size,ChanX,Chan4,"AL");
		  Waveform4->Draw("AL");
		}
	      
	      
	      Waveform0->GetXaxis()->SetTitle("Time (s)");
	      Waveform0->GetYaxis()->SetTitle("Voltage");
	      Waveform0->GetYaxis()->SetRangeUser(-.1,1);
	      Waveform0->SetTitle("PMT0 High Gain");
	      Waveform1->GetXaxis()->SetTitle("Time (s)");
	      Waveform1->GetYaxis()->SetTitle("Voltage");
	      Waveform1->GetYaxis()->SetRangeUser(-.1,5);
	      Waveform1->SetTitle("PMT0 Low Gain");
	      Waveform2->GetXaxis()->SetTitle("Time (s)");
	      Waveform2->GetYaxis()->SetTitle("Voltage");
	      Waveform2->GetYaxis()->SetRangeUser(-.1,1);
	      Waveform2->SetTitle("PMT1 High Gain");
	      Waveform3->GetXaxis()->SetTitle("Time (s)");
	      Waveform3->GetYaxis()->SetTitle("Voltage");
	      Waveform3->GetYaxis()->SetRangeUser(-.1,5);
	      Waveform3->SetTitle("PMT1 Low Gain");
	      Waveform4->GetXaxis()->SetTitle("Time (s)");
	      Waveform4->GetYaxis()->SetTitle("Voltage");
	      Waveform4->GetYaxis()->SetRangeUser(0,0.5);
	      Waveform4->GetYaxis()->SetNdivisions(5,kFALSE);
	      Waveform4->GetYaxis()->SetLabelFont(100);
	      Waveform4->SetTitle("PMT2");
	      
	      sprintf(EventText," Event:       %07d Run: %07d",EventNumber,RunNumber);
	      sprintf(HistText, " Hist Entries:%07d",HistNumber);
	      

	    }
	  unlink(FileName);
	  
	}
      else
	{
	  if(ReadAttempts > 10)
	    {
	      cerr << "Unable to find " << FileName << "# " << ReadAttempts << ".  Is the DAQ running?" << endl;
	      EverythingIsOk = false;
	    }
	  ReadAttempts++;
	}
      unlock(); 
    }
  else
    {
      if(ReadAttempts > 10)
	{
	  cerr << "Unable get lock for " << FileName << ".  Maybe something ended badly last run? " << endl;
	  EverythingIsOk = false;
	}
      ReadAttempts++;
    }
  
  if(EverythingIsOk)
    {
      c01->SetFillColor(kWhite);
      c02->SetFillColor(kWhite);
      c03->SetFillColor(kWhite);
      c04->SetFillColor(kWhite);
      c05->SetFillColor(kWhite);
      c06->SetFillColor(kWhite);
      c07->SetFillColor(kWhite);
      c08->SetFillColor(kWhite);
      c09->SetFillColor(kWhite);
    }
  else
    {
      //Let the user know something is up
      c01->SetFillColor(kRed);
      c02->SetFillColor(kRed);
      c03->SetFillColor(kRed);
      c04->SetFillColor(kRed);
      c05->SetFillColor(kRed);
      c06->SetFillColor(kRed);
      c07->SetFillColor(kRed);
      c08->SetFillColor(kRed);
      c09->SetFillColor(kRed);

      c01->Draw();
      c02->Draw();
      c03->Draw();
      c04->Draw();
      c05->Draw();
      c06->Draw();
      c07->Draw();
      c08->Draw();
      c09->Draw();
    }

  
  

  
  c01->Update();
  c02->Update();
  c03->Update();
  c04->Update();
  c05->Update();
  c06->Update();
  c07->Update();
  c08->Update();
  c09->Update();

  

  fLabel01->SetText(EventText);
  fLabel02->SetText(TimeText);
  fLabel03->SetText(HistText);
  fLabel01->Layout();
  fLabel02->Layout();
  fLabel03->Layout();
  
}




Int_t Viewer::FindPlotMaxBin(TH1F * h)
{
  Int_t DrawBin = h->GetNbinsX();
  for(Int_t Bin = 1 ; Bin < h->GetNbinsX();Bin++)
    {
      if(h->GetBinContent(Bin) > 0)
	{
	  DrawBin = Bin;
	}
    }
  return(DrawBin);
}
Int_t Viewer::FindPlotMaxBin(TH1D * h)
{
  Int_t DrawBin = h->GetNbinsX();
  for(Int_t Bin = 1 ; Bin < h->GetNbinsX();Bin++)
    {
      if(h->GetBinContent(Bin) > 0)
	{
	  DrawBin = Bin;
	}
    }
  return(DrawBin);
}

Int_t Viewer::FindPlotMaxBinX(TH2F * h)
{
  TH1D * Projection = h->ProjectionX();
  Int_t DrawBin = FindPlotMaxBin(Projection);
  delete Projection;
  return(DrawBin);
}
Int_t Viewer::FindPlotMaxBinY(TH2F * h)
{
  TH1D * Projection = h->ProjectionY();
  Int_t DrawBin = FindPlotMaxBin(Projection);
  delete Projection;
  return(DrawBin);
}
Int_t Viewer::FindPlotMaxBinX(TH2D * h)
{
  TH1D * Projection = h->ProjectionX();
  Int_t DrawBin = FindPlotMaxBin(Projection);
  delete Projection;
  return(DrawBin);
}
Int_t Viewer::FindPlotMaxBinY(TH2D * h)
{
  TH1D * Projection = h->ProjectionY();
  Int_t DrawBin = FindPlotMaxBin(Projection);
  delete Projection;
  return(DrawBin);
}
