#ifndef __STATMESSAGE__
#define __STATMESSAGE__
#include <stdint.h>
#include <RAT/DS/DCDAQ_DCTime.hh>
#include <boost/static_assert.hpp>

#include <Units.h>
#include <SubID.h>
#include <Level.h>

namespace StatMessage
{
  enum Type
    {
      BLANK = 0,
      
      //Basic rates
      EVENT_RATE,      
      PACKET_RATE,
      READOUT_RATE,
      DATA_RATE,
      REDUCTION_RATE,
      BAD_EVENT_RATE,
      TRIGGER_RATE,
            
      OCCUPANCY,
      
      FEPC,
      DRPC,
      RAT_DISK,
      FREE_FULL,

      BASELINE,
      ZLECOUNT,

      COUNT //ALWAYS LAST!
    };
  const char * GetTypeName(StatMessage::Type type);
  StatMessage::Type GetType(const char * name);
    
  struct StatBase
  {
    StatMessage::Type type;
  };
  struct StatInfo
  {
    DCtime_t time;
    DCtime_t interval;
    SubID::Type subID; 
    BOOST_STATIC_ASSERT(sizeof(SubID::Type) == sizeof(uint32_t));
    Level::Type level; 
    BOOST_STATIC_ASSERT(sizeof(Level::Type) == sizeof(uint32_t));
    StatInfo() 
    {
      (*this).time = 0;
      (*this).interval = 0;
      (*this).subID = SubID::BLANK;
      (*this).level = Level::BLANK;
    };
  };
  
  struct StatRate
  {
    StatBase sBase;
    StatInfo sInfo;
    uint64_t count;  
    BOOST_STATIC_ASSERT(sizeof(Units::Type) == sizeof(uint32_t));
    Units::Type units;
    StatRate() :sBase(),sInfo()
    {
      (*this).count = 0;
      (*this).units = Units::NONE;
    };
  };

  struct StatFloat
  {
    StatBase sBase;
    StatInfo sInfo;    
    BOOST_STATIC_ASSERT(sizeof(float) == sizeof(uint32_t));
    float val;
  };

  struct StatVHash
  {
    uint64_t size;
    uint64_t active;
  };

  struct StatQueue
  {
    uint64_t size;
    uint64_t addCount;
  };

  struct StatFEPCMM
  {
    StatBase sBase;
    StatInfo sInfo;
    uint64_t allocatedSize;
    StatVHash assembled;
    StatVHash storage;
    StatQueue free;
    StatQueue unSent;
    StatQueue send;
    StatQueue badEvent;
  };
  struct StatDRPCMM
  {
    StatBase sBase;
    StatInfo sInfo;
    uint64_t allocatedSize;
    StatVHash eventStore;
    StatQueue free;
    StatQueue unProcessed;
    StatQueue respond;
    StatQueue badEvent;
    StatQueue EBPC;
  };
  struct StatRATDiskMM
  {
    StatBase sBase;
    StatInfo sInfo;
    uint64_t allocatedSize;
    StatVHash eventStore;
    StatQueue free;
    StatQueue toDisk;
    StatQueue badEvent;
  };
  struct StatFreeFull
  {
    StatBase sBase;
    StatInfo sInfo;
    uint64_t allocatedSize;
    StatQueue free;
    StatQueue full;
  };


};
#endif








