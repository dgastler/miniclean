--Whenever the receive buffer indicates that a packet has come in (via RX_READY signal),
--this module springs into action--determining whether the packet is an ARP, UDP, or ping
--packet. It then grants the associated module the ability to read the incoming packet
--and create a new packet in response.
--Original code by Jeremy Mans.
--Translation into VHDL along with the addition of the udp2 and udp3 states to more accurately
--assess UDP packet length and various other aesthetic modifications to the state machine 
--by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity packet_handler is Port(
	CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	RX_READY : in STD_LOGIC;
	RX_DONE : out STD_LOGIC;
	RXD : in STD_LOGIC_VECTOR(7 downto 0);	 
	RXA : out STD_LOGIC_VECTOR(10 downto 0);
	TXD : out STD_LOGIC_VECTOR(7 downto 0);
	TXMIT : out STD_LOGIC;
	IP : in STD_LOGIC_VECTOR(31 downto 0);
	ARP_RXA : in STD_LOGIC_VECTOR(5 downto 0);
	ARP_TXD : in STD_LOGIC_VECTOR(7 downto 0);
	ARP_XMIT : in STD_LOGIC;
	ARP_DONE : in STD_LOGIC;
	ARP_READY : out STD_LOGIC;
	ICMP_RXA : in STD_LOGIC_VECTOR(9 downto 0);
	ICMP_TXD : in STD_LOGIC_VECTOR(7 downto 0);
	ICMP_XMIT : in STD_LOGIC;
	ICMP_DONE : in STD_LOGIC;
	ICMP_READY : out STD_LOGIC;
	UDP_PORT_CTL : in STD_LOGIC_VECTOR(15 downto 0);
	UDP_SIZE : out STD_LOGIC_VECTOR(15 downto 0);
	UDP_RXA : in STD_LOGIC_VECTOR(10 downto 0);
	UDP_TXD : in STD_LOGIC_VECTOR(7 downto 0);
	UDP_XMIT : in STD_LOGIC;
	UDP_DONE : in STD_LOGIC;
	UDP_XMIT_REQ : in STD_LOGIC;
	UDP_READY : out STD_LOGIC; 
	UDP_XMIT_OK : out STD_LOGIC);
end packet_handler;

architecture Behavioral of packet_handler is
	signal self_rxa : STD_LOGIC_VECTOR(5 downto 0);
	signal eth0 : STD_LOGIC_VECTOR(7 downto 0);
	signal ignore : STD_LOGIC;
	type mode is (src_self, src_arp, src_icmp, src_udp);
	signal activeSrc : mode := src_self;
	type state_type is (st_idle, st_ignored, st_waiteth0, st_eth0, st_eth1, st_arp,
		st_ip0, st_ip1, st_ip2, st_ip3, st_ip, st_icmp, st_udp, st_udp0,
		st_udp1, st_udp2, st_udp3);
	signal state : state_type := st_idle;
	type state_type2 is (st_notudp, st_indeedudp);
	signal state2 : state_type2 := st_notudp;
begin
	RX_DONE <= ARP_DONE when activeSrc = src_arp else
		ICMP_DONE when activeSrc = src_icmp else
		UDP_DONE when activeSrc = src_udp else ignore;
	RXA <= "00000" & ARP_RXA when activeSrc = src_arp else
		'0' & ICMP_RXA when activeSrc = src_icmp else
		UDP_RXA when activeSrc = src_udp else "00000" & self_rxa;

	input_muxing : process(CLK)
	begin
		if CLK'event and CLK = '1' then
			eth0 <= RXD;
			if RESET = '1' then
				ARP_READY <= '0';
				ICMP_READY <= '0';
				UDP_READY <= '0';
				state <= st_idle;
				activeSrc <= src_self;
				ignore <= '0';
			else
				case state is
				when st_idle =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					ignore <= '0';
					if RX_READY = '1' then
						self_rxa <= "001100";--get ethtype0
						state <= st_waiteth0;
					else
						state <= st_idle;
					end if;
				when st_ignored =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					ignore <= '0';
					state <= st_idle;
				when st_waiteth0 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					ignore <= '0';
					self_rxa <= "001101";--get ethtype1
					state <= st_eth0;
				when st_eth0 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					ignore <= '0';
					self_rxa <= "011110";--get dest ip addr 0
					state <= st_eth1;
				when st_eth1 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "011111";--get dest ip addr 1
					if (RXD = x"06") and (eth0 = x"08") then
						ignore <= '0';
						state <= st_arp;
					elsif (RXD = x"00") and (eth0 = x"08") then
						ignore <= '0';
						state <= st_ip0;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_arp =>
					if state2 = st_indeedudp then--if UDP packet being sent out already,
						ARP_READY <= '0';--wait to parse the ARP packet
					else
						ARP_READY <= '1';
					end if;
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_arp;
					ignore <= '0';
					if ARP_DONE = '1' then
						state <= st_idle;
					else
						state <= st_arp;
					end if;
				when st_ip0 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "100000";--get dest ip addr 2
					if RXD = IP(31 downto 24) then
						ignore <= '0';
						state <= st_ip1;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_ip1 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "100001";--get dest ip addr 3
					if RXD = IP(23 downto 16) then
						ignore <= '0';
						state <= st_ip2;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_ip2 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "010111";--get the ip type (if valid)
					if RXD = IP(15 downto 8) then
						ignore <= '0';
						state <= st_ip3;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_ip3 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "100100";--get the udp port MSB (if needed)
					if RXD = IP(7 downto 0) then
						ignore <= '0';
						state <= st_ip;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_ip =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "100101";--get the udp port LSB (if needed)
					if RXD = x"01" then
						ignore <= '0';
						state <= st_icmp;
					elsif RXD = x"11"  then
						ignore <= '0';
						state <= st_udp0;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_icmp =>
					ARP_READY <= '0';
					if state2 = st_indeedudp then--if UDP packet being sent out already,
						ICMP_READY <= '0';--wait to parse the ping packet
					else
						ICMP_READY <= '1';
					end if;
					UDP_READY <= '0';
					activeSrc <= src_icmp;
					ignore <= '0';
					if ICMP_DONE = '1' then
						state <= st_idle;
					else
						state <= st_icmp;
					end if;
				when st_udp =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '1';
					activeSrc <= src_udp;
					ignore <= '0';
					if UDP_DONE = '1' then
						state <= st_idle;
					else
						state <= st_udp;
					end if;
				when st_udp0 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "100110";--get the udp data size
					if RXD = UDP_PORT_CTL(15 downto 8) then
						ignore <= '0';
						state <= st_udp1;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_udp1 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					self_rxa <= "100111";--get the udp data size
					if RXD = UDP_PORT_CTL(7 downto 0) then
						ignore <= '0';
						state <= st_udp2;
					else
						ignore <= '1';
						state <= st_ignored;
					end if;
				when st_udp2 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					ignore <= '0';
					UDP_SIZE(15 downto 8) <= RXD;
					state <= st_udp3;
				when st_udp3 =>
					ARP_READY <= '0';
					ICMP_READY <= '0';
					UDP_READY <= '0';
					activeSrc <= src_self;
					ignore <= '0';
					UDP_SIZE(7 downto 0) <= RXD;
					state <= st_udp;
				end case;
			end if;
		end if;
	end process input_muxing;

	output_muxing : process(CLK)
	begin
		if CLK'event and CLK = '1' then
			if RESET = '1' then
				UDP_XMIT_OK <= '0';
				TXMIT <= '0';
				TXD <= x"00";
				state2 <= st_notudp;
			else
				case state2 is
				when st_notudp =>
					UDP_XMIT_OK <= '1';
					if activeSrc = src_arp then
						TXMIT <= ARP_XMIT;
						TXD <= ARP_TXD;
					elsif activeSrc = src_icmp then
						TXMIT <= ICMP_XMIT;
						TXD <= ICMP_TXD ;
					else
						TXMIT <= '0';
						TXD <= x"00";
					end if;
					if UDP_XMIT_REQ = '0' or state = st_arp or state = st_icmp then
						state2 <= st_notudp;--if no UDP packet ready or another packet is already in transmit,
					else
						state2 <= st_indeedudp;--do not send out a UDP packet
					end if;
				when st_indeedudp =>
					UDP_XMIT_OK <= '1';
					TXMIT <= UDP_XMIT;
					TXD <= UDP_TXD;
					if UDP_XMIT_REQ = '0' then
						state2 <= st_notudp;
					else
						state2 <= st_indeedudp;
					end if;
				end case;
			end if;
		end if;
	end process output_muxing;
end Behavioral;
