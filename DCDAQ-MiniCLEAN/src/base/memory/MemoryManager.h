#ifndef __MEMORYMANAGER__
#define __MEMORYMANAGER__

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <DCMessage.h>
#include <DCQueue.h>
#include <string>

class MemoryManager
{
 public:
  MemoryManager(){};
  virtual ~MemoryManager() {Deallocate();};

  virtual bool AllocateMemory(xmlNode * SetupNode) = 0;

  virtual void PrintStatus(int indent = 0, int verbosity = 0) = 0;

  virtual void Shutdown() = 0;
  virtual void WakeUp() = 0;
  virtual bool IsShutdown() = 0;

  //Name get functions
  const std::string & GetName() {return ManagerName;};
  const std::string & GetType() {return ManagerType;};

  //Statistics
  DCMessage::Message GetMessageOut();
  const int * const  GetMessageOutFDs() {return(MessagesOut.GetFDs());};



 protected:
  void SetName(std::string _Name){ManagerName = _Name;};
  void SetType(std::string _Type){ManagerType = _Type;};
  void Print(const std::string &text);
  void PrintError(const std::string &text);
  void SendMessageOut(DCMessage::Message &message);

  int threadCountWriting;
  int threadCountListening;

 private:
  std::string ManagerName;
  std::string ManagerType;
  virtual void Deallocate(){};
  //Outgoing messages to DCDAQ::DCMessageProcessor
  DCQueue<DCMessage::Message> MessagesOut; //Locked queue with messages 
};

#endif
