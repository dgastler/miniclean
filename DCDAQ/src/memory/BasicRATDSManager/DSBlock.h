#ifndef __DSBLOCKS__
#define __DSBLOCKS__

#include "RAT/DS/Root.hh"

struct DSBlock
{
  RAT::DS::Root * array;
  uint32_t allocatedSize;
  uint32_t usedSize;
};
#endif
