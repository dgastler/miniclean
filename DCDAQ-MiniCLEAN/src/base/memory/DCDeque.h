#ifndef __DCDEQUE__
#define __DCDEQUE__

#include <iostream>
#include <cstdio>
#include <cstring> //for strerr

#include <deque>

//ObjectModels
#include <LockedObject.h>
#include <NotLockedObject.h>

//SelectModels
#include <PipeSelect.h>
#include <NoSelect.h>



//==========================================================================
//Functions you care about:
//
//Get( Type, wait):  Gets the next Type.  Waits if specified 
//                   returns true if a message was returned in Type
//                   returns false if no message to return.  returns bad Type
//
//Add( Type, wait):  Adds the next Type.  Waits if specified
//
//==========================================================================
// NO ERROR CHECKING! NEVERY CALL Unlock() WITHOUT CALLING Lock() SUCESSFULLY.
// DO NOT call Lock() after calling Lock() SUCESSFULLY!
// DO NOT call Unlock() after calling Unlock SUCESSFULLY!
// DO NOT call Get or Add while the deque is LOCKED!
// If you are using this with threading, I am giving you a GUN and POINTING
// it at YOUR foot.  Do not mess up!
//Lock(wait):          The mutex is held in lock until unlock is called
//                     Waits if specified
//
//At(index,wait): Get's the Type at index i. Waits if specified
//
//Back(wait):     Get's the last element. Waits if specified
//
//Front(wait):    Get's the last element. Waits if specified
//
//Unlock():            Release the mutex lock from the function Lock()
//==========================================================================
//
//SingleReadFD(wait):Call this to do one read on the deque's pipe.
//                   This is helpful for when you call Get, and then
//                   want to Add the result back to the end of the deque. 
//
//GetSize():            Returns deque size
//
//Clear():           Clears all data from deque
//
//ShutdownWait():    Removes the wait option on deques (used when shutting down DCDAQ)
//
//WakeUp():          Uses condition variable to wake anyone stuck waiting on data.
//                   Used after ShutdownWait() when shutting down DCDAQ.
//Setup(Type):       Sets the value returned by Get if there is not data to be returned.
//

//==========================================================================

template< 
  class T , 
  class SelectModel = PipeSelect,
  class ObjectModel = LockedObject> 
class DCDeque: public SelectModel
{
public:
  DCDeque(): SelectModel()
  {
    //Setup the Deque
    badValueSet = false;
    isLocked = true;
  };
  
  virtual ~DCDeque()
  {
    Clear(true);
  };

  void Setup(T _badValue)
  {
    badValue = _badValue;
    badValueReturnCopy = badValue;
    badValueSet = true;
  };

  void ShutdownWait()
  {
    Lock.ShutdownLockWait();
  }

  void WakeUp()
  {
    Lock.WakeUpSignal();
  }

  size_t GetSize(){return(Deque.size());};

  bool Get( T &Obj,bool Wait = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCDEQUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCDeque badValue not set!\n");
      }
#endif

    //Needed for later, but due to some MACRO scope issues
    //this needs to be declaired here.
    bool notEmpty;
    
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	Obj = badValue;
	return(false);
      }

    //We now have the mutex!

    //if we are waiting for a new block wait on a condition variable.
    if(Wait)
      {
	while(Deque.empty() && Lock.ConditionWait())
	  {
	    //We are in condition wait with the understanding
	    //that a ShutdownLockWait() and a wakeup will
	    //cause this loop to terminate
	  }
      }
    notEmpty = !Deque.empty();    
    if(notEmpty)
      {
	//Set obj to the element at the front of the deque
	Obj = Deque.front();
	//Remove the front object.
	Deque.pop_front();
	//Read from select model
	SelectModel::Read();	
      }
    else
      {
	//Set the object to badValue;
	Obj = badValue;
      }
    //Unlock 
    Lock.UnlockObject();
    return(notEmpty);
  };

  bool Add(T &Obj,bool Wait = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCDEQUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCDeque badValue not set!\n");
      }
#endif

    //Lock
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	return(false);
      }
    
    //We now have the mutex!

    //Add obj to the end of the deque
    T PushObj = Obj;   //Make a copy so the Adder can't
                       //change it later.
    Deque.push_back(PushObj);
    //Write to our select model (write a zero)
    SelectModel::Write(0);
    
    //Now that the Object has been added to the deque, return back to the user the badValue.
    Obj = badValue;
    //signal so that if anyone is in cond_wait in Get() they wake up
    Lock.WakeUpSignal();
    //release the mutex.
    Lock.UnlockObject();
    return(true);
  };

T & At(size_t index,bool /*wait*/ = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCDEQUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCDeque badValue not set!\n");
      }
#endif

    if(!isLocked)
      {
	//Not waiting for unlock
	badValueReturnCopy = badValue;
	return badValueReturnCopy;
      }
    //We now have the mutex!
    
    //Check if index is in range.
    if(index < Deque.size())
      {
	//Return value, do not unlock
	return Deque[index];
      }

    //REturn badValue
    badValueReturnCopy = badValue;
    return badValueReturnCopy;
  }

  T& Front(bool wait = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCDEQUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCDeque badValue not set!\n");
      }
#endif

    if(!isLocked)
      {
	//Not waiting for unlock
	badValueReturnCopy = badValue;
	return badValueReturnCopy;
      }
    //We now have the mutex!
    
    if(!Deque.empty())
      {
	//Return front object
	return Deque.front();
      }

    //return badvalue
    badValueReturnCopy = badValue;
    return badValueReturnCopy;
  }

T& Back(bool /*wait*/ = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCDEQUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCDeque badValue not set!\n");
      }
#endif

    if(!isLocked)
      {
	//Not waiting for unlock
	badValueReturnCopy = badValue;
	return badValueReturnCopy;
      }
    //We now have the mutex!
    
    if(!Deque.empty())
      {
	//Return front object
	return Deque.back();
      }
    
    //return badvalue
    badValueReturnCopy = badValue;
    return badValueReturnCopy;
  }
  
  bool LockDeque(bool Wait = true)
  {
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	return false;
      }
    //We now have the mutex
    isLocked = true;
    return true;
  }

  bool UnlockDeque()
  {
    bool ret;
    if(isLocked)
      {
	ret = Lock.UnlockObject();
      }
    else
      {
	ret = false;
      }
    return ret;
  }


  bool Clear(bool wait = true)
  {
    //Lock
    if(!Lock.LockObject(wait))
      {
	//Not waiting for unlock
	return(false);
      }
    
    //We now have the mutex!
    
    //clear all objects in the deque
    while(Deque.size())
      {
	//Remove an entry
	Deque.pop_front();
	//Read from our Select model
	SelectModel::Read();
      }
	  
    //no nead to signal the condition variable since no one wants to know the
    //deque is empty. 
    
    //unlock mutex
    Lock.UnlockObject();
    return(true);
  };

  const T GetBadValue(){return(badValue);};
  
private:  
  ObjectModel Lock;
  std::deque< T > Deque;   //The free deque
  bool isLocked;
  T badValue;
  T badValueReturnCopy;
  volatile bool badValueSet;
};

#endif
