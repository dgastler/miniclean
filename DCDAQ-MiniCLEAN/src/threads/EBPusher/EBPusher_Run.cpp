#ifdef __RATDS__
#include <EBPusher.h>

void EBPusher::MainLoop()
{
  //Check for a new connection
  CheckForNewConnection();

  //Check for a message from our DCNetThread
  if(EBAssembler.IsRunning())
    {
      if(IsReadReady(EBAssembler.GetMessageFD()))
	{
	  ProcessChildMessage();
	}
      if(IsReadReady(FEPC_mem->GetToSendFDs()[0]))
	{      
	  ProcessEvent();
	}
    }
}

bool EBPusher::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      EBAssembler.Shutdown();
      FEPC_mem->Shutdown();
      RemoveReadFD(FEPC_mem->GetToSendFDs()[0]);
      SetupSelect();
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Remove the FEPCRATManager from select
      RemoveReadFD(FEPC_mem->GetToSendFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the WFD/EV block's free queue's FDs with select if
      //we have a network connection.
      if(EBAssembler.IsRunning())
	{
	  AddReadFD(FEPC_mem->GetToSendFDs()[0]);      
	}
      else
	{
	  PrintWarning("No network connection! Not listening to the FEPC memory structure!");
	}
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}

void EBPusher::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
//      float timeStep = difftime(currentTime,lastTime);
//      DCMessage::Message message;
//      DCMessage::TimedCount Count;
//      sprintf(Count.text,"Events read");
//      Count.count = timeStepProcessedEvents;
//      Count.dt = timeStep;
//      message.SetDataStruct(Count);
//      message.SetType(DCMessage::STATISTIC);
//      SendMessageOut(message,false);

      std::map<int,TH1F*> TotalQHist = packetBuilder.SendHistograms();

//      DCMessage::Message message;
//      DCMessage::DCTObject rootObj;
//      std::vector<uint8_t> dataTemp;
//      std::map<int,TH1F*>::iterator histIt;
//      for(histIt = TotalQHist.begin();
//	  histIt != TotalQHist.end();
//	  histIt++)
//	{
//	  //fprintf(stdout,"In histogram loop\n");
//	  dataTemp = rootObj.SetData((TObject * )(histIt->second),DCMessage::DCTObject::H1F);
//	  message.SetDestination();
//	  message.SetType(DCMessage::HISTOGRAM);
//	  message.SetData(&(dataTemp[0]),dataTemp.size());
//	  message.SetTime(currentTime);
//	  printf("Sending histogram %s with size %zu\n",histIt->second->GetTitle(),dataTemp.size());
//	  SendMessageOut(message,false);
//	  histIt->second->Reset();
//	}
      
      timeStepProcessedEvents = 0;
	  
	  //Setup next time
      lastTime = currentTime;
    }

  //Catch a timeout if we don't have a DCNetThread
  if((!EBAssembler.IsRunning()) && 
     (EBAssembler.GetSocketFD() != EBAssembler.badFD))
    {
      EBAssembler.Shutdown();
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(EBAssembler.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }
}

#endif
