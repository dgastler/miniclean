#include <v1720FakeReadout.h>

void  v1720FakeReadout::MainLoop()
{
  //  if(FD_ISSET(FreeFDs[0],
  //	      &GetReadSet()))
  if(IsReadReady(FreeFDs[0]) &&
     (blockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Mem->GetFree(blockIndex);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = &(Mem->Memory[blockIndex]);
	  block->dataSize = 0;
	}
    }
  
  if(blockIndex != FreeFullQueue::BADVALUE)
    {
      uint32_t eventCount = 0;      
      while(eventCount < EventsPerBlock)
	{
	  uint32_t * event = FakeEvent[iEvent];//get event
	  uint32_t eventSize = event[0]&0x0FFFFFFF;//get this event's size	      
	  if(((block->dataSize>>2) + eventSize) <= (block->allocatedSize>>2))
	    {
	      //Get a pointer to part of the Block buffer we are goign to write to.
	      uint32_t * bufferPointer = (uint32_t * )(block->buffer + block->dataSize);
	      //Copy from event to bufferPointer eventsize*4 bytes.
	      memcpy(bufferPointer,event,eventSize<<2);
	      //Set block's data size to include this event
	      block->dataSize += (eventSize <<2);		  
	      //Move to the next event.
	      eventCount++;
	      iEvent++;
	      //If we are at the end of our loaded events go back to zero.
	      if(iEvent >= FakeEvent.size())
		{
		  if(MemoryLoop)
		    {
		      iEvent = 0;
		    }
		  else
		    {
		      printf("v1720FakeReadout:   Sent all events\n");
		      Loop = false;
		      
		      RemoveReadFD(FreeFDs[0]);
		      SetupSelect();
		      break;
		    }
		}
	    } //Add event
	  else
	    {
	      break;
	    }	      
	}//Fill block
      //Pass off this full block.
      Mem->AddFull(blockIndex);
      TimeStep_BlocksRead++; 
    }
}

void  v1720FakeReadout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      BlocksRead += TimeStep_BlocksRead;
      DCMessage::SingleUINT64 Count;

      //Data message
      Count.i=TimeStep_BlocksRead;
      message.SetData(&Count,sizeof(Count));
      message.SetType(DCMessage::BLOCKSREAD);
      SendMessageOut(message,false);

      TimeStep_BlocksRead = 0;

      //Setup next time
      lastTime = currentTime;
    }
}
