#include <EBAssembler.h>

void EBAssembler::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.serverSocketFD))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < server.Size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(server[iConnection].thread->GetInPacketManager()->GetFullFDs()[0]))
	{
	  ProcessEBPusher(server[iConnection].thread);
	}
      //Check for a new message
      if(IsReadReady(server[iConnection].thread->GetMessageOutFDs()[0]))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

void EBAssembler::ProcessEBPusher(DCNetThread * thread)
{

  uint8_t * dataPointer = NULL;
  uint16_t  dataSize = 0;

  //Get new incomming packet
  int32_t iDCPacket = FreeFullQueue::BADVALUE;
  DCNetPacket * packet = NULL;
  thread->GetInPacketManager()->GetFull(iDCPacket,true);
  //Check for bad packet type
  if(iDCPacket != FreeFullQueue::BADVALUE)
    //valid packet
    {
      packet = thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Check if this is the correct kind of packet
      if(!(
	   (packet->GetType() == DCNetPacket::EB_INTEGRATED_WINDOWS_PARTIAL_PACKET) ||
	   (packet->GetType() == DCNetPacket::EB_INTEGRATED_WINDOWS_FULL_PACKET)    ||
	   (packet->GetType() == DCNetPacket::EB_ZLE_WAVEFORMS_PARTIAL_PACKET)      ||
	   (packet->GetType() == DCNetPacket::EB_ZLE_WAVEFORMS_FULL_PACKET)         ||
	   (packet->GetType() == DCNetPacket::EB_PROMPT_TOTAL_PARTIAL_PACKET)       ||
	   (packet->GetType() == DCNetPacket::EB_PROMPT_TOTAL_FULL_PACKET)  
	   ))
	{
	  //ProcessBadPacket cleans up.
	  PrintError("Unexpected packet type!\n");
	  //return packet
	  thread->GetInPacketManager()->AddFree(iDCPacket);
	  //move on
	  return;
	}
      //Got a packet with real data. 
      dataPointer = (uint8_t *) packet->GetData();
      dataSize = packet->GetSize();
    }

  //Cast the data to an EBPacket::Event structure.
  if(dataSize >= sizeof(EBPacket::Event))
    {
      //Read the header Event structure from the packet data. 
      EBPacket::Event * eventHeader = (EBPacket::Event *) dataPointer;        
      //We now need to find the corresponding EBPCBlock event
      
      //      fprintf(stderr,"Got Event #%u\n",eventHeader->ID);

      //Get this event from the Event hash
      int32_t index;
      EBPCBlock * event = NULL;
      uint64_t password;
      //first try to get the event
      int64_t retEBPCmem = EBPC_mem->GetEvent(eventHeader->ID,index,password);
      while(retEBPCmem >= 0)
	//Loop over this call untill we don't have a collision	
	{
	  //Move old event to bad event queue and 
	  //address the FEPCs at fault
	  ProcessCollision(eventHeader->ID,retEBPCmem,index,password);
	  //Now things should be fixed and we should get our event
	  retEBPCmem = EBPC_mem->GetEvent(eventHeader->ID,index,password);      
	}      
      
      //No collision
      if(retEBPCmem == returnDCVectorHash::OK)		     
	//We got the index from the vector hash and it's now locked to us. 
	{
	  //Get our event
	  event = EBPC_mem->GetEBPCBlock(index);
	  
	  //Set up status values if this is a new event. 
	  //(if they haven't been set up yet
	  if(event->eventID == -1)
	    {
	      event->eventID = eventHeader->ID;
	      event->state = EventState::UNFINISHED; 
	    }	        
	}
      else 
	{
	  PrintError("EBPCmem error!\n");
	  SendStop();
	}      

      ev = event->ds->GetEV(0);

      //Now we have a EBPCBlock for this event and we can fill it with data
      //from the newly arrived packet. 



      int processedChannels = -1;  //A count of how many channels were 
                                   //actually parsed from the packet for
                                   //this event.
      //====================================
      //Event type switch.  Order in most probable first
      //====================================
      if(eventHeader->dataType == ReductionLevel::CHAN_ZLE_INTEGRAL)
	{
	  processedChannels = ProcessCHAN_ZLE_INTEGRAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::CHAN_ZLE_WAVEFORM)
	{
	  processedChannels = ProcessCHAN_ZLE_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::CHAN_PROMPT_TOTAL)
	{
	  processedChannels = ProcessCHAN_PROMPT_TOTAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_ZLE_WAVEFORM)
	{
	  processedChannels = ProcessEVNT_ZLE_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_ZLE_INTEGRAL)
	{
	  processedChannels = ProcessEVNT_ZLE_INTEGRAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_PROMPT_TOTAL)
	{
	  processedChannels = ProcessEVNT_PROMPT_TOTAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_FULL_WAVEFORM)
	{
	  processedChannels = ProcessEVNT_FULL_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::CHAN_FULL_WAVEFORM)
	{
	  processedChannels = ProcessCHAN_FULL_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::UNKNOWN)
	{
	  PrintError("UNKNOWN reduction level!\n");
	}
      else
	{
	  PrintError("Corrupted reduction level!\n");
	}	

      //Now we have parsed our packet and we need to check that the
      //number of channels in the data we actually parsed is equal
      //to the number of channels the packet said we'd get. 

      if(processedChannels != eventHeader->channelCount)
	{
	  char * buffer = new char[100];
	  sprintf(buffer,"We lost a PMT! (%u:%u)",processedChannels,eventHeader->channelCount);
	  PrintError(buffer);
	  delete [] buffer;
	  ProcessBadPacketLostPMT(packet);
	}	  
      //Check if we have readout all the PMTs for this event.
      else if(ev->GetPMTCount() == nCopies*int(pmtList.size()))
	{
	  EBPC_mem->MoveEventToDiskQueue(eventHeader->ID,
					 index,
					 password);
	}
      else
	//if the channel add was fine, but we haven't fully filled
	//the event, return it to the EBPCRATManager
	{
	  EBPC_mem->SetEvent(eventHeader->ID,index,password);
	}
    }
  else
    {
      PrintError("Packet too small!");
    }
  //return packet
  thread->GetInPacketManager()->AddFree(iDCPacket);
}


void EBAssembler::ProcessCollision(int64_t newEventID,
				   int64_t oldEventID,
				   int32_t evIndex,
				   uint64_t password)
{
  //We have a conflict
# if __WORDSIZE == 64
  fprintf(stderr,"Collision! %ld %ld\n",oldEventID,newEventID);
# else
  fprintf(stderr,"Collision! %lld %lld\n",oldEventID,newEventID);
# endif
  //The data in the vector hash is for another event. 
  //We have a lock on it though!
  //We will move this event to the bad queue
  //Action concerning this bad event will be left to
  //another thread.
  EBPC_mem->MoveEventToBadQueue(oldEventID,
				evIndex,
				password);
  
  PrintError("Address collision\n");
}


void EBAssembler::ProcessBadPacketLostPMT(DCNetPacket*)
{
  PrintError("Lost PMT\n");
  SendStop();
}
