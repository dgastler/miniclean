#ifdef __RATDS__
#ifndef __DRPUSHERTHROWAWAY__
#define __DRPUSHERTHROWAWAY__
/*
  Thread description
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <FEPCRATManager.h>

#include <map>

class DRPusherThrowAway : public DCThread 
{
 public:
  DRPusherThrowAway(std::string Name);
  ~DRPusherThrowAway(){};

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);  

  virtual void MainLoop();
 private:

  //==============================================================
  //DCThread 
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);  
  virtual void ProcessTimeout();

  //==============================================================
  //Memory manager interface
  //==============================================================
  FEPCRATManager * mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  uint32_t timeStepEventCount;
  
};
#endif
#endif
