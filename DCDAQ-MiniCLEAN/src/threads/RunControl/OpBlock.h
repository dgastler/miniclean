#ifndef __OPBLOCK__
#define __OPBLOCK__

#include <xmlHelper.h>
#include <DCMessage.h>

#include <vector>
#include <list>

class OpBlock
{
 public:
  OpBlock(){Clear();};
  OpBlock(const OpBlock & rhs){CopyObj(rhs);};
  OpBlock & operator=(const OpBlock &rhs){CopyObj(rhs); return *this;};
  ~OpBlock(){Clear();};

  //Load ops message and expected responses from XML
  bool Load(xmlNode * blockNode);
  //Reset block to the just afer Load() state
  void Reset();
  void Clear();

  //Get this block's operations in message form to send out to DCDAQ
  bool GetOpsMessage(DCMessage::Message &message);
  //check if this is a response message that this block cares about
  bool IsMessageOfInterest(DCMessage::Message &message);

  bool IsDone(){return (responsesLeft.empty() && !failed);};
  bool HasFailed(){return failed;};
  std::string Print();
 private:
  void CopyObj(const OpBlock & rhs);

  //Data loaded for this block
  DCMessage::Message blockOps;
  struct blockResponse
  {
    bool nameSet;
    bool addrSet;
    bool typeSet;
    bool dataSet;
    
    DCMessage::Message mesg;
  };

  std::vector<blockResponse> blockResponses;

  //OpBlock operation completion data members
  bool valid;
  bool failed;
  std::list<blockResponse> responsesLeft;

};
#endif
