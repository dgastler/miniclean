#ifndef __FAKE_CONTROL__
#define __FAKE_CONTROL__

#include "DCQueue/LockedObject.h"
#include "ATLYS_Control.h"
#include "packets.h"

#include <math.h>

void * launch_FAKE_Control(void * ptr);

class FAKE_Control
{
 public:
  FAKE_Control();
  bool Setup_Threads(std::vector<std::string> & files,
		     std::vector<std::string> & IPs);
  bool Setup_Random(double _frequency, double _deadtime);
  void Start()
  {
    running = true;
    pthread_create(&thread_ID,NULL,
		   &launch_FAKE_Control,
		   this);
  };
  void Join(){pthread_join(thread_ID,NULL);};
  void Stop()
  {
    printf("Stopping FAKE Control\n");
    running = false;
    //Turn off locks  
    freq_Lock.ShutdownLockWait();
    freq_Lock.WakeUpSignal();
    //stop the boards;
    board[0].Stop();
    board[1].Stop();
    board[2].Stop();
  };
  void * Run();
 private:
  //Random numbers and times
  double Compute_dt();
  uint32_t Get_Next_Time();
  double log_N; //log(0xFFFF)
  double convert; //gives micro-seconds
  uint16_t lfsr;
  
  double frequency; //hz
  double dead_Time; //micro-seconds

  LockedObject freq_Lock;

  //Hardware
  uint32_t event_Time;
  ATLYS_Control board[3];
  ATLYS_Packet * packet[3];

  //Run thread control
  volatile bool running;
  pthread_t thread_ID;

};

#endif
