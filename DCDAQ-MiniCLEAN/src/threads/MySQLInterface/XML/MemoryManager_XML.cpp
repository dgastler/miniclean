#include <MemoryManager_XML.h>


MemoryManager_XML::MemoryManager_XML(DCMySQL * conn,int _runNumber)
{
  DCMySQLConnection = conn;
  runNumber = _runNumber;
}

bool MemoryManager_XML::Generate_XML(xmlNode * memManagersNode,
				     std::string pcName,
				     std::string mmType,
				     std::string mmName)
{
  bool ret = true;

  //build the base XML nodes for the memory manager
  memManagerNode = xmlNewChild(memManagersNode,NULL,
			       (xmlChar *)"MANAGER",NULL);
  xmlNewTextChild(memManagerNode,NULL,BAD_CAST "TYPE",BAD_CAST mmType.c_str());
  xmlNewTextChild(memManagerNode,NULL,BAD_CAST "NAME",BAD_CAST mmName.c_str());


  std::vector<std::vector<std::string> > memManagerTemplate;
  std::vector<std::vector<std::string> > memManagerData; 
  std::vector<std::vector<std::string> > memManagerDefaults;
  std::vector<std::vector<std::string> > memManagerRequired;
  if(!FillData(pcName,
	       mmType,
	       mmName,
	       memManagerTemplate,
	       memManagerData,    
	       memManagerDefaults,
	       memManagerRequired))
    {
      fprintf(stderr,"MemoryManager_XML::Generate_XML: failed to get data\n");
      ret = false;
      return ret;
    }

  //Build XML
  size_t fieldCount = memManagerTemplate[0].size();
  for(size_t iField = 0; iField < fieldCount;iField++)
    {
      //Check if there is an entry for this in memManagerData
      if(!boost::algorithm::iequals(memManagerData[0][iField],"NULL"))
	{      
	  xmlNewTextChild(memManagerNode,NULL,
			  (xmlChar*) memManagerTemplate[0][iField].c_str(),
			  (xmlChar*) memManagerData[0][iField].c_str());
	}
      //if there isn't, check if there is a default
      else if(!boost::algorithm::iequals(memManagerDefaults[0][iField],"NULL"))
	{
	  xmlNewTextChild(memManagerNode,NULL,
			  (xmlChar*) memManagerTemplate[0][iField].c_str(),
			  (xmlChar*) memManagerDefaults[0][iField].c_str());
	}
      //if there isn't, check if this is required and fail if it is
      else if(boost::algorithm::iequals(memManagerRequired[0][iField],"YES"))
	{
	  //Failed
	  fprintf(stderr,"MemoryManager_XML::Error required field %s not set\n",memManagerTemplate[0][iField].c_str());
	  ret = false;
	  return ret;
	}
    }
  return ret;
}


bool MemoryManager_XML::FillData(const std::string & pcName,
				 const std::string & memManagerType,
				 const std::string & memManagerName,
				 std::vector<std::vector<std::string> > &memManagerTemplate,
				 std::vector<std::vector<std::string> > &memManagerData,
				 std::vector<std::vector<std::string> > &memManagerDefaults,
				 std::vector<std::vector<std::string> > &memManagerRequired)
{
  std::stringstream query;
  std::string fieldNames("data_01,data_02,data_03,data_04,data_05,data_06,data_07,data_08,data_09,data_10");
  
  //=====================================================
  //Gets meaning of data entries for specific memManager type
  //=====================================================
  query << "SELECT " << fieldNames  
	<< " FROM Memory_Templates WHERE mm_type='" 
	<< memManagerType << "'";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }
  DCMySQLConnection->GetTable(memManagerTemplate);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query

  //=====================================================
  //Get data for this memManager
  //=====================================================
  query << "SELECT " << fieldNames << " FROM Memory_MemoryManagers"
	<< " where mm_type='" << memManagerType 
	<< "' AND mm_name='" << memManagerName 
	<< "' AND pc_name='" << pcName 
	<< "' AND run_number=" << runNumber 
	<< " ORDER BY order_id";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }
  DCMySQLConnection->GetTable(memManagerData);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query


  //=====================================================
  //Get the default values for datavalues
  //=====================================================
  query << "SELECT " << fieldNames 
	<<" FROM Memory_Defaults WHERE mm_type='"
	<< memManagerType << "'";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }
  DCMySQLConnection->GetTable(memManagerDefaults);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query

  //=====================================================
  //Get required data for this memManager
  //=====================================================
  query << "SELECT " << fieldNames 
	<<" FROM Memory_Necessary WHERE mm_type='"
	<< memManagerType << "'";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }
  DCMySQLConnection->GetTable(memManagerRequired);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query

  


  //Check for bad data sizes
  if((memManagerData.size() == 1) &&
     (memManagerDefaults.size() == 1) &&
     (memManagerTemplate.size() == 1) &&
     (memManagerRequired.size() == 1))
    {
      if( !( (memManagerData[0].size() == memManagerDefaults[0].size()) &&
	     (memManagerData[0].size() == memManagerRequired[0].size()) &&
	     (memManagerData[0].size() == memManagerTemplate[0].size()) ))
	{
	  return false;
	}
    }
  else
    {
      fprintf(stderr,"MemManager_XML: Failed to have unique memManager(%s:%s)\n",memManagerType.c_str(),memManagerName.c_str());
      fprintf(stderr,"    memManagerData:       %zu\n",memManagerData.size());
      fprintf(stderr,"    memManagerDefaults:   %zu\n",memManagerDefaults.size());
      fprintf(stderr,"    memManagerTemplate:   %zu\n",memManagerTemplate.size());
      fprintf(stderr,"    memManagerRequired:   %zu\n",memManagerRequired.size());
      return false;
    }
  return true;
}
