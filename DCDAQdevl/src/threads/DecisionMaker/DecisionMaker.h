#ifndef __DECISIONMAKER__
#define __DECISIONMAKER__
/*
This thread takes full events with reduced information and makes a decision
about what data will be saved. 
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <DRPCRatManager.h>

#include <TH2F.h>
#include <TFile.h>

class DecisionMaker : public DCThread 
{
 public:
  //==============================================================
  //DecisionMaker.cpp
  //==============================================================
  DecisionMaker(std::string Name);
  ~DecisionMaker();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  
  //==============================================================
  //DecisionMaker_Run.cpp
  //==============================================================
  virtual void MainLoop();
 private:

  //==============================================================
  //DecisionMaker_Run.cpp
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(){};

  //==============================================================
  //DecisionMaker_Processing.cpp
  //==============================================================
  void ComputeDecision(DRPCBlock * event);
  int  SoftwareTrigger(RAT::DS::EV*,int64_t);

  //==============================================================
  //Memory manager interface
  //==============================================================
  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  std::vector<int> reductionLevelCount;
  double statfprompt;
  int fpromptCount;
  double statearliestWindowTime;
  double statlatestWindowTime;
  double stattotalQ;
  double statpromptQ;

  std::vector<TH2F*> hist;
  TFile * rootfile;

};
#endif
