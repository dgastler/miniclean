#include <iostream>
#include <TDirectory.h>
#include <TProcessID.h>

#include "PEvent.h"


using namespace std;

ClassImp(PEvent)


//______________________________________________________________________________
  PEvent::PEvent() : fIsValid(kFALSE)
{
  fIsValid = kTRUE;

  AbvTHRS = false;

  iEventNumber = 0;

  
  
}
//______________________________________________________________________________
  
PEvent::~PEvent()
{
  Clear("C");
}

//______________________________________________________________________________

void PEvent::Clear(Option_t *option)
{
  fIsValid = kFALSE;
 
  AbvTHRS = false;

  iEventNumber = 0;

  if(option == "C")
    {
      vFast0.resize(vFast0.size(),0);
      vFast1.resize(vFast0.size(),0);
      vFast2.resize(vFast0.size(),0);
      vFast3.resize(vFast0.size(),0);
      vFast4.resize(vFast0.size(),0);
      vSlow0.resize(vFast0.size(),0);
      vSlow1.resize(vFast0.size(),0);
      vSlow2.resize(vFast0.size(),0);
      vSlow3.resize(vFast0.size(),0);
      vSlow4.resize(vFast0.size(),0);
      vTOF.resize(vFast0.size(),0);
      vShift.resize(vFast0.size(),0);
      vDelay.resize(vFast0.size(),0);
      vMaxHeight.resize(vFast0.size(),0);
    }
  else
    {
      vFast0.clear();
      vFast1.clear();
      vFast2.clear();
      vFast3.clear();
      vFast4.clear();
      vSlow0.clear();
      vSlow1.clear();
      vSlow2.clear();
      vSlow3.clear();
      vSlow4.clear();
      vTOF.clear();
      vShift.clear();
      vDelay.clear();
      vMaxHeight.clear();
    }
  
}

//______________________________________________________________________________
void PEvent::SetSize(UInt_t i)
{
  vFast0.resize(i);
  vFast1.resize(i);
  vFast2.resize(i);
  vFast3.resize(i);
  vFast4.resize(i);
  vSlow0.resize(i);
  vSlow1.resize(i);
  vSlow2.resize(i);
  vSlow3.resize(i);
  vSlow4.resize(i);
  vTOF.resize(i);
  vShift.resize(i);
  vDelay.resize(i);
  vMaxHeight.resize(i);
}
//______________________________________________________________________________
Float_t PEvent::GetSize()
{
  return(vFast0.size());
}
//______________________________________________________________________________
void    PEvent::Fast(UInt_t i,UInt_t n,Float_t value)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  vFast0[i] = value;
	  break;
	case 1:
	  vFast1[i] = value;
	  break;
	case 2:
	  vFast2[i] = value;
	  break;
	case 3:
	  vFast3[i] = value;
	  break;
	case 4:
	  vFast4[i] = value;
	  break;
	default:
	  break;
	}
    }
}
Float_t PEvent::Fast(UInt_t i,UInt_t n)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  return(vFast0[i]);
	  break;
	case 1:
	  return(vFast1[i]);
	  break;
	case 2:
	  return(vFast2[i]);
	  break;
	case 3:
	  return(vFast3[i]);
	  break;
	case 4:
	  return(vFast4[i]);
	  break;
	default:
	  return(0);
	}
    }
  return(0);
}
void    PEvent::Slow (UInt_t i,UInt_t n,Float_t value)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  vSlow0[i] = value;
	  break;
	case 1:
	  vSlow1[i] = value;
	  break;
	case 2:
	  vSlow2[i] = value;
	  break;
	case 3:
	  vSlow3[i] = value;
	  break;
	case 4:
	  vSlow4[i] = value;
	  break;
	default:
	  break;
	}
    }
}
Float_t PEvent::Slow (UInt_t i,UInt_t n)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  return(vSlow0[i]);
	  break;
	case 1:
	  return(vSlow1[i]);
	  break;
	case 2:
	  return(vSlow2[i]);
	  break;
	case 3:
	  return(vSlow3[i]);
	  break;
	case 4:
	  return(vSlow4[i]);
	  break;
	default:
	  return(0);
	}
    }
  return(0);
}
