#ifndef __V1720READOUT__
#define __V1720READOUT__

#include <map>
#include <string>

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <v1720WFD.h>
#include <xmlHelper.h>

class v1720Readout : public DCThread 
{
 public:

  v1720Readout(std::string Name);
  ~v1720Readout();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  uint32_t MinimumFreeBufferSpace;
  uint32_t MaxReadoutsPerBlock;

  //Statistics
  std::vector<uint64_t> WFDReadoutNumber;
  std::vector<uint64_t> WFDTimeStep_ReadoutNumber;

  std::vector<uint64_t> WFDReadoutSize;
  std::vector<uint64_t> WFDTimeStep_ReadoutSize;  

  std::vector<uint32_t> WFDEventCount;

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message & message);
  //Handle select timeout
  virtual void ProcessTimeout();
  
  //Memory block manager
  MemoryBlockManager * Mem; 
  MemoryBlock * block;
  int32_t blockIndex;
  
  //Setup data
  std::string WFDSetup; //Strings containing the WFD XML config strings
  xmlDoc * XMLDoc; //xml documents created from the XML strings
  bool StartWFDs();
  bool StopWFDs();


  //Time values
  time_t currentTime;
  time_t lastTime;

  //WFD vectors.
  std::vector<xmlNode*> BaseXMLNodes; //base XML node of the XML Doc
  std::vector<v1720*> WFD; //Vector of v1720WFD classes
};


#endif

