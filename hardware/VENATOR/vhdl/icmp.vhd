--This module takes incoming ping requests and creates a ping reply packet.
--The method by which this feat is accomplished is not that sophisticated--
--data is simply echoed back with the exception of the source and destination
--addresses (which are swapped), the IP protocol (changed from ping request to
--ping reply), and the data checksum (trivially updated to reflect the altered IP 
--protocol. This module must also synchronize its efforts with the packet buffers.
--Original code by Jeremy Mans. 
--Translation into VHDL along with modifications to the state machine to make it 
--decently efficient by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity icmp is Port(
	MAC_CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	SEND_CKE : in STD_LOGIC;
	PACKET_READY : in STD_LOGIC;
	DONE_WITH_PACKET : out STD_LOGIC;
	PACKET_DATA : in unsigned(7 downto 0);
	PACKET_READ_ADDR : out unsigned(9 downto 0);
	PACKET_OUT : out unsigned(7 downto 0);
	EARLY_WARNING : out STD_LOGIC;
	PACKET_XMIT : out STD_LOGIC);
end icmp;

architecture Behavioral of icmp is
	type state_type is (st_idle, st_check0wait, st_check0, st_resp_we, st_done);
	signal state : state_type := st_idle;
	signal resp_read_addr : unsigned(9 downto 0);
	signal resp_data : unsigned(7 downto 0);
	signal packet_oa : unsigned(9 downto 0);
	signal packet_oa1 : unsigned(9 downto 0);
	signal packet_oa2 : unsigned(9 downto 0);
	signal we : STD_LOGIC_VECTOR(3 downto 0);
	signal packet_ra : unsigned(9 downto 0);
	signal packet_l : unsigned(9 downto 0);
-- worth responding to: HARDWARE = 0x0 0x1, PROTOCOL = 0x8 0x0,
--                      HLEN = 0x6, PLEN=0x4, OPERATION = 0x0 0x1
--                      TARGET IA = my IP 
begin
	PACKET_XMIT <= we(3) and we(0);
	EARLY_WARNING <= we(0);
	PACKET_READ_ADDR <= packet_ra;
	resp_read_addr <= "00" & x"06" when packet_oa = "00" & x"00" else
		--this section swaps the transmit/receive MAC addresses
		"00" & x"07" when packet_oa = "00" & x"01" else
		"00" & x"08" when packet_oa = "00" & x"02" else
		"00" & x"09" when packet_oa = "00" & x"03" else
		"00" & x"0A" when packet_oa = "00" & x"04" else
		"00" & x"0B" when packet_oa = "00" & x"05" else
		"00" & x"00" when packet_oa = "00" & x"06" else
		"00" & x"01" when packet_oa = "00" & x"07" else
		"00" & x"02" when packet_oa = "00" & x"08" else
		"00" & x"03" when packet_oa = "00" & x"09" else
		"00" & x"04" when packet_oa = "00" & x"0A" else
		"00" & x"05" when packet_oa = "00" & x"0B" else
		--this section swaps the transmit/receive IP addresses
		"00" & x"1E" when packet_oa = "00" & x"1A" else
		"00" & x"1F" when packet_oa = "00" & x"1B" else
		"00" & x"20" when packet_oa = "00" & x"1C" else
		"00" & x"21" when packet_oa = "00" & x"1D" else
		"00" & x"1A" when packet_oa = "00" & x"1E" else
		"00" & x"1B" when packet_oa = "00" & x"1F" else
		"00" & x"1C" when packet_oa = "00" & x"20" else
		"00" & x"1D" when packet_oa = "00" & x"21" else packet_oa;
	--outgoing data is the incoming data (with some minor alterations)
	resp_data <= x"00" when (packet_oa2 = "00" & x"22") or
		(packet_oa2 = "00" & x"23") else PACKET_DATA + x"08" when
		(packet_oa2 = "00" & x"24") else PACKET_DATA;

	process(MAC_CLK)
	begin
		if MAC_CLK'event and MAC_CLK = '1' then
			if SEND_CKE = '1' then
				packet_oa1 <= packet_oa;
				packet_oa2 <= packet_oa1;
				we(3 downto 1) <= we(2 downto 0);
			end if;
			if RESET = '1' then
				state <= st_idle;
				we(0) <= '0';
				DONE_WITH_PACKET <= '0';
			else
				case state is
				when st_idle =>
					we(0) <= '0';
					DONE_WITH_PACKET <= '0';
					if PACKET_READY = '1' then--check the type
						packet_ra <= "00" & x"22";
						packet_oa <= "00" & x"00";
						state <= st_check0wait;
					else
						state <= st_idle;
					end if;
				when st_check0wait => 
					state <= st_check0;--one wait state
					we(0) <= '0';
					DONE_WITH_PACKET <= '0';
				when st_check0 =>
					if PACKET_DATA /= x"08" then--only handle ping!
						we(0) <= '0';
						state <= st_done;
						DONE_WITH_PACKET <= '1';--failure
					elsif SEND_CKE = '1' then
						we(0) <= '1';
						state <= st_resp_we;
						DONE_WITH_PACKET <= '0';
					else
						we(0) <= '0';
						state <= st_check0;
						DONE_WITH_PACKET <= '0';
					end if;
					DONE_WITH_PACKET <= '0';
				when st_resp_we =>
					if packet_oa2 = "00" & x"10" then
						packet_l(9 downto 8) <= PACKET_DATA(1 downto 0);
					elsif packet_oa2 = "00" & x"11" then
						packet_l(7 downto 0) <= PACKET_DATA;
					elsif packet_oa2 = "00" & x"12" and SEND_CKE = '1' then
						packet_l <= packet_l + ("00" & x"0E");--for the Ethernet header
					end if;
					if SEND_CKE = '0' then
						we(0) <= '1';
						state <= st_resp_we;
						DONE_WITH_PACKET <= '0';
					else
						PACKET_OUT <= resp_data;
						packet_ra <= resp_read_addr;
						if (packet_oa2 = packet_l) and  (packet_oa2 > "00" & x"12") then
							packet_oa <= "00" & x"00";
							we(0) <= '0';
							state <= st_done;
							DONE_WITH_PACKET <= '1';--packet sent successfully
						else
							packet_oa <= packet_oa + ("00" & x"01");
							we(0) <= '1';
							state <= st_resp_we;
							DONE_WITH_PACKET <= '0';
						end if;
					end if;
				when st_done =>
					state <= st_idle;--one wait state
					we(0) <= '0';
					DONE_WITH_PACKET <= '1';
				end case;
			end if;
		end if;
	end process;
end Behavioral;
