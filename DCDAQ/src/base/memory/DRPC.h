#ifndef __DRPC__
#define __DRPC__

#include <LockedQueue.h>
#include <LockedVectorHash.h>

class DRPC
{
 public:
  DRPC(){};
  virtual ~DRPC(){Clear();};

  enum
  {
    BADVALUE = -1,
    LOCKVALUE = -2    
  };

  //Setup queues and define the size of the EventStore.
  void Setup(uint16_t Size)
  {
      Free.Setup(BADVALUE);
      UnProcessed.Setup(BADVALUE);          
      EventStore.Setup(Size,BADVALUE,LOCKVALUE);
      Respond.Setup(BADVALUE);
  };


  //clear queues
  void Clear()
  {
    Free.Clear(true);
    UnProcessed.Clear(true);
    Respond.Clear(true);
    EventStore.Clear(true);
  };

  //Shutdown all wait options in all Locked objects
  virtual void Shutdown()
  {
    Free.ShutdownWait();
    UnProcessed.ShutdownWait();
    Respond.ShutdownWait();
    EventStore.ShutdownWait();
    WakeUp();
  };
  
  //Wakeup every Locked object (call after Shutdown())
  virtual void WakeUp()
  {
    Free.WakeUp();
    UnProcessed.WakeUp();
    Respond.WakeUp();
    EventStore.WakeUp();
  };


  //=======================================================================
  //Queues
  //=======================================================================
  //Get functions.
  virtual bool GetFree(int32_t &index,bool wait = true)
  {    
    return(Free.Get(index,wait));
  };
  virtual bool GetUnProcessed(int32_t &index,bool wait = true)
  {
    return(UnProcessed.Get(index,wait));
  };
  virtual bool GetRespond(int32_t &index,bool wait = true)
  {
    return(Respond.Get(index,wait));
  };

  //Add functions
  virtual bool AddFree  (int32_t & Index){return(Free.Add(Index,true));};
  virtual bool AddUnProcessed(int32_t & Index){return(UnProcessed.Add(Index,true));};
  virtual bool AddRespond(int32_t & Index){return(Respond.Add(Index,true));};

  //=======================================================================
  //Event Store
  //=======================================================================
  virtual bool GetStoredEvent(uint16_t Event, int32_t &index, bool wait = true)
  {
    return(EventStore.Get(Event,index,wait));
  };
  virtual bool StoreEvent(uint16_t Event, int32_t &index)
  {
    return(EventStore.Put(Event,index,true));
  }
  virtual bool ClearEvent(uint16_t Event)
  {
    return(EventStore.Clear(Event,true));
  }

 private:
  LockedQueue<int32_t> Free;  //queue of indicies of free memory blocks
  LockedVectorHash<int32_t> EventStore; //vector-hash of stored events.
  LockedQueue<int32_t> UnProcessed;  //queue of indicies of un-processed events
  LockedQueue<int32_t> Respond; //queue of indicies to respond about.
};
#endif
