#include <DRGetter.h>

void DRGetter::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(DRSender.fdSocket) || IsWriteReady(DRSender.fdSocket))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(DRSender.fdSocket);
      RemoveWriteFD(DRSender.fdSocket);      
      SetupSelect();

      //If the connect check fails
      if(!DRSender.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %lds\n",DRSender.remoteIP.c_str(),GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  DRSender.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      SendStop();
	    }	  
	}
    }

  //Check for new data from the DCNetThread
  if(DRSender.thread)
    //We have a connection
    {
      if(IsReadReady(DRSender.thread->GetInPacketManager()->GetFullFDs()[0]))
	{
	  ProcessPacket();
	}
      //Check for a new message
      if(IsReadReady(DRSender.thread->GetMessageOutFDs()[0]))
	{
	  ProcessNetThreadMessage();
	}
    }

  //Check the local DecisionQueue
  if(IsReadReady(DecisionQueue.GetFDs()[0]))
    {
      ProcessDecision();
    }

}

void DRGetter::ProcessTimeout()
{
  //Update the current time
  currentTime = time(NULL);
  //Do statistics stuff


  //==============================================================
  //Network connection stuff
  //==============================================================
  //Catch a timeout if we don't have a DCNetThread
  if((DRSender.thread == NULL) && (DRSender.fdSocket != -1))
    {
      if(DRSender.fdSocket != BadPipeFD)
	{
	  close(DRSender.fdSocket);
	  DRSender.fdSocket = BadPipeFD;
	}
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(DRSender.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }


}

bool DRGetter::ProcessMessage(DCMessage::Message & message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      
      //Since this thread always needs to be running to notice network
      //connections, we don't pause anything.  
      RemoveReadFD(DecisionQueue.GetFDs()[0]);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      AddReadFD(DecisionQueue.GetFDs()[0]);
      SetupSelect();
      //We are always running
    }
  else
    {
      ret = false;
    }
  return(ret); 
}
