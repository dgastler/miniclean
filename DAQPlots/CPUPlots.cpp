#include "CPUPlots.h"

void MakeCPUPlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grDREBPC;
  TGraph * grFEPC1;
  TGraph * grFEPC2;
  TGraph * grFEPC3;
  TGraph * grFEPC4;

  ss << "select time,(cpu0+cpu1+cpu2+cpu3+cpu4+cpu5+cpu6+cpu7) from sc_sens_cpu_cleanpc1 where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grDREBPC = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,(cpu0+cpu1+cpu2+cpu3+cpu4+cpu5+cpu6+cpu7) from sc_sens_cpu_cleanpc2 where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC1 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,(cpu0+cpu1+cpu2+cpu3+cpu4+cpu5+cpu6+cpu7) from sc_sens_cpu_cleanpc3 where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC2 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,(cpu0+cpu1+cpu2+cpu3+cpu4+cpu5+cpu6+cpu7) from sc_sens_cpu_cleanpc4 where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC3 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,(cpu0+cpu1+cpu2+cpu3+cpu4+cpu5+cpu6+cpu7) from sc_sens_cpu_cleanpc5 where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC4 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);  
  TPad * padDREBPC  = new TPad("padDREBPC","DREBPC",0.0,0.0,1.0,0.5);
  TPad * padFEPC   = new TPad("padFEPC","FEPC"  ,0.0,0.5,1.0,1.0);


  padDREBPC->SetBorderSize(0);
  padDREBPC->SetBorderMode(0);
  padFEPC->SetBorderSize(0);
  padFEPC->SetBorderMode(0);

  padDREBPC->SetRightMargin(padDREBPC->GetRightMargin()*0.3);  
  padFEPC ->SetRightMargin(padFEPC ->GetRightMargin()*0.3);

  padFEPC->SetBottomMargin(0);

  padDREBPC->SetTopMargin(0);
  padDREBPC->SetBottomMargin(padDREBPC->GetBottomMargin()*2.0);
  //  padDREBPC->SetBottomMargin(1.2);


  padFEPC ->Draw();
  padDREBPC->Draw();  


  grFEPC1->SetLineColor(kGreen);
  grFEPC2->SetLineColor(kRed);
  grFEPC3->SetLineColor(kBlue);
  grFEPC4->SetLineColor(kViolet);


  double maxFEPC1 = FindTGraphMax(grFEPC1);
  double maxFEPC2 = FindTGraphMax(grFEPC2);
  double maxFEPC3 = FindTGraphMax(grFEPC3);
  double maxFEPC4 = FindTGraphMax(grFEPC4);

  double maxFEPC = std::max(maxFEPC1,
			    std::max(maxFEPC2,
				     std::max(maxFEPC3,
					      maxFEPC4)));

  padFEPC->cd();  
  grFEPC1->GetYaxis()->SetRangeUser(-5,maxFEPC*1.1);
  grFEPC1->GetYaxis()->SetNdivisions(210);
  grFEPC1->GetYaxis()->SetLabelSize(grFEPC1->GetYaxis()->GetLabelSize()*2);
  grFEPC1->GetYaxis()->SetTitleSize(grFEPC1->GetYaxis()->GetTitleSize()*2);
  grFEPC1->GetYaxis()->SetTitleOffset(grFEPC1->GetYaxis()->GetTitleOffset()*0.5);
  grFEPC1->GetYaxis()->SetTitle("Total CPU usage (%)");
  grFEPC1->GetXaxis()->SetLabelColor(kWhite);
  grFEPC1->GetXaxis()->SetTitleColor(kWhite);
  grFEPC1->Draw("AL");
  grFEPC2->Draw("L");
  grFEPC3->Draw("L");
  grFEPC4->Draw("L");
  TLegend * leg = new TLegend(0.75,0.15,0.95,0.65);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  leg->SetBorderSize(1);
  leg->SetShadowColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(grFEPC1,"FEPC-1  CPU(%)","l");
  leg->AddEntry(grFEPC2,"FEPC-2  CPU(%)","l");
  leg->AddEntry(grFEPC3,"FEPC-3  CPU(%)","l");
  leg->AddEntry(grFEPC4,"FEPC-4  CPU(%)","l");
  leg->AddEntry(grDREBPC,"DREBPC CPU(%)","l");
  leg->Draw("SAME");

  padDREBPC->cd();  
  grDREBPC->GetYaxis()->SetRangeUser(0,FindTGraphMax(grDREBPC)*1.1);
  grDREBPC->GetYaxis()->SetNdivisions(210);
  grDREBPC->GetYaxis()->SetLabelSize(grDREBPC->GetYaxis()->GetLabelSize()*2);
  grDREBPC->GetYaxis()->SetTitleSize(grDREBPC->GetYaxis()->GetTitleSize()*2);
  grDREBPC->GetYaxis()->SetTitleOffset(grDREBPC->GetYaxis()->GetTitleOffset()*0.5);
  grDREBPC->GetYaxis()->SetTitle("");
  grDREBPC->GetXaxis()->SetTitle("Run Time (hours)");
  grDREBPC->GetXaxis()->SetLabelSize(grDREBPC->GetXaxis()->GetLabelSize()*2);
  grDREBPC->GetXaxis()->SetTitleSize(grDREBPC->GetXaxis()->GetTitleSize()*2);
  grDREBPC->Draw("AL");

  c1->Print("CPU_Usage.pdf");
  

  delete c1;
}

