#include <DRTriggerAssembler.h>


DRTriggerAssembler::DRTriggerAssembler(std::string Name)
{
  SetType("DRTriggerAssembler");
  SetName(Name);
  Loop = true;
  badRunningChecks=0;
}

bool DRTriggerAssembler::Setup(xmlNode * SetupNode, 
			       std::vector<MemoryManager*> &MemManager)
{
  Ready = false;

  //================================
  //Get the DRPC memory manager
  //================================
  if(!FindMemoryManager(DRPC_mem, SetupNode, MemManager))
    {
      return false;
    }

  //===============================
  //Load detector info
  //===============================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::WFD_COUNT);
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete.\n");
      return false;
    }
   
  //Packet ID response check number.
  packetID = 0;
  sockfd = socket(AF_INET,SOCK_DGRAM,0);
  if(sockfd == -1)
    return false;
  //Set socket to non-blocking
  SetNonBlocking(sockfd,true);
  
  bzero(&servaddr,sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr=inet_addr("192.168.22.144");
  servaddr.sin_port=htons(791);
  packet = reinterpret_cast<uint32_t*>(packetBytes);//data in packet can be accessed in both uint32_t and char form

  if(FindSubNode(SetupNode, "CLOCKLOW") == NULL)
    clock_low = 0;
  else
    GetXMLValue(SetupNode, "CLOCKLOW", GetName().c_str(), clock_low);

  if(FindSubNode(SetupNode, "CLOCKHIGH") == NULL)
    clock_high = 0;
  else
    GetXMLValue(SetupNode, "CLOCKHIGH", GetName().c_str(), clock_high);

  if(FindSubNode(SetupNode, "NEXTEVENT") == NULL)
    next_event = -1;
  else
    GetXMLValue(SetupNode, "NEXTEVENT", GetName().c_str(), next_event);

  if(FindSubNode(SetupNode, "GATEDURATION") == NULL)
    gate_duration = 0x3E8;//8 us trigger gate
  else
    GetXMLValue(SetupNode, "GATEDURATION", GetName().c_str(), gate_duration);

  if(FindSubNode(SetupNode, "TRIGGEROFFSET") == NULL)
    trigger_offset = 0x39D;// 200ns presamples for 8us window
  else
    GetXMLValue(SetupNode, "TRIGGEROFFSET", GetName().c_str(), trigger_offset);

  if(FindSubNode(SetupNode, "TRIGGERMASK") == NULL)
    trigger_mask = 0xFF;
  else
    GetXMLValue(SetupNode, "TRIGGERMASK", GetName().c_str(), trigger_mask);

  if(FindSubNode(SetupNode, "TRIGGERACTIVETIME") == NULL)
    trigger_active_time = 0x40;
  else
    GetXMLValue(SetupNode, "TRIGGERACTIVETIME", GetName().c_str(), trigger_active_time);

  if(FindSubNode(SetupNode, "USEEXTERNALCLOCK") == NULL)
    use_external_clock = 0;
  else
    GetXMLValue(SetupNode, "USEEXTERNALCLOCK", GetName().c_str(), use_external_clock);

  //turn use_external_clock and enable_triggers into booleans
  use_external_clock = !!use_external_clock;
  enable_triggers = 0;


  ///Initialize the triggerbox and fail if there is a failure. 
  //Keep trying for 45 seconds to account for clock reset. 
  time_t time_now = time(NULL);
  time_t time_end = time_now + 120;


  while(time_now < time_end)
    {
      time_now = time(NULL);
      if(InitializeTriggerBox() >= 0)
	{	 	  
	  //Read out any event in the device and throw them away.
	  uint16_t words;
	  read_registers(1, 4, 1);
	  words = (((uint16_t)packetBytes[4] & 0x00FF)<<6) | (((uint16_t)packetBytes[5] & 0x00F0)>>2);//upper 12 bits of register 4
	  if(words != 0)
	    {
	      parsing_position = 0;
	      read_registers(words, 16, 0);//read all available events--starting at register 16
	    }
	  std::stringstream ss;
	  ss << "Clearing " << event_queue.GetSize() << " events from the trigger box!\n";
	  Print(ss.str());
	  event_queue.Clear();
	  
	  Ready = true;
	  break;
	}
      else
	{
	  Ready = false;
	}      
      sleep(5);
      PrintWarning("Trying to connect to the VENATOR module\n");
    }
  if(Ready)
    {
      period_perturber = new PID(10, 1, 1);//Pterm weighted ten times more heavily than other terms
    }
  return Ready;
}

void DRTriggerAssembler::MainLoop()
{ 
  if(IsReadReady(event_queue.GetFDs()[0]))
    {
      sEvent event;
      if(event_queue.Get(event))
	HandleEvent(event);
    }
}


void DRTriggerAssembler::ProcessTimeout()
{
  //Set time.
  currentTime = time(NULL);
  if((Period != 0) && (Loop == true))
    {
      //We should check if something bad has happened on VENATOR that would cause it to stop
      if(read_registers(6,1,1) >= int(6*sizeof(uint32_t)))  //Read out the status of the VENATOR
	{
	  //Check for stop if we say we are running
	  uint32_t runningBit = packet[5]&0x1; //access the running bit from the packet we just read out
	  //If we have the VENATOR set to run, but it is not, then we should fail
	  if(Loop && (!!enable_triggers) && (!runningBit))
	    {
	      if(badRunningChecks > 3)
		{
		  //	      fprintf(stderr,"DRTrigger check: %d %d\n",!!enable_triggers,!runningBit);
		  PrintError("VENATOR is no longer running");
		  DCMessage::Message message;
		  message.SetType(DCMessage::PAUSE);
		  SendMessageIn(message);
		}
	      else
		{
		  badRunningChecks++;
		}
	    }
	  else 
	    {
	      badRunningChecks=0;
	    }
	}

      //Read out how many events we can read out of VENATOR
      uint16_t words;
      read_registers(1, 4, 1);
      
      words = (((uint16_t)packetBytes[4] & 0x00FF)<<6) | (((uint16_t)packetBytes[5] & 0x00F0)>>2);//upper 12 bits of register 4
      if(words != 0)
	{
	  parsing_position = 0;
	  read_registers(words, 16, 0);//read all available events--starting at register 16
	}
    }
}

void DRTriggerAssembler::HandleEvent(sEvent event)
{  
  //DRPC MM interface variables
  int32_t evIndex;
  uint64_t password = DRPC_mem->GetBlankPassword();
  RATEVBlock *evBlock = NULL;
  
  //Load this event from the DRPC memory manager
  int64_t eventKey = DRPC_mem->GetKey(event);
  int64_t retDRPCmem = DRPC_mem->GetEvent(eventKey, evIndex, password);
#ifdef __GROUP_TIMING_DEBUG__
  fprintf(stderr,"DRTriggerAssembler using key: %zd(0x%zX) Got index:%d\n",eventKey,eventKey,evIndex);
#endif
  

  while(retDRPCmem >= 0)
    //If there is a collision
    {
      //Report the collision
      int64_t retDRPCmem_collision = DRPC_mem->GetEvent(retDRPCmem,evIndex,password);
      if(retDRPCmem_collision == returnDCVector::OK)
	{
	  evBlock = DRPC_mem->GetDRPCBlock(evIndex);
	  int ev_eventID = -1;
	  int boardCount = -1;
	  int triggerPattern = -1;
	  if(evBlock->ev != NULL)
	    {
	      ev_eventID = evBlock->ev->GetEventID();	      
	      boardCount = evBlock->ev->GetBoardCount();
	      triggerPattern = evBlock->ev->GetTriggerPattern();
	    }
	  
	  std::stringstream ss;
	  ss << "Collision!  Old key: " << retDRPCmem
	     << "   New key: " << eventKey << std::endl
	     << "   Board count: " << boardCount << std::endl
	     << "   dt: " << double(eventKey)-double(retDRPCmem) << std::endl;
	  for(int WFDIndex = 0; WFDIndex<boardCount; WFDIndex++)
	    {
	      std::vector<uint32_t> boardHeader = evBlock->ev->GetBoard(WFDIndex)->GetHeader();	
	      int wfdID = evBlock->ev->GetBoard(WFDIndex)->GetID();
	      ss << "\t WFD " <<  wfdID
		 << "  Time:" << std::hex << boardHeader[3]
		 << std::dec << "\n";
	    }	  
	  PrintWarning(ss.str().c_str());
	  //fprintf(stderr,ss.str().c_str());
 

	  //char errorString[1000];
	  //snprintf(errorString,sizeof(errorString),
	  //	   "Event %"PRId64" collided with event %"PRId64"\n Event ID: %i\n Board count: %i\n Trigger pattern: 0x%08X\n",
	  //	   eventKey,retDRPCmem,ev_eventID,boardCount,triggerPattern);
	  
	  // PrintWarning(errorString);
	}
      else if(retDRPCmem_collision > returnDCVector::OK)
	{
	  char errorString[1000];
	  snprintf(errorString,sizeof(errorString),
		   "Event %"PRId64" collided with event %"PRId64"\n Could not get the collided-with event; GetEvent returned %"PRId64"",
		   eventKey,retDRPCmem,retDRPCmem_collision);
	  PrintWarning(errorString);
	}

      //Move the old event to the bad queue
      DRPC_mem->MoveEventToBadQueue(retDRPCmem,
				    password,
				    Level::STORAGE_COLLISION);
      //Now things should be fixed and we should get our event
      retDRPCmem = DRPC_mem->GetEvent(eventKey,evIndex,password);      
    }      
  //We should have something other than a collision now

  //Everything is ok
  if(retDRPCmem == returnDCVector::OK)
    {
      //Get our evBlock
      evBlock = DRPC_mem->GetDRPCBlock(evIndex);
      
      if(evBlock != NULL)
	{
	  //copy event information into the RAT EV Block
	  evBlock->eventID = (int64_t)event.eventID;
	  evBlock->eventTime = event.timeStamp;
	  evBlock->eventTriggers = event.triggerCounterNibbles;
	  if(evBlock->ev != NULL)
	    {
	      evBlock->ev->SetEventID(evBlock->eventID);
	      evBlock->ev->SetTriggerPattern((int)event.triggerCounterNibbles);
	      //Check whether this event is done and ready to be moved to Unprocessed queue
	      if(evBlock->ev->GetBoardCount() == int(detectorInfo.GetWFDCount()))
	      	{
		  // 	  std::stringstream ss;
		  // 	  ss << "Finished event. " << "Event ID: " << evBlock->ev->GetEventID() <<" Event time: " << evBlock->eventTime << std::endl;
		  //Print(ss.str().c_str());
		  DRPC_mem->MoveEventToUnProcessedQueue(eventKey, password);
		}
	      else
		{
		//DRPCAssembler has not finished with this event yet.
		//Return it to the DRPCRATManager.

//		  std::stringstream ss;
//		   ss << "VENATOR event board count:  " << evBlock->ev->GetBoardCount() << "\n";
//		   ss << "Event time: " << evBlock->eventTime << std::endl
//		      << "Event ID: " << evBlock->eventID << std::endl;
//		   for(int WFDIndex = 0; WFDIndex<evBlock->ev->GetBoardCount(); WFDIndex++)
//		     {
//		       std::vector<uint32_t> boardHeader = evBlock->ev->GetBoard(WFDIndex)->GetHeader();	
//		       int wfdID = evBlock->ev->GetBoard(WFDIndex)->GetID();
//		       ss << "\t WFD " <<  wfdID
//		   	 << "  Time:" << boardHeader[3]
//		   	 << std::dec << "\n";
//		     }
//		   fprintf(stderr,"%s\n",ss.str().c_str());
		   //Print(ss.str().c_str());
		  DRPC_mem->SetEvent(eventKey, evIndex, password);
		}
	    }	  
	  else
	    {
	      //DRPCAssembler has not finished with this event yet.
	      //Return it to the DRPCRATManager.
	      DRPC_mem->SetEvent(eventKey, evIndex, password);
	    }
	}
      else
	{	  
	  PrintError("Found NULL evBlock!");
	}
    }  
  else if(retDRPCmem == returnDCVector::BAD_PASSWORD)
    {
      //Someone else has this event checked out.
      if(!event.has_been_blocked)
	{
	  //Note that this happened
	  event.has_been_blocked = true;
	  //put event at the end of queue--try again later
	  event_queue.Add(event);	  
	}
      else
	{
	  //Drop the event
	}
    }
  else
    {
      std::stringstream ss;
      ss << "DRPC_mem error! " << event_queue.GetSize() 
	 <<" , " << event_queue.GetSize() 
	 <<" , " << retDRPCmem 
	 <<" , " << eventKey 
	 <<" , " << evIndex 
	 <<" , " << password << endl;
      PrintError(ss.str().c_str());      
    }
}

void DRTriggerAssembler::SetPeriod(uint32_t _Period)
{
  Period = _Period;
  SetDefaultSelectTimeout(Period/1000000, Period%1000000);
}

bool DRTriggerAssembler::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      //===============================
      //Listen to the local DCQueue
      //===============================
      SetPeriod(0);
      RemoveReadFD(event_queue.GetFDs()[0]);
      SetupSelect();

      //Stop the run on the board
      enable_triggers = 0;
      InitializeTriggerBox();

      Loop = false;
      Running = false;
      DRPC_mem->Shutdown();
      event_queue.ShutdownWait();
      //      event_queue.Clear();//delete the queue
      //     delete period_perturber;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //===============================
      //Listen to the local DCQueue
      //===============================
      RemoveReadFD(event_queue.GetFDs()[0]);
      SetupSelect();

      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
      enable_triggers = 0;
      InitializeTriggerBox();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      enable_triggers = 1;      
      InitializeTriggerBox();

      //===============================
      //Listen to the local DCQueue
      //===============================
      AddReadFD(event_queue.GetFDs()[0]);


      //WTF?  
      DCtime_t tempTime = message.GetTime();
      fprintf(stderr,"%s (%s): Starting up thread.\n",
	      GetTime(&tempTime).c_str(),
	      GetName().c_str());
      SetupSelect();
      Loop = true;
      Print("Starting VENATOR Run");
      SetPeriod(2);
    }
  else
    ret = false;
  return(ret);
}

