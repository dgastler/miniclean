-- Gray code counter with variable width (generic width)
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity gray_n is generic(
	WIDTH : integer := 32); port(
	RESET, SET, CLOCK : in STD_LOGIC;
	A : in unsigned(WIDTH downto 1);
	Q : out unsigned(WIDTH downto 1));
end gray_n;

architecture Behavioral of gray_n is
	signal q1 : unsigned(WIDTH downto 0) := (0 => '1', others => '0');
	signal z : unsigned(WIDTH downto 0);
	signal extra : STD_LOGIC_VECTOR(WIDTH downto 0) := (0 => '1', others => '0');
begin
	process(A, extra)
	begin
		for i in 1 to WIDTH loop
			extra(i) <= extra(i-1) xor A(i);
		end loop;--extra(WIDTH) is an odd-parity bit
	end process;

	process(RESET, CLOCK)
	begin
		if RESET = '1' then
			q1 <= (0 => '1', others => '0');
		elsif SET = '1' then
			q1(WIDTH downto 1) <= A;
			q1(0) <= extra(WIDTH);
		elsif CLOCK'event and CLOCK = '0' then--update counter at end of gate
			q1(0) <= not q1(0);-- parity bit always inverts
			q1(WIDTH - 1 downto 1) <= q1(WIDTH - 1 downto 1) xor z(WIDTH - 2 downto 0);
			q1(WIDTH) <= q1(WIDTH) xor z(WIDTH - 1) xor z(WIDTH);
		end if;
	end process;
	z <= ((not q1) + 1) and q1;-- marks the position of the right-most 1 in q1
	Q <= q1(WIDTH downto 1);
end Behavioral;

