#ifndef ROOT_Event
#define ROOT_Event
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Event                                                                //
//                                                                      //
// Description of the event and track parameters                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>
//#include <TObjArray.h>
#include <TClonesArray.h>
#include <TRefArray.h>
#include <TRef.h>
#include <TH1.h>
#include <TMath.h>

#define DATESIZE 30

class RawWave : public TObject {

 private:
  //  The iSize expression after Wave is to let Root know that iSize is the 
  //  number of elements in Wave.
  Int_t   iID;
  Int_t   iTrig;
  Float_t fVOffset;
  Float_t fVMult;
  Float_t fTick;
  Int_t   iSize;
  UChar_t * Wave; //[iSize]
 public:
  RawWave(){iSize = 0; Wave = 0;}
  ~RawWave() {}

  void      Build(UChar_t *fData,Int_t iSize,Float_t fvoffset,Float_t fvmult,Float_t ftick,Int_t ID);
  void      Build(UChar_t *fData,Int_t iSize);
  void      Build(UInt_t *fData,Int_t iSize);

  Float_t   Voffset() {return(fVOffset);}
  void      Voffset(Float_t f) {fVOffset = f;}
  Float_t   VMult() {return(fVMult);}
  void      VMult(Float_t f) {fVMult = f;}
  Float_t   Tick() {return(fTick);}
  void      Tick(Float_t f) {fTick = f;}
  Int_t     ID(){return(iID);}
  void      ID(Int_t i){iID = i;}
  Int_t     Trig(){return(iTrig);}
  void      Trig(Int_t i){iTrig = i;}
  Int_t     Size(){return(iSize);}

  UChar_t   Waveform(Int_t i);
  void      Waveform(Int_t i, UChar_t data);
  void      Clear(Option_t *option);
  
  
  ClassDef(RawWave,2)
    
};

class EventHeader {
  
 private:
  Int_t   iEvent;
  Int_t   iRun;
  Char_t  aDate[DATESIZE];
  
 public:
  EventHeader() {iEvent = 0; iRun = 0; }
  virtual ~EventHeader() { }
  void    Set(Int_t i, Int_t r, Char_t * d, Int_t n);
  Int_t   GetEvtNum() const { return iEvent; }
  Int_t   GetRun() const { return iRun; }
  void    GetDate(Char_t adate[DATESIZE]) const {for(Int_t i =0; i < DATESIZE;i++) adate[i] = aDate[i]; }
  
  ClassDef(EventHeader,1)  //Event Header
};
    
class Event : public TObject {
  
 private:
  
  Int_t          iRealRaw;
  Int_t          iRaw;
  TClonesArray   *RawWaves; //->
  //TObjArray  *RawWaves;
  
  
  Bool_t         fIsValid;  
  
  EventHeader    fEvtHdr;
  
  
 public:
  Event();
  virtual ~Event();
  
  void          Clear(Option_t *option);
  Bool_t        IsValid() const { return fIsValid; }
  //  void          Reset();
  
  void          SetHeader(Int_t i, Int_t run,Char_t *aDate,Int_t aDate_size);
  EventHeader   *GetHeader() { return &fEvtHdr; }
  
  // "i" refers to which waveform. "n" refers to an elementin that waveform
  //************* Set up a system of titles to differentiate graphs of raw
  //************* waveforms and processed ones. 
  
  
  RawWave       *AddWave();
  void          SetID(Int_t i,Int_t iID);
  void          SetTrig(Int_t i,Int_t iTrig);
  void          SetVOffset(Int_t i,Float_t fVOffset);
  void          SetVMult(Int_t i,Float_t fVMult);
  void          SetTick(Int_t i,Float_t fTick);
  void          SetWaveform(Int_t i,Int_t n,UChar_t *fY);
  void          SetWaveform(Int_t i,Int_t n,UInt_t *fY);
  
  Int_t         GetSize(Int_t i);
  Int_t         GetID(Int_t i);
  Int_t         GetTrig(Int_t i);
  Float_t       GetVOffset(Int_t i);
  Float_t       GetVMult(Int_t i);
  Float_t       GetTick(Int_t i);
  UChar_t        GetPoint(Int_t i, Int_t n);
  
  RawWave       *GetWave(Int_t i);
  Int_t         NRaw(){return(iRaw);}
  
  
  
  
  
  
  
  

   ClassDef(Event,2)  //Event structure
};


#endif
