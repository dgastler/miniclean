/*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/

#include "CLEANViewer.h"

//function for loading a file into CLEANViewer form the command line argument
void CLEANViewer::LoadFile(const char * DataFile)
{



  
 
  if(DEBUG) fprintf(stderr,"CLEANViewer::LoadFile(char * DataFile)\n");
  
  Bool_t GoodFile = true;
 
  // Lets check if the file exists and is a ROOT file.



      

  TFile * TestFile = TFile::Open(DataFile);
  if(!TestFile)
    {

      GoodFile = false;
    }
  else
    {
      TTree *  TestTree = (TTree *) TestFile->Get("T");
      if(TestTree)
	{  
	  // Assume this is a RAT file
	  std::string TreeName = TestTree->GetTitle();
	  std::string RATTreeName = "RAT Tree";
	  std::cout << TreeName << std::endl;
	  printf("Tree Type: %s\n",TreeName.c_str());
	  if(TreeName == RATTreeName){
	    GoodFile = true;
	    //This is a RAT file;
	  }
	  else
	    {
	      //Not a RAT file
	      //program will exit since GoodFile == False
	      fprintf(stderr,"Incorrect TTree.\n");
	      GoodFile = false;
	    }
	}
      else
	{
	  //No TTree in the file.
	  fprintf(stderr,"No TTree.\n");
	  GoodFile = false;
	}
      TestFile->Close();
    }
  delete TestFile;
  
  if(!GoodFile)
    {
      //Exit if anything went wrong. 
      //CloseWindow();

      //If not RAT format, try DAQ data format
      
      LoadFileDAQ(DataFile);
    }
  else
    {  
      //open RAT reader
      ratReader = new RAT::DSReader(DataFile);
      if(DEBUG) fprintf(stderr,"just opened file \n");
      //Get the number of events for number entry
      nEvents = ratReader->GetTotal();
      if(AVG>nEvents) AVG=nEvents;
      //make the max limit on the event entry nEvents-1
      eventNumberEntry->SetLimitValues(0, nEvents-1);
      eventNumberEntry->SetState(kTRUE);
      

      //use nEvents to resetup the progress bar
      eventProgress->SetRange(0, nEvents-1);
      eventProgress->SetPosition(0);

      //Event#0 is the first event loaded
      currentEvent = 0;
      eventNumberEntry->SetNumber(currentEvent);
      

      if(AVG>0)
	{
	  if(ratReader->GetEvent(0)->GetEV(0)->GetPMTCount()>0)
	    {
	      fprintf(stderr,"Running through all %i events \n",int(AVG));
	      for(int iPMT=0;iPMT<ratReader->GetEvent(0)->GetEV(0)->GetPMTCount();iPMT++)
		{
		  SummedChargeVector.push_back(0); 
		}
	      double nPercentDone=0;
	      double nEventsDone;
	      for(int iEvents=0;iEvents<int(AVG);iEvents++)
		{
		  nEventsDone=iEvents+1;
		  while(nEventsDone/AVG>nPercentDone/100)
		    {
		      nPercentDone+=10;

		      fprintf(stderr,"%i percent completed \n",int(nPercentDone));
		
		    }

		  int pmtID;
		  for(int iPMT=0;iPMT<ratReader->GetEvent(iEvents)->GetEV(0)->GetPMTCount();iPMT++)
		    {
		      pmtID=ratReader->GetEvent(iEvents)->GetEV(0)->GetPMT(iPMT)->GetID();
		      SummedChargeVector[pmtID]+=ratReader->GetEvent(iEvents)->GetEV(0)->GetPMT(iPMT)->GetTotalQ()/nEvents;

		    }
		}
	    }
	}
      if(DEBUG) fprintf(stderr,"About to do NumberEV(kFALSE) \n");
     
      
      //Get the Number of EV for combo box
      NumberEV(kFALSE,true);

 


      //initial filling of PMT checkboxes vector
      FillPmtVector();

      
      //      DrawEventDisplay();
      
      //displayTabTCanvas->Update();
      //displayTabTCanvas->Modified();
      fprintf(stderr,"About to Update \n");
      //displayTCanvas->Update();
      fprintf(stderr,"About to Modify \n");
      //displayTCanvas->Modified();
    }
}

void CLEANViewer::LoadFileDAQ(const char * DataFile)
{

  //open file (this code based on if not directly coppied from MemBlockReader in DCDAQ)
  //if(true)
  //{
  InFile=fopen(DataFile,"rb");
  fpos_t InFilePos;
  fgetpos(InFile,&InFilePos);
  memBlockStartVec.push_back(InFilePos);
 
  cBlock=new MemoryBlock();  //compressed memBlock
  cBlock->buffer=new uint32_t[100];
  cBlock->allocatedSize=100;
  dBlock=new MemoryBlock();  //decompressed memBlock
  dBlock->buffer=new uint32_t[100];
  dBlock->allocatedSize=100;
  DAQdat=true;
  nEvents=99999999;
  //first word should be block header word
  ReadMemBlock(cBlock);
  //Now decompress memblock if necessary
  if(cBlock->uncompressedSize>0)
    {
      // fprintf(stderr,"DECOMPRESS\n");
      DecompressMemBlock(cBlock,dBlock);
    }
  else
    {
      if(dBlock->allocatedSize<cBlock->dataSize)
	{
	  delete (dBlock->buffer);
	  dBlock->buffer=new uint32_t[cBlock->dataSize];
	}
      memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
      dBlock->dataSize=cBlock->dataSize;
    }

 
  ratDS=new RAT::DS::Root();
  DAQBufferPtr=dBlock->buffer;
  //Read run header
  DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
 
  ratEV=ratDS->AddNewEV();
  int32_t size = StreamBufftoEV_func((uint8_t*)DAQBufferPtr,ratEV);

  DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
  //DAQBufferPtr += size;
  sizeBlockRead = (uint32_t)(DAQBufferPtr-dBlock->buffer);
  //DAQdat=true;
 
  currentEvent=ratEV->GetEventID();
  bufferEvtNumber=currentEvent+1;
  firstEvent=currentEvent;
  bufferMemNumber=0;
  eventNumberEntry->SetNumber(currentEvent);
  eventNumberEntry->SetState(kTRUE);
  eventNumberEntry->SetLimitValues(currentEvent,currentEvent+9999999);
  
  NumberEV(kFALSE,true);

  FillPmtVector();
 
  
  //  DrawEventDisplay3D();
 

  //displayTabTCanvas->Update();
  //displayTabTCanvas->Modified();
  // displayTCanvas->Update();
  //displayTCanvas->Modified();
 


}


void CLEANViewer::ReadMemBlock(MemoryBlock * mBlock)
{
  MemBlockMessage::header blockHeader;
 
  int freadret = fread(&blockHeader,sizeof(blockHeader),1,InFile);
  if(freadret==1)
    {
      if(!feof(InFile)) //check that not at end of file
	{
	  uint32_t blockSize;
	  //Make sure header word is valid
	  if(blockHeader.sizeMagic!= 0xAA)
	    {
	      fprintf(stderr,"Bad Header word to start DCDAQ .dat file %x \n",blockHeader.sizeMagic);
	      CloseWindow();
	    }
	  blockSize=blockHeader.size;

	 
	  //Find if memBlock compressed;
	 

	  if(blockHeader.compressionMagic == 0xBB)
	    {
	      
	      mBlock->uncompressedSize=blockHeader.compressionSize;     
	    }
	  else
	    {

	      if(blockHeader.compressionMagic != 0xCC)
		{
		  fprintf(stderr,"Bad uc Header word %x \n",blockHeader.compressionMagic);
		  CloseWindow();
		}

	    }
	  //read blockSize words, this will give us first memblock.  note we only read one memblock at a time
	  //blockSize=int(ceil(blockSize/sizeof(uint32_t)));
	  if(blockSize>mBlock->allocatedSize)
	    {
	      delete (mBlock->buffer);
	      mBlock->buffer=new uint32_t[blockSize];
	      mBlock->allocatedSize=blockSize;
	    }
	  mBlock->dataSize=0;
	  uint32_t * bufferPointer= (uint32_t *)(mBlock->buffer);
	  freadret=fread(bufferPointer,sizeof(uint32_t),blockSize,InFile);
	  if(uint32_t(freadret)== blockSize)
	    {
	      mBlock->dataSize=blockSize;
	     
	    }
	  else
	    {
	      fprintf(stderr,"Bad read of DCDAQ memblock");
	      CloseWindow();
	    }
	}
      
      else
	{
	  fprintf(stderr,"Size of file Bad, ended after header");
	  CloseWindow();
	}

    }

  else
    {
      if(ferror(InFile))
	{	
	  fprintf(stderr,"Problem reading header word");
	  CloseWindow();
	}
      else
	{
	  fprintf(stderr,"End of file, no header word");
	  CloseWindow();
	}
    }


}

void CLEANViewer::DecompressMemBlock(MemoryBlock * cBlock,MemoryBlock * dBlock)
{
 
  if(dBlock->allocatedSize<(uint32_t)cBlock->uncompressedSize)
    {
      delete(dBlock->buffer);
      dBlock->buffer = new uint32_t[cBlock->uncompressedSize];
      dBlock->allocatedSize=cBlock->uncompressedSize;
    }
  z_stream stream;
  stream.zalloc = Z_NULL;
  stream.zfree = Z_NULL;
  stream.opaque = Z_NULL;
  if(Z_OK == inflateInit(&stream))
    {
      stream.next_in=(Bytef*) cBlock->buffer;
      stream.avail_in=(cBlock->dataSize)<<2;
      //stream.avail_in=(cBlock->dataSize);
      stream.next_out=(Bytef*) dBlock->buffer;
      stream.avail_out=(dBlock->allocatedSize)<<2;
      //stream.avail_out=(dBlock->allocatedSize);
      int ret = inflate(&stream,Z_FINISH);
      if(ret == Z_STREAM_END)
	{
	  inflateEnd(&stream);
	  dBlock->dataSize=dBlock->allocatedSize-(stream.avail_out>>2);
	  // dBlock->dataSize=dBlock->allocatedSize-(stream.avail_out);
	  if(dBlock->dataSize != (uint32_t)cBlock->uncompressedSize)
	    {
	      fprintf(stderr,"uc size was incorrect \n");
	    }
	}
      else
	{
	  fprintf(stderr,"Error with decompression \n");
	  CloseWindow();
	}
    }
   
}


Bool_t CLEANViewer::LoadShiftModeZMQ(const char * InputName)
{


  string daqZMQName;
  daqZMQName=InputName;
  //adjust type of string, add tcp:// if not yet there
  if(daqZMQName.find("tcp://")==string::npos)
    {
     
      daqZMQName.insert(0,"tcp://");
    }
  if(daqZMQName.find_last_of(":")<4)
    {

      daqZMQName.insert(daqZMQName.size(),":10000");
    }
  printf("daqZMQName: %s \n",daqZMQName.c_str());
  context = new zmq::context_t(1);
  daqSocketRequest=new zmq::socket_t(*context,ZMQ_REQ);
  daqSocketRequest->connect(daqZMQName.c_str());

  //send request for port 
  ZMQ::Request * zmq_req=new ZMQ::Request();
  zmq_req->req=ZMQ::REQPORT;
  zmq::message_t message(sizeof(ZMQ::Request));
  memcpy(message.data(),zmq_req,sizeof(ZMQ::Request));
  fprintf(stderr,"Requesting port assignment from DCDAQ.  Awaiting response \n");
  daqSocketRequest->send(message);
  
 
  zmq::message_t response;
  daqSocketRequest->recv(&response);
  delete zmq_req;
  //check to see that good message
  ZMQ::MessageHead MssgHead;
  memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
  uint8_t msg=MssgHead.mssg;
  if(msg!=ZMQ::MSGRECVDGOOD)
    {
      if(msg==ZMQ::MSGWRNGVERSION)
	{
	  fprintf(stderr,"Wrong ZMQ Version.  Please svn update RAT, and recompile RAT and CLEANViewer.  If this does not fix problem, please contact someone in charge of DCDAQ. \n");
	  CloseWindow();
	}
      else if(msg==ZMQ::MSGBADMAGIC)
	{
	  fprintf(stderr,"BAD Magic word sent to DCDAQ \n");
	  CloseWindow();
	}
      else
	{

	  fprintf(stderr,"ZMQ Messaging error \n");
	  CloseWindow();
	}
    }
  string endpoint=daqZMQName;
  size_t namePos=daqZMQName.find_last_of(":");
  endpoint.resize(namePos+1);
  zmqPort=MssgHead.port;
//memcpy(&zmqPort,(uint8_t *)response.data()+sizeof(ZMQ::MessageHead),sizeof(int32_t));
  // sprintf(endpoint,"%s%i",endpoint,zmqPort);
  stringstream ss;
  ss<<zmqPort;
  endpoint.append(ss.str());
  fprintf(stderr,"Got port %s, setting up now \n",endpoint.c_str());
  fprintf(stderr,"Port numebr is %i\n",zmqPort);
 
  daqSocketData=new zmq::socket_t(*context,ZMQ_PULL);
  daqSocketData->connect(endpoint.c_str());
 
 
  //setupcorrect filter
  SendFilterRequest(0);
  
  zmqFD=-1;
  size_t zmqfd_size=sizeof(zmqFD);
  daqSocketData->getsockopt(ZMQ_FD,&zmqFD,&zmqfd_size);
  zmqhandler=new TFileHandler(zmqFD,TFileHandler::kRead);
  zmqhandler->Connect("Notified()","CLEANViewer",this,"ZMQReadNewEvent()");
  zmqhandler->Add();
  ratDS=new RAT::DS::Root();
  return false;

}



uint32_t* CLEANViewer::ReadPastRunHeader(uint32_t * bufferPtr)
{
  uint32_t * returnAdr=bufferPtr;
  DCDAQEvent::Run *run = (DCDAQEvent::Run *) bufferPtr;
  //fprintf(stderr,"version %i\n",run->version);
  // if(run->version!=DCDAQEventVersion)
  //   {
  //     fprintf(stderr,"Wrong DCDAQEventVersion, data is version %i, I am version %i\n",run->version,DCDAQEventVersion);
  //     CloseWindow();
  //   }
  if(run->version==DCDAQEventVersion)
    StreamBufftoEV_func = &StreamBufftoEV;
  else if(run->version==2)
    StreamBufftoEV_func = &StreamBufftoEV_v2;
  else
    {
      //format not supported
      fprintf(stderr,"DCDAQEventVersion %i not supported\n",run->version);
      CloseWindow();
    }
  if(run->magic == DCDAQEvent::RUNSTART)
    {
      if(run->version==DCDAQEventVersion)
	{
	  DCDAQEvent::RunStart* runStart=(DCDAQEvent::RunStart*) bufferPtr;
	  returnAdr += int(ceil(sizeof(DCDAQEvent::RunStart)/sizeof(uint32_t)));
	  returnAdr += runStart->channelMapSize;
	  ratDS->SetRunID(runStart->runNumber);
	}
      else if(run->version==2)
	{
	  DCDAQEvent_v2::RunStart* runStart=(DCDAQEvent_v2::RunStart*) bufferPtr;
	  returnAdr += int(ceil(sizeof(DCDAQEvent_v2::RunStart)/sizeof(uint32_t)));
	  returnAdr += runStart->channelMapSize;
	  ratDS->SetRunID(runStart->runNumber);
	}
    }
  else if(run->magic ==DCDAQEvent::RUNCONTINUE)
    {

      returnAdr += int(ceil(sizeof(DCDAQEvent::RunContinue)/sizeof(uint32_t)));
      DCDAQEvent::RunContinue *runContinue=(DCDAQEvent::RunContinue*) bufferPtr;
      ratDS->SetRunID(runContinue->runNumber);
    }
  else
    {
      fprintf(stderr,"Error: MemBlock did not start with Run Header \n");
      CloseWindow();
    }

  return returnAdr;

}


int CLEANViewer::NumberEV(Bool_t Prev,bool First)
{

  
  if(DEBUG) fprintf(stderr,"CLEANViewer::NumberEV()\n"); 
  if(!DAQdat && !ZMQ)
    {
      nEV = 0;
  
      while(nEV == 0)
	{
	  //load ratDS for current selected event
	  std::cout << "Total Events: " << ratReader->GetTotal() << std::endl;
	  std::cout << "Current Event Entry: " << currentEvent << std::endl;

	  ratDS = ratReader->GetEvent(currentEvent);

	  std::cout << "EventLoaded" << std::endl; 
	  //get EV count from ratDS
	  nEV = ratDS->GetEVCount();
	  std::cout << "Total EV for event #" << currentEvent << ": " << nEV << std::endl;
	  if (nEV == 0)
	    {
	      if (currentEvent == 0)
		Prev = kFALSE;
	      if (currentEvent == nEvents)
		Prev = kTRUE;
	      EventCheck(Prev);
	    }
	}

  
      Int_t entry = 3000;
      //remove previous entries from EV Select Menu
      while (popupEV->GetEntry(entry) != 0)
	{
	  popupEV->DeleteEntry(entry);
	  entry++;
	}

      //fill EV Select Menu with new entries
      for (UInt_t iEV = 0; iEV < nEV; iEV++)
	{
	  char evTempBuff[100];
	  sprintf(evTempBuff,"EV# %i", iEV);
	  //ComboBox entries have entry ids 3000-3998
	  popupEV->AddEntry(evTempBuff, 3000+iEV);
	}
 
      popupEV->CheckEntry(3000);

      //first EV is always loaded initally
  
      
  
      ratDS->GetRunID();
    }
  if(RATServerMode)
    {
      ratSend->DSEvent(ratDS);

    }
  
  ratEV = ratDS->GetEV(0);
 
    
  if(!((LocalFilter->ReducLevels & (1<<ratEV->GetReductionLevel())) && (LocalFilter->HardwareTrig & ratEV->GetTriggerPattern()) && (LocalFilter->TotalQ<=ratEV->GetTotalQ())))
	{
	  return -1;
	}


  BuildMaps();
  LoadChannels();


  
 
  SetChannelLocations();
 
  
  CheckForEntries();
  DrawEventDisplay3D();
  //DrawHistogram();
  return 0;

}

void CLEANViewer::CheckForEntries()
{
 
  for(size_t i=0;i<pmtCheckInfoVector.size();i++)
    {
      int iWFD=pmtCheckInfoVector[i].Board;
      int iInput=pmtCheckInfoVector[i].Channel;
      
      char tempInputLabel[100];
      sprintf(tempInputLabel,"%i%i",iWFD,iInput);
      int PMTID=InputMap[tempInputLabel];

      char tempPMTLabel[25];
      char tempToolTipText[25];
      if(PMTID<0)
	{
	  //pmtCheckInfoVector[i].isDown=pmtCheckInfoVector[i].CheckButton->IsDown();
	  pmtCheckInfoVector[i].CheckButton->SetEnabled(kFALSE);
	  sprintf(tempToolTipText, "No PMT");
	  sprintf(tempPMTLabel, "No PMT");

	}
      else
	{
	  if(!pmtCheckInfoVector[i].CheckButton->IsEnabled())
	    {
	      pmtCheckInfoVector[i].CheckButton->SetEnabled(kTRUE);
	      //pmtCheckInfoVector[i].CheckButton->SetDown(pmtCheckInfoVector[i].isDown);
	    }
	  pmtCheckInfoVector[i].PMTID=PMTID;
	  sprintf(tempToolTipText,"PMT %i",PMTID);
	  sprintf(tempPMTLabel, "%i",PMTID);
	}

     
      //      fprintf(stderr,"i %i, PMTID %i %s %s\n",(int)i,PMTID,tempPMTLabel,tempToolTipText);
      
      pmtCheckInfoVector[i].CheckButton->SetToolTipText(tempToolTipText);
      
      pmtCheckInfoVector[i].CheckOrderButton->SetText(tempPMTLabel);
    }
}

void CLEANViewer::EventCheck(Bool_t Prev)
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::EventCheck()\n");
  if(!Prev)
    currentEvent++;
  if(Prev)
    currentEvent--;
  eventNumberEntry->SetNumber(currentEvent);
  if(eventNumberEntry3D)  eventNumberEntry3D->SetNumber(currentEvent);
  eventProgress->SetPosition(currentEvent);	 
  eventProgress->ShowPosition();
  if(currentEvent == 0)
    {
      prevEvent->SetEnabled(kFALSE);
      if(prevEvent3D)prevEvent3D->SetEnabled(kFALSE);
    }
  else
    {
      prevEvent->SetEnabled(kTRUE);
      if(prevEvent3D)prevEvent3D->SetEnabled(kTRUE);
    }
  if(currentEvent == nEvents)
    {
      nextEvent->SetEnabled(kFALSE);
      if(nextEvent3D) nextEvent3D->SetEnabled(kFALSE);
    }
  else
    {
      nextEvent->SetEnabled(kTRUE);
      if(nextEvent3D) nextEvent3D->SetEnabled(kTRUE);
    }
}

//function for loading all the pmts
void CLEANViewer::LoadChannels()
{

  commonScale=kTRUE;
  
  if(DEBUG) fprintf(stderr,"CLEANViewer::LoadChannels()\n"); 
  //initally disables all pmt checks before enabling those that are valid
 
  vector <uint16_t> rawADC;
  vector <double> QCalib;
  UInt_t sampleNumber = 0;  
  
  RAT::DS::Board * ratTempBoard = NULL;
  RAT::DS::Channel * ratTempChannel = NULL;
  RAT::DS::RawBlock * ratTempRawBlock = NULL;

  RAT::DS::PMT * ratTempPMT=NULL;
  RAT::DS::Block * ratTempBlock=NULL;
 
  //Clear the prompt/late histograms if they exist (if pervious event was drawPromptTotal)
  if(drawPromptTotal)
    {
      delete PromptHist;
      delete LateHist;
      if(calibrated)
	{
	  delete PromptCalibHist;
	  delete LateCalibHist;
	}
      if(PromptChannelsHist)
	{
	  delete PromptChannelsHist;
	  PromptChannelsHist=NULL;
	}
      if(LateChannelsHist)
	{
	  delete LateChannelsHist;
 	  LateChannelsHist=NULL;
	}
      if(calibrated)
	{
	  if(PromptCalibChannelsHist)
	    {
	      delete PromptCalibChannelsHist;
	      PromptCalibChannelsHist=NULL;
	    }
	  if(LateCalibChannelsHist)
	    {
	      delete LateCalibChannelsHist;
	      LateCalibChannelsHist=NULL;
	    }

	}

    }
      
  //Checking that reductionLevel is set correctly.  So far only checking for three options
  reductionLevel=ratEV->GetReductionLevel();
  
  drawPromptTotal=false;
  bool kRawBlocks=false;
  bool kSamples=false;

  if(ratEV->GetPMTCount()!=0) calibrated=true;
  for(int iBoard=0;(iBoard<ratEV->GetBoardCount() )& (!kRawBlocks);iBoard++)
    {
      for(int iChannel=0;(iChannel<ratEV->GetBoard(iBoard)->GetChannelCount() )& (!kRawBlocks);iChannel++)
	{
	  if(ratEV->GetBoard(iBoard)->GetChannel(iChannel)->GetRawBlockCount()!=0)
	    {
	      kRawBlocks=true;
	      if(ratEV->GetBoard(iBoard)->GetChannel(iChannel)->GetRawBlock(0)->GetSamples().size()!=0) kSamples=true;
	    }
	}
    }
  /*
  if(!kRawBlocks)
    {
      reductionLevelTrue=ReductionLevel::CHAN_PROMPT_TOTAL;
    }

  else
    {
      if(!kSamples)
	{
	  reductionLevelTrue=ReductionLevel::CHAN_ZLE_INTEGRAL;
	}

      else reductionLevelTrue=ReductionLevel::CHAN_ZLE_WAVEFORM;
    }

  if(reductionLevel!=reductionLevelTrue)
    {
      fprintf(stderr,"******************Reduction Level was set incorrectly. This could be a problem in RAT. Fixing Now *******************\n");
      fprintf(stderr,"prev RL %i , ev %i \n",reductionLevel,ratEV->GetEventID());
      reductionLevel=reductionLevelTrue;
      }*/



  if(reductionLevel!=ReductionLevel::CHAN_ZLE_WAVEFORM)
  {
      if(drawSeperator)
	{
	  drawSeperator=kFALSE;
	  popupView->UnCheckEntry(16);
	}
     popupView->DisableEntry(16);
  }
  else popupView->EnableEntry(16);
 
  if(reductionLevel==ReductionLevel::CHAN_PROMPT_TOTAL)
    {

 
      //Find the highest PMT ID
      Int_t nID = 0;

      for(Int_t iBoard = 0; iBoard < ratEV->GetBoardCount(); iBoard++)
	{
	  ratTempBoard = ratEV->GetBoard(iBoard);
	  nID+=ratTempBoard->GetChannelCount();
	}

     

      PromptHist = new TH1F("PromptHist","PromptHist",nID,0,nID);
      LateHist = new TH1F("LateHist","LateHist",nID,0,nID);
      //allocate *ChannelHist so safe to delete later
      PromptChannelsHist=new TH1F;
      LateChannelsHist=new TH1F;
      if(calibrated)
	{
	  PromptCalibHist = new TH1F("PromptCalibHist","PromptCalibHist",nID,0,nID);
	  LateCalibHist = new TH1F("LateCalibHist","LateCalibHist",nID,0,nID);
	  
	  PromptCalibChannelsHist=new TH1F;
	  LateCalibChannelsHist=new TH1F;

	}


      drawPromptTotal = true;
    }


  if(WaveformVector.size()!= 0)
    WaveformVector.clear();


 
  //Time zero (first board's time)
  UInt_t timeZero = 0xFFFFFFFF;


  for (Int_t iBoard = 0; iBoard < ratEV->GetBoardCount(); iBoard++)
    {
      ratTempBoard = ratEV->GetBoard(iBoard);
      UInt_t boardTimeZero = ((ratTempBoard->GetHeader()[3]&0x7FFFFFFF)) << 1;	
      if(boardTimeZero < timeZero)
	timeZero = boardTimeZero;
    }


  //getting boards/pmts with data
 
      if(reductionLevel==ReductionLevel::CHAN_PROMPT_TOTAL)
	{
	  double promptSum=0;
	  double lateSum=0;
	  double promptCalibSum=0;
	  double lateCalibSum=0;
	   for (Int_t iBoard = 0; iBoard < ratEV->GetBoardCount(); iBoard++)
	     {

     
	       ratTempBoard = ratEV->GetBoard(iBoard);
	       //UInt_t boardOffset = ((ratTempBoard->GetHeader()[3]&0x7FFFFFFF)) << 1;	
	       //boardOffset -= timeZero;

	       for(Int_t iChannel = 0;
		   iChannel < ratTempBoard->GetChannelCount();
		   iChannel++) 
		 {
		   ratTempChannel = ratTempBoard->GetChannel(iChannel);
		   UInt_t ChannelID = ratTempChannel->GetID();
		   double lateCharge = 
		     ratTempChannel->GetTotalQ() - ratTempChannel->GetPromptQ();
		  
		   PromptHist->SetBinContent(ChannelIDOrderMap[ChannelID],ratTempChannel->GetPromptQ());
		  
		   LateHist->SetBinContent(ChannelIDOrderMap[ChannelID],lateCharge);
		   promptSum+=PromptHist->GetBinContent(ChannelIDOrderMap[ChannelID]);
		   lateSum+=LateHist->GetBinContent(ChannelIDOrderMap[ChannelID]);
	      
  
		   if(calibrated)
		     {
		       
		       for(int iPMT=0;iPMT<ratEV->GetPMTCount();iPMT++)
			 {
	
			   if(ratEV->GetPMT(iPMT)->GetChannelID()==int(ChannelID))
			     {
			      
			       
			       PromptCalibHist->SetBinContent(ChannelIDOrderMap[ChannelID],ratEV->GetPMT(iPMT)->GetPromptQ());
			       double lateCalibCharge=ratEV->GetPMT(iPMT)->GetTotalQ()-ratEV->GetPMT(iPMT)->GetPromptQ();
			       LateCalibHist->SetBinContent(ChannelIDOrderMap[ChannelID],lateCalibCharge);
			       promptCalibSum+=PromptCalibHist->GetBinContent(ChannelIDOrderMap[ChannelID]);
			       lateCalibSum+=LateCalibHist->GetBinContent(ChannelIDOrderMap[ChannelID]);

			     }
			 }
		       
		     }
		   
		 }
	     }
	   PromptHist->SetBinContent(ChannelIDOrderMap[-100],promptSum);
	   LateHist->SetBinContent(ChannelIDOrderMap[-100],lateSum);
	   if(calibrated)
	     {
	       PromptCalibHist->SetBinContent(ChannelIDOrderMap[-100],promptCalibSum);
	       LateCalibHist->SetBinContent(ChannelIDOrderMap[-100],lateCalibSum);
	     }
	}

      else
	{
	  if(reductionLevel == ReductionLevel::CHAN_ZLE_WAVEFORM)
	    {

	      UInt_t minSampleNumber=999999;
	     
	    for (Int_t iBoard = 0; iBoard < ratEV->GetBoardCount(); iBoard++)
	      {
		
		ratTempBoard = ratEV->GetBoard(iBoard);
	
		//UInt_t boardOffset = ((ratTempBoard->GetHeader()[3]&0x7FFFFFFF)) << 1;	
		//boardOffset -= timeZero;
	
		//fprintf(stderr,"Board %i, offset %i\n",ratTempBoard->GetID(),boardOffset);
		for(Int_t iChannel = 0; 
		      iChannel < ratTempBoard->GetChannelCount();
		      iChannel++)
		    {
		      ratTempChannel = ratTempBoard->GetChannel(iChannel);
		      UInt_t ChannelID = ratTempChannel->GetID();
		      //      fprintf(stderr,"    ChannelID %i\n",ChannelID);
		      //enables pmts involved with collecting data
		      //	      fprintf(stderr,"ChannelID %i\n",(int)ChannelID);
		      drawFill = false;
		      //ZLE waveform format
		      
		      for (Int_t iRawBlock = 0;
			   iRawBlock < ratTempChannel->GetRawBlockCount();
			   iRawBlock++)
			{
			  sZLEwindow tempZLEWindow;
			  ratTempRawBlock = ratTempChannel->GetRawBlock(iRawBlock);
			  //sampleNumber = ratTempRawBlock->GetOffset()+boardOffset;
			  sampleNumber = ratTempRawBlock->GetOffset();

			  rawADC = ratTempRawBlock->GetSamples();
			  
			  
			  vector <uint16_t> SampleVector, CalibSampleVector;
			  for (UInt_t iSample = 0;
			       iSample < rawADC.size();
			       iSample++)
			    {
			      SampleVector.push_back(sampleNumber);
			      sampleNumber++;
			    }

			  tempZLEWindow.ADCs = rawADC;
			  tempZLEWindow.Samples = SampleVector;
			  rawADC.clear();
			  QCalib.clear();
			  //get all that data into a waveform for plotting
			  if (iRawBlock == 0)
			    {
			      Waveform tempWaveform(tempZLEWindow, 
						    DEBUG,
						    ChannelID);
			      WaveformVector.push_back(tempWaveform);
			    }
			  else
			    WaveformVector.back().AddZLE(tempZLEWindow);
			}
		    }
		  if(calibrated)
		    {
		     
		      for(size_t iChannel=0;iChannel<WaveformVector.size();iChannel++)
			{
			  uint32_t ChannelID=WaveformVector[iChannel].GetID();
			  
			  vector<sZLEwindow> windows=WaveformVector[iChannel].GetWindows();
			  for(int iPMT=0;iPMT<ratEV->GetPMTCount();iPMT++)
			    {
			      
			      if(ratEV->GetPMT(iPMT)->GetChannelID()==int(ChannelID))
				{
				  
				  
				  for(size_t iBlock=0;iBlock<(size_t)ratEV->GetPMT(iPMT)->GetBlockCount();iBlock++)
				    {
				      QCalib=ratEV->GetPMT(iPMT)->GetBlock(iBlock)->GetSamples();
				      sampleNumber=ratEV->GetPMT(iPMT)->GetBlock(iBlock)->GetOffset();
				      if(sampleNumber<minSampleNumber)
					{
					  minSampleNumber=sampleNumber;
					}
				      for(unsigned int iQCalib=0;iQCalib<QCalib.size();iQCalib++)
					QCalib[iQCalib]=-1000*QCalib[iQCalib];
				      
				      vector<uint16_t> CalibSamples;
				      if(iBlock<windows.size())
					{
					
					  sZLEwindow tempWindow=windows[iBlock];
					  tempWindow.QCalib=QCalib;
					  for(size_t iSample=0;iSample<QCalib.size();iSample++)
					    { //  fprintf(stderr,"Calib %f\n",QCalib[iSample]);
					      CalibSamples.push_back(sampleNumber);
					      sampleNumber++;
					    }
					  tempWindow.CalibSamples=CalibSamples;
					  WaveformVector[iChannel].InsertZLE(tempWindow,iBlock);
					  WaveformVector[iChannel].Process();
					  
					  
					}
				      else
					{
					  sZLEwindow tempWindow;
					  tempWindow.QCalib=QCalib;
					  for(size_t iSample=0;iSample<QCalib.size();iSample++)
					    {
					      CalibSamples.push_back(sampleNumber);
					      sampleNumber++;
					    }
					  tempWindow.QCalib=QCalib;
					  WaveformVector[iChannel].AddZLE(tempWindow);
					  WaveformVector[iChannel].Process();
					  
					}
				      
				      
				    }
				  break;
			      //				  else
			      //QCalib.clear();
				}
			    }
			  
			}
		    }
	      }
	    if(calibrated)
	      {
		//Add Sum Waveform;
		sZLEwindow tempWindow;
		vector<double> QCalib;
		      
		QCalib=ratEV->GetCalibratedSum();
		vector<uint16_t> CalibSamples,Samples;
		vector<uint16_t> ADC;
		sampleNumber=0;
		for(size_t i=0;i<QCalib.size();i++)
		  {
		    CalibSamples.push_back(sampleNumber);
		    Samples.push_back(sampleNumber);
		    ADC.push_back(3000);
		    QCalib[i]=-1000*QCalib[i];
		    sampleNumber++;
		  }
		tempWindow.QCalib=QCalib;
		tempWindow.CalibSamples=CalibSamples;
		tempWindow.ADCs=ADC;
		tempWindow.Samples=Samples;
		Waveform tempWaveform(tempWindow,DEBUG,-100);
		WaveformVector.push_back(tempWaveform);
		
	      }
	    }
	
	  else if(reductionLevel == ReductionLevel::CHAN_ZLE_INTEGRAL)
	    {
		  
		  drawFill = true;
		  //ZLE integral format
		  for (Int_t iBoard = 0; iBoard < ratEV->GetBoardCount(); iBoard++)
		    {
		      
		      ratTempBoard = ratEV->GetBoard(iBoard);
		      //UInt_t boardOffset = ((ratTempBoard->GetHeader()[3]&0x7FFFFFFF)) << 1;	
		      //boardOffset -= timeZero;
		      
		      for(Int_t iChannel = 0; 
			  iChannel < ratTempBoard->GetChannelCount();
			  iChannel++)
			{
			  ratTempChannel = ratTempBoard->GetChannel(iChannel);
			  UInt_t ChannelID = ratTempChannel->GetID();
			  
			  if(calibrated)
			    {
			      
			      for(int iPMT=0;iPMT<ratEV->GetPMTCount();iPMT++)
				{
				  if(ratEV->GetPMT(iPMT)->GetChannelID()==int(ChannelID))
				    {
				      ratTempPMT=ratEV->GetPMT(iPMT);
				    }
				}
			    }
			  
			  for (Int_t iRawBlock = 0;
			       iRawBlock < ratTempChannel->GetRawBlockCount();
			       iRawBlock++)
			    {
			      sZLEwindow tempZLEWindow;
			      ratTempRawBlock = ratTempChannel->GetRawBlock(iRawBlock);
			      //sampleNumber = ratTempRawBlock->GetOffset() + boardOffset;
			      sampleNumber = ratTempRawBlock->GetOffset();

			      ratTempBlock=NULL;
			     
			      if(calibrated and iRawBlock<ratTempPMT->GetBlockCount()) ratTempBlock=ratTempPMT->GetBlock(iRawBlock);
			     
			      uint16_t averageSample = (uint16_t)(4000+int(ratTempRawBlock->GetIntegral()/ratTempRawBlock->GetWidth()));
			
			      double averageSampleCalib=0;
			      if(ratTempBlock) 
				{
				  
				  averageSampleCalib=1000*ratTempBlock->GetIntegral()/ratTempBlock->GetWidth();
				 
				}
			      //			      if(ratTempRawBlock->GetIntegral()<0) averageSample=0;
			      
			      //			      fprintf(stderr,"averageSample %i\n",(int)averageSample);
			      
			      
			      vector<uint16_t> SampleVector, CalibSampleVector;
			      
			      rawADC.clear();
			      QCalib.clear();
			      for(UInt_t iSample = 0;
				  iSample < ratTempRawBlock->GetWidth();
				  iSample++)
				{
				  SampleVector.push_back(sampleNumber);
				  rawADC.push_back(averageSample);
				
				  sampleNumber++;
				}
			      sampleNumber--;
			      for(UInt_t iSample = 0; 
				  iSample< ratTempRawBlock->GetWidth();
				  iSample++)
				{
				  SampleVector.push_back(sampleNumber);
				  rawADC.push_back(4000);
				  sampleNumber--;

				}
			      sampleNumber++;
			      SampleVector.push_back(sampleNumber);
			      rawADC.push_back(averageSample);

			      if(ratTempBlock)
				{
				  sampleNumber = ratTempBlock->GetOffset();
				  for(UInt_t iSample = 0;
				      iSample < ratTempBlock->GetWidth();
				      iSample++)
				    {
				      CalibSampleVector.push_back(sampleNumber);
				      QCalib.push_back(averageSampleCalib);
				      sampleNumber++;
				    }
				  sampleNumber--;
				  for(UInt_t iSample = 0; 
				      iSample< ratTempBlock->GetWidth();
				      iSample++)
				    {
				      CalibSampleVector.push_back(sampleNumber);
				      sampleNumber--;
				      QCalib.push_back(0);
				    }
				  sampleNumber++;
				  CalibSampleVector.push_back(sampleNumber);
				  QCalib.push_back(averageSampleCalib);
				} 
			      
			      //			      fprintf(stderr,"iRawBloclk %i\n",iRawBlock);
			      tempZLEWindow.ADCs = rawADC;
			      tempZLEWindow.Samples = SampleVector;
			      tempZLEWindow.QCalib=QCalib;
			      tempZLEWindow.CalibSamples=CalibSampleVector;
			      //get all that data into a waveform for plotting
			      if (iRawBlock == 0)
				{
				  Waveform tempWaveform(tempZLEWindow, 
							DEBUG,
							ChannelID);
				 
				  WaveformVector.push_back(tempWaveform);
				}
			      else
				WaveformVector.back().AddZLE(tempZLEWindow); 
				}
			}
			
		      
		      
		    }
		}
	  
	  //	  if((reductionLevel==ReductionLevel::CHAN_ZLE_WAVEFORM)& (calibrated))
	    //SumWaveforms();
		  
	}
      
}

void CLEANViewer::SumWaveforms()
{
  int minSample=9999999;
  int maxSample=-9999999;

  for(UInt_t iWaveform=0;iWaveform<WaveformVector.size();iWaveform++)
    {
      if(WaveformVector[iWaveform].GetMinSample(true)<minSample)
	minSample=WaveformVector[iWaveform].GetMinSample(true);
      if(WaveformVector[iWaveform].GetMaxSample(true)>maxSample)
	 maxSample=WaveformVector[iWaveform].GetMaxSample(true);
    }



  vector<uint16_t> ADC;
  vector<double> QCalibVector;
  vector<uint16_t> SampleVector;
   for(int iSample=minSample;iSample<=maxSample;iSample++)
     {
       SampleVector.push_back(iSample);
       ADC.push_back(0);
       QCalibVector.push_back(0);

     }
  for(UInt_t iWaveform=0;iWaveform<WaveformVector.size();iWaveform++)
    {
 
     
      
      vector<sZLEwindow> TempZLEWindowVec=WaveformVector[iWaveform].GetWindows();
	
      for(UInt_t iZLE=0;iZLE<TempZLEWindowVec.size();iZLE++)
	{ 

	
	  for(size_t iSampleIndex=0;iSampleIndex<TempZLEWindowVec[iZLE].QCalib.size();iSampleIndex++)
	    {
	      QCalibVector[TempZLEWindowVec[iZLE].CalibSamples[iSampleIndex]-minSample]+=TempZLEWindowVec[iZLE].QCalib[iSampleIndex];
		  
	    }
  
	}
 
    
    }



  sZLEwindow tempZLEWindow;

  tempZLEWindow.Samples=SampleVector;
  tempZLEWindow.CalibSamples=SampleVector;
  tempZLEWindow.QCalib=QCalibVector;
  tempZLEWindow.ADCs=ADC;

  Waveform SumCalibWaveform(tempZLEWindow,DEBUG,-100);
  WaveformVector.push_back(SumCalibWaveform);
  

}


static  bool OrderBoards(RAT::DS::Board a,RAT::DS::Board b)
{
  return a.GetID()<b.GetID();

}

	
void CLEANViewer::BuildMaps()
{
  
  int PMTID;
  bool inputPluggedin;


  
  OrderedBoards.clear();
  InputMap.clear();
  for(int iBoard=0;iBoard<ratEV->GetBoardCount();iBoard++)
    {
  
      OrderedBoards.push_back(*ratEV->GetBoard(iBoard));
      
    }
  
  sort(OrderedBoards.begin(),OrderedBoards.end(),OrderBoards);
 

  int badLabel=-1;
 
  for(int iInput=0;iInput<8;iInput++)
    {
     
      for(size_t iBoard=0;iBoard<OrderedBoards.size();iBoard++)
	{
	 
	  inputPluggedin=false;
	  /*if(iBoard+iInput==0) BoardStart=ratEV->GetBoard(iBoard)->GetID();
	  if(iInput==0)
	    {
	      if(ratEV->GetBoard(iBoard)->GetID()<BoardStart) BoardStart=ratEV->GetBoard(iBoard)->GetID();
	      }*/

	  char tempInputStrLookingFor[25];
	  sprintf(tempInputStrLookingFor,"%i%i",OrderedBoards[iBoard].GetID(),iInput);
	 
	  // fprintf(stderr,"ChannelCount %i \n",OrderedBoards[iBoard].GetChannelCount());
	  for(int iChannel=0;iChannel<OrderedBoards[iBoard].GetChannelCount();iChannel++)
	    {
	      PMTID=OrderedBoards[iBoard].GetChannel(iChannel)->GetID();
	      if(iInput==0)
		{
		  // BoardMap[PMTID]=(int)iBoard;
		  //ChannelMap[PMTID]=iChannel;
		  if(iBoard+iChannel==0) PMTIDStart=PMTID;
		  else if(PMTID<PMTIDStart) PMTIDStart=PMTID;
		  
		  
		}


		
	      if(!inputPluggedin)
		{
		  char tempInputStr[25];
		  sprintf(tempInputStr,"%i%i",OrderedBoards[iBoard].GetID(),int(OrderedBoards[iBoard].GetChannel(iChannel)->GetInput()));
		  

		  
		  
		  if(iInput==OrderedBoards[iBoard].GetChannel(iChannel)->GetInput())
		    {
		      InputMap[tempInputStr]=PMTID;
		      inputPluggedin=true;
		  
		    }

		}
	  
			
	    }
	  
	  

	
      if(!inputPluggedin)
	    {
	      InputMap[tempInputStrLookingFor]=badLabel;
	      badLabel--;
	    }
	} 
    }

  int channelCounter=0;
  for(size_t iBoard=0;iBoard<OrderedBoards.size();iBoard++)
    {
      for(int iChannel=0;iChannel<OrderedBoards[iBoard].GetChannelCount();iChannel++)
	{
	  int ChannelID=OrderedBoards[iBoard].GetChannel(iChannel)->GetID();
	  ChannelIDOrderMap[ChannelID]=channelCounter;
	  channelCounter++;
	}
    }
  ChannelIDOrderMap[-100]=channelCounter;
	
}


void CLEANViewer::SetChannelLocations()
{
  RAT::DB *rDB=new RAT::DB();

  char dataArrays[100];
  char minicleanArrays[100];
  sprintf(minicleanArrays,"/MiniCLEAN/MiniCLEAN_CASSETTE_ARRAYS.ratdb");
  sprintf(dataArrays,getenv("GLG4DATA"));
  strcat(dataArrays,minicleanArrays);

  rDB->Load(dataArrays);
  RAT::DBLinkPtr lCas=rDB->GetLink("MiniCLEAN_CASSETTE_ARRAYS");
  xCas=lCas->GetFArrayFromD("x");
  yCas=lCas->GetFArrayFromD("y");
  zCas=lCas->GetFArrayFromD("z");




    for(int iPMT=0;iPMT<92;iPMT++)
      { 

	  
	ChannelLocsY.push_back(.8*acos(zCas[iPMT])/3.14159+.1);
	ChannelLocsYCentered.push_back(.8*acos(zCas[iPMT])/3.14159+.1);
	double sign=1;
	if(yCas[iPMT]<0) sign=-1;
	if((zCas[iPMT]<.999999999) & (zCas[iPMT]>-.999999999))
	  {
	    ChannelLocsX.push_back((sin(acos(zCas[iPMT]))*(sign*acos(xCas[iPMT]/sqrt(xCas[iPMT]*xCas[iPMT]+yCas[iPMT]*yCas[iPMT]))/3.14159)+1)*.4+.1);
	    ChannelLocsXCentered.push_back((sin(acos(zCas[iPMT]))*(sign*acos(xCas[iPMT]/sqrt(xCas[iPMT]*xCas[iPMT]+yCas[iPMT]*yCas[iPMT]))/3.14159)+1)*.4+.1);
	
	  }
	  else 
	    {
	      ChannelLocsX.push_back(.5);
	      ChannelLocsXCentered.push_back(.5);
	    }
      }
			        


}

void CLEANViewer::CreateCenteredArrays(int centeredPMT)
{
  ChannelLocsXCentered.clear();
  ChannelLocsYCentered.clear();

  double theta=acos(zCas[centeredPMT]);
  double phi;
  if((yCas[centeredPMT]<.000000001)&(yCas[centeredPMT]>-.000000001))
    {
      if(xCas[centeredPMT]<0)
	{
	  phi=3.14159;
	}
      else phi=0;
    }
  else
    {
      if(yCas[centeredPMT]>0) phi=acos(xCas[centeredPMT]/sin(theta));
      else phi=-acos(xCas[centeredPMT]/sin(theta));
    }


  double xCentered;
  double yCentered;
  double zCentered;
  for(int iPMT=0;iPMT<92;iPMT++)
    {
      xCentered=sin(theta)*cos(phi)*xCas[iPMT]+sin(theta)*sin(phi)*yCas[iPMT]+cos(theta)*zCas[iPMT];
      yCentered=-sin(phi)*xCas[iPMT]+cos(phi)*yCas[iPMT];
      zCentered=-cos(theta)*cos(phi)*xCas[iPMT]-cos(theta)*sin(phi)*yCas[iPMT]+sin(theta)*zCas[iPMT];

	ChannelLocsYCentered.push_back(.8*acos(zCentered)/3.14159+.1);
	double sign=1;
	if(yCentered<0) sign=-1;
	if((zCentered<.999999999) & (zCentered>-.999999999))
	  {
	    ChannelLocsXCentered.push_back((sin(acos(zCentered))*(sign*acos(xCentered/sqrt(xCentered*xCentered+yCentered*yCentered))/3.14159)+1)*.4+.1);
	
	  }
	  else ChannelLocsXCentered.push_back(.5);
  
      
    }

  
    


}
