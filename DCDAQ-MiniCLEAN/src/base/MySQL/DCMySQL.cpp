#include "DCMySQL.h"

// Constructor for DCMySQL
DCMySQL::DCMySQL()
{
  ConnectionCheck = NULL;
  result = NULL;
  isConnected = false;
}

// Destructor for DCMySQL
DCMySQL::~DCMySQL()
{
  if (ConnectionCheck == NULL)
    {
      if(result != NULL)
	mysql_free_result(result);
      mysql_close(&conn);
    }
}

void DCMySQL::CheckConnectionError()
{
  unsigned int error = mysql_errno(&conn);
  if((error == CR_SERVER_GONE_ERROR) ||
     (error == CR_SERVER_LOST))
    {
      std::cerr << "Trying to reconnect to server: " << server << std::endl;
      CloseConnect();
      OpenConnect();
    }
}

std::string DCMySQL::GetError()
{
  std::string returnString(mysql_error(&conn));
  return returnString;
}

// Individual functions for setting connection variables
void DCMySQL::SetServer(std::string mysql_server)
{
  server = mysql_server;
}

void DCMySQL::SetUser(std::string mysql_user)
{
  user = mysql_user;
}

void DCMySQL::SetPassword(std::string mysql_password)
{
  password = mysql_password;
}

void DCMySQL::SetDatabase(std::string mysql_database)
{
  database = mysql_database;
}

// Sets the server, user, password and database to connect with
void DCMySQL::SetConnect(std::string mysql_server, std::string mysql_user, 
			    std::string mysql_password, std::string mysql_database)
{ 
  
  SetServer(mysql_server);
  SetUser(mysql_user);
  SetPassword(mysql_password);
  SetDatabase(mysql_database);
}

// Connects to MySQL database with the server,user, password and database given by SetConnection
std::string DCMySQL::OpenConnect()
{
  isConnected = true;
  std::string returnString = "";
  mysql_init(&conn);
  ConnectionCheck = mysql_real_connect(&conn, server.c_str(), user.c_str(), 
				       password.c_str(), database.c_str(), 
				       0, NULL, 0);
  if (ConnectionCheck == NULL)
    {
      std::cout << "MySQL Connection error:\t";
      std::cout << mysql_error(&conn) << std::endl;
      returnString = mysql_error(&conn);
      isConnected = false;
    }
  return returnString;
}

// Closes the connection and frees the result pointer to th MySQL database
void DCMySQL::CloseConnect()
{
  mysql_free_result(result);
  mysql_close(&conn);
}	

// Run MySQL Query on database
bool DCMySQL::RunQuery(std::string mysql_query)
{
  if(result != NULL)
    {
      fprintf(stderr,"DCMySQL::RunQuery: result is not NULL, You forgot to Free!\n");
    }
  std::string returnString = "";
  query_length = mysql_query.size();
  query_state = mysql_real_query(&conn, mysql_query.c_str(), query_length);
  if (query_state != 0)
    {
      CheckConnectionError();
      //      std::cout << "MySQL Query error:\t";
      //      std::cout << mysql_error(&conn) << std::endl;
      //      std::cout << "In query: " << mysql_query << std::endl;
      return false;
    }
  else
    {
      result = mysql_store_result(&conn);
    }
  return true;
}

unsigned DCMySQL::GetNumFields()
{
  if(result != NULL)
    {
      return mysql_num_fields(result);
    }
  return 0;
}
unsigned DCMySQL::GetNumRows()
{  
  if(result != NULL)
    {
      return mysql_num_rows(result);
    }
  return 0;
}
void DCMySQL::FreeResult()
{
  if(result != NULL)
    mysql_free_result(result);
  result = NULL;
}

MYSQL_ROW DCMySQL::GetRow()
{
  if(result != NULL)
    return mysql_fetch_row(result);
  return NULL;
}



void DCMySQL::SetConnect(xmlNode * baseNode)
{
  //Get the MySQL server.
  if(FindSubNode(baseNode,"SERVER") == NULL)
    {
      //Default if we can't find value
      fprintf(stderr,"Could not find MySQL server value");
    }
  else
    GetXMLValue(baseNode,"SERVER","DCMySQL",server);
  
  //Get the MySQL server.
  if(FindSubNode(baseNode,"USER") == NULL)
    {
      //Default if we can't find value
      fprintf(stderr,"Could not find MySQL user value");
    }
  else
    GetXMLValue(baseNode,"USER","DCMySQL",user);

  //Get the MySQL server.
  if(FindSubNode(baseNode,"PASSWORD") == NULL)
    {
      //Default if we can't find value
      fprintf(stderr,"Could not find MySQL password value");
     }
  else
    GetXMLValue(baseNode,"PASSWORD","DCMySQL",password);

  //Get the MySQL server.
  if(FindSubNode(baseNode,"DATABASE") == NULL)
    {
      //Default if we can't find value
      fprintf(stderr,"Could not find MySQL database  value");
    }
  else
    GetXMLValue(baseNode,"DATABASE","DCMySQL",database);  
}


bool DCMySQL::GetTable(std::vector<std::vector<std::string> > &table)
{
  bool ret = false;
  if(result != NULL)
    {
      ret = true;
      table.clear();
      
      size_t numFields = mysql_num_fields(result);
      size_t numRows = mysql_num_rows(result);

      for(size_t iRow = 0; iRow < numRows; iRow++)
	{
	  MYSQL_ROW  row;
	  if((row = mysql_fetch_row(result)) != NULL)
	    {
	      std::vector<std::string> temp;
	      for(size_t iField = 0; iField < numFields; iField++)
		{
		  if(row[iField]!=NULL)
		    {
		      temp.push_back(row[iField]);
		    }
		  else
		    {
		      temp.push_back("NULL");
		    }
		}
	      table.push_back(temp);
	    }
	  else
	    {
	      ret = false;
	      break;
	    }
	}
    }
  return ret;
}

bool DCMySQL::GetSingleValue(std::string mysql_query,std::string & value)
{
  bool ret = true;
  if(!RunQuery(mysql_query))
    {
      return false;
    }

  int i=0;
  MYSQL_ROW row;
  while((row=mysql_fetch_row(result)))
    {
      if(row[0]!=NULL)
	{
	  value.clear();
	  value.assign(row[0]);
	  break;
	}
      i++;
    }
  
  if(i>1)
    {
      ret = false;
    }
  FreeResult();
  return ret;
}

bool DCMySQL::GetFieldNames(std::vector<std::string> &table_columns)
{
  bool ret = false;
  if(result != NULL)
    {
      size_t numFields=mysql_num_fields(result);
      MYSQL_FIELD * fields = mysql_fetch_fields(result);
      table_columns.clear();

      for(size_t iField=0;iField<numFields;iField++)
	{
	  table_columns.push_back(fields[iField].name);
	}
      ret = true;
    }
  return ret;
}


