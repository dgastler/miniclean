#include <MemBlockThrowAway.h>

MemBlockThrowAway::MemBlockThrowAway(std::string Name)  
{
  SetName(Name);
  SetType(std::string("MemBlockThrowAway"));
  BlocksRead = 0;
  TimeStep_BlocksRead= 0;
  last = time(NULL);
  current = time(NULL);
}

bool MemBlockThrowAway::Setup(xmlNode * SetupNode,
		      std::vector<MemoryManager*> &MemManager)
{
  //Find the memoryManager we need
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return false;
    }
  Ready = true;
    
  return(true);  
}

bool MemBlockThrowAway::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the Full queue's FD so we know when we have a new 
      //memory block to process
      AddReadFD(Mem->GetFullFDs()[0]); //Add the FD to this threads mask
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void MemBlockThrowAway::MainLoop()
{  
  //Get new memory block
  Mem->GetFull(blockIndex);
  if(blockIndex != -1)
    {
      //      block = Mem->Memory[blockIndex];
      
      //Pass off free memory block
      Mem->AddFree(blockIndex);
      //DO NOT USE "block" NOW!
      //Update status variables
      TimeStep_BlocksRead++;
    }
}


void MemBlockThrowAway::ProcessTimeout()
{
  current = time(NULL);
  if(current - last > GetUpdateTime())
    {
      BlocksRead += TimeStep_BlocksRead;

      TimeStep_BlocksRead = 0;
      //Setup next time
      last = current;
    }
}
