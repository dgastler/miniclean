#include <iostream>
#include <sstream>

#include <DCMySQL.h>

#include <boost/algorithm/string.hpp>

//return value gives if query worked
//Entry count returned via count
bool CheckRunEntryCount(DCMySQL * conn,
			const std::string & table,
			int run_number_source,
			int &count)
{
  std::stringstream query;
  query << "select count(*) from " << table << " where run_number=" << run_number_source;
  std::string response;
  if(!conn->GetSingleValue(query.str(),response))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  count = atoi(response.c_str());
  return true;
}

bool RemoveRunFromTable(DCMySQL * conn, 
			std::string table,
			int run_number)
{
  std::stringstream query;
  query << "delete from " << table << " where run_number=" << run_number;
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  query.str(std::string());
  return true;
}

bool CopyRunInTable(DCMySQL * conn, 
		    std::string table,
		    int run_number_source,
		    int run_number_destination)
{
  std::stringstream query;
  //Get columns
  query << "show columns in " << table << " where Field !='run_number'";
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  std::vector<std::vector<std::string> > fields;
  conn->GetTable(fields);
  conn->FreeResult();
  query.str(std::string());


  //BUild copy sql command

  //build the insert command
  query << "insert into " << table
	<< " (run_number";  // include run_number which will be "hard coded"
  //Add names of all other columns in the table
  for(size_t iField = 0; iField < fields.size();iField++)
    {
      query << "," <<  fields[iField][0];
    }
  query << ")";
  //Build select command that hard codes the destination run_number
  //and gets the data from the source run number
  query	<< " select " << run_number_destination;
  for(size_t iField = 0; iField < fields.size();iField++)
    {
      query << "," <<  fields[iField][0];
    }
  query << " from " << table << " where run_number=" << run_number_source;
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  conn->FreeResult();
  query.str(std::string());
  return true;
}





int main(int argc, char ** argv)
{
  int run_number_source;
  int run_number_destination;
  //list of tables
  std::vector<std::string> Tables; 
  Tables.push_back("SThread_Data_Compressed");
  Tables.push_back("NetworkConnections");
  Tables.push_back("SThread_DecisionMaker");
  Tables.push_back("Settings_DataReduction");
  Tables.push_back("Memory_MemoryManagers");
  Tables.push_back("Messages_BeforeThreads");
  Tables.push_back("Messages_End");
  Tables.push_back("Messages_Start");
  Tables.push_back("Messages_Shutdown");
  Tables.push_back("Settings_PMTs");
  Tables.push_back("SThread_Data_Raw");
  Tables.push_back("Thread_Threads");
  Tables.push_back("Settings_WFDs");


  //========================================================
  //parse CLI arguments
  //========================================================
  if(argc < 3)
    {
      std::cout << "Usage: " << argv[0] 
		<< "source_run_number destination_run_number"
		<< std::endl;
      return 1;
    }
  else
    {
      run_number_source = atoi(argv[1]);
      run_number_destination = atoi(argv[2]);
    }

  //========================================================
  //Connect to sql database
  //========================================================
  DCMySQL conn;
  conn.SetConnect("192.168.22.6","control_user","27K2boil","daq_interface");
  conn.OpenConnect();


  //========================================================
  //Check that the source exists
  //========================================================
  bool empty_run = true;
  for(size_t iTable = 0; iTable < Tables.size();iTable++)
    {
      int count = -1;
      //if there are any errors, consider the run empty and end
      if(!CheckRunEntryCount(&conn,Tables[iTable],run_number_source,count))
	{
	  std::cout << "Failed due to errors" << std::endl;
	  empty_run = true;
	  break;
	}        
      if(count > 0)
	{
	  empty_run = false;
	  break;
	}
    }
  if(empty_run)
    {
      std::cout << "Error: Empty source run number" << std::endl;
      return 1;
    }

  //========================================================
  //check for overwrites
  //========================================================
  empty_run = true;
  for(size_t iTable = 0; iTable < Tables.size();iTable++)
    {
      int count = -1;
      if(!CheckRunEntryCount(&conn,Tables[iTable],run_number_destination,count))
	{
	  std::cout << "Failed due to errors" << std::endl;
	  empty_run = false;
	  break;
	}
      if((count > 0))
	{
	  empty_run = false;
	  break;
	}
    }
  if(!empty_run)
    {
      //warning!
      std::cout << "Warning: Data exists for run number " << run_number_destination << std::endl;
      std::cout << "Are you sure you want to overwrite run number " << run_number_destination 
		<< "(y/n)?" << std::endl;
      std::string response;
      std::cin >> response;
      if(!boost::algorithm::iequals(response,"y"))
	{
	  return 0;
	}
    }

  if(run_number_destination > 0)
    {
      std::cout<< "I will not allow you to copy to a positive run number.  Sorry." << std::endl;
      return 0;
    }


  //========================================================
  //copy the run
  //========================================================
  bool error = false;
  for(size_t iTable = 0; 
      ((iTable < Tables.size())&&(!error)); //stop if we have an error
      iTable++)
    {
      if(RemoveRunFromTable(&conn, 
			    Tables[iTable],
			    run_number_destination))
	{
	  if(!CopyRunInTable(&conn, 
			     Tables[iTable],
			     run_number_source,
			     run_number_destination))
	    {
	      error = true;	      
	    }
	}
      else
	{
	  error = true;	  
	}
    }

  //========================================================
  //if there was an error, delete destination run number entries
  //========================================================
  if(error)
    {
      for(size_t iTable = 0;iTable < Tables.size();iTable++)
	{
	  RemoveRunFromTable(&conn, 
			     Tables[iTable],
			     run_number_destination);
	}
    }
  return 0;
}
