#ifdef __RATDS__
#include <RATWriter.h>

void RATWriter::MainLoop()
{
  if(IsReadReady(EBPC_mem->GetToDiskFDs()[0]))
    { 
      //Get the complete event's index
      int32_t index;
      if(EBPC_mem->GetToDisk(index))
	//We got an index from the 
	{
	  if(index != EBPC_mem->GetBadValue())
	    //A valid event
	    {
	      //BranchDS = new RAT::DS::Root;
	      //Check if we have a file opened.  If we don't open one.
	      // fprintf(stderr,"Setting BranchDS to null");
	      BranchDS = NULL;

	      RATDSBlock * event = EBPC_mem->GetRATDiskBlock(index);
	      
	      //If the ds is null, we are done with all events and we should shut down.
	      if(event->ds == NULL)
		{
		  CloseFile();
//		  //Write an empty file to signal succesful completion
//		  char * buffer = new char[100];
//		  sprintf(buffer,"%scomplete.dat",outputDirectory.c_str()); 
//		  FILE * endfile = fopen(buffer,"w");
//		  fclose(endfile);
//		  SendStop(false);
		  Print("Ending Run");
		  SendStop(false);
		  return;
		}

	      uint32_t runNumber = event->ds->GetRunID();

	      //If we don't have an OutFile, open one named with the correct run number.
	      if(!OutFile)
		{		      
		  fileReady = OpenFile(runNumber);
		}
	      if(fileReady)
		{	      
		  //Make pointer to ds for filling tree
		  BranchDS = event->ds;
		  event->ds->GetEV(0)->SetEventID(EventNumber);
		  //		  RAT::DS::TriggerSummary ts = event->ds->GetEV(0)->GetTriggerSummary();
		  //		  fprintf(stderr,"Time: %ld.%ld\n",ts.utc.GetTimeSpec().tv_sec,ts.utc.GetTimeSpec().tv_nsec);
		 
		  //Fill the TFile tree with the contents of BranchDS
		  Tree->Fill(); 
		  BranchDS = NULL;

		  EventNumber++;  //Increment the event number
		  timeStepProcessedEvents++;  //Increment the time step event count
		  //add index to the free queue
		  if(!EBPC_mem->AddFree(index))
		    {
		      PrintError("Lost a EVBlock\n");
		    }
		  
		  
		  //Check if we have gone over the maximum number of events
		  if((MaxRunEvents != 0) && (EventNumber > MaxRunEvents))
		    {			  
		      printf("Max run events reached\n");		      
		      SendStop(false);
		    }
		  else
		    {
		      SubEventNumber++;
		      //Handle subrun changes
		      if(SubEventNumber > SubRunMaxEvents)
			{
			  CloseFile();
			  SubRunNumber++;
			}		  
		    }
		}//file is open
	      else
		{
		  PrintError("File not ready for write!");
		}
	    
	    }
	  else
	    {
	      //We are probably shutting down now. 
	      PrintError("Bad event index\n");
	    }
	}
      else
	//We couldn't get a new ToDisk
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a ToDisk event.  DCDAQ shutdown?\n");
	}
    }
}


bool RATWriter::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      EBPC_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(EBPC_mem->GetToDiskFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the to disk queue with select
      AddReadFD(EBPC_mem->GetToDiskFDs()[0]);      
      SetupSelect();
      Loop = true;
    }
  else if(message.GetType() == DCMessage::RUN_INFO)
    {
      //Get the run information and fill a RunInfo struct
      // std::vector<uint8_t> headerVector = message.GetData();
      // RunInfo::sRunInfo_001 runHeader;
      // runHeader.ReadData(&(headerVector[0]));

      // //Set the values to be written in the output file
      // runType = runHeader.run_type;
      // runTime = runHeader.run_time;
      // runPresamples = runHeader.run_presamples;
      // runStartPrompt = runHeader.run_start_prompt;
      // runEndPrompt = runHeader.run_end_prompt;
      // runText = runHeader.run_text;
      // runXML = runHeader.run_xml;

    }
  else
    {
      ret = false;
    }						
  return(ret);
}


void RATWriter::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t timeStep = difftime(currentTime,lastTime);      
      DCMessage::Message message;
      StatMessage::StatRate ssRate;

      ssRate.sBase.type = StatMessage::EVENT_RATE;
      ssRate.count = timeStepProcessedEvents;
      ssRate.sInfo.time = currentTime;
      ssRate.sInfo.interval = timeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.units = Units::NUMBER;   
      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);
      
      timeStepProcessedEvents = 0;

      //Setup next time
      lastTime = currentTime;
    }
}

#endif
