--Boston University Electronics Design Facility--Conor DuBois.
--Project derived from work of Xilinx and Jeremy Mans.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sub_top is Port(
	CLK125 : in STD_LOGIC;
	CLK125_RX : in STD_LOGIC;
	PHY_RESET_L : out STD_LOGIC;
	PHY_MDC : out STD_LOGIC;
	PHY_MDIO : inout STD_LOGIC;
	PHY_RXCTRL_RXDV : in STD_LOGIC;
	PHY_RXD : in STD_LOGIC_VECTOR(7 downto 0);
	PHY_TXC_GTXCLK : out STD_LOGIC;
	PHY_TXCTL_TXEN : out STD_LOGIC;
	PHY_TXD : out STD_LOGIC_VECTOR(7 downto 0);
	PHY_TXCLK : in STD_LOGIC;
	myMAC : STD_LOGIC_VECTOR(47 downto 0);
	myIP : STD_LOGIC_VECTOR(31 downto 0);
	udpPort : STD_LOGIC_VECTOR(15 downto 0);
	DATA_I : in unsigned(31 downto 0);
	DATA_O : out unsigned(31 downto 0);
	STROBE : out STD_LOGIC;
	WRITING : out STD_LOGIC;
	DTACK : in STD_LOGIC;
	BERR : in STD_LOGIC;
	ADDR : out unsigned(31 downto 0);
	RESET : in STD_LOGIC);
end sub_top;

architecture Behavioral of sub_top is
	signal mac_txdv : STD_LOGIC;
	signal mac_rxdv : STD_LOGIC;
	signal mac_cke : STD_LOGIC;
	signal mac_txd : STD_LOGIC_VECTOR(7 downto 0);
	signal mac_rxd : STD_LOGIC_VECTOR(7 downto 0);
	signal packet_rxd : STD_LOGIC_VECTOR(7 downto 0);
	signal packet_rxw : unsigned(31 downto 0);
	signal packet_rxa : STD_LOGIC_VECTOR(10 downto 0);
	signal packet_rxready : STD_LOGIC;
	signal packet_rxdone : STD_LOGIC;
	signal arp_rxa : STD_LOGIC_VECTOR(5 downto 0);
	signal arp_txd : STD_LOGIC_VECTOR(7 downto 0);
	signal arp_xmit : STD_LOGIC;
	signal arp_done : STD_LOGIC;
	signal arp_ready : STD_LOGIC;
	signal icmp_rxa : STD_LOGIC_VECTOR(9 downto 0);
	signal icmp_txd : STD_LOGIC_VECTOR(7 downto 0);
	signal icmp_xmit : STD_LOGIC;
	signal icmp_done : STD_LOGIC;
	signal icmp_ready : STD_LOGIC;
	signal udp_size : STD_LOGIC_VECTOR(15 downto 0);
	signal udp_rxa : STD_LOGIC_VECTOR(10 downto 0);
	signal udp_txd : STD_LOGIC_VECTOR(7 downto 0);
	signal udp_xmit : STD_LOGIC;
	signal udp_done : STD_LOGIC;
	signal udp_ready : STD_LOGIC;
	signal udp_xmit_req : STD_LOGIC;
	signal udp_xmit_ok : STD_LOGIC;
	signal packet_req_addr : unsigned(8 downto 0);
	signal packet_req_len : unsigned(8 downto 0);
	signal packet_resp_addr : unsigned(8 downto 0);
	signal packet_resp_len : unsigned(8 downto 0);
	signal packet_resp_data : unsigned(31 downto 0);
	signal resp_we : STD_LOGIC;
	signal req_avail : STD_LOGIC;
	signal resp_done : STD_LOGIC_VECTOR(1 downto 0);
	signal goodbad_frame : STD_LOGIC_VECTOR(1 downto 0);
	signal send_cke : STD_LOGIC;
	signal special_cke : STD_LOGIC;
	signal early_warning : STD_LOGIC_VECTOR(1 downto 0);
	signal fragment_count : unsigned(10 downto 0);
	signal link_speed : STD_LOGIC;

	component gbe_rxpacketbuffer port(
		MAC_RXD : in STD_LOGIC_VECTOR(7 downto 0);
		MAC_RXDV : in STD_LOGIC;
		MAC_CKE : in STD_LOGIC;
		SEND_CKE : in STD_LOGIC;
		RESET : in STD_LOGIC;
		MAC_RXPACKETOK : in STD_LOGIC;
		MAC_RXPACKETBAD : in STD_LOGIC;
		MAC_CLK : in STD_LOGIC;
		PACKET_RXD : out STD_LOGIC_VECTOR(7 downto 0);
		PACKET_RXW : out unsigned(31 downto 0);
		PACKET_RXA : in unsigned(10 downto 0);
		PACKET_RXREADY : out STD_LOGIC;
		PACKET_RXDONE : in STD_LOGIC);
	end component;

	component packet_handler port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		RX_READY : in STD_LOGIC;
		RX_DONE : out STD_LOGIC;
		RXD : in STD_LOGIC_VECTOR(7 downto 0);	 
		RXA : out STD_LOGIC_VECTOR(10 downto 0);
		TXD : out STD_LOGIC_VECTOR(7 downto 0);
		TXMIT : out STD_LOGIC;
		IP : in STD_LOGIC_VECTOR(31 downto 0);
		ARP_RXA : in STD_LOGIC_VECTOR(5 downto 0);
		ARP_TXD : in STD_LOGIC_VECTOR(7 downto 0);
		ARP_XMIT : in STD_LOGIC;
		ARP_DONE : in STD_LOGIC;
		ARP_READY : out STD_LOGIC;
		ICMP_RXA : in STD_LOGIC_VECTOR(9 downto 0);
		ICMP_TXD : in STD_LOGIC_VECTOR(7 downto 0);
		ICMP_XMIT : in STD_LOGIC;
		ICMP_DONE : in STD_LOGIC;
		ICMP_READY : out STD_LOGIC;
		UDP_PORT_CTL : in STD_LOGIC_VECTOR(15 downto 0);
		UDP_SIZE : out STD_LOGIC_VECTOR(15 downto 0);
		UDP_RXA : in STD_LOGIC_VECTOR(10 downto 0);
		UDP_TXD : in STD_LOGIC_VECTOR(7 downto 0);
		UDP_XMIT : in STD_LOGIC;
		UDP_DONE : in STD_LOGIC;
		UDP_XMIT_REQ : in STD_LOGIC;
		UDP_READY : out STD_LOGIC; 
		UDP_XMIT_OK : out STD_LOGIC);
	end component;

	component arp port(
		MAC_CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		SEND_CKE : in STD_LOGIC;
		PACKET_READY : in STD_LOGIC;
		DONE_WITH_PACKET : out STD_LOGIC;
		PACKET_DATA : in STD_LOGIC_VECTOR(7 downto 0);
		PACKET_READ_ADDR : out unsigned(5 downto 0);
		MYMAC : in STD_LOGIC_VECTOR(47 downto 0);
		MYIP : in STD_LOGIC_VECTOR(31 downto 0);
		PACKET_OUT : out STD_LOGIC_VECTOR(7 downto 0);
		EARLY_WARNING : out STD_LOGIC;
		PACKET_XMIT : out STD_LOGIC);
	end component;

	component icmp port(
		MAC_CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		SEND_CKE : in STD_LOGIC;
		PACKET_READY : in STD_LOGIC;
		DONE_WITH_PACKET : out STD_LOGIC;
		PACKET_DATA : in unsigned(7 downto 0);
		PACKET_READ_ADDR : out unsigned(9 downto 0);
		PACKET_OUT : out unsigned(7 downto 0);
		EARLY_WARNING : out STD_LOGIC;
		PACKET_XMIT : out STD_LOGIC);
	end component;

	component sub_packetbuffer port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		SEND_CKE : in STD_LOGIC;
		LINK_SPEED : in STD_LOGIC;
		INCOMING_READY : in STD_LOGIC;
		DONE_WITH_INCOMING : out STD_LOGIC;
		RXA : out unsigned(10 downto 0);
		RXW : in unsigned(31 downto 0);
		TXD : out unsigned(7 downto 0);
		TX_REQ : out STD_LOGIC;
		TX_SEND : out STD_LOGIC;
		TX_OK : in STD_LOGIC;
		FRAGMENT_COUNT : in unsigned(10 downto 0);
		UDP_SIZE : in unsigned(15 downto 0);
		REQ_ADDR : in unsigned(8 downto 0);
		REQ_LEN : out unsigned(8 downto 0);
		RESP_ADDR : in unsigned(8 downto 0);
		RESP_DATA : in unsigned(31 downto 0);
		RESP_LEN : in unsigned(8 downto 0);
		RESP_WE : in STD_LOGIC;
		REQ_AVAIL : out STD_LOGIC;
		RESP_DONE : in STD_LOGIC_VECTOR(1 downto 0));
	end component;

	component sub_transactor port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		BUS_DATA_I : in unsigned(31 downto 0);
		BUS_DATA_O : out unsigned(31 downto 0);
		BUS_ADDR : out unsigned(31 downto 0);
		BUS_WRITE : out STD_LOGIC;
		BUS_STROBE : out STD_LOGIC;
		BUS_DTACK : in STD_LOGIC;
		BUS_BERR : in STD_LOGIC;
		PACKET_DATA_I : in unsigned(31 downto 0);
		PACKET_LEN_I : in unsigned(8 downto 0);
		PACKET_ADDR_I : out unsigned(8 downto 0);
		PACKET_DATA_O : out unsigned(31 downto 0);
		PACKET_ADDR_O : out unsigned(8 downto 0);
		PACKET_LEN_O : out unsigned(8 downto 0);
		PACKET_WE_O : out STD_LOGIC;
		NEW_PACKET : in STD_LOGIC;
		FRAGMENT_COUNT : out unsigned(10 downto 0);
		DONE : out STD_LOGIC_VECTOR(1 downto 0));
	end component;
	
	component ge_mac_stream port(
		PHY_RESET_L : out STD_LOGIC;
		PHY_MDC : out STD_LOGIC;
		PHY_MDIO : inout STD_LOGIC;
		GE_RXCLK : in STD_LOGIC;
		GE_RXDV : in STD_LOGIC;
		GE_RXD : in STD_LOGIC_VECTOR(7 downto 0);
		GE_TXCLK : out STD_LOGIC;
		GE_TXEN : out STD_LOGIC;
		GE_TXD : out STD_LOGIC_VECTOR(7 downto 0);
		FE_TXCLK : in STD_LOGIC;
		CLK125 : in STD_LOGIC;
		RESET : in STD_LOGIC;
		GOODBAD_FRAME : out STD_LOGIC_VECTOR(1 downto 0);
		ETH_RX_STREAM : out STD_LOGIC_VECTOR(9 downto 0);
		TX_EN : out STD_LOGIC;
		LINK_SPEED : out STD_LOGIC;
		ETH_TX_STREAM : in STD_LOGIC_VECTOR(8 downto 0));
	end component;
begin
	special_cke <= (not (early_warning(1) or early_warning(0))) or send_cke;

	rxbuffer : gbe_rxpacketbuffer port map(
		MAC_RXD => mac_rxd,
		MAC_RXDV => mac_rxdv,
		MAC_CKE => mac_cke,
		SEND_CKE => special_cke,
		RESET => RESET,
		MAC_RXPACKETOK => goodbad_frame(1),
		MAC_RXPACKETBAD => goodbad_frame(0),
		MAC_CLK => CLK125,
		PACKET_RXD => packet_rxd,
		PACKET_RXW => packet_rxw,
		PACKET_RXA => unsigned(packet_rxa),
		PACKET_RXREADY => packet_rxready,
		PACKET_RXDONE => packet_rxdone);

	thePacketDistributor: packet_handler port map(
		CLK => CLK125,
		RESET => RESET,
		RX_READY => packet_rxready,
		RX_DONE => packet_rxdone,
		RXD => packet_rxd, 
		RXA => packet_rxa,
		TXD => mac_txd,
		TXMIT => mac_txdv,
		IP => myIP,
		ARP_RXA => arp_rxa,
		ARP_TXD => arp_txd,
		ARP_XMIT => arp_xmit,
		ARP_DONE => arp_done,
		ARP_READY => arp_ready,
		ICMP_RXA => icmp_rxa,
		ICMP_TXD => icmp_txd,
		ICMP_XMIT => icmp_xmit,
		ICMP_DONE => icmp_done,
		ICMP_READY => icmp_ready,
		UDP_PORT_CTL => udpPort,
		UDP_SIZE => udp_size,
		UDP_RXA => udp_rxa,
		UDP_TXD => udp_txd,
		UDP_XMIT => udp_xmit,
		UDP_DONE => udp_done,
		UDP_XMIT_REQ => udp_xmit_req,
		UDP_READY => udp_ready,
		UDP_XMIT_OK => udp_xmit_ok);

	theArp: arp port map(
		MAC_CLK => CLK125,
		RESET => RESET,
		SEND_CKE => send_cke,
		PACKET_READY => arp_ready,
		DONE_WITH_PACKET => arp_done,
		PACKET_DATA => packet_rxd,
		STD_LOGIC_VECTOR(PACKET_READ_ADDR) => arp_rxa,
		MYMAC => myMAC,
		MYIP => myIP,
		PACKET_OUT => arp_txd,
		EARLY_WARNING => early_warning(0),
		PACKET_XMIT => arp_xmit);

	theICMP : icmp port map(
		MAC_CLK => CLK125,
		RESET => RESET,
		SEND_CKE => send_cke,
		PACKET_READY => icmp_ready,
		DONE_WITH_PACKET => icmp_done,
		PACKET_DATA => unsigned(packet_rxd),
		STD_LOGIC_VECTOR(PACKET_READ_ADDR) => icmp_rxa,
		STD_LOGIC_VECTOR(PACKET_OUT) => icmp_txd,
		EARLY_WARNING => early_warning(1),
		PACKET_XMIT => icmp_xmit);

	theCtlBuffer: sub_packetbuffer port map(
		CLK => CLK125,
		RESET => RESET,
		SEND_CKE => send_cke,
		LINK_SPEED => link_speed,
		INCOMING_READY => udp_ready,
		DONE_WITH_INCOMING => udp_done,
		STD_LOGIC_VECTOR(RXA) => udp_rxa,
		RXW => packet_rxw,
		STD_LOGIC_VECTOR(TXD) => udp_txd,
		TX_REQ => udp_xmit_req,
		TX_SEND => udp_xmit,
		TX_OK => udp_xmit_ok,
		FRAGMENT_COUNT => fragment_count,
		UDP_SIZE => unsigned(udp_size),
		REQ_ADDR => packet_req_addr,
		REQ_LEN => packet_req_len,
		RESP_ADDR => packet_resp_addr,
		RESP_DATA => packet_resp_data,
		RESP_LEN => packet_resp_len,
		RESP_WE => resp_we,
		REQ_AVAIL => req_avail,
		RESP_DONE => resp_done);

	theTransactor : sub_transactor port map(
		CLK => CLK125,
		RESET => RESET,
		BUS_DATA_I => DATA_I,
		BUS_DATA_O => DATA_O,
		BUS_ADDR => ADDR,
		BUS_WRITE => WRITING,
		BUS_STROBE => STROBE,
		BUS_DTACK => DTACK,
		BUS_BERR => BERR,
		PACKET_DATA_I => packet_rxw,
		PACKET_LEN_I => packet_req_len,
		PACKET_ADDR_I => packet_req_addr,
		PACKET_DATA_O => packet_resp_data,
		PACKET_ADDR_O => packet_resp_addr,
		PACKET_LEN_O => packet_resp_len,
		PACKET_WE_O => resp_we,
		NEW_PACKET => req_avail,
		FRAGMENT_COUNT => fragment_count,
		DONE => resp_done);

	theMAC : ge_mac_stream port map(
		PHY_RESET_L => PHY_RESET_L,
		PHY_MDC => PHY_MDC,
		PHY_MDIO => PHY_MDIO,
		GE_RXCLK => CLK125_RX,
		GE_RXDV => PHY_RXCTRL_RXDV,
		GE_RXD => PHY_RXD,
		GE_TXCLK => PHY_TXC_GTXCLK,
		GE_TXEN => PHY_TXCTL_TXEN,
		GE_TXD => PHY_TXD,
		FE_TXCLK => PHY_TXCLK,
		CLK125 => CLK125,
		RESET => RESET,
		GOODBAD_FRAME => goodbad_frame,
		ETH_RX_STREAM(9) => mac_cke,
		ETH_RX_STREAM(8) => mac_rxdv,
		ETH_RX_STREAM(7 downto 0) => mac_rxd,
		TX_EN => send_cke,
		LINK_SPEED => link_speed,
		ETH_TX_STREAM(8) => mac_txdv,
		ETH_TX_STREAM(7 downto 0) => mac_txd);
end Behavioral;
