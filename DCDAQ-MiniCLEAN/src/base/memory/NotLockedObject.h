#ifndef __NOTLOCKEDOBJECT__
#define __NOTLOCKEDOBJECT__

#include <stdint.h>

class NotLockedObject
{
 public:
  NotLockedObject(){};
  virtual ~NotLockedObject(){};

  virtual void     ShutdownLockWait(){Shutdown = true;};
  virtual void     WakeUpSignal(){};  

  bool LockObject(bool /*Wait*/ = true){return true;};
  bool ConditionWait(){return !GetShutdown();};
  bool UnlockObject(){return true;}
  const bool    GetShutdown(){return Shutdown;};
  
 private:
  volatile bool Shutdown;  



  //================================
  //Disallow coping of NotLockedObjects
  //================================
  NotLockedObject(const NotLockedObject&);              //Never implement these functions
  NotLockedObject& operator=(const NotLockedObject&);   //Never implement these functions
};

#endif
