#ifdef __CAENLIBS__
#include <v1720FakeTrigReadout.h>

bool v1720FakeTrigReadout::Setup(xmlNode * SetupNode,
			 std::vector<MemoryManager*> &MemManager)
{
  //=================================================
  //Find the MemoryManager we need
  //=================================================
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      PrintError("Error on memory manager\n");
      return false;
    }

  //Setup tempBlock
  tempBlock.allocatedSize = 0x40000;
  tempBlock.buffer = new uint32_t[tempBlock.allocatedSize]; //figure out from setup
  tempBlock.dataSize = 0;

  //=================================================
  //Find readout settings
  //=================================================

  //Find max readouts(CAEN events) into a block before we pass it off
  if(FindSubNode(SetupNode,"MaxReadoutsPerBlock") == NULL)
    {
      MaxReadoutsPerBlock = 100;
    }
  else
    {
      GetXMLValue(SetupNode,"MaxReadoutsPerBlock","THREAD",MaxReadoutsPerBlock);
    }
  
  xmlNode * WFDsNode = FindSubNode(SetupNode,"WFDS");
  if(WFDsNode == NULL)
    {
      fprintf(stderr,"No WFD setup XMLs\n");
      return(false);
    }
  else
    {
      MinimumFreeBufferSpace = 0;
      uint32_t nWFDs = NumberOfNamedSubNodes(WFDsNode,"WFD");
      for(unsigned int iWFD = 0; iWFD < nWFDs;iWFD++)
	{
	  xmlNode * tempNode = FindIthSubNode(WFDsNode,"WFD",iWFD);
	  BaseXMLNodes.push_back(tempNode);
	  WFD.push_back(new v1720());
	  if(WFD.back()->LoadXMLConfig(BaseXMLNodes.back()) != 0)
	    {
	      PrintError("Error in XML configuration\n");
	      return false;
	    }
	  
	  TimeStep_EventCount.push_back(0);
	  
	  TimeStep_ReadoutNumber.push_back(0);
	  
	  TimeStep_ReadoutSize.push_back(0);  

	  TimeStep_Occupancy.push_back(0);

	  MinimumFreeBufferSpace += WFD.back()->GetEventSize();
	}
    }

  //Find out what offset we should add when we loop
  if(FindSubNode(SetupNode,"LOOPOFFSET") == NULL)    
    {
      //Default value
      LoopOffset = 0;
    }
  else
    {
      GetXMLValue(SetupNode,"LOOPOFFSET",GetName().c_str(),LoopOffset);
    }
  
  //Get the input file name
  if(FindSubNode(SetupNode,"FILENAME") == NULL)
    {
      PrintError("FILENAME subnode not found");
      return false;
    }
  else    
    GetXMLValue(SetupNode,"FILENAME",GetName().c_str(),InFileName);
  InFile = fopen(InFileName.c_str(),"rb");
  if(!InFile)
    {
      printf("Error: %s not found.\n",InFileName.c_str());
      return false;
    }

  //Build the read file's full path.
  std::string fullPath;
  if(!std::getenv("PWD"))
    {
      fprintf(stderr,"Error getting path\n");
      return false;
    };
  fullPath.assign(std::getenv("PWD"));
  fullPath+="/";
  fullPath+=InFileName;
  //Get file stats.
  struct stat fileStats;
  int statErr = stat(fullPath.c_str(),&fileStats);
  if(statErr)
    {
      fprintf(stderr,"Error on file stat\n");
      return false;
    }

  //Get the file's size in bytes and divide by four to get uint32_t size.
  //Allocate enough memory to hold the whole file in memory.
  FakeWFDData = new uint32_t[fileStats.st_size >> 2];
  if(!FakeWFDData) 
    {
      fprintf(stderr,"Error allocating fake data\n");
      return false;
    }
  FakeWFDDataSize = fileStats.st_size >>2;
  //read in the file.
  uint32_t readNumber = fread((uint8_t*)FakeWFDData,sizeof(uint8_t),FakeWFDDataSize <<2,InFile);
  if(readNumber != (FakeWFDDataSize <<2))
    {
      fprintf(stderr,"Error between file size read number\n");
      return false;
    }

  //Find events.
  for(uint32_t iWord = 0; iWord < FakeWFDDataSize;)
    {
      //Check for event header
      if((FakeWFDData[iWord]&0xF0000000) == 0xA0000000) 
	{
	  //Store pointer to this event.
	  FakeWFDEvent.push_back(FakeWFDData + iWord);
	  //Move forward by even size.
	  iWord += (FakeWFDData[iWord]&0x0FFFFFFF);
	}
      else
	{
	  iWord++;
	}
    }
  printf("v1720FakeReadout: Loaded %lu WFD events from file %s.\n",FakeWFDEvent.size(),fullPath.c_str());
 
  //Set the loop offset variable
  if(LoopOffset == 0)
    {
      //The user didn't specify an even offset value,
      //so use the event count
      FakeEventCount = FakeWFDEvent.size();
    }
  else
    {
      FakeEventCount = LoopOffset;
    }

  WFDsPerEvent = FakeWFDEvent.size()/FakeEventCount;
  EventCountOffset = 0;

  //We no longer need fInFile open, so let's close it now.
  fclose(InFile);
  //Reset statistics
  lastTime = time(NULL);  

  //Set the current FAKE event index to zero
  iWFDEvent = 0;

  //Set real event ID and WFD event IDs to zero
  for(int iWFD=0;iWFD<14;iWFD++)
    {
      WFDEventID[iWFD] = 0;
    }





  currentTime = time(NULL);
  lastTime = time(NULL);
  Ready = true;
  return true ;  
}

#endif
