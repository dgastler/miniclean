#ifndef __DRPUSHER__
#define __DRPUSHER__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <Connection.h>
#include <DRDataPacket.h>

#include <FEPCRATManager.h>

#include <DCDeque.h>

class DRPusher : public DCThread 
{
 public:
  DRPusher(std::string Name);
  ~DRPusher();
  virtual bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  
  //_Net.cpp
  bool ProcessNewConnection();
  void DeleteConnection();
  void ProcessNetThreadMessage();
  bool StartNewConnection();
  //Connection to our server
  Connection DRAssembler;
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;


  

  //_EventProcess
  void ProcessUnSentEvent();  
  void ParseEvent(int32_t index, uint64_t password);
  void MoveBad(int64_t badEventID,int32_t badIndex,int32_t errorCode,
	       int32_t index,uint64_t password);
  bool AddToLocalDeque(FEPCBlock *block,
		       int32_t index,
		       uint64_t password,
		       time_t time,
		       uint32_t channelsProcessed);

  //_NetworkProcess
  bool CheckParseStatus();
  void SetupDRPacketEvent();
  void AddToPacket(RAT::DS::Raw * raw, uint16_t pmtID);
  void SendData();
  bool SendPacket();


  
  //DCThread overloads
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(){DeleteConnection();};
  virtual void PrintStatus();

  //Update control
  time_t lastTime;
  time_t currentTime;

  //Stats
  uint32_t timeStepProcessedEvents;

  // Memory access
  //FEPC Rat manager
  FEPCRATManager * FEPC_mem;

  //====================================
  //local queue
  //====================================
  struct sLocal
  {
    FEPCBlock * block;  //Has eventID,and sent status
    int32_t index;
    uint64_t password;
    time_t time;
    uint32_t channelsProcessed;
    bool operator==(const sLocal &rhs) const 
    {
      return((rhs.block == block)&&
	     (rhs.index == index)&&
	     (rhs.password == password)&&
	     (rhs.time == time)&&
	     (rhs.channelsProcessed == channelsProcessed));	 
    };
    bool operator!=(const sLocal &rhs) const 
    {
      return !(*this == rhs);
    };
  };
  DCDeque<sLocal,NoSelect,NotLockedObject> LocalDeque;
  void AddToDeque(FEPCBlock * block,int32_t index,uint64_t password,
		  time_t time,uint32_t channelProcessed);
  //  void Print_sLocal(sLocal local);
    
  //====================================
  //packet interface 
  //====================================
  DCNetPacket * packet;          //Pointer to packet object
  uint8_t *packetDataPtr;        //Ptr to the begining of the packet data
  uint8_t *currentPacketDataPtr; //Ptr to current position in the packet
  uint16_t packetSizeLeft;       //remaining packet data size
  int32_t iDCPacket;
  //====================================
  //event parsing 
  //====================================
  DRPacket::Event * drEvent;     //Pointer to the current Event struct
                                 //in the packet
};


#endif
