#include <DRPusher.h>

bool DRPusher::ProcessNewConnection()
{  
  bool ret = true;
  if(DRAssembler.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(DRAssembler.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(DRAssembler.thread->GetMessageOutFDs()[0]);      
      DRAssembler.thread->Start(NULL);
      printf("Connected to DRAssembler at: %s\n",DRAssembler.remoteIP.c_str());
      //If Loop is true then we need to add the FEPCRatMemory manager
      //to the select set. 
      //It wouldn't have been allowed to be added in the absence of a 
      //net thread connection
      if(Loop)
	{
	  AddReadFD(FEPC_mem->GetUnSentFDs()[0]);      
	}
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}
void DRPusher::DeleteConnection()
{
  //Making sure that the thread actually exists
  if(DRAssembler.thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(DRAssembler.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(DRAssembler.thread->GetMessageOutFDs()[0]);
    }
  //See if we are actually connected
  if(DRAssembler.fdSocket != BadPipeFD)
    {
      //remove any selecting on fdSocket
      RemoveReadFD(DRAssembler.fdSocket);
      RemoveWriteFD(DRAssembler.fdSocket);
    }  

  //Shutdown the thread
  DRAssembler.Shutdown();

  //stop listening to the FEPC memory manager, since we can't process anymore events
  RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
  SetupSelect();
}

void DRPusher::ProcessNetThreadMessage()
{
  DCMessage::Message message = DRAssembler.thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::ERROR)
    {
      //Shut down our current thread
      DeleteConnection();
      //Start trying to get a new connection
      StartNewConnection();
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message,true);
    }
  else
    {
      PrintWarning("Unknown message from DCMessageClient's DCNetThread\n");
    }
}


//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DRPusher::StartNewConnection()
{
  if(DRAssembler.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return(true);
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}

    }
  else 
    {
      if(DRAssembler.fdSocket >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (DRAssembler.fdSocket);
	  AddWriteFD(DRAssembler.fdSocket);
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}
    }
  return(true);
}
