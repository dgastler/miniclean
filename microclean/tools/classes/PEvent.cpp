#include <iostream>
#include <TDirectory.h>
#include <TProcessID.h>

#include "PEvent.h"


using namespace std;

ClassImp(PEvent)


//______________________________________________________________________________
  PEvent::PEvent() : fIsValid(kFALSE)
{

  Clear("C");

  fIsValid = kTRUE;

  
  
}
//______________________________________________________________________________
  
PEvent::~PEvent()
{
  Clear("C");
}

//______________________________________________________________________________

void PEvent::Clear(Option_t *option)
{
  fIsValid = kFALSE;
 
  AbvTHRS = false;
  bSubEvent = false;
  iSubType = 0;

  iEventNumber = 0;

  if(option == "C")
    {
      vFast0.assign(vFast0.size(),0);
      vFast1.assign(vFast0.size(),0);
      vFast2.assign(vFast0.size(),0);
      vFast3.assign(vFast0.size(),0);
      vFast4.assign(vFast0.size(),0);
      vSlow0.assign(vFast0.size(),0);
      vSlow1.assign(vFast0.size(),0);
      vSlow2.assign(vFast0.size(),0);
      vSlow3.assign(vFast0.size(),0);
      vSlow4.assign(vFast0.size(),0);

      vTOF.assign(vFast0.size(),0);
      vShift.assign(vFast0.size(),0);
      vDelay.assign(vFast0.size(),0);
      vMaxHeight.assign(vFast0.size(),0);
      vOffset.assign(vFast0.size(),0);
      vOffsetSigma.assign(vFast0.size(),0);
      
      vSubFast0.assign(vSubFast0.size(),0);
      vSubFast1.assign(vSubFast0.size(),0);
      vSubFast2.assign(vSubFast0.size(),0);
      vSubFast3.assign(vSubFast0.size(),0);
      vSubFast4.assign(vSubFast0.size(),0);
      vSubSlow0.assign(vSubFast0.size(),0);
      vSubSlow1.assign(vSubFast0.size(),0);
      vSubSlow2.assign(vSubFast0.size(),0);
      vSubSlow3.assign(vSubFast0.size(),0);
      vSubSlow4.assign(vSubFast0.size(),0);

      vSubTOF.assign(vSubFast0.size(),0);
      vSubShift.assign(vSubFast0.size(),0);
      vSubDelay.assign(vSubFast0.size(),0);
      vSubMaxHeight.assign(vSubFast0.size(),0);

    }
  else
    {
      vFast0.clear();
      vFast1.clear();
      vFast2.clear();
      vFast3.clear();
      vFast4.clear();
      vSlow0.clear();
      vSlow1.clear();
      vSlow2.clear();
      vSlow3.clear();
      vSlow4.clear();

      vTOF.clear();
      vShift.clear();
      vDelay.clear();
      vMaxHeight.clear();

      vSubFast0.clear();
      vSubFast1.clear();
      vSubFast2.clear();
      vSubFast3.clear();
      vSubFast4.clear();
      vSubSlow0.clear();
      vSubSlow1.clear();
      vSubSlow2.clear();
      vSubSlow3.clear();
      vSubSlow4.clear();

      vSubTOF.clear();
      vSubShift.clear();
      vSubDelay.clear();
      vSubMaxHeight.clear();
    }
  
}

//______________________________________________________________________________
void PEvent::SetSize(UInt_t i)
{
  vFast0.resize(i,0);
  vFast1.resize(i,0);
  vFast2.resize(i,0);
  vFast3.resize(i,0);
  vFast4.resize(i,0);
  vSlow0.resize(i,0);
  vSlow1.resize(i,0);
  vSlow2.resize(i,0);
  vSlow3.resize(i,0);
  vSlow4.resize(i,0);

  vTOF.resize(i,0);
  vShift.resize(i,0);
  vDelay.resize(i,0);
  vMaxHeight.resize(i,0);

  vSubFast0.resize(i,0);
  vSubFast1.resize(i,0);
  vSubFast2.resize(i,0);
  vSubFast3.resize(i,0);
  vSubFast4.resize(i,0);
  vSubSlow0.resize(i,0);
  vSubSlow1.resize(i,0);
  vSubSlow2.resize(i,0);
  vSubSlow3.resize(i,0);
  vSubSlow4.resize(i,0);

  vSubTOF.resize(i,0);
  vSubShift.resize(i,0);
  vSubDelay.resize(i,0);
  vSubMaxHeight.resize(i,0);

  fIsValid = kTRUE;

}
//______________________________________________________________________________
Float_t PEvent::GetSize()
{
  return(vFast0.size());
}
//______________________________________________________________________________
void    PEvent::Fast(UInt_t i,UInt_t n,Float_t value)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  vFast0[i] = value;
	  break;
	case 1:
	  vFast1[i] = value;
	  break;
	case 2:
	  vFast2[i] = value;
	  break;
	case 3:
	  vFast3[i] = value;
	  break;
	case 4:
	  vFast4[i] = value;
	  break;
	default:
	  break;
	}
    }
}
Float_t PEvent::Fast(UInt_t i,UInt_t n)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  return(vFast0[i]);
	  break;
	case 1:
	  return(vFast1[i]);
	  break;
	case 2:
	  return(vFast2[i]);
	  break;
	case 3:
	  return(vFast3[i]);
	  break;
	case 4:
	  return(vFast4[i]);
	  break;
	default:
	  return(0);
	}
    }
  return(0);
}
void    PEvent::Slow (UInt_t i,UInt_t n,Float_t value)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  vSlow0[i] = value;
	  break;
	case 1:
	  vSlow1[i] = value;
	  break;
	case 2:
	  vSlow2[i] = value;
	  break;
	case 3:
	  vSlow3[i] = value;
	  break;
	case 4:
	  vSlow4[i] = value;
	  break;
	default:
	  break;
	}
    }
}
Float_t PEvent::Slow (UInt_t i,UInt_t n)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  return(vSlow0[i]);
	  break;
	case 1:
	  return(vSlow1[i]);
	  break;
	case 2:
	  return(vSlow2[i]);
	  break;
	case 3:
	  return(vSlow3[i]);
	  break;
	case 4:
	  return(vSlow4[i]);
	  break;
	default:
	  return(0);
	}
    }
  return(0);
}


//______________________________________________________________________________
//Sub versions of these functions
void    PEvent::SubFast(UInt_t i,UInt_t n,Float_t value)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  vSubFast0[i] = value;
	  break;
	case 1:
	  vSubFast1[i] = value;
	  break;
	case 2:
	  vSubFast2[i] = value;
	  break;
	case 3:
	  vSubFast3[i] = value;
	  break;
	case 4:
	  vSubFast4[i] = value;
	  break;
	default:
	  break;
	}
    }
}
Float_t PEvent::SubFast(UInt_t i,UInt_t n)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  return(vSubFast0[i]);
	  break;
	case 1:
	  return(vSubFast1[i]);
	  break;
	case 2:
	  return(vSubFast2[i]);
	  break;
	case 3:
	  return(vSubFast3[i]);
	  break;
	case 4:
	  return(vSubFast4[i]);
	  break;
	default:
	  return(0);
	}
    }
  return(0);
}
void    PEvent::SubSlow (UInt_t i,UInt_t n,Float_t value)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  vSubSlow0[i] = value;
	  break;
	case 1:
	  vSubSlow1[i] = value;
	  break;
	case 2:
	  vSubSlow2[i] = value;
	  break;
	case 3:
	  vSubSlow3[i] = value;
	  break;
	case 4:
	  vSubSlow4[i] = value;
	  break;
	default:
	  break;
	}
    }
}
Float_t PEvent::SubSlow (UInt_t i,UInt_t n)
{
  if(i < GetSize())
    {
      switch(n)
	{
	case 0:
	  return(vSubSlow0[i]);
	  break;
	case 1:
	  return(vSubSlow1[i]);
	  break;
	case 2:
	  return(vSubSlow2[i]);
	  break;
	case 3:
	  return(vSubSlow3[i]);
	  break;
	case 4:
	  return(vSubSlow4[i]);
	  break;
	default:
	  return(0);
	}
    }
  return(0);
}
