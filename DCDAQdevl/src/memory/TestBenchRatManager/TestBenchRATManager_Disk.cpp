#include <TestBenchRATManager.h>

bool TestBenchRATManager::GetDisk(int32_t &index)
{
  bool ret = false;
  ret = Disk.Get(index);
  return ret ;
}


bool TestBenchRATManager::AddFree(int32_t &index)
{
  bool ret = false;
  ret = Free.Add(index);
  //signal so that if anyone is in cond_wait in Get() they wake up
  return(ret);
}
