#include <PrintStats.h>

PrintStats::PrintStats(std::string Name)
{
  Ready = false;
  SetType("PrintStats");
  SetName(Name);  
}

PrintStats::~PrintStats()
{
  //Create a DC id register data type
  DCMessage::DCTypeID typeUnRegisterMessage;
  //UnRoute DCMessage::STATISTIC messages to this thread
  std::vector<uint8_t> messageData;
  messageData = typeUnRegisterMessage.SetData(DCMessage::STATISTIC,
					      GetName());
  
  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  message.SetType(DCMessage::UNREGISTER_TYPE);
  message.SetDestination(); //No destination means that DCMessageProcessor 
                            //route it to it's DCDAQ command list
  message.SetData(&messageData[0],messageData.size());
  SendMessageOut(message,true);
}

bool PrintStats::Setup(xmlNode * /*SetupNode*/,
		       std::map<std::string,MemoryManager*> &/*MemManager*/)
{
  //Create a DC id register data type
  DCMessage::DCTypeID typeRegisterMessage;
  //Route DCMessage::STATISTIC messages to this thread
  std::vector<uint8_t> messageData;
  messageData = typeRegisterMessage.SetData(DCMessage::STATISTIC,
					    GetName());
  
  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  message.SetType(DCMessage::REGISTER_TYPE);
  message.SetDestination(); //No destination means that DCMessageProcessor 
                            //route it to it's DCDAQ command list
  message.SetData(&messageData[0],messageData.size());
  SendMessageOut(message,true);
  Ready = true;
  return(true);
}

bool PrintStats::ProcessMessage(DCMessage::Message &message)
{
  if(message.GetType() == DCMessage::STATISTIC)
    {
      //Get message source
      std::string name;
      struct in_addr addr;
      message.GetSource(name,addr);

      //Get message time
      DCMessage::DCtime_t timeTemp = message.GetTime();	        

      //Get statistics message      
      DCMessage::SingleUINT64 sUINT64;
      DCMessage::TimedCount   tCount;
      DCMessage::TimedDouble  tDouble;
      DCMessage::SingleDouble sDouble;
      
      if(message.GetDataStruct(sUINT64))
	{
# if __WORDSIZE == 64
	  char buffer[] = "%s (%s):%s %lu\n";
# else
	  char buffer[] = "%s (%s):%s %llu\n";
# endif
	  printf(buffer,
		 GetTime(&timeTemp).c_str(),name.c_str(),
		 sUINT64.text,sUINT64.i);
	}
      else if(message.GetDataStruct(tCount))
	{
	  double value = double(tCount.count)/tCount.dt;
	  char * buffer = new char[100];
	  if(fabs(value) < 10000)
	    sprintf(buffer,"%s","%s (%s):%s %fps\n");
	  else
	    sprintf(buffer,"%s","%s (%s):%s %5.2eps\n");	  
	  printf(buffer,
		 GetTime(&timeTemp).c_str(),name.c_str(),
		 tCount.text,value
		 );	  
	  delete [] buffer;
	}
      else if(message.GetDataStruct(tDouble))
	{	  
	  double value = double(tDouble.value)/tCount.dt;
	  char * buffer = new char[100];
	  if(fabs(value) < 10000)
	    sprintf(buffer,"%s","%s (%s):%s %fps\n");
	  else
	    sprintf(buffer,"%s","%s (%s):%s %5.2eps\n");	  
	  printf(buffer,
		 GetTime(&timeTemp).c_str(),name.c_str(),
		 tDouble.text,value
		 );	  
	  delete [] buffer;
	}
      else if(message.GetDataStruct(sDouble))
	{
	  printf("%s (%s):%s %f\n",
		 GetTime(&timeTemp).c_str(),name.c_str(),
		 sDouble.text,sDouble.value);
	}
      else
	{
	  printf("%s (%s): Unknown stat.\n",
		 GetTime(&timeTemp).c_str(),
		 name.c_str());
	}
      return(true);
    }
  return(false);
}
