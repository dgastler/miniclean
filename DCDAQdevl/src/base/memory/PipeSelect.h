#ifndef __PIPESELECT__
#define __PIPESELECT__

#include <stdlib.h>

class PipeSelect
{
 public:
  PipeSelect()
    {
      //Allocate our pipe
      int pipeReturn = pipe(Pipe);   
      if(pipeReturn == -1)           
	//something went wrong with our pipe
	{
	  printf("Warning:  Locked Queue failed to make FIFO! (error:%s)\n",
		 strerror(errno));	  
	  //Make sure the FIFOs are assigned to -1 if thing went wrong
	  Pipe[0] = -1;
	  Pipe[1] = -1;
	  //Cause code to fail
	  abort();
	}
    }
  virtual ~PipeSelect()
    {
      //close our FIFO
      if(Pipe[1] != -1)
	{
	  close(Pipe[1]);   //Close the pipe
	  Pipe[1] = -1;     //Make sure we think it's closed
	}
      if(Pipe[0] != -1)
	{
	  close(Pipe[0]);   //Close the pipe
	  Pipe[0] = -1;     //Make sure we think it's closed
	}
    }

  uint8_t Read()
  {
    //Read one byte from the pipe to show that we got the message
    uint8_t readValue;
    //This value doesn't matter, just that it exists in the pipe
    ssize_t readReturn = read(Pipe[0],&readValue,sizeof(uint8_t));
    //Check for any read errors
    if(readReturn == -1)
      {
	fprintf(stderr,
		"Error in pipe read: %s\n",
		strerror(errno)
		);
      }
    return(readValue);
  }
  
  void Write(uint8_t val)
  {
    //Write the val into the pipe.  
    ssize_t writeReturn = write(Pipe[1],&val,sizeof(uint8_t));
    //Check that our write worked
    if(writeReturn == -1)
      {
	fprintf(stderr,
		"Error in queue FIFO write: %s\n",
		strerror(errno)
		);
      }
  }
  

  const int * GetFDs() {return(Pipe);}; //Return a constant pointer to the FDs
 private:
  //internal pipe
  int Pipe[2];
};

#endif
