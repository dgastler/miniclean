#include <EBWriter.h>

void EBWriter::MainLoop()
{
  if(IsReadReady(EBPC_mem->GetToDiskFDs()[0]))
    { 
      //Get the complete event's index
      int32_t index;
      if(EBPC_mem->GetToDisk(index))
	//We got an index from the 
	{
	  if(index != EBPC_mem->GetBadValue())
	    //A valid event
	    {
	      //BranchDS = new RAT::DS::Root;
	      //Check if we have a file opened.  If we don't open one.
	      // fprintf(stderr,"Setting BranchDS to null");
	      BranchDS = NULL;
	      if(!OutFile)
		{		      
		  fileReady = OpenFile();
		}
	      if(fileReady)
		{	      
		  EBPCBlock * event = EBPC_mem->GetEBPCBlock(index);
		  //fprintf(stderr,"Writing event #%lu\n",event->eventID);
		  //Make pointer to ds for filling tree
		  BranchDS = event->ds;
		  event->ds->GetEV(0)->SetEventID(EventNumber);
		 
		  //Fill the TFile tree with the contents of BranchDS
		  Tree->Fill(); 
		  event->ds->GetEV(0)->PrunePMT();
		  BranchDS = NULL;

		  EventNumber++;  //Increment the event number
		  timeStepProcessedEvents++;  //Increment the time step event count
		  //add index to the free queue
		  if(!EBPC_mem->AddFree(index))
		    {
		      PrintError("Lost a EVBlock\n");
		    }
		  
		  
		  //Check if we have gone over the maximum number of events
		  if(EventNumber > MaxRunEvents)
		    {			  
		      printf("Max run events reached\n");
		      SendStop();
		    }
		  else
		    {
		      SubEventNumber++;
		      //Handle subrun changes
		      if(SubEventNumber > SubRunMaxEvents)
			{
			  CloseFile();
			  SubRunNumber++;
			}		  
		    }
		}//file is open
	      else
		{
		  PrintError("File not ready for write!");
		  SendStop();
		}
	    
	    }
	  else
	    {
	      //We are probably shutting down now. 
	      PrintError("Bad event index\n");
	      SendStop();
	    }
	}
      else
	//We couldn't get a new ToDisk
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a ToDisk event.  DCDAQ shutdown?\n");
	  SendStop();
	}
    }
}


bool EBWriter::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(EBPC_mem->GetToDiskFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the to disk queue with select
      AddReadFD(EBPC_mem->GetToDiskFDs()[0]);      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}


void EBWriter::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      float timeStep = difftime(currentTime,lastTime);
      DCMessage::Message message;
      DCMessage::TimedCount Count;
      sprintf(Count.text,"Events read");
      Count.count = timeStepProcessedEvents;
      Count.dt = timeStep;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      timeStepProcessedEvents = 0;

      //Setup next time
      lastTime = currentTime;
    }
}

