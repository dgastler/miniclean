#ifndef __CHAN_ZLE_WAVEFORM__
#define __CHAN_ZLE_WAVEFORM__

#include <PacketBuilder.h>

class CHAN_ZLE_WAVEFORM : public PacketBuilder
{
 public:
  CHAN_ZLE_WAVEFORM()
  {
    ClearPacket();
    packetTypeFull=DCNetPacket::EB_ZLE_WAVEFORMS_FULL_PACKET;
    packetTypePartial=DCNetPacket::EB_ZLE_WAVEFORMS_PARTIAL_PACKET;
  };
  virtual ~CHAN_ZLE_WAVEFORM(){ClearPacket();};
  
protected:
  virtual uint32_t CalculatePMTSize(RAT::DS::Raw * raw);
  virtual int AddData(RAT::DS::Raw * raw);
  virtual int AddChannelHeader(RAT::DS::PMT * pmt);
};

#endif
