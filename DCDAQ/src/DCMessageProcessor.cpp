#include <DCMessageProcessor.h>

DCMessageProcessor::DCMessageProcessor(DCLauncher * _Launcher)
{
  Launcher = _Launcher;
  typeMap[DCMessage::STOP]           = &DCMessageProcessor::Stop;
  typeMap[DCMessage::GO]             = &DCMessageProcessor::Go;
  typeMap[DCMessage::PAUSE]          = &DCMessageProcessor::Pause;
  typeMap[DCMessage::READOUTSIZE]    = &DCMessageProcessor::ReadOutSize;
  typeMap[DCMessage::READOUTNUMBER]  = &DCMessageProcessor::ReadOutNumber;
  typeMap[DCMessage::READOUTEVENTS]  = &DCMessageProcessor::ReadOutEvents;
  typeMap[DCMessage::RATERROR_POE]   = &DCMessageProcessor::RATError_POE;
  typeMap[DCMessage::EVENTERROR_POE] = &DCMessageProcessor::EventError_POE;
  typeMap[DCMessage::BLOCKSREAD]     = &DCMessageProcessor::BlocksRead;
  typeMap[DCMessage::PULSEHITS]      = &DCMessageProcessor::PulseHits;
  typeMap[DCMessage::EVENTSREAD]     = &DCMessageProcessor::EventsRead;
  typeMap[DCMessage::DATAIN]         = &DCMessageProcessor::DataIn;
  typeMap[DCMessage::DATAOUT]        = &DCMessageProcessor::DataOut;
  typeMap[DCMessage::LAUNCH]         = &DCMessageProcessor::Launch;          
  typeMap[DCMessage::RUN_NUMBER]     = &DCMessageProcessor::RunNumber;
}

bool DCMessageProcessor::ProcessMessage(int selectReturn,
					fd_set retReadSet,
					fd_set retWriteSet,
					fd_set retExceptionSet)
{
  //Iterator for out command lookup.
  std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,DCThread *,bool &)>::iterator it;
  //Iterator for our FD-thread map
  std::map<int,int>::const_iterator FDit;

  bool runReturn = true;
  //Loop over all the entries in the FDMap and find those that are readible
  for(FDit = Launcher->GetFDMap().begin();
      ((FDit != Launcher->GetFDMap().end()) &&
       (selectReturn >0));
      FDit++)
    {
      //Ask if this FD was ready
      if(FD_ISSET(FDit->first,&retReadSet))
	{
	  //report that we've addressed this fd
	  selectReturn--;
	  //Get the message from the read thread.
	  DCMessage::Message message = Launcher->GetThreads()[FDit->second]->GetMessageOut(true);
	  //Search for the correct command function for this message type.
	  it = typeMap.find(message.GetType());
	  if(it != typeMap.end())
	    {
	      //Pass off the message, the thread that sent it and the DCDAQ loop
	      //running bool to the command function
	      (*this.*(it->second))(message,
			      Launcher->GetThreads()[FDit->second],
			      runReturn);
	    }
	}
    }
  return(runReturn);
}

bool DCMessageProcessor::ProcessMessage(DCMessage::Message message)
{
  bool runReturn = true;
  //Iterator for out command lookup.
  std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,DCThread *,bool &)>::iterator it;
  //Search for the correct command function for this message type.
  it = typeMap.find(message.GetType());
  if(it != typeMap.end())
    {
      //Pass off the message, the thread that sent it and the DCDAQ loop
      //running bool to the command function
      (*this.*(it->second))(message,
			    NULL,
			    runReturn);
    }
  return(runReturn);
}


//Commands
void DCMessageProcessor::Stop(DCMessage::Message & message,
			      DCThread * thread,
			      bool & run)
{
  run = false;
  uint64_t timeTemp = message.GetTime();
  printf("%s (%s): STOP!\n",
	 GetTime(&(timeTemp)).c_str(),
	 thread->GetName().c_str());      
}

void DCMessageProcessor::Go(DCMessage::Message & message,
			    DCThread * thread,
			    bool & run)
{
  std::vector<DCThread*> threads = Launcher->GetThreads();
  //Send all threads the GO message
  for(unsigned int i = 0; i < threads.size();i++)
    {	  
      DCMessage::Message GOmessage;
      GOmessage.SetType(DCMessage::GO);
      threads[i]->SendMessageIn(GOmessage,true);
    }
}
void DCMessageProcessor::Pause(DCMessage::Message & message,
			       DCThread * thread,
			       bool & run)
{
  std::vector<DCThread*> threads = Launcher->GetThreads();
  //Send all threads the PAUSE message
  for(unsigned int i = 0; i < threads.size();i++)
    {
      DCMessage::Message PAUSEmessage;
      PAUSEmessage.SetType(DCMessage::PAUSE);
      threads[i]->SendMessageIn(PAUSEmessage,true);
    }
}

void DCMessageProcessor::ReadOutSize(DCMessage::Message & message,
				     DCThread * thread,
				     bool & run)             
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::WFDTrans))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::WFDTrans * formData = (DCMessage::WFDTrans *)&(data[0]);
      printf("%s (%s): WFD %d data rate:  %f MBytes per second\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->ID,
	     formData->bytes/(formData->dt*1048576.0) /*MBytes*/
	     );
    }
}
void DCMessageProcessor::ReadOutNumber(DCMessage::Message & message,    
				       DCThread * thread,      
				       bool & run)             
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::WFDROs))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::WFDROs * formData = (DCMessage::WFDROs *)&(data[0]);
      printf("%s (%s): WFD %d readouts:  %f ps\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->ID,
	     formData->readouts/formData->dt);
    }
}
void DCMessageProcessor::ReadOutEvents(DCMessage::Message & message,    
				       DCThread * thread,      
				       bool & run)             
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::WFDEv))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::WFDEv * formData = (DCMessage::WFDEv *)&(data[0]);
      printf("%s (%s): WFD %d event rate: %fhz\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->ID,
	     formData->events/formData->dt);
    }
}
void DCMessageProcessor::RATError_POE(DCMessage::Message & message,    
				      DCThread * thread,      
				      bool & run)             

{
  run = false;	
  uint64_t timeTemp = message.GetTime();	  
  printf("%s (%s): RAT ERROR STOP!\n",
	 GetTime(&timeTemp).c_str(),
	 thread->GetName().c_str());
}	      
void DCMessageProcessor::EventError_POE(DCMessage::Message & message,    
					DCThread * thread,      
					bool & run)             
{
  run = false;		  
  uint64_t timeTemp = message.GetTime();
  printf("%s (%s): EVENT ERROR STOP!\n",
	 GetTime(&timeTemp).c_str(),
	 thread->GetName().c_str());
}

void DCMessageProcessor::BlocksRead(DCMessage::Message & message,    
				    DCThread * thread,      
				    bool & run)             

{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::SingleUINT64))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::SingleUINT64 * formData = (DCMessage::SingleUINT64 *)&(data[0]);
# if __WORDSIZE == 64
      printf("%s (%s): Processed %lu blocks.\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->i);
# else
      printf("%s (%s): Processed %llu blocks.\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->i);
#endif
    }
}

void DCMessageProcessor::PulseHits(DCMessage::Message & message,    
				   DCThread * thread,      
				   bool & run)             
  
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::PulseFreq))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::PulseFreq * formData = (DCMessage::PulseFreq *)&(data[0]);
      printf("%s (%s): Generating waveforms with %d pulses at %d frequency.\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->n + 1, 
	     formData->f);
    }
}


void DCMessageProcessor::EventsRead(DCMessage::Message & message,    
				    DCThread * thread,      
				    bool & run)             
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::TimedCount))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::TimedCount * formData = (DCMessage::TimedCount *)&(data[0]);
      printf("%s (%s): RAT event rate: %f hz.\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->count/formData->dt);
    }
}

void DCMessageProcessor::DataIn(DCMessage::Message & message,    
				DCThread * thread,      
				bool & run)             
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::TimedCount))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::TimedCount * formData = (DCMessage::TimedCount *)&(data[0]);
      printf("%s (%s): Network data (RX) rate: %f MBps.\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->count/(formData->dt*1024.0*1024.0));
    }
}

void DCMessageProcessor::DataOut(DCMessage::Message & message,    
				 DCThread * thread,      
				 bool & run)             
{
  std::vector<uint8_t> data;
  message.GetData(data); //Fill data with the message's data
  if(data.size() >= sizeof(DCMessage::TimedCount))
    {
      uint64_t timeTemp = message.GetTime();
      DCMessage::TimedCount * formData = (DCMessage::TimedCount *)&(data[0]);
      printf("%s (%s): Network data (TX) rate: %f MBps.\n",
	     GetTime(&timeTemp).c_str(),
	     thread->GetName().c_str(),
	     formData->count/(formData->dt*1024.0*1024.0));
    }
}
  
void DCMessageProcessor::Launch(DCMessage::Message & message,    
				DCThread * thread,      
				bool & run)             
{  
  std::vector<uint8_t> data;
  message.GetData(data);
  if(data.size() >0)
    {
      //copy string data.
      std::string setupText((char*)&data[0],data.size());
      Launcher->Launch(setupText);      
    }
}

void DCMessageProcessor::RunNumber(DCMessage::Message & message,
				   DCThread * thread,
				   bool & run)
{
  //Get data from message
  std::vector<uint8_t> data;
  message.GetData(data);
  //Check that it's a valid size
  if(data.size() >= sizeof(int))
    {
      std::vector<DCThread*> threads = Launcher->GetThreads();
      //forward the RUN_NUMBER message to everyone.
      for(unsigned int i = 0; i < threads.size();i++)
	{
	  //Copy message's data into RunNumberMessage and send out.
	  DCMessage::Message RunNumberMessage;      
	  RunNumberMessage.SetData(&data[0],data.size());
	  RunNumberMessage.SetType(DCMessage::RUN_NUMBER);
	  threads[i]->SendMessageIn(RunNumberMessage,true);
	}
    }
}
