Brief DCDAQ code overview.   (contact Dan Gastler @ dgastler@bu.edu)

DCDAQ is the DAQ software package for the DEAP/CLEAN experiments.   It's a collection of processors(threads) and memory managers (thread safe memory managers) used to operate DAQ hardware and provide real time data reduction and event building.   The package is written in C++ and uses pthreads for multi-threading, libxml2 for setup configuration files, RAT for data output/storage and scons for building.   It is packaged with the CAEN PCI optical interface driver for compiling, but if you are going to use DCDAQ with this device you should install the driver yourself and set the CAENPATH environment variable.  

Directory structure:

build/
	This is where scons puts all the object files for DCDAQ.  The directory structure of the src directory is replicated so there are no name collisions. 

include/
	This is where a copy of every DCDAQ header file is stored for compiling.

XML/
	This is where DCDAQ configuration files can be stored and also where sub-configuration files(CAEN V1720 setup files) are kept.

drivers/
	This is where the CAEN PCI-optical link drivers are stored.

doc/
	Where loads of documentation about DCDAQ are to be stored.
	
src/
	This is where all the DCDAQ source code is stored.  This root of this directory has the main DCDAQ run routines and the base, memory, and threads directories.   The base directory contains base classes and common code for DCDAQ.  The memory directory contains all the derived memory manager classes used by DCDAQ to pass data between threads in a thread safe way.   The threads directory holds the code for all DCDAQ threads. 

	
Code structure:

src:
     DCDAQ.cpp:
	This contains the main routine for the package and is in charge of setting up each memory manager and each thread and monitoring their status using the internal DCThread messaging system.   It also cleans up threads when the DAQ is closed.
     setupDAQ.cpp:
	This code calls the memory manager and thread specific setup routines.

src/memory:
     memorySetup.cpp:
	This code loads the requested memory managers.   If you add your own memory manager, you need to add it to the list of memory managers in this code. 

src/threads:
     threadSetup.cpp:
	This code loads the requested processors.   If you add your own processor thread, you need to add it to the list here.

src/base:
     DCThread.h/cpp:
	This is the base class for all threads in DCDAQ.  It provides a virtual run() function that you override with the main loop of your thread's code.  A Setup() function is also provided for you to override with your thread's setup code.   A in and out message queue is provided as well as the interface for DCDAQ to start your thread. 
	
     DCNetThread.h/cpp:
	This is the class for handling networking sockets in DCDAQ.  It inherits from DCThread and will add a thread for every network socket you open.

     MemoryManager.h:	
     	This is the base class for all memory managers in DCDAQ.

     LockedQueue.h:
	Thread save queue for DCThread message system and thread-safe memory managers.

     netHelper/xmlHelper:  
     	These files help with xml parsing and network communication.

     V1720WFD:
	Classes and interface code for working with the CAEN V1720 WFD.

     V1720Event:
	Code for parsing CAEN V1720 data.  Works with all modes of V1720 data.
