#include <Control.h>
#include <cl_helper.h>
#include <readline/readline.h>
#include <readline/history.h>

Control::Control(std::string Name)
{
  SetType(std::string("Control"));
  SetName(Name);
  bufferSize = 1000;
  buffer = new char[bufferSize];
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  loopTime = 0;
  nextLoopTime = time(NULL);
  //Note Readline prompt and callback handler is installed in Setup()
};
Control::~Control()
{
  delete [] buffer;
  //remove the callback handler
  rl_callback_handler_remove();
}

void Control::LowerCase(std::string &Text)
{
  std::transform(Text.begin(),
		 Text.end(),
		 Text.begin(),
		 tolower);
}
void Control::RemoveEOL(std::string &Text)
{
  size_t pos = 0;
  while((pos = Text.find('\n',0)) != std::string::npos)
    {
      Text.erase(pos,1);
    }
}


std::string Control::ProcessCommand(std::string Command,
				    DCMessage::DCAddress destAddress)
{
  std::string ret("");//Default return string
  //Remove EOL
  RemoveEOL(Command);

  //Find the base of the command
  size_t CommandEnd = Command.find(' ');
  std::string CommandBase = Command.substr(0,CommandEnd);
  //Find the arguments if any.
  std::string CommandArg;

  //Convert all commands to lower case
  LowerCase(CommandBase);

  if((CommandEnd == Command.size()) ||
     (CommandEnd == std::string::npos))
    {
      CommandArg = "";
    }
  else
    {
      CommandArg = Command.substr(CommandEnd+1,
				  Command.size() - CommandEnd);
    }


  //check base against all commands.
  std::vector<std::string>::iterator it = find(Commands.begin(),Commands.end(),CommandBase);
  if(it != Commands.end())
    {
      (*this.*(CommandFunctions[distance(Commands.begin(),it)]))
	(CommandArg,
	 destAddress);
      ret = "DCDAQ>";
    }
  else    
    {
      ret = "Unknown: ";
      ret += CommandBase.c_str();
      ret += "\nDCDAQ>";
    }
  return ret;
}

void Control::MainLoop()
{     
  if(IsReadReady(STDIN_FILENO))
    { 
      SetObjectPtr(this);
      //Readline reads each char until carriage return and then calls the callback function defined in cl_helper.cpp
      rl_callback_read_char();
      ResetObjectPtr();
    }
}

void Control::ProcessTimeout()
{
  //not needed for this thread
  if(loopTime > 0)
    {
      SetSelectTimeout(loopTime,0);
      if(difftime(time(NULL),nextLoopTime)>=0)
	{
	  nextLoopTime = time(NULL) + loopTime;
	  ProcessCommand(loopCommand,loopAddress);
	}
    }
}

bool Control::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      rl_callback_handler_remove();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //We shouldn't pause the human interface thread
      // Loop = false;
      printf("Warning:    Control thread ignoring pause request.\n");
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This shouldn't be needed since this thread
      //should never have Loop=false, but it doesn't hurt.
      Loop = true;
      //Register the STDIN_FILENO fd with DCThread
      AddReadFD(STDIN_FILENO);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::LAUNCH_RESPONSE)
    {
      std::string sourceName;
      uint64_t launchReturn = 0xFFFFFFFFFFFFFFFFLL; //error
      if(message.GetData(launchReturn))
	{	
	  std::stringstream response("");
	  response << "Launch sent to ("
		   << message.GetSource().GetName()
		   << ":"
		   << message.GetSource().GetAddrStr()
		   << ") returned "
		   << launchReturn
		   << message.GetSource().GetAddrStr();
	  Print(response.str());
	}
    }
  else if(message.GetType() == DCMessage::COMMAND_RESPONSE)
    {
      std::string response;
      DCMessage::DCString dcString;
      dcString.GetData(message.GetData(),response);      
      Print(response);
    }
  else
    {
      ret = false;
    }
  return ret;
}
