#include <DCNetPacketManager.h>

DCNetPacketManager::DCNetPacketManager(std::string Name)
{
  //Make sure our memory block is empty.
  SetName(Name);
  SetType(std::string("DCNetPacketManager"));
  Deallocate();
}

DCNetPacketManager::~DCNetPacketManager()
{
  Deallocate();
}

void DCNetPacketManager::Deallocate()
{
  while(Packets.size())
    {
      delete Packets.back();
      Packets.pop_back();
    }
  Packets.clear();
}

bool DCNetPacketManager::AllocateMemory(xmlNode * SetupNode)
{
  bool ret = true;
  //Get the block size;
  uint32_t dataSize;
  int dataSizeERR = GetXMLValue(SetupNode,"DATASIZE","DCNetPacket",dataSize);
  if(dataSizeERR){ret = false;}
  //Get the number of Blocks
  uint32_t nPackets;
  int packetCountERR = GetXMLValue(SetupNode,"PACKETCOUNT","MANAGER",nPackets);
  if(packetCountERR){ret = false;}
  

  Deallocate();  

  for(unsigned int i = 0;(ret && (i < nPackets));i++)
    {
      DCNetPacket * packet = new DCNetPacket(dataSize);      
      Packets.push_back(packet);
      int32_t index = Packets.size()-1;
      AddFree(index);
    }

  return(ret);
}


