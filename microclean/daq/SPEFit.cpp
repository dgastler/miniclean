#include "SPEFit.h"

Double_t function(Double_t *x, Double_t *par)
{
  //SPE function to be fitted to the SPE spectrum. 
  Double_t N = par[0];
  Double_t m = par[1];
  Double_t a = par[2];
  Double_t qa = par[3];
  Double_t b = par[4];
  Double_t qb = par[5];
  Double_t c = par[6];
  Double_t d = par[7];
  Double_t ret = N * m/TMath::Gamma(m) * (
					  a * (pow(m*x[0]/qa,m-1)*exp(-1*m*x[0]/qa)) +
					  b * (pow(m*x[0]/qb,m-1)*exp(-1*m*x[0]/qb)) +
					  c * exp(-1*x[0]/d));
  return(ret);
}


TF1 * SPEfit(TH1F * h,Char_t * Name)
{
  //Take a 1D histogram of pulses and a name for the fit funtion.  
  //Calculate the fit function for this spectrum and return the function as a TF1.  
  //SPE is parameter 5 of the function.
  //Everything here is in C of charge. 
  TF1 * fit = new TF1(Name,function,h->GetBinCenter(h->GetMaximumBin()),1E-10,8);  

  fit->SetNpx(1000);
  //Find a rough region to fit for the dark noise part of the spectrum. 

  //  Float_t Range2i = 0;
  Float_t Range1i = 0;//h->GetBinCenter(h->GetMaximumBin());
  Int_t   Range1i_bin = 0;
  Float_t Range1f = 0;

  for(int i = 1;i < h->GetNbinsX();i++)
    {
      if(h->GetBinContent(i+1) < h->GetBinContent(i))
	{
	  Range1i = h->GetBinCenter(i);
	  Range1i_bin = i;
	  //	  i = h->GetNbinsX();
	  break;
	}
    }

  Float_t SPEguess = 0;
  Int_t SPEguessBin = 0;
  Float_t SPEHeight = 0;

  //  for(Int_t i = h->GetMaximumBin(); i < h->GetNbinsX();i++) 
//  for(Int_t i = Range1i_bin; i < h->GetNbinsX();i++) 
//    {
//      if(h->GetBinContent(i)/h->GetMaximum() < 0.5)
//	{
//	  Range2i = h->GetBinCenter(i-1);
//	  //exit loop
//	  i = h->GetNbinsX();
//	}
//    }
  //  for(Int_t i = h->GetMaximumBin(); i < h->GetNbinsX()-1;i++) 
  Bool_t FoundRange1f = false;
  for(Int_t i = Range1i_bin; i < h->GetNbinsX()-1;i++) 
    {
      if(h->GetBinContent(i+1) > h->GetBinContent(i)  && !FoundRange1f)
	{
	  Range1f = h->GetBinCenter(i);
	  FoundRange1f = true;
	}
      if(FoundRange1f && h->GetBinContent(i) > SPEHeight)
	{
	  SPEHeight = h->GetBinContent(i);
	  SPEguessBin = i;
	  //	  break;
	}
    }
  
  SPEguess = h->GetBinCenter(SPEguessBin);
  TF1 * Gaus = new TF1("Gaus","gaus",Range1f,SPEguess*2 - Range1f);
  Gaus->SetParameters(h->GetBinContent(SPEguessBin),
		      SPEguess,
		      SPEguess - Range1f);
  h->Fit(Gaus,"MBQR");
  SPEguess = Gaus->GetParameter(1);
  h->GetListOfFunctions()->Clear();
  delete Gaus;
  //  std::cout << Range1i << " " << Range1f << " " << SPEguess << std::endl;
//  TLine * L1 = new TLine(Range1i,0,Range1i,h->GetMaximum()*1.05);
//  L1->SetLineColor(kRed);
//  TLine * L2 = new TLine(Range1f,0,Range1f,h->GetMaximum()*1.05);
//  L2->SetLineColor(kBlue);
//  TLine * L3 = new TLine(SPEguess,0,SPEguess,h->GetMaximum()*1.05);
//  L3->SetLineColor(kGreen);
//
//  h->GetListOfFunctions()->AddLast(L1);
//  h->GetListOfFunctions()->AddLast(L2);
//  h->GetListOfFunctions()->AddLast(L3);
  
  //Fitting dark noise peak only
  fit->SetParameters(h->GetMaximum(),
		      1,
		      1,
		     Range1i,
		     //		     h->GetBinCenter(h->GetMaximumBin()),
		      0,
		      1E-11,
		      0,
		      1E-10);
  fit->SetParLimits(1,1E-30,100);
  fit->SetParLimits(2,0,1);
  //  fit->SetParLimits(3,1E-30,Range1f);
  fit->FixParameter(4,0);
  fit->FixParameter(5,SPEguess);
  fit->FixParameter(6,0);
  fit->FixParameter(7,1E-10);
  //  fit->SetParLimits(6,0,1);
  //  fit->SetParLimits(7,1E-30,1E-10);
  //  h->Fit(fit,"MB","",Range2i,Range1f);
  //  h->Fit(fit,"BQ","",Range1i,Range1f);
  //  h->Fit(fit,"MBQ","",Range1i,Range1f);
  h->Fit(fit,"BQ","",0,Range1f);
  h->Fit(fit,"MBQ","",0,Range1f);

  //Fitting only SPE peak with noise peak fixed from last fit.
  fit->SetParLimits(1,1E-30,100);
  fit->SetParLimits(2,0,1);
  fit->FixParameter(3,fit->GetParameter(3));
  fit->SetParLimits(4,0,1);
  fit->SetParameter(4,0.1);
  fit->SetParLimits(5,1E-30,1E-10);
  //fit->SetParLimits(6,0,1);
  //  fit->SetParLimits(7,1E-30,1E-10);
  h->Fit(fit,"BQ","",Range1f,2*SPEguess);
  h->Fit(fit,"MBQ","",Range1f,2*SPEguess);


  //Fit tail
  fit->SetParLimits(1,1E-30,100);
  fit->SetParLimits(2,0,1);
  fit->FixParameter(3,fit->GetParameter(3));
  fit->SetParLimits(4,0,1);
  //  fit->SetParLimits(5,1E-30,1E-10);
  fit->FixParameter(5,fit->GetParameter(5));
  fit->SetParLimits(6,0,1);
  fit->SetParameter(6,0.1);
  fit->SetParLimits(7,1E-30,1E-10);
  h->Fit(fit,"BQ","",0,45E-12);
  h->Fit(fit,"MBQ","",0,45E-12);
  //  h->Fit(fit,"MBQ","",Range1i,2*SPEguess);



  //Fit with everything free but SPE;
  fit->SetParLimits(0,0,fit->GetParameter(0)*100);
  fit->SetParLimits(1,1E-30,100);
  fit->SetParLimits(2,0,1);
  fit->SetParLimits(3,1E-30,1E-10);
  fit->SetParLimits(4,0,1);
  //  fit->SetParLimits(5,1E-30,1E-10);
  fit->FixParameter(5,fit->GetParameter(5));
  fit->SetParLimits(6,0,1);
  fit->SetParLimits(7,1E-30,1E-10);
  //h->Fit(fit,"MB","",0,15E-12);
  h->Fit(fit,"BQ","",Range1i,2*SPEguess - Range1f);
  h->Fit(fit,"MBQ","",Range1i,2*SPEguess - Range1f);


    //Fit with everything free;
  fit->SetParLimits(0,0,fit->GetParameter(0)*100);
  fit->SetParLimits(1,1E-30,100);
  fit->SetParLimits(2,0,1);
  fit->SetParLimits(3,1E-30,1E-10);
  fit->SetParLimits(4,0,1);
  fit->SetParLimits(5,1E-30,1E-10);
  //fit->FixParameter(5,fit->GetParameter(5));
  fit->SetParLimits(6,0,1);
  fit->SetParLimits(7,1E-30,1E-10);
  //  if(Range1i_bin != 1)
  h->Fit(fit,"BQ","",0,15E-12);
  h->Fit(fit,"MBQ","",0,15E-12);
    //  else
    //    h->Fit(fit,"MBQ","",(Range1i+Range1f)/2.0,2*SPEguess);
  return(fit);

}
