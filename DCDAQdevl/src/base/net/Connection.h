#ifndef __CONNECTION__
#define __CONNECTION__

#include <string>
#include <DCNetThread.h>

//Structure to hold connection info
class Connection
{ 
 public:
  Connection(){badFD = -1;Clear();};
  ~Connection();
  void Clear();
  bool Setup(xmlNode * SetupNode);
  //Start trying to connect to a remote server
  bool AsyncConnectStart(int type = SOCK_STREAM,int protocol = 0);
  //Check if the remote server attempt worked. 
  bool AsyncConnectCheck();
  bool SetupDCNetThread(xmlNode * managerNode);

  std::string           localIP;
  std::string           remoteIP;
  uint32_t              port;
  //Cast as what you need ie struct sockaddr_in, struct sockaddr_in6, struct sockaddr_un based on SocketFamily
  struct sockaddr_in    addrLocal;
  struct sockaddr_in    addrRemote;
  int                   fdSocket;
  DCNetThread *         thread;
  int                   retryCountDown;
  bool                  retry;
  
  struct in_addr GetRemoteRouteAddr(){return(addrRemote.sin_addr);};


  //SetRetry() and Retry() are used to wait a DCThread timeout
  //before trying another connection.  The countdown is 2 to 
  //account for the checking of ProcessTimeout() after calling
  //MainLoop() in DCThread
  void SetRetry(int countdown = 2);
  bool Retry();
  //Stops and deletes netthread.  Closes network socket.
  void Shutdown();
 private:
  int badFD;
  void PrintError(const char * str){fprintf(stderr,"Connection error: %s\n",str);};
  void PrintWarning(const char * str){printf("Connection warning: %s\n",str);};
  void DestructiveCopy(Connection & obj);
  //Do not impliment these.
  Connection(Connection & obj);
  Connection & operator=(Connection & rhs);
};

#endif
