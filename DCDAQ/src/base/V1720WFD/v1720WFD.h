#ifndef __V1720__
#define __V1720__

#include <stdint.h> //uint32_t

#include <CAENVMElib.h> //Communication functions for CONET/VME.  CAEN Errors.
#include <v1720WFDchannel.h> //Channel struct

#include <MemBlocks.h> //Definition of MemoryBlock structure


//xmllib2 support
#include <libxml/parser.h>
#include <libxml/tree.h>

#include <vector>

enum v1720ret 
  {
    OK = 0,
    BUFFERFULL = -1,
    NOTREADY = -2
  };

class v1720
{
 public:

  v1720();
  ~v1720();
  
  //Loads the XML config for this channel. Pass it an xml something
  int LoadXMLConfig(xmlNode * WFDNode); 
  //Start/Stop digitization.
  int Start(); //Should fail if WFD not setup!
  int Stop();
  //Force a software trigger on this WFD.
  //This will only work if software triggers are on in the trigger mask!
  int SoftwareTrigger();
  //Readout any data from the this V1720.
  int Readout(MemoryBlock &block);
  bool TwoPack();

  uint32_t GetNumberOfEvents(){return(numberOfEvents);};
  uint32_t GetGoodBERR(){return(goodBERR);};
  uint32_t GetBadBERR(){return(badBERR);};
  uint32_t GetVMEtransfers(){return(VMEtransfers);};
  uint32_t GetTransferredBytes(){return(TransferredBytes);};


 private:
  //True when Start() is ready to go.
  bool Ready;
  //Event size in 32bit words;
  uint32_t EventSize;

  uint32_t numberOfEvents;
  uint32_t goodBERR;
  uint32_t badBERR;
  uint32_t VMEtransfers;
  uint32_t TransferredBytes;


  //==============================================================================
  //Core Initialization functions.   
  //Read input XML something
  //Check written registers and generate Running XML something
  //==============================================================================
  //Setup the CONET connection to this board.
  int CommInit(xmlNode * CommNode);
  //Setup this WFD
  int BoardInit(xmlNode *BoardNode); //Pass some kind of XML stuff
  //Setup a channel
  int ChannelInit(xmlNode *ChannelNode); //Pass some kind of XML stuff


  //==============================================================================
  //Functions to help 
  //==============================================================================
  //Calculate and return the active channel mask;
  uint32_t GetChannelMask(); 
  //Given an XML node, find a subnode called Name and assign it's value to reg.
  //Different flavors for different regs.

  int a32d32r(uint32_t address,uint32_t &value);
  int a32d32w(uint32_t address,uint32_t value);
  //  int a32MBLTr(uint32_t address,char * buffer,uint32_t bufferSize,uint32_t &readSize);
  //  int a32MBLTr(uint32_t address,char * buffer,uint32_t bufferSize,uint32_t &readSize);
  int a32MBLTr(uint32_t address,uint8_t * buffer,uint32_t bufferSize,uint32_t &readSize);
  int SetRegister(uint32_t address,uint32_t value,const char * Name);


  //==============================================================================
  //Communication variables
  //==============================================================================
  //Addressing information needed to make a connection over the CAEN CONET optical network
  CVBoardTypes BdType;
  int32_t Link;
  int32_t BdNum;
  //VME address of board. Used for read/write even if over CONET
  uint32_t VMEBaseAddress;
  //Handle for this connection on the CONET network
  int32_t Handle;  //  long Handle;



  //==============================================================================
  //WFD settings
  //==============================================================================
  //vector of v1720Channel structs.  Holds each channel's register settings.
  std::vector<v1720channel> Channels;
  //V1720 VME control setup
  //  bit 07: (RORA/ROAK) interrupts (0/1)
  //  bit 06: disable rotary addressing.  FALSE
  //  bit 05: Align64
  //  bit 04: BERR
  //  bit 03: Optical interrupts
  //  bits 02-00: IRQ level
  uint32_t VMEControl; //0xef00
  //V1720 global channel configureation;
  //  bits 19-16: 0000 no suppression, 0010 ZLE, 0011 full suppression
  //  bit 11: (0/1) Pack 2.5 (disabled/enabled)
  //  bit 06: Internal trigger (+/-) logic (0/1)
  //  bit 04: Memory access.  Set to 1!
  //  bit 03: Test pattern.  Set to 0!
  //  bit 01: Trigger overlapping (enabled/disabled) (1/0)
  uint32_t ChannelsConfig; //0x8000
  //Maximum ditization buffer size and number of independent buffers
  //  Number of Buffers is (0x1 << MemConfig)
  //  Max samples in Buffer is blah!
  //  Range (0x0 - 0xA)
  uint32_t MemConfig; //0x800C
  //Number of samples saved after the trigger. (presamples = MemConfig size - Postsamples)
  uint32_t PostSamples; //0x8114
  //Mask for enabled triggers.
  //  bit 31: software triggers
  //  bit 30: external triggers
  //  bits 26-24: internal trigger coincidence level.
  //  bits 07-00: internal channel trigger bits.
  uint32_t TriggerSource; //0x810C

  //Enabled Channels mask
  //  bits 07-00:  channel enabled
  uint32_t ChannelMask; //0x8120
  //Acquisition control
  // bit 3: reject overlapping triggers
  // bit 2: acquisition run (XML should have a 0 here)
  // bits 01-00: acquisition running conditions
  uint32_t AcquisitionControl; //0x8100

  //Monitor Mode
  // 0x0 majority
  // 0x1 waveform generator
  // 0x2 reserved
  // 0x3 buffer occupancy
  // 0x4 voltage level
  uint32_t MonitorMode; //0x8144
  

  //==============================================================================
  //Firmware version (X.Y)
  //==============================================================================
  //  bits: (31-16) Revision date Y/M/DD
  //  bits: (15-08) Firmware revision X
  //  bits: (07-00) Firmware revision Y
  uint32_t ROCFPGA;
  
 
};

#endif
