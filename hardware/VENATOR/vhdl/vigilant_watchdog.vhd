library IEEE;
use IEEE.STD_LOGIC_1164.All;
use IEEE.NUMERIC_STD.All;

--watchdog that is "interrupt" driven by the DCM_LOCK
entity vigilant_watchdog is Port(
  DCM_LOCK_STATUS : in STD_LOGIC;
  WATCHDOG_CLOCK : in STD_LOGIC;
  WATCHDOG_DCM_RESET : out STD_LOGIC;
  WATCHDOG_MAIN_RESET : out STD_LOGIC);
end vigilant_watchdog;      

architecture Behavioral of vigilant_watchdog is
  -- WARNING: This will only work continuously for two years... 
  signal error_counter : unsigned(27 downto 0) := x"0000000";
  signal main_error_end_count : unsigned(27 downto 0) := x"0000000";
  signal dcm_error_end_count : unsigned(7 downto 0) := x"00";
begin
  process(WATCHDOG_CLOCK,DCM_LOCK_STATUS)
  begin
    if WATCHDOG_CLOCK'event and WATCHDOG_CLOCK = '1' then

      if error_counter >= main_error_end_count then
        -------------------------------------------------------------------------
        --If we aren't currently addressing an error, look for a new one.
        --Deal with our counters
        -------------------------------------------------------------------------
        
        -- set WATCHDOG_MAIN_RESET to 0 because there is no error
        WATCHDOG_MAIN_RESET <= '0';             

        if DCM_LOCK_STATUS = '0' then
          --we have a new error, so go into reset mode in the next clock tick
          main_error_end_count <= x"3FFFFFF";  --about 1second
          dcm_error_end_count <=  x"06";  -- about 4 62.5Mhz
                                                        -- clock ticks
        else
          --we have no errors and we aren't addressing one, so reset our counters
          error_counter <= x"0000000";
          main_error_end_count <= x"0000000";
          dcm_error_end_count <= x"00";
        end if;
      else
        -----------------------------------------------------------------------
        -- Deal with the current error
        -----------------------------------------------------------------------        
        WATCHDOG_MAIN_RESET <= '1';
        
        -- set WATCHDOG_DCM_RESET to 1 if something went wrong with the DCM
        if error_counter < dcm_error_end_count then
          WATCHDOG_DCM_RESET <= '1';
        else
          WATCHDOG_DCM_RESET <= '0';        
        end if;

        -- move our clounter forward.        
        error_counter <= error_counter + x"1";
      end if;

    end if;    
  end process;
end Behavioral;
