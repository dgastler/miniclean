#include <OutFakeWFDProc.hh>

namespace RAT 
{
  
  OutFakeWFDProc::OutFakeWFDProc() : Processor("OutFakeWFDProc")
  {
    fakeDataBuilder = NULL;
    writeSize = 0;
  }
  
  OutFakeWFDProc::~OutFakeWFDProc()
  {
    delete fakeDataBuilder;
    info << "OutFakeWFD: Wrote " << (writeSize >> 20) << " Mbytes to disk\n";
  }
  
  void OutFakeWFDProc::SetS(std::string param, std::string value)
  {
    if (param == "file") 
      {
	//Alocate a new
       	PMTID[value];
      }
    else
      {
	warn << "OutFakeWFD: unknown command " << param << " " << value << endl;	
      }
  }
  
  void OutFakeWFDProc::SetI(std::string param, int value)
  {
    if(param.find("WFD_") != std::string::npos)
      {
	param = param.substr(4,param.size()-4);
      }
    //Check to see if this string is a filename key.
    //If it is, then we are adding a WFD to this file (ID = BOARDID)    
    if(PMTID.find(param) != PMTID.end())
      {
	PMTID[param][value];
      }
    else
      {
	//Check for existing board ID
	int BoardID = atoi(param.c_str());
	//Iterators for the filenames and the WFDs
	std::map< std::string, std::map<int,std::vector<int> > >::iterator itFile;
	std::map<int,std::vector<int> >::iterator itBoardID;
	//Loop over all files looking for BoardID
	for(itFile = PMTID.begin();itFile != PMTID.end();itFile++)
	  {
	    //Search the BoardID map at each iterator.
	    if(itFile->second.find(BoardID) != itFile->second.end())
	      {
		//Set the board iterator to the found BoardID element
		itBoardID = itFile->second.find(BoardID);
		//If we find BoardID, break
		break;
	      }
	  }
	//If we actually found BoardID, then it will be less than
	//PMTID.end(), otherwise it will be PMTID.end()
	if(itFile != PMTID.end())
	  {
	    //Loop over this vector and see if this PMTID (value) has already
	    //been added.   Warn if it has.
	    for(unsigned int iVector = 0; iVector < itBoardID->second.size();iVector++)
	      {
		if(itBoardID->second[iVector] == value)
		  {
		    warn << "OutFakeWFD: " << itFile->first << ":" 
			 << itBoardID->first << ":" << value 
			 << " already defined!\n";		
		  }		
	      }
	    //Add PMTID (value) to this board's vector
	    itBoardID->second.push_back(value);		
	  }
	else
	  {
	    warn << "OutFakeWFD: unknown command " << param << " " << value << endl;
	  }
      }
  }
  
  DAQTestData * OutFakeWFDProc::SetupDataBuilder()
  {
    //Delete a old fakeDataBuilder if it exists
    if(fakeDataBuilder != NULL)
      delete fakeDataBuilder;
    //Create a new fakeDataBuilder
    fakeDataBuilder = new DAQTestData;
    //Check that allocation worked.
    if(fakeDataBuilder == NULL)
      return(NULL);

    std::map< std::string, std::map<int,std::vector<int> > >::iterator itFile;
    std::map<int,std::vector<int> >::iterator itWFD;

    //Loop over all filenames
    for(itFile = PMTID.begin(); itFile != PMTID.end();itFile++)
      {
	//Add a new DAQFile class for this filename
	DAQFile * tempDAQFile = fakeDataBuilder->AddDAQFile();
	//Check if add worked
	if(tempDAQFile == NULL)
	  return(NULL);  //Exit with failure

	std::cout << (itFile->first) + std::string(".dat") << std::endl;

	//append ".dat" to the end of each file name.
	tempDAQFile->SetFileName((itFile->first) + std::string(".dat"));
	//Get a WFD iterator from the file iterator and add new FakeWFDs to the DAQFile
	for(itWFD = itFile->second.begin();itWFD != itFile->second.end();itWFD++)
	  {	    
	    FakeWFD * tempFakeWFD = tempDAQFile->AddWFD();
	    if(tempFakeWFD == NULL)
	      return(NULL); //Exit with failure
	    //                 BoardID     PMTID list
	    tempFakeWFD->Setup(itWFD->first,itWFD->second);
	    
	    std::cout << "\t" <<  itWFD->first << std::endl;
	    for(int i = 0; i < itWFD->second.size();i++)
	      {
		std::cout << "\t\t" << itWFD->second[i] << std::endl;
	      }
	  }
      }    
    return(fakeDataBuilder);
  }
  
  Processor::Result OutFakeWFDProc::DSEvent(DS::Root *ds)
  {
    if(!fakeDataBuilder)
      {
	if(SetupDataBuilder() ==NULL)
	    Log::Die("OutFakeWFD: Error setting up the data builder class.\n");
      }
    

    //Process and write out this event
    int32_t processReturn = fakeDataBuilder->Process(ds);

    if(processReturn < 0)
	Log::Die("OutFakeWFD:  Error while parsing event\n");
    else
      writeSize += (uint32_t) processReturn;


    return Processor::OK;
  }


} // namespace RAT
