#ifdef __RATDS__

#include <FEPCRATManager.h>


int64_t FEPCRATManager::GetAssembledEvent(int64_t eventKey,
					  int32_t &index,
					  uint64_t &password)
{ 
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"FEPCRATManager::GetAssembledEvent(eventKey=%zd,index=%d,password=%zX)\n",eventKey,index,password);
#endif 
  int64_t ret = returnDCVector::BAD_MUTEX;  
  //Get the index at eventKey
  ret = AssembledEvent.GetEntry(eventKey,index,password);
  if(ret == returnDCVector::EMPTY_KEY)
    //The index was empty
    {      
      //Get a new index from the free queue
      int32_t freeIndex;
      bool gotFree = false;
      while(!(gotFree = Free.Get(freeIndex)) && 
	    !ShutdownStatus)
	{
	  //We are in condition wait on the EBPCRatmanager level
	  //locks. 
	  //Either a free will be added or we will ShutDownLockWait()
	  //will break us out of this loop
	  //A free can be added because we are using ConditionWait()!
	}
      if(gotFree)
	//We got a free
	{
	  index = freeIndex;
	  //Add time stamp
	  RATEVBlock * ptr = GetFEPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::FE_RATAssembler_START);
	  //Update entry
	  ret = AssembledEvent.UpdateEntry(eventKey,freeIndex,password); 	  
	}
      else
	//Failed to get a free
	{	  
	  index = AssembledEvent.GetBadValue();
	  //Must block to make sure everything is consistent
	  ret = AssembledEvent.ClearEntry(eventKey,password);
	  if(ret == returnDCVector::OK)
	    {
	      ret = returnDCVector::NO_FREE;
	    }
	}
    }
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"FEPCRATManager::GetAssembledEvent return: %ld\n\n",ret);
#endif 
  return ret;
}

int64_t FEPCRATManager::ReturnAssembledEvent(int32_t &index,
					     uint64_t password)
{
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"FEPCRATManager::ReturnAssembledEvent(index=%d,password=0x%zX)\n",index,password);
#endif 
  int64_t ret = returnDCVector::OK;
  //Get the block for the index we are dealing with
  RATEVBlock * block = GetFEPCBlock(index);
  if(block != NULL)
    {
      //Get key for this event
      int64_t eventKey = GetKey(block);
      int32_t tempIndex = index;  //Temp index so we don't lose ours
      //Update the entry at eventKey 
      ret = AssembledEvent.UpdateEntry(eventKey,tempIndex,password);
      if(ret == returnDCVector::OK)
	{
	  //clear the password for this event if everything worked
	  ret = AssembledEvent.ClearPassword(eventKey,password);	  
	  if(ret == returnDCVector::OK)
	    {
	      index = tempIndex;
	    }
	}
    }
  else
    {
      //The entry was empty.  Why did you call this? 
      ret = returnDCVector::EMPTY_KEY;
    }
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"\n");
#endif 
  return ret;  
}


int64_t FEPCRATManager::MoveAssembledEventToUnSentQueue(int64_t eventKey,
							uint64_t password)
{
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"FEPCRATManager::MoveAssembledEventToUnSentQueue(eventKey=%zd,password=%zX)\n",eventKey,password);
#endif 
  int64_t ret = returnDCVector::BAD_MUTEX;  
  int32_t index;
  //Get the index for this eventKey.
  ret = AssembledEvent.GetEntry(eventKey,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventKey.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVector::OK)||(ret >= 0))
    {
      //Remote this event from the AssembledEvent vector hash
      ret = AssembledEvent.ClearEntry(eventKey,password);
      if(ret == returnDCVector::OK)
	//The event has been cleared from the vector hash
	{
	  //Timestampe the event
	  RATEVBlock * ptr = GetFEPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::FE_RATAssembler_END);	  
	  //Add index to the UnSent queue
	  bool moveWorked = UnSent.Add(index);
	  if(moveWorked)
	    {
	      index = AssembledEvent.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVector::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  //We should just return the error we had
	}	
    }
  else
    {
      //Return the error we got from Get
      printf("Got error %"PRId64" from get\n",ret);
    }
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"\n");
#endif 
  return ret;
}

int64_t FEPCRATManager::MoveAssembledEventToBadQueue(int64_t eventKey,
						     uint64_t password,
						     Level::Type errorCode)
{  
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,
	  "FEPCRATManager::MoveAssembledEventToBadQueue(eventKey=%zd,password=%zX,errorCode%d)\n",
	  eventKey,password,errorCode);
#endif 
  int64_t ret = returnDCVector::BAD_MUTEX;  
  int32_t index;
  //Get the index for this eventKey.
  ret = AssembledEvent.GetEntry(eventKey,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventKey.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVector::OK)||(ret >= 0))
    {
      //Remove this event from the AssembledEvent vector hash
      ret = AssembledEvent.ClearEntry(eventKey,password);
      if(ret == returnDCVector::OK)
	//The event has been cleared from the vector hash
	{
	  //Add index to the Bad queue
	  BadEvent::sBadEvent badEvent;
	  badEvent.errorCode = errorCode;
	  badEvent.index = index;
	  badEvent.eventKey = eventKey;
	  //Add time stamp
	  RATEVBlock * ptr = GetFEPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::FE_BadEvent);
	  bool moveWorked = BadEvent.Add(badEvent);
	  if(moveWorked)
	    {
	      index = AssembledEvent.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVector::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  //We should just return the error we had
	}	
    }
  else
    {
      //Return the error we got from Get
    }
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"\n");
#endif 
  return ret;
}
#endif
