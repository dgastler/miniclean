#include <v1720Readout.h>

void v1720Readout::MainLoop()
{ 
  //Get a new block if we don't already have one
  if(IsReadReady(Mem->GetFreeFDs()[0]) &&
     (blockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Mem->GetFree(blockIndex);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = &(Mem->Memory[blockIndex]);
	  block->dataSize = 0;
	}
    }
  
  //Fill the current block with data
  if(blockIndex != FreeFullQueue::BADVALUE)
    {      
      //Readout loop
      uint32_t nWFDs = WFD.size();  //Won't change in this loop
      //Loop over all the wfds a minimum of this many times
      //before passing off the current block.
      for(uint32_t iBlockReadout = 0; (iBlockReadout < MaxReadoutsPerBlock);iBlockReadout++)
	{
	  //Check if we have the minimum amount of free space available 
	  //in the current block. 
	  if((block->allocatedSize - block->dataSize) > MinimumFreeBufferSpace)
	    {
	      //
	      for(uint32_t iWFD = 0; iWFD < nWFDs;iWFD++)
		{
		  int32_t readoutReturn = WFD[iWFD]->Readout(*block);
		  if((readoutReturn == OK)||(readoutReturn == BUFFERFULL))
		    {
		      WFDEventCount[iWFD] += WFD[iWFD]->GetNumberOfEvents();
		      WFDTimeStep_ReadoutNumber[iWFD]++;
		      WFDTimeStep_ReadoutSize[iWFD] += WFD[iWFD]->GetTransferredBytes();
		      
		      //Return block for processing.
		      if(readoutReturn == BUFFERFULL)
			{			     
			  iBlockReadout = MaxReadoutsPerBlock; 
			  iWFD = nWFDs;
			}
		    }
		  else
		    {
		      //Print error and stop DAQ
		      char * buffer = new char[100];
		      sprintf(buffer,
			      "WFD %d readout error %d",
			      iWFD,readoutReturn);			  
		      PrintError(buffer);
		      delete [] buffer;
		      
		      //Exit loop
		      iBlockReadout = MaxReadoutsPerBlock; 
		      iWFD = nWFDs;
		      if(readoutReturn == NOTREADY)
			{
			  //This is a bad error.  Stop the DAQ
			  DCMessage::Message message;
			  
			  //Shut down the DAQ
			  message.SetType(DCMessage::STOP);
			  SendMessageOut(message,true);		  
			}
		    }// readoutReturn
		}
	    }//Block size left check
	  else if(iBlockReadout == 0)
	    {
	      //This is a bad enough error to shut down the DAQ
	      PrintError("Not enough memory allocated!");
	      DCMessage::Message message;
	      
	      //Shut down the DAQ
	      message.SetType(DCMessage::STOP);
	      SendMessageOut(message,false);		  
	    }
	}
      //Pass off memory block
      Mem->AddFull(blockIndex);
    }
}

void  v1720Readout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      float TimeStep = difftime(currentTime,lastTime);
      DCMessage::TimedCount  timedCount;
      
      DCMessage::Message message;
      uint64_t nWFDs = WFD.size();
      for(uint64_t iWFD= 0; iWFD < nWFDs;iWFD++)
	{		  
	  //Update total data values
	  WFDReadoutSize[iWFD]   += WFDTimeStep_ReadoutSize[iWFD];
	  WFDReadoutNumber[iWFD] += WFDTimeStep_ReadoutNumber[iWFD];
	  
	  //Send Bytes transfered.

	  //Save value in the first 32 bits
	  sprintf(timedCount.text,"Readout size");
	  timedCount.ID = iWFD;
	  timedCount.count = WFDTimeStep_ReadoutSize[iWFD];
	  timedCount.dt = TimeStep;
	  message.SetDataStruct(timedCount);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message,false);
	  WFDTimeStep_ReadoutSize[iWFD] = 0;
	  
	  //Send Readout numbers
	  sprintf(timedCount.text,"Readout count");
	  timedCount.ID = iWFD;
	  timedCount.count = WFDTimeStep_ReadoutNumber[iWFD];
	  timedCount.dt = TimeStep;
	  message.SetDataStruct(timedCount);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message,false);
	  WFDTimeStep_ReadoutNumber[iWFD] = 0;
	  
	  //Send Event numbers
	  sprintf(timedCount.text,"Event count");
	  timedCount.ID = iWFD;
	  timedCount.count = WFDEventCount[iWFD];
	  timedCount.dt = TimeStep;
	  message.SetDataStruct(timedCount);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message,false);
	  WFDEventCount[iWFD] = 0;
	}
      
               
      //Setup next time
      lastTime = currentTime;
    }
}

