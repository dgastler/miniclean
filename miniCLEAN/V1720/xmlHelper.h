#include <libxml/parser.h>
#include <libxml/tree.h>


xmlNode * FindSubNode(xmlNode * baseNode, char * Name);
xmlNode * FindIthSubNode(xmlNode * baseNode, char * Name,int i);
int       NumberOfSubNodes(xmlNode * baseNode);
int       NumberOfNamedSubNodes(xmlNode * baseNode, char * Name);
char * GetNodeText(xmlNode * baseNode);
xmlNode * AddNewSubNode(xmlNode * baseNode,char * Name);
