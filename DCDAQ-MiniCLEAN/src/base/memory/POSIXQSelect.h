#ifndef __POSIXQSELECT__
#define __POSIXQSELECT__

#include <stdlib.h>
#include <SelectValues.h>
#include <unistd.h>
#include <fcntl.h>
#include <StackTracer.h>
#include <mqueue.h>

#define PIPE_ATTEMPTS 10;

class POSIXQSelect
{
 public:

  POSIXQSelect()
    {
      //Allocate our pipe
      int posixReturn;
      struct mq_attr buf;
      buf.mq_msgsize = 1;
      buf.mq_maxmsg = 10;
      buf.mq_flags = O_NONBLOCK;
      buf.mq_curmsgs = O_RDWR|O_NONBLOCK|O_CREAT|O_EXCL;
      //Create a unique name for this queue
      for(int i = 9; i>= 0;i--)
	{
	  snprintf(name,sizeof(name),"/DCMQ_%lu",random());
	  //posixReturn = mq_open(name,O_RDWR | O_NONBLOCK | O_CREAT | O_EXCL , 0777,&buf);
	  posixReturn = mq_open(name,buf.mq_curmsgs,0777,&buf);
	  //	  posixReturn = mq_open(name,buf.mq_curmsgs,0777,NULL);
	  if(posixReturn != -1)
	    {
	      break;
	    }
	  printf("mq_open %s failed: %s(%d)\n",name,strerror(errno),errno);
	  if(i == 0)
	    {
	      abort();
	    }
	}

      if(posixReturn == -1)           
	//something went wrong with our pipe
	{
	  printf("Warning:  Locked Queue failed to make FIFO! (error:%s)\n",
		 strerror(errno));	  
	  //Make sure the FIFOs are assigned to -1 if thing went wrong
	  FDs[0] = -1;
	  FDs[1] = -1;
	  //Cause code to fail
	  abort();
	}
      else
	{
	  FDs[0] = posixReturn;
	  FDs[1] = posixReturn;
	}
    }
  virtual ~POSIXQSelect()
    {
      //close our MQ
      if((FDs[1] != -1)||(FDs[0] != -1))
	{	
	  //unblock needed?
	  mq_unlink(name);
	  if(FDs[0] != -1)
	    mq_close(FDs[0]);   //Close the pipe
	  else
	    mq_close(FDs[1]);
	  FDs[1] = -1;     //Make sure we think it's closed
	  FDs[0] = -1;     //Make sure we think it's closed
	}
    }

  uint8_t Read()  
  {
    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"POSIXQSelect::Read()\n");
    #endif
    //Read one byte from the pipe to show that we got the message
    uint8_t readValue;
    
    //Do a read and return the read value
    int attemptsLeft = PIPE_ATTEMPTS;
    bool error = false;
    unsigned priority;
    while(mq_receive(FDs[0],(char*) &readValue,sizeof(readValue),&priority) == -1)
      {
	//If this was a blocking issue, our pipe is oddly empty and return EMPTY
	if((errno == EAGAIN) ||
	   (errno == EWOULDBLOCK))
	  {
	    readValue=SelectValues::EMPTY;
	    error = true;
	    break;
	  }
	//If we have an interrupt that prevented the read, we have attemptsLeft retry, then a FAIL
	else if(errno == EINTR)
	  {
	    if(--attemptsLeft <= 0)
	      {
		readValue=SelectValues::FAIL;
		error = true;
		break;
	      }	    
	  }
	//Anything else, just fail
	else
	  {
	    readValue=SelectValues::FAIL;
	    error = true;
	    break;
	  }
      }
    if(error)
      {
	//If there is an error
	fprintf(stderr,"Error in message queue read: %s\n",strerror(errno));
      }
    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"POSIXQSelect::Read(): returned %d\n\n",readValue);
    #endif
    return readValue;
  }
  
  uint8_t Write()
  {
    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"POSIXQSelect::Write()\n");
    #endif
    uint8_t val = SelectValues::OK;
    //Write the val into the pipe and return how it went.
    int attemptsLeft = PIPE_ATTEMPTS;
    bool error = false;
    unsigned priority = 0;
    while(mq_send(FDs[1],(char*)&val,sizeof(val),priority) == -1)
      {
	//If this was a blocking issue, our pipe is full and return FULL
	if((errno == EAGAIN) ||
	   (errno == EWOULDBLOCK))
	  {
	    val=SelectValues::FULL;
	    error = true;
	    break;
	  }
	//If we have an interrupt that prevented the read, we have attemptsLeft retry, then a FAIL
	else if(errno == EINTR)
	  {
	    if(--attemptsLeft <= 0)
	      {
		val=SelectValues::FAIL;
		error = true;
		break;
	      }	    
	  }
	//Anything else, just fail
	else
	  {
	    val=SelectValues::FAIL;
	    error = true;
	    break;
	  }
      }
    if(error)
      {
	//We had an error
	fprintf(stderr,"Error in queue FIFO write: %s\n",strerror(errno));
      }

    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"POSIXQSelect::Write(): returned %d\n",val);
    #endif
    return val;
  }
  

  const int * GetFDs() {return(FDs);}; //Return a constant pointer to the FDs
 private:
  //FAKE PIPE FDs
  int FDs[2];  
  char name[26];
};

#endif
