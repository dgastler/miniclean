#include <FEPCAssembler.h>
#include <fstream>
#include <iostream>


void FEPCAssembler::ProcessZLEWindow(RAT::DS::RawBlock * rawBlock,
				     int PMTID)
{
  //Find the offset for this PMT channel
  double offset  = detectorInfo.GetPMTOffset(PMTID);
  size_t preSamples = detectorInfo.GetPMTPreSamples(PMTID);
  size_t preSamplesUsed = 0;
  
  //Integral and baseline values (integer and double)
  double dIntegral = 0;
  uint32_t iIntegral = 0;

  double dBaseline = 0;
  double dBaselineRMS = 0;
  uint32_t iBaseline = 0;
  uint32_t iBaselineSquared = 0;

  
  //Get the parsed 12bit samples (packed in 16bit ints)
  std::vector<uint16_t> samples = rawBlock->GetSamples();
  for(size_t iSample = 0; iSample < samples.size();iSample++)
    {
      //Baseline calculation
      if(iSample < preSamples)
	{
	  iBaseline        += uint32_t(samples[iSample]);
	  iBaselineSquared += (uint32_t(samples[iSample])*
			       uint32_t(samples[iSample]));
	  preSamplesUsed++;
	}
      //Integral calculation 
      iIntegral += uint32_t(samples[iSample]);
    }
  
  //Calculate the ZLE baseline
  dBaseline = double(iBaseline)/preSamplesUsed;
  dBaselineRMS = sqrt((preSamplesUsed * iBaselineSquared) - 
		      (iBaseline * iBaseline))/(preSamplesUsed*preSamplesUsed);
  
  //Calculate the integral
  dIntegral = (double(iIntegral) - (offset*samples.size()));

  //Add this info to this rawBlock
  rawBlock->SetIntegral(dIntegral);
  rawBlock->SetWidth(samples.size());
  rawBlock->SetBaseline(dBaseline);
  rawBlock->SetBaselineRMS(dBaselineRMS);
}

int FEPCAssembler::ProcessOneEvent(v1720Event & V1720Event)
{
  int ret = 0;
  int32_t evIndex = -3;
  RATEVBlock * evBlock = NULL;
  //Now try to get the vector hash event for this event
  uint64_t password = FEPC_mem->GetBlankPassword();
  int64_t eventKey = FEPC_mem->GetKey(V1720Event,detectorInfo);
  int64_t retFEPCmem = FEPC_mem->GetAssembledEvent(eventKey,evIndex,password);
  if(retFEPCmem == returnDCVector::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    {      
      //Get our evBlock
      evBlock = FEPC_mem->GetFEPCBlock(evIndex);
      //Loop over the WFD channels.
      size_t numberOfChannels = V1720Event.GetChannelCount();

      //create new board object
      RAT::DS::Board * board = evBlock->ev->AddNewBoard();
      uint32_t boardID = V1720Event.GetBoardID();
      board->SetID(boardID);
      board->SetHeader(V1720Event.GetHeader());
      #ifdef DEBUG_ROLLOVER
      std::vector<uint32_t> headerVector = V1720Event.GetHeader();
      headerVector[2] = headerVector[2] & (~(0x1<<DEBUG_ROLLOVER));
      board->SetHeader(headerVector);
      #endif

      //Loop over channels
      for(size_t iChannel = 0; iChannel < numberOfChannels;iChannel++)
	{
	  
	  //Check if this is a channel we want to save (i.e. in detector info)
	  int  pmtid = detectorInfo.GetChannelMap(boardID,iChannel);
	  if(pmtid >=0 )
	    {
	      //Check if the V1720 digitized this channel
	      if(V1720Event.ValidChannel(iChannel))
		//Allocate a new PMT
		{
		  //Allocate a channel for this board
		  RAT::DS::Channel * channel = board->AddNewChannel();
		  
		  //Get the parsed channel structure.
		  v1720EventChannel currentParsedChannel = V1720Event.GetChannel(iChannel);
		  channel->SetInput(iChannel);
		  channel->SetID(detectorInfo.GetChannelMap(boardID,iChannel));
		  
		  //Loop over the zero suppressed windows.
		  size_t numberOfWindows = currentParsedChannel.GetWindowCount();		  
		  for(size_t iWindow = 0; iWindow < numberOfWindows;iWindow++)
		    {
		      RAT::DS::RawBlock * rawBlock = channel->AddNewRawBlock();
		      
		      //Get the current parsed window in this channel
		      v1720EventWindow currentParsedWindow = currentParsedChannel.GetWindow(iWindow);
		      //For each window show the window's time offset to the event time stamp
		      rawBlock->SetOffset(currentParsedWindow.GetTimeOffset());
		      //Set the data samples for this window
		      WindowDataVector.clear();//clears, but does not deallocate
		      uint32_t windowSize = currentParsedWindow.GetSampleCount();
		      //Unpack the samples from this window and enter them in to the RAT window
		      for(uint32_t iSample = 0;iSample < windowSize;iSample++)
			{
			  WindowDataVector.push_back(currentParsedWindow.GetSample(iSample));
			}
		      rawBlock->SetSamples(WindowDataVector);
		      
		      //Calculate the ZLE window integral for the DRPC
		      
		      ProcessZLEWindow(rawBlock, 
				       detectorInfo.GetChannelMap(boardID,iChannel));
		      
		      //Add statistics info
		      if(pmtMap.count(pmtid)==0)
			{
			  TimeStep_ZLECount.push_back(0);
			  TimeStep_Baseline.push_back(0);
			  TimeStep_BaselineRMS.push_back(0);
			  TimeStep_Integral.push_back(0);
			  TimeStep_EventCount.push_back(0);
			  pmtMap[pmtid] = TimeStep_ZLECount.size()-1;
			}
		      size_t vectorIndex = pmtMap[pmtid];
		      TimeStep_ZLECount[vectorIndex]++;
		      TimeStep_Baseline[vectorIndex] += rawBlock->GetBaseline();
		      TimeStep_BaselineRMS[vectorIndex] += rawBlock->GetBaselineRMS();
		      TimeStep_Integral[vectorIndex] += rawBlock->GetIntegral();

		    }//Window/RAW loop
		}
	      if(pmtMap.count(pmtid)==0)
		//In case there were no ZLE windows and this stuff never got set
		{
		  TimeStep_EventCount.push_back(0);
		  TimeStep_ZLECount.push_back(0);
		  TimeStep_Baseline.push_back(0);
		  TimeStep_BaselineRMS.push_back(0);
		  TimeStep_Integral.push_back(0);
		  pmtMap[pmtid] = TimeStep_EventCount.size()-1;
		}
	      size_t vectorIndex = pmtMap[pmtid];
	      TimeStep_EventCount[vectorIndex]++;
	      
	    }
	}//Channel loop

      //Push this WFD into the WFD vector
      evBlock->WFD.push_back(boardID);
      //Setup evBlock values if this is the first WFD added to the event
      if (evBlock->WFD.size() == 1)
	{
	  //This is the first event in this evblock, so we should
	  //set its status
	  evBlock->isSent = false;
	  evBlock->state = EventState::UNFINISHED;
	  #ifndef DEBUG_ROLLOVER
	  evBlock->wfdEventID = V1720Event.GetWFDEventID();
	  #endif
	  #ifdef DEBUG_ROLLOVER
	  evBlock->wfdEventID = V1720Event.GetWFDEventID() &  (~(0x1<<DEBUG_ROLLOVER));
	  #endif
	  evBlock->eventTime = V1720Event.GetWFDTimeStamp();
	}
      
      //Determine what to do with the evBLock depending on how many WFDs it has in it
      if(evBlock->WFD.size() == detectorInfo.GetWFDCount())
	{
	  //The event is full and we need to ship it off
	  evBlock->isSent = false;
	  evBlock->state = EventState::READY;
	  retFEPCmem = FEPC_mem->MoveAssembledEventToUnSentQueue(eventKey,
								 password);
	  //check for MoveEventToSendQueue errors
	  if(retFEPCmem == returnDCVector::OK)
	    {
	      ret = OK;
		}
	  else
	    {
	      //There should be no way we can anything other than
	      //OK.   If we do, then someone isn't following the
	      //standard rules and we should fail.
	      char * buffer = new char[100];
	      sprintf(buffer,"MoveEventToSendQueue returned %"PRId64"\n",retFEPCmem);
	      PrintError(buffer);
	      delete [] buffer;
	      ret = FATAL_ERROR;
	    }
	}
      else if(evBlock->WFD.size() > 0)
	{
	  //Put back the event.
	  ret = OK;	  
	  retFEPCmem = FEPC_mem->ReturnAssembledEvent(evIndex,password);
	}
      else if (evBlock->WFD.size() > detectorInfo.GetWFDCount())
	{	      
	  //This should never happen, but if it does we
	  //have something very strange going on and we'll
	  //pass it on to the Badqueue
	  char * buffer = new char[100];
	  sprintf(buffer,"WFD count(%zu) exceeds maxWFDCount(%zu)!\n" ,evBlock->WFD.size(),detectorInfo.GetWFDCount());
	  PrintWarning(buffer);
	  delete [] buffer;
	  evBlock->state = EventState::TOO_MANY_WFDS;
	  retFEPCmem = FEPC_mem->MoveAssembledEventToBadQueue(
							      FEPC_mem->GetKey(evBlock),
							      password,
							      Level::TOO_MANY_WFDS);
	  if(retFEPCmem == returnDCVector::OK)	
	    {
	      //We successfully moved the bad event to the bad event queue
	      //We will not process this event now, but wait for the
	      //clean up loop to catch it. 
	      ret = WFD_NOT_PROCESSED;
	    }
	  else
	    {
	      //There should be no way we can anything other than
	      //OK.   If we do, then someone isn't following the
	      //standard rules and we should fail.
	      char * buffer = new char[100];
	      sprintf(buffer,"MoveEventToBadQueue (maxEventWFDCount %zu:%zu) returned %"PRId64"\n",
		      evBlock->WFD.size(),
		      detectorInfo.GetWFDCount(),
		      retFEPCmem
		      );
	      //PrintError(buffer);
	      delete [] buffer;
	      ret = FATAL_ERROR; 
	    }  
	}
    }

 else if( retFEPCmem == returnDCVector::BAD_PASSWORD)
    {	
      ret = LOCK_BLOCKED;
    }
 else if( retFEPCmem == returnDCVector::NO_FREE)
    {
      ret = OUT_OF_MEMORY;
    }
 else if (retFEPCmem >= 0)
   {
     uint boardCount = evBlock->WFD.size();
     char * buffer = new char[100];
     sprintf(buffer,"Collision! %"PRId64" %"PRId64" %u\n",retFEPCmem,eventKey,boardCount);
      //      FEPC_mem->PrintStatus();
      PrintWarning(buffer);
 

      delete [] buffer;



      //The data in the vector hash is for another event. 
      //We have a lock on it though!
      //We will move this event to the bad queue
      //Action concerning this bad event will be left to
      //another thread.
      int64_t retCollision = FEPC_mem->MoveAssembledEventToBadQueue(
					retFEPCmem,
					password,
					Level::STORAGE_COLLISION);
      if(retCollision == returnDCVector::OK)	
	{
	  ret = COLLISION;
	}
      else 
	{
	  //There should be no way we can anything other than
	  //OK.   If we do, then someone isn't following the
	  //standard rules and we should fail.
	  char * buffer = new char[100];
	  sprintf(buffer,"MoveEventToBadQueue(collision) returned %"PRId64"\n",retFEPCmem);
	  PrintError(buffer);
	  delete [] buffer;
	  ret = FATAL_ERROR; 	
	}
    }
  else if(retFEPCmem == returnDCVector::BAD_MUTEX)
    {
      //This should never happen since we will block on the mutex.
      //If this happens, then DCDAQ is probably shutting down.
      ret = FATAL_ERROR; 
    } 

  return ret;


}
