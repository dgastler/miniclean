#ifndef __V1720_EVENT_CHANNEL__
#define __V1720_EVENT_CHANNEL__

#include <stdint.h> //uint32_t
#include <vector>

#include <v1720EventWindow.h>

class v1720EventChannel
{
 public:
  v1720EventChannel();
  ~v1720EventChannel();
  void Clear();

  //Get data windows for this channel
  const v1720EventWindow & GetWindow(size_t i);
  //Get count of data windows
  size_t                   GetWindowCount(){return(window.size());};
  //Get the ID for this channel
  uint8_t                  GetChannelID(){return(channelID);}
  

  //Process raw data for this channel and build window objects
  int                      ProcessChannel(uint32_t * _rawData,
					  uint32_t _size,
					  bool _ZLE ,
					  bool _twoPack,
					  uint8_t _channelID);
  
  //Get raw data for this channel
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);};
 private:
  //Parsed data and attributes
  std::vector<v1720EventWindow> window;
  uint8_t channelID;  
  bool twoPack;
  bool ZLE;

  //This class doesn't own this.
  uint32_t * rawData;
  uint32_t size;
  
  //Channel parsing functions (called by ProcessChannel)
  int ProcessZLE();
  int ProcessFULL();
};

#endif
