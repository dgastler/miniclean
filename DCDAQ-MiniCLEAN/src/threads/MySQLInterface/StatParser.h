#ifndef __STATPARSER__
#define __STATPARSER__

#include <StatMessage.h>
#include <DCMessage.h>
#include <sstream>

#include <map>
class StatParser
{
 public:
  StatParser(){Setup();}
  std::string ProcessStat(DCMessage::Message & message);
  private:  
  void Setup();
  std::map< StatMessage::Type,
    std::string (StatParser::*)(DCMessage::Message & message)> ssMap;
  std::string ProcessRate(DCMessage::Message & message);
  std::string ProcessFloat(DCMessage::Message & message);
  std::string ProcessFloatWithoutSource(DCMessage::Message & message);
  std::string ProcessFEPC(DCMessage::Message & message);
  std::string ProcessDRPC(DCMessage::Message & message);
  std::string ProcessRAT_DISK(DCMessage::Message & message);
  std::string ProcessFREE_FULL(DCMessage::Message & message);

  std::map<std::string,std::string> shortName_NameMap;
  std::map<SubID::Type,std::string> shortName_SubIDMap;
  std::map<Level::Type,std::string> shortName_LevelMap;
  std::map<StatMessage::Type,std::string> shortName_StatMap;

  std::string NameText(std::string);
  std::string SubIDText(SubID::Type type);
  std::string LevelText(Level::Type type);   
  std::string StatText(StatMessage::Type type);   
};
#endif
