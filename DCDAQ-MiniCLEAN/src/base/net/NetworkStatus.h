#ifndef __NetworkStatus__
#define __NetworkStatus__
namespace NetworkStatus
{
  enum Type
  {
    BLANK = 0,
    CONNECTED,
    DISCONNECTED,
    RETRY,
    FAILED,
    COUNT
  };
  const char * GetTypeName(enum Type type); 
  enum Type GetTypeFromName(const char * name);
};
#endif
