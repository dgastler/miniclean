#include <iostream>
#include <vector>
#include <map>
#include <string>

#include <string.h> //strerror()

//DCDAQ
#include <DCThread.h>
#include <MemoryManager.h>
#include <DCLauncher.h>
#include <DCMessage.h>
#include <DCMessageProcessor.h>

//Helpers
#include <runNumberHelper.h>

volatile bool run = true;

int main(int argc, char ** argv)
{
  bool CommandLine = false;
  std::string CommandLineXMLString;

  if(argc < 2)
    {
      printf("Warning: Running without setup XML\n");
    }
  else
    {
      //Load the command line XML file
      std::string CommandLineFileName(argv[1]);
      if(!LoadFile(CommandLineFileName,CommandLineXMLString))
	{
	  fprintf(stderr,"Bad input file.\n");
	  return(0);
	}
      else
	{
	  CommandLine = true;
	}
    }


  //Hardcoded config files for the control and netcontrol threads.
  std::string ControlSetupString("<SETUP><THREADS><THREAD><TYPE>Control</TYPE><NAME>Control</NAME></THREAD></THREADS></SETUP>");
  

  //Get the current run number
  int  RunNumber;
  if(!GetRunNumber(RunNumber))
    {
      fprintf(stderr,"Run number failed.\n");
      return(0);
    }


  printf("==================================\n");
  printf("=========Setting up DCDAQ=========\n");
  printf("==================================\n");

 
  //Create the launcher for DCDAQ threads and memory managers
  DCLauncher * Launcher = new DCLauncher;
  //Create the message processor for DCDAQ
  DCMessageProcessor * MessageProcessor = new DCMessageProcessor(Launcher);

  DCMessage::Message message;
  //Start the user interface
  message.SetData(ControlSetupString.c_str(),ControlSetupString.size());
  message.SetType(DCMessage::LAUNCH);
  MessageProcessor->ProcessMessage(message);
  //load the command line requested objects
  if(CommandLine)
    {
      message.SetData(CommandLineXMLString.c_str(),CommandLineXMLString.size());
      message.SetType(DCMessage::LAUNCH);
      MessageProcessor->ProcessMessage(message);
    }
  
  //Set the run number
  message.SetData(&RunNumber,sizeof(RunNumber));
  message.SetType(DCMessage::RUN_NUMBER);
  MessageProcessor->ProcessMessage(message);


  //Setup select for main DCDAQ loop
  long MaxSleepTime = 600;// 10 minutes
  struct timeval SleepTime;
  SleepTime.tv_sec= MaxSleepTime; 
  SleepTime.tv_usec= 0;   
  int MaxFDPlusOne = 0;
  //FD sets for select. 
  fd_set ReadSet,WriteSet,ExceptionSet;   

  printf("===================================\n");
  printf("===========DCDAQ Running===========\n");
  printf("===================================\n");

  //Main loop
  while(run)
    {
      //Wait for a thread message
      SleepTime.tv_sec= MaxSleepTime; 
      SleepTime.tv_usec= 0;
      //Get the fd sets for the running threads
      Launcher->GetFDSets(ReadSet,
			  WriteSet,
			  ExceptionSet,
			  MaxFDPlusOne);
      //Block in select
      int selectReturn = select(MaxFDPlusOne,
				&ReadSet,
				&WriteSet,
				&ExceptionSet,
				&SleepTime);
      
      if(selectReturn > 0)
	{
	  run = MessageProcessor->ProcessMessage(selectReturn,
						 ReadSet,
						 WriteSet,
						 ExceptionSet);
	}
      //Note, selectReturn = 0 just means it timed out.  
      //We dont' really care about that, so we don't catch it in this
      //if statement.    select return negative means a real error
      else if(selectReturn < 0)
	{
	  printf("Error:     DCDAQ select had error %d: %s\n",errno,strerror(errno));
	}
    }
  
  //Shutdown and clean up all of the memory managers and threads.
  Launcher->Shutdown();
  return(0);
}
