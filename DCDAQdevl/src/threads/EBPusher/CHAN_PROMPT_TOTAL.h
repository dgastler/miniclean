#ifndef __CHAN_PROMPT_TOTAL__
#define __CHAN_PROMPT_TOTAL__

#include <PacketBuilder.h>
class CHAN_PROMPT_TOTAL : public PacketBuilder
{
 public:
  CHAN_PROMPT_TOTAL(uint32_t _eventIntegrationStartTime,
		    uint32_t _eventIntegrationEndTime,
		    uint32_t _promptIntegrationEndTime,
		    std::vector<double>    _baseline)
  {
    ClearPacket();
    packetTypeFull=DCNetPacket::EB_PROMPT_TOTAL_FULL_PACKET;
    packetTypePartial=DCNetPacket::EB_PROMPT_TOTAL_PARTIAL_PACKET;
    
    eventIntegrationStartTime = _eventIntegrationStartTime;
    eventIntegrationEndTime = _eventIntegrationEndTime;
    promptIntegrationEndTime = _promptIntegrationEndTime;
    baseline = _baseline;

  };
  virtual ~CHAN_PROMPT_TOTAL(){ClearPacket();};
  
protected:
  virtual uint32_t CalculatePMTSize(RAT::DS::Raw * raw);
  virtual int AddData(RAT::DS::Raw * raw);
  virtual int AddChannelHeader(RAT::DS::PMT *pmt);

  uint32_t eventIntegrationStartTime;
  uint32_t eventIntegrationEndTime;
  uint32_t promptIntegrationEndTime;
  std::vector<double> baseline;
 private:
  CHAN_PROMPT_TOTAL();
};

#endif
