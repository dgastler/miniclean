#include <cl_helper.h>
#include <iostream>
#include <Control.h>

static Control *ptr;
//initialize commandstr so size() function makes sense
static std::string commandstr="";

//Readline alternative interface callback function
void Callback(const char * command)
{
  if(command != NULL)
    {
      //make up-arrow return the last command entered
      if(command[0] != 0)
	{
	  add_history(command);
	}
      
      commandstr.assign(command);
      //check that commandstr exists and has some size
      if(IsSet())
	{
	  //check that the ptr is assigned to something
	  if(ptr!=NULL)
	    {
	      //process the command, use this ptr to access public member function ProcessCommand
	      ptr->Control::ProcessCommand(commandstr);
	    }
	}
    }
}

//sets this ptr to ptr
void SetObjectPtr(Control *_ptr)
{
  ptr=_ptr;
}

//free ptr
void ResetObjectPtr()
{
  ptr=NULL;
}

//check that commandstr contains a command
bool IsSet()
{
  return !!(commandstr.size());
}


