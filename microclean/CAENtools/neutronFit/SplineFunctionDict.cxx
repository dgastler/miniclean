//
// File generated by rootcint at Mon Jun 30 15:57:36 2008

// Do NOT change. Changes will be lost next time file is generated
//

#include "RConfig.h"
#if !defined(R__ACCESS_IN_SYMBOL)
//Break the privacy of classes -- Disabled for the moment
#define private public
#define protected public
#endif

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;
#include "SplineFunctionDict.h"

#include "TClass.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"

// START OF SHADOWS

namespace ROOT {
   namespace Shadow {
   } // of namespace Shadow
} // of namespace ROOT
// END OF SHADOWS

namespace ROOT {
   void SplineFunction_ShowMembers(void *obj, TMemberInspector &R__insp, char *R__parent);
   static void *new_SplineFunction(void *p = 0);
   static void *newArray_SplineFunction(Long_t size, void *p);
   static void delete_SplineFunction(void *p);
   static void deleteArray_SplineFunction(void *p);
   static void destruct_SplineFunction(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SplineFunction*)
   {
      ::SplineFunction *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SplineFunction >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SplineFunction", ::SplineFunction::Class_Version(), "SplineFunction.h", 14,
                  typeid(::SplineFunction), DefineBehavior(ptr, ptr),
                  &::SplineFunction::Dictionary, isa_proxy, 4,
                  sizeof(::SplineFunction) );
      instance.SetNew(&new_SplineFunction);
      instance.SetNewArray(&newArray_SplineFunction);
      instance.SetDelete(&delete_SplineFunction);
      instance.SetDeleteArray(&deleteArray_SplineFunction);
      instance.SetDestructor(&destruct_SplineFunction);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SplineFunction*)
   {
      return GenerateInitInstanceLocal((::SplineFunction*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SplineFunction*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
TClass *SplineFunction::fgIsA = 0;  // static to hold class pointer

//______________________________________________________________________________
const char *SplineFunction::Class_Name()
{
   return "SplineFunction";
}

//______________________________________________________________________________
const char *SplineFunction::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SplineFunction*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int SplineFunction::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SplineFunction*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
void SplineFunction::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SplineFunction*)0x0)->GetClass();
}

//______________________________________________________________________________
TClass *SplineFunction::Class()
{
   if (!fgIsA) fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SplineFunction*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
void SplineFunction::Streamer(TBuffer &R__b)
{
   // Stream an object of class SplineFunction.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SplineFunction::Class(),this);
   } else {
      R__b.WriteClassBuffer(SplineFunction::Class(),this);
   }
}

//______________________________________________________________________________
void SplineFunction::ShowMembers(TMemberInspector &R__insp, char *R__parent)
{
      // Inspect the data members of an object of class SplineFunction.
      TClass *R__cl = ::SplineFunction::IsA();
      Int_t R__ncp = strlen(R__parent);
      if (R__ncp || R__cl || R__insp.IsA()) { }
      R__insp.Inspect(R__cl, R__parent, "*Spline", &Spline);
      R__insp.Inspect(R__cl, R__parent, "SplineSet", &SplineSet);
      R__insp.Inspect(R__cl, R__parent, "rMax", &rMax);
      R__insp.Inspect(R__cl, R__parent, "rMin", &rMin);
      TObject::ShowMembers(R__insp, R__parent);
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SplineFunction(void *p) {
      return  p ? new(p) ::SplineFunction : new ::SplineFunction;
   }
   static void *newArray_SplineFunction(Long_t nElements, void *p) {
      return p ? new(p) ::SplineFunction[nElements] : new ::SplineFunction[nElements];
   }
   // Wrapper around operator delete
   static void delete_SplineFunction(void *p) {
      delete ((::SplineFunction*)p);
   }
   static void deleteArray_SplineFunction(void *p) {
      delete [] ((::SplineFunction*)p);
   }
   static void destruct_SplineFunction(void *p) {
      typedef ::SplineFunction current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SplineFunction

/********************************************************
* SplineFunctionDict.cxx
* CAUTION: DON'T CHANGE THIS FILE. THIS FILE IS AUTOMATICALLY GENERATED
*          FROM HEADER FILES LISTED IN G__setup_cpp_environmentXXX().
*          CHANGE THOSE HEADER FILES AND REGENERATE THIS FILE.
********************************************************/

#ifdef G__MEMTEST
#undef malloc
#undef free
#endif

extern "C" void G__cpp_reset_tagtableSplineFunctionDict();

extern "C" void G__set_cpp_environmentSplineFunctionDict() {
  G__add_compiledheader("TObject.h");
  G__add_compiledheader("TMemberInspector.h");
  G__add_compiledheader("SplineFunction.h");
  G__cpp_reset_tagtableSplineFunctionDict();
}
#include <new>
extern "C" int G__cpp_dllrevSplineFunctionDict() { return(30051515); }

/*********************************************************
* Member function Interface Method
*********************************************************/

/* SplineFunction */
static int G__SplineFunctionDict_175_0_1(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
   SplineFunction* p = NULL;
   long gvp = G__getgvp();
   int n = G__getaryconstruct();
   if (n) {
     if ((gvp == G__PVOID) || (gvp == 0)) {
       p = new SplineFunction[n];
     } else {
       p = new((void*) gvp) SplineFunction[n];
     }
   } else {
     if ((gvp == G__PVOID) || (gvp == 0)) {
       p = new SplineFunction;
     } else {
       p = new((void*) gvp) SplineFunction;
     }
   }
   result7->obj.i = (long) p;
   result7->ref = (long) p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_2(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
   SplineFunction* p = NULL;
   long gvp = G__getgvp();
   //m: 4
   if ((gvp == G__PVOID) || (gvp == 0)) {
     p = new SplineFunction(
*(vector<float>*) libp->para[0].ref, *(vector<float>*) libp->para[1].ref
, (float) G__double(libp->para[2]), (float) G__double(libp->para[3]));
   } else {
     p = new((void*) gvp) SplineFunction(
*(vector<float>*) libp->para[0].ref, *(vector<float>*) libp->para[1].ref
, (float) G__double(libp->para[2]), (float) G__double(libp->para[3]));
   }
   result7->obj.i = (long) p;
   result7->ref = (long) p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_3(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      ((SplineFunction*) G__getstructoffset())->SetSpline(*(vector<float>*) libp->para[0].ref, *(vector<float>*) libp->para[1].ref
, (float) G__double(libp->para[2]), (float) G__double(libp->para[3]));
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_4(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letdouble(result7, 100, (double) ((SplineFunction*) G__getstructoffset())->operator()((double*) G__int(libp->para[0]), (double*) G__int(libp->para[1])));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_5(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 85, (long) SplineFunction::Class());
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_6(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 67, (long) SplineFunction::Class_Name());
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_7(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 115, (long) SplineFunction::Class_Version());
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_8(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      SplineFunction::Dictionary();
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_12(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      ((SplineFunction*) G__getstructoffset())->StreamerNVirtual(*(TBuffer*) libp->para[0].ref);
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_13(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 67, (long) SplineFunction::DeclFileName());
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_14(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 105, (long) SplineFunction::ImplFileLine());
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_15(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 67, (long) SplineFunction::ImplFileName());
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__SplineFunctionDict_175_0_16(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
      G__letint(result7, 105, (long) SplineFunction::DeclFileLine());
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__SplineFunctionDict_175_0_17(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)

{
   SplineFunction* p;
   void* tmp = (void*) G__int(libp->para[0]);
   p = new SplineFunction(*(SplineFunction*) tmp);
   result7->obj.i = (long) p;
   result7->ref = (long) p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef SplineFunction G__TSplineFunction;
static int G__SplineFunctionDict_175_0_18(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
   long gvp = G__getgvp();
   long soff = G__getstructoffset();
   int n = G__getaryconstruct();
   //
   //has_a_delete: 1
   //has_own_delete1arg: 0
   //has_own_delete2arg: 0
   //
   if (!soff) {
     return(1);
   }
   if (n) {
     if (gvp == G__PVOID) {
       delete[] (SplineFunction*) soff;
     } else {
       G__setgvp(G__PVOID);
       for (int i = n - 1; i >= 0; --i) {
         ((SplineFunction*) (soff+(sizeof(SplineFunction)*i)))->~G__TSplineFunction();
       }
       G__setgvp(gvp);
     }
   } else {
     if (gvp == G__PVOID) {
       delete (SplineFunction*) soff;
     } else {
       G__setgvp(G__PVOID);
       ((SplineFunction*) (soff))->~G__TSplineFunction();
       G__setgvp(gvp);
     }
   }
   G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__SplineFunctionDict_175_0_19(G__value* result7, G__CONST char* funcname, struct G__param* libp, int hash)
{
   SplineFunction* dest = (SplineFunction*) G__getstructoffset();
   *dest = *(SplineFunction*) libp->para[0].ref;
   const SplineFunction& obj = *dest;
   result7->ref = (long) (&obj);
   result7->obj.i = (long) (&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* Setting up global function */

/*********************************************************
* Member function Stub
*********************************************************/

/* SplineFunction */

/*********************************************************
* Global function Stub
*********************************************************/

/*********************************************************
* Get size of pointer to member function
*********************************************************/
class G__Sizep2memfuncSplineFunctionDict {
 public:
  G__Sizep2memfuncSplineFunctionDict(): p(&G__Sizep2memfuncSplineFunctionDict::sizep2memfunc) {}
    size_t sizep2memfunc() { return(sizeof(p)); }
  private:
    size_t (G__Sizep2memfuncSplineFunctionDict::*p)();
};

size_t G__get_sizep2memfuncSplineFunctionDict()
{
  G__Sizep2memfuncSplineFunctionDict a;
  G__setsizep2memfunc((int)a.sizep2memfunc());
  return((size_t)a.sizep2memfunc());
}


/*********************************************************
* virtual base class offset calculation interface
*********************************************************/

   /* Setting up class inheritance */

/*********************************************************
* Inheritance information setup/
*********************************************************/
extern "C" void G__cpp_setup_inheritanceSplineFunctionDict() {

   /* Setting up class inheritance */
   if(0==G__getnumbaseclass(G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction))) {
     SplineFunction *G__Lderived;
     G__Lderived=(SplineFunction*)0x1000;
     {
       TObject *G__Lpbase=(TObject*)G__Lderived;
       G__inheritance_setup(G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction),G__get_linked_tagnum(&G__SplineFunctionDictLN_TObject),(long)G__Lpbase-(long)G__Lderived,1,1);
     }
   }
}

/*********************************************************
* typedef information setup/
*********************************************************/
extern "C" void G__cpp_setup_typetableSplineFunctionDict() {

   /* Setting up typedef entry */
   G__search_typename2("Version_t",115,-1,0,-1);
   G__setnewtype(-1,"Class version identifier (short)",0);
   G__search_typename2("TVectorT<Float_t>",117,G__get_linked_tagnum(&G__SplineFunctionDictLN_TVectorTlEfloatgR),0,-1);
   G__setnewtype(-1,NULL,0);
   G__search_typename2("TVectorT<Double_t>",117,G__get_linked_tagnum(&G__SplineFunctionDictLN_TVectorTlEdoublegR),0,-1);
   G__setnewtype(-1,NULL,0);
}

/*********************************************************
* Data Member information setup/
*********************************************************/

   /* Setting up class,struct,union tag member variable */

   /* SplineFunction */
static void G__setup_memvarSplineFunction(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction));
   { SplineFunction *p; p=(SplineFunction*)0x1000; if (p) { }
   G__memvar_setup((void*)0,85,0,0,G__get_linked_tagnum(&G__SplineFunctionDictLN_TSpline3),-1,-1,4,"Spline=",0,"->");
   G__memvar_setup((void*)0,103,0,0,-1,-1,-1,4,"SplineSet=",0,(char*)NULL);
   G__memvar_setup((void*)0,100,0,0,-1,-1,-1,4,"rMax=",0,(char*)NULL);
   G__memvar_setup((void*)0,100,0,0,-1,-1,-1,4,"rMin=",0,(char*)NULL);
   G__memvar_setup((void*)0,85,0,0,G__get_linked_tagnum(&G__SplineFunctionDictLN_TClass),-1,-2,4,"fgIsA=",0,(char*)NULL);
   }
   G__tag_memvar_reset();
}

extern "C" void G__cpp_setup_memvarSplineFunctionDict() {
}
/***********************************************************
************************************************************
************************************************************
************************************************************
************************************************************
************************************************************
************************************************************
***********************************************************/

/*********************************************************
* Member function information setup for each class
*********************************************************/
static void G__setup_memfuncSplineFunction(void) {
   /* SplineFunction */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction));
   G__memfunc_setup("SplineFunction",1457,G__SplineFunctionDict_175_0_1, 105, G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction), -1, 0, 0, 1, 1, 0, "", (char*)NULL, (void*) NULL, 0);
   G__memfunc_setup("SplineFunction",1457,G__SplineFunctionDict_175_0_2, 105, G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction), -1, 0, 4, 1, 1, 0, 
"u 'vector<float,allocator<float> >' 'vector<float>' 1 - X u 'vector<float,allocator<float> >' 'vector<float>' 1 - Y "
"f - - 0 - Min_ f - - 0 - Max_", (char*)NULL, (void*) NULL, 0);
   G__memfunc_setup("SetSpline",919,G__SplineFunctionDict_175_0_3, 121, -1, -1, 0, 4, 1, 1, 0, 
"u 'vector<float,allocator<float> >' 'vector<float>' 1 - X u 'vector<float,allocator<float> >' 'vector<float>' 1 - Y "
"f - - 0 - Min_ f - - 0 - Max_", (char*)NULL, (void*) NULL, 0);
   G__memfunc_setup("operator()",957,G__SplineFunctionDict_175_0_4, 100, -1, -1, 0, 2, 1, 1, 0, 
"D - - 0 - x D - - 0 - p", (char*)NULL, (void*) NULL, 0);
   G__memfunc_setup("Class",502,G__SplineFunctionDict_175_0_5, 85, G__get_linked_tagnum(&G__SplineFunctionDictLN_TClass), -1, 0, 0, 3, 1, 0, "", (char*)NULL, (void*) (TClass* (*)())(&SplineFunction::Class), 0);
   G__memfunc_setup("Class_Name",982,G__SplineFunctionDict_175_0_6, 67, -1, -1, 0, 0, 3, 1, 1, "", (char*)NULL, (void*) (const char* (*)())(&SplineFunction::Class_Name), 0);
   G__memfunc_setup("Class_Version",1339,G__SplineFunctionDict_175_0_7, 115, -1, G__defined_typename("Version_t"), 0, 0, 3, 1, 0, "", (char*)NULL, (void*) (Version_t (*)())(&SplineFunction::Class_Version), 0);
   G__memfunc_setup("Dictionary",1046,G__SplineFunctionDict_175_0_8, 121, -1, -1, 0, 0, 3, 1, 0, "", (char*)NULL, (void*) (void (*)())(&SplineFunction::Dictionary), 0);
   G__memfunc_setup("IsA",253,(G__InterfaceMethod) NULL,85, G__get_linked_tagnum(&G__SplineFunctionDictLN_TClass), -1, 0, 0, 1, 1, 8, "", (char*)NULL, (void*) NULL, 1);
   G__memfunc_setup("ShowMembers",1132,(G__InterfaceMethod) NULL,121, -1, -1, 0, 2, 1, 1, 0, 
"u 'TMemberInspector' - 1 - insp C - - 0 - parent", (char*)NULL, (void*) NULL, 1);
   G__memfunc_setup("Streamer",835,(G__InterfaceMethod) NULL,121, -1, -1, 0, 1, 1, 1, 0, "u 'TBuffer' - 1 - b", (char*)NULL, (void*) NULL, 1);
   G__memfunc_setup("StreamerNVirtual",1656,G__SplineFunctionDict_175_0_12, 121, -1, -1, 0, 1, 1, 1, 0, "u 'TBuffer' - 1 - b", (char*)NULL, (void*) NULL, 0);
   G__memfunc_setup("DeclFileName",1145,G__SplineFunctionDict_175_0_13, 67, -1, -1, 0, 0, 3, 1, 1, "", (char*)NULL, (void*) (const char* (*)())(&SplineFunction::DeclFileName), 0);
   G__memfunc_setup("ImplFileLine",1178,G__SplineFunctionDict_175_0_14, 105, -1, -1, 0, 0, 3, 1, 0, "", (char*)NULL, (void*) (int (*)())(&SplineFunction::ImplFileLine), 0);
   G__memfunc_setup("ImplFileName",1171,G__SplineFunctionDict_175_0_15, 67, -1, -1, 0, 0, 3, 1, 1, "", (char*)NULL, (void*) (const char* (*)())(&SplineFunction::ImplFileName), 0);
   G__memfunc_setup("DeclFileLine",1152,G__SplineFunctionDict_175_0_16, 105, -1, -1, 0, 0, 3, 1, 0, "", (char*)NULL, (void*) (int (*)())(&SplineFunction::DeclFileLine), 0);
   // automatic copy constructor
   G__memfunc_setup("SplineFunction", 1457, G__SplineFunctionDict_175_0_17, (int) ('i'), G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction), -1, 0, 1, 1, 1, 0, "u 'SplineFunction' - 11 - -", (char*) NULL, (void*) NULL, 0);
   // automatic destructor
   G__memfunc_setup("~SplineFunction", 1583, G__SplineFunctionDict_175_0_18, (int) ('y'), -1, -1, 0, 0, 1, 1, 0, "", (char*) NULL, (void*) NULL, 0);
   // automatic assignment operator
   G__memfunc_setup("operator=", 937, G__SplineFunctionDict_175_0_19, (int) ('u'), G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction), -1, 1, 1, 1, 1, 0, "u 'SplineFunction' - 11 - -", (char*) NULL, (void*) NULL, 0);
   G__tag_memfunc_reset();
}


/*********************************************************
* Member function information setup
*********************************************************/
extern "C" void G__cpp_setup_memfuncSplineFunctionDict() {
}

/*********************************************************
* Global variable information setup for each class
*********************************************************/
static void G__cpp_setup_global0() {

   /* Setting up global variables */
   G__resetplocal();

}

static void G__cpp_setup_global1() {
}

static void G__cpp_setup_global2() {

   G__resetglobalenv();
}
extern "C" void G__cpp_setup_globalSplineFunctionDict() {
  G__cpp_setup_global0();
  G__cpp_setup_global1();
  G__cpp_setup_global2();
}

/*********************************************************
* Global function information setup for each class
*********************************************************/
static void G__cpp_setup_func0() {
   G__lastifuncposition();

}

static void G__cpp_setup_func1() {
}

static void G__cpp_setup_func2() {
}

static void G__cpp_setup_func3() {
}

static void G__cpp_setup_func4() {
}

static void G__cpp_setup_func5() {
}

static void G__cpp_setup_func6() {
}

static void G__cpp_setup_func7() {
}

static void G__cpp_setup_func8() {
}

static void G__cpp_setup_func9() {
}

static void G__cpp_setup_func10() {
}

static void G__cpp_setup_func11() {

   G__resetifuncposition();
}

extern "C" void G__cpp_setup_funcSplineFunctionDict() {
  G__cpp_setup_func0();
  G__cpp_setup_func1();
  G__cpp_setup_func2();
  G__cpp_setup_func3();
  G__cpp_setup_func4();
  G__cpp_setup_func5();
  G__cpp_setup_func6();
  G__cpp_setup_func7();
  G__cpp_setup_func8();
  G__cpp_setup_func9();
  G__cpp_setup_func10();
  G__cpp_setup_func11();
}

/*********************************************************
* Class,struct,union,enum tag information setup
*********************************************************/
/* Setup class/struct taginfo */
G__linked_taginfo G__SplineFunctionDictLN_TClass = { "TClass" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_TBuffer = { "TBuffer" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_TMemberInspector = { "TMemberInspector" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_TObject = { "TObject" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_TVectorTlEfloatgR = { "TVectorT<float>" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_TVectorTlEdoublegR = { "TVectorT<double>" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_TSpline3 = { "TSpline3" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_vectorlEfloatcOallocatorlEfloatgRsPgR = { "vector<float,allocator<float> >" , 99 , -1 };
G__linked_taginfo G__SplineFunctionDictLN_SplineFunction = { "SplineFunction" , 99 , -1 };

/* Reset class/struct taginfo */
extern "C" void G__cpp_reset_tagtableSplineFunctionDict() {
  G__SplineFunctionDictLN_TClass.tagnum = -1 ;
  G__SplineFunctionDictLN_TBuffer.tagnum = -1 ;
  G__SplineFunctionDictLN_TMemberInspector.tagnum = -1 ;
  G__SplineFunctionDictLN_TObject.tagnum = -1 ;
  G__SplineFunctionDictLN_TVectorTlEfloatgR.tagnum = -1 ;
  G__SplineFunctionDictLN_TVectorTlEdoublegR.tagnum = -1 ;
  G__SplineFunctionDictLN_TSpline3.tagnum = -1 ;
  G__SplineFunctionDictLN_vectorlEfloatcOallocatorlEfloatgRsPgR.tagnum = -1 ;
  G__SplineFunctionDictLN_SplineFunction.tagnum = -1 ;
}


extern "C" void G__cpp_setup_tagtableSplineFunctionDict() {

   /* Setting up class,struct,union tag entry */
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TClass);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TBuffer);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TMemberInspector);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TObject);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TVectorTlEfloatgR);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TVectorTlEdoublegR);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_TSpline3);
   G__get_linked_tagnum_fwd(&G__SplineFunctionDictLN_vectorlEfloatcOallocatorlEfloatgRsPgR);
   G__tagtable_setup(G__get_linked_tagnum(&G__SplineFunctionDictLN_SplineFunction),sizeof(SplineFunction),-1,323840,(char*)NULL,G__setup_memvarSplineFunction,G__setup_memfuncSplineFunction);
}
extern "C" void G__cpp_setupSplineFunctionDict(void) {
  G__check_setup_version(30051515,"G__cpp_setupSplineFunctionDict()");
  G__set_cpp_environmentSplineFunctionDict();
  G__cpp_setup_tagtableSplineFunctionDict();

  G__cpp_setup_inheritanceSplineFunctionDict();

  G__cpp_setup_typetableSplineFunctionDict();

  G__cpp_setup_memvarSplineFunctionDict();

  G__cpp_setup_memfuncSplineFunctionDict();
  G__cpp_setup_globalSplineFunctionDict();
  G__cpp_setup_funcSplineFunctionDict();

   if(0==G__getsizep2memfunc()) G__get_sizep2memfuncSplineFunctionDict();
  return;
}
class G__cpp_setup_initSplineFunctionDict {
  public:
    G__cpp_setup_initSplineFunctionDict() { G__add_setup_func("SplineFunctionDict",(G__incsetup)(&G__cpp_setupSplineFunctionDict)); G__call_setup_funcs(); }
   ~G__cpp_setup_initSplineFunctionDict() { G__remove_setup_func("SplineFunctionDict"); }
};
G__cpp_setup_initSplineFunctionDict G__cpp_setup_initializerSplineFunctionDict;

