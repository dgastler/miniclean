#ifndef __DRPCBLOCK__
#define __DRPCBLOCK__

#include <vector>
#include "RAT/DS/EV.hh"
#include <ReductionLevel.h>
#include <EventState.h>

struct DRPCBlock
{   
  //Pointer to ev structure
  RAT::DS::EV * ev;
  //Reduction level of event (ReductionLevel.h)
  int8_t reductionLevel;
  //vector of PMTs with data
  std::vector<uint16_t> PMT;
  //status of this event in the DRPC
  int8_t state;
  int64_t eventID;
  DRPCBlock(){ev = NULL;Clear();};
  void Clear()
  {
    state = EventState::UNFINISHED;
    eventID = -1;
    reductionLevel = ReductionLevel::UNKNOWN;
    PMT.clear();
    if(ev != NULL)      
      ev->PrunePMT();
  };
  ~DRPCBlock()
  {
  }
};
#endif
