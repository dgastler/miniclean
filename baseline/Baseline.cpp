#include <RAT/DSReader.hh>
#include <TH1F.h>
#include <TFile.h>
#include <TCanvas.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>

double ComputeBaseline(RAT::DS::RawBlock * raw, bool zle_mode, int sample_parameter)
{
  double baseline = 0;
  std::vector<uint16_t> & sample = raw->GetSamples();
  
  if(zle_mode)
    {
      //ZLE mode
      for(size_t i = 0; i < sample_parameter; i++)
	{
	  baseline += sample[i];
	}
	return baseline/sample_parameter;
    }
  else
    {
      //Full waveform mode
      for(size_t i = sample_parameter; i < sample.size();i++)
	{
	  baseline += sample[i];
	}
      return baseline/(sample.size()-sample_parameter);
    }

  
}


int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      printf("Usage: %s rootfile\n",argv[0]);
      return -1;
    }

  int first_file = 1;
  bool zle_mode = false;
  bool dac_mode = false;
  int dac_initial = 0;
  int target_baseline = 4000;
  int Thresh[120] = {0};
  int ZLEThresh[120] = {0};
  int sample_parameter = 0; 
  int wfdCount = 0;
  int pmtCount = 0;
  std::vector<int> skippedChannels; //List of channels we had to skip
  bool skipThisChannel = false;

  for(int argi = 1; argi < argc; argi++)
    {
      if(strcmp(argv[argi],"-f") == 0)
	{
	  zle_mode = false;
	  //In full mode, sample_parameter is the number of samples to skip in each waveform.
	  sample_parameter = atoi(argv[argi+1]);
	  if(sample_parameter > 0)
	    first_file += 2;
	  else
	    {
	      //No valid number of samples to skip was given
	      sample_parameter = 0; //Default value
	      first_file += 1;
	    }
	}
      else if(strcmp(argv[argi],"-z") == 0)
	{
	  zle_mode = true;
	  //In ZLE mode, sample_parameter is the number of ZLE presamples to use.
	  sample_parameter = atoi(argv[argi+1]);
	  if(sample_parameter > 0)
	    first_file += 2;
	  else
	    {
	      //No valid number of presamples argument was given
	      sample_parameter = 2; //Default value
	      first_file += 1;
	    }
	}
      else if(strcmp(argv[argi],"-dac") ==0)
	{
	  dac_mode = true;
	  dac_initial = atoi(argv[argi+1]);
	  target_baseline = atoi(argv[argi+2]);
	  if ((dac_initial>0) & (target_baseline>0))
	    {
	      first_file += 3;
	    }
	  else
	    {
	      printf("Must supply a prior DAC value and a target baseline in DAC mode");
	      return -1;
	    }
	}
      
    }

  if(zle_mode)
    std::cerr << "ZLE mode" << std::endl;
  else
    std::cerr << "Full waveform mode" << std::endl;

  int skip_events = 500; //Number of events to skip at the beginning of the data

  //Determine total number of events
  Int_t n_events = 0;
  for(int i = first_file; i < argc; i++)
    {
      RAT::DSReader reader(argv[i]);
      n_events += reader.GetTotal();
    }
  n_events -= skip_events;
  std::cerr << n_events << " events" << std::endl;

  std::vector<int> event_number;
  std::vector< std::vector<double> > event_baseline(120,std::vector<double>(n_events));

  int DAC[120];
  double average_baseline[120];
  int WFDID[120];
  int Input[120];

  RAT::DS::Root * ds;
  TH1F ** baseline = new  TH1F * [120];
  std::stringstream ss;
  for(int i = 0; i < 120;i++)
    {      
      ss.str("");
      ss << i;
      baseline[i] = new TH1F(ss.str().c_str(),
			     ss.str().c_str(),
			     4096,0,4095);
    }

  TGraph ** channel_baseline = new TGraph * [120];
  TGraph ** channel_baseline_zeroed = new TGraph * [120];
  TGraph ** coarse_baseline = new TGraph * [120];

  int event_index = 0; //Index over all events
  int waveform_event_count = 0; //Index over ZLE_WAVEFORM events only
  int n_raw[120] = {0};

  //Loop over all events in all files and calculate the baselines
  for(int iFile = first_file; iFile < argc; iFile++)
    {
      RAT::DSReader reader(argv[iFile]);
      ds = reader.GetEvent(skip_events);
      while((ds = reader.NextEvent()))
	{
	  RAT::DS::EV * ev = ds->GetEV(0);
	  if(((event_index+1)%10000) == 0)
	    std::cerr << "Finished " << event_index+1 << " events" << std::endl;
	  //If this is the first event, set WFD count.
	  wfdCount = ev->GetBoardCount();
	  pmtCount = 8*wfdCount;
	  if(ev->GetReductionLevel()==2)
	    //Only calculate baselines for ZLE_Waveform events
	    {
	      event_number.push_back(ev->GetEventID());
	      for(size_t iBoard = 0; iBoard < ev->GetBoardCount();iBoard++)
		{
		  RAT::DS::Board * board = ev->GetBoard(iBoard);
		  for(size_t iChan = 0; iChan < board->GetChannelCount();iChan++)
		    {
		      RAT::DS::Channel * chan = board->GetChannel(iChan);
		      WFDID[chan->GetID()] = board->GetID();
		      Input[chan->GetID()] = chan->GetInput();
		      if(chan->GetID()<0)
			{
			  std::cerr << "Channel ID: " << chan->GetID() << std::endl;
			}
		      for(size_t iRaw = 0; iRaw < chan->GetRawBlockCount();iRaw++)
			{
			  
			  double raw_baseline = ComputeBaseline(chan->GetRawBlock(iRaw),zle_mode,sample_parameter);
			  event_baseline[chan->GetID()][waveform_event_count] += raw_baseline;
			  average_baseline[chan->GetID()] += raw_baseline;
			  n_raw[chan->GetID()]++;
			}
		      if(chan->GetRawBlockCount()==0)
			{
			  event_baseline[chan->GetID()][waveform_event_count] = 0;
			}
		      else
			{
			  event_baseline[chan->GetID()][waveform_event_count] 
			    = event_baseline[chan->GetID()][waveform_event_count]/chan->GetRawBlockCount();
			  baseline[chan->GetID()]->Fill(event_baseline[chan->GetID()][waveform_event_count]);
			}
		      
		    }
		}
	      waveform_event_count++;
	    }
	  event_index++;
	}   
    }
 
  std::cerr << waveform_event_count << " ZLE_WAVEFORM events processed" << std::endl;
  double *event_number_array;
  event_number_array = new double[waveform_event_count];
  for(int i = 0; i < waveform_event_count;i++)
    {
      event_number_array[i] = event_number[i];
    }
  int n_coarse_bins = int(ceil(waveform_event_count/200))+1;
  double *coarse_bin_number;
  coarse_bin_number = new double[n_coarse_bins];
  double *event_baseline_array;
  double *event_baseline_zeroed;
  double *coarse_baseline_array;
  event_baseline_array = new double[waveform_event_count];
  event_baseline_zeroed = new double[waveform_event_count];
  coarse_baseline_array = new double[n_coarse_bins];

  TFile * outRootFile = new TFile("Channel_Histograms.root","RECREATE");
  ofstream outTextFile;
  outTextFile.open("Baselines.txt");

  //Populate the baseline TGraphs and print the average baselines for each channel
  for(int i = 1; i < (pmtCount+1);i++)
    { 
      if(n_raw[i]>0)
	average_baseline[i] = average_baseline[i]/n_raw[i];
      else
	{
	  //We will have to skip this channel when we write the output file.
	  std::cerr << "No raw blocks found on channel " << i << std::endl;
	  skippedChannels.push_back(i);
	}
      int iCoarse = 0;
      coarse_bin_number[iCoarse] = 0;
      coarse_baseline_array[iCoarse] = 0;
      int coarse_denominator = 0;
      for(int j = 0; j < waveform_event_count; j++)
	{
	  event_baseline_array[j] = event_baseline[i][j];
	  event_baseline_zeroed[j] = event_baseline_array[j] - average_baseline[i];
	  if((j+1)%200 == 0)
	    {
	      coarse_bin_number[iCoarse] = coarse_bin_number[iCoarse]/200;
	      coarse_baseline_array[iCoarse] = coarse_baseline_array[iCoarse]/coarse_denominator;
	      iCoarse++;
	      coarse_baseline_array[iCoarse] = 0;
	      coarse_bin_number[iCoarse] = 0;
	      coarse_denominator = 0;
	    }
	  coarse_bin_number[iCoarse] += event_number_array[j];
	  coarse_baseline_array[iCoarse] += event_baseline_array[j];
	  if(event_baseline_array[j] > 0)
	    {
	      coarse_denominator++;
	    }
	}

      channel_baseline[i] = new TGraph(waveform_event_count,event_number_array,event_baseline_array);
      channel_baseline_zeroed[i] = new TGraph(waveform_event_count,event_number_array,event_baseline_zeroed);
      coarse_baseline[i] = new TGraph(n_coarse_bins,coarse_bin_number,coarse_baseline_array);
      // if(((i-1)%8) == 0)
      // 	{
      // 	  std::cout << std::endl;
      // 	}
      
      //Write the information for this channel to the output file, unless we are skipping it.
      for(int j=0; j<skippedChannels.size();j++)
	{
	  if(i==skippedChannels[j])
	    skipThisChannel = true;
	}
      if(!skipThisChannel)
	{
	  if(dac_mode)
	    {
	      DAC[i] = floor(dac_initial - (target_baseline - average_baseline[i])*13.8);
	      
	      Thresh[i] = target_baseline - 5;
	      ZLEThresh[i] = target_baseline - 5;
	      
	      outTextFile << std::dec << WFDID[i] << "    " << Input[i] << std::hex << "   0x" << DAC[i] << "    " << std::dec << target_baseline << "    0x" << std::hex << Thresh[i] << "    0x" << ZLEThresh[i] << std::endl;
	    }
	  else
	    {
	      outTextFile << std::dec << WFDID[i] << "    " << Input[i] << "    " << average_baseline[i] << "    0x" << std::hex << int(floor(average_baseline[i])) << std::dec << std::endl;
	    }
	  // std::cout << i-1 << " " 
	  // 		<< std::hex << -2 + int(baseline[i]->GetBinCenter(baseline[i]->GetMaximumBin())) 
	  // 		<< std::dec<< std::endl;   
	  
	  baseline[i]->Write();
	}
      skipThisChannel = false;
    }  

  outTextFile.close();

  //Draw the graphs and save
  for(int i = 1; i < (pmtCount+1); i++)
    {
      std::stringstream name;
      name << "Channel_" << i;
      TCanvas *canvas = new TCanvas(name.str().c_str(),name.str().c_str());
      channel_baseline[i]->SetMarkerColor(1);
      channel_baseline[i]->Draw("AP");
      canvas->Write();
      //canvas->SaveAs(name.str().c_str());
    }

  for(int i = 1; i < (pmtCount+1); i++)
    {
      std::stringstream name;
      name << "Coarse_Channel_" << i;
      TCanvas *canvas = new TCanvas(name.str().c_str(),name.str().c_str());
      channel_baseline[i]->SetMarkerColor(1);
      channel_baseline[i]->Draw("AP");
      coarse_baseline[i]->SetMarkerColor(2);
      coarse_baseline[i]->SetMarkerStyle(7);
      coarse_baseline[i]->Draw("P same");
      canvas->Write();
      //canvas->SaveAs(name.str().c_str());
    }

 for(int i = 0; i < wfdCount; i++)
    {
      std::stringstream name;
      name << "WFD_" << i+1;
      TCanvas *canvas = new TCanvas(name.str().c_str(),name.str().c_str());
      canvas->DrawFrame(event_number[0],-4,event_number[waveform_event_count-1],4);
      channel_baseline_zeroed[i*8+1]->SetMarkerColor(1);
      channel_baseline_zeroed[i*8+1]->Draw("P");
      for(int j = 1; j < 8; j++)
	{
	  channel_baseline_zeroed[i*8+j+1]->SetMarkerColor(j+1);
	  channel_baseline_zeroed[i*8+j+1]->Draw("P same");
	}
      canvas->Write();
      //canvas->SaveAs(name.str().c_str());
    }

  outRootFile->Write();
  outRootFile->Close();
  return 0;
}
