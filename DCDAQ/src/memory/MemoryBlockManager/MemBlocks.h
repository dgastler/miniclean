#ifndef __MEMBLOCKS__
#define __MEMBLOCKS__

#include <stdint.h>

//Structure for Memory blocks
struct MemoryBlock
{
  uint8_t * buffer;
  uint32_t allocatedSize;
  uint32_t dataSize;
};
#endif
