#include <CHAN_PROMPT_TOTAL.h>

int CHAN_PROMPT_TOTAL::AddChannelHeader(RAT::DS::PMT *pmt)
{
  ebChannel = (EBPacket::ChannelData*) currentPtr;
  ebChannel->ID = pmt->GetID();   
  ebChannel->dataBlockCount = 1;
  //move forward in our array
  currentPtr += sizeof(EBPacket::ChannelData); 
  //update the free space available. 
  dataSizeLeft -= sizeof(EBPacket::ChannelData); 
  return 0;
}

uint32_t CHAN_PROMPT_TOTAL::CalculatePMTSize(RAT::DS::Raw * raw)
{
  uint32_t channelSize = (sizeof(EBPacket::ChannelData) + 
			  sizeof(EBPacket::PromptTotalBlock));          
  return(channelSize);
}

int CHAN_PROMPT_TOTAL::AddData(RAT::DS::Raw * raw)
{
  //Calculate the prompt and total integrals for this waveform
  float promptIntegral = 0;
  double totalIntegral = 0;
  

  //Calculate how much space we need for this data
  int rawWaveformCount = raw->GetRawWaveformCount();  
  //Add the raw waveforms=
  for(int iRawWaveform = 0;iRawWaveform < rawWaveformCount;iRawWaveform++)
    {
      //Get the the current RAT rawWaveform header
      RAT::DS::RawWaveform * rawWaveform = raw->GetRawWaveform(iRawWaveform);

      std::vector<uint16_t> waveform = rawWaveform->GetSamples();
      uint32_t sampleTime = rawWaveform->GetTimeOffset();
      
      for(unsigned int iSample = 0; iSample < waveform.size();iSample++)
	{
	  //If we are in the valid event time of the waveform
	  if((sampleTime > eventIntegrationStartTime) &&
	     (sampleTime < eventIntegrationEndTime))	    
	    {
	      float sample = baseline[ebChannel->ID] - waveform[iSample];
		//Add charge to prompt integral if in prompt time
		if(sampleTime < promptIntegrationEndTime)
		  {
		    promptIntegral+=sample;
		  }
	      //Add charge to total integral
	      totalIntegral+=sample;
	    }
	}
    }

  //available spot in the packet array
  EBPacket::PromptTotalBlock * ebPromptTotalBlock = (EBPacket::PromptTotalBlock *) currentPtr;
  
  //Copy data
  ebPromptTotalBlock->prompt  = uint16_t(promptIntegral);
  ebPromptTotalBlock->total   = uint32_t(totalIntegral);

  //move forward in our array
  currentPtr += sizeof(EBPacket::PromptTotalBlock); 
  //update the free space available. 
  dataSizeLeft -= sizeof(EBPacket::PromptTotalBlock); 

  return 0;
}

