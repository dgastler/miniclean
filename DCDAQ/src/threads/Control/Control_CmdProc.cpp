#include <Control.h>

bool Control::Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager)
{
  Ready = true;

  //Add Commands.
  //==========================================
  //List all commands
  //==========================================
  //ECHO
  Commands.push_back(std::string("echo"));
  CommandFunctions.push_back(&Control::Echo);

  //SENDOUT
  Commands.push_back(std::string("sendout"));
  CommandFunctions.push_back(&Control::SendOut);
		     
  //HELP
  Commands.push_back(std::string("help"));
  CommandFunctions.push_back(&Control::Help);
  //==========================================
  //Tell DCDAQ to shut down all threads and clean up
  //==========================================
  //QUIT
  Commands.push_back(std::string("quit"));
  CommandFunctions.push_back(&Control::Quit);
  //STOP
  Commands.push_back(std::string("stop"));
  CommandFunctions.push_back(&Control::Quit);
  //EXIT
  Commands.push_back(std::string("exit"));
  CommandFunctions.push_back(&Control::Quit);

  //==========================================
  //Tell DCDAQ to startup all threads
  //==========================================
  //START
  Commands.push_back(std::string("start"));
  CommandFunctions.push_back(&Control::Start);
  //GO
  Commands.push_back(std::string("go"));
  CommandFunctions.push_back(&Control::Start);
  //BEGIN
  Commands.push_back(std::string("begin"));
  CommandFunctions.push_back(&Control::Start);

  //==========================================
  //Tell DCDAQ to pause all threads
  //==========================================
  //PAUSE
  Commands.push_back(std::string("pause"));
  CommandFunctions.push_back(&Control::Pause);
  //HOLD
  Commands.push_back(std::string("hold"));
  CommandFunctions.push_back(&Control::Pause);

  //make sure all commands are lower case
  for(std::vector<std::string>::iterator it = Commands.begin();
      it != Commands.end();++it)
    {
      LowerCase(*it);
    }

  Loop= true;
  //Register the STDIN_FILENO fd with DCThread
  AddReadFD(STDIN_FILENO);
  SetupSelect();
  return(true);
}

void Control::Start(std::string arg)
{
  DCMessage::Message message;      
  message.SetType(DCMessage::GO);
  SendMessageOut(message,true);
}
void Control::Pause(std::string arg)
{
  DCMessage::Message message;      
  message.SetType(DCMessage::PAUSE);
  SendMessageOut(message,true);
}
void Control::Quit(std::string arg)
{
  DCMessage::Message message;      
  message.SetType(DCMessage::STOP);
  SendMessageOut(message,true);
}
void Control::Help(std::string arg)
{
  std::vector<std::string>::iterator it = Commands.begin();
  printf("\n Commands:\n");
  for(;it != Commands.end();it++)
    {
      printf("\t%s\n",it->c_str());
    }
}
void Control::Echo(std::string arg)
{
  printf("%s\n",arg.c_str());
}

void Control::SendOut(std::string arg)
{  
  DCMessage::Message message;
  message.SetDestination("",NULL,-2);
  if(arg.compare("stop") == 0)
    {
      message.SetType(DCMessage::STOP);
    }
  else if(arg.compare("pause") == 0)
    {
      message.SetType(DCMessage::PAUSE);
    }
  else if(arg.compare("go") == 0)
    {
      message.SetType(DCMessage::GO);
    }
  else
    {
      printf("\n Unknown option %s\n",arg.c_str());
    }    
}
