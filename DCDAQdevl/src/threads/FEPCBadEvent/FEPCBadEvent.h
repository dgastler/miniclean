#ifndef __FEPCBADEVENT__
#define __FEPCBADEVENT__
/*
  Thread description
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <FEPCRATManager.h>

#include <map>

class FEPCBadEvent : public DCThread 
{
 public:
  FEPCBadEvent(std::string Name);
  ~FEPCBadEvent(){};

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);  

  virtual void MainLoop();
 private:

  //==============================================================
  //DCThread 
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);  
  virtual void ProcessTimeout();

  //==============================================================
  //Memory manager interface
  //==============================================================
  FEPCRATManager * mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  uint32_t badEventCount;
  
  std::map<int8_t,uint32_t> stateCount;
  std::map<int32_t,uint32_t> errorCodeCount;  
};
#endif
