/*==DAQ File==============
  This class manages several FakeWFD classes and writes their
  output to disk.
==DAQ File==============*/
#ifndef __DAQFILE__
#define __DAQFILE__

//System includes
#include <string>
#include <vector>
#include <cstdio>

//Fake WFD includes
#include "FakeWFD.h"

//RAT includes
#include <RAT/DS/Root.hh>

class DAQFile
{
 public:
  DAQFile();
  ~DAQFile();

  //Filename and FILE setup
  int SetFileName(std::string name); //Sets filename and opens file. 
  std::string GetFileName(){return(filename);}

  uint32_t GetWFDCount(){return(WFDs.size());}
  FakeWFD * GetWFD(uint32_t i)
  {
    if(i < WFDs.size())
      return(WFDs[i]);
    return(NULL);
  }
  FakeWFD * AddWFD()
  {
    FakeWFD * newFakeWFD = new FakeWFD;
    WFDs.push_back(newFakeWFD);
    return(newFakeWFD);
  }
  
  //Take in a ds event and parse it.
  int Process(RAT::DS::Root * ds);
  //Write the current parsed event to disk.
  int Write();
  //If you really want to close the file yourself...
  void Close(){CloseFile();}
 private:
  std::vector<FakeWFD*> WFDs;
  FILE * outFile;
  std::string filename;
 
  bool validData;

  void CloseFile();
};
#endif
