#ifndef __MEM_BLOCK_DECOMPRESSOR__
#define __MEM_BLOCK_DECOMPRESSOR__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>
#include <RAT/DS/DCDAQ_DCTime.hh>

#include <zlib.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>


class MemBlockDeCompressor : public DCThread
{
 public:
  MemBlockDeCompressor(std::string Name);
  ~MemBlockDeCompressor()
    {
    };
  virtual bool Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  time_t last;
  time_t current;

  //Statistics
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;

  //Memory Access
  MemoryBlockManager * compressedMM;  
  MemoryBlockManager * eventMM;

  //zlib
  z_stream stream;
  int level;
};

#endif
