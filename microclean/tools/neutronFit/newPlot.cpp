#include "TTree.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TF1.h"
#include "TSpline.h"
#include "TBox.h"

#include "/home/dgastler/.rootlogon.C"

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <math.h>

// Functor to sort energy pair values
class EnergyLessThan : public std::binary_function< std::pair<double,double>, std::pair<double,double>, bool>
{
public:
  bool operator()(const std::pair<double,double> &x, const std::pair<double,double> &y) 
  {
    return (x.first < y.first);
  }
};

Double_t GetHistogramMax(TH1F * h,Double_t rangeMin,Double_t rangeMax)
{
  Int_t hMaxBin = h->FindBin(rangeMin);	  
  for(int ibin = h->FindBin(rangeMin);
      ibin < h->FindBin(rangeMax);
      ibin++)
    {
      if(h->GetBinContent(ibin) > h->GetBinContent(hMaxBin))
	{
	  hMaxBin = ibin;
	}
    }
  return(h->GetBinCenter(hMaxBin));
}

float GetAngle(std::vector<float> angles, std::vector<int> energies,int energy)
{
  float ret = -1;
  for(unsigned int i = 0; i < energies.size();i++)
    {
      if(energies[i] == energy)
	{
	  ret = angles[i];
	  break;
	}
    }
  return(ret);
}

int main(int argc, char ** argv)
{
  std::string fileEXE;
  std::string filename;
  if(argc < 2)
    {
      printf("%s filename\n",argv[0]);
      return(0);
    }
  if(argc >2)
    {
      fileEXE.assign(argv[2]);
    }
  else
    {
      fileEXE.assign(".eps");
    }
  char * Buffer = new char[100];
  rootlogon();
  gStyle->SetFillColor(0);
  //  TFile * InFile = TFile::Open("NewFit.root","READ");
  TFile * InFile = TFile::Open(argv[1],"READ");
  if(!InFile)
    {
      printf("Bad file\n");
      return(0);
    }
  TTree * T = (TTree*) InFile->Get("T");
  if(!T)
    {
      printf("Bad tree\n");
      return(0);
    }
  //Tree variables.
  Double_t chisqr;
  Double_t smearing;
  Double_t nval;
  Double_t nvalerrL;
  Double_t nvalerrH;
  Double_t nvalparabolicError;
  Double_t eval;
  Double_t evalerrL;
  Double_t evalerrH;
  Double_t evalparabolicError;
  Double_t BackgroundRatioVal;
  Double_t BackgroundRatioValErrL;
  Double_t BackgroundRatioValErrH;
  Double_t BackgroundRatioValParabolicError;
  TH1F * hData = new TH1F();
  TH1F * hMC = new TH1F();
  TH1F * hMCn = new TH1F();
  TH1F * hTOF = new TH1F();
  TSpline3 * SignalFit = new TSpline3();
  TSpline3 * BackgroundFit = new TSpline3();
  //Set branches to variables.
  T->SetBranchAddress("nval",&nval,0);
  T->SetBranchAddress("chisqr",&chisqr,0);
  T->SetBranchAddress("smearing",&smearing,0);
  T->SetBranchAddress("nvalerrL",&nvalerrL,0);
  T->SetBranchAddress("nvalerrH",&nvalerrH,0);
  T->SetBranchAddress("nvalparabolicError",&nvalparabolicError,0);
  T->SetBranchAddress("eval",&eval,0);
  T->SetBranchAddress("evalerrL",&evalerrL,0);
  T->SetBranchAddress("evalerrH",&evalerrH,0);
  T->SetBranchAddress("evalparabolicError",&evalparabolicError,0);
  T->SetBranchAddress("BackgroundRatioVal",&BackgroundRatioVal,0);
  T->SetBranchAddress("BackgroundRatioValErrL",&BackgroundRatioValErrL,0);
  T->SetBranchAddress("BackgroundRatioValErrH",&BackgroundRatioValErrH,0);
  T->SetBranchAddress("BackgroundRatioValParabolicError",&BackgroundRatioValParabolicError,0);
  T->SetBranchAddress("hData"             ,&hData);              
  T->SetBranchAddress("hMC"               ,&hMC  );              
  T->SetBranchAddress("hMCn"              ,&hMCn );              
  T->SetBranchAddress("hTOF"              ,&hTOF );              
  T->SetBranchAddress("SignalFit"         ,&SignalFit);
  T->SetBranchAddress("BackgroundFit"     ,&BackgroundFit);
  //Angle values
  std::vector<float> angles;
  angles.push_back( 22.4);
  angles.push_back( 26.3);
  angles.push_back( 30.3);
  angles.push_back( 32.9);
  angles.push_back( 36.8);
  angles.push_back( 40.0);
  angles.push_back( 43.9);
  angles.push_back( 47.2);
  angles.push_back( 51.1);
  angles.push_back( 55.0);
  angles.push_back( 58.9);
  angles.push_back( 61.5);
  angles.push_back( 64.8);
  angles.push_back( 70.0);
  angles.push_back( 73.2);
  angles.push_back( 80.4);
  angles.push_back(110.3);
  angles.push_back(114.2);
  angles.push_back(124.7);
  angles.push_back(141.6);

  //Energy values
  std::vector<int> energies;
  energies.push_back( 11);
  energies.push_back( 15);
  energies.push_back( 19);
  energies.push_back( 22);
  energies.push_back( 28);
  energies.push_back( 33);
  energies.push_back( 39);
  energies.push_back( 45);
  energies.push_back( 52);
  energies.push_back( 59);
  energies.push_back( 67);
  energies.push_back( 72);
  energies.push_back( 79);
  energies.push_back( 91);
  energies.push_back( 98);
  energies.push_back(114);
  energies.push_back(183);
  energies.push_back(191);
  energies.push_back(211);
  energies.push_back(239);


  gStyle->SetLineStyleString(11,"4 15");

  Float_t TopCornerX = 1.0 - gStyle->GetPadRightMargin();
  Float_t TopCornerY = 1.0 - gStyle->GetPadTopMargin();
  //  Float_t TextBoxLeft = 0.515;//0.475; //2011-02-15
  Float_t TextBoxLeft = 0.44;
  //  Float_t BoxFontSize = 25; // 2011-02-15
  Float_t BoxFontSize = 30;  
  Int_t   BoxFont = 83;
  Float_t BoxDelta = 0.08;  

  TCanvas * c1 = new TCanvas("c1","c1",500,500);  
  int Entries = T->GetEntries();
    
  //  TGraphAsymmErrors * gr = new TGraphAsymmErrors(Entries);
  //  TGraphAsymmErrors * grP = new TGraphAsymmErrors(Entries);
  std::map<double,TGraphAsymmErrors *> grs;
  std::map<double,TGraphAsymmErrors *> grPs;

  std::vector< std::pair<double,double> > SE;
  std::vector< std::pair<double,double> > SE_Ehigh;
  std::vector< std::pair<double,double> > SE_Elow;
  std::vector< std::pair<double,double> > SE_Epar; 
  std::vector<double> chiSquared;

  //  gr->SetName("SE");
  //  grP->SetName("SEP");

  //Functions for width fitting
  TF1 * DataGaus = new TF1("DataGaus","gaus(0)",0,180);
  DataGaus->SetNpx(10000);
  TF1 * MCGaus = new TF1("MCGaus","gaus(0)",0,180);
  MCGaus->SetNpx(10000);

  for(Int_t Entry = 0;Entry < Entries;Entry++)
    {
      T->GetEntry(Entry);

      chiSquared.push_back(chisqr);

      std::string sEnergy(hData->GetName());
      sEnergy = sEnergy.substr(0,sEnergy.find("_"));
      Double_t dEnergy = atof(sEnergy.c_str());
      Int_t iEnergy = (Int_t) (dEnergy+0.5);
      printf(" %03d SE: %05.3f (+/- %05.3f) (%+06.3f / %+06.3f)\n",
	     iEnergy,
	     eval,evalparabolicError,evalerrH,evalerrL);
      if(!grs[smearing])
	{
	  grs[smearing] = new TGraphAsymmErrors();
	}
      if(!grPs[smearing])
	{
	  grPs[smearing] = new TGraphAsymmErrors();
	}
      
      grs[smearing] ->SetPoint     (grs[smearing]->GetN(),dEnergy,eval);
      grs[smearing] ->SetPointError(grs[smearing]->GetN()-1,0,0,fabs(evalerrL),evalerrH);
      grPs[smearing]->SetPoint     (grPs[smearing]->GetN(),dEnergy,eval);
      grPs[smearing]->SetPointError(grPs[smearing]->GetN()-1,0,0,evalparabolicError,evalparabolicError);

      SE.push_back(      std::pair<double,double>(dEnergy,eval));      
      SE_Ehigh.push_back(std::pair<double,double>(dEnergy,fabs(evalerrH)));    
      SE_Elow.push_back( std::pair<double,double>(dEnergy,fabs(evalerrL)));     
      SE_Epar.push_back( std::pair<double,double>(dEnergy,evalparabolicError));     
      
//      gr->SetPoint(Entry,dEnergy,eval);
//      gr->SetPointError(Entry,0,0,fabs(evalerrL),evalerrH);
//      grP->SetPoint(Entry,dEnergy,eval);
//      grP->SetPointError(Entry,0,0,evalparabolicError,evalparabolicError);
      
      //Make Angle Plot

      Double_t PlotEnergyMax = 0;
      if(dEnergy < 25)
	PlotEnergyMax = 80;
      else if(dEnergy < 50)
	PlotEnergyMax = 100;
      else if(dEnergy < 79)
	PlotEnergyMax = 110;
      else if(dEnergy < 180)
	PlotEnergyMax = 160;
      else
	//	PlotEnergyMax = 180; //2011-02-16
	PlotEnergyMax = 220;

      PlotEnergyMax *= 0.8;

      //Make the MC histogram the total histogram
      //      hMC->Add(hMCn);
      if(BackgroundRatioVal != 0)
	{
	  hMC->Scale(BackgroundRatioVal);
	}
      else
	{
	  hMC->Scale(nval);
	}
      hMCn->Scale(nval);
      printf("Scale: %f %f\n",BackgroundRatioVal,nval);
      TH1F * hMCT = (TH1F*) hMC->Clone("hMCT");
      hMCT->Add(hMCn);
      //Histogram of non-plotted region... for plotting
      TH1F * hMCTnotPlot = (TH1F*) hMCT->Clone("hMCTnotPlot");
      //Set the correct range for the fitted part of the plot.
      for(int iBin = 1; iBin < hMCn->GetXaxis()->GetFirst();iBin++)
	{
	  hMCT->SetBinContent(iBin,0);
	}
      for(int iBin = hMCn->GetXaxis()->GetLast()+1; iBin <= hMCn->GetNbinsX();iBin++)
	{
	  hMCT->SetBinContent(iBin,0);
	}
      //set the correct range for the unfitted part of the plot.
      for(int iBin = hMCn->GetXaxis()->GetFirst();iBin < hMCn->GetXaxis()->GetLast()+1;iBin++)
	{
	  hMCTnotPlot->SetBinContent(iBin,0);
	}
      hMCTnotPlot->GetXaxis()->SetRangeUser(hData->GetXaxis()->GetXmin(),hData->GetXaxis()->GetXmax());
      Double_t PlotMaximum = 1.1 * TMath::Max(hData->GetMaximum(),hMCT->GetMaximum());

      //SetColors
      hData->SetLineColor(kBlack);
      hMCT ->SetLineColor(kRed);
      hMCT->SetLineWidth(4);
      hMCn ->SetLineColor(kBlue);
      hMCTnotPlot->SetLineColor(kRed);
      hMCTnotPlot->SetLineStyle(11);
      hMCTnotPlot->SetLineWidth(2);
      
      



      //Fit widths of DATA/MC for the last three energies.
      Double_t hMCTpeak = 0;
      Double_t hMCTpeakHeight = 0;
      std::string PlotFit("N");
      if(iEnergy > 190)
	{
	  hMCTpeak = GetHistogramMax(hMCT,40,hMCT->GetXaxis()->GetXmax());
	  hMCTpeakHeight = hMCT->GetBinContent(hMCT->FindBin(hMCTpeak));
	}
      if(iEnergy == 191)
	{	  
	  DataGaus->SetParameters(50,65,15);
	  MCGaus->SetParameters(hMCTpeakHeight,hMCTpeak,15);
	  hData->Fit(DataGaus,PlotFit.c_str(),"",45,80);
	  hMCT->Fit(MCGaus,PlotFit.c_str(),"",hMCTpeak - 15,hMCTpeak + 15);
	}
      else if(iEnergy == 211)
	{
	  DataGaus->SetParameters(50,65,15);
	  MCGaus->SetParameters(hMCTpeakHeight,hMCTpeak,15);
	  hData->Fit(DataGaus,PlotFit.c_str(),"",45,80);
	  hMCT->Fit(MCGaus,PlotFit.c_str(),"",hMCTpeak - 15,hMCTpeak + 15);
	}
      else if(iEnergy == 239)
	{
	  DataGaus->SetParameters(80,65,15);
	  MCGaus->SetParameters(hMCTpeakHeight,hMCTpeak,15);
	  hData->Fit(DataGaus,PlotFit.c_str(),"",45,80);
	  hMCT->Fit(MCGaus,PlotFit.c_str(),"",hMCTpeak - 15,hMCTpeak + 15);
	}
      if(iEnergy > 190)
	{
	  sprintf(Buffer,"widths%03d.dat",iEnergy);
	  FILE * DatFile = fopen(Buffer,"a");
	  if(DatFile)
	    {
	      fprintf(DatFile,"%03d %04.1f %03.0f %03.0f %03e %03e %03e %03e\n",
		      iEnergy,
		      smearing,
		      DataGaus->GetParameter(2),
		      MCGaus->GetParameter(2),
		      MCGaus->GetParameter(2)/DataGaus->GetParameter(2),
		      MCGaus->GetParameter(2) - DataGaus->GetParameter(2),
		      MCGaus->GetParError(2),
		      DataGaus->GetParError(2)
		      );
	      fclose(DatFile);
	    }
	}
      

      //Draw
      //New 2011-02-15
      double labelScaleFactor = 1.4;
      hMCT->GetXaxis()->SetLabelSize(labelScaleFactor*hMCT->GetXaxis()->GetLabelSize());
      hMCT->GetXaxis()->SetTitleSize(labelScaleFactor*hMCT->GetXaxis()->GetTitleSize());
      hMCT->GetXaxis()->SetNdivisions(505);
      hMCT->GetXaxis()->SetLabelOffset(labelScaleFactor*hMCT->GetXaxis()->GetLabelOffset());       //New 2011-02-22

      hMCT->GetYaxis()->SetLabelSize(labelScaleFactor*hMCT->GetYaxis()->GetLabelSize());
      hMCT->GetYaxis()->SetTitleSize(labelScaleFactor*hMCT->GetYaxis()->GetTitleSize());
      hMCT->GetYaxis()->SetNdivisions(505);
      hMCT->GetYaxis()->SetLabelOffset(3*labelScaleFactor*hMCT->GetYaxis()->GetLabelOffset());       //New 2011-02-22
      //End new

      hMCT->SetMaximum(PlotMaximum);
      hMCT->SetMinimum(0);
      hMCT->GetXaxis()->SetRangeUser(0,PlotEnergyMax);
      hMCT->GetXaxis()->SetTitle("Energy(keVee)");
      //2011-02-22hMCT->SetLineWidth(4);
      hMCT->Draw();
      hData->Draw("E SAME");
      hMCn->Draw("SAME");
      hMCTnotPlot->Draw("SAME");

      

      
      TPaveText * AngleBox = new TPaveText(TextBoxLeft,TopCornerY - BoxDelta,
					   TopCornerX,TopCornerY,"NDC");
      AngleBox->SetFillColor(kWhite);
      AngleBox->SetFillStyle(1001);
      AngleBox->SetTextFont(BoxFont);
      AngleBox->SetTextSize(BoxFontSize);
      AngleBox->SetName("bnitle");
      AngleBox->SetBorderSize(1);
      sprintf(Buffer,"%3.0f#circ recoils",GetAngle(angles,energies,iEnergy));
      AngleBox->AddText(Buffer);
      AngleBox->SetTextColor(kBlack);
      AngleBox->Draw("SAME");

      TPaveText * TitleBox = new TPaveText(TextBoxLeft,TopCornerY - 2*BoxDelta,
					   TopCornerX,TopCornerY - BoxDelta,"NDC");
      TitleBox->SetFillColor(kWhite);
      TitleBox->SetFillStyle(1001);
      TitleBox->SetTextFont(BoxFont);
      TitleBox->SetTextSize(BoxFontSize);
      TitleBox->SetName("bnitle");
      TitleBox->SetBorderSize(1);
      //      sprintf(Buffer,"% 3.0f keV recoils",dEnergy);
      sprintf(Buffer,"% 3.0f keVr",dEnergy);
      TitleBox->AddText(Buffer);
      TitleBox->SetTextColor(kBlack);
      TitleBox->Draw("SAME");

      
      TPaveText * FitBox = new TPaveText(TextBoxLeft,TopCornerY - 4*BoxDelta,
					 TopCornerX,TopCornerY - 2*BoxDelta,"NDC");
      FitBox->SetFillColor(kWhite);
      FitBox->SetFillStyle(1001);
      FitBox->SetTextFont(BoxFont);
      FitBox->SetTextSize(BoxFontSize);
      FitBox->SetName("bfittitle");
      FitBox->SetBorderSize(1);

      sprintf(Buffer,"Entries %4.0f",hData->Integral());
      FitBox->AddText(Buffer)->SetTextAlign(12);
      sprintf(Buffer,"L_{eff}  %3.2f^{%+4.2f}_{%+4.2f}",eval,evalerrH,evalerrL);
      FitBox->AddText(Buffer)->SetTextAlign(12);
      FitBox->SetTextColor(kBlack);
      FitBox->Draw("SAME");

      for(int iSignalFit = 0; iSignalFit < SignalFit->GetNp();iSignalFit++)
	{
	  double x,y,b,c,d;
	  SignalFit->GetCoeff(iSignalFit,x,y,b,c,d);
	  y *= nval;
	  b *= nval;
	  c *= nval;
	  d *= nval;
	  SignalFit->SetPoint(iSignalFit,x,y);
	  SignalFit->SetPointCoeff(iSignalFit,b,c,d);
	}
      for(int iBackgroundFit = 0; iBackgroundFit < BackgroundFit->GetNp();iBackgroundFit++)
	{
	  double x,y,b,c,d;
	  BackgroundFit->GetCoeff(iBackgroundFit,x,y,b,c,d);
	  if(BackgroundRatioVal != 0)
	    {
	      y *= BackgroundRatioVal;
	      b *= BackgroundRatioVal;
	      c *= BackgroundRatioVal;
	      d *= BackgroundRatioVal;
	    }
	  else
	    {
	      y *= nval;
	      b *= nval;
	      c *= nval;
	      d *= nval;
	    }
	  BackgroundFit->SetPoint(iBackgroundFit,x,y);
	  BackgroundFit->SetPointCoeff(iBackgroundFit,b,c,d);
	}

      //      SignalFit->Draw("SAME");
      //      BackgroundFit->Draw("SAME");
      hMCT->Draw("sameaxis");

      //Save the file.
      sprintf(Buffer,"%s_%04.1f",hData->GetName(),smearing);
      filename = std::string(Buffer) + fileEXE;
      c1->Print(filename.c_str());


      //========================================================
      //Print data/MC difference plot
      //========================================================
//      TGraphErrors * diff = new TGraphErrors();
//      for(int iBin = 0; iBin < hData->GetNbinsX();iBin++)
//	{
//	  diff->SetPoint(iBin,
//			 hData->GetBinCenter(iBin+1),
//			 hData->GetBinContent(iBin+1)-hMCT->GetBinContent(iBin+1));
//	  diff->SetPointError(iBin,0,hData->GetBinError(iBin+1));
//	}
//      diff->GetYaxis()->SetTitle("Data - MC");
//      diff->GetXaxis()->SetRangeUser(0,PlotEnergyMax);
//      c1->Clear();
//      diff->Draw("AP");
//      sprintf(Buffer,"%s_diff",hData->GetName(),smearing);
//      filename = std::string(Buffer) + fileEXE;
//      c1->Print(filename.c_str());
      //========================================================
      //END Print data/MC difference plot
      //========================================================
    }


  //Sort SE values by energy
  std::sort(SE.begin(),SE.end(),EnergyLessThan());
  std::sort(SE_Ehigh.begin(),SE_Ehigh.end(),EnergyLessThan());
  std::sort(SE_Elow.begin(),SE_Elow.begin(),EnergyLessThan());
  std::sort(SE_Epar.begin(),SE_Epar.end(),EnergyLessThan());


  c1->Clear();
  c1->SetCanvasSize(1024,768);
  c1->Range(0,0,250,0.6);
  filename = std::string("SE") + fileEXE;

  //SE plot
  //Plot all graphs with different colors
  std::map<double,TGraphAsymmErrors*>::iterator igrs = grs.begin();
  std::map<double,TGraphAsymmErrors*>::iterator igrPs = grPs.begin();

  double yMin = 0.2;
  double yMax = 0.6;

  while(igrPs != grPs.end())
    {
      (*igrPs).second->GetYaxis()->SetTitle("Scintillation efficiency");
      (*igrPs).second->GetXaxis()->SetRangeUser(0,260);
      (*igrPs).second->GetXaxis()->SetTitle("Recoil energy (keVr)");
      (*igrPs).second->SetMarkerStyle(20);
      (*igrPs).second->SetMinimum(yMin);
      (*igrPs).second->SetMaximum(yMax);
      (*igrPs).second->SetMarkerSize(0.75);
      (*igrPs).second->SetLineWidth(1.2);
      (*igrPs).second->SetLineColor(kRed);
      igrPs++;
    }
  while(igrs != grs.end())
    {
      (*igrs).second->GetYaxis()->SetTitle("Scintillation efficiency");
      (*igrs).second->GetXaxis()->SetRangeUser(0,260);
      (*igrs).second->GetXaxis()->SetTitle("Recoil energy (keVr)");
      (*igrs).second->SetMarkerStyle(20);
      (*igrs).second->SetMinimum(yMin);
      (*igrs).second->SetMaximum(yMax);
      (*igrs).second->SetMarkerSize(0.75);
      (*igrs).second->SetLineColor(kBlack);
      igrs++;
    }

  int ColorNumber = 0;

  //igrPs = grPs.begin();  
  ////Draw the first graph to set up the canvas
  //((*igrPs).second)->SetPoint(((*igrPs).second)->GetN(),0,0);
  //((*igrPs).second)->SetPointError(((*igrPs).second)->GetN()-1,0,0,0,0);
  //((*igrPs).second)->SetMarkerColor(gStyle->GetColorPalette(ColorNumber++));
  //((*igrPs).second)->Draw("AP");
  //igrPs++;
  //igrs = grs.begin();
  //while(igrPs != grPs.end())
  //  {      
  //    ((*igrPs).second)->Draw("SAME P");
  //    ((*igrPs).second)->SetMarkerColor(gStyle->GetColorPalette(ColorNumber++));
  //    igrPs++;
  //  }
  //ColorNumber = 0;
  //while(igrs != grs.end())
  //  {
  //    ((*igrs).second)->Draw("SAME P");
  //    ((*igrs).second)->SetMarkerColor(gStyle->GetColorPalette(ColorNumber++));
  //    igrs++;
  //  }

  igrs = grs.begin();  
  //Draw the first graph to set up the canvas
  ((*igrs).second)->SetPoint(((*igrs).second)->GetN(),0,0);
  ((*igrs).second)->SetPointError(((*igrs).second)->GetN()-1,0,0,0,0);
  //  ((*igrs).second)->SetMarkerColor(gStyle->GetColorPalette(ColorNumber++));
  ((*igrs).second)->SetMinimum(0); //2011-02-16
  ((*igrs).second)->Draw("AP");


  //Legend
  TLegend * SELegend = new TLegend(TextBoxLeft,TopCornerY - 3*BoxDelta,
				   TopCornerX,TopCornerY);
  SELegend->SetBorderSize(1);
  SELegend->SetFillColor(0);
  //  SELegend->SetTextSize((SELegend->GetY1NDC()-SELegend->GetY2NDC())/3.0);
  Double_t Mean = 0;
  Double_t Mean2 = 0;
  Double_t Normalization = 0;
  Double_t NMean = 0;
  Double_t CutOff = 20;
  Int_t mStart = 0;
  Int_t mEnd = SE.size();
  Int_t MaxX = 0;
  for(int i = 0; i < mEnd;i++)
    {
      if(SE[i].first > MaxX)
	{
	  MaxX = SE[i].first;
	}
	
      if((SE[i].first >= CutOff)&&(SE[i].second < 0.5)&&(SE[i].second > 0.2))
	{
	  Double_t Error = (SE_Ehigh[i].second + SE_Elow[i].second)/2.0; 
	  //weighted mean calculation.
	  Mean += SE[i].second/Error;
	  //	  Mean2 += SE[i].second*SE[i].second;
	  //Weighted number of points normalization. (also 1/mean error)
	  Normalization+=1.0/Error;
	  NMean++;
	}
    }
  //  Mean2 = Mean2 - Mean*Mean/(NMean);
  //  Mean2 /= NMean -1;
  //  Mean2 = sqrt(Mean2);
  Mean /= Normalization;
  //  printf("Mean S.E. %e +/- %e\n",Mean,Mean2);
  double MeanError = rint(100.0*NMean/Normalization)/100.0;  
  printf("Mean S.E. %e +/- %e\n",Mean,MeanError);

  TLine * MeanLine = new TLine(CutOff,
			       Mean,
			       MaxX,
			       Mean);
  MeanLine->SetLineColor(kRed);
  MeanLine->Draw("SAME");
  Double_t WSE = 0.28;
  Double_t WSEenergy = 65;
  Double_t WSEerr = 0.028;
  TGraphErrors * Warp = new TGraphErrors(1,&WSEenergy,&WSE,0,&WSEerr);
  Warp->SetMarkerStyle(23);
  Warp->SetMarkerSize(1);
  Warp->SetMarkerColor(kBlue);
  Warp->SetLineColor(kBlue);
  Warp->Draw("SAME P");



  SELegend->SetTextColor(kBlack);
  SELegend->AddEntry(((*grs.begin()).second),"micro-CLEAN","p");
  SELegend->AddEntry(Warp,"WARP","p");
  sprintf(Buffer,"Mean S.E. %3.2f +/- %3.2f",Mean,MeanError);
  SELegend->AddEntry(MeanLine,Buffer,"l");
  SELegend->Draw("SAME");


  FILE * CSVFile = fopen("SE.csv","w");


  for(igrs = grs.begin();igrs != grs.end();igrs++)
    {      

      double index = (*igrs).first;
      Int_t GraphSize = grs[index]->GetN();
      printf("Smearing: %f\n",index);
      printf("Point keVr leff   +     -   (+/-)\n");
      for(int i = 0; i < GraphSize;i++)
	{
	  printf("%02d    %03.0f  %04.2f %05.3f %05.3f %05.3f %05.3f\n",
		 i,
		 grs[index]->GetX()[i],
		 grs[index]->GetY()[i],
		 grs[index]->GetEYhigh()[i],
		 grs[index]->GetEYlow()[i],
		 grPs[index]->GetEYhigh()[i],
		 chiSquared[i]
		 );
	  
	  if(CSVFile)
	    {
	      fprintf(CSVFile,"%d,%f,%f,%f,%f,%f\n",
		      i,
		      grs[index]->GetX()[i],
		      grs[index]->GetY()[i],
		      grs[index]->GetEYhigh()[i],
		      grs[index]->GetEYlow()[i],
		      grPs[index]->GetEYhigh()[i]
		      );
	    }
	}
    }
  c1->Print(filename.c_str());
  delete [] Buffer;
  return(0);
}
