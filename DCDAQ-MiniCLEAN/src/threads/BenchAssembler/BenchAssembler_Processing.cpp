#include <BenchAssembler.h>
#include <fstream>
#include <iostream>


//==========DELETE THIS==========
//enum
//  {
//    OK,
//    NO_INPUT,
//    NO_WAVEFORM,
//    BAD_THRESHOLD
//  };
//==========DELETE THIS==========
//==========DELETE THIS==========
//int FindPulseTime(uint32_t &headerTime,  //return header time in samples
//		   uint32_t &waveformTime, //return the waveform time in samples
//		   uint16_t threshold,
//		   RAT::DS::Board * board,
//		   int input = 0)
//{
//  //Find the time offset of the header.
//  std::vector<uint32_t> header = board->GetHeader();
//  headerTime = (header[3]&0x7FFFFFFF) << 1;
//  
//  //find input
//  RAT::DS::Channel * channel = NULL;
//  for(int channelIndex = 0;
//      channelIndex < board->GetChannelCount();
//      channelIndex++)
//    {
//      if(board->GetChannel(channelIndex)->GetInput() == input)
//	{
//	  channel = board->GetChannel(channelIndex);
//	  break;
//	}
//    }
//  
//  
//  if(channel == NULL)
//    {
//      return NO_INPUT;
//    }
//
//  //Find the waveform start time.
//  if(channel->GetRawBlockCount() == 0)
//    {
//      return NO_WAVEFORM;
//    }
//  
//  //Get the first raw block to get the waveform time
//  RAT::DS::RawBlock * rawBlock = channel->GetRawBlock(0);
//  uint32_t rawBlockOffset = rawBlock->GetOffset();
//  std::vector<uint16_t> samples = rawBlock->GetSamples();
//  uint32_t rawBlockTriggerTime = samples.size();
//  for(size_t i = 0 ; i < samples.size();i++)
//    {
//      if(samples[i] < threshold)
//	{
//	  rawBlockTriggerTime = i;
//	  break;
//	}
//    }  
//  if(rawBlockTriggerTime == samples.size())
//    {
//      return BAD_THRESHOLD;
//    }
//  waveformTime = rawBlockOffset + rawBlockTriggerTime;
//  return OK;
//}
//==========DELETE THIS==========


void BenchAssembler::ProcessZLEWindow(RAT::DS::RawBlock * rawBlock,
				      int PMTID)
{
  //Find the offset for this PMT channel
  double offset  = detectorInfo.GetPMTOffset(PMTID);
  size_t preSamples = detectorInfo.GetPMTPreSamples(PMTID);
  size_t preSamplesUsed = 0;
  
  //Integral and baseline values (integer and double)
  double dIntegral = 0;
  uint32_t iIntegral = 0;

  double dBaseline = 0;
  double dBaselineRMS = 0;
  uint32_t iBaseline = 0;
  uint32_t iBaselineSquared = 0;

  
  //Get the parsed 12bit samples (packed in 16bit ints)
  std::vector<uint16_t> samples = rawBlock->GetSamples();
  for(size_t iSample = 0; iSample < samples.size();iSample++)
    {
      //Baseline calculation
      if(iSample < preSamples)
	{
	  iBaseline        += uint32_t(samples[iSample]);
	  iBaselineSquared += (uint32_t(samples[iSample])*
			       uint32_t(samples[iSample]));
	  preSamplesUsed++;
	}
      //Integral calculation 
      iIntegral += uint32_t(samples[iSample]);
    }
  
  //Calculate the ZLE baseline
  dBaseline = double(iBaseline)/preSamplesUsed;
  dBaselineRMS = sqrt((preSamplesUsed * iBaselineSquared) - 
		      (iBaseline * iBaseline))/(preSamplesUsed*preSamplesUsed);
  
  //Calculate the integral
  dIntegral = (double(iIntegral) - (offset*samples.size()))*(-1);

  //Adda new raw integral structure and fill it. 
  rawBlock->SetIntegral(dIntegral);
  rawBlock->SetWidth(samples.size());
  rawBlock->SetBaseline(dBaseline);
  rawBlock->SetBaselineRMS(dBaselineRMS);
}

int BenchAssembler::ProcessOneEvent(v1720Event & V1720Event)
{
  int ret = 0;
  int32_t evIndex;
  RATDSBlock * event = NULL;
  //Now try to get the vector hash event for this event
  uint64_t password = 0;
  int64_t eventID = V1720Event.GetWFDEventID();
  int64_t retEBPCmem = EBPC_mem->GetEvent(eventID,evIndex,password);
  if(retEBPCmem == returnDCVector::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    {      
      //Get our evBlock
      event = EBPC_mem->GetRATDiskBlock(evIndex);
      RAT::DS::EV * ev = event->ds->GetEV(0);
    
      //Loop over the WFD channels.
      size_t numberOfChannels = V1720Event.GetChannelCount();

      //create new board object
      RAT::DS::Board * board = ev->AddNewBoard();    
      uint32_t boardID = V1720Event.GetBoardID();

      board->SetID(boardID);
      board->SetHeader(V1720Event.GetHeader());


       //Loop over channels
      for(size_t iChannel = 0; iChannel < numberOfChannels;iChannel++)
	{
	
	  if(detectorInfo.GetChannelMap(boardID,iChannel) >=0 )
	    {
	      //Allocate a channel for this board
	      RAT::DS::Channel * channel = board->AddNewChannel();
	    
	      //Get the parsed channel structure.
	      v1720EventChannel currentParsedChannel = V1720Event.GetChannel(iChannel);
	      channel->SetInput(iChannel);
	      channel->SetID(detectorInfo.GetChannelMap(boardID,iChannel));	      
	      //Loop over the zero suppressed windows.
	      size_t numberOfWindows = currentParsedChannel.GetWindowCount();
	      for(size_t iWindow = 0; iWindow < numberOfWindows;iWindow++)
		{
		  RAT::DS::RawBlock * rawBlock = channel->AddNewRawBlock();
		  
		  //Get the current parsed window in this channel
		  v1720EventWindow currentParsedWindow = currentParsedChannel.GetWindow(iWindow);
		  
		  //For each window show the window's time offset to the event time stamp
		  rawBlock->SetOffset(currentParsedWindow.GetTimeOffset());
		  //Set the data samples for this window
		  WindowDataVector.clear();//clears, but does not deallocate
		  uint32_t windowSize = currentParsedWindow.GetSampleCount();
		  //Unpack the samples from this window and enter them in to the RAT window
		  for(uint32_t iSample = 0;iSample < windowSize;iSample++)
		    {
		      WindowDataVector.push_back(currentParsedWindow.GetSample(iSample));
		    }
		  rawBlock->SetSamples(WindowDataVector);
		  
		  //Calculate the ZLE window integral for the DRPC
		  ProcessZLEWindow(rawBlock, 
				   detectorInfo.GetChannelMap(boardID,iChannel));
		  
		}//Window/RAW loop
	    }
	}//Channel loop

      
      //==========DELETE THIS==========
//      uint32_t headerTime = 0;
//      uint32_t waveformTime =0;
//      FindPulseTime(headerTime,  
//		    waveformTime, 
//		    3000,
//		    board,
//		    0);
//      event->eventTimeWFD[boardID] = (int64_t(headerTime) + int64_t(waveformTime));
//      event->waveWFD[boardID] = int64_t(waveformTime);
//      event->headWFD[boardID] = int64_t(headerTime);
      //==========DELETE THIS==========

      //Push this WFD into the WFD vector
      event->WFD.push_back(boardID);      
      //Check this board's header
      if(checkHeaders>0)
	{
	  if((board->GetHeader()[1] & 0xFF00)>>8 != (eventID & 0xFF))
	    {
	      char * textBuffer = new char[100];
	      sprintf(textBuffer,"WFD header does not match event number\n Event: %"PRIu64", WFD: %u, header pattern: %u\n",
		      eventID,boardID,(board->GetHeader()[1] & 0xFF00)>>8);
	      PrintWarning(textBuffer);
	      delete [] textBuffer;
	    }
	}
      //Setup event values if this is the first WFD added to the event
      if (event->WFD.size() == 1)
	{
	  //This is the first event in this evblock, so we should
	  //set its status
	  //event->isSent = false;
	  event->state = EventState::UNFINISHED;
	  event->eventID = eventID;
	 
	  ev->SetEventID(eventID);
	}
      
      //Determine what to do with the event depending on how many WFDs it has in it
      if(event->WFD.size() == detectorInfo.GetWFDCount())
	{
	  //==========DELETE THIS==========
//	  for(std::map<uint8_t,int64_t>::iterator it = event->eventTimeWFD.begin(); 
//	      it != event->eventTimeWFD.end();
//	      it++)
//	    {
//	      if(abs(it->second - event->eventTimeWFD.begin()->second) > 1)
//		{
//		  fprintf(stderr,"Event %08lu had bad timing (%010ld - %010ld = %+02ld) dhead(%+02ld) dwave(%+02ld)\n",
//			  event->eventID,
//			  it->second,
//			  event->eventTimeWFD.begin()->second,
//			  it->second - event->eventTimeWFD.begin()->second,
//			  event->headWFD[it->first] - event->headWFD.begin()->second,
//			  event->waveWFD[it->first] - event->waveWFD.begin()->second
//			  );
//		}
//	    }
	  //==========DELETE THIS==========

	  //The event is full and we need to ship it off
	  //event->isSent = false;
	  event->state = EventState::READY;
	  retEBPCmem = EBPC_mem->MoveEventToDiskQueue(event->eventID, evIndex,
								 password);
	  //check for MoveEventToDiskQueue errors
	  if(retEBPCmem == returnDCVector::OK)
	    {
	      ret = OK;
		}
	  else
	    {
	      //There should be no way we can anything other than
	      //OK.   If we do, then someone isn't following the
	      //standard rules and we should fail.
	      char * buffer = new char[100];
# if __WORDSIZE == 64
	      sprintf(buffer,"MoveEventToDiskQueue returned %ld\n",retEBPCmem);
# else
	      sprintf(buffer,"MoveEventToDiskQueue returned %lld\n",retEBPCmem);
#endif
	      PrintError(buffer);
	      delete [] buffer;
	      ret = FATAL_ERROR;
	    }
	}
      else if(event->WFD.size() > 0)
	{
	  //Put back the event.
	  ret = OK;
	  retEBPCmem = EBPC_mem->SetEvent(eventID,evIndex,password);
	}
      else if (event->WFD.size() > detectorInfo.GetWFDCount())
	{	      
	  //This should never happen, but if it does we
	  //have something very strange going on and we'll
	  //pass it on to the Badqueue
	  char * buffer = new char[100];
# if __WORDSIZE == 64
	  sprintf(buffer,"WFD count(%lu) exceeds maxWFDCount(%lu)!\n"  ,event->WFD.size(),detectorInfo.GetWFDCount());
# else	  
	  sprintf(buffer,"WFD count(%u) exceeds maxWFDCount(%u)!\n",event->WFD.size(),detectorInfo.GetWFDCount());
# endif

	  PrintError(buffer);
	  delete [] buffer;
	  event->state = EventState::TOO_MANY_WFDS;
	  retEBPCmem = EBPC_mem->MoveEventToBadQueue(event->eventID,evIndex,password,Level::TOO_MANY_WFDS)
;
	  if(retEBPCmem == returnDCVector::OK)	
	    {
	      //We successfully moved the bad event to the bad event queue
	      //We will not process this event now, but wait for the
	      //clean up loop to catch it. 
	      ret = WFD_NOT_PROCESSED;
	    }
	  else
	    {
	      //There should be no way we can anything other than
	      //OK.   If we do, then someone isn't following the
	      //standard rules and we should fail.
	      char * buffer = new char[100];
# if __WORDSIZE == 64
	      sprintf(buffer,"MoveEventToBadQueue (maxEventWFDCount %lu:%lu) returned %ld\n",
		      event->WFD.size(),
		      detectorInfo.GetWFDCount(),
		      retEBPCmem
		      );
# else
	      sprintf(buffer,"MoveEventToBadQueue (maxEventWFDCount %u:%u) returned %lld\n",
		      event->WFD.size(),
		      detectorInfo.GetWFDCount(),
		      retEBPCmem
		      );
#endif
	      
	      //PrintError(buffer);
	      delete [] buffer;
	      ret = FATAL_ERROR; 
	    }  
	}
	  
    }

 else if( retEBPCmem == returnDCVector::BAD_PASSWORD)
    {	
      ret = LOCK_BLOCKED;
    }
 else if( retEBPCmem == returnDCVector::NO_FREE)
    {
      ret = OUT_OF_MEMORY;
    }
 else if (retEBPCmem >= 0)
   {
      char * buffer = new char[1000];
      event = EBPC_mem->GetRATDiskBlock(evIndex);
      sprintf(buffer,"Collision! %"PRId64" %"PRId64" (WFD entries: ",retEBPCmem,eventID);
      for(size_t iWFD = 0; iWFD < event->WFD.size();iWFD++)
	{
	  sprintf(buffer + strlen(buffer)," %u",event->WFD[iWFD]);
	}
      sprintf(buffer + strlen(buffer),")\n");
      EBPC_mem->PrintStatus();
      PrintError(buffer);
 

      delete [] buffer;



      //The data in the vector hash is for another event. 
      //We have a lock on it though!
      //We will move this event to the bad queue
      //Action concerning this bad event will be left to
      //another thread.
      int64_t retCollision = EBPC_mem->MoveEventToBadQueue(
				        eventID,
					evIndex,
					password,
					Level::STORAGE_COLLISION);
      
      if(retCollision == returnDCVector::OK)	
	{
	  ret = COLLISION;
	}
      else 
	{
	  //There should be no way we can anything other than
	  //OK.   If we do, then someone isn't following the
	  //standard rules and we should fail.
	  char * buffer = new char[100];
# if __WORDSIZE == 64
	  sprintf(buffer,"MoveEventToBadQueue(collision) returned %ld\n",retEBPCmem);
# else
	  sprintf(buffer,"MoveEventToBadQueue(collision) returned %lld\n",retEBPCmem);
#endif 
	  PrintError(buffer);
	  delete [] buffer;
	  ret = FATAL_ERROR; 	
	}
    }
  else if(retEBPCmem == returnDCVector::BAD_MUTEX)
    {
      //This should never happen since we will block on the mutex.
      //If this happens, then DCDAQ is probably shutting down.
      ret = FATAL_ERROR; 
    } 
  return(ret);


}
