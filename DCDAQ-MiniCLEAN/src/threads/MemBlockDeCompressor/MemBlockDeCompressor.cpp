#include <MemBlockDeCompressor.h>

MemBlockDeCompressor::MemBlockDeCompressor(std::string Name)
{
  SetName(Name);
  SetType(std::string("MemBlockDeCompressor"));
  last = time(NULL);
  current = time(NULL);
}

bool MemBlockDeCompressor::Setup(xmlNode * SetupNode,
			   std::vector<MemoryManager*> &MemManager)
{
  Ready = false;
  xmlNode * rawNode = FindSubNode(SetupNode,"RAW");
  if(rawNode == NULL)
    {
      printf("Can't find RAW tag\n");
      return false;  
    }
  if(!FindMemoryManager(eventMM,rawNode,MemManager))
    {  
      return false;
    }
  eventMM->IncrementWritingThreads();
  xmlNode * compressedNode = FindSubNode(SetupNode,"COMPRESSED");
  if(compressedNode == NULL)
    {
      printf("Can't find COMPRESSED tag\n");
      return false;
    }
  if(!FindMemoryManager(compressedMM,compressedNode,MemManager))
    {
      return false;
    }
  compressedMM->IncrementListeningThreads();
  Ready = true;
  return true;
}

void MemBlockDeCompressor::MainLoop()
{
  if(IsReadReady(compressedMM->GetFullFDs()[0]))
    {
      int32_t eIndex = FreeFullQueue::BADVALUE;
      int32_t cIndex = FreeFullQueue::BADVALUE;
      if(compressedMM->GetFull(cIndex,true) &&
	 eventMM->GetFree(eIndex,true))
	{
	  if(cIndex == FreeFullQueue::SHUTDOWN)
	    //This means that we should shut down
	    {
	      compressedMM->DeIncrementListeningThreads();
	      if(eventMM->DeIncrementWritingThreads())
		{
		  eventMM->AddEndOfRun(eIndex);
		  compressedMM->Shutdown();
		}
	      RemoveReadFD(compressedMM->GetFullFDs()[0]);
	      Loop = false;
	      Running = false;
	      return;
	    }

	  stream.zalloc = Z_NULL;
	  stream.zfree = Z_NULL;
	  stream.opaque = Z_NULL;
	  if (Z_OK == inflateInit(&stream))
	    {
	      MemoryBlock * compressed = compressedMM->GetBlock(cIndex);
	      MemoryBlock * event = eventMM->GetBlock(eIndex);
	      
	      // //If the compressed block is empty, we are done with all blocks.
	      // if(compressed->dataSize == 0)
	      // 	{
	      // 	  //Add the empty event block to the full queue.
	      // 	  eventMM->AddFull(eIndex);
	      // 	  return;
	      // 	}
	      //fprintf(stderr,"all %i,u %i\n",(int)event->allocatedSize,compressed->uncompressedSize);
	      //If uncompressedSize==-1, block is not compressed, send straight through
	      if(event->allocatedSize<compressed->dataSize)
		{
		  PrintError("RAW MemBlock too small\n");
		  return;
		}
	      if(compressed->uncompressedSize<0)
		{
		  event->dataSize=compressed->dataSize;
		  memcpy(event->buffer,compressed->buffer,compressed->dataSize*sizeof(uint32_t));
		  

		  compressedMM->AddFree(cIndex);
		  compressed=NULL;
		  eventMM->AddFull(eIndex);
		  event=NULL;
		  return;
		}
	      if((int)event->allocatedSize<compressed->uncompressedSize)
		{
		  PrintError("Raw MemBlock not large enough to hold uncompressed data \n");
		  return;
		}
	      stream.next_in = (Bytef*) compressed->buffer;
	      stream.avail_in = (compressed->dataSize)<< 2;
	      stream.next_out = (Bytef*) event->buffer;
	      stream.avail_out = (event->allocatedSize)<<2;
	      int ret = inflate(&stream,Z_FINISH);
	      if(ret == Z_STREAM_END)
		{
		  inflateEnd(&stream);
		  compressedMM->AddFree(cIndex);
		  compressed = NULL;
		  event->dataSize = event->allocatedSize - (stream.avail_out>>2);
		  eventMM->AddFull(eIndex);
		  event = NULL;
		}
	      else if(ret == Z_STREAM_ERROR)
		{
		  PrintWarning("An error happened decompressing event, will try again later. ");
		  compressedMM->AddFull(cIndex);
		  compressed = NULL;
		  eventMM->AddFree(eIndex);
		  event = NULL;
		}
	      else 
		{
		  //fprintf(stderr,"An error happened decompressing event, fail fail fail");
		  //PrintError("An error happened decompressing event, fail fail fail");
		  compressedMM->AddFull(cIndex);
		  compressed = NULL;
		  eventMM->AddFree(eIndex);
		  event = NULL;
		}
	    }
	  else
	    {
	      PrintError("GZIP inflateInit failed!\n");	      
	    }
	}
      else
	{
	  PrintError("Could not get compressed and raw memory managers!");
	}
    }
}

void MemBlockDeCompressor::ProcessTimeout()
{
  current = time(NULL);
    if(current - last > GetUpdateTime())
      {
	last = current;
      }
}

bool MemBlockDeCompressor::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      RemoveReadFD(compressedMM->GetFullFDs()[0]);
      Loop = false;
      Running = false;
      compressedMM->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      RemoveReadFD(compressedMM->GetFullFDs()[0]);
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the Full queue's FD so we know when we have a new 
      //memory block to process
      AddReadFD(compressedMM->GetFullFDs()[0]); //Add the FD to this threads mask
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return ret;
}
