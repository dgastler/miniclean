#include <RATParse.h>

void RATParse::ProcessOneEvent(v1720Event & V1720Event,RAT::DS::Root * DS)
{
  //Make sure there is an ev structure and if there isn't allocated one.
  RAT::DS::EV * RATEvent;
  if(DS->GetEVCount() == 0)
    {
      DS->AddNewEV();
    }
  RATEvent = DS->GetEV(0);
  //Set IDs
  DS->SetRunID(GetRunNumber());
  //Event ID set by writer thread
  
  //Loop over the WFD channels.
  unsigned int numberOfChannels = V1720Event.Channels.size();
  for(unsigned int iChannel = 0; iChannel < numberOfChannels;iChannel++)
    {
      //Allocate a new PMT if there aren't enough
      if(iChannel >= (unsigned int) RATEvent->GetPMTCount())
	{
	  RATEvent->AddNewPMT();
	}
      //RAT::DS::PMT.
      RAT::DS::PMT * RATPMT = RATEvent->GetPMT(iChannel);
      //Get the parsed channel structure.
      v1720EventChannel currentParsedChannel = V1720Event.Channels[iChannel];
      RATPMT->SetID(currentParsedChannel.GetChannelID());
      RATPMT->SetPMTType(1);
      //Get this PMT's raw event.
      //RAT::DS::Raw * RATRaw = RATPMT->AddRaw();
      RAT::DS::Raw * RATRaw = RATPMT->GetRaw();

      //Set the window header.
      WindowHeaderVector.clear(); //clears, but does not deallocate
      //Set the header for this PMT
      WindowHeaderVector.push_back(V1720Event.header[0]);
      WindowHeaderVector.push_back(V1720Event.header[1]);
      WindowHeaderVector.push_back(V1720Event.header[2]);
      WindowHeaderVector.push_back(V1720Event.header[3]);
      RATRaw->SetHeader(WindowHeaderVector);

      //Clear the raw waveforms already in this PMT.
      //      RATRaw->ClearRaw();      
      //Loop over the zero suppressed windows.
      unsigned int numberOfWindows = currentParsedChannel.Window.size();
      for(unsigned int iWindow = 0; iWindow < numberOfWindows;iWindow++)
	{
	  RAT::DS::RawWaveform * RATRawWaveform = NULL;
	  //	  RATRawWaveform = RATRaw->AddNewRawWaveform();
	  if(iWindow >= ((unsigned int) RATRaw->GetRawWaveformCount()))
	    RATRawWaveform = RATRaw->AddNewRawWaveform();
	  else
	    RATRawWaveform = RATRaw->GetRawWaveform(iWindow);
	  
	  //Get the current parsed window in this channel
	  v1720EventWindow currentParsedWindow = currentParsedChannel.Window[iWindow];

	  //For each window show the window's time offset to the event time stamp
	  RATRawWaveform->SetTimeOffset(currentParsedWindow.timeOffset);
	  //Set the data samples for this window
	  WindowDataVector.clear();//clears, but does not deallocate
	  uint32_t windowSize = currentParsedWindow.GetSize();
	  //Unpack the samples from this window and enter them in to the RAT window
	  for(uint32_t iSample = 0;iSample < windowSize;iSample++)
	    {
	      WindowDataVector.push_back(currentParsedWindow.GetSample(iSample));
	    }
	  RATRawWaveform->SetSamples(WindowDataVector);
	}//Window/RAW loop
    }//Channel loop
}
