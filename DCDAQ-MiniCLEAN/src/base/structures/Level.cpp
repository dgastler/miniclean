#include <Level.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[Level::COUNT] = 
  {
    "BLANK",
    "STORAGE_COLLISION",
    "STORAGE_EMPTY",
    "TOO_MANY_WFDS",
    "LVDS_MISMATCH",
    "SEND_FAILED",
    "MISSING_EVENT",
    "UNKNOWN",
    "CHAN_FULL_WAVEFORM ",
    "CHAN_ZLE_WAVEFORM",
    "CHAN_ZLE_INTEGRAL",
    "CHAN_PROMPT_TOTAL",
    "EVNT_FULL_WAVEFORM",
    "EVNT_ZLE_WAVEFORM",
    "EVNT_ZLE_INTEGRAL",
    "EVNT_PROMPT_TOTAL",
    "SAVE_NOTHING",
    "IN",
    "OUT",
    "VETO",
    "EVNT",
    "COMP",
    "BUTN",
    "CAL0",
    "CAL1",
    "CAL2",
    "PERI",
    "RAW_EVENT",
  };  

const char * Level::GetTypeName(Level::Type type)
{
  if(type < Level::COUNT)
    return TypeName[type];
  return NULL;
} 

Level::Type Level::GetType(const char * name)
{
  std::string searchString(name);  
  Level::Type ret = Level::Type(0);
  for(Level::Type type = Level::Type(0);
      type < Level::COUNT;
      type = Level::Type(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
