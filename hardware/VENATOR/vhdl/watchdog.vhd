library IEEE;
use IEEE.STD_LOGIC_1164.All;
use IEEE.NUMERIC_STD.All;

--watchdog that is "interrupt" driven by the DCM_LOCK
entity watchdog is Port(
  DCM_LOCK_STATUS : in STD_LOGIC;
  WATCHDOG_CLOCK : in STD_LOGIC;
  WATCHDOG_DCM_RESET : out STD_LOGIC;
  WATCHDOG_MAIN_RESET : out STD_LOGIC);
end watchdog;      

architecture Behavioral of watchdog is
  -- WARNING: This will only work continuously for two years... 
  signal main_reset_countdown : unsigned(27 downto 0) := x"0000000";
  signal dcm_reset_countdown : unsigned(7 downto 0) := x"00";
begin
  process(WATCHDOG_CLOCK)
  begin
    if WATCHDOG_CLOCK'event and WATCHDOG_CLOCK = '1' then
      if main_reset_countdown = x"0000000" then
        -----------------------------------------------------------------------
        --There is currently no error, look for one
        -----------------------------------------------------------------------
        if DCM_LOCK_STATUS = '0' then
          --new error, set countdown timers
          main_reset_countdown <= x"3FFFFFF";   --about 1second
          dcm_reset_countdown <=  x"06";        -- about 4 62.5Mhz clock ticks
          --turn on reset signals
          WATCHDOG_MAIN_RESET <= '1';
          WATCHDOG_DCM_RESET <= '1';
        else
          --no error so turn off resets
          WATCHDOG_MAIN_RESET <= '0';
          WATCHDOG_DCM_RESET <= '0';
        end if;
      else
        -----------------------------------------------------------------------
        --We are processing an error (count down)
        -----------------------------------------------------------------------
        if dcm_reset_countdown = x"00" then
          --let the DCM try to re-lock
          --turn off dcm reset
          WATCHDOG_DCM_RESET <= '0';
        else
          dcm_reset_countdown <= dcm_reset_countdown - x"1";
        end if;
        main_reset_countdown <= main_reset_countdown - x"1";
      end if;
    end if;    
  end process;
end Behavioral;
