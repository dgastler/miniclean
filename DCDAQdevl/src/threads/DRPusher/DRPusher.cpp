#include <DRPusher.h>

DRPusher::DRPusher(std::string Name)
{  
  SetName(Name);
  SetType(std::string("DRPusher"));

  //FEPC memory manager
  FEPC_mem = NULL;

  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);
  timeStepProcessedEvents = 0;

  //Packet interface
  packet = NULL;                      //Pointer to packet object
  packetDataPtr = NULL;               //Ptr to the begining of the packet data
  currentPacketDataPtr = NULL;        //Ptr to current position in the packet
  packetSizeLeft = 0;                 //remaining packet data size
  iDCPacket = FreeFullQueue::BADVALUE;

  //Event parsing
  drEvent = NULL;


  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>1400</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>1400</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
  //Needed for network connetion
  Loop = true;
}

DRPusher::~DRPusher()
{
  //Delete connection
  DeleteConnection();
}

bool DRPusher::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager)
{
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  bool ret = true;
  
  // Find and load the FEPC memory interface
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!FEPC_mem)
    {
      char * buffer = new char[100];	  
      sprintf(buffer,"Error %s: Missing FEPC RAT memory manager\n.",GetName().c_str());      
      PrintError(buffer);
      delete [] buffer;
      ret = false;
    }
  else
    {
      //Set the bad value for the LocalDeque
      sLocal badValue;
      badValue.block = NULL;
      badValue.index = FEPC_mem->GetBadValue();
      badValue.password = FEPC_mem->GetStorageBlankPassword();
      badValue.time = 0;
      badValue.channelsProcessed = 0;
      LocalDeque.Setup(badValue);
    }

  //================================
  //Parse data for Client config and expected clients.
  //================================
  //Use port 9999 for DCAssembler
  if(!DRAssembler.Setup(SetupNode))
    //Connection setup failed
    {
      ret = false;
    }
  if(ret)
    {
      Ready = true;
    }
  else
    {
      return(false);
    }
  return(StartNewConnection());
}

void DRPusher::PrintStatus()
{
  DCThread::PrintStatus();
# if __WORDSIZE == 64
  printf("  Deque size: %lu\n",LocalDeque.GetSize());
# else 
  printf("  Deque size: %u\n",LocalDeque.GetSize());
# endif
  if(DRAssembler.thread != NULL)
    {
      DRAssembler.thread->PrintStatus();
    }
}
