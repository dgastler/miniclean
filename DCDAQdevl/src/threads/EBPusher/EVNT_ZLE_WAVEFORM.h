#ifndef __EVNT_ZLE_WAVEFORM__
#define __EVNT_ZLE_WAVEFORM__

#include <PacketBuilder.h>

class EVNT_ZLE_WAVEFORM : public PacketBuilder
{
 public:
  EVNT_ZLE_WAVEFORM()
  {
    ClearPacket();
  };
  virtual ~EVNT_ZLE_WAVEFORM(){ClearPacket();};
  virtual bool Send(FEPCBlock * event)
  {
    printf("EVNT_ZLE_WAVEFORM not written.  FAIL!\n");
    return(false);
  }

};

#endif
