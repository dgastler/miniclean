<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
<?php include ('color_scheme.php'); ?>
<?php include ('timeIntervals.php'); ?>

<style type="text/css">
   //HALT and PCARRAY are global variables

body
{
background-image: url(./images/grey_wash_wall.jpg);
}

a:link {
  text-decoration: none;
 color: <?php echo $black ?>;
 }

.pcRemoveButton{
 position:absolute;
 left:397px;
 top:2px;
 line-height: 1;
 text-decoration:none;
 display: inline-block;
 padding: 2px 4px; 
 font-family: Verdana, sans-serif;
 text-transform:uppercase;
 font-size:10px;
 letter-spacing:1px;
 color:<?php echo $remove_text ?>;
 cursor:pointer;
 border-radius: 5px;
 -webkit-border-radius: 5px;
 box-shadow: 0 1px 3px <?php echo $remove_shadow ?>;
 -webkit-box-shadow: 0 1px 3px <?php echo $remove_shadow ?>;
 text-shadow: -1px -1px 0px <?php echo $remove_shadow ?>;
 background-color:<?php echo $remove_color ?>;
 }

.pcRemoveButton:hover{
 opacity:0.9;
 }

.threadRemoveButton{      
 position:absolute;
 left:397px;
 height:15px;
 top:2px;
 line-height: 0.4;
 text-decoration:none;
 display: inline-block;
 padding: 2px 4px;
 font-family: Verdana, sans-serif;
 text-transform:uppercase;
 font-size:10px;
 letter-spacing:1px;
 color:<?php echo $remove_text ?>;
 cursor:pointer;
 border-radius: 5px;
 -webkit-border-radius: 5px;
 box-shadow: 0 1px 3px <?php echo $remove_shadow ?>;
 -webkit-box-shadow: 0 1px 3px <?php echo $remove_shadow ?>;
 text-shadow: -1px -1px 0px <?php echo $remove_shadow ?>;
 background-color:<?php echo $remove_color ?>;
 }

.threadRemoveButton:hover{
 opacity:0.9;
 }

.mmRemoveButton{
 position:absolute;
 left:397px;
 height:15px;
 top:2px;
 line-height: 0.4;
 text-decoration:none;
 display: inline-block;
 padding: 2px 4px;
 font-family: Verdana, sans-serif;
 text-transform:uppercase;
 font-size:10px;
 letter-spacing:1px;
 color:<?php echo $remove_text ?>;
 cursor:pointer;
 border-radius: 5px;
 -webkit-border-radius: 5px;
 box-shadow: 0 1px 3px <?php echo $remove_shadow ?>;
 -webkit-box-shadow: 0 1px 3px <?php echo $remove_shadow ?>;
 text-shadow: -1px -1px 0px <?php echo $remove_shadow ?>;
 background-color:<?php echo $remove_color ?>;
 }

.mmRemoveButton:hover{
 opacity:0.9;
 }

.goButton{
 position:absolute;
 left: 980px;
 top: 27px;
 line-height: 1;
 text-decoration:none;
 display: inline-block;
 padding: 8px 12px;
 font-family: Verdana, sans-serif;
 text-transform:uppercase;
 font-size:22px;
 letter-spacing:1px;
 color:<?php echo $s_text ?>;
 cursor:pointer;
 border-radius: 5px;
 -webkit-border-radius: 5px;
 box-shadow: <?php echo $s_shadow ?>;
 -webkit-box-shadow: <?php echo $s_shadow ?>;
 text-shadow: <?php echo $s_shadow ?>;
 background-color:<?php echo $start_color ?>;		  	    
 }

.goButton:hover {
 opacity:0.9;
 }

.stopButton{
 position:absolute;
 left: 980px;
 top: 27px;
 line-height: 1;
 text-decoration:none;
 display: inline-block;
 padding: 8px 12px;
 font-family: Verdana, sans-serif;
 text-transform:uppercase;
 font-size:22px;
 letter-spacing:1px;
 color:<?php echo $s_text ?>;
 cursor:pointer;
 border-radius: 5px;
 -webkit-border-radius: 5px;
  box-shadow: 0 1px 3px <?php echo $s_shadow ?>;
 -webkit-box-shadow: 0 1px 3px <?php echo $s_shadow ?>;
 //text-shadow: -1px -1px 0px <?php echo $s_shadow ?>;
 background-color:<?php echo $stop_color ?>;		  	    
 }

.stopButton:hover {
 opacity:0.9;
 }

.pcbutton
{
padding: 0px;
width: 500px;
float: left;
line-height:0;
font-size: 20px;
font-family: Georgia;
text-decoration:none;
text-align: left;
color:<?php echo $pc_text ?>
background:-webkit-gradient(linear,0% 0%,0% 100%,from(<?php echo $pc_button_top ?>),to(<?php echo $pc_button_bottom ?>));
background:-moz-linear-gradient(top,<?php echo $pc_button_top ?>,<?php echo $pc_button_bottom ?>);
border-radius: 5px;  
webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $pc_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $pc_shadow ?>;
}

.runMenu
{
position:absolute;
left:245px;
top:50px;
width: 200px;
font-size:15px;
color:<?php echo $run_menu_text ?>;
border:none;
font-family: Georgia;
text-decoration:none;
text-align: left;
background:-webkit-gradient(linear,0% 0%,0% 100%,from(<?php echo $run_menu_top ?>),to(<?php echo $run_menu_bottom ?>));
background:-moz-linear-gradient(top,<?php echo $run_menu_top ?>,<?php echo $run_menu_bottom ?>);
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $run_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $run_shadow ?>;
}

.addMenu
{
width: 425px;
  margin-top:2px;
font-size:15px;
color:<?php echo $run_menu_text ?>;
border:none;
font-family: Georgia;
text-decoration:none;
text-align: left;
background:-webkit-gradient(linear,0% 0%,0% 100%,from(<?php echo $run_menu_top ?>),to(<?php echo $run_menu_bottom ?>));
background:-moz-linear-gradient(top,<?php echo $run_menu_top ?>,<?php echo $run_menu_bottom ?>);
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $run_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $run_shadow ?>;
}

.runMenuLabel
{
position:absolute;
left:245px;
top:15px;
width:195px;
height:15px;
  font-size:18px;
  font-family:Verdana, sans-serif;
color:<?php echo $run_label ?>;
}

.wfdbutton
{
position:relative;
padding:0px;
width: 425px;
float: left;
font-size: 20px;
font-family: Georgia;
text-decoration:none;
text-align: left;
color:<?php echo $run_label ?>;
//color:red;
background:-webkit-gradient(linear,0% 0%,0% 100%,from(<?php echo $pc_button_top ?>),to(<?php echo $pc_button_bottom ?>));
background:-moz-linear-gradient(top,<?php echo $pc_button_top ?>,<?php echo $pc_button_bottom ?>);
border-radius: 5px;  
webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $pc_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $pc_shadow ?>;
}

.contentdiv
{
position: absolute;
overflow: auto;
width: 95%;
  height: 75%;
  margin-top: 10%;
  margin-left: 25px;
  margin-right: 20px;
  background-color:#EDEDED;
  border-radius: 15px;
  -moz-border-radius: 15px;
}

.headerdiv
{
position: absolute;
overflow: auto;
width: 95%;
height: 19%;
  font-family: Verdana, sans-serif;
  font-size: 45px;
  text-align:center;
  //  vertical-align:middle;
  line-height:100px;
color: <?php echo $run_label ?>;  
  margin-top: 5px;
  margin-left: 25px;
  margin-right: 20px;
  background-color:#EDEDED;
  border-radius: 15px;
  -moz-border-radius: 15px;
}

.logodiv
{
background-image: url(./images/dan-gastler.jpg); 
  background-position: center;
position: absolute;
overflow: auto;
height: 90px;
width: 90px;
  margin-left: 25px;
  margin-right: 15px;
  margin-top: 10px;
  margin-bottom: 15px;
  border-radius: 15px;
  -moz-border-radius: 15px;
  border: 2px solid gray;
}
      
.container
{
position: absolute;
overflow: auto;
  float:left;
width: 99%;
height: auto;
margin-top: 84px;
padding: 5px;
}

.bannerdiv
{
position: absolute;
  margin-top: 38px;
  margin-left: 550px;
width: 200px;
  font-size: 20px;
  font-family: Verdana, sans-serif;
color: <?php echo $run_label ?>;
}

.leftwrapper
{
float:left;
width:465px;
height:auto;
margin-top: 2px;
margin-left: 10px;
margin-bottom: 2px;
padding: 0;
}

.rightwrapper
{
float:left;
width:475px;
height:auto;
margin-top: 2px;
//margin-left: 2px;
margin-bottom: 2px;
padding:0;
}

.pcwrapper
{
  float:left;
width:375px;
height:auto;
  margin-top:2px;
  margin-left:10px;
  margin-bottom:2px;
padding:0;
}


.runCopyMenu
{
position:absolute;
left:20px;
top:50px;
width: 200px;
font-size:15px;
color:<?php echo $run_menu_text ?>;
border:none;
font-family: Georgia;
text-decoration:none;
text-align: left;
background:-webkit-gradient(linear,0% 0%,0% 100%,from(<?php echo $run_menu_top ?>),to(<?php echo $run_menu_bottom ?>));
background:-moz-linear-gradient(top,<?php echo $run_menu_top ?>,<?php echo $run_menu_bottom ?>);
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $run_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $run_shadow ?>;
}

.runCopyLabel
{
position:absolute;
left:20px;
top:15px;
width:195px;
height:15px;
  font-size:18px;
  font-family:Verdana, sans-serif;
color:<?php echo $run_label ?>;
}

.addWFDMenu
{
position:absolute;
left:475px;
top:50px;
width: 200px;
font-size:15px;
color:<?php echo $run_menu_text ?>;
border:none;
font-family: Georgia;
text-decoration:none;
text-align: left;
background:-webkit-gradient(linear,0% 0%,0% 100%,from(<?php echo $run_menu_top ?>),to(<?php echo $run_menu_bottom ?>));
background:-moz-linear-gradient(top,<?php echo $run_menu_top ?>,<?php echo $run_menu_bottom ?>);
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $run_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $run_shadow ?>;
}

.addWFDLabel
{
position:absolute;
left:475px;
top:15px;
width:195px;
height:15px;
  font-size:18px;
  font-family:Verdana, sans-serif;
color:<?php echo $run_label ?>;
}

.threadbox
{
position:relative;
width: 425px;
  //top:5px;
font-size: 17px;
font-family: Georgia;
text-decoration:none;
text-align: left;
background-color: <?php echo $t_color ?>;
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $t_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $t_shadow ?>;
}

.mmbox
{
position:relative;
width: 425px;
  font-size: 17px;
  font-family: Georgia;
  text-decoration:none;
  text-align: left;
  background-color: <?php echo $mm_color ?>;
  border-radius: 5px;
  -webkit-border-radius: 5px;
  box-shadow: 0 1px 3px <?php echo $mm_shadow ?>;
  -webkit-box-shadow:0 0px 2px <?php echo $mm_shadow ?>;
}

.databox
{				      
position:relative;
width: 425px;
  font-size: 15px;
  font-family: Georgia;
  text-decoration:none;
  text-align: left;
color: <?php echo $data_text ?>;
background-color: <?php echo $data_color ?>;
border-radius: 5px;
-webkit-border-radius: 5px;
box-shadow: 0 1px 3px <?php echo $data_shadow ?>;
-webkit-box-shadow:0 0px 2px <?php echo $data_shadow ?>;
}

.sdatabox
{				      
position:relative;
width: 425px;
  font-size: 15px;
  font-family: Georgia;
  text-decoration:none;
  text-align: left;
  background-color: <?php echo $sdata_color ?>;
  border-radius: 5px;
  -webkit-border-radius: 5px;
  box-shadow: 0 1px 3px <?php echo $sdata_shadow ?>;
  -webkit-box-shadow:0 0px 2px <?php echo $sdata_shadow ?>;
  box-shadow: 0 1px 3px black;
}

.statusbox				    {
 position:absolute;
 left: 1160px;
 top: 30px;
 width:110px;
   font-family: Georgia;
   text-decoration:none;
   text-align: center;
 color: <?php echo $status_text ?>;
 background-color:<?php echo $status_color ?>;
 border-radius: 5px;  
 -webkit-border-radius: 5px;  			
 -webkit-box-shadow: 0 0px 2px <?php echo $status_shadow ?>;
 box-shadow: 0 1px 3px <?php echo $status_shadow ?>;  
 }

.errorbox
{
position:relative;
width: 425px;
font-size: 15px;
font-family: Georgia;
text-decoration:none;
text-align: left;
color: <?php echo $error_text ?>;
  background-color:<?php echo $error_color ?>;
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $error_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $error_shadow ?>;
}

.fixbox
{
position:relative;
width: 425px;
font-size: 15px;
font-family: Georgia;
text-decoration:none;
text-align: left;
color: <?php echo $fix_text ?>;
  background-color:<?php echo $fix_color ?>;
border-radius: 5px;  
-webkit-border-radius: 5px;  			
box-shadow: 0 1px 3px <?php echo $fix_shadow ?>;  
-webkit-box-shadow: 0 0px 2px <?php echo $fix_shadow ?>;
}

.clear
{
clear:both;
}

.save
{
display: none;
margin: 5px 10px 10px;
}

.formbox
{
position:absolute;
left:190px;
top:7px;
width:125px;
  background-color: <?php echo $data_form ?>;
border: 1px solid #000000;
  margin-bottom: 10px;
}
.sformbox
{
position:absolute;
left:190px;
top:7px;
width:125px;
  background-color: <?php echo $sdata_form ?>;
border: 1px solid #000000;
  margin-bottom: 10px;
}
.fixformbox
{
position:absolute;
left:190px;
top:7px;
width:125px;
  background-color: <?php echo $fix_form ?>;
border: 1px solid #000000;
  margin-bottom: 10px;
}

.errorformbox
{
position:absolute;
left:190px;
top:7px;
width:125px;
  background-color: <?php echo $error_form ?>;
border: 1px solid #000000;
  margin-bottom: 10px;
}

.tupdatebutton
{
position:absolute;
  background-color: <?php echo $t_color ?>;
left: 340px;
top:7px;
}

.mmupdatebutton
{
position:absolute;
  background-color: <?php echo $mm_color ?>;
left: 340px;
top: 7px;
}

.addthreadbox
{				      
position:relative;
width: 425px;
  font-size: 15px;
  font-family: Georgia;
  text-decoration:none;
  text-align: left;
color: <?php echo $data_text ?>;
background-color: <?php echo $data_color ?>;
border-radius: 5px;
-webkit-border-radius: 5px;
box-shadow: 0 1px 3px <?php echo $data_shadow ?>;
-webkit-box-shadow:0 0px 2px <?php echo $data_shadow ?>;
}

.pcupbutton
{
position:absolute;
left:373px;
  top:4px;
  line-height: 1;
  text-decoration:none;
display: inline-block;
padding: 2px 4px;
  width: 15px;
  height: 15px;
  //background-color: <?php echo $start_color ?>;
  background-color: #FFFFFF
  //background-image: -webkit-linear-gradient(top, <?php echo $up_startgrad ?>, <?php echo $up_endgrad ?>);
  //background-image: -moz-linear-gradient(top, <?php echo $up_startgrad ?>, <?php echo $up_endgrad ?>);
//margin-right:-4px;
border-radius:50%;
-webkit-border-radius:50%;
box-shadow: 0 1px 1px <?php echo $remove_shadow ?>;
-webkit-box-shadow: 0 1px 1px <?php echo $remove_shadow ?>;
-moz-box-shadow: inset 0 1px 1px <?php echo $remove_shadow ?>;
}

.pcupbutton:hover{
 opacity:0.9;
 }


</style>
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.selectbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

				    /* Clear data from run zero */
function clearExistingData(){
   
   $.ajax({
     type: 'POST',
     url: "clearExistingData.php",
     success: function(data){
	 
       }
     });

   }

function stopRun() {
/*Function to stop the runs from the stop button*/
  $.ajax({
    type: 'POST',
	url: "stopRuns.php",
	success: function(stop) {
	
      }
    });
}

function startRun() {
  /*Function to update the runlock table */
  $.ajax({
    type: 'POST',
	url: "startRuns.php",
	success: function(start) {
	
      }
    });
}

function createPC(pc_name,ip,port){  
  /* AJAX query to get the PCs for a given run */
  $.ajax({
    type: 'POST',
	url: "createPC.php",
	data: { pcName: pc_name, IP: ip, Port: port },
	success: function(data){	       
      }
    });
}

  /* Function to slide the threads up and down */
function threadSlide(thechosenone) {
  $('.threadboxes').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}

/* Function to slide the wfd setting up and down */
function settingsSlide(thechosenone) {
  $('.settingsboxes').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}

//Function to slide the mms up and down
function mmSlide(thechosenone) {
  $('.mmboxes').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}

				    //Function to slide the addthreadboxes up and down
function addThreadSlide(thechosenone) {
  $('.addThreadButtons').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}			    
//Function to slide the added memorymanagers up and down
function addMMSlide(thechosenone) {
  $('.addMMButtons').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}			    

//Function to slide the channel boxes up and down
function channelSlide(thechosenone) {
  $('.channelboxes').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}			    

/*Function to slide the thread data boxes up and down */
function dataSlide(thechosenone) {
  $('.databoxes').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if(($(this).prop('id') == thechosenone))
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}


/* Function to slide the PC boxes up and down */
function pcSlide(thechosenone) {
  $('.pcSlideBox').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if($(this).prop('id') == thechosenone)
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}

/*Function to slide the WFD boxes up and down*/
function wfdSlide(thechosenone) {
  $('.wfdSlideBox').each(function(index) {
      if ($(this).prop('id') == thechosenone && ($(this).is(":visible"))==1) {
	$(this).slideUp(200);
      }
      else if($(this).prop('id') == thechosenone)
	{
	  $(this).slideDown(200);
	}
      else 
	{
	  $(this).slideUp(600);
	}
    });
}

function copyRunToRunZero(selectedRun){
  
  var selectedRunSplit = selectedRun.split(':');
  var selectedRunNumber = selectedRunSplit[0];
  
  $.ajax({
    type: 'POST',
	url: "copyRunToRunZero.php",
	async: false,
	data:{runNumber:selectedRunNumber},
	success: function(data){
      }
    }); 
}

function copyIndividual(table,runFrom,type,name,pc) {
  
  var selectedRunSplit = runFrom.split(':');
  var selectedRunNumber = selectedRunSplit[0];
  $.ajax({
    type: 'POST',
	url: "copyIndividualRow.php",
	async: false,
	data: {runNumber:selectedRunNumber, table:table, type:type, name:name, pcName:pc},
	success: function(data) {
      }
    });
}

function getDataFromPHP(name, pcName, table, runNumber) {
  var data = 0;	  
  $.ajax({
    type: 'POST',
	url: "getData.php",
	async: false,
	data: {type: name, pcName: pcName, table: table, runNumber: runNumber},
	success: function(returndata){
	data = returndata;
      }
    });
  return data;
}

function getThreadType(table) {
  var typeArray = new Array();
  $.ajax({
    type: 'POST',
	url: "getThreadType.php",
	async: false,
	data: {table:table},
	success:function (threaddata) {
	$.each($.parseJSON(threaddata),function(threadindex,threadvalue) {
	    typeArray[threadindex] = threadvalue;
	  });
      }
    });
  return typeArray;
}

function getColumnNamesFromPHP(threadName, pcName, table, runNumber) {
  var column_names=0;
  $.ajax({
    type: 'POST',
	url: "getColumnNames.php",
	async: false,
	data: {threadType: threadName, pcName: pcName, table: table, runNumber: runNumber},
	success: function(column_data){
	column_names = column_data;
      }
    });
  return column_names;
}

function getSettingsFromPHP(runNumber, boardID, table) {
  var settings=0;
  $.ajax({
    type: 'POST',
	url: "getWFDSettings.php",
	async: false,
	data: {runNumber:runNumber, boardID:boardID, table:table},
	success: function(settings_data) {
	settings = settings_data;
      }
    });
  return settings;
}

function getRunsFromPHP(table) {
      $.ajax({
      type: 'POST',
	  url: "getRuns.php",
	  async: false,
	  data: {table: table},
	  success: function(run_data){	       
	  var runs=$.parseJSON(run_data);
	  if(runs != null){
	    $.each(runs, function(runIndex,runValue){
		var selectRun = "'<option value=" + '"' + runValue + '"' + ">" + runValue + "</option>'";
		if (table == 'RunNames') {
		$("[id=runupload-dropdown]").append(selectRun);
		$("[id=runcopy-dropdown]").append(selectRun);	    
		} else {
		  $("[id=runupload-dropdown]").append(selectRun);
		}
	      });
	  }
	  }
      });
}

function checkClash(table,name,pc_name) {
  var valid = 0;
  $.ajax({
    type: 'POST',
	url: "checkClash.php",
	async: false,
	data: {table:table, name:name, pc_name:pc_name},
	success: function(isvalid) {
	valid = isvalid;
      }
    });
  return valid;
}



function showRun(selectedRunNumber, editable) {
      if (editable==='readonly') {
	var pcremoveable='';
	var threadremoveable='';
      } else {
	var pcremoveable='<button type="button" id="removepc" class="pcRemoveButton">-</button>';
	var threadremoveable='<button type="button" id="removethread" class="threadRemoveButton">-</button>';
      }

      pcArray = new Array();
      ipArray = new Array();
      /* AJAX query to get the PCs for a given run */
      $.ajax({
	type: 'POST',
	    url: "getPCs.php",
	    async: false,
	    data: { runNumber: selectedRunNumber },
	    success: function(pc_data){	       
	    var pcNames = $.parseJSON(pc_data);
	    index=0;
	    dataIndex=0;
	    dataIndex2=0;
	    //pcIndex = 0;
	
	    $.each(pcNames, function(pcIndex,pcValue) {
		var pcSplit=pcValue.split(":");
		var pcName=pcSplit[0];
		var pcIP=pcSplit[1];
 	        //NOW WE HAVE ALL OF THE PC VALUES
		pcArray[index] = pcName;
		ipArray[index] = pcIP;
		index++;
	      });
	  }
	});

      //So now we have the set of PCs that have that given run on them! Yay
      var tIndex = 0;
      var pcFlags = new Array();
      var threadArray = new Array();
      for (var i=0;i<(pcArray.length);i++) {
	//	var pcTag = pcArray[i];
	/* AJAX query to get the threads for a given PC */
      var pcIterName =pcArray[i];
      $.ajax({
	type: 'POST',
	    url: "getThreads.php",
	    async: false,
	    data: { pcName: pcIterName, runNumber: selectedRunNumber },
	    success: function(thread_data){
	    var threads = $.parseJSON(thread_data);
	    if(threads != null) {
	    $.each(threads,function(threadIndex,threadValue) {
		//threadValue has threadType:threadName in it
		var threadSplit = threadValue.split(':');
		var threadTypeValue = threadSplit[0];
		var threadNameValue = threadSplit[1];
		var this_threadType = "'" + threadTypeValue + "'";
		var this_threadName = "'" + threadNameValue + "'";
		threadArray[tIndex] = threadValue + ":" + pcIterName;
		tIndex++;
	      });
	    }
	  }
	}); 
      //These are the indices where the threads change the PCs
      pcFlags[i+1] = tIndex;
      }
      pcFlags[0] = 0;

      //Now use the array of thread names to look up the data in each of these threads. This array will just store the entire string of data; it gets parsed later
      //create the 2d array to store the data for each thread for each pc
      //note: format is [pc index][thread index]
      var dataArray = new Array(pcArray.length);
      var tTemplateArray = new Array(pcArray.length);
      var tDefaultArray = new Array(pcArray.length);
      var tNecessaryArray = new Array(pcArray.length);
      for (var x = 0; x < (pcArray.length); x++) {
	var ta_length = pcFlags[x+1]-pcFlags[x];
	dataArray[x] = new Array(ta_length);
	tTemplateArray[x] = new Array(ta_length);
	tDefaultArray[x] = new Array(ta_length);
	tNecessaryArray[x] = new Array(ta_length);
      }
      
      //This gets the json strings and places them in the array
      for (var pi=0; pi < (pcArray.length); pi++) {
	for (var ti=pcFlags[pi]; ti < (pcFlags[pi+1]); ti++) {
	  var pcName = pcArray[pi];
	  var threadVal = threadArray[ti].split(":");
	  var threadName = threadVal[0];
	  var table1 = 'Thread_Threads';
	  var table2 = 'Thread_Templates';
	  var table3 = 'Thread_Defaults';
	  var table4 = 'Thread_Necessary';
	  dataArray[pi][ti] = getDataFromPHP(threadName, pcName, table1, selectedRunNumber);
	  tTemplateArray[pi][ti] = getDataFromPHP(threadName, pcName, table2, selectedRunNumber);
	  tDefaultArray[pi][ti] = getDataFromPHP(threadName, pcName, table3, selectedRunNumber);
	  tNecessaryArray[pi][ti] = getDataFromPHP(threadName, pcName, table4, selectedRunNumber);
	}
      }
      
      //Now the JSON strings holding all of the data for each string are stored in the array to be referenced by dataArray[pcname][threadname]
      //Thread templates are similarly stored, with the same referencing scheme: tTemplateArray[pcname][threadname]
      //Get the memory managers for all of these PCs as well
      var mmIndex = 0;
      var mmPCFlags = new Array();
      var mmArray = new Array();
      for (var z=0; z < (pcArray.length); z++) {
	var mmIterName = pcArray[z];
      $.ajax({
	type: 'POST',
	    url: "getMMs.php",
	    async: false,
	    data: { pcName: mmIterName, runNumber: selectedRunNumber},
	    success: function(mm_data) {
	    var MMs = $.parseJSON(mm_data);
	    if(MMs != null){
	      $.each(MMs,function(mIndex, mmValue){
		  //mmValue has mmType:mmName in it
		  var mmSplit = mmValue.split(':');
		  var mmTypeValue = mmSplit[0];
		  var mmNameValue = mmSplit[1];
		  var this_mmType = "'" + mmTypeValue + "'";
		  var this_mmName = "'" + mmNameValue + "'";
		  mmArray[mmIndex] = mmValue + ":" + pcIterName;
		  mmIndex++;
		});
	    }
	  }
	  }); 
      //These are the indices where the threads change the PCs
      mmPCFlags[z+1] = mmIndex;
      }
      mmPCFlags[0] = 0;
      
      var mmDataArray = new Array(pcArray.length);
      var mmTemplateArray = new Array(pcArray.length);
      var mmDefaultArray = new Array(pcArray.length);
      var mmNecessaryArray = new Array(pcArray.length);
      for (var x = 0; x < (pcArray.length); x++) {
	var mma_length = pcFlags[x+1]-pcFlags[x];
	mmDataArray[x] = new Array(mma_length);
	mmTemplateArray[x] = new Array(mma_length);
	mmDefaultArray[x] = new Array(mma_length);
	mmNecessaryArray[x] = new Array(mma_length);
      }

      for (var pi=0; pi < (pcArray.length); pi++) {
	for (var mmi=mmPCFlags[pi]; mmi < mmPCFlags[pi+1]; mmi++) {
	  var pcName = pcArray[pi];
	  var mmVal = mmArray[mmi].split(":");
	  var mmName = mmVal[0];
	  var table1 = 'Memory_MemoryManagers';
	  var table2 = 'Memory_Templates';
	  var table3 = 'Memory_Defaults';
	  var table4 = 'Memory_Necessary';
	  dataIndex = mmPCFlags[pi+1]-mmi;
	  mmDataArray[pi][mmi] = getDataFromPHP(mmName, pcName, table1, selectedRunNumber);
	  mmTemplateArray[pi][mmi] = getDataFromPHP(mmName, pcName, table2, selectedRunNumber);
	  mmDefaultArray[pi][mmi] = getDataFromPHP(mmName, pcName, table3, selectedRunNumber);
	  mmNecessaryArray[pi][mmi] = getDataFromPHP(mmName, pcName, table4, selectedRunNumber);
	}
      }

      //BEGINNING FUNCTIONS TO DISPLAY ONTO WEBPAGE
      //Print the pc boxes onto the screen using the array of pcs
      var pcExpandingBox;
      var threadAddExpandingBox;
      for (var j=pcArray.length-1; j >=0; j--) {
	//make the PC boxes
	var PCString = "'" + pcArray[j] + "'";
	var IPString = ipArray[j];
	var pcupbutton = '<button type="button" id="PCUP-'+ipArray[j]+'" class="pcupbutton" onclick="ipClick(\''+IPString+'\')"></button>';

	//pcExpand: clickable button to trigger the pcSlide function and display all of the pcs - on one click, it displays all threads and mms stored
	//in that pc, on the next, it closes and displays just the pcname (buttons)
	var pcExpand = '<div id="PC' + pcArray[j] + '"><div class="wfdbutton" style="float:left;" id=' + pcArray[j]+ '><a id="' + pcArray[j] + '" href="javascript:pcSlide(' + PCString + ');" >&nbsp&nbsp<span style="color:<?php echo $pc_text ?>;">' + pcArray[j] + " : " + ipArray[j] + '</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+pcupbutton+'&nbsp&nbsp&nbsp'+pcremoveable+'</a><div>';
	//pcBox - the physical box with the pcExpand functionality
	var pcBox = '<div class="pcSlideBox" id="' + pcArray[j] + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
	pcExpandingBox = pcExpand+pcBox;
	$(pcExpandingBox).appendTo("#leftwrapper");		   
      }

      //Make the restart button execute the restart script
      // var restartThis= document.getElementById('PCUP-'+pcArray[j]);
      //alert(restartThis);

      var threadExpandingBox;
      for (var j=0; j < (pcArray.length); j++) {

	if (editable!=='readonly') {
	var addThreadSelect = '<select id="addThread-dropdown:' + pcArray[j] + '" class="addMenu" name="addThread-dropdown:' + pcArray[j] + '" ><option>Add a thread...</option></select>';
	var pcE = document.getElementById(pcArray[j]);
	$(pcE).find('.pcSlideBox').append(addThreadSelect);
	var myselect = document.getElementById("addThread-dropdown:" + pcArray[j]);
	}

	//Populate the menu with all of the thread types
	var typeArray = getThreadType('Thread_Threads');
	for (var typei=0; typei < typeArray.length; typei++) {
	  $(myselect).append('<option value="'+typeArray[typei]+'">'+typeArray[typei]+'</option>');	  
	}
				      
	for (var k = pcFlags[j]; k < pcFlags[j+1]; k++) {
	  var threadSplit = threadArray[k].split(':');
	  var threadTypeValue = threadSplit[0];
	  var threadNameValue = threadSplit[1];
	  var threadPC = threadSplit[2];
	  var this_threadType = "'" + threadTypeValue + "'";
	  var this_threadName = "'" + threadNameValue + "'";
	  //this is the id of each of the divs for the thread array
	  var threadDivName = threadArray[k] + " " + pcArray[j];
	  var threadString = "'" + threadDivName + "'";

	      var threadExpand = '<div id="' + threadDivName + '"><div class="threadbox" id="' + threadDivName + '"><a id="' + threadDivName + '" href="javascript:threadSlide(' + threadString + ');" >&nbsp&nbsp' + threadTypeValue + " : " + threadNameValue + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+threadremoveable+'</a><div>';
		      var threadBox = '<div class="threadboxes" id="' + threadDivName + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
		      threadExpandingBox = threadExpand + threadBox;
		      var pcE = document.getElementById(pcArray[j]);
		      $(pcE).find('.pcSlideBox').append(threadExpandingBox);


		      //***CHECK FOR SPECIAL THREADS 

	  if ((threadTypeValue==='DecisionMaker')||(threadTypeValue==='PackData')||(threadTypeValue==='MemBlockCompressor')) {
	    var SThreadMenu = 'SThreadMenu';
	    var SThreadMenuString = "'" + SThreadMenu +":"+threadNameValue+"'";
	      var sthreadExpand = '<div id="SThreadMenu:'+threadNameValue+'"><div class="databox" style="font-size:18px;" id="SThreadMenu:'+threadNameValue+'"><a id="SThreadMenu:'+threadNameValue+'" href="javascript:dataSlide('+SThreadMenuString+');" >&nbsp&nbspSpecial Threads...&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+threadremoveable+'</a><div>';
	    
		      var sthreadBox = '<div class="databoxes" id="SThreadMenu:'+threadNameValue+'" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
		      var sthreadExpandingBox = sthreadExpand + sthreadBox;
		      var pcE = document.getElementById(threadDivName);
		      $(pcE).find('.threadboxes').append(sthreadExpandingBox);

	    //This means it is a special thread! We need to get more data from it with a SPECIAL QUERY
	    //PUT THESE INTO A DIFFERENT DROPDOWN MENU

	    var sthread1 = getDataFromPHP(threadNameValue,threadPC,'SThread_Data_Raw',selectedRunNumber);
	    var sthread2 = getDataFromPHP(threadNameValue,threadPC,'SThread_Data_Compressed',selectedRunNumber);
	    var sthread3 = getDataFromPHP(threadNameValue,threadPC,'SThread_DecisionMaker',selectedRunNumber);
	    var col_names = getColumnNamesFromPHP(threadNameValue,threadPC,'SThread_DecisionMaker',selectedRunNumber);
	    var parse_sthread1 = $.parseJSON(sthread1);
	    var parse_sthread2 = $.parseJSON(sthread2);
	    var parse_sthread3 = $.parseJSON(sthread3);
	    var parse_col_names = $.parseJSON(col_names);
	    
	    //SThread_Data_Raw - only need 'raw'; that is index 4
	    var rawval = parse_sthread1[4];
	    //SThread_Data_Compressed - need 'compressed'; also index 4;
	    var compressedval = parse_sthread2[4];
	    //SThread_DecisionMaker - need the final three entries - indexes 2,3,4
	    var dmArray = new Array();

	    for (var i=2; i<5; i++) {
	      dmArray[i-2] = parse_sthread3[i];

	      if (dmArray[i-2] !== '') {

	    if (editable==='readonly') {
  var dataExpand = '<div id="' + dmArray[i-2] + '" class="sdatabox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + parse_col_names[i] + " : " + dmArray[i-2] + '<div>';
	    } else {
	      var dataExpand = '<div id="' + dmArray[i-2] + '" class="sdatabox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + dmArray[i-2] + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + parse_col_names[i] + ' : ' + '</label><input type="text" class="sformbox" id="threaddata:'+selectedRunNumber+':'+pcArray[j]+':'+threadNameValue+':SThread_DecisionMaker:'+parse_col_names[i]+'" name="threaddata" value="'+dmArray[i-2]+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+pcArray[j]+':'+threadNameValue+':SThread_DecisionMaker:'+parse_col_names[i]+'" value="Update" /></form><div>';
	    }
		      var pcE = document.getElementById('SThreadMenu:'+threadNameValue);
		      $(pcE).find('.databoxes').append(dataExpand);
		      } 
		      
	    }

	    //Now print out the data (if it applies)
	    if (rawval !== '') {
	      if (editable==='readonly') {
  var dataExpand = '<div id="' + rawval + '" class="sdatabox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Raw : ' + rawval + '<div>';
	    } else {
	      var dataExpand = '<div id="' + rawval + '" class="sdatabox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + rawval + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + 'Raw' + ' : ' + '</label><input type="text" class="sformbox" id="threaddata:'+selectedRunNumber+':'+pcArray[j]+':'+threadNameValue+':SThread_Data_Raw:Raw" name="threaddata" value="'+rawval+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+pcArray[j]+':'+threadNameValue+':SThread_Data_Raw:Raw" value="Update" /></form><div>';
	    }
	      var pcE = document.getElementById('SThreadMenu:'+threadNameValue);
	      $(pcE).find('.databoxes').append(dataExpand);
	    }
	    if (compressedval !== '') {
	      if (editable==='readonly') {
  var dataExpand = '<div id="' + compressedval + '" class="sdatabox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Compressed : ' + compressedval + '<div>';
	      } else {
	      var dataExpand = '<div id="' + compressedval + '" class="sdatabox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + compressedval + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + 'Compressed' + ' : ' + '</label><input type="text" class="sformbox" id="threaddata:'+selectedRunNumber+':'+pcArray[j]+':'+threadNameValue+':SThread_Data_Compressed:Compressed'+':'+i+'" name="threaddata" value="'+compressedval+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+pcArray[j]+':'+threadNameValue+':SThread_Data_Compressed:Compressed" value="Update" /></form><div>';
	      }
	      var pcE = document.getElementById('SThreadMenu:'+threadNameValue);
	      $(pcE).find('.databoxes').append(dataExpand);
	    }
	  }

	}

	    for (var s = pcFlags[j]; s < pcFlags[j+1]; s++) {  
	      var threadIndex = s;
		//GETTING THE THREAD DATA: we have j and s, corresponding with the json string stored in the dataArray[][]
	      var tdata = new Array();
	      var ttemplate = new Array();
	      var tdefault = new Array();
	      var tnecessary = new Array();
	      var parse_djstring = $.parseJSON(tDefaultArray[j][s]);
	      var parse_jstring = $.parseJSON(dataArray[j][s]);
	      var parse_tjstring = $.parseJSON(tTemplateArray[j][s]);
	      var parse_njstring = $.parseJSON(tNecessaryArray[j][s]);
	      $.each(parse_jstring, function(index,value) {
		  tdata[index] = value;
		});
	      $.each(parse_tjstring, function(tindex, tvalue) {
		  ttemplate[tindex] = tvalue;
		});
	      $.each(parse_djstring, function(dindex, dvalue) {
		  tdefault[dindex] = dvalue;
		});
	      $.each(parse_njstring, function(nindex, nvalue) {
		  tnecessary[nindex] = nvalue;
		});

	    //OK! now we have the data FOR THIS THREAD stored in an array (tdata) and the corresponding template (ttemplate). Print it out!
	    //the first few values are different - I just start with the data. What else do we want to print out?
	    for (var i=5; i< tdata.length-1; i++) {
	      var printData = tdata[i];
	      var printTemplate = ttemplate[i-4];
	      var printDefault = tdefault[i-4];
	      var printNecessary = tnecessary[i-4];
	      if (!tdata[i]) {
		printData = "NULL";
	      }
	      if (!ttemplate[i-4]) {
		printTemplate = "NULL";
	      }
	      if (!tdefault[i-4]) {
		printDefault = "NULL";
	      }
	      if (!tnecessary[i-4]) {
		printNecessary = "NO";
	      }

	      var threadIndex = s;
//Now add the thread data for each of the boxes - pcindex = j, threadindex = k
	      //we have to parse the json string stored in that location
	      //Check to make sure that the data is not a NULL:NULL set (which doesn't need to be displayed)
	      if((printData==="NULL") && (printDefault!=="NULL")) {
		if (editable==='readonly') {
  var dataExpand = '<div id="' + tdata[3] + '" class="fixbox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printTemplate + " : " + printData + ' <br>(default: '+printDefault+')'+'<div>';
		} else {
	      var dataExpand = '<div id="' + tdata[3] + '" class="fixbox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + printData + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + printTemplate + " : " + '<br>(default: '+printDefault+') </label><input type="text" class="fixformbox" id="threaddata:'+selectedRunNumber+':'+threadArray[s]+':Thread_Threads:'+printData+':'+i+'" name="threaddata" value="'+printData+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+threadArray[s]+':Thread_Threads:'+printData+':'+i+'" value="Update" /></form><div>';
		}
 var dataBox = '<div class="databoxes" id="' + tdata[3] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>';
	      var dataExpandingBox = dataExpand + dataBox;
	      var pcE = document.getElementById(threadArray[threadIndex]+" "+pcArray[j]);
	      $(pcE).find('.threadboxes').append(dataExpandingBox);

	      } else if((printData==="NULL")&&(printNecessary==="YES")) {
	      var dataExpand = '<div id="' + tdata[3] + '" class="errorbox"; ><a id="' + tdata[3] + '" href="javascript::dataSlide(' + tdata[3] + ');" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printTemplate + " : " + printData + " : CANNOT HAVE NULL VALUE" + '</a><div>';
	      //	      alert(printData + ":" + printTemplate);
 var dataBox = '<div class="databoxes" id="' + tdata[3] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>';
	      var dataExpandingBox = dataExpand + dataBox;
	      var pcE = document.getElementById(threadArray[threadIndex]+" "+pcArray[j]);
	      $(pcE).find('.threadboxes').append(dataExpandingBox);

	      } else {
		//DEFAULT SITUATION - NOW WITH FORMBOXES
	      if ((printData !=  "NULL") || (printTemplate != "NULL")) {
		//check to see if you need the form box. If it is readonly, just display the data
		if (editable==='readonly') {
  var dataExpand = '<div id="' + tdata[3] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printTemplate + " : " + printData + '<div>';
		} else {
	      var dataExpand = '<div id="' + tdata[3] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + printData + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + printTemplate + " : " + '</label><input type="text" class="formbox" id="threaddata:'+selectedRunNumber+':'+threadArray[s]+':Thread_Threads:'+printData+':'+i+'" name="threaddata" value="'+printData+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+threadArray[s]+':Thread_Threads:'+printData+':'+i+'" value="Update" /></form><div>';
		}
 var dataBox = '<div class="databoxes" id="' + tdata[3] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>';
	      var dataExpandingBox = dataExpand + dataBox;
	      var pcE = document.getElementById(threadArray[threadIndex]+" "+pcArray[j]);
	      $(pcE).find('.threadboxes').append(dataExpandingBox);
              
	      }
	      }
	    }
	    }
      

      //ADD A THREAD MENU FUNCTIONALITY
      if (editable!=='readonly') {
	var threadPC = pcArray[j];
	$(myselect).change(function(){
	    //When the dropdown menu selection changes from its default positioning
	    var selectedThread = $(myselect + "option:selected").val();
	    //Do you want an error check here?
	    var newThreadName = prompt("What do you want to name this thread?",selectedThread);
	    //Copy this new thread into the database
	    var checktest = checkClash('Thread_Threads',newThreadName,threadPC);
	    if (parseInt(checktest) === 0) {
	      alert("That thread name is already taken!");
	    } else {
	      copyIndividual('Thread_Threads',selectedRunNumber,selectedThread,newThreadName,threadPC);

	      var threadAddDivName = "addthread:" + selectedThread;
	      var threadAddString = "'" + threadAddDivName + "'";

	      var threadAddExpand = '<div id="' + threadAddDivName + '"><div class="threadbox" id="' + threadAddDivName + '"><a id="' + threadAddDivName + '" href="javascript:addThreadSlide(' + threadAddString + ');" >&nbsp&nbsp'+selectedThread+' : '+newThreadName+'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+threadremoveable+'</a><div>';
	      var threadAddBox = '<div class="addThreadButtons" id="' + threadAddDivName + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
	      threadAddExpandingBox = threadAddExpand+threadAddBox;
	      $(threadAddExpandingBox).insertAfter(myselect);
	      var addTemplate = getDataFromPHP(selectedThread, threadPC, 'Thread_Templates', selectedRunNumber);
	      var addDefault = getDataFromPHP(selectedThread, threadPC, 'Thread_Defaults', selectedRunNumber);
	    
	      var ttemplate = new Array();
	      var tdefault = new Array();
	      var parse_djstring = $.parseJSON(addDefault);
	      var parse_tjstring = $.parseJSON(addTemplate);
	      $.each(parse_tjstring, function(tindex, tvalue) {
		  ttemplate[tindex] = tvalue;
		});
	      $.each(parse_djstring, function(dindex, dvalue) {
		  tdefault[dindex] = dvalue;
		});
	    
	      for (var i=1; i< ttemplate.length; i++) {
		if (!ttemplate[i]) {
		  ttemplate[i]="NULL";
		}
		if (!tdefault[i]) {
		  tdefault[i]="NULL";
		}
	      
		if ((ttemplate[i] !=  "NULL") || (tdefault[i] != "NULL")) {
		  //check to see if you need the form box. If it is readonly, just display the data
		  var dataExpand = '<div id="AddThread:'+ttemplate[i]+":"+tdefault[i]+'" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + ttemplate[i]+':'+tdefault[i]+'" method="POST" action=""><label for="addThreadData"> &nbsp&nbsp' + ttemplate[i] + " : " + '</label><input type="text" class="formbox" id="AddThread:'+selectedRunNumber+':'+ttemplate[i]+':Thread_Threads:'+tdefault[i]+':'+i+'" name="addthreaddata" value="'+tdefault[i]+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+ttemplate[i]+':Thread_Threads:'+tdefault[i]+':'+i+'" value="Update" /></form><div>';

		  var dataBox = '<div class="databoxes" id="AddThread:'+ttemplate[i]+":"+tdefault[i]+'" display: none; block: padding 5px; width: 500 px; position: relative></div>';
		  var dataExpandingBox = dataExpand + dataBox;
		  var pcE = document.getElementById(threadAddDivName);
		  $(pcE).find('.addThreadButtons').append(dataExpandingBox);
		}
	      }
	    }
	  });
      }
      }      
      //ADD THE MEMORY MANAGER BOXES
      for (var j=0; j < (pcArray.length); j++) {
	
	if (editable!=='readonly') {
	  var addMMSelect = '<select id="addMM-dropdown:' + pcArray[j] + '" class="addMenu" name="addMM-dropdown:' + pcArray[j] + '" ><option>Add a memory manager...</option></select>';
	  var pcE = document.getElementById(pcArray[j]);
	  $(pcE).find('.pcSlideBox').append(addMMSelect);
	  var mmyselect = document.getElementById("addMM-dropdown:" + pcArray[j]);
	}
	
	var typeArray = getThreadType('Memory_MemoryManagers');
	for (var typem=0; typem < typeArray.length; typem++) {
	  $(mmyselect).append('<option value="'+typeArray[typem]+'">'+typeArray[typem]+'</option>');
	}
	
	for (var m = mmPCFlags[j]; m < mmPCFlags[j+1]; m++) {
	  var mmSplit = mmArray[m].split(':');
	  var mmTypeValue = mmSplit[0];
	  var mmNameValue = mmSplit[1];
	  var mmPC = mmSplit[2];
 	  var this_mmType = "'" + mmTypeValue + "'";
	  var this_mmName = "'" + mmNameValue + "'";
	  var mmDivName = mmArray[m] + " " + pcArray[j];
	  var mmString = "'" + mmDivName + "'";
	  
	  var mmExpand = '<div id="' + mmDivName + '"><div class="mmbox" id="' + mmDivName + '"><a id="' + mmDivName + '" href="javascript:mmSlide(' + mmString + ');" >&nbsp&nbsp' + mmTypeValue + " : " + mmNameValue + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+threadremoveable+'</a><div>';
	  var mmBox = '<div class="mmboxes" id="' + mmDivName + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
	  mmExpandingBox = mmExpand + mmBox;
	  var pcE = document.getElementById(pcArray[j]);
	  $(pcE).find('.pcSlideBox').append(mmExpandingBox);
	}

	//	    var stop = mmPCFlags[j+1] - mmPCFlags[j];
	for (var ms = mmPCFlags[j]; ms < mmPCFlags[j+1]; ms++) {  
	  mmIndex = ms;
	  var mmdata = new Array();
	  var mmtemplate = new Array();
	  var mmdefault = new Array();
	  var mmnecessary = new Array();
	  var parse_jstring = $.parseJSON(mmDataArray[j][mmIndex]);
	  var parse_tjstring = $.parseJSON(mmTemplateArray[j][mmIndex]);
	  var parse_djstring = $.parseJSON(mmDefaultArray[j][mmIndex]);
	  var parse_njstring = $.parseJSON(mmNecessaryArray[j][mmIndex]);
	  
	  $.each(parse_jstring, function(index,value) {
	      mmdata[index] = value;
	    });
	  $.each(parse_tjstring, function(tindex, tvalue) {
	      mmtemplate[tindex] = tvalue;
	    });
	  $.each(parse_djstring, function(dindex, dvalue) {
	      mmdefault[dindex] = dvalue;
	    });
	  $.each(parse_njstring, function(nindex, nvalue) {
	      mmnecessary[nindex] = nvalue;
	    });
	  
	  for (var i=5; i< mmdata.length-1; i++) {
	    var printData = mmdata[i];
	    var printTemplate = mmtemplate[i-4];
	    if (!mmdata[i]) {
	      printData = "NULL";
	    }
	    if (!mmtemplate[i-4]) {
	      printTemplate = "NULL";
	    }
	    
	    if((printData==="NULL") && (printDefault!=="NULL")) {
	      var dataExpand = '<div id="' + mmdata[3] + '" class="fixbox"; ><a id="' + mmdata[3] + '" href="javascript::dataSlide(' + mmdata[3] + ');" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printTemplate + " : " + printDefault + " (changed from " + printData + ")" + '</a><div>';
	      
	      var dataBox = '<div class="databoxes" id="' + mmdata[3] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>';
	      var dataExpandingBox = dataExpand + dataBox;
	      var pcE = document.getElementById(mmArray[mmIndex]+" "+pcArray[j]);
	      $(pcE).find('.mmboxes').append(dataExpandingBox);

	      } else if((printData==="NULL")&&(printNecessary==="YES")) {
	      var dataExpand = '<div id="' + mmdata[3] + '" class="errorbox"; ><a id="' + mmdata[3] + '" href="javascript::dataSlide(' + mmdata[3] + ');" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printTemplate + " : " + printData + '</a><div>';

	      var dataBox = '<div class="databoxes" id="' + mmdata[3] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>';
	      var dataExpandingBox = dataExpand + dataBox;
              var pcE = document.getElementById(mmArray[mmIndex]+" "+pcArray[j]);
	      $(pcE).find('.mmboxes').append(dataExpandingBox);

	      } else {
	      if ((printData !=  "NULL") || (printTemplate != "NULL")) {
		//***DEFAULT MM SITUATION - NOW WITH FORMBOXES
		if (editable==='readonly') {
  var dataExpand = '<div id="' + mmdata[3] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printTemplate + " : " + printData + '<div>';
		} else {
		var dataExpand = '<div id="' + mmdata[3] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + printData + '" method="POST" action=""><label for="threaddata">' + printTemplate + " : " + '</label><input type="text" class="formbox" id="threaddata:'+selectedRunNumber+':'+mmArray[ms]+':Memory_MemoryManagers:'+printData+':'+i+'" name="threaddata" value="'+printData+'" '+editable+'/><input type="submit" class="mmupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+mmArray[ms]+':Memory_MemoryManagers:'+printData+':'+i+'" value="Update" /></form><div>';
		}
	      var dataBox = '<div class="databoxes" id="' + mmdata[3] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>';
	      var dataExpandingBox = dataExpand + dataBox;

	      var pcE = document.getElementById(mmArray[mmIndex]+" "+pcArray[j]);
	      $(pcE).find('.mmboxes').append(dataExpandingBox);
	      }
	      }
	    }
	    }
      }      

      //ADD A MEMORY MANAGER MENU FUNCTIONALITY
      if (editable!=='readonly') {
	$(mmyselect).change(function(){
	    //When the dropdown menu selection changes from its default positioning
	    var selectedMM = $(mmyselect).val();
	    //Do you want an error check here?
	    var newMMName = prompt("What do you want to name this memory manager?",selectedMM);
	    //Copy this new mm into the database
	    copyIndividual('Memory_MemoryManagers',selectedRunNumber,selectedMM,newMMName,pcArray[j]);

	    var mmAddDivName = "addmm:" + selectedMM;
	    var mmAddString = "'" + mmAddDivName + "'";

	    var mmAddExpand = '<div id="' + mmAddDivName + '"><div class="mmbox" id="' + mmAddDivName + '"><a id="' + mmAddDivName + '" href="javascript:addMMSlide(' + mmAddString + ');" >&nbsp&nbsp'+selectedMM+' : '+newMMName+'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+threadremoveable+'</a><div>';
	    var mmAddBox = '<div class="addMMButtons" id="' + mmAddDivName + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
	    var selectedMMPC = pcArray[j];
	    mmAddExpandingBox = mmAddExpand+mmAddBox;
	    $(mmAddExpandingBox).insertAfter(mmyselect);
	    var addTemplate = getDataFromPHP(selectedMM, selectedMMPC, 'Memory_Templates', selectedRunNumber);
	    var addDefault = getDataFromPHP(selectedMM, selectedMMPC, 'Memory_Defaults', selectedRunNumber);
	    
	    var mmtemplate = new Array();
	    var mmdefault = new Array();
	    var parse_djstring = $.parseJSON(addDefault);
	    var parse_tjstring = $.parseJSON(addTemplate);
	    $.each(parse_tjstring, function(tindex, tvalue) {
		mmtemplate[tindex] = tvalue;
	      });
	    $.each(parse_djstring, function(dindex, dvalue) {
		mmdefault[dindex] = dvalue;
	      });
	    
	    for (var i=1; i< mmtemplate.length; i++) {
	      if (!mmtemplate[i]) {
		mmtemplate[i]="NULL";
	      }
	      if (!mmdefault[i]) {
		mmdefault[i]="NULL";
	      }
	      
	      if ((mmtemplate[i] !=  "NULL") || (mmdefault[i] != "NULL")) {
		//check to see if you need the form box. If it is readonly, just display the data
		var dataExpand = '<div id="AddMM:'+mmtemplate[i]+":"+mmdefault[i]+'" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + mmtemplate[i]+':'+mmdefault[i]+'" method="POST" action=""><label for="addMMData"> &nbsp&nbsp' + mmtemplate[i] + " : " + '</label><input type="text" class="formbox" id="AddMM:'+selectedRunNumber+':'+mmtemplate[i]+':Memory_MemoryManagers:'+mmdefault[i]+':'+i+'" name="addmmdata" value="'+mmdefault[i]+'" '+editable+'/><input type="submit" class="mmupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+mmtemplate[i]+':Memory_MemoryManagers:'+mmdefault[i]+':'+i+'" value="Update" /></form><div>';

		var dataBox = '<div class="databoxes" id="AddMM:'+mmtemplate[i]+":"+mmdefault[i]+'" display: none; block: padding 5px; width: 500 px; position: relative></div>';
		var dataExpandingBox = dataExpand + dataBox;
		var pcE = document.getElementById(mmAddDivName);
		$(pcE).find('.addMMButtons').append(dataExpandingBox);
	      }
	    }
	  });
      }
      
}

function showSettings(selectedRunNumber, editable) {			      
  if (editable==='readonly') {
    var pcremoveable='';
    var threadremoveable='';
  } else {
    var pcremoveable='<button type="button" id="removepc" class="pcRemoveButton">-</button>';
    var threadremoveable='<button type="button" id="removethread" class="threadRemoveButton">-</button>';

    var addWFDMenu = '<button id="addwfd-dropdown" name="addwfd-dropdown" class="addWFDMenu" onclick="addWFD('+selectedRunNumber+')">Add a WFD...</button>';
    $(addWFDMenu).appendTo("#theultimatewrapper");
  }
  
  var wfdArray = new Array();
  /* AJAX query to get the BoardIDs for a given run */
  $.ajax({
    type: 'POST',
	url: "getWFDs.php",
	async: false,
	data: { runNumber: selectedRunNumber },
	success: function(data){	       
	var wfdNames = $.parseJSON(data);
	$.each(wfdNames, function(wfdIndex,wfdValue) {
	    wfdArray[wfdIndex] = wfdValue;
	  });
      }
    });
  
  //Now all of the WFD IDs are stored in an array - store the settings for each wfd in a new 2d array
  var wfdSettings = new Array(wfdArray.length);
  var wfdSettingNames = new Array(wfdArray.length);
  var pmtArray = new Array(wfdArray.length);
  var pmtSettings = new Array(wfdArray.length);
  var pmtSettingNames = new Array(wfdArray.length);
  
  for (var k=0; k< wfdSettings.length; k++) {
    wfdSettings[k] = new Array();
    wfdSettingNames[k] = new Array();
    pmtArray[k] = new Array();
    pmtSettings[k] = new Array();
    pmtSettingNames[k] = new Array();
  }
  
  //iterate through the array of WFD ids to get the information for each of them
  for (var i=0; i< wfdArray.length; i++) {
    var boardID=wfdArray[i];
    var table1='Settings_WFDs';
    var table2='Settings_PMTs';
    //Get the settings data
    $.ajax({
	  type: 'POST',
	  url: "getWFDSettings.php",
	  async: false,
	  data: {runNumber:selectedRunNumber, boardID:boardID, table:table1},
	  success: function(data) {
	  var parse_wfdSettings = $.parseJSON(data);
	  if(parse_wfdSettings != null){
	    $.each(parse_wfdSettings, function(settingIndex, settingData) {
		wfdSettings[i][settingIndex] = settingData;
	      });
	  }	 
	}
      });
    
    //Get the column names for the settings field
    var settings_names = getColumnNamesFromPHP(boardID, '', "Settings_WFDs", selectedRunNumber);
    var parse_settings_names = $.parseJSON(settings_names);
    $.each(parse_settings_names, function(nameIndex, nameValue) {
	wfdSettingNames[i][nameIndex] = nameValue;
      });
  }
  
      for (var i=0; i< wfdArray.length; i++) {
	var boardID=wfdArray[i];
      //Get the channels for each WFD (PMT pos)
      $.ajax({
	type: 'POST',
	    url: "getPMTs.php",
	    async: false,
	    data: {runNumber:selectedRunNumber, boardID:boardID},
	    success: function(data) {
	    var parse_pmts=$.parseJSON(data);
	    if(parse_pmts != null){
	    $.each(parse_pmts, function(pmtIndex, pmtValue) {
		pmtArray[i][pmtIndex] = pmtValue;
	      });
	    }
	  }
	});

      //Get the settings for each channel      
      for (var m=0; m< pmtArray[i].length; m++) {
	$.ajax({
 	  type: 'POST',
	      url: "getPMTSettings.php",
	      async: false,
	      data: {runNumber:selectedRunNumber, boardID:boardID, channel:m},
	      success: function(data) {
	      pmtArray[i][m] = data;
	    }
	  });	      
      }
      }

	for (var i=0; i< wfdArray.length; i++) {
      //Get the column names for each channel
     	var pmt_names = getColumnNamesFromPHP(boardID, pmtArray[m], "Settings_PMTs", selectedRunNumber);
	var parse_pmt_names = $.parseJSON(pmt_names);
	$.each(parse_pmt_names, function(nameIndex, nameValue) {
	    pmtSettingNames[i][nameIndex] = nameValue;
	    
	  });
	}
				    
      
      
      //***DISPLAY ALL OF THE WFD BOXES, AND THE SETTINGS/PMTs WITHIN THEM
      var wfdExpandingBox;
      for (var j=0; j < wfdArray.length; j++) {
	//make the boxes
	
      var wfdString = "'" + wfdArray[j] + "'";
		
      var wfdExpand = '<div id="WFD' + wfdArray[j] + '"><div class="wfdbutton" id=' + wfdArray[j]+ '"><a id="' + wfdArray[j] + '" href="javascript:wfdSlide(' + wfdString + ');" ><span style="color: <?php echo $pc_text ?>;">&nbsp&nbsp WFD ' + wfdArray[j] + '</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+pcremoveable+'</a><div>';

      var wfdBox = '<div class="wfdSlideBox" id="' + wfdArray[j] + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
      wfdExpandingBox = wfdExpand+wfdBox;
      $(wfdExpandingBox).appendTo('#rightwrapper');

      var settingDivName = "Settings:"+wfdArray[j];
      var settingString = "'" + settingDivName + "'";
      //***DISPLAY THE SETTINGS IN A DROPDOWN BOX INSIDE THE WFD BOX
      var settingExpand = '<div id="' + settingDivName + '"><div class="threadbox" id="' + settingDivName + '"><a id="' + settingDivName + '" href="javascript:settingsSlide(' + settingString + ');" >&nbsp&nbsp Settings &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a></div>';
      var settingBox = '<div class="settingsboxes" id="' + settingDivName + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
      var settingExpandingBox = settingExpand + settingBox;
      var pcE = document.getElementById('WFD'+wfdArray[j]);
      $(pcE).find('.wfdSlideBox').append(settingExpandingBox);      
		      
      //***DISPLAY THE EDITABLE SETTINGS IN THE SETTINGS DROPDOWN TAB
      for (var k=1; k< wfdSettings[j].length; k++) {
	var i=k-1;
	var printSetting = wfdSettings[j][k];
	var printName = wfdSettingNames[j][k];
			
	if (editable==='readonly') {
	  var dataExpand = '<div id="' + printSetting + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + printName + " : " + printSetting + '<div>';
	} else {
	  var dataExpand = '<div id="' + printSetting + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + printSetting + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + printName + " : " + '</label><input type="text" class="formbox" id="threaddata:'+selectedRunNumber+':'+printSetting+':'+printName+':Settings_WFDs:'+i+'" name="threaddata" value="'+printSetting+'" '+editable+'/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+printSetting+':'+printName+':Settings_WFDs:'+i+'" value="Update" /></form><div>';
	}
	var dataBox = '<div class="databoxes" id="' + printSetting + '" display: none; block: padding 5px; width: 500 px; position: relative></div>'; 	      
	var dataExpandingBox = dataExpand + dataBox;
	var pcE = document.getElementById('WFD'+wfdArray[j]);
	$(pcE).find('.settingsboxes').append(dataExpandingBox);
      }

      if (editable!=='readonly') {
	var addPMTSelect = '<button id="addPMT-dropdown:' + wfdArray[j] + '" class="addMenu" name="addPMT-dropdown:' + wfdArray[j] + '" onclick="addPMT('+selectedRunNumber+','+wfdArray[j]+')">Add a Channel...</button>';
	var pcE = document.getElementById('WFD'+wfdArray[j]);
	$(pcE).find('.wfdSlideBox').append(addPMTSelect);
	var myselect = document.getElementById("addPMT-dropdown:" + wfdArray[j]);
      }


      for (var pmtl=0; pmtl < pmtArray[j].length; pmtl++) {
	//var printPMT = pmtArray[j][pmtl];
	var pmtData = new Array();
	var printPMT=pmtl;
	var pmtString = "'" + printPMT + "'";
	var pmtDivName = "PMT:" + printPMT + ":WFD:" + wfdArray[j];
	var pmtExpand = '<div id="PMT' + pmtDivName + '"><div class="mmbox" id="' + pmtDivName + '"><a id="' + pmtDivName + '" href="javascript:channelSlide(' + pmtString + ');" >&nbsp&nbspChannel ' + printPMT + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+threadremoveable+'</a><div>';
	var pmtBox = '<div class="channelboxes" id="' + printPMT + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
	pmtExpandingBox = pmtExpand + pmtBox;
	
	var pcE = document.getElementById('WFD'+wfdArray[j]);
	$(pcE).find('.wfdSlideBox').append(pmtExpandingBox);
	
	var parse_pmtData = $.parseJSON(pmtArray[j][pmtl]);
	$.each(parse_pmtData, function(pmtIndex,pmtValue) {
	    pmtData[pmtIndex] = pmtValue;
	  });
	
	for (var z=1; z< pmtData.length; z++) {		      
	  if (editable==='readonly') {
	    var dataExpand = '<div id="' + pmtData[z] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + pmtSettingNames[j][z] + " : " + pmtData[z] + '<div>';
	  } else {
	    var dataExpand = '<div id="' + pmtData[z] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + pmtData[z] + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + pmtSettingNames[j][z] + " : " + '</label><input type="text" class="formbox" id="threaddata:'+selectedRunNumber+':'+pmtl+':'+wfdArray[j]+':Settings_PMTs:'+pmtSettingNames[j][z]+'" name="threaddata" value="'+pmtData[z]+'" '+editable+'/><input type="submit" class="mmupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':'+pmtl+':'+wfdArray[j]+':Settings_PMTs:'+pmtSettingNames[j][z]+'" value="Update" /></form><div>';
	  }
	  var dataBox = '<div class="databoxes" id="' + pmtData[z] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>'; 	      
	  var dataExpandingBox = dataExpand + dataBox;
	  var pcE = document.getElementById(pmtDivName);
	  $(pcE).find('.channelboxes').append(dataExpandingBox);
	}	      
      }
      }
}

function getIPs(selectedRunNumber) {
  var ipArray = new Array();
  /* AJAX query to get the PCs for a given run */
  $.ajax({
    type: 'POST',
	url: "getIPs.php",
	async: false,
	data: { runNumber: selectedRunNumber },
	success: function(ip_data){	       
	var ipNames = $.parseJSON(ip_data);
	$.each(ipNames, function(ipIndex,ipValue) {
	    ipArray[ipIndex] = ipValue;
	  });
      }
    });
  
  //Now we have all of the IP addresses (in ipArray). Are they alive?
  /* for (var i=0; i< ipArray.length; i++) { */
  /*   var ipSplit = ipArray[i].split(':'); */
  /*   var address = ipSplit[0]; */
  /*   $.ajax({ */
  /*     type: 'POST', */
  /* 	  url: "testPing.php", */
  /* 	  async: false, */
  /* 	  data: { ipAddress: address }, */
  /* 	  success: function(is_alive) { */
  /* 	  var id = 'PCUP-'+address; */
  /* 	  var ipel = document.getElementById(id); */
  /* 	  if (is_alive === '1') { */
  /* 	    //var bgcolor = <?php echo $bright_red ?>; */
  /* 	    //ipel.style.backgroundColor = bgcolor; */
  /* 	    ipel.style.backgroundColor = 'green'; */
  /* 	  } else { */
  /* 	    ipel.style.backgroundColor = '#000000'; */
  /* 	  } */
  /* 	} */
  /*     }); */
  /* } */
}

function ipClick(restartIP) {
  $.ajax({
    type: 'POST',
	url: "restart.php",
	async: false,
	data: { restartIP:restartIP },
    success: function(ip_success) {
	//	alert(ip_success);
	//writing to a file isn't working....but the ip address does get passed to the restart.php script
      }
    });
}

function addWFD(selectedRunNumber) {
  var newWFD = prompt("What do you want to number this WFD?",0);
  var wfdtest = checkClash('Settings_WFDs',newWFD,'');
  if (parseInt(wfdtest) === 0) {
    alert("That thread name is already taken!");
  } else {
    var wfdCols = getColumnNamesFromPHP(1,'','Settings_WFDs',0);
    var parse_wfdCols = $.parseJSON(wfdCols);
    var wfdString = "'" + newWFD + "'";

    //Now append a new WFD button that will contain all of these values
    var wfdExpandingBox;
    var pcremoveable='<button type="button" id="removepc" class="pcRemoveButton">-</button>';
    var wfdExpand = '<div id="WFD' + newWFD + '"><div class="wfdbutton" id=' + newWFD + '"><a id="' + newWFD + '" href="javascript:wfdSlide(' + wfdString + ');" ><span style="color: <?php echo $pc_text ?>;">&nbsp&nbsp WFD ' + newWFD + '</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+pcremoveable+'</a><div>';
      var wfdBox = '<div class="wfdSlideBox" id="' + newWFD + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
      wfdExpandingBox = wfdExpand+wfdBox;
      $(wfdExpandingBox).appendTo('#rightwrapper');

      var settingDivName = "Settings:"+newWFD;
      var settingString = "'" + settingDivName + "'";
      //***DISPLAY THE SETTINGS IN A DROPDOWN BOX INSIDE THE WFD BOX
      var settingExpand = '<div id="' + settingDivName + '"><div class="threadbox" id="' + settingDivName + '"><a id="' + settingDivName + '" href="javascript:settingsSlide(' + settingString + ');" >&nbsp&nbsp Settings &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a></div>';
      var settingBox = '<div class="settingsboxes" id="' + settingDivName + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
      var settingExpandingBox = settingExpand + settingBox;
      var pcE = document.getElementById('WFD'+newWFD);
      $(pcE).find('.wfdSlideBox').append(settingExpandingBox);

      for (var z=1; z< parse_wfdCols.length; z++) {
	var dataExpand = '<div id="' + parse_wfdCols[z] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + parse_wfdCols[z] + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + parse_wfdCols[z] + " : " + '</label><input type="text" class="formbox" id="threaddata:'+selectedRunNumber+':0:'+parse_wfdCols[z]+':Settings_WFDs:'+z+'" name="threaddata" value="0"/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':0:'+parse_wfdCols[z]+':Settings_WFDs:'+z+'" value="Update" /></form><div>';
	var dataBox = '<div class="databoxes" id="' + parse_wfdCols[z] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>'; 	      
	var dataExpandingBox = dataExpand + dataBox;
	var pcE = document.getElementById(settingDivName);
	$(pcE).find('.settingsboxes').append(dataExpandingBox);
      }
  }
}

function addPMT(selectedRunNumber,WFDnum) {
  var newPMT = prompt("What do you want to number this channel?",0);
  var pmttest = checkClash('Settings_PMTs',newPMT,WFDnum);
  if (parseInt(pmttest) === 0) {
    alert("That thread name is already taken!");
  } else {
    var pmtCols = getColumnNamesFromPHP(1,'','Settings_PMTs',0);
    var parse_pmtCols = $.parseJSON(pmtCols);
    var pmtString = "'" + newPMT + "'";

    //Now append a new PMT button that will contain all of these values
    var pmtExpandingBox;
    var pcremoveable='<button type="button" id="removepc" class="pcRemoveButton">-</button>';
    var pmtExpand = '<div id="PMT' + newPMT + '"><div class="mmbox" id=' + newPMT + '"><a id="' + newPMT + '" href="javascript:channelSlide(' + pmtString + ');" ><span>&nbsp&nbspChannel ' + newPMT + '</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'+pcremoveable+'</a><div>';
      var pmtBox = '<div class="channelboxes" id="' + newPMT + '" style="display: none; block:padding 5px; height: auto; position:relative;"></div>';
      pmtExpandingBox = pmtExpand+pmtBox;
      var pcE = document.getElementById('WFD'+WFDnum);
      $(pcE).find('.wfdSlideBox').append(pmtExpandingBox);

      for (var z=1; z< parse_pmtCols.length; z++) {
	var dataExpand = '<div id="' + parse_pmtCols[z] + '" class="databox"; >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<form name="myform" id="myform:' + parse_pmtCols[z] + '" method="POST" action=""><label for="threaddata"> &nbsp&nbsp' + parse_pmtCols[z] + " : " + '</label><input type="text" class="formbox" id="threaddata:'+selectedRunNumber+':0:'+parse_pmtCols[z]+':Settings_PMTs:'+z+'" name="threaddata" value="0"/><input type="submit" class="tupdatebutton" name="update" id="formsubmitted:' +selectedRunNumber+':0:'+parse_pmtCols[z]+':Settings_PMTs:'+z+'" value="Update" /></form><div>';
	var dataBox = '<div class="databoxes" id="' + parse_pmtCols[z] + '" display: none; block: padding 5px; width: 500 px; position: relative></div>'; 	      
	var dataExpandingBox = dataExpand + dataBox;
	var pcE = document.getElementById('PMT'+newPMT);
	$(pcE).find('.channelboxes').append(dataExpandingBox);

      }
  }
}

$(document).ready(function(){
    //Create the "Choose a run..." sliding menu bar

    $("[id=runupload-dropdown]").append('<option value="choose">Display a run...</option>');
    $("[id=runupload-dropdown]").appendTo("#theultimatewrapper");
    $("[id=runcopy-dropdown]").append('<option value="copy">Load run template...</opion>');
    $("[id=runcopy-dropdown]").appendTo("#theultimatewrapper");
    
    $("#runcopy-label").appendTo("#theultimatewrapper");
    $("#runupload-label").appendTo("#theultimatewrapper");
    var table = "RunNames";
    getRunsFromPHP(table);
    var divider = "'<option disabled>----------------------------</option>'";
    $("[id=runupload-dropdown]").append(divider);
    var table2 = "Thread_Threads";
    getRunsFromPHP(table2);

    

    /* Make the Remove PC button remove PCs */
    $("#removepc").live("click", "#removepc", function() {
	var pc_conf = confirm("Are you sure you want to remove this?");
	if (pc_conf) {
	$(this).parent().parent().remove();
	}
      });

    $("#removeThread").live("click","#removeThread",function(){
	var thread_conf = confirm("Are you sure you want to remove this?");
	if (thread_conf) {
	$(this).parent().remove();
	}
      });  


    //This is where the copy-runs bar calls the copyRun function  
    $("[id=runcopy-dropdown]").change(function(){
	var answer=confirm("Load Selected Run?  This will delete current 'run zero' and load your setting in place.");
	if(answer)
	  {
	    $("#leftwrapper").empty();
	    $("#rightwrapper").empty();
	    $("#pcwrapper").empty();
	    $("#bannerdiv").empty();
 	    var runSelector="#runcopy-dropdown";
	    var selectedRun = $(runSelector).val();

	    //Clear current data stored in run 0
	    clearExistingData();
	    copyRunToRunZero(selectedRun);
	    //Display selected run
	    var editable='';
    	    var selectedRunSplit = selectedRun.split(':');
	    var selectedRunNumber = selectedRunSplit[0];
	    showRun(selectedRunNumber, editable);
	    showSettings(selectedRunNumber, editable);
	    getIPs(selectedRunNumber);
            setInterval(function() {getIPs(selectedRunNumber)}, <?php echo $pingTime ?>);
	  }
	    		    
      });
    
    //Dropdown menu for "display a run" bar
  $("[id=runupload-dropdown]").change(function(){	
      $("#leftwrapper").empty();
      $("#rightwrapper").empty();
      $("#pcwrapper").empty();
      $("#bannerdiv").empty();
      var runSelector = "#runupload-dropdown";
      var selectedRun = $(runSelector).val();
      var editable = 'readonly';
      var pcremoveable='';
      var threadremoveable='';
      var selectedRunSplit = selectedRun.split(':');
      var selectedRunNumber = selectedRunSplit[0];
      showRun(selectedRunNumber, editable);
      showSettings(selectedRunNumber, editable);
      getIPs(selectedRunNumber);
      setInterval(function() {getIPs(selectedRunNumber)},<?php echo $pingTime ?>);
    });

				      
  /*Start button functionality */  
  $("#go").live('click',function() {
      var conf = confirm("Load selected run?");
      if (conf) {
	/*Go into RUNLOCK and change the setting */
	startRun();
	$("#go").hide();
	$("#stop").show();
      }
    });
  $("#go").appendTo("#theultimatewrapper");



  /*Stop button functionality */
  $("#stop").live('click',function() {
      var conf = confirm("Are you sure you want to stop the current run?");
      if (conf) {
	/*Go into RUNLOCK and change the setting */
	stopRun();
	$("#stop").hide();
	$("#go").show();

	$("#pcwrapper").empty();
	$("#leftwrapper").empty();
	$("#rightwrapper").empty();
      }
    });
  $("#stop").appendTo("#theultimatewrapper");
  
  $("#current_status").appendTo("#theultimatewrapper");
  //Current status/ run number operations
  halt = false;

  var check = function(callback) {
    // this function gets the current status stored in the RUNLOCK table
    $.ajax({
      type: 'GET',
	  url: "checkStatus.php",
	  success: function(current_status) {
	  var status = $.parseJSON(current_status);
	  callback(String(status));
	}
      });
  }
  
  var callback = function(status) {
    var statusSplit = status.split(':');
    var theDiv = document.getElementById("current_status");
    if (statusSplit[0] === "RUNNING") {
      $("#go").hide();
      $("#stop").show();
      theDiv.innerHTML = "Current Run Number: " + statusSplit[1];
      if (window.halt === false) {
	//If it's running, get the current run number and display it(not editable)
	$("#leftwrapper").empty();
	$("#rightwrapper").empty();
	$("#pcwrapper").empty();
	showRun(statusSplit[1], "readonly");
	showSettings(statusSplit[1], "readonly");
	getIPs(statusSplit[1]);
	setInterval(function() {getIPs(statusSplit[1])},<?php echo $pingTime ?>);
	var banner = '<div id="bannerdiv" class="bannerdiv">Currently Running!</div>';
	$(banner).insertBefore("#container");
	halt = true;
      }
    } else {
      if (statusSplit[0] === "NEW_RUN") {
	$("#go").hide();
	$("#stop").show();
      } else {
	$("#go").show();
	$("#stop").hide();
	$("#bannerdiv").empty();
	halt = false;
	theDiv.innerHTML = "Current status: " + statusSplit[0];
      }
    }
  }
  
  check(callback);
  //This is the timer function that checks every set amount of time and then checks the current state
  setInterval(function() {check(callback)}, <?php echo $statusTime ?>);
  //Using this: check(callback); <- callback function - will call the ajax function to grab it from the db
  
  //UPDATE BUTTON FUNCTIONALITY
  //AJAX to submit the data in that form without refreshing the page
  
  $(':submit').live('click', function() {
      //Grab the id from the clicked button
      var subid = $(this).attr('id');
      var pdata0 = subid.split(":");
      var pdata1 = pdata0.slice(1,pdata0.length);
      //replace : into the pdata1 array
      var pdata=pdata1[0];
      for (var pdl=1;pdl < pdata1.length;pdl++) {
	pdata=pdata + ":" + pdata1[pdl];
      }
      //Now grab the value from the text box
      var els = document.getElementById("threaddata:"+pdata);
      var input_data = $(els).val();
      if (pdata0[4]=='Settings_WFDs') {
	var table=pdata0[4];
	var nameID=pdata0[5];
	var pcName='';
	var dataIndex=pdata0[3];
      } else if ((pdata0[4]=='SThread_Data_Raw') || (pdata0[4]=='SThread_Data_Compressed') || (pdata0[4]=='SThread_DecisionMaker') || (pdata0[4]=='Settings_PMTs')) { 
	var table=pdata0[4];
	var nameID=pdata0[3];
	var pcName=pdata0[2];
	var dataIndex=pdata0[5];
      } else {
	var table=pdata0[5];
	var nameID=pdata0[3];
	var pcName=pdata0[4];
	var dataIndex=pdata0[7];
      }
      $.ajax({
	type:'POST',
	    data: {nameID:nameID, pcName:pcName, table:table, input_data:input_data, dataindex:dataIndex}, 
	    url: 'processData.php',
	    success: function(data) {
	    var buttonel = document.getElementById(subid);
	    buttonel.value = "Updated!";
	  }
	});
      return false;
    });
  });

				    
</script>
</head>
<body>

<div id="headerdiv" class="headerdiv">
<div id="logodiv" class="logodiv"></div>
<!--Change this to change the text in the banner at the top-->
MiniCLEAN DAQ Control
</div>
<div id="runcopy-label" name="runcopy-label" class="runCopyLabel">Load run template...</div>
<select id="runcopy-dropdown" name="runcopy-dropdown" class="runCopyMenu"></select></div>
<div id="runupload-label" name="runupload-label" class="runMenuLabel">Display a run...</div>
<select id="runupload-dropdown" name="runupload-dropdown" class="runMenu"></select>
<button type="button" id="go" class="goButton">Start Run</button>
<button type="button" id="stop" class="stopButton">Stop Run</button>
<div id="current_status" class="statusbox"></div>
<div id="theultimatewrapper" class="contentdiv">
<div id="container" class="container">
<div id="leftwrapper" class="leftwrapper"></div>
<div id="rightwrapper" class="rightwrapper"></div>
<div id="pcwrapper" class="pcwrapper"></div>
<div style="clear:both;"></div>
</div>

</div>		
</body>
</html>

