--Takes 50MHz DATA_IN and serializes it to 350MHZ DATA_OUT.
--Boston University CLEAN project--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity serializer is Port(
	CLK_IN : in STD_LOGIC;
   CLK_OUT : out STD_LOGIC_VECTOR(3 downto 0);
   DATA_IN : in STD_LOGIC_VECTOR(111 downto 0);
   DATA_OUT : out STD_LOGIC_VECTOR(15 downto 0));
end serializer;

architecture Behavioral of serializer is
	type state_type is (st1, st2, st3, st4, st5, st6, st7);
	signal state : state_type := st1;
begin
	process(CLK_IN)
	begin
		if CLK_IN'event and CLK_IN = '1' then
			case state is
			when st1 =>
				state <= st7;
				DATA_OUT <= DATA_IN(105)&DATA_IN(98)&DATA_IN(91)&DATA_IN(84)&
								DATA_IN(77)&DATA_IN(70)&DATA_IN(63)&DATA_IN(56)&
								DATA_IN(49)&DATA_IN(42)&DATA_IN(35)&DATA_IN(28)&
								DATA_IN(21)&DATA_IN(14)&DATA_IN(7)&DATA_IN(0);
				CLK_OUT <= "0011";
			when st2 =>
				state <= st1;
				DATA_OUT <= DATA_IN(106)&DATA_IN(99)&DATA_IN(92)&DATA_IN(85)&
								DATA_IN(78)&DATA_IN(71)&DATA_IN(64)&DATA_IN(57)&
								DATA_IN(50)&DATA_IN(43)&DATA_IN(36)&DATA_IN(29)&
								DATA_IN(22)&DATA_IN(15)&DATA_IN(8)&DATA_IN(1);
				CLK_OUT <= "0011";
			when st3 =>
				state <= st2;
				DATA_OUT <= DATA_IN(107)&DATA_IN(100)&DATA_IN(93)&DATA_IN(86)&
								DATA_IN(79)&DATA_IN(72)&DATA_IN(65)&DATA_IN(58)&
								DATA_IN(51)&DATA_IN(44)&DATA_IN(37)&DATA_IN(30)&
								DATA_IN(23)&DATA_IN(16)&DATA_IN(9)&DATA_IN(2);
				CLK_OUT <= "1100";
			when st4 =>
				state <= st3;
				DATA_OUT <= DATA_IN(108)&DATA_IN(101)&DATA_IN(94)&DATA_IN(87)&
								DATA_IN(80)&DATA_IN(73)&DATA_IN(66)&DATA_IN(59)&
								DATA_IN(52)&DATA_IN(45)&DATA_IN(38)&DATA_IN(31)&
								DATA_IN(24)&DATA_IN(17)&DATA_IN(10)&DATA_IN(3);
				CLK_OUT <= "1100";
			when st5 =>
				state <= st4;
				DATA_OUT <= DATA_IN(109)&DATA_IN(102)&DATA_IN(95)&DATA_IN(88)&
								DATA_IN(81)&DATA_IN(74)&DATA_IN(67)&DATA_IN(60)&
								DATA_IN(53)&DATA_IN(46)&DATA_IN(39)&DATA_IN(32)&
								DATA_IN(25)&DATA_IN(18)&DATA_IN(11)&DATA_IN(4);
				CLK_OUT <= "1100";
			when st6 =>
				state <= st5;
				DATA_OUT <= DATA_IN(110)&DATA_IN(103)&DATA_IN(96)&DATA_IN(89)&
								DATA_IN(82)&DATA_IN(75)&DATA_IN(68)&DATA_IN(61)&
								DATA_IN(54)&DATA_IN(47)&DATA_IN(40)&DATA_IN(33)&
								DATA_IN(26)&DATA_IN(19)&DATA_IN(12)&DATA_IN(5);
				CLK_OUT <= "0011";
			when st7 =>
				state <= st6;
				DATA_OUT <= DATA_IN(111)&DATA_IN(104)&DATA_IN(97)&DATA_IN(90)&
								DATA_IN(83)&DATA_IN(76)&DATA_IN(69)&DATA_IN(62)&
								DATA_IN(55)&DATA_IN(48)&DATA_IN(41)&DATA_IN(34)&
								DATA_IN(27)&DATA_IN(20)&DATA_IN(13)&DATA_IN(6);
				CLK_OUT <= "0011";
			end case;
		end if;
	end process;
end Behavioral;

