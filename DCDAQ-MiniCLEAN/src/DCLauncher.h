#ifndef __DCLAUNCHER__
#define __DCLAUNCHER__

//System includes
#include <iostream>
#include <vector>
#include <map>
#include <string>

#include <boost/algorithm/string.hpp>

//DCDAQ includes
#include <DCThread.h>
#include <MemoryManager.h>
#include <xmlHelper.h>
#include <SelectErrorIgnore.h>

//Error code includes
#include <DCLauncher_ErrorCodes.h>

class DCLauncher
{
 public:
 DCLauncher():selectErrorIgnore()
    {
      SleepTime.tv_sec = 1; // 10 seconds
      SleepTime.tv_usec = 0;
      lastUpdateTime = time(NULL);
      UpdateTime = 60; //Seconds
      maxThreadStopsSent = 3; //Force kill a thread after asking it to stop 10 times
      lastStopCheck = 0;
      stopWait = 2; //seconds
    };
  ~DCLauncher(){Shutdown();selectErrorIgnore.PrintReport();};
  //Shutdown active DCDAQ sessions for exit
  void Shutdown();


  //Thread/MM creation interface
  uint64_t Launch(std::string config);
  
  //Interfaces for DCDAQ main loop
  const std::map<int,size_t> & GetThreadFDMap();
  const std::map<int,size_t> & GetMMFDMap();
  void GetFDSets(fd_set & retReadSet,
		 fd_set & retWriteSet,
		 fd_set & retExceptionSet,
		 int    & retMaxFDPlusOne,
		 struct timeval & retSleepTime);
  void ProcessTimeout();
  void StopAllThreads(){if(lastStopCheck ==0){lastStopCheck = time(NULL)-1;} StopThreads(thread);}
  bool DeleteThread(size_t pos,std::vector<DCThread*> & ThreadVector);
  bool DeleteThread(size_t pos);
  SelectErrorIgnore<LockedObject> * GetSelectErrorIgnore(){return(&selectErrorIgnore);};
  

  //Interfaces for DCMessageProcessor
  const std::vector<DCThread *> & GetThreads() {return(thread);}
  const std::vector<MemoryManager *> & GetMemoryManagers() {return(memoryManager);}
  
 private:
  //Copy constructors (not allowed)(not implimented)
  DCLauncher(const DCLauncher & rhs);
  DCLauncher & operator=(const DCLauncher &rhs);
  
  //Setup functions for groups of memory managers and threads
  uint64_t SetupMemoryManagers(xmlNode * MMNode,
			       //This is the map of newly created memory 
			       //managers that will be merged into 
			       //memoryManager if the thread setup works. 
			       std::vector<MemoryManager *> &tempMemoryManager);
  uint64_t SetupThreads(xmlNode * ThreadNode,
			//vector of to be made threads
			//will be merged into threads if everything
			//works. 
			std::vector<DCThread *> &tempThread,
			//This is the map of newly created memory 
			//managers that will be merged into 
			//memoryManager if the thread setup works. 
			std::vector<MemoryManager *> &tempMemoryManager);

  bool AddMemoryManager(std::vector<MemoryManager *> &MM);
  bool AddThread(std::vector<DCThread *> & T);

  //Shutdown cleanup of threads and memory managers
  void ShutdownMemoryManagerMap(std::vector<MemoryManager *> &MM);
  void DeleteMemoryManagerMap(std::vector<MemoryManager *> &MM);
  void DeleteThreadVector(std::vector<DCThread *> & T);
  void StopThreads(std::vector<DCThread *> & T);

  //Select information from threads
  int MaxFDPlusOne;                    //Current FDmax plus one for select
  std::map<int,size_t> FDtoThreadMap;  //Current mapping between FD and thread
  std::map<int,size_t> FDtoMMMap;  //Current mapping between FD and thread
  fd_set ReadSet;                      //Current fd sets
  fd_set WriteSet;
  fd_set ExceptionSet;
  long MaxSleepTime;
  struct timeval SleepTime;

  time_t lastUpdateTime;
  time_t UpdateTime;

  void UpdateFD();

  //Error monitoring for select
  SelectErrorIgnore<LockedObject> selectErrorIgnore;

  //Vectors or in-use memory managers and threads
  std::vector<MemoryManager *> memoryManager;
  std::vector<DCThread*> thread;
  std::map<DCThread*,size_t> threadStopsSent;  
  size_t maxThreadStopsSent;
  time_t lastStopCheck;
  time_t stopWait;
};

#endif
