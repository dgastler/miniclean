<?php
include "ui.class.php";
if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_POST);
}

$nameID=$_POST['nameID'];
$pcName=$_POST['pcName'];
$table=$_POST['table'];
$data = $_POST['input_data'];
$dataindex=$_POST['dataindex'];
//Dataindex offset  - data_01 corresponds to the 5th index in the json string
if ($table=='Thread_Threads' || $table=='Memory_MemoryManagers') {
  $dataindex=$dataindex-4;
}

$thread_success=$opt->processData($pcName,$nameID,$table,$data,$dataindex);
echo json_encode($thread_success) . PHP_EOL;
?>