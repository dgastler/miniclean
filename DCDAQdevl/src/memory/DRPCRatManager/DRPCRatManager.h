#ifndef __DRPCRATMANAGER__
#define __DRPCRATMANAGER__


#include <MemoryManager.h>
#include <xmlHelper.h>
#include <DRPCBlock.h>
#include "RAT/DS/EV.hh"

#include <DCVectorHash.h>
#include <DCQueue.h>

//#define LOCK_EVERYTHING

class DRPCRATManager : public MemoryManager
//class DRPCRATManager : public MemoryManager, public LockedObject
//class DRPCRATManager : public MemoryManager, public NotLockedObject
{
 public:
  DRPCRATManager(std::string Name);
  ~DRPCRATManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  //Block access
  DRPCBlock * GetDRPCBlock(uint32_t iDRPC) 
    {
      if(iDRPC < Memory.size())
	{
	  return(&(Memory[iDRPC]));
      }
    return(NULL);
    };  
  //FD access
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetUnProcessedFDs(){return(UnProcessed.GetFDs());};
  const int * GetRespondFDs(){return(Respond.GetFDs());};
  const int * GetBadEventFDs(){return(BadEvent.GetFDs());};

  //Shutdown
  virtual void Shutdown()
  {
    //This object
    Lock.ShutdownLockWait();
    //Child objects
    Free.ShutdownWait();
    UnProcessed.ShutdownWait();
    EventStore.ShutdownWait();
    Respond.ShutdownWait();
    BadEvent.ShutdownWait();

    WakeUp();
  };

  //Get the defined bad value
  int32_t GetBadValue(){return(EventStore.GetBadValue());};

  //Un-sent queue
  bool AddFree(int32_t &index);

  //Un-sent queue
  bool GetUnProcessed(int32_t &index);

  //To-sent queue
  bool GetRespond(int32_t &index);
  bool AddRespond(int32_t &index);

  //BadEvent queue
  bool GetBadEvent(int32_t &index);
  bool AddBadEventNOFD(int32_t &index);
  bool AddBadEvent(int32_t &index);


  //Vector hash access
  int64_t GetEvent(int64_t Event,int32_t &index,uint64_t &password);
  int64_t SetEvent(int64_t Event,int32_t &index,uint64_t password);
  int64_t ClearEvent(int64_t key, uint64_t password);
     
  //Autopassing functions
  int64_t MoveEventToUnProcessedQueue(int64_t Event,int32_t &index,uint64_t password);
  int64_t MoveEventToRespondQueue(int64_t Event,int32_t &index,uint64_t password);
  int64_t MoveEventToBadQueue(int64_t Event,int32_t &index,uint64_t password);
 
  virtual void PrintStatus(int indent =0);

  virtual void WakeUp()
  {
    //THis object
    Lock.WakeUpSignal();
    //child objects
    Free.WakeUp();
    UnProcessed.WakeUp();
    EventStore.WakeUp();
    Respond.WakeUp();
    BadEvent.WakeUp();
  }

 private: 
  uint32_t NumDRPCBlocks;
  uint32_t VectorHashSize;
  std::vector<DRPCBlock> Memory;

  LockedObject Lock;
  
  //Index management 
  DCQueue<int32_t,PipeSelect,NotLockedObject> Free;  //queue of indicies of free memory blocks
  DCQueue<int32_t,PipeSelect,NotLockedObject> UnProcessed;  //queue of indicies of un-processed memory blocks
  DCVectorHash<int32_t,NotLockedObject> EventStore; //vector-hash of stored events.
  DCQueue<int32_t,PipeSelect,NotLockedObject> Respond; //queue of indicies to send back with the decision.
  DCQueue<int32_t,PipeSelect,NotLockedObject> BadEvent; //queue of indexes that have collided. 
  
  void Deallocate();
};

#endif
