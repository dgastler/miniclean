#ifndef __INFO_TYPES__
#define __INFO_TYPES__

#include <string>
#include <boost/integer/static_log2.hpp>

namespace InfoTypes 
{
  //watch out for 32/64bits
  enum Type
  {
    NONE           = 0x0000,
    
    WFD_COUNT      = 0x0001,
    PMT_COUNT      = WFD_COUNT << 1,
    BOARD_NODE     = PMT_COUNT << 1,
    CHANNEL_MAP    = BOARD_NODE << 1,
    PMT_OFFSET     = CHANNEL_MAP << 1,
    PMT_PRESAMPLES = PMT_OFFSET << 1,
    PMT_ID         = PMT_PRESAMPLES << 1,
    PMT_TYPE       = PMT_ID << 1,
    
    EVENT_START    = PMT_TYPE << 1,
    EVENT_END      = EVENT_START << 1,
    EVENT_PROMPT   = EVENT_END << 1
    //YOU MUST HAVE THE LAST ENTRY HERE USED IN count BELOW!
  };

  //  const int COUNT = int(log2(EVENT_PROMPT)) + 2;
  const int COUNT = int(boost::static_log2<EVENT_PROMPT>::value) + 2;

  const char * GetTypeName(InfoTypes::Type type);
  InfoTypes::Type GetType(const char * name);
};
#endif
