#ifdef __RATDS__
#include <FEPusher.h>

FEPusher::FEPusher(std::string Name)
{
  Ready = false;
  SetType("FEPusher");
  SetName(Name);
  Loop = true;      //This thread should start asap
                    //This is so the network connections can be made
  
  //Default packet manager string.
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
}

void FEPusher::ThreadCleanUp()
{
  //Clean up what's left of our network connections.
  server.Clear();
}

bool FEPusher::Setup(xmlNode * SetupNode,
			  std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;
  //==============================================================
  //Load the DRPC memory manager
  //==============================================================
  if(!FindMemoryManager(DRPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
 
  //==============================================================
  //Parse data for server config and expected clients.
  //==============================================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  if(ret && server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.GetSocketFD());
      SetupSelect();
    }
  else
    {
      ret = false;
    }


  //If everything in setup worked, then set Ready to true;
  if(ret == true)
    {
      Ready = true;
    }
  return(ret);
}

void FEPusher::PrintStatus()
{
  DCThread::PrintStatus();
  for(size_t i = 0; i <server.Size();i++)
    {
      if(server[i].IsRunning())
	{
	  server[i].PrintStatus();
	}
    }
}
#endif
