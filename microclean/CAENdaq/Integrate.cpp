#include "Integrate.h"

Float_t Integrate(Float_t * Y,Int_t Start, Int_t End,TH2F * h1=0,Float_t Sigma = 2,Int_t Window = 20,Int_t before = 5)
//This function performs the integral of the waveform from Start to End time
//steps.   It also fills the 2-D histogram h1 that will be used for find the
//single photo-electron (pulse integral and pulse height) for this waveform.
//This histogram won't be filled if h1 = 0 (default)
//Y is the waveform array (should be in ADC counts)
//Start is the low bound on the array (normally zero, but you can integrate whatever you want)
//End is the end of the array
//h1 is the SPE 2D histogram.  Only filled when h1 !=0
//Sigma is the trigger threshold in adc counts
//Window is the size in time steps of the integration window (*2ns for seconds)
//Before is the time the integration will try to step back after a trigger before integrating.
{
  Float_t SPE = 0;
  Float_t Height = 0;
  Int_t after = Window; //Size of integration window
  Int_t hafter = floor(Window/2.0); // Halfe window size to use for check for double SPEs
  Float_t Integral = 0;
  Int_t LastPoint = Start;

  for(int i = Start+1; i < End;i++)
    {
      //If the waveform goes above threshold
      if(Y[i] > Sigma)
	{
	  //initially set the integral window to after
	  Int_t Length = after;
	  Bool_t ThrowAway = false;
	  //Go back before steps unless we are entering the last window range.
	  if(i - LastPoint -1> before)
	    {
	      i-=before;
	    }
	  else
	    {
	      i-= (i - LastPoint) -1;
	      ThrowAway = true;
	    }

	  //Integrate window loop
	  for(int j = i; (j < End && j < i + after && j > 0) ;j++)
	    {
	      
	      //Find the max height	      
	      if(Y[j] > Height)
		{
		  Height = Y[j];
		}
	      //Check if there is a double SPE in window and don't add 
	      //this to h1 if there is. 
	      if(j > i + hafter)
		{
		  if(Y[j] > 0.5*Height)
		    {
		      ThrowAway = true;
		    }
		}
	      //Integrate the SPE
	      SPE += Y[j];
	      //keep track of the real length of the window
	      //Just in case it gets cut off at the end
	      //or some other strange way.
	      Length = j - i;
	    }
	  //move i since we were advancing in j
	  i+=Length;
	  //###############################
	  //Add window to integral if the height was greater than 5 adc counts
	  //###############################
	  if(Height > 5 && Length > 4)
	    Integral += SPE;
	  Height *= Volts;
	  //Add SPE to h1 if it's good and h1 !=0
	  if(h1 != 0 && !ThrowAway)
	    {
	      h1->Fill(SPE*C,Height);
	    }
	  //Reset everything for next window
	  SPE = 0;
	  Height = -100;
	  LastPoint = i;
	}
    }
  //Return integral in coulumbs
  return(Integral*C);
}
