#ifndef __DRDATAPACKET__
#define __DRDATAPACKET__
#include <RAT/DS/ReductionLevel.hh>

namespace DRPacket
{
  struct Event
  {
    uint32_t ID;
    uint32_t EventTime;   
    uint8_t BoardCount;
    uint8_t DataType;
  };
  struct Board
  {
    uint16_t LVDSPattern;
    uint32_t Time;
    uint8_t BoardID;
    uint8_t ChannelPattern;       
  };
  struct Channel
  {
    uint8_t RawIntegralCount;
  };
  
  struct RawIntegral
  {
    float   integral;     //ADC sum
    uint16_t startTime; //clock ticks
    uint8_t width; //clock ticks
  };

  struct PromptTotalBlock
  {
    //Prompt integral (ADC sum)
    float prompt;
    //total integral (ADC sum)
    float total;
  };
}
#endif
