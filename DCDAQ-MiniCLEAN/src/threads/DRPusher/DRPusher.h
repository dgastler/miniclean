#ifdef __RATDS__

#ifndef __DRPUSHER__
#define __DRPUSHER__

#include <DCThread.h>
#include <xmlHelper.h>

#include <Connection.h>
#include <DRDataPacket.h>

#include <FEPCRATManager.h>

#include <DCDeque.h>

#include <DetectorInfo.h>

class DRPusher : public DCThread 
{
 public:
  DRPusher(std::string Name);
  ~DRPusher() {};
  virtual bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  
  //_Net.cpp
  bool ProcessNewConnection();
  void CheckForNewConnection();
  void DeleteConnection();
  void ProcessChildMessage();
  bool StartNewConnection();
  //Connection to our server
  Connection DRAssembler;
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;


  

  //_EventProcess
  

  void ProcessUnSentEvent();  
  void SendEvent(int32_t index, uint64_t password);
  void MoveBad(int64_t badEventID,int32_t badIndex,Level::Type errorCode,
	       int32_t index,uint64_t password);
  int promptTotalMode;  //If this is false (zero), then we're using ZLE integrals.


  //_NetworkProcess

  void AddToPacket(RAT::DS::Board * board,
		   uint8_t * &currentPacketDataPtr,
		   uint16_t &packetSizeLeft,
		   int16_t PMTID = -1);


  
  //DCThread overloads
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(){DeleteConnection();};
  virtual void PrintStatus();

  //Update control
  time_t lastTime;
  time_t currentTime;

  //Stats
  uint32_t timeStepProcessedEvents;

  // Memory access
  //FEPC Rat manager
  FEPCRATManager * FEPC_mem;
    


  //================================
  //Detector Info
  //================================
  DetectorInfo detectorInfo;

  uint32_t SizeAdded;

  #ifdef DEBUG_ROLLOVER
  bool rollover;
  #endif

};


#endif

#endif
