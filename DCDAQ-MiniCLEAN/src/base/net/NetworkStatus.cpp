#include <NetworkStatus.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[NetworkStatus::COUNT] = 
  {
    "BLANK",
    "CONNECTED",
    "DISCONNECTED",
    "RETRY",
    "FAILED",    
  };  

const char * NetworkStatus::GetTypeName(Type type)
{
  if(type < COUNT)
    return TypeName[type];
  return NULL;
} 

enum NetworkStatus::Type NetworkStatus::GetTypeFromName(const char * name)
{
  std::string searchString(name);  
  Type ret = BLANK;
  for(Type type = BLANK;
      type < COUNT;
      type = Type(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
