#ifdef __RATDS__

#ifndef __EBPUSHER__
#define __EBPUSHER__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <Connection.h>
#include <EBDataPacket.h>

#include <FEPCRATManager.h>

#include <math.h>

#include <DetectorInfo.h>
#include <PacketBuilder.h>

#include <TH1F.h>

#include <sstream>


class EBPusher : public DCThread 
{
 public:
  EBPusher(std::string Name);
  ~EBPusher()
    {
    };
  virtual bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  
  //_Net.cpp
  void CheckForNewConnection();
  bool ProcessNewConnection();
  void DeleteConnection();
  void ProcessChildMessage();
  bool StartNewConnection();
  //Connection to our server
  Connection EBAssembler;
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;

  //_EventProcess.cpp
  void ProcessEvent();
  bool SendData(RATEVBlock * event);
  
  //Packet building
  PacketBuilder  packetBuilder;

  //Detector information
  DetectorInfo detectorInfo;
  
  //DCThread overloads
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(){DeleteConnection();};
  void PrintStatus();

  //Update control
  time_t lastTime;
  time_t currentTime;

  //Stats
  uint32_t timeStepProcessedEvents;

  // Memory access
  //FEPC Rat manager
  FEPCRATManager * FEPC_mem;


};


#endif

#endif
