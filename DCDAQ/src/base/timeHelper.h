#ifndef __TIMEHELPER__
#define __TIMEHELPER__

#include <string>
#include <stdint.h>
#include <DCMessage.h>

std::string GetTime(const time_t *now);
std::string GetTime(const DCMessage::DCtime_t *now);

#endif
