#include "SplineFunction.h"
#include <assert.h>

ClassImp(SplineFunction)

SplineFunction::SplineFunction()
{
  SplineSet=false;
  rMax = 0;
  rMin = 0;
}

SplineFunction::SplineFunction(std::vector<float> &X,std::vector<float> &Y,float Min_ =0,float Max_=0)
{
  SplineSet=false;
  rMax = 0;
  rMin = 0;
  SetSpline(X,Y,Min_,Max_);
}


void SplineFunction::SetSpline(std::vector<float> &X,std::vector<float> &Y,float Min_ =0,float Max_=0)
{
  assert(X.size() == Y.size());
  int Size = X.size();
  Double_t dx = 0;
  Double_t * Xs = new Double_t[Size];
  Double_t * Ys = new Double_t[Size];
  if(Size > 2)
    dx = fabs(X[1] - X[0]);
  double VectorIntegral = 0;
  for(int i = 0; i < Size;i++)
    {
      Xs[i] = X[i];
      Ys[i] = Y[i];
    }
  Spline = new TSpline3("EnToAng",Xs,Ys,Size,"",0,0);

  Spline->SetNpx(Spline->GetNpx()*1000);
  SplineSet = true;

  if((Min_ == 0) && (Max_ == 0))
    {
      rMin = Spline->GetXmin();
      rMax = Spline->GetXmax();
    }
  else
    {
      rMin = Min_;
      rMax = Max_;
    }
}

double SplineFunction::operator() (double *x,double*p)
{
  if(!SplineSet)
    {
      return(0);
    }
  
  double t = x[0]/p[1];
  if(t>rMax)
    {
      return(0);
    }

  //  double ret = (p[0]*p[1])*Spline->Eval(t);
  double ret = (p[0])*Spline->Eval(t);
  return(ret);
}
