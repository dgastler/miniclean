/*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/

#include <TGClient.h>
#include <TSystem.h>
#include "CLEANViewer.h"
#include "tclap/CmdLine.h"
#include "signal.h"
#include "execinfo.h"
#include "StackTracer.h"

TApplication *theApp;
CLEANViewer *mainWin;

int testFD;
time_t timeLastAlarm;
vector<time_t> timeGaps;
//void Sleepy();
//These signal handlers are a bit unsafe, perhaps should find a better way to do this

void signal_handler(int sig)
{
  if(sig==SIGINT)
    {

      fprintf(stderr,"sigint\n");
      //      mainWin->CloseWindow();
      delete(mainWin);
      //printf("after delete\n");
      theApp->Terminate();
      //printf("after terminate");
    }
  if(sig==SIGPIPE)
    {
      printf("sigpipe************************* \n");
      /* mainWin->CloseFifos();
      delete(mainWin);
      printf("after delete\n");
      theApp->Terminate();*/
    }
  if(sig==SIGALRM)
    {
      time_t timeNow=time(NULL);
      fprintf(stderr,"DCDAQ hasn't sent anything in a while, seems sleepy\n");
      timeGaps.push_back(timeNow-timeLastAlarm);
      if(timeGaps.size()>5)
	{
	  timeGaps.erase(timeGaps.begin());
	  int OffCounts=0;
	  for(size_t iGap=0;iGap<timeGaps.size();iGap++)
	    {
	      if(timeGaps[0]!=timeGaps[iGap])
		OffCounts+=1;
	    }
	
	
	  if(OffCounts<1)
	    theApp->Terminate();
	  
	}
      
      // mainWin->SleepWindow();
      mainWin->FilterWindow();
      timeLastAlarm=timeNow;
      mainWin->SetNewAlarm();


    }
  if(sig==SIGSEGV)
    {

      //print stacktrace
#ifdef __gnu_linux__
      print_trace();
#endif
      fprintf(stderr,"sigsegv\n");
      //mainWin->CloseWindow();
      if(mainWin)
	delete(mainWin);
      //printf("after delete\n");
      theApp->Terminate();
      //printf("after terminate");
    }
}

int main (int argc, char **argv)
{
  //Uses TCLAP for parsing command line arguments
  std::string _DataFile = "";
  std::string _DataFileF = "";
  bool _DebugMode;
  bool _ZMQMode;
  bool _Enable3DMode;
  UInt_t _AvgValue=0;
  UInt_t _Period=2;
  std::string _RATServer ="";
  vector<unsigned> _ReducVect;
  size_t _BufferSize=5;
  try
    {
      TCLAP::CmdLine cmd("CLEANViewer is the ROOT GUI event viewer for the MiniCLEAN RAT data.",
			 ' ',
			 "CLEANViewer alpha v2.0.1"
			 );
      //argument for loading RAT data file at startup
      TCLAP::UnlabeledValueArg<std::string> DataArg("DataFile",
						    "Initial RAT data file loaded into CLEAN viewer. Overwritten by DataFileFlag if both are used.",
						    false,
						    _DataFile,
						    "file name",
						    cmd
						    );
      TCLAP::ValueArg<std::string> DataArgF("f",
					   "DataFileFlag",
					   "Initial RAT data file loaded into CLEAN viewer. Overwrites DataFile if both are used.",
					   false,
					   _DataFileF,
					   "file name",
					   cmd
					   );
      TCLAP::SwitchArg DebugSwitch("D",
				   "Debug",
				   "Switch to toggle Debug Mode for CLEANViewer",
				   cmd,
				   false);

      TCLAP::ValueArg<int> AvgNumb("a",
				   "Avg",
				   "Switch to load all events in beginning for averaging",
				   false,
				   _AvgValue,
				   "number to avg over",
				   cmd
				   );
      TCLAP::MultiArg<unsigned> ReducLevels("R",
					    "Reduc",
					    "Choose Reduction Levels",
					    false,
					    "reduction level",
					    cmd
					    );

      TCLAP::ValueArg<unsigned> PeriodValue("p",
					    "Period",
					    "Choose Period",
					    false,
					    _Period,
					    "period",
					    cmd
					    );
      TCLAP::SwitchArg ZMQSwitch("Z",
				 "ZMQ",
				 "Switch to toggle ZMQ Shift Mode.  File input should be address:port",
				 cmd,
				 false);

      TCLAP::SwitchArg Enable3DSwitch("P",
				 "Plain_Mode",
				 "Switch to toggle 3D event display drawing off",
				 cmd,
				 true);

      TCLAP::ValueArg<std::string> RatServer("s",
					   "ratserver",
					   "Rat server to be used to send data to for processing before displaying",
					   false,
					   _RATServer,
					   "rat server",
					   cmd
					   );
      TCLAP::ValueArg<size_t> BufferSize("B",
				   "BuffSize",
				   "Size of Shift Mode Buffer",
				   false,
				   _BufferSize,
				   "shift mode buffer size",
				   cmd
				   );




      cmd.parse(argc, argv);
      _DataFile = DataArg.getValue();
      _DataFileF = DataArgF.getValue();
      _DebugMode = DebugSwitch.getValue();
      _AvgValue = AvgNumb.getValue();
      _ZMQMode = ZMQSwitch.getValue();
      _ReducVect=ReducLevels.getValue();
      _Period=PeriodValue.getValue();
      _RATServer=RatServer.getValue();
      _Enable3DMode=Enable3DSwitch.getValue();
      _BufferSize=BufferSize.getValue();
    }
  catch(TCLAP::ArgException &e)
    {
      fprintf(stderr, "Error %s for arg %s\n",
	     e.error().c_str(), e.argId().c_str());
      return 0;
    }
  
  if(_DataFileF.compare("") != 0)
    _DataFile = _DataFileF;
  
  /*
  for(int sig=0;sig<kMAXSIGNALS;sig++)
    {
      gSystem->ResetSignal((ESignals)sig);
    }*/
  struct sigaction sa;
  sa.sa_handler = signal_handler;
  sigemptyset(&sa.sa_mask);
  gSystem->ResetSignal((ESignals)SIGINT);
  sigaction(SIGINT, &sa, NULL);
  gSystem->ResetSignal((ESignals)SIGPIPE);
  sigaction(SIGPIPE,&sa,NULL);
#ifdef __gnu_linux__
  gSystem->ResetSignal((ESignals)SIGSEGV);
  sigaction(SIGSEGV,&sa,NULL);
#endif
  gSystem->ResetSignal((ESignals)SIGALRM);
  sigaction(SIGALRM,&sa,NULL);

 
  theApp=new TApplication("App",&argc,argv);
 
  
  mainWin=new CLEANViewer(gClient->GetRoot(),1200, 900, 
			  _DataFile.c_str(), _DebugMode,_AvgValue,_ZMQMode,_Period,_ReducVect,_RATServer,_Enable3DMode,_BufferSize);
  



  
  theApp->Run();


  return 0;
}


