#include <RATWrite.h>

RATWrite::RATWrite(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATWrite"));
  OutFile=NULL; 
  failedFileOpens = 0;
  
  SubRunNumber = 1; //Starting subrun ID
  SubRunNumberMax = 999; //Maximum number of sub runs.
  SubRunMaxEvents = 100000; //Default events per subrun.
  TimeStep_Events = 0;
  
  EventNumber = 1; //Starting event number
  SubEventNumber = 1; //Starting sub-event number
  
  dsIndex = FreeFullQueue::BADVALUE;
  dsBlock = NULL;
  fileReady = false;
  BranchDS = NULL;
  Mem = NULL;
  
  RunEndTime = 0; // Run end time. (never by default)
  lastTime = time(NULL);   //Time of last stat report
  currentTime = time(NULL); //Current time.
}

RATWrite::~RATWrite()
{
  CloseFile();
}


bool RATWrite::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager)
{
  //Find the memoryManager we need
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return  false;
    }
  if(Mem == NULL)
    {
      PrintError("Bad memorymanager!");
      return(false);
    }
  
  //Get the autosave value for the root file (Bytes)
  if(FindSubNode(SetupNode,"AUTOSAVE") == NULL)
    {
      Autosave = 1024*1024; //Default autosave size (Bytes)
      PrintWarning("Using default autosave value");
    }
  else    
    GetXMLValue(SetupNode,"AUTOSAVE",GetName().c_str(),Autosave);

  //Get the subrun event count value for the root files
  if(FindSubNode(SetupNode,"SUBRUNEVENTS") == NULL)
    {
      SubRunMaxEvents = 10000;
      PrintWarning("Using default value for subrun event count");
    }
  else
    GetXMLValue(SetupNode,"SUBRUNEVENTS",GetName().c_str(),SubRunMaxEvents);
  
  
  //Get the max events value for this run. 
  if(FindSubNode(SetupNode,"EVENTS") == NULL)
    {
      MaxRunEvents = 100000; //Default max number of events.
      PrintWarning("Using default value for total run events");
    }
  else
    GetXMLValue(SetupNode,"EVENTS",GetName().c_str(),MaxRunEvents);

  //Get the max file open error level
  if(FindSubNode(SetupNode,"MAXFAILEDOPENS") == NULL)
    {
      maxFailedFileOpens = 10; //Default max number of events.
      PrintWarning("Using default value for max file opening failures");
    }
  else
    GetXMLValue(SetupNode,"MAXFAILEDOPENS",GetName().c_str(),maxFailedFileOpens);

  //Get the length we are running for.
  //Use default if the tag doesn't exist
  if(FindSubNode(SetupNode,"TIME") == NULL)
    {
      RunEndTime = 0;     
    }
  else
    {
      uint32_t timeTemp;
      GetXMLValue(SetupNode,"TIME",GetName().c_str(),timeTemp);     
      //Add the run time in seconds to the current time
      if(timeTemp != 0)
	RunEndTime = time(NULL) + timeTemp;
    }

  //Set thread state to go and change it to fail if anything goes wrong.
  Ready = true;
  return(Ready);
}


bool RATWrite::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      //Shutdown the memory manager
      Mem->Shutdown();
      Loop = false;
      Running = false;
      CloseFile();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Remove the memory manager's free queue from the select mask
      RemoveReadFD(Mem->GetFullFDs()[0]);
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());

      //Get the full block queue's FDs and add it to the select set
      AddReadFD(Mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = true;
    }
  else if(message.GetType() == DCMessage::RUN_NUMBER)
    {
      DCMessage::SingleINT64 data;
      if(message.GetDataStruct(data))
	{
	  CloseFile();
	  SetRunNumber(data.i);
	  EventNumber = 1; //Starting event number
	  SubEventNumber = 1; //Starting sub-event number
	  SubRunNumber = 1; //Starting subrun ID
	  TimeStep_Events = 0;
	}
    }
  else
    {
      ret = false;
    }
  return(ret);
}
