#ifndef __ATLYS_Control__
#define __ATLYS_Control__

#include "packets.h"
#include "FreeFullQueue.h"
#include <stdint.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>

void * launch_ATLYS_Control(void * ptr);

class ATLYS_Control
{
 public:
  ATLYS_Control();

  bool IsMaster;

  void Start()
  {
    Running = true;
    pthread_create(&thread_ID,NULL,
		   &launch_ATLYS_Control,
		   this);
  };
  void Join(){pthread_join(thread_ID,NULL);};
  void Stop()
  {
    //Queues can no longer block
    Packet_Indexes.ShutdownQueues();
    //add a bad index to the queue to tell thread to shutdown
    int32_t stop_Index = -1;
    Packet_Indexes.AddFull(stop_Index);
    //Set running to false to break out of any packet sending loop
    Running = false;
  }

  //Load data from file into packets and return the number of events found
  uint32_t Load_Data(const std::string & file);

  //Set up network connections
  bool Setup_Network(const std::string & IP);

  //FreeFullQueue for syncing of events between ATLYS boards
  FreeFullQueue Packet_Indexes;
  //Access to packets
  ATLYS_Packet * GetPacket(int32_t i) {return packets+i;}

  //Class thread main
  void * main(void * args);

 private:
  //Array of packets to use
  std::string source_File;
  ATLYS_Packet * packets;
  uint32_t number_Of_Events;

  //Functions to send event parts to the board
  bool Send_Part1(int32_t index);
  bool Send_Part2(int32_t index);
  void Send_Start();
  void Send_Stop();
  void Set_Event_Timeout();

  //Error checking variables
  uint32_t last_Time;
  //  FILE * debug;

  //Network values
  int sockFD;
  struct sockaddr_in sAddr;    

  //Thread control
  bool Running;
  pthread_t thread_ID;
  const bool Is_Running(){return Running;};
};

#endif
