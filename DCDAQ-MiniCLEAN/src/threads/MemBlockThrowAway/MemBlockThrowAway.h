#ifndef __MEM_BLOCK_THROW_AWAY__
#define __MEM_BLOCK_THROW_AWAY__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>


class MemBlockThrowAway : public DCThread 
{
 public:
  MemBlockThrowAway(std::string Name);
  ~MemBlockThrowAway()
    {
    };
  virtual bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  //Update control
  time_t last;
  time_t current;

  //Statistics
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;

  // Memory access
  int32_t blockIndex;
  MemoryBlockManager * Mem; 

};


#endif
