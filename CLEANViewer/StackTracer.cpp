#include <StackTracer.h>
//#include <string.h>
//#include <string>

/* 
example stolen from
http://www.gnu.org/software/libc/manual/html_node/Backtraces.html
*/
#ifdef __gnu_linux__

void print_trace() 
{
  void *array[40];
  size_t size;
  char **strings;
  size_t i;
  
  int status;
  char * demangled=NULL;

  //Get backtrace symbols
  size = backtrace (array, 40);
  strings = backtrace_symbols_lines (array, size);
  
  printf ("Obtained %zd stack frames.\n", size);

  //Process each entry
  for (i = 0; i < size; i++)
    {
      //Convert string to stl
      std::string functionName(strings[i]);
      std::string fileName;

      //Parse for only the mangled name
      size_t end_position = functionName.size();//functionName.rfind(')');
      functionName = functionName.substr(0,end_position+1);
      size_t start_position = functionName.rfind('\t');
      start_position--;
      end_position=functionName.size();
      
      if((start_position != std::string::npos) &&
	 (end_position != std::string::npos)) //There is a valid name
	{
	  //Get raw strings for filename and function name
	  start_position+=2;
	  end_position-=2;
	  fileName = functionName.substr(0,start_position);
	  functionName = functionName.substr(start_position,end_position-start_position);

	  //demange the name
	  demangled = abi::__cxa_demangle(functionName.c_str(), 0,0, &status);

	  //The name was demangled
	  if(status == 0)
	    printf (">    %s  :  %s\n", fileName.c_str(),demangled);
	  //C name or error
	  else
	    printf (">    %s  :  %s\n", fileName.c_str(),functionName.c_str());

	  free(demangled);
	}
      else  // no valid name
	{
	  printf (">    %s\n", strings[i]);
	}
    }
  
  free (strings);
}
#endif
