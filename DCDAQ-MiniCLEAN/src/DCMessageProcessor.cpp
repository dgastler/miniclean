#include <DCMessageProcessor.h>

DCMessageProcessor::DCMessageProcessor(DCLauncher * _Launcher)
{
  Name.assign("DCMessageProcessor");
  Launcher = _Launcher;
  RegisterCommands();
}

bool DCMessageProcessor::ProcessSelect(int selectReturn,
				       fd_set retReadSet,
				       fd_set /*retWriteSet*/,
				       fd_set /*retExceptionSet*/,
				       bool run
				       )
{
  //Iterator for our FD-thread map
  std::map<int,size_t>::const_iterator FDit;
  //Loop over all the entries in the ThreadFDMap and find those that are readible
  for(FDit = Launcher->GetThreadFDMap().begin();
      ((FDit != Launcher->GetThreadFDMap().end()) && (selectReturn >0));)
    {
      //Ask if this FD was ready
      if(FD_ISSET(FDit->first,&retReadSet))
	{
	  //report that we've addressed this fd
	  selectReturn--;
	  //Get the message from the read thread and pass it to ProcessMessage(message)
	  if(Launcher->GetThreads()[FDit->second]->OutgoingMessageCount() > 0)
	    {
	      DCMessage::Message newMessage = Launcher->GetThreads()[FDit->second]->GetMessageOut();	      
	      if(newMessage.GetType() == DCMessage::END_OF_THREAD)
		{
		  RemoveThreadRoute(Launcher->GetThreads()[FDit->second]->GetName());
		  RemoveThreadTypeRoute(Launcher->GetThreads()[FDit->second]->GetName());
		  Launcher->DeleteThread(FDit->second);  		  
		}
	      else
		{
		  run = (ProcessMessage(newMessage) && run);
		}
	    }
	  break;
	}
      if(Launcher->GetThreadFDMap().size() != 0)
	{
	  FDit++;
	}
      else
	{
	  break;
	}
    }
  //Loop over all the entries in the MMFDMap and find those that are readible
  for(FDit = Launcher->GetMMFDMap().begin();
      ((FDit != Launcher->GetMMFDMap().end()) && (selectReturn >0));
      FDit++)
    {
      //Ask if this FD was ready
      if(FD_ISSET(FDit->first,&retReadSet))
	{
	  //report that we've addressed this fd
	  selectReturn--;
	  //Get the message from the read thread and pass it to ProcessMessage(message)
	  DCMessage::Message newMessage = Launcher->GetMemoryManagers()[FDit->second]->GetMessageOut();
	  //Check if this is an END_OF_THREAD message
	  run = (ProcessMessage(newMessage) && run);
	  break;
	}
    }


  return run;
}

bool DCMessageProcessor::ProcessMessage(DCMessage::Message message)
{
  bool runReturn = true;
  //First check if this message should be routed outside of this DCDAQ session
  if(! (message.GetDestination().IsDefaultAddr()))
    //we have a message for outside of this DCDAQ session
    {
      runReturn = RouteRemoteMessage(message);
    }
  else if(!message.GetDestination().GetName().empty())
    //Route to the local thread
    {
      runReturn = RouteLocalMessage(message);
    }
  else
    //Check for a type function
    {
      runReturn = RouteMessageType(message);
    }  
  return runReturn;
}

DCThread * DCMessageProcessor::FindThread(std::string Name)
{
  DCThread * thread = NULL;
  //Search through all the threads in the thread vector for a thread
  //with name Name
  for(std::vector<DCThread*>::const_iterator it = Launcher->GetThreads().begin(); 
      it != Launcher->GetThreads().end();
      it++)
    {
      //check for the correct name     
      if((*it)!=NULL && boost::iequals(Name,(*it)->GetName()))
	{
	  //copy the thread pointer
	  thread = *it;
	  //break from the loop
	  break;
	}
    }
  return(thread);
}

void DCMessageProcessor::BuildError(DCMessage::SingleUINT64 errorData)
{
  DCMessage::Message errorMessage;
  errorMessage.SetType(DCMessage::ERROR);
  errorMessage.SetDestination();
  errorMessage.SetSource("DCLauncher");
    
  errorMessage.SetDataStruct(errorData);
  ProcessMessage(errorMessage);		  
}

void DCMessageProcessor::RemoveThreadRoute(const std::string & threadName)
{
  for(std::map<DCMessage::DCAddress, std::string >::iterator it = messageAddrToNameMap.begin();
      it !=  messageAddrToNameMap.end();
      )
    {
      if(boost::algorithm::iequals(threadName,it->second))
	{
	  messageAddrToNameMap.erase(it);
	  it = messageAddrToNameMap.begin();
	}
      else
	{
	  it++;
	}
    }
}

void DCMessageProcessor::RemoveThreadTypeRoute(const std::string & threadName)
{
  for(std::map<uint16_t,DCMessage::DCAddress >::iterator it = messageTypeToAddressMap.begin();
      it != messageTypeToAddressMap.end();
      )
    {
      if(boost::algorithm::iequals(threadName,it->second.GetName()))
	{
	  messageTypeToAddressMap.erase(it);
	  it = messageTypeToAddressMap.begin();
	}
      else
	{
	  it++;
	}
    }
}


