#include <EBPCRatManager.h>

EBPCRATManager::EBPCRATManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("EBPCRATManager"));
  //Make sure our memory vector is empty
  Memory.clear();
}

EBPCRATManager::~EBPCRATManager()
{
  Deallocate();
}

void EBPCRATManager::Deallocate()
{
  if(!Lock.LockObject())
    {
      fprintf(stderr,"Lock error on deallocation\n");
      abort();
    }
  //Clear our containers
  Free.Clear(true);
  EventStore.Clear(true);
  ToDisk.Clear(true);
  BadEvent.Clear(true); 
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //      delete EBPC event (check if it's not NULL)
      if(Memory.back().ds)
	{
	  delete Memory.back().ds;
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
  Lock.UnlockObject();
}


int64_t EBPCRATManager::GetEvent(int64_t Event,
				 int32_t &index,
				 uint64_t &password)
{ 
  int64_t ret = returnDCVectorHash::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      //The object is now mutex locked

      //Get the index at Event
      ret = EventStore.GetEntry(Event,index,password);
      if(ret == returnDCVectorHash::EMPTY_KEY)
	//The index was empty
	{      
	  //Get a new index from the free queue
	  int32_t freeIndex;
	  bool gotFree = false;
	  while(!(gotFree = Free.Get(freeIndex)))
	    {
	    }
	  if(gotFree)
	    {
	      index = freeIndex;
	      ret = EventStore.UpdateEntry(Event,freeIndex,password); 	  
	    }
	  else
	    {
	      index = EventStore.GetBadValue();
	      //Must block to make sure everything is consistent
	      ret = EventStore.ClearEntry(Event,password);
	      if(ret == returnDCVectorHash::OK)
		{
		  ret = returnDCVectorHash::NO_FREE;
		}
	    }	
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

int64_t EBPCRATManager::SetEvent(int64_t Event,
				 int32_t &index,
				 uint64_t password)
{
  int64_t ret = returnDCVectorHash::OK;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = EventStore.UpdateEntry(Event,index,password);
      if(ret == returnDCVectorHash::OK)
	{
	  ret = EventStore.ClearPassword(Event,password);
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);  
}

int64_t EBPCRATManager::ClearEvent(int64_t key, uint64_t password)
{
  int64_t ret = returnDCVectorHash::OK;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = EventStore.ClearEntry(key,password);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);  
}

bool EBPCRATManager::AllocateMemory(xmlNode * setupNode)
{
  if(!Lock.LockObject())
    {
      fprintf(stderr,"Lock error on allocation\n");
      abort();
    }
  bool ret = true;
  
  int NumEBPCBlocksERR = GetXMLValue(setupNode,"NUMEBPCBLOCKS","MANAGER",NumEBPCBlocks);
  if(NumEBPCBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMEBPCBLOCKS in EBPCRATManager.\n");
      ret = false;
    }
  int VectorHashSizeERR = GetXMLValue(setupNode,"VECTORHASHSIZE","MANAGER",VectorHashSize);
  if(VectorHashSizeERR != 0)
    {
      fprintf(stderr,"Error reading VECTORHASHSIZE in EBPCRATManager.\n");
      ret = false;
    }

  //Allocate the needed RAT EBPC blocks
  if(ret)
    {
      for(uint32_t iEBPCBlock = 0; iEBPCBlock < NumEBPCBlocks;iEBPCBlock++)
	{
	  EBPCBlock evBlock;	  
	  Memory.push_back(evBlock);
	  Memory.back().ds = new RAT::DS::Root;	  
	  Memory.back().ds->AddNewEV();
	  int32_t index = int32_t(Memory.size())-1;
	  Free.Add(index);
	  if(Memory.back().ds == NULL)
	    {
	      ret = false;
	      break;
	    }
	}     
    }
  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash (-1 emtpy value, -2 bad value)
      EventStore.Setup(VectorHashSize,-1,-2);
    }
  Lock.UnlockObject();
  return(ret);
}

bool EBPCRATManager::AddBadEventNOFD(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      //Read off one from the FD to not cause select to fire on this event.
      ret = BadEvent.Add(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);  
}

int64_t EBPCRATManager::MoveEventToBadQueue(int64_t Event,
					    int32_t &index,
					    uint64_t password)
{  
  int64_t ret = returnDCVectorHash::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      int32_t indexToSend = index;
      bool moveWorked = BadEvent.Add(indexToSend);
      if(moveWorked)
	//We moved this event, so now we need to clear it. 
	{
	  //We will always wait on this!
	  ret = EventStore.ClearEntry(Event,
				      password,
				      true);        
	  if(ret == returnDCVectorHash::OK)
	    //The caller no longer should know this
	    {
	      index = EventStore.GetBadValue();
	    }
	}
      else
	//The bad queue was locked and we aren't waiting
	{
	  ret = returnDCVectorHash::EXTERNAL;
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

int64_t EBPCRATManager::MoveEventToDiskQueue(int64_t Event,
					     int32_t &index,
					     uint64_t password)
{  
  int64_t ret = returnDCVectorHash::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      int32_t indexToSend = index;
      bool moveWorked = ToDisk.Add(indexToSend);      
      if(moveWorked)
	//We moved this event, so now we need to clear it. 
	{
	  //We will always wait on this!
	  ret = EventStore.ClearEntry(Event,
				      password,
				      true);        
	  if(ret == returnDCVectorHash::OK)
	    //The caller no longer should know this
	    {
	      index = EventStore.GetBadValue();
	    }
	}
      else
	//The Send queue was locked and we aren't waiting
	{
	  ret = returnDCVectorHash::EXTERNAL;
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

void EBPCRATManager::PrintStatus(int indent)
{
  printf("\n");
  if(indent > 0)
    {
      for(int i = 0; i < indent;i++)
	{
	  printf(" ");
	}
    }
# if __WORDSIZE == 64
  char printCommand[] = "EBPCRATManager\n     Size: %04lu\n     Free: %04lu\n     Active Stored: %04lu(%04lu)\n     ToDisk: %04lu\n     BadEvent: %04lu\n";
# else
  char printCommand[] = "EBPCRATManager\n     Size: %04u\n     Free: %04u\n     Active Stored: %04u(%04u)\n     ToDisk: %04u\n     BadEvent: %04u\n";
#endif
  printf(printCommand,
	 Memory.size(),
	 Free.GetSize(),
	 EventStore.GetActiveEntryCount(),
	 EventStore.GetSize(),
	 ToDisk.GetSize(),
	 BadEvent.GetSize());

}

bool EBPCRATManager::AddFree(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = Free.Add(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

bool EBPCRATManager::GetToDisk(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = ToDisk.Get(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}
bool EBPCRATManager::AddToDisk(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = ToDisk.Add(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

bool EBPCRATManager::GetBadEvent(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = BadEvent.Get(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}


