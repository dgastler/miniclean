#include <ConvertController.h>

bool ConvertController::Setup(xmlNode * SetupNode,
			      std::vector<MemoryManager*> & MemManager)
{
  //Watch path
  if(FindSubNode(SetupNode, "PATH") == NULL)
    watchPath.assign("./");
  else
    GetXMLValue(SetupNode, "PATH", GetName().c_str(), watchPath);

  //file base
  if(FindSubNode(SetupNode, "FILE_BASE") == NULL)
    watchFileBase.assign("MemBlocks");
  else
    GetXMLValue(SetupNode, "FILE_BASE", GetName().c_str(), watchFileBase);

  //file extension
  if(FindSubNode(SetupNode, "FILE_EXTENSION") == NULL)
    watchFileBase.assign(".dat");
  else
    GetXMLValue(SetupNode, "FILE_BASE", GetName().c_str(), watchFileExtension);
  
  //Set up inotify
  iNotifyFD = -1;
  iNotifyFD = inotify_init();
  if(iNotifyFD < 0)
    {
      fprintf(stderr,"iNotify init failed\n");
      return false;
    }

  //Add watch for closed file in watch directory
  watchFD = -1;
  watchFD = inotify_add_watch(iNotifyFD,
			      (std::string("./") + watchPath).c_str(), 
			      IN_CLOSE_WRITE);
  if(watchFD < 0)
    {
      fprintf(stderr,"iNotify watch on directory %s failed\n",watchPath.c_str());
      return false;
    }
  Ready=true;
  return Ready;
}

void ConvertController::MainLoop()
{
  if(IsReadReady(iNotifyFD))
    {
      inotify_event * event;
      int readSize;
      int retError;
      while(-1 == (readSize = readN(iNotifyFD,
				    ((uint8_t *) &bufferData[0]),
				    bufferData.size(),
				    retError)))
	{
	  bufferData.resize(bufferData.size()+1);
	}
      
      event = (inotify_event *) &(bufferData[0]);
      if(event->wd == watchFD)
	{
	  ProcessNewFile(event->name);
	}
      else
	{
	  //loop over all runs and see if this is a socket creation message
	  std::map<int,ConvertGroup>::iterator itGroup;
	  for(itGroup = Run.begin(); itGroup != Run.end();itGroup++)
	    {	   
	      if(itGroup->second.remoteSocketWatchDescriptor == event->wd)
		{
		  printf("This is a socket creation message\n");
		  if(! (itGroup->second.running))
		    {
		      printf("Let's build a run control for the new DCDAQ session\n");
		      //Build a run control for the new DCDAQ session
		      BuildRunGroup(itGroup->second.runNumber);
		      itGroup->second.running = true;
		    }
		  inotify_rm_watch(iNotifyFD,itGroup->second.remoteSocketWatchDescriptor);
		  itGroup->second.remoteSocketWatchDescriptor = -1;
		  break;
		}
	    }
	}
      bufferData.resize(sizeof(inotify_event));
    }
}

void ConvertController::ProcessNewFile(char * filename)
{
  //parse and compute run number. 
  std::string name(filename);
  //Check if the file has the correct base. 
  if(boost::algorithm::find_first(name,watchFileBase))
    {
      name = name.substr(name.find("_")+1);
      int runNumber = atoi((name.substr(0,name.size() - name.find("_"))).c_str());
      std::map<int,ConvertGroup>::iterator runIt = Run.find(runNumber);
      //Check for an existing conversion group
      if(runIt != Run.end())
	{
	  printf("Found an existing conversion group\n");
	  runIt->second.files.push(filename);
	  while(runIt->second.running && runIt->second.files.size())
	    {
	      //Move the file into the correct directory	      
	      std::stringstream ss;
	      ss << " mv ~/DCDAQ-MiniCLEAN/" 
		 << watchPath << "/" << filename 
		 << " ~/DCDAQ-MiniCLEAN/" << watchPath 
		 << "/" << runNumber << "/";
	      int ret = system(ss.str().c_str());
	      if(ret)
		{
		  PrintError("Abnormal system call.");
		}

	      DCMessage::Message message;
	      message.SetType(DCMessage::COMMAND);
	      message.SetDestination(runIt->second.MemBlockReader);
	      DCMessage::DCString string;
	      std::vector<uint8_t> data = string.SetData(runIt->second.files.front());
	      message.SetData(&(data[0]),data.size());
	      SendMessageOut(message);

	      runIt->second.files.pop();
	    }
	}
      //We have a valid new run number
      else if(runNumber > 0)
	{
	  printf("New run number\n");
	  Run[runNumber].runNumber=runNumber;
	  Run[runNumber].running = false;

	  //Add a new inotify watch for the unix socket made by this DCDAQsession
	  //We need it to exist before we run BuildRunGroup
	  Run[runNumber].remoteSocketWatchDescriptor = 
	    inotify_add_watch(iNotifyFD,
			      socketPath.c_str(),
			      IN_CREATE);
	  Run[runNumber].files.push(filename);
	  if(Run[runNumber].remoteSocketWatchDescriptor < 0)
	    {
	      printf("Failed to add watch\n");
	    }

	  //Create a new DCDAQ session in a screen
	  CreateNewDCDAQSession(runNumber);
	}
      else
	{
	  std::stringstream ss;
	  ss << "Error reading file: " << filename;
	  PrintError(ss.str());
	}
    }
			       
}

void ConvertController::CreateNewDCDAQSession(int runNumber)
{
  printf("Creating new DCDAQ session\n");
  //Create a new screen
  std::stringstream ss;
  ss << "screen -dmS Convert_" << runNumber;
  int ret = system(ss.str().c_str());
  ss.str("");

  //make dir
  ss << "mkdir -p ~/DCDAQ-MiniCLEAN/" << watchPath << "/" << runNumber;
  ret = system(ss.str().c_str());
  ss.str("");

  //Add convert DCDAQ session on the command line
  ss << "screen -S Convert_" <<  runNumber 
     << " -p 0"
     << " -X stuff \""
     << " source Rat.sh ;\"";
  ret = system(ss.str().c_str());
  ss.str("");

  ss << socketPath << "DCDAQ." << runNumber;
  Run[runNumber].Addr.Set("",ss.str());
  ss.str("");

  //Add convert DCDAQ session on the command line
  ss << "screen -S Convert_" <<  runNumber 
     << " -p 0"
     << " -X stuff \""
     << " cd ~/DCDAQ-MiniCLEAN/" << watchPath << "/" << runNumber << ";"
     << " ~/DCDAQ-MiniCLEAN/DCDAQ -a "
     << Run[runNumber].Addr.GetAddrStr()
     << " -p 0"
    //     << "\""
     << ";exit\""
     << "$'\n'";
  ret = system(ss.str().c_str());
  ss.str("");

  //Move the initial file into the correct directory	      
  ss << " mv ~/DCDAQ-MiniCLEAN/" 
     << watchPath << "/" << Run[runNumber].files.back()
     << " ~/DCDAQ-MiniCLEAN/" << watchPath 
     << "/" << runNumber << "/";
  ret = system(ss.str().c_str());  
  ss.str("");

  printf("Done creating new DCDAQ session\n");
}

void ConvertController::BuildRunGroup(int runNumber)
{  
  std::stringstream ss;
  ss << runNumber;
  std::string sRunNumber = ss.str();
  xmlDoc * doc = xmlNewDoc((xmlChar*)"1.0");  
  //Build XML for new run group
  xmlNode * setupNode=xmlNewDocNode(doc,NULL,(xmlChar*) "SETUP",NULL);
  xmlDocSetRootElement(doc,setupNode);

  //Add memory managers  
  xmlNode * memManagersNode = xmlNewChild(setupNode,NULL,(xmlChar *)"MEMORYMANAGERS",NULL);
  //Add threads  
  xmlNode * threadsNode = xmlNewChild(setupNode,NULL,(xmlChar *)"THREADS",NULL);

  //Build run control thread
  std::string sRunControlName = "RUNCONTROL_" + sRunNumber;
  xmlNode * RCthread = xmlNewChild(threadsNode,NULL,(xmlChar*)"THREAD",NULL);
  xmlNewChild(RCthread,NULL,(xmlChar*)"TYPE",(xmlChar*)"RunControl");
  xmlNewChild(RCthread,NULL,(xmlChar*)"Name",(xmlChar*)sRunControlName.c_str());
  xmlNewChild(RCthread,NULL,(xmlChar*)"GO",NULL);
  
  //Rount nework connections
  xmlNode * opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  xmlNode * op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"REGISTER_TYPE");
  xmlNode * data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TEXT",(xmlChar*)"NETWORK");
  
  //Setup message client ot new DCDAQ session
  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"LAUNCH");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  data = xmlNewChild(data,NULL,(xmlChar*)"XML",NULL);
  data = xmlNewChild(data,NULL,(xmlChar*)"SETUP",NULL);
  data = xmlNewChild(data,NULL,(xmlChar*)"THREADS",NULL);
  data = xmlNewChild(data,NULL,(xmlChar*)"THREAD",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TYPE",(xmlChar*)"DCMessageClient");
  std::string sDCMessageClientName = "DCMessageClient" + sRunNumber;
  xmlNewChild(data,NULL,(xmlChar*)"NAME",(xmlChar*)sDCMessageClientName.c_str());

  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());
  xmlNewChild(data,NULL,(xmlChar*)"PORT",(xmlChar*)"0");
  //responses
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"RESPONSE",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"NETWORK");
  data = xmlNewChild(op,NULL,(xmlChar*)"SOURCE",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"NAME",(xmlChar*)sDCMessageClientName.c_str());
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"WORD32",(xmlChar*)"0x1");

  op = xmlNewChild(opBlock,NULL,(xmlChar*)"RESPONSE",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"LAUNCH_RESPONSE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"WORD32",(xmlChar*)"0x0");
  xmlNewChild(data,NULL,(xmlChar*)"WORD32",(xmlChar*)"0x0");


  //Rount nework connections
  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"REGISTER_TYPE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TEXT",(xmlChar*)"ERROR");
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());

  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"REGISTER_TYPE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TEXT",(xmlChar*)"WARNING");
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());

  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"REGISTER_TYPE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TEXT",(xmlChar*)"TEXT");
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());
  data = xmlNewChild(op,NULL,(xmlChar*)"SOURCE",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str()); 
  xmlNewChild(data,NULL,(xmlChar*)"NAME",(xmlChar*)"Print"); 

  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"REGISTER_TYPE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TEXT",(xmlChar*)"STOP");
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());

  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"REGISTER_TYPE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"TEXT",(xmlChar*)"NETWORK");
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());
  


  //Set up threads for new  DCDAQ session
  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"LAUNCH");
  //Destination
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  data = xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());
  //data
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  data = xmlNewChild(data,NULL,(xmlChar*)"XML",NULL);
  setupNode = xmlNewChild(data,NULL,(xmlChar*)"SETUP",NULL);

  //memory managers
  std::string BlockSize("0x110000");
  std::string BlockCount("40");
  std::string NumBlocks("1000");
  std::string VectorHashSize("1000");

  memManagersNode = xmlNewChild(setupNode,NULL,(xmlChar *)"MEMORYMANAGERS",NULL);
  //Raw blocks
  std::string sRawName = "RAWBLOCKS" + sRunNumber;
  xmlNode * memManager = xmlNewChild(memManagersNode,NULL,(xmlChar*)"MANAGER",NULL);
  xmlNewChild(memManager,NULL,(xmlChar*)"TYPE",(xmlChar*)"MemoryBlockManager");
  xmlNewChild(memManager,NULL,(xmlChar*)"NAME",(xmlChar*)sRawName.c_str());
  xmlNewChild(memManager,NULL,(xmlChar*)"BLOCKSIZE",(xmlChar*)BlockSize.c_str());
  xmlNewChild(memManager,NULL,(xmlChar*)"BLOCKCOUNT",(xmlChar*)BlockCount.c_str());

  //CMP blocks
  std::string sCmpName = "CMPBLOCKS" + sRunNumber;
  memManager = xmlNewChild(memManagersNode,NULL,(xmlChar*)"MANAGER",NULL);
  xmlNewChild(memManager,NULL,(xmlChar*)"TYPE",(xmlChar*)"MemoryBlockManager");
  xmlNewChild(memManager,NULL,(xmlChar*)"NAME",(xmlChar*)sCmpName.c_str());
  xmlNewChild(memManager,NULL,(xmlChar*)"BLOCKSIZE",(xmlChar*)BlockSize.c_str());
  xmlNewChild(memManager,NULL,(xmlChar*)"BLOCKCOUNT",(xmlChar*)BlockCount.c_str());
  
  //DS blocks
  std::string sDSName = "DS_MM" + sRunNumber;
  memManager = xmlNewChild(memManagersNode,NULL,(xmlChar*)"MANAGER",NULL);
  xmlNewChild(memManager,NULL,(xmlChar*)"TYPE",(xmlChar*)"RATDiskBlockManager");
  xmlNewChild(memManager,NULL,(xmlChar*)"NAME",(xmlChar*)sDSName.c_str());
  xmlNewChild(memManager,NULL,(xmlChar*)"NUMBLOCKS",(xmlChar*)NumBlocks.c_str());
  xmlNewChild(memManager,NULL,(xmlChar*)"VECTORHASHSIZE",(xmlChar*)VectorHashSize.c_str());
  
  //Add Threads
  threadsNode = xmlNewChild(setupNode,NULL,(xmlChar *)"THREADS",NULL);
  
  //MemBlockReader
  std::string sMemBlockReaderName = "MemBlockReader" + sRunNumber;
  Run[runNumber].MemBlockReader.Set(sMemBlockReaderName,
				    Run[runNumber].Addr.GetAddrStr());  //Set MemBlockReader!
  xmlNode * thread = xmlNewChild(threadsNode,NULL,(xmlChar*)"THREAD",NULL);
  xmlNewChild(thread,NULL,(xmlChar*)"TYPE",(xmlChar*)"MemBlockReader");
  xmlNewChild(thread,NULL,(xmlChar*)"Name",(xmlChar*)sMemBlockReaderName.c_str());
  xmlNewChild(thread,NULL,(xmlChar*)"MEMORYMANAGER",(xmlChar*)sCmpName.c_str());
  xmlNewChild(thread,NULL,(xmlChar*)"FILE",(xmlChar*)Run[runNumber].files.back().c_str());
  Run[runNumber].files.pop();

  //MemBlockDecompressor
  std::string sMemBlockDecompressorName = "MemBlockDecompressor" + sRunNumber;
  thread = xmlNewChild(threadsNode,NULL,(xmlChar*)"THREAD",NULL);
  xmlNewChild(thread,NULL,(xmlChar*)"TYPE",(xmlChar*)"MemBlockDecompressor");
  xmlNewChild(thread,NULL,(xmlChar*)"Name",(xmlChar*)sMemBlockDecompressorName.c_str());
  memManager = xmlNewChild(thread,NULL,(xmlChar*)"RAW",NULL);
  xmlNewChild(memManager,NULL,(xmlChar*)"MEMORYMANAGER",(xmlChar*)sRawName.c_str());
  memManager = xmlNewChild(thread,NULL,(xmlChar*)"COMPRESSED",NULL);
  xmlNewChild(memManager,NULL,(xmlChar*)"MEMORYMANAGER",(xmlChar*)sCmpName.c_str());

  //MemBlockDecompressor
  std::string sUnPackDataName = "UnPackData" + sRunNumber;
  thread = xmlNewChild(threadsNode,NULL,(xmlChar*)"THREAD",NULL);
  xmlNewChild(thread,NULL,(xmlChar*)"TYPE",(xmlChar*)"UnPackData");
  xmlNewChild(thread,NULL,(xmlChar*)"Name",(xmlChar*)sUnPackDataName.c_str());
  xmlNewChild(thread,NULL,(xmlChar*)"MEMORYMANAGER",(xmlChar*)sRawName.c_str());
  xmlNewChild(thread,NULL,(xmlChar*)"MEMORYMANAGER",(xmlChar*)sDSName.c_str());

  //RatWriter
  std::string sRATWriterName = "RATWriter" + sRunNumber;
  thread = xmlNewChild(threadsNode,NULL,(xmlChar*)"THREAD",NULL);
  xmlNewChild(thread,NULL,(xmlChar*)"TYPE",(xmlChar*)"RATWriter");
  xmlNewChild(thread,NULL,(xmlChar*)"Name",(xmlChar*)sRATWriterName.c_str());
  xmlNewChild(thread,NULL,(xmlChar*)"MEMORYMANAGER",(xmlChar*)sDSName.c_str());
  xmlNewChild(thread,NULL,(xmlChar*)"EVENTS",(xmlChar*)"0");
  xmlNewChild(thread,NULL,(xmlChar*)"SUBRUNEVENTS",(xmlChar*)"100000");
  xmlNewChild(thread,NULL,(xmlChar*)"AUTOSAVE",(xmlChar*)"1048576");
  xmlNewChild(thread,NULL,(xmlChar*)"COMPRESSION",(xmlChar*)"1");
  xmlNewChild(thread,NULL,(xmlChar*)"BRANCHBUFFER",(xmlChar*)"37000");
  ss.str("");
  ss << "~/DCDAQ-MiniCLEAN/" << watchPath << "/" << runNumber;
  xmlNewChild(thread,NULL,(xmlChar*)"OUTPUTDIRECTORY",(xmlChar*)ss.str().c_str());

  //responses
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"RESPONSE",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"LAUNCH_RESPONSE");
  data = xmlNewChild(op,NULL,(xmlChar*)"DATA",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"WORD32",(xmlChar*)"0x0");
  xmlNewChild(data,NULL,(xmlChar*)"WORD32",(xmlChar*)"0x0");

  opBlock = xmlNewChild(RCthread,NULL,(xmlChar*)"OPBLOCK",NULL);
  op = xmlNewChild(opBlock,NULL,(xmlChar*)"OP",NULL);
  xmlNewChild(op,NULL,(xmlChar*)"TYPE",(xmlChar*)"GO");
  data = xmlNewChild(op,NULL,(xmlChar*)"DEST",NULL);
  xmlNewChild(data,NULL,(xmlChar*)"ADDR",(xmlChar*)Run[runNumber].Addr.GetAddrStr().c_str());


  char * launchString = NULL;
  int launchStringSize;
  xmlDocDumpFormatMemory(doc,(xmlChar**)&launchString,&launchStringSize,1);
  if(launchString)
    {
      DCMessage::Message message;
      message.SetData(launchString,launchStringSize);
      message.SetType(DCMessage::LAUNCH);
      message.SetDestination();
      SendMessageOut(message);
    }
  else
    {
      PrintError("Failed to build ConvertGroup XML");
    }
}

bool ConvertController::ProcessMessage(DCMessage::Message & message)
{
   bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      RemoveReadFD(iNotifyFD);
      SetupSelect();
      Loop = false;
      Running = false;
    }
  else if (message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",GetTime(&tempTime).c_str(), GetName().c_str());
      RemoveReadFD(iNotifyFD);
      SetupSelect();
      Loop = false;
    }
  else if (message.GetType() == DCMessage::GO)
    {
        DCtime_t tempTime = message.GetTime();
	printf("%s (%s): Starting up thread.\n",
  	     GetTime(&tempTime).c_str(),
  	     GetName().c_str());
	AddReadFD(iNotifyFD);
	SetupSelect();
	Loop = true;
    }
  else
    {
      ret = false;
    }
  return ret;
}
