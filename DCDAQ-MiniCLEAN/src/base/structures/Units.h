#ifndef __UNITS__
#define __UNITS__
namespace Units
{
  enum Type 
    {
      NONE = 0,
      NUMBER,
      PACKETS,
      BYTES,
      KBYTES,
      MBYTES,      
      COUNT //ALWAYS LAST
    };
  const char * GetTypeName(Units::Type type);
  Units::Type GetType(const char * name);
};
#endif
