#ifndef __EVENTMONITOR__
#define __EVENTMONITOR__

#include <RATCopyQueue.h>
#include <RAT/DS/DCDAQ_RATEVBlock.hh>
#include <RAT/DS/DCDAQ_RATDSBlock.hh>

class EventMonitor
{
public:
  EventMonitor();
  ~EventMonitor();
  void Setup(RATCopyQueue * queue);
  void operator() (RATEVBlock * event);
  void operator() (RATDSBlock * event);
private:
  void ProcessEvent(int64_t eventID,
		    uint64_t eventTime,
		    uint32_t eventTriggers,
		    uint8_t reductionLevel,
		    RAT::DS::EV * ev);
  RATCopyQueue * queue;
  time_t lastTime;
  time_t timeBetweenEvents;  
};
#endif
