#include <v1720EvilFakeReadout.h>

bool v1720EvilFakeReadout::Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager)
{
  //Find the memoryManager we need
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return  false;
    }

  //Get the Events per block.
  if(FindSubNode(SetupNode,"EVENTSPERBLOCK") == NULL)
    {
      //Default if we can't find value
      PrintWarning("Using default events per block");
      EventsPerBlock = 128;
    }
  else
    GetXMLValue(SetupNode,"EVENTSPERBLOCK",GetName().c_str(),EventsPerBlock);

  //Get the wait time between events.
  if(FindSubNode(SetupNode,"EVENTPERIOD") == NULL)
    {
      Period = 0;
    }
  else
    GetXMLValue(SetupNode,"EVENTPERIOD",GetName().c_str(),Period);
  //Set the actual wait period, taking into account EventsPerBlock
  //SetPeriod(Period);

  
  //Find out what offset we should add when we loop
  if(FindSubNode(SetupNode,"LOOPOFFSET") == NULL)    
    {
      //Default value
      LoopOffset = 0;
    }
  else
    {
      GetXMLValue(SetupNode,"LOOPOFFSET",GetName().c_str(),LoopOffset);
    }
  
  //Find out if we should loop over the events or end after one loop
  int32_t _MemoryLoop = 1;
  if(FindSubNode(SetupNode,"MEMORYLOOP") == NULL)
    {
      //Default value
      MemoryLoop = true;
    }
  else
    {
      GetXMLValue(SetupNode,"MEMORYLOOP",GetName().c_str(),_MemoryLoop);
      MemoryLoop = _MemoryLoop;
    }

  //Get the input file name
  if(FindSubNode(SetupNode,"FILENAME") == NULL)
    {
      PrintError("FILENAME subnode not found");
      return false;
    }
  else    
    GetXMLValue(SetupNode,"FILENAME",GetName().c_str(),InFileName);
  InFile = fopen(InFileName.c_str(),"rb");
  if(!InFile)
    {
      printf("Error: %s not found.\n",InFileName.c_str());
      return false;
    }

  //Get the WFD that will skip an event
  if(FindSubNode(SetupNode,"SKIPWFD") == NULL)
    {
      skipWFD = 0;
    }
  else
    GetXMLValue(SetupNode,"SKIPWFD",GetName().c_str(),skipWFD);

  //Get the event to skip
  if(FindSubNode(SetupNode,"SKIPEVENT") == NULL)
    {
      skipEvent = 0;
    }
  else
    GetXMLValue(SetupNode,"SKIPEVENT",GetName().c_str(),skipEvent);



  //Build the read file's full path.
  std::string fullPath;
  if(!std::getenv("PWD")){return false;};
  fullPath.assign(std::getenv("PWD"));
  fullPath+="/";
  fullPath+=InFileName;
  //Get file stats.
  struct stat fileStats;
  int statErr = stat(fullPath.c_str(),&fileStats);
  if(statErr){return false;}

  //Get the file's size in bytes and divide by four to get uint32_t size.
  //Allocate enough memory to hold the whole file in memory.
  FakeWFD = new uint32_t[fileStats.st_size >> 2];
  if(!FakeWFD) {return false;}
  FakeWFDSize = fileStats.st_size >>2;
  //read in the file.
  uint32_t  readNumber = fread((uint8_t*)FakeWFD,sizeof(uint8_t),FakeWFDSize <<2,InFile);
  if(readNumber != (FakeWFDSize <<2)){return false;}

  //Find events.
  for(uint32_t iWord = 0; iWord < FakeWFDSize;)
    {
      //Check for event header
      if((FakeWFD[iWord]&0xF0000000) == 0xA0000000) 
	{
	  //Store pointer to this event.
	  FakeEvent.push_back(FakeWFD + iWord);
	  //Move forward by even size.
	  iWord += (FakeWFD[iWord]&0x0FFFFFFF);
	}
      else
	{
	  iWord++;
	}
    }
# if __WORDSIZE == 64
  printf("v1720EvilFakeReadout: Loaded %lu events from file %s.\n",FakeEvent.size(),fullPath.c_str());
# else
  printf("v1720EvilFakeReadout: Loaded %u events from file %s.\n",FakeEvent.size(),fullPath.c_str());
#endif
 

  //Set the loop offset variable
  if(LoopOffset == 0)
    {
      //The user didn't specify an even offset value,
      //so use the event count
      FakeEventCount = FakeEvent.size();
    }
  else
    {
      FakeEventCount = LoopOffset;
    }
  EventCountOffset = 0;
  Ready = true;

  //We no longer need fInFile open, so let's close it now.
  fclose(InFile);
  //Reset statistics
  BlocksRead = 0;
  TimeStep_BlocksRead= 0;
  lastTime = time(NULL);  

  //Set the current FAKE event index to zero
  iEvent = 0;

  //Set real event ID and WFD event IDs to zero
  RealEventID = 0;
  WFDEventID.push_back(0);
  WFDEventID.push_back(0);
  WFDEventID.push_back(0);

  return(true);
}

