#ifndef __SELECTERRORIGNORE__
#define __SELECTERRORIGNORE__

#include <NotLockedObject.h>
#include <LockedObject.h>
#include <errno.h>
#include <map>

template<class LockModel = LockedObject>
class SelectErrorIgnore
{
public:
  typedef std::pair<bool, unsigned int> ErrnoInfo;
  typedef std::map<int, ErrnoInfo > ErrnoMap;
  
  SelectErrorIgnore() {errnoMap.clear();};
  virtual ~SelectErrorIgnore(){};
  
  bool IgnoreSelectError(int select_errno)
  {
    bool ret = false; //If lock fail, force the error to be not ignored. 
    if(lock.LockObject())
      //Object is now locked
      {
	ErrnoMap::iterator it;
	//Search our map for this errno
	it = errnoMap.find(select_errno);
	if(it == errnoMap.end())
	  //errno not found (we need to add it and pay attention)
	  {
	    std::pair<int,ErrnoInfo > newErrno;	    
	    //Add key for this entry
	    newErrno.first = select_errno;
	    //We don't want to ignore this error	   
	    newErrno.second.first = false;
	    //Start counting occurrences. 
	    newErrno.second.second = 1;
	    errnoMap.insert(newErrno);
	    ret = false;
	  }
	else
	  //errno found
	  {
	    //set the return value to the ignore state for this errno
	    ret = it->second.first;
	    //Count occurrences
	    it->second.second++;
	  }
	lock.UnlockObject();
      }
    return(ret);
  };
  void AddIgnoreErrno(int select_errno)
  {
    if(lock.LockObject())
      {
	ErrnoMap::iterator it = errnoMap.find(select_errno);
	//Search our map for this errno
	if(it == errnoMap.end())
	  //errno not found (we need to add it and don't pay attention)
	  {	   
	    std::pair<int,ErrnoInfo> newErrno;
	    //Add key
	    newErrno.first = select_errno;
	    //Igonre is true
	    newErrno.second.first = true;
	    //zero error count
	    newErrno.second.second = 0;
	    errnoMap.insert(newErrno);	
	  }
	else
	  {
	    //This already exists and we want to make sure to ignore it. 
	    it->second.first = true;
	    //leave second (errno occurrence count alone)
	  }
	lock.UnlockObject();
      }    
  };
  void RemoveIgnoreErrno(int select_errno)
  {
    if(lock.LockObject())
      {
	ErrnoMap::iterator it;
	//Search our map for this errno
	it = errnoMap.find(select_errno);
	if(it == errnoMap.end())
	  //errno not found (we need to ignore it)
	  {	   
	    std::pair<int,ErrnoInfo> newErrno;
	    //Add key
	    newErrno.first = select_errno;
	    //don't ignore
	    newErrno.second.first = false;
	    //Reset count
	    newErrno.second.second = 0;
	    errnoMap.insert(newErrno);	
	  }
	else
	  {
	    //This already exists and we want to make sure to ignore it. 
	    it->second.first = false;
	    //leave second (errno occurrence count alone)
	  }
	lock.UnlockObject();
      }    
  };

  bool GetSelectIgnore(int select_errno)
  {
    bool ret = false;
    if(lock.LockObject())
      {
	ErrnoMap::iterator it;
	//Search our map for this errno
	it = errnoMap.find(select_errno);
	if(it != errnoMap.end())
	  //errno found	  
	  {	   
	    ret = it->second.first;
	  }
	lock.UnlockObject();
      }
    return(ret);
  };

  int GetSelectCount(int select_errno)
  {
    int ret = 0;
    if(lock.LockObject())
      {
	ErrnoMap::iterator it;
	//Search our map for this errno
	it = errnoMap.find(select_errno);
	if(it != errnoMap.end())
	  //errno found	  
	  {	   
	    ret = it->second.second;
	  }
	lock.UnlockObject();
      }
    return(ret);
  };

  void PrintReport()
  {
    if(lock.LockObject())
      {
	ErrnoMap::iterator it;
	printf("Errno report\n");
	//      0123456789012345678901234567890123456789
	printf("Errno:    Ignore    Count     Error\n");
	for(it = errnoMap.begin();it != errnoMap.end();it++)
	  {
	    if(it->second.first)
	      //ignore == true
	      {
		//      0123456789012345678901234567890123456789
		printf("   %03d      true    %08d      %s\n",
		       it->first,
		       it->second.second,
		       strerror(it->first)
		       );
	      }
	    else
	      //ignore == false
	      {
		//      0123456789012345678901234567890123456789
		printf("   %03d      false   %08d      %s\n",
		       it->first,
		       it->second.second,
		       strerror(it->first)
		       );
	      }
	  }
	lock.UnlockObject();
      }
  };
  
private:
  ErrnoMap errnoMap;
  LockModel lock;
  
  SelectErrorIgnore(const SelectErrorIgnore &);
  SelectErrorIgnore & operator=(const SelectErrorIgnore&);
  
};
#endif
