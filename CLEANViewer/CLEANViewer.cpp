//*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/

#include "CLEANViewer.h"
CLEANViewer::CLEANViewer()
{}

CLEANViewer::CLEANViewer(const TGWindow *p, UInt_t w, UInt_t h, const char * _DataFile, Bool_t _DebugSwitch, UInt_t _AvgValue,Bool_t _ZMQSwitch,UInt_t _Period,vector<unsigned> _ReducVect,std::string _RATServer,Bool_t _Enable3DMode,size_t _BufferSize)
  :TGMainFrame(p, w, h)
{
  //DEBUG is set via the command line flag -D, toggles on/off debug mode.
  DEBUG = _DebugSwitch;
  
  //AVG is set via command line flag -A, toggles on/off loading of all events in beginning
  AVG=_AvgValue;

  //SHIFT is set via command line flag -S, toggle on/off shift mode
 

  ZMQ= _ZMQSwitch;
  bufferSize=_BufferSize;
  fprintf(stderr,"buff %zu\n",bufferSize);
  if(_RATServer.compare("")==0)
    RATServerMode=false;
  else RATServerMode=true;
  if(DEBUG) fprintf(stderr,"CLEANViewer::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, Char_t * DataFile)\n");
  //intial NULL allocation of rat pointers
  ratReader = NULL;
  ratDS = NULL;
  ratEV = NULL;
  ratSend = NULL;
 
  eventNumberEntry=NULL;
  eventNumberEntry3D=NULL;
  nextEvent3D=NULL;
  prevEvent3D=NULL;
  colorScaleMax=5000;
  sizeScaleMax=50;
   winFilter=NULL;
  winFilterLocal=NULL;
  filterNumSelected=0;
  calibrated=false;
  drawPromptTotal=false;
  drawSeperator=false;
  AxisSelecting=true;
  orderByBoard=true;
  labelByBoard=true;
  AutoScaling=true;
  FirstRead=false;
  Draw3D=_Enable3DMode;
  AvgDraw=AVG;
  cursorOverPMT=99999999;
  timeOutFactor=10;
  winFilterFrame=NULL;
  ColorScaleWin=NULL;
  SupressEmpty=true;
  LocalFilter=new LocalEventFilter();
  InfoFrame=NULL;
  highLightPMTButton=NULL;
  InfoTopText=NULL;
  InfoMidText=NULL;
  //InfoWindow=NULL;
  InfoTextPad=NULL;
  InfoPlotPad=NULL;
//winSleep=NULL;
  ColI=-1;
  PromptHist=NULL;
  PromptChannelsHist=NULL;
  PromptCalibChannelsHist=NULL;
  PromptCalibHist=NULL;
  LateHist=NULL;
  LateCalibHist=NULL;
  LateCalibChannelsHist=NULL;
  LateChannelsHist=NULL;
  miniPromptHist=NULL;
  miniLateHist=NULL;
  miniPromptAxis=NULL;
  miniLateAxis=NULL;
  winHist=NULL;
  frameHist=NULL;
  histECanWin=NULL;
 
  winRaw=NULL;
  tabRawWin=NULL;
  rawECanWin=NULL;
  winCalib=NULL;
  tabCalibWin=NULL;
  calibECanWin=NULL;
  

  StopButton3D=NULL;
  StartButton3D=NULL;
  fprintf(stderr,"/////////////////////////////////////////////////////////// \n");
  fprintf(stderr,"//                This is CLEANViewer                    // \n");
  fprintf(stderr,"//                  Not yet Sparta                       // \n");
  fprintf(stderr,"//             And Definitely NOT RooFit                 // \n");
  fprintf(stderr,"//                    Created By                         // \n");
  fprintf(stderr,"//                    (Redacted)                         // \n");
  fprintf(stderr,"//           To Look at Events from MiniCLEAN            // \n");
  fprintf(stderr,"//                      Enjoy!                           // \n");
  fprintf(stderr,"/////////////////////////////////////////////////////////// \n");
  //initial setup funtion for gui widgets and frames

  
  if(RATServerMode)
    {
      fprintf(stderr,"OutProc\n");
      ratSend=new RAT::OutNetProc();
      fprintf(stderr,"versionCheck\n");
      ratSend->SetI("versioncheck",1);
      fprintf(stderr,"host\n");
      ratSend->SetS("host",_RATServer);
      fprintf(stderr,"SetS done\n");
    }

  ratDC=RAT::DetectorConfig::GetDetectorConfig("MiniCLEAN","MiniCLEAN/MiniCLEAN.geo");
  
  man=NULL;
  v=NULL;
  sizeScaleState=1;
  colorScaleState=2;

  SetMenu();
  SetToolBar();
  //guiHorizontal is the main Parent Frame
  horizontalLayout = new TGLayoutHints(kLHintsCenterY | 
				       kLHintsExpandX | 
				       kLHintsExpandY);
  guiHorizontal = new TGHorizontalFrame(this, 1190, 765);
  AddFrame(guiHorizontal, horizontalLayout);


  SetGroupFrames();
  SetTabs();

  //convert const char* to char* before passing to the LoadFile function
  dataFile = new char[100];
  int nChar = sprintf(dataFile,"%s", _DataFile);

  Bool_t Ready=true;
  //if a data file has been passed as a command line argument, load it.
  if (nChar > 0)
    {
      if (ZMQ)
	{
	  zmqEventFilterVec.push_back(new StreamFilter());
	  filterNames.push_back("Filter 0");
	  fprintf(stderr,"filterNames pushed back size %zu\n",filterNames.size());
	  zmqEventFilterVec.back()->period=_Period;
	 
	  if(_ReducVect.size()>0)
	    {
	     
	      zmqEventFilterVec.back()->ReducLevels=0;

	      for(size_t iRLV=0;iRLV<_ReducVect.size();iRLV++)
		zmqEventFilterVec.back()->ReducLevels=zmqEventFilterVec.back()->ReducLevels | (1<<_ReducVect[iRLV]);
	    }
	  else
	    {
	     
	      zmqEventFilterVec.back()->ReducLevels=0;

	      zmqEventFilterVec.back()->ReducLevels=zmqEventFilterVec.back()->ReducLevels | (1<<ReductionLevel::CHAN_ZLE_WAVEFORM);
	      zmqEventFilterVec.back()->ReducLevels=zmqEventFilterVec.back()->ReducLevels | (1<<ReductionLevel::CHAN_PROMPT_TOTAL);
	    }
	  Ready=LoadShiftModeZMQ(dataFile);
	  alarm(timeOutFactor*zmqEventFilterVec.back()->period);
	  printf("loaded zmq \n");
 
	}
      else
	LoadFile(dataFile);
    }

 
  if(Ready)
    {
     
      //Mapping Windows and Layout
      MapSubwindows();
     
      Layout();
 
      SetWindowName("CLEAN Event Viewer");
      SetIconName("CLEAN Event Viewer");
      MapWindow();
 
      //if(ZMQ)
      CheckAllPMTs();
 
      FirstRead=true;
    }
}

//Basic Deconstructor
CLEANViewer::~CLEANViewer()
{
  fprintf(stderr,"CLEANViewer::~CLEANViewer()\n");
  
  //DON'T FORGET TO CLEAN UP YOUR MEMORY!
  delete PromptCalibHist;
  delete PromptCalibChannelsHist;
  delete LateCalibHist;
  delete LateCalibChannelsHist;
  
  delete ratReader;
  delete guiToolBar;
  delete guiTab;
  
  //delete selectionTab;
  delete tabBoard;
 

  delete tabPMT;
  delete tabLayout;
  
  delete rawECanvas;
  delete calECanvas;
  delete displayECanvas;
  delete histogramECanvas;

  
  delete groupLayout;
  delete groupVertical;
  delete orderingVertical;
  delete pmtOrderGroup;
  delete pmtSelect;
  delete pmtGroup;
  delete eventGroup;
  delete eventHorizontal;
  delete eventNumberEntry;
  delete eventProgress;
  delete guiHorizontal;
  delete horizontalLayout;
  delete popupFile;
  delete popupView;
  delete popupRAT;
  delete popupEV;
  delete popupHelp;
  delete popupWFD;
  delete guiMenu;
  delete dataFile;
  

  
  for(UInt_t iFVector = 0; 
      iFVector < pmtFrameVector.size();
      iFVector++)
    delete pmtFrameVector[iFVector];

  for(UInt_t iOFVector = 0; 
      iOFVector < pmtOrderFrameVector.size();
      iOFVector++)
    delete pmtOrderFrameVector[iOFVector];

  for(UInt_t iMVector = 0; 
      iMVector < wfdMenuVector.size();
      iMVector++)
    delete wfdMenuVector[iMVector];

  /* for(UInt_t iCVector = 0;
      iCVector < pmtCheckVector.size();
      iCVector++)
    delete pmtCheckVector[iCVector];
  */

  for(UInt_t iPVector = 0;
      iPVector < PolyLineVector.size();
      iPVector++)
    delete PolyLineVector[iPVector];

  for(UInt_t iAVector = 0;
      iAVector < AxisVector.size();
      iAVector++)
    delete AxisVector[iAVector];

  if(LocalFilter)
    {
      delete LocalFilter;
    }

  //Cleanup();

  if(ZMQ)
    {
      fprintf(stderr,"deleting ZMQ stuff \n");
      daqSocketRequest->close();
      delete daqSocketRequest;
      daqSocketData->close();
      delete daqSocketData;
      delete context;
      zmqEventFilterVec.clear();
    }
}
