#include "CLEANViewer.h"

//Function for the initial setup of guiMenu
void CLEANViewer::SetMenu()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::SetMenu()\n");

  guiMenu = new TGMenuBar(this, 60, 20, 
			  kHorizontalFrame | 
			  kRaisedFrame);

  popupFile = new TGPopupMenu(gClient->GetRoot());
  popupFile->Connect("Activated(Int_t)", 
		     "CLEANViewer",
		     this, 
		     "FileMenu(Int_t)");
  popupFile->AddEntry("Open File", 1);
  popupFile->AddEntry("Open DQM Connection",11);
  popupFile->AddEntry("Close File", 2);
  popupSave=new TGPopupMenu(gClient->GetDefaultRoot());
  popupSave->AddEntry("Save Raw",30);
  popupSave->AddEntry("Save Calibrated",31);
  popupSave->AddEntry("Save Display",32);
  popupSave->AddEntry("Save Hists",33);
  popupFile->AddPopup("Save",popupSave);
  popupFile->AddSeparator();
  popupFile->AddEntry("Start Stream", 4);
  popupFile->AddEntry("Stop Stream", 5);
  popupFile->AddSeparator();
  popupFile->AddEntry("Exit CLEAN Viewer", 6);
  if(ZMQ or DAQdat)
    {
      popupFile->DisableEntry(1);
      popupFile->DisableEntry(2);
    }
  popupFile->DisableEntry(4);
  popupFile->DisableEntry(5);
  
  popupView = new TGPopupMenu (gClient->GetRoot());
  popupView->Connect("Activated(Int_t)", 
		     "CLEANViewer",
		     this, 
		     "ViewMenu(Int_t)");
  popupView->AddEntry("Reset Canvas", 11);
  /*popupGridLines=new TGPopupMenu(gClient->GetRoot());
  popupGridLines->AddEntry("X",12);
  popupGridLines->AddEntry("Y",13);
  popupView->AddPopup("Show Grid Lines",popupGridLines);*/
  popupView->AddEntry("Channel Separators",16);
  popupView->AddEntry("Supress Empty Channels",161);
  popupView->AddEntry("Y-axis CommonScale",14);
  popupView->AddEntry("View Full Waveform", 15);
  popupView->CheckEntry(14);
  popupView->AddSeparator();
  popupOrdering=new TGPopupMenu(gClient->GetRoot());
  popupOrdering->AddEntry("Order by Board/Channel",17);
  popupOrdering->AddEntry("Order by PMT ID",18);
  popupView->AddPopup("Wavefrom Ordering",popupOrdering);
  popupLabeling=new TGPopupMenu(gClient->GetRoot());
  popupLabeling->AddEntry("Label by Board/Channel",19);
  popupLabeling->AddEntry("Label by PMT ID",20);
  popupView->AddPopup("Wavefrom Labeling",popupLabeling);
  popupView->AddSeparator();
  popupView->AddEntry("Auto-Scale Colors",21);
  popupLabeling->CheckEntry(19);
  popupOrdering->CheckEntry(17);
  popupView->CheckEntry(21);
  popupView->AddEntry("Draw Average Over all Events",22);
  if(AVG)
    popupView->CheckEntry(22);
  else
    popupView->DisableEntry(22);
  popupUnsnap=new TGPopupMenu(gClient->GetRoot());
  popupView->AddPopup("Unsnap",popupUnsnap);
  popupUnsnap->AddEntry("Raw Waveform",23);
  popupUnsnap->AddEntry("Calibrated Waveform",24);
  popupUnsnap->AddEntry("Histogram",25);
//  popupUnsnap->AddEntry("Event Display",25);
  popupView->AddEntry("Draw 3D Event Display",26);
  if(Draw3D)
    popupView->CheckEntry(26);
  if(SupressEmpty)
    popupView->CheckEntry(161);
  popupRAT = new TGPopupMenu (gClient->GetRoot());
  popupRAT->Connect("Activated(Int_t)", 
		    "CLEANViewer",
		    this, 
		    "RATMenu(Int_t)");
  popupRAT->AddEntry("Next Event", 21);
  popupRAT->AddEntry("Previous Event", 22);
  popupRAT->AddSeparator();
  popupRAT->AddEntry("Uncheck all PMTs", 23);
  popupRAT->AddEntry("Check all PMTs", 24);

  if(ZMQ)
    {
      popupRAT->DisableEntry(22);
      popupRAT->DisableEntry(21);
    }

  popupEV = new TGPopupMenu (gClient->GetRoot());
  popupEV->Connect("Activated(Int_t)", 
		   "CLEANViewer",
		   this, 
		   "EVMenu(Int_t)");
  
  popupHelp = new TGPopupMenu (gClient->GetRoot());
  popupHelp->AddEntry("Comand Line Arguments", 31);
  popupHelp->AddEntry("About CLEAN Viewer", 32);
  popupHelp->DisableEntry(31);
  popupHelp->DisableEntry(32);
  
  popupWFD = new TGPopupMenu (gClient->GetRoot());
  popupWFD->Connect("Activated(Int_t)", 
		    "CLEANViewer",
		    this, 
		    "WFDMenu(Int_t)");

  popupDQM = new TGPopupMenu(gClient->GetRoot());
  popupFilters=new TGPopupMenu(gClient->GetRoot());
  popupDQM->AddPopup("Filters",popupFilters);
 
  popupFilters->AddEntry("Filter 0",11);
  popupDQM->AddEntry("Add Filter",1);
  popupRemoveFilters=new TGPopupMenu(gClient->GetRoot());
  popupDQM->AddPopup("Remove Filter",popupRemoveFilters);
  popupRemoveFilters->AddEntry("Filter 0",101);
  popupDQM->AddEntry("Stop",2);
  popupDQM->AddEntry("Go",3);
  popupDQM->DisableEntry(3);
  popupBuffer = new TGPopupMenu(gClient->GetRoot());
  
  popupDQM->AddPopup("Buffer",popupBuffer);
  popupDQM->AddEntry("Local Filter",400);
  if(!ZMQ)
    {

      popupDQM->DisableEntry(2);
      popupDQM->DisableEntry(3);
      popupFilters->DisableEntry(11);
      popupDQM->DisableEntry(1);
      popupDQM->DisableEntry(101);
    }
  else
    {
      popupDQM->DisableEntry(400);
    }
  
  popupDQM->Connect("Activated(Int_t)",
		    "CLEANViewer",
		    this,
		    "DQMMenu(Int_t)");

  popup3D=new TGPopupMenu(gClient->GetRoot());
  popup3DSize=new TGPopupMenu(gClient->GetRoot());
  popup3D->AddPopup("Size Scaling",popup3DSize);
  popup3DSize->AddEntry("Off",11);
  popup3DSize->AddEntry("Charge",12);
  popup3DColor=new TGPopupMenu(gClient->GetRoot());
  popup3D->AddPopup("Color Scaling",popup3DColor);
  popup3DColor->AddEntry("Off",21);
  popup3DColor->AddEntry("Charge",22);
  popup3D->Connect("Activated(Int_t)",
		    "CLEANViewer",
		    this,
		    "ThreeDMenu(Int_t)");
  popup3DSize->CheckEntry(12);
  popup3DColor->CheckEntry(22);
  this->AddFrame(guiMenu, 
		 new TGLayoutHints(kLHintsTop | kLHintsExpandX));
  
  guiMenu->AddPopup("&File", popupFile,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
  guiMenu->AddPopup("&View", popupView,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
  guiMenu->AddPopup("&RAT", popupRAT,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
  guiMenu->AddPopup("&EV Select", popupEV,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
  guiMenu->AddPopup("&Help", popupHelp,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
  guiMenu->AddPopup("&WFD Select", popupWFD,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
 
  guiMenu->AddPopup("&DQM",popupDQM,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
  guiMenu->AddPopup("&3DViewer",popup3D,
		    new TGLayoutHints(kLHintsTop | kLHintsLeft,
				      0, 4, 0, 0));
 
}

//Function for the initial setup of guiToolBar
void CLEANViewer::SetToolBar()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::SetToolBar()\n");
  
  //Conatiner with Button Icon file names and relative paths 
  //const char *ButtonIcons[] = {
  const char *ButtonIconsRelativePath[] = {
    "/icons/89.png",
    "/icons/55.png",
    "/icons/document_save.png",
    "/icons/186.png",
    "",
    "/icons/178.png",
    "",
    "/icons/17.png",
    "/icons/162.png",
    0
  };

  //We need to convert the relative paths to absolute paths. 
  //To do this we create a new array of paths that add on the build
  //path to the relative paths.   The build path is set using 
  //__BUILDPATH__ from the compiler

  //Parse the build path from the __BUILDPATH__ macro define
  char const * buildPath = NULL;
  size_t buildPathSize = 0;
#ifdef __BUILDPATH__
  //If defined, use some macro magic to get the value of __BUILDPATH__
  //into a string
#define QUOTEME2(x) QUOTEME(x)
#define QUOTEME(x) #x
  buildPath = QUOTEME2(__BUILDPATH__);
#else
  //If it isn't defined, assume the pngs are always relative to the current directory
  buildPath = ".";
#endif
  buildPathSize = strlen(buildPath);

  //Calculate how many elements we have in our relative path array so we know how many to make in our absolute path array
  size_t ButtonIconCount = (sizeof(ButtonIconsRelativePath)/sizeof(char*)) - 1u;  //1 for null termination
  char ** ButtonIcons = (char **) (new char* [ButtonIconCount+1u]);
  for(size_t i = 0; i < ButtonIconCount;i++)
    {
      //For spacing, we need to conserve paths to a zero size null terminated string, so check for a zero sized string and don't add the build path if it is zero. 
      if(strlen(ButtonIconsRelativePath[i]) > 0)
	{
	  //Figure out how big our new entry needs to be for the absolute path  
	  size_t absolutePathSize = buildPathSize + strlen(ButtonIconsRelativePath[i]) + 1u;
	  //Allocated the new string
	  ButtonIcons[i] = new char[absolutePathSize];
	  //And print the absolute path into it. 
	  snprintf(ButtonIcons[i],absolutePathSize,
		   "%s%s",buildPath,ButtonIconsRelativePath[i]);
	}
      else
	{
	  ButtonIcons[i] = new char[1];
	  ButtonIcons[i][0] = 0;
	}
    }
  //Null termination of our main array
  ButtonIcons[ButtonIconCount] = NULL;


  //Container with Button tool tips
  const char *toolTips[] = {
    "Open File",
    "Close File",
    "Save Canvas",
    "Reset Canvas",
    "",
    "Exit CLEAN Viewer",
    "",
    "Stop Stream",
    "Stream Data",
    0
  };
  
  //number of pixels before the first button
  int separator = 5;
  string stream = "Stream Data";
  
  //structure containing toolbar button information
  ToolBarData_t barData[9];
  
  //Toolbar object a child of the main frame
  guiToolBar = new TGToolBar(this, 1200, 80);
  for (int iBar = 0; iBar < 9; iBar++)
    {
      barData[iBar].fPixmap = ButtonIcons[iBar]; //icon file
      barData[iBar].fTipText = toolTips[iBar];   //tool tip text
      barData[iBar].fStayDown = kFALSE;          //Button Behavior when clicked
      if (stream.compare(toolTips[iBar]) == 0)
	//Stream button stays down until Stop Stream is pressed
	barData[iBar].fStayDown = kTRUE;
      //Tool Bar button ids are in the range 2000-2999
      barData[iBar].fId = 2000 + iBar;           //Button id
      barData[iBar].fButton = NULL;              //Button Pointer
      if (strlen(ButtonIcons[iBar]) == 0)
	{
	  //number of pixels between groups of buttons
	  separator = 50;
	  continue;
	}
      guiToolBar->AddButton(this, &barData[iBar], separator);
      //Default number of pixels between buttons
      separator = 0;
    }
  
  //Disable Buttons that don't have functions yet
  guiToolBar->GetButton(2000)->SetEnabled(kFALSE);
  guiToolBar->GetButton(2001)->SetEnabled(kFALSE);
  if(!ZMQ)
    guiToolBar->GetButton(2007)->SetEnabled(kFALSE);
  guiToolBar->GetButton(2008)->SetEnabled(kFALSE);
  
  if(ZMQ)
    {
      guiToolBar->GetButton(2007)->Connect("Clicked()","CLEANViewer",this,"StreamStop()");
      guiToolBar->GetButton(2008)->Connect("Clicked()","CLEANViewer",this,"StreamGo()");
      
    }
  //Add the tool bar to the main frame
  this->AddFrame(guiToolBar, 
		 new TGLayoutHints(kLHintsTop | kLHintsExpandX));
  //Add the horiontal line for a separator
  TGHorizontal3DLine *toolHorizontal = new TGHorizontal3DLine(this);
  this->AddFrame(toolHorizontal, 
		 new TGLayoutHints(kLHintsTop | kLHintsExpandX));


}



//Function to fill the pmtCheckVector
void CLEANViewer::FillPmtVector()
{
  if(DEBUG) fprintf(stderr, "CLEANViewer::FillPmtVector()\n");

  

  vector<int> pmtIDs;
  
  char TempPMTLabelBuff [100];
  char TempWFDLabelBuff [100];
  char TempToolTipLabelBuff[100];

  pmtCheckInfoVector.clear();



  bool isTherePMT;
  std::vector<TGGroupFrame*> wfdFrameVector;
  for (size_t iBoard = 0; iBoard<OrderedBoards.size(); iBoard++)
    { 
      int iWFD=OrderedBoards[iBoard].GetID();
     
      //int iWFD=OrderedBoards[iBoard]->GetID();
      NChannelsClickedOnBoardVector.push_back(0);
      std::vector<int> TempidsVector;


      sprintf(TempWFDLabelBuff, "WFD %i", iWFD);

      TGPopupMenu *tempPopup = new TGPopupMenu (gClient->GetRoot());
      wfdMenuVector.push_back(tempPopup);
      wfdMenuVector.back()->AddEntry("Check All PMTs", 60+iWFD);
      wfdMenuVector.back()->AddEntry("Uncheck All PMTs", 80+iWFD);
      popupWFD->AddPopup(TempWFDLabelBuff, wfdMenuVector.back());
	

      if(DEBUG) fprintf(stderr,"Just did popup \n");



      TGGroupFrame *TempwfdFrame=new TGGroupFrame(pmtGroup,TempWFDLabelBuff);
      wfdFrameVector.push_back(TempwfdFrame);
      pmtGroup->AddFrame(wfdFrameVector.back(),
			 new TGLayoutHints(kLHintsExpandX |
					   kLHintsExpandY |
					   kLHintsCenterX,0,0,0,0));

      

      TGHorizontalFrame *TempLabelFrame=new TGHorizontalFrame(wfdFrameVector.back());
      wfdFrameVector.back()->AddFrame(TempLabelFrame,
				      new TGLayoutHints(kLHintsExpandX |
							//kLHintsExpandY|
					   kLHintsCenterY,16,18,0,0));

      for(int iChan=0;iChan<8;iChan++)
	{
	  sprintf(TempPMTLabelBuff, "%3i",iChan);


	  TGLabel * TempLabel=new TGLabel(TempLabelFrame,TempPMTLabelBuff);
	  TempLabelFrame->AddFrame(TempLabel,new TGLayoutHints(kLHintsExpandX |
							kLHintsExpandY|
					   kLHintsBottom,0,0,0,0));
	  
	}

	  TGLabel * TempLabel=new TGLabel(TempLabelFrame,"All");

	  TempLabelFrame->AddFrame(TempLabel,new TGLayoutHints(
							kLHintsExpandY|
					   kLHintsBottom,30,0,0,0));

      TGHorizontalFrame *TempCheckFrame = new TGHorizontalFrame(wfdFrameVector.back());
      wfdFrameVector.back()->AddFrame(TempCheckFrame,
			 new TGLayoutHints(kLHintsExpandX |
					   kLHintsCenterY,0,0,0,0));


      TGButtonGroup *TempButtonGroup=new TGButtonGroup(TempCheckFrame,"",kHorizontalFrame);
      pmtGroupVector.push_back(TempButtonGroup);
      TempCheckFrame->AddFrame(pmtGroupVector.back(),
			 new TGLayoutHints(kLHintsExpandX |
					   //kLHintsExpandY |
					   kLHintsCenterY,0,0,0,0));
      for(int iInput=0;iInput<8;iInput++)
	{
	  isTherePMT=true;
	  char tempInputLabel[25];
	  char tempToolTipText[25];
	  sprintf(tempInputLabel,"%i%i",iWFD,iInput);
	  int PMTID=InputMap[tempInputLabel];
	  if(PMTID<0)
	    {

	      sprintf(TempPMTLabelBuff, "No PMT");
	      sprintf(tempToolTipText, "No PMT");
	     
	      isTherePMT=false;
	    }
	  else 
	    {
	      sprintf(TempPMTLabelBuff, "%3i",iInput);
	      sprintf(tempToolTipText,"PMT %i",PMTID);
	    }
	  TGCheckButton *TempPMT = 
	    new TGCheckButton(pmtGroupVector.back(), 
			      "", 
			      1000+PMTID);
	  if(!isTherePMT)
	    {
	       TempPMT->SetEnabled(kFALSE);
	    }
	 
	  pmtCheckInfo TempPmtCheckInfo;
	  TempPMT->SetToolTipText(tempToolTipText);
	 
	  TempPmtCheckInfo.CheckButton=TempPMT;
	  TempPmtCheckInfo.Board=iWFD;
	  TempPmtCheckInfo.Channel=iInput;
	  TempPmtCheckInfo.PMTID=PMTID;
  
	  
	 
	  pmtCheckInfoVector.push_back(TempPmtCheckInfo);

	  pmtGroupVector.back()->AddFrame(pmtCheckInfoVector.back().CheckButton,new TGLayoutHints(kLHintsExpandX |								  kLHintsExpandY|
                  kLHintsCenterY,0,0,0,0));

	  idGroupVectorMap[1000+PMTID]=iBoard;
	  pmtIDsVector.push_back(1000+PMTID);
	  TempidsVector.push_back(PMTID+1000);

	  
	  //pmtCheckVector.back()->SetFont(gClient->GetFontByName("-adobe-courier-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
	  if(DEBUG) fprintf(stderr,"just pushed back TempPMT \n");


	  sprintf(TempToolTipLabelBuff,"PMT %i",PMTID);
	  
	  pmtGroupVector.back()->Connect("Clicked(Int_t)","CLEANViewer",this,"pmtButtonClicked(Int_t)");



	}
      idsOnBoardVector.push_back(TempidsVector);
      TGVertical3DLine * vertSeperator=new TGVertical3DLine(TempCheckFrame);
      TempCheckFrame->AddFrame(vertSeperator,new TGLayoutHints(kLHintsExpandY));
      TGButtonGroup * TempWFDButtonGroup=new TGButtonGroup(TempCheckFrame);
      wfdGroupVector.push_back(TempWFDButtonGroup);
      TempCheckFrame->AddFrame(wfdGroupVector.back());
      TGCheckButton * TempAllWFDCheck=new TGCheckButton(wfdGroupVector.back(),"",iBoard);
      TempAllWFDCheck->SetEnabled();
      wfdGroupVector.back()->Connect("Clicked(Int_t)","CLEANViewer",this,"wfdButtonClicked(Int_t)");
    }
  sort(pmtCheckInfoVector.begin(),pmtCheckInfoVector.end(),CLEANViewer::orderbyIDs);

  int indexPmt=0;
  int indexFrame=-1;


    
  for(unsigned int iPMT=0;iPMT<pmtCheckInfoVector.size();iPMT++)
    {
     
      //if(pmtCheckInfoVector[iPMT].PMTID!=-1)
      //{
      if (indexPmt % 4 == 0)
	{


	  TGButtonGroup *TempButtonGroup=new TGButtonGroup(pmtOrderGroup,"",kHorizontalFrame);
	  pmtOrderGroupVector.push_back(TempButtonGroup);
	  pmtOrderGroup->AddFrame(pmtOrderGroupVector.back(), 
				  new TGLayoutHints(
						    //kLHintsExpandY |
						    kLHintsCenterY,0,0,0,0));

	  indexFrame+=1;
	}
	  
      sprintf(TempPMTLabelBuff,"%i",pmtCheckInfoVector[iPMT].PMTID);
      int vecIndex=idGroupVectorMap[pmtCheckInfoVector[iPMT].PMTID+1000];
      //fprintf(stderr,"vecIndex %i, size %i \n",vecIndex,(int) pmtGroupVector.size());
      int id=pmtGroupVector[vecIndex]->GetId(pmtCheckInfoVector[iPMT].CheckButton);

      TGCheckButton *TempPMT = 
	new TGCheckButton(pmtOrderGroupVector.back(),
			  TempPMTLabelBuff, 
			  id);

      pmtCheckInfoVector[iPMT].CheckOrderButton=TempPMT;

	 
      idOrderGroupVectorMap[id]=indexFrame;

      pmtOrderGroupVector.back()->Connect("Clicked(Int_t)","CLEANViewer",this,"pmtOrderButtonClicked(Int_t)");
      if(int(iPMT)<ratEV->GetPMTCount())
	{
	  int wfd=ratEV->GetPMT(iPMT)->GetBoardNumber();
	  int input=ratEV->GetPMT(iPMT)->GetInputNumber();
	  sprintf(TempToolTipLabelBuff,"WFD %i, Input %i",wfd,input);
	  pmtCheckInfoVector[iPMT].CheckOrderButton->SetToolTipText(TempToolTipLabelBuff);
	}
      indexPmt+=1;
      //    }

	  
    }




  sort(pmtCheckInfoVector.begin(),pmtCheckInfoVector.end(),CLEANViewer::orderbyBoards);
  
  //  if(pmtCheckInfoVector.size()<92) fprintf(stderr,"\n***********Warning: You are running with fewer than 92 channels, be aware that selecting a PMT with ID higher than %i in the display select tab will cause a seg fault*********************\n",int(pmtCheckInfoVector.size())-1+PMTIDStart); 
}

bool CLEANViewer::orderbyIDs(const pmtCheckInfo &a,const pmtCheckInfo &b)
{
  return a.PMTID<b.PMTID;
}

bool CLEANViewer::orderbyBoards(const pmtCheckInfo &a,const pmtCheckInfo &b)
{
  return ((a.Board<=b.Board)&(a.Channel<b.Channel));
}
//Function for the initial setup of guiTab
void CLEANViewer::SetTabs()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::SetTabs()\n");

  //LayoutHints for all the tabs
  tabLayout = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY);

  tabVertical = new TGVerticalFrame(guiHorizontal, 760, 705);
  guiHorizontal->AddFrame(tabVertical,new TGLayoutHints(kLHintsExpandY|kLHintsExpandX));//, tabLayout);

  //construct and add the main TGTab frame
  guiTab = new TGTab(tabVertical, 790,590);
  tabVertical->AddFrame(guiTab,tabLayout);
  
  //add the individual tabs with thier titles
  tabRawWave = guiTab->AddTab("Raw Waveform");
  tabCalWave = guiTab->AddTab("Calibrated Waveform");
  tabHistogram = guiTab->AddTab("Histograms");
  //tabEventDis = guiTab->AddTab("Event Display");


  //add blank canvas to all the tabs for initial setup
  rawECanvas = new TRootEmbeddedCanvas("rawECanvas", tabRawWave, 750, 590);
  tabRawWave->AddFrame(rawECanvas, tabLayout);
  rawTCanvas = rawECanvas->GetCanvas();
  rawTCanvas->SetEditable(kFALSE);

  rawTCanvas->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
                                  "CLEANViewer", this,
                                  "RawCanvasClicked(Int_t,Int_t,Int_t,TObject*");



    //    TempMenuItem->SetToggle();


  yLabel = new TText(0.08, 0.77, "ADC Counts");
  yLabel->SetTextAngle(90);
  yLabel->SetTextSize(0.025);

  yLabelCalib= new TText(0.08,0.75, "Voltage (mV)");
  yLabelCalib->SetTextAngle(90);
  yLabelCalib->SetTextSize(0.025);

  
  calECanvas = new TRootEmbeddedCanvas("calECanvas", tabCalWave, 750,590);
  tabCalWave->AddFrame(calECanvas, tabLayout);
  calibTCanvas=calECanvas->GetCanvas();
  calibTCanvas->SetEditable(kFALSE);

  calibTCanvas->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
                                  "CLEANViewer", this,
                                  "CalibCanvasClicked(Int_t,Int_t,Int_t,TObject*");



  histogramECanvas = new TRootEmbeddedCanvas("histogramECanvas", 
					     tabHistogram, 750, 590);
  tabHistogram->AddFrame(histogramECanvas, tabLayout);
  histogramTCanvas = histogramECanvas->GetCanvas();
  


  
  //displayECanvas = new TRootEmbeddedCanvas("displayECanvas", 
  //					   tabEventDis, 750, 590);
  //displayTCanvas = displayECanvas->GetCanvas();
  displayTCanvas=new TCanvas("","",400,400);
  displayTCanvas->SetFillColor(1);
 
  //displayTCanvas->SetEditable(kFALSE);
  //displayECanvas->GetCanvas()->cd();
  //displayTPad=new TPad("","",.05,.05,.95,.95);
  //  displayTView=TView::CreateView(1);
  //  tabEventDis->AddFrame(displayECanvas, tabLayout);

  
    



TGHorizontalFrame *bottomFrame=new TGHorizontalFrame(tabVertical);
 tabVertical->AddFrame(bottomFrame,new TGLayoutHints(kLHintsExpandX));

 /*orderButtonGroup=new TGButtonGroup(bottomFrame,"PMT Ordering");
  orderByBoardButton=new TGRadioButton(orderButtonGroup,"Order by Board/Channel",1);
  bottomFrame->AddFrame(orderButtonGroup);//,new TGLayoutHints(kLHintsExpandX,10,10,0,0));
  orderByPMTButton=new TGRadioButton(orderButtonGroup,"Order by PMT ID",2);

  orderButtonGroup->SetButton(1);
  orderButtonGroup->Connect("Clicked(Int_t)", 
			      "CLEANViewer",
			      this, 
			      "PlotChannel()");

  labelButtonGroup=new TGButtonGroup(bottomFrame,"Waveform Labeling");
  labelByBoardButton=new TGRadioButton(labelButtonGroup,"Label by Board/Channel",1);
  bottomFrame->AddFrame(labelButtonGroup);//,new TGLayoutHints(kLHintsExpandX,10,10,0,0));
  labelByPMTButton=new TGRadioButton(labelButtonGroup,"Label by PMT ID",2);

  labelButtonGroup->SetButton(1);
  labelButtonGroup->Connect("Clicked(Int_t)", 
			      "CLEANViewer",
			      this, 
			    "PlotChannel()");*/

//add group frame for the x-axis adjust
  zoomGroup = new TGGroupFrame(bottomFrame, "X Axis Adjust",kHorizontalFrame);
  zoomGroup->SetTitlePos(TGGroupFrame::kLeft);
  bottomFrame->AddFrame(zoomGroup, new TGLayoutHints(kLHintsCenterX |
						     kLHintsExpandX, 
						     0,0,0,0));
  

  //Button for easily viewing the full waveform
  /* fullWaveformButton = new TGTextButton(zoomGroup, 
					" View Full Waveform ", 5001);
  fullWaveformButton->Connect("Pressed()", 
			    "CLEANViewer",
			     this, 
			     "ZoomFull()");
 zoomGroup->AddFrame(fullWaveformButton, 
			    new TGLayoutHints(kLHintsLeft));*/

  //Check button for allowing x-axis selection with cursor
  cursorSelectCheckButton=new TGCheckButton(zoomGroup,"Adjust with Cursor");
  zoomGroup->AddFrame(cursorSelectCheckButton);
  cursorSelectCheckButton->Connect("Clicked()","CLEANViewer",this,"cursorSelectChecked()");
  cursorSelectCheckButton->SetState(kButtonDown);

//Double slider for adjusting the x-axis
  zoomSlider = new TGDoubleHSlider(zoomGroup, 100, 
				   kScaleBoth, -1);
  zoomSlider->Connect("PositionChanged()", 
		      "CLEANViewer",
		      this, 
		      "XAxisChange()");
  //set possible range to the range of a full waveform
  zoomSlider->SetRange(0, 40000);
  zoomGroup->AddFrame(zoomSlider, new TGLayoutHints(kLHintsExpandX));



  //bool to turn on/off gridlines on the axises
  gridLinesX = kFALSE;
  gridLinesY=kFALSE;
  //bool to turn on/off the zoom reset when a new pmt is checked
  zoomAdjusted = kFALSE;
}

//Function for the initial setup of Group Frames
void CLEANViewer::SetGroupFrames()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::SetGroupFrames()\n");

  //Layout for only the groupVertical Frame
  groupLayout = new TGLayoutHints(kLHintsCenterY | 
				  kLHintsExpandY | 
				  kLHintsRight);


  //construct and add the groupVertical Frame
  groupVertical = new TGVerticalFrame(guiHorizontal, 450, 765, kFixedWidth);
  guiHorizontal->AddFrame(groupVertical, groupLayout);

  //event select group and widgets

  bufferGroup = new TGGroupFrame(groupVertical, "DQMBufferSize");
  bufferGroup->SetTitlePos(TGGroupFrame::kLeft);
  groupVertical->AddFrame(bufferGroup, new TGLayoutHints(kLHintsTop | 
							kLHintsCenterX |
							kLHintsExpandX,
							10,10,10,10));

  bufferSizeNumberEntry = new TGNumberEntry(bufferGroup, bufferSize, 2, 3000,
				       TGNumberFormat::kNESInteger,
				       TGNumberFormat::kNEANonNegative,
				       TGNumberFormat::kNELLimitMinMax,
				       0,20);
  bufferGroup->AddFrame(bufferSizeNumberEntry, new TGLayoutHints(kLHintsTop | 
							kLHintsCenterX |
							kLHintsExpandX,
							10,10,10,10));


  eventGroup = new TGGroupFrame(groupVertical, "Event Select");
  eventGroup->SetTitlePos(TGGroupFrame::kLeft);
  groupVertical->AddFrame(eventGroup, new TGLayoutHints(kLHintsTop | 
							kLHintsCenterX |
							kLHintsExpandX,
							10,10,10,10));

  //Construct and set initials for event select widgets
  eventHorizontal = new TGHorizontalFrame(eventGroup, 350, 50);
  eventGroup->AddFrame(eventHorizontal, new TGLayoutHints(kLHintsCenterX |
							  kLHintsTop,
							  5, 5, 5, 5));
  //number Entry widget
  eventNumberEntry = new TGNumberEntry(eventHorizontal, 0, 15, 4000,
				       TGNumberFormat::kNESInteger,
				       TGNumberFormat::kNEANonNegative,
				       TGNumberFormat::kNELLimitMinMax,
				       0,0);

  eventNumberEntry->SetState(kFALSE);
  eventNumberEntry->Connect("ValueSet(Long_t)", "CLEANViewer", 
			    this, "EventChange()");
  eventHorizontal->AddFrame(eventNumberEntry, 
			    new TGLayoutHints(kLHintsTop | kLHintsLeft));

  //larger prev and next event buttons because ed doesn't have as young of eyes as us
  prevEvent = new TGTextButton(eventHorizontal, " &Prev Event ", 4001);
  prevEvent->Connect("Pressed()", "CLEANViewer", 
		     this, "PrevEventPressed()");
  prevEvent->SetEnabled(kFALSE);
  eventHorizontal->AddFrame(prevEvent,
			    new TGLayoutHints(kLHintsTop | kLHintsCenterX));

  nextEvent = new TGTextButton(eventHorizontal, 
			       "   &Next Event >>   ", 
			       4002);
  nextEvent->Connect("Pressed()", "CLEANViewer", 
		     this, "NextEventPressed()");

  if(ZMQ)
    nextEvent->SetEnabled(kFALSE);
  eventHorizontal->AddFrame(nextEvent,
			    new TGLayoutHints(kLHintsTop | 
					      kLHintsRight));

  //ProgressBar Widget
  eventProgress = new TGHProgressBar(eventGroup, TGProgressBar::kFancy, 350);
  eventProgress->ShowPosition();
  eventProgress->SetBarColor("lightblue");
  eventProgress->SetFillType(TGProgressBar::kBlockFill);  
  eventGroup->AddFrame(eventProgress, new TGLayoutHints(kLHintsBottom |
							kLHintsLeft |
							kLHintsExpandX,
							0, 5, 5, 5));

//Button to Check All the PMTs at once
  TGHorizontalFrame* CheckAllFrame=new TGHorizontalFrame(groupVertical);
  groupVertical->AddFrame(CheckAllFrame,new TGLayoutHints(kLHintsExpandX,0,0,0,10));
  allPMTs = new TGTextButton (CheckAllFrame, " Check All PMTs ", 5004);
  allPMTs->Connect("Pressed()",
		   "CLEANViewer",
		   this,
		   "CheckAllPMTs()");
  CheckAllFrame->AddFrame(allPMTs, new TGLayoutHints(kLHintsCenterX));

  //Button to uncheck all the PMTs at once
  noPMTs = new TGTextButton (CheckAllFrame, " Uncheck All PMTs ", 5005);
  noPMTs->Connect("Pressed()",
		   "CLEANViewer",
		   this,
		   "UncheckAllPMTs()");
  CheckAllFrame->AddFrame(noPMTs, new TGLayoutHints(kLHintsCenterX));

  summedWaveformButton=new TGCheckButton(CheckAllFrame,"Draw Summed Waveform");
  CheckAllFrame->AddFrame(summedWaveformButton, new TGLayoutHints(kLHintsCenterX));
  
  summedWaveformButton->Connect("Clicked()","CLEANViewer",this,"PlotChannel()");
 
  //pmt select group and widgets
  selectionTab=new TGTab(groupVertical,350,800);
  //groupVertical->AddFrame(selectTab,tabLayout);
  tabBoard=selectionTab->AddTab("Channel Select");
  tabPMT=selectionTab->AddTab("PMT Select");
  //tabDisplay=selectionTab->AddTab("Display Select");
  //pmtSelect=new TGGroupFrame(groupVertical,"PMT Select");
  pmtGroupCanvas = new TGCanvas(tabBoard,1,1,kNone);
 
  //pmtContainer=new TGContainer(pmtGroupCanvas->GetViewPort());
  pmtGroup=new TGCompositeFrame(pmtGroupCanvas->GetViewPort());
  
  pmtGroupCanvas->SetContainer(pmtGroup);
  //pmtContainer->AddFrame(pmtGroup);


  pmtGroupCanvas->SetScrolling(2);
 

  pmtGroupCanvas->GetViewPort()->AddFrame(pmtGroup,new TGLayoutHints(kLHintsCenterX |
						      kLHintsExpandX |
						      kLHintsExpandY,
						      0,0,0,0));
  //pmtContainer->DrawRegion(.5,.5,1,1);
  groupVertical->AddFrame(selectionTab, new TGLayoutHints(kLHintsCenterX |kLHintsCenterY|
						      kLHintsExpandX |
						      kLHintsExpandY,
						      5,5,0,0));
  tabBoard->AddFrame(pmtGroupCanvas, new TGLayoutHints(kLHintsCenterX |kLHintsCenterY|
						      kLHintsExpandX |
						      kLHintsExpandY,
						      5,5,0,0));


  pmtOrderGroupCanvas = new TGCanvas(tabPMT,1,1,kNone);
 

  pmtOrderGroup=new TGCompositeFrame(pmtOrderGroupCanvas->GetViewPort());
  pmtOrderGroupCanvas->SetContainer(pmtOrderGroup);

 
  pmtOrderGroupCanvas->SetScrolling(2);
 

  pmtOrderGroupCanvas->GetViewPort()->AddFrame(pmtOrderGroup,new TGLayoutHints(kLHintsCenterX |
						      kLHintsExpandX |
						      kLHintsExpandY,
						      0,0,0,0));
  tabPMT->AddFrame(pmtOrderGroupCanvas, new TGLayoutHints(kLHintsCenterX |kLHintsCenterY|
						      kLHintsExpandX |
						      kLHintsExpandY,
						      5,5,0,0));


  /*displayTabECanvas=new TRootEmbeddedCanvas("displayTabECanvas",tabDisplay,400,400);

  tabDisplay->AddFrame(displayTabECanvas);
 
  displayTabTCanvas = displayTabECanvas->GetCanvas();
  displayTabTCanvas->SetEditable(kFALSE);
  */
 

  }
/*
void CLEANViewer::MakeInfoWindow()
{

  TGMainFrame * winInfo=new TGMainFrame(gClient->GetRoot(),350,700);
  winInfo->SetWindowName("Event Info");
  TGCompositeFrame * 




}
*/
