<?php

if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_POST);
}

$ip_address = $_POST['ipAddress'];
$ping_string = "ping -c 1 " . $ip_address . " 2>&1 | awk 'START{alive=0}{if($4==\"1\"){alive=1}}END{print alive+0}'";

$ping_test = exec($ping_string);

echo $ping_test;
?>