#include <PacketBuilder.h>

bool PacketBuilder::CheckBoardSize_CHAN_ZLE_WAVEFORM(RATEVBlock * event,RAT::DS::Board * board)
{
  //Calculate the size for this RAT::DS::Board in EBPacket structures
  uint32_t boardSize = sizeof(EBPacket::Board);
  int channelCount = board->GetChannelCount();
  for(int iChannel = 0; iChannel < channelCount;iChannel++)
    {
      boardSize += sizeof(EBPacket::Channel);      
      RAT::DS::Channel * channel = board->GetChannel(iChannel);
      int rawBlockCount = channel->GetRawBlockCount();
      for(int iRawBlock = 0; iRawBlock < rawBlockCount; iRawBlock++)
	{
	  boardSize += sizeof(EBPacket::WaveformBlock);
	  RAT::DS::RawBlock * rawBlock = channel->GetRawBlock(iRawBlock);
	  boardSize += (rawBlock->GetSamples().size() * sizeof(uint16_t));
	}
    }

  //See if we have room
  if(boardSize > dataSizeLeft || packet==NULL)
    {
      //fprintf(stderr,"Board size is greater than data size left\n");
      //fprintf(stderr,"Board size: %u; data size left: %u\n",boardSize,dataSizeLeft);
      return CheckPacket(boardSize,event);
      //return true;
    }
  return true;
}

void PacketBuilder::AddChannel_CHAN_ZLE_WAVEFORM(RAT::DS::Channel * channel, int16_t /*PMTID*/)
{
  //fprintf(stderr,"Adding channel\n");
  //Add channel
  EBPacket::Channel * ebChannel = (EBPacket::Channel *) currentPtr;
  currentPtr += sizeof(EBPacket::Channel);
  dataSizeLeft -= sizeof(EBPacket::Channel);
  ebChannel->dataBlockCount = channel->GetRawBlockCount();
  for(int iRawBlock = 0; iRawBlock < ebChannel->dataBlockCount;iRawBlock++)
    {
        //Add channel
      EBPacket::WaveformBlock * ebWaveform = (EBPacket::WaveformBlock *) currentPtr;
      currentPtr += sizeof(EBPacket::WaveformBlock);
      dataSizeLeft -= sizeof(EBPacket::WaveformBlock);
      RAT::DS::RawBlock * rawBlock = channel->GetRawBlock(iRawBlock);
      ebWaveform->startTime = rawBlock->GetOffset();
      ebWaveform->dataBytes = rawBlock->GetSamples().size()*sizeof(uint16_t);
      memcpy(currentPtr,&rawBlock->GetSamples()[0],ebWaveform->dataBytes);
      currentPtr   += ebWaveform->dataBytes;
      dataSizeLeft -= ebWaveform->dataBytes;
    }
}
