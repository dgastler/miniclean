#ifndef __EVENTTIMEGROUP__
#define __EVENTTIMEGROUP__

#include <EventBuilder.h>
#include <cstdlib>

template <class T>
class EventTimeGroup : public EventBuilder<T>
{
public:
  EventTimeGroup()
  {
    AssociationTime = 200;  //Get a better value for this!
    fprintf(stderr,"EventTimeGroup: AssociationTime is %"PRId64", set it properly\n",AssociationTime);
    next_empty_index = 0;
    oldest_valid_index = 0;
    fill_size = 0;
  };

  inline bool EquivalentKeys(int64_t key1, int64_t key2)
  {
    //Since the WFD time is 31 bits, drop down to 32bit ints
    //    const int32_t mask = 0x1FFFFFF;
    const int32_t mask = 0x7FFFFFFF;
    int32_t shortKey1 = mask&key1;
    int32_t shortKey2 = mask&key2;
    return ( (int32_t(abs(shortKey1-shortKey2))&mask) < AssociationTime ) ;
  }

protected:
  
  virtual size_t GetKeyIndex(int64_t key)
  { 
    size_t current_index;
    //Clean up any empty entries
    GarbageCollect();
    //search previous entries
    if(fill_size > 0)
      {
	//Start at the most current event (+1)
	//and move backwards in time
	current_index = next_empty_index;
	do
	  {
	    MoveIndexBackward(current_index);
	    //grab they key for this index if valid
	    if((*(this->GetEntry()))[current_index].IsValid())
	      {
		int64_t current_index_key = (*(this->GetEntry()))[current_index].key;
		//if they key works, return the index
		if(EquivalentKeys(current_index_key,key))
		  {
		    return current_index;	    
		  }
	      }
	  }while(current_index != oldest_valid_index);
      }
  
    //We didn't find an existing index
    //We should then return the next free
    //The next free is not necessarily "free" 
    current_index = next_empty_index;
    MoveIndexForward(next_empty_index);
    if(fill_size < this->GetEntry()->size())
      //there are open entries
      {
	fill_size++;
      }
    else
      //no more entries, we return the oldest to be a collision
      {
	MoveIndexForward(oldest_valid_index);
      }
    return current_index;
  }
private:
  
  //Mapping into the Entry container
  size_t fill_size;
  size_t next_empty_index;
  size_t oldest_valid_index;
  
  //Assembly variables
  int64_t AssociationTime;
  
  //Helper inlines
  inline void MoveIndexForward(size_t & index)
  {
    index++;
    //Wrap around fix
    if(index >= this->GetEntry()->size())
      {
	size_t entrySize = this->GetEntry()->size();
	//	index = entrySize - ((0xFFFFFFFFFFFFFFFF - index + 1)%entrySize);
	index %= entrySize;
      }
  }
  inline void MoveIndexBackward(size_t & index)
  {
    index--;    
    //Wrap around fix
    if(index >= this->GetEntry()->size())
      {
	size_t entrySize = this->GetEntry()->size();
	index = entrySize - ((0xFFFFFFFFFFFFFFFFULL - index + 1)%entrySize);
      }
     
  }
  

  void GarbageCollect()
  {
    //Escape if there are no entries
    if(fill_size ==0)
      { 
	return;
      }
    //Remove old entries that are invalid
    do
      {
	if((*(this->GetEntry()))[oldest_valid_index].IsValid())
	  {
	    //Oldest entry is still valid, move on to search
	    break;
	  }
	else	      
	  {
	    //Oldest value can be dropped
	    //remove this entry
	    MoveIndexForward(oldest_valid_index);
	    fill_size--;
	  }
      }while(oldest_valid_index != next_empty_index);
  }
  
  
  //Don't impliment these because they should never be called
  EventTimeGroup(const DCVectorEntry<T> & rhs);
  EventTimeGroup & operator=(const EventTimeGroup & rhs);
};
#endif
