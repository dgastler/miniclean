#include <iostream>
#include "PEvent.h"
#include "Event.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

using namespace std;

Int_t function;
void signal_handler(int num)
{
  cerr << function << endl;
  kill(getpid(),SIGABRT);
}

Float_t Integrate(Float_t * Y,Int_t Start, Int_t End,Float_t Sigma)
{
  //1;
  Float_t Baseline = 0;
  Float_t Integral = 0;
  for(int i = Start; i < End;i++)
    {
      Bool_t ChangeBaseline = true;
      
      for(int j = i;j < i + 5 && j < End;j++)
	{
	  if(Y[j] - Baseline < -2*Sigma)
	    ChangeBaseline = ChangeBaseline&true;
	  else
	    ChangeBaseline = ChangeBaseline&false;
	}
      if(ChangeBaseline)
	Baseline -= Sigma;

      Integral+=Y[i] - Baseline;
    }
  return(Integral);
}

Float_t Integrate2(Float_t * Y,Int_t Start, Int_t End,Float_t Sigma)
{
  //1;
//  Float_t Baseline = 0;
  Float_t Integral = 0;
//  for(int i = Start; i < End;i++)
//    {
//      Bool_t ChangeBaseline = true;
//      
//      for(int j = i;j < i + 5 && j < End;j++)
//	{
//	  if(Y[j] - Baseline < -2*Sigma)
//	    ChangeBaseline = ChangeBaseline&true;
//	  else
//	    ChangeBaseline = ChangeBaseline&false;
//	}
//      if(ChangeBaseline)
//	Baseline -= Sigma;
//
//      //      Integral+=Y[i] - Baseline;
//      if(Y[i] > Baseline + 5*Sigma)
//	{
//	  Integral+=Y[i] - Baseline;
//	}
//    }
  for(int i = Start; i < End; i++)
    {
      Integral += Y[i];
    }
  return(Integral);
}



Int_t Find_Waveform_Maximum(Int_t ChannelSize, Float_t * Y)
{
  //2;
  Float_t MaxY = 0; //3;
  Int_t   MaxYlocation = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Y[i] > MaxY)
	{
	  MaxY = Y[i];
	  MaxYlocation = i;
	}
    }
  return(MaxYlocation);
}

Int_t Find_Waveform_Start(Int_t ChannelSize, Float_t * Y)
{
  //3;
  Float_t MaxY = 0;//Y[0];
  Int_t   MaxYlocation = 1;
  for(Int_t i = 1; i < ChannelSize;i++)
    {
      if(Y[i] > MaxY)
	{
	  MaxY = Y[i];
	  MaxYlocation = i;
	}
    }
  for(Int_t i = 0; MaxYlocation + i > 0;i--)
    {
      if(5.0*Y[MaxYlocation + i] < MaxY)
	return(MaxYlocation + i);
    }
  return(0);
}


Float_t MergeWaveforms(Int_t ChannelSize, Float_t * Ya,Float_t *Yb,Float_t Y_Offset)
{
  //4;
  Float_t start = 0;
  Float_t end = 0;
  Float_t deltaT = 0;
  Float_t trip = 200-Y_Offset;
  //  for(int i = 0; i < ChannelSize;i++)
  //Find position of channel a hitting 200-Y_Offset on the way up and down.
  for(int i = 1; i < ChannelSize;i++)
    {
      if((Ya[i] > trip)&&(start == 0))
	{
	  start = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) + i - 1;
	}
      if((Ya[i] < trip)&&(start !=0))
	{
	  end = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) +i - 1;
	  i = ChannelSize;
	}
      
    }
  
  deltaT = end - start;
  
  //5;
  //Find B channel MAXes
  Int_t ChannelB_MAX = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Yb[i] >= Yb[ChannelB_MAX])
	{
	  ChannelB_MAX = i;
	}
      if((i == 2*ChannelB_MAX)&&(Yb[ChannelB_MAX] > 5))
	break;
    }
  
  Int_t Tf = ChannelB_MAX;
  Int_t Ti = Tf-1;
  
  //6;
  while( Tf - Ti < deltaT )
    {
      if(((Tf >= 0) && (Tf < ChannelSize)) && ((Ti >= 0) && (Ti < ChannelSize)))
      //      if ( Yb[Tf] > Yb[Ti] )
	{
	  if(Yb[Tf] > Yb[Ti])
	    {
	      Tf++;
	    }
	  else
	    {
	      Ti--;
	    }
	}
      else
	{
	  break;
	  Ti = 0xefffffff;
	  Tf = 0xefffffff;
	}
    }

	
  Float_t shift;
  shift = (start - Ti + end - Tf)/2.0;
  
  Float_t Gain = 10;
  //7;
  for(int i = 0;i < ChannelSize;i++)
    {
      //      if((i + shift) > 0 && (i + shift) < ChannelSize)
      if((i - (int)shift) > 0 && (i - (int)shift) < ChannelSize)
	{
	  if(Ya[i] >= 255 - ceil(Y_Offset) && Gain*Yb[i-(int)shift] > Ya[i])
	    {
	      Ya[i] = Gain * Yb[i-(int)shift];
	    }
	}
      
    }
  
  return(shift);
}





int main(int argc, char **argv)
{
  //  signal(SIGSEGV,signal_handler);
  if(argc < 7)
    {
      cout << argv[0] << " Weight1 Weight2 OutFile InFile_base Ni Nf" << endl;
      return(0);
    }

  //  TFile   *    InFile = new TFile();
  TFile   *    InFile = 0;
  TTree   *    tree = 0;
  Event   *    event = 0;
  Char_t  *    filename = new Char_t[200];
  Char_t  *    filebase = argv[4];
  Int_t        ni = atoi(argv[5]);
  Int_t        nf = atoi(argv[6]);



  TFile   *    OutFile = new TFile(argv[3],"recreate");
  OutFile->SetCompressionLevel(9);
  PEvent  *    pevent = new PEvent;
  TTree   *    ptree = new TTree("ptree","Run");
  ptree->SetAutoSave(100000);
  ptree->Branch("PEvent branch","PEvent",&pevent,64000,0);
 
  Int_t        Size  = 3;
  Float_t *    Array1 = new Float_t[Size];
  Float_t *    Array2 = new Float_t[Size];
  Float_t *    Array3 = new Float_t[Size];
  Float_t *    Array4 = new Float_t[Size];
  Float_t *    Array5 = new Float_t[Size];
  Float_t *    Array6 = new Float_t[Size];
  Float_t *    Array7 = new Float_t[Size];

  Float_t      W1 = atof(argv[1]);
  Float_t      W2 = atof(argv[2]);
  Int_t        TotalEvents = 0;

  Int_t TotalEventNumber = 0;

  for(int filenumber = ni;filenumber <= nf; filenumber++)
    {
      sprintf(filename,"%s%i.root",filebase,filenumber);
      cout << "Opening file: " << filename << endl;
      
      
      //      InFile->Open(filename);
      InFile                 = new TFile(filename,"READ","",9);
      //      InFile->Map();
      tree                   = (TTree*) InFile->Get("tree");
      event                  = 0;
      Int_t NEvents          = tree->GetEntries();
      //tree->Print();
      tree->SetBranchAddress("Event branch",&event);
      
      for(int EventN = 0; EventN < NEvents; EventN++)
	{
	  pevent->Build(Array1,Array2,Array3,Array4,Array5,Array6,Array7,Size);
	  pevent->SPE(0,W1);
	  pevent->SPE(1,W2);
	  pevent->SPE(2,1);
	  pevent->EventNumber(TotalEventNumber);
	  TotalEventNumber++;

	  tree->GetEntry(EventN);

	  bool Process_Event                   = true;
	  bool FixGain[2]                      = {false,false};
	  Int_t ChannelSize                    = event->GetSize(0);
	  
	  Float_t Channel1_Offset              = 0;
	  Float_t Channel1b_Offset             = 0;
	  Float_t Channel2_Offset              = 0;
	  Float_t Channel2b_Offset             = 0;
	  Float_t Channel3_Offset              = 0;
	  
	  Float_t Channel1_OffsetSquared       = 0;
	  Float_t Channel1b_OffsetSquared      = 0;
	  Float_t Channel2_OffsetSquared       = 0;
	  Float_t Channel2b_OffsetSquared      = 0;
	  Float_t Channel3_OffsetSquared       = 0;
	  
	  Float_t Sigma_Channel1_Offset        = 0;
	  Float_t Sigma_Channel1b_Offset       = 0;
	  Float_t Sigma_Channel2_Offset        = 0;
	  Float_t Sigma_Channel2b_Offset       = 0;
	  Float_t Sigma_Channel3_Offset        = 0;
	  
	  for(int i = 0;i < 4;i++)
	    {
	      if(event->GetSize(i) != ChannelSize)
		{
		  cout << "*ERROR*  Channel Sizes different" << endl;
		  OutFile->Write();		  
		  OutFile->Close();
		  return(0);
		}
	    }
	  
	  Float_t * Channel1                   = new Float_t[ChannelSize];
	  Float_t * Channel1b                  = new Float_t[ChannelSize];
	  Float_t * Channel2                   = new Float_t[ChannelSize];
	  Float_t * Channel2b                  = new Float_t[ChannelSize];
	  Float_t * Channel3                   = 0;
	  bool Chan3 = false;
	  if(event->NRaw() > 4)
	    {
	      Chan3 = true;
	      Channel3 = new Float_t[ChannelSize];
	    }
	  
	  
	  //Read in Waveform and check if it needs to be merged with the second
	  // gain scale.
	  Float_t threshold                    = 100;
	  bool Abovethrs                       = false;
	  //	  Process_Event = true;
	  for(int i = 0; i < ChannelSize;i++)
	    {
	      Channel1[i] = event->GetPoint(0,i);
	      Channel1b[i] = event->GetPoint(1,i);
	      Channel2[i] = event->GetPoint(2,i);
	      Channel2b[i] = event->GetPoint(3,i);
	      
	      if(Chan3)
		{
		  Channel3[i] = event->GetPoint(4,i);
		}
	      
	      if(!Abovethrs)
		{
		  if(i < 5000)
		    {
		      if(Channel1[i] > threshold || Channel2[i] > threshold)
			Abovethrs = true;
		    }
		}
	      
	      if(Channel1[i] >= 254)  //==255)
		{
		  FixGain[0] = true;
		}
	      if(Channel2[i] >= 254)
		{
		  FixGain[1] = true;
		}
	      if(i < 1000)
		{
		  Channel1_Offset  += Channel1[i];
		  Channel1b_Offset += Channel1b[i];
		  Channel2_Offset  += Channel2[i];
		  Channel2b_Offset += Channel2b[i];
		  if(Chan3)
		    Channel3_Offset  += Channel3[i];
				  
		  
		  Channel1_OffsetSquared  += Channel1[i]*Channel1[i];
		  Channel1b_OffsetSquared += Channel1b[i]*Channel1[i];
		  Channel2_OffsetSquared  += Channel2[i]*Channel2[i];
		  Channel2b_OffsetSquared += Channel2b[i]*Channel1[i];
		  if(Chan3)
		    Channel3_OffsetSquared  += Channel3[i]*Channel3[i];
		}
	    }



		  

	  if(!Abovethrs)
	    {
	      Process_Event = false;
	      //    OUTFILE << "Run:"<< filenumber << " Event:" << EventN
	      //      << " Not above threshold" << endl;
	    }
	  Sigma_Channel1_Offset    = sqrt((Channel1_OffsetSquared - Channel1_Offset*Channel1_Offset/1000)/(1000-1));
	  Sigma_Channel1b_Offset   = sqrt((Channel1b_OffsetSquared - Channel1b_Offset*Channel1b_Offset/1000)/(1000-1));
	  Sigma_Channel2_Offset    = sqrt((Channel2_OffsetSquared - Channel2_Offset*Channel2_Offset/1000)/(1000-1));
	  Sigma_Channel2b_Offset   = sqrt((Channel2b_OffsetSquared - Channel2b_Offset*Channel2b_Offset/1000)/(1000-1));
	  if(Chan3)
	    Sigma_Channel3_Offset    = sqrt((Channel3_OffsetSquared - Channel3_Offset*Channel3_Offset/1000)/(1000-1));
		  
	  
	  
	  
	  
	  Channel1_Offset  /= 1000.0;
	  Channel1b_Offset /= 1000.0;
	  Channel2_Offset  /= 1000.0;
	  Channel2b_Offset /= 1000.0;
	  if(Chan3)
	    Channel3_Offset  /= 1000.0;
	  
	  for(int i = 0; i < ChannelSize; i++)
	    {
	      Channel1[i]  -= Channel1_Offset;
	      Channel1b[i] -= Channel1b_Offset;
	      Channel2[i]  -= Channel2_Offset;
	      Channel2b[i] -= Channel2b_Offset;
	      if(Chan3)
		Channel3[i]  -= Channel3_Offset;
	    }

	  if(FixGain[1])
	    {
	      Float_t Shift = MergeWaveforms(ChannelSize,Channel1,Channel1b,Channel1_Offset);
	      pevent->Shift(1,Shift);
	    }
	  else
	    pevent->Shift(1,-1);
	  if(FixGain[0])
	    {
	      Float_t Shift = MergeWaveforms(ChannelSize,Channel2,Channel2b,Channel2_Offset);
	      pevent->Shift(0,Shift);
	      //	      cout << pevent->Shift(0) << endl;
	    }
	  else 
	    pevent->Shift(0,-1);
	  
	  if(Chan3)
	    {
	      //Delay Time   TOF!
	      Float_t Delay_PMT1 = (2e-6)*(Find_Waveform_Start(ChannelSize,Channel1) - Find_Waveform_Start(ChannelSize,Channel3));
	      Float_t Delay_PMT2 = (2e-6)*(Find_Waveform_Start(ChannelSize,Channel2) - Find_Waveform_Start(ChannelSize,Channel3));
	      
	      //	      Float_t Delay_Time = (Delay_PMT1 + Delay_PMT2)/2;
	      pevent->TOF(0,Delay_PMT1);
	      pevent->TOF(1,Delay_PMT2);
	      //pevent->TOFAve = Delay_Time;
	      //cout << Delay_Time << "\t" ;
	    }
	  else
	    {
	      pevent->TOF(0,0);
	      pevent->TOF(1,0);
	      //pevent->TOFAve = 0;
	    }
	  
	  Int_t Peak[3];
	  Peak[0] = Find_Waveform_Start(ChannelSize,Channel1);
	  Peak[1] = Find_Waveform_Start(ChannelSize,Channel2);
	  if(Chan3)
	    {
	      Peak[2] = Find_Waveform_Start(ChannelSize,Channel3);
	    }
	  pevent->Delay(0,Peak[0]);
	  pevent->Delay(1,Peak[1]);
	  pevent->MaxHeight(0,Channel1[Find_Waveform_Maximum(ChannelSize,Channel1)]);
	  pevent->MaxHeight(1,Channel2[Find_Waveform_Maximum(ChannelSize,Channel2)]);
	  if(Chan3)
	    {
	      pevent->MaxHeight(2,Channel3[Find_Waveform_Maximum(ChannelSize,Channel3)]);
	      pevent->Delay(2,Peak[2]);
	    }
	  //      cout << Peak[0] << "\t" << Peak[1] << endl;
	  Int_t Before = -10; //Number of points before the peak to integrate
	  Int_t After = 50; //Number of Points after the peak to integrate
	  
	  Float_t FastPMT0 = Integrate(Channel1,Peak[0] + Before,Peak[0] + After,Sigma_Channel1_Offset);
	  Float_t SlowPMT0 = Integrate(Channel1,Peak[0] + After,ChannelSize-500,Sigma_Channel1_Offset);
	  //	  cout << 2000 << endl;
	  Float_t FastPMT1 = Integrate(Channel2,Peak[1] + Before,Peak[1] + After,Sigma_Channel2_Offset);
	  Float_t SlowPMT1 = Integrate(Channel2,Peak[1] + After,ChannelSize-500,Sigma_Channel2_Offset);
	  //	  cout << 2000 << endl;
	  Float_t FastPMT2 = 0;
	  Float_t SlowPMT2 = 0;
	  if(Chan3)
	    {
	      FastPMT2 = Integrate(Channel3,Peak[2] + Before,Peak[2] + After,Sigma_Channel3_Offset);
	      SlowPMT2 = Integrate(Channel3,Peak[2] + After,ChannelSize-500,Sigma_Channel3_Offset);
	    }
	  pevent->AboveTHRS(Abovethrs);
	  pevent->fFast(0,FastPMT0/W1);
	  pevent->fFast(1,FastPMT1/W2);
	  pevent->fSlow(0,SlowPMT0/W1);
	  pevent->fSlow(1,SlowPMT1/W2);
	  if(Chan3)
	    {
	      pevent->fFast(2,FastPMT2);
	      pevent->fSlow(2,SlowPMT2);
	    }
	  
	  //	  pevent->TOFAve = 5;
	  delete [] Channel1;
	  delete [] Channel1b;
	  delete [] Channel2 ;
	  delete [] Channel2b;
	  if(Chan3)
	    delete [] Channel3 ;

	  ptree->Fill();
	  pevent->Clear("C");
	  TotalEvents++;
	}
      cerr << "Closing file : " << filename << endl;
      InFile->Close("R");
      delete InFile;
    }
  //  ptree->Print();
  OutFile->Write();

  OutFile->Close();
      
  return(0);
}
