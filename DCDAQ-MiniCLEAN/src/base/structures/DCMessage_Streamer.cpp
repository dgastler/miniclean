#include <DCMessage.h>

int DCMessage::Message::StreamMessage(std::vector<uint8_t> &stream)
{     
  uint32_t size;
  //Empty stream. 
  stream.clear();
  //=========Add a total size marker=========
  Add(&size,sizeof(uint32_t),stream);
  //=========Add in the type=========
  Add(&type,sizeof(type),stream);
  //=========Add in DCtime_t (assume 64bits)=========
  Add(&timeStamp,sizeof(DCtime_t),stream);
      
  //=========Add in source terms=========
  std::vector<uint8_t> addrData;  
  size = Source.MakeStream(addrData);
  Add(&size,sizeof(uint32_t),stream);
  Add(&(addrData[0]),size,stream);
      
  //=========Add in destination terms=========
  addrData.clear();
  size = Destination.MakeStream(addrData);
  Add(&size,sizeof(uint32_t),stream);
  Add(&(addrData[0]),size,stream);
      
  //=========Add data (32bits of size, then value)=========
  size = Data.size();
  Add(&size,sizeof(uint32_t),stream);
  Add(&(Data[0]),size,stream);

  //=========Update a total size marker=========
  size = stream.size();
  memcpy(&(stream[0]),&size,sizeof(size));

 
  return(stream.size());
}
    

void DCMessage::Message::SetMessage(uint8_t * streamSource,unsigned int size)
{
  uint8_t * ptr = streamSource;
  uint32_t pos = 0;
  uint32_t TotalSize = 0;
  unsigned int subSize;
  uint32_t vectorSize;
  //Get total size.
  if(size > sizeof(uint32_t))
    {
      memcpy(&TotalSize,streamSource,sizeof(uint32_t));
      ptr+=sizeof(uint32_t);
      pos+=sizeof(uint32_t);
    }
  else
    {
      Clear();
      return;
    }
  //Size is incorrect.
  if(TotalSize != size)
    {
      Clear();
      return;
    }



  //=========Parse the type=========
  subSize = sizeof(type);
  if(pos + subSize < TotalSize)
    {

      memcpy(&type,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      Clear();
      return;
    }


  //=========Parse the uint64_t timestamp=========
  subSize = sizeof(DCtime_t);
  if(pos + subSize < TotalSize)
    {

      memcpy(&timeStamp,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      Clear();
      return;
    }

  //=========Parse the Source=========
  subSize = sizeof(uint32_t);	
  if(pos + subSize < TotalSize)
    {
      vectorSize=0;
      memcpy(&vectorSize,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      Clear();
      return;
    }
  if(pos + vectorSize < TotalSize)
    {
      Source.LoadStream(ptr,vectorSize);
      ptr+= vectorSize;
      pos+= vectorSize;
    }
  else
    {
      Clear();
      return;
    }

  //=========Parse the Destination=========
  subSize = sizeof(uint32_t);	
  if(pos + subSize < TotalSize)
    {
      vectorSize=0;
      memcpy(&vectorSize,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      Clear();
      return;
    }
  if(pos + vectorSize < TotalSize)
    {
      Destination.LoadStream(ptr,vectorSize);
      ptr+= vectorSize;
      pos+= vectorSize;
    }
  else
    {
      Clear();
      return;
    }

  //=========Parse the Data=========
  subSize = sizeof(uint32_t);	
  if(pos + subSize <= TotalSize)
    {
      vectorSize=0;
      memcpy(&vectorSize,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      Clear();
      return;
    }
  if(pos + vectorSize <= TotalSize)
    {
      Data.resize(vectorSize);
      memcpy(&(Data[0]),ptr,vectorSize);
      ptr+= vectorSize;
      pos+= vectorSize;
    }
  else
    {
      Clear();
      return;
    }

}
