#ifndef __MESSAGESTRUCTURES__
#define __MESSAGESTRUCTURES__

#include <DCAddress.h>
#include <RAT/DS/DCDAQ_DCTime.hh>

#include <TObject.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TBufferFile.h>


namespace DCMessage
{
  //===================================
  //Useful struct types for parsing message data
  //===================================

  enum 
  {
    TIMEDCOUNT,
    SINGLEUINT64,
    SINGLEINT64,
    DCROUTE,
    DCTYPEID,
    TIMEDDOUBLE,
    SINGLEDOUBLE,
    DCSTRING,
    DCTOBJECT
  };

  const size_t textSize = 1000;

  //TimedCount
  struct TimedCount
  {
    uint8_t type;
    char text[textSize];
    int32_t ID;
    uint32_t count;
    float dt;    
    TimedCount(){type=TIMEDCOUNT;};
  };
  
 //TimedDouble
  struct TimedDouble
  {
    uint8_t type;
    char text[textSize];
    int32_t ID;
    double value;
    float dt;    
    TimedDouble(){type=TIMEDDOUBLE;};
  };

  //single uint64_t
  struct SingleUINT64
  {
    uint8_t type;
    char text[textSize];
    uint64_t i;
    SingleUINT64(){type=SINGLEUINT64;};
  };
  
  //single int64_t
  struct SingleINT64
  {
    uint8_t type;
    char text[textSize];
    int64_t i;
    SingleINT64(){type=SINGLEINT64;};
  };

  //single double
  struct SingleDouble
  {
    uint8_t type;
    char text[textSize];
    double value;
    SingleDouble(){type=SINGLEDOUBLE;};
  };

  struct DCString
  {
    uint8_t type;
    int32_t stringOffset;
    DCString()
    {
      type=DCSTRING;
      stringOffset = -1;
    };
    std::vector<uint8_t> SetData(const std::string &text)
    {
      //Create a vector to store the data to pass to DCMessage
      std::vector<uint8_t> data;
      //Copy this structure into it. 
      for(size_t i = 0; i < sizeof(DCString);i++)
	data.push_back( ((uint8_t*)(this))[i]);
      //Set string Offset
      stringOffset = ((DCString *) (&data[0]))->stringOffset = (int32_t) data.size();
      //Copy string
      for(size_t i = 0; i < text.size();i++)
	data.push_back(text[i]);
      return data;
    };
    bool GetData(const std::vector<uint8_t> &data,
		 std::string &text)
    {
      //Clear 
      text.clear();
      //Check for size
      if(data.size() >= sizeof(DCString))
	{
	  //Copy the header info
	  for(size_t i = 0; i < sizeof(DCString);i++)
	    {
	      ((uint8_t *) (this))[i] = data[i];
	    }      
	  //Check the size can work
	  if((stringOffset >= 0) && 
	     (int32_t(data.size()) >= stringOffset))
	    {
	      //Copy text
	      text.resize(data.size() - stringOffset);
	      for(size_t i = stringOffset; i < data.size();i++)
		text[i - stringOffset] = data[i];
	    }
	  else
	    {
	      Clear();
	      return false;
	    }
	}
      else
	{
	  Clear();
	  return false;
	}
      return true;
    };
    void Clear()
    {
      stringOffset = -1;
    };
  };

  //DCDAQ route data
  struct DCRoute
  {
    int8_t type;
    //Address information
    int32_t AddrOffset;
    //Thread name info
    int32_t ThreadOffset;
    DCRoute()
    {
      type=DCROUTE;
      AddrOffset=-1;
      ThreadOffset=-1;
    };
    std::vector<uint8_t> SetData(DCMessage::DCAddress const & addr, std::string Thread)
    {
      //Create a vector to store the data to pass to DCMessage
      std::vector<uint8_t> data;
      //Copy this structure into it. 
      for(size_t i = 0; i < sizeof(DCRoute);i++)
	data.push_back( ((uint8_t*)(this))[i]);
      
      //Set Addr Offset
      AddrOffset = ((DCRoute *) (&data[0]))->AddrOffset = (int32_t) data.size();
      //Copy Addr
      std::vector<uint8_t> stream;
      addr.MakeStream(stream);
      for(size_t i = 0; i < stream.size();i++)
	data.push_back(stream[i]);
            
      //Set Thread name Offset
      ThreadOffset = ((DCRoute *) (&data[0]))->ThreadOffset = (int32_t) data.size();
      //Copy Addr
      for(size_t i = 0; i < Thread.size();i++)
	data.push_back(Thread[i]);
      
      return data;
    };
    bool GetData(const std::vector<uint8_t> &data,
		 DCMessage::DCAddress & addr,
		 std::string &Thread)
    {
      //Clear 
      Thread.clear();
      //Check for size
      if(data.size() >= sizeof(DCRoute))
	{
	  //Copy the header info
	  for(size_t i = 0; i < sizeof(DCRoute);i++)
	    {
	      ((uint8_t *) (this))[i] = data[i];
	    }

	  //Check the size can work
	  if((AddrOffset >= 0) &&
	     (ThreadOffset >= AddrOffset) &&
	     (int32_t(data.size()) >= ThreadOffset))
	    {
	      //Copy Addr
	      addr.LoadStream((uint8_t*) &data[AddrOffset],(ThreadOffset-AddrOffset));

	      //Copy Thread name
	      Thread.resize(data.size() - ThreadOffset);
	      for(size_t i = ThreadOffset;i < data.size();i++)
		{
		  Thread[i - ThreadOffset] = data[i];
		}
	    }
	  else
	    {
	      Clear();
	      return(false);
	    }
	}
      else
	{
	  Clear();
	  return(false);
	}
      return(true);
    };
    void Clear()
    {
      AddrOffset=-1;
      ThreadOffset=-1;
    };
  };
  

  struct DCTObject
  {
    uint8_t type;
    enum RootType			       
      {
	OBJECT,
	GRAPH,
	H1F,
	H2F,
	CANVAS,
	COUNT
      };
    RootType rootType;    
    int32_t objectOffset;
    DCTObject()
    {
      type=DCTOBJECT;
      objectOffset = -1;
      rootType = COUNT;
    };
    std::vector<uint8_t> SetData(TObject * obj,RootType _rootType)
    {
      //Create a vector to store the data to pass to DCMessage
      std::vector<uint8_t> data;
      //Check that obj is valid
      if(obj != NULL)
	{
	  //Copy this structure into it. 
	  for(size_t i = 0; i < sizeof(DCTObject);i++)
	    data.push_back( ((uint8_t*)(this))[i]);
	  rootType = ((DCTObject *) (&(data[0])))->rootType = _rootType;
	  //Set object Offset
	  objectOffset = ((DCTObject *) (&(data[0])))->objectOffset = (int32_t) data.size();
	  //Use roots built in streamer to build a char* array of this object
	  TBufferFile stream(TBuffer::kWrite);
	  stream.SetWriteMode();
	  stream.MapObject(obj);
	  obj->Streamer(stream);
	  data.resize(data.size() + stream.BufferSize());
	  //Copy data from the stream
	  memcpy(&(data[0]) + objectOffset,
		 stream.Buffer(),
		 stream.BufferSize());
	}
      else
	{
	  Clear();
	}
      return data;
    };
    bool GetData(const std::vector<uint8_t> &data,
		 TObject * &obj)
    {
      //Check for size
      if(data.size() >= sizeof(DCTObject))
	{
	  //Copy the header info
	  for(size_t i = 0; i < sizeof(DCTObject);i++)
	    {
	      ((uint8_t *) (this))[i] = data[i];
	    } 
	  //create the new object
	  switch (rootType)
	    {
	    case OBJECT:
	      obj = new TObject;
	      break;
	    case GRAPH:
	      obj = new TGraph;
	      break;
	    case H1F:
	      obj = new TH1F;
	      break;
	    case H2F:
	      obj = new TH2F;
	      break;
	    case CANVAS:
	      obj = new TCanvas;
	      break;
	    default:
	      Clear();
	      return false;
	    }
	  //Check the size can work
	  if((objectOffset >= 0) && 
	     (int32_t(data.size()) >= objectOffset))
	    {
	      TBufferFile stream(TBuffer::kRead,
				 data.size()-objectOffset,
				 (char*)((&(data[0])) + objectOffset),kFALSE);
	      stream.SetReadMode();
	      //	      stream.ResetMap();
	      stream.SetBufferOffset(0);
	      stream.MapObject(obj);
	      obj->Streamer(stream);
	      obj->ResetBit(kIsReferenced);
	      obj->ResetBit(kCanDelete);
	    }
	  else
	    {
	      Clear();
	      return false;
	    }
	}
      else
	{
	  Clear();
	  return false;
	}
      return true;
    };
    void Clear()
    {
      objectOffset = -1;
    };
  };


}
#endif
