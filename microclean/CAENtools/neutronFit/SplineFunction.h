#ifndef ROOT_SplineFunction
#define ROOT_SplineFunction
#include <iostream>
#include "TObject.h"
//#include "TNamed.h"
#include "TSpline.h"
#include <vector>
#include "TString.h"
//#include "TMVA/TSpline1.h"

#include <math.h>

class SplineFunction : public TObject
{
 public:
  SplineFunction();
  //  SplineFunction(char * name,std::vector<float> X,std::vector<float> Y,float,float);
  SplineFunction(std::vector<float> &X,std::vector<float> &Y,float Min_,float Max_);
  //  void SetSpline(char * name,std::vector<float> X,std::vector<float> Y,float,float);
  void SetSpline(std::vector<float> &X,std::vector<float> &Y,float Min_,float Max_);
  double operator() (double *x,double*p);
  //  void fcn(int &npar, double *gin, double &f, double *par, int iflag) { f = operator()(gin,par);};
 private:
  TSpline3 * Spline;   //->
  //TMVA::TSpline1 * Spline;   //->
  bool SplineSet;
  double rMax;
  double rMin;
  ClassDef(SplineFunction,1)
};
#endif
