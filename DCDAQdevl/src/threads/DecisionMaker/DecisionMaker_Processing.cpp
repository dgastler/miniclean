#include <DecisionMaker.h>
#include <ReductionLevel.h>
#include <vector>
#include <fstream>
#include <iostream>

const int triggerMode=1;

const int zlePresamples=5;
const int zlePostsamples=20;
const int promptWindow=37;

const int prescale = 100;
const int prescaleIntQ = 100;
const int prescalePromptQ = 100;
const int prescaleMaxQ = 100;

const double intQthresh = 27709;//332517.0; full detector
const double promptQthresh = 0.5;
const double maxQthresh = 0.2;

const double triggerOffset = 1345;

int countIntQ=0;
int countPromptQ=0;
int countMaxQ=0;

enum {
  PRESCALE,
  INTQ_PASS,
  INTQ_PRESCALE,
  PROMPTQ_PASS,
  PROMPTQ_PRESCALE,
  MAXQ_PASS,
  MAXQ_PRESCALE,
  TRIGGER_MODE
};

enum { 
  CAENWFD_FULL_FORMAT_2PACK,
  CAENWFD_FULL_FORMAT_25PACK,
  CAENWFD_ZLE_FORMAT_2PACK,
  CAENWFD_ZLE_FORMAT_25PACK
};

int dataFormat = CAENWFD_ZLE_FORMAT_25PACK;


void DecisionMaker::ComputeDecision(DRPCBlock * event){
  RAT::DS::EV* ev=event->ev;
  int64_t eventID=event->eventID;
  int8_t reductionLevel=SoftwareTrigger(ev,eventID);

//  //simple mode  
//  if(event->eventID%5 == 0)
//    {
//      reductionLevel = ReductionLevel::CHAN_ZLE_WAVEFORM;
//    }
//  else
//    {
//      reductionLevel = ReductionLevel::CHAN_ZLE_INTEGRAL;
//    } 
//   
  event->reductionLevel=reductionLevel;
  

//#if __WORDSIZE == 64
//  printf("Event #%ld reduction level %d\n",
//	 event->eventID,event->reductionLevel);
//# else
//  printf("Event #%lld reduction level %d\n",
//  	 event->eventID,event->reductionLevel);
//#endif
}


  /** uses rawintegrals to compute variables which are rough approximations for the position,
   *  prompt charge, and radius of the event.  includes an overall prescale, and prescales for 
   *  each data reduction cut used.  if prune=1, then based on the reduction level, some data
   *  is pruned from the ev.  
   *  triggermode==0--overall prescale  
   *  triggermode==1--overall prescale, cut on integrated charge
   *  triggermode==2--overall prescale, cuts on integrated charge and fprompt
   *  triggermode==3--overall prescale, cuts on integrated charge, fprompt, and max charge in a pmt
   */
int DecisionMaker::SoftwareTrigger(RAT::DS::EV* ev, int64_t eventID){
  unsigned int trigger=0;
  bool passIntQ=false;
  bool passPromptQ=false;
  bool passMaxQ=false;
  double earliestWindowTime=999999999;
  double latestWindowTime=0;

  double histIntQ;
  double histFPrompt;

  //  std::ofstream filestr ("outfile.txt",std::ios::app);

  // overall prescale used in all trigger modes
  if( (eventID%prescale) == 0 ){
    trigger += 1 << PRESCALE;
  }
  // if we are using data reduction variables, calculate them from the rawintegrals
  if( triggerMode>0 ){
    double intQ=0;
    double promptQ=0;
    double triggerTime=triggerOffset;
    double promptCutoff=triggerTime+promptWindow;
    std::vector<double> pmtCharge(ev->GetPMTCount(),0);
    RAT::DS::Raw* raw;
    RAT::DS::RawIntegral* rawIntegral; 
    for(int ipmt=0; ipmt<ev->GetPMTCount(); ipmt++){
      if(ev->GetPMT(ipmt)->ExistRaw()){
	raw=ev->GetPMT(ipmt)->GetRaw();
	for(int irwf=0; irwf<raw->GetRawIntegralCount(); irwf++){
	  rawIntegral=raw->GetRawIntegral(irwf);
	  double charge=rawIntegral->GetIntegral();
	  int startTime=rawIntegral->GetStartTime()+zlePresamples;
	  int endTime=rawIntegral->GetWidth()+rawIntegral->GetStartTime()-zlePostsamples;
	  if (startTime<earliestWindowTime){
	    earliestWindowTime = startTime;
	  }
	  if (endTime>latestWindowTime){
	    latestWindowTime = endTime;
	  }
	  intQ+=charge;
	  if( startTime<promptCutoff && endTime>promptCutoff)
	    promptQ+=charge*( (promptCutoff-startTime)/(double)(endTime-startTime) );
	  else if( startTime<promptCutoff && endTime<=promptCutoff)
	    promptQ+=charge;
	  pmtCharge[ipmt]+=charge;
	  rawIntegral=NULL;

//	  filestr<<"Event, PMT: "<<eventID<<", "<<ipmt<<std::endl;
//	  filestr<<"PMT charge: "<<charge<<std::endl;
//	  filestr<<"Start time: "<<startTime<<std::endl;
//	  filestr<<"End time: "<<endTime<<std::endl;
//	  filestr<<"Cumulative charge: "<<intQ<<std::endl;
//	  filestr<<"Cumulative prompt charge: "<<promptQ<<std::endl;

	}
	raw=NULL;
      }
    }
    delete rawIntegral;
    delete raw;
    double fprompt = (double)promptQ/intQ;
    statpromptQ += promptQ;
    stattotalQ += intQ;
    statfprompt += fprompt;
    statearliestWindowTime += earliestWindowTime;
    statlatestWindowTime += latestWindowTime;
    fpromptCount += 1;
    
    // std::cout<<"fprompt: " << fprompt<<std::endl;
    // std::cout<<"promptQ: " << promptQ<<std::endl;
    double maxQ=0;
    for(unsigned int ipmt=0; ipmt<pmtCharge.size(); ipmt++)
      if( pmtCharge[ipmt]>maxQthresh*intQ ) 
	maxQ=pmtCharge[ipmt]/(double)intQ;
    if(intQ<intQthresh)
      passIntQ=true;
    if(fprompt>promptQthresh)
      passPromptQ=true;
    if(maxQ<maxQthresh)
      passMaxQ=true;

//    filestr<<"Total charge: "<<intQ<<std::endl;
//    filestr<<"Prompt charge: "<<promptQ<<std::endl;
//    filestr<<"fprompt: "<<fprompt<<std::endl;
//    filestr<<"Earliest time: "<<earliestWindowTime<<std::endl;
//    filestr<<"Latest time: "<<latestWindowTime<<std::endl;
//    filestr<<"Pass IntQ, PromptQ, MaxQ? "<<passIntQ<<", "<<passPromptQ<<", "<<passMaxQ<<std::endl;
    histIntQ = intQ;
    histFPrompt = fprompt;

    // set bits in trigger word and set in the ev
    if( triggerMode>=1){  
      if( passIntQ )
	trigger += 1 << INTQ_PASS;
      else{
	countIntQ++;
	if( (countIntQ%prescaleIntQ) == 0 )
	  trigger += 1 << INTQ_PRESCALE;
      }
      if( triggerMode>=2 ){
	if( passPromptQ ) 
	  trigger += 1 << PROMPTQ_PASS;
	else{
	  countPromptQ++;
	  if( (countPromptQ%prescalePromptQ) == 0)
	    trigger += 1 << PROMPTQ_PRESCALE;
	}
	if( triggerMode>=3 ){
	  if( passMaxQ ) 
	    trigger += 1 << MAXQ_PASS;
	  else{
	    countMaxQ++;
	    if( (countMaxQ%prescaleMaxQ) == 0)
	      trigger += 1 << MAXQ_PRESCALE;
	  }
	}
      }
    }
    trigger += ((unsigned int)triggerMode) << TRIGGER_MODE;
  }
  //ev->SetSoftwareTriggerWord(trigger);
  int reductionLevel = ReductionLevel::CHAN_PROMPT_TOTAL;
  if( (dataFormat==CAENWFD_FULL_FORMAT_2PACK) || (dataFormat==CAENWFD_FULL_FORMAT_25PACK) )
    reductionLevel = ReductionLevel::CHAN_FULL_WAVEFORM;
  else if( (dataFormat==CAENWFD_ZLE_FORMAT_2PACK) || (dataFormat==CAENWFD_ZLE_FORMAT_25PACK) ){
    if( (trigger & (1 << PRESCALE)) ){
      //|| (trigger & (1 << INTQ_PRESCALE)) 
      //|| (trigger & (1 << PROMPTQ_PRESCALE))
      //|| (trigger & (1 << MAXQ_PRESCALE)) ){
      reductionLevel = ReductionLevel::CHAN_ZLE_WAVEFORM;
    }
    else if( triggerMode>0 ){
      if( passIntQ || passPromptQ )
	reductionLevel=ReductionLevel::CHAN_ZLE_INTEGRAL;
      if( passIntQ ){
	reductionLevel = ReductionLevel::CHAN_ZLE_WAVEFORM;     
	if( triggerMode>1 && !passPromptQ )
	  reductionLevel = ReductionLevel::CHAN_ZLE_INTEGRAL;
	else if( triggerMode>2 && !passMaxQ )
	  reductionLevel = ReductionLevel::CHAN_ZLE_INTEGRAL;	
      }
    }
  }

  hist[reductionLevel]->Fill(histIntQ,histFPrompt);
  //  filestr<<"Decision: "<<reductionLevel<<std::endl;
  return reductionLevel;

  //Temporarily set all reduction levels to waveform
  reductionLevel=ReductionLevel::CHAN_ZLE_WAVEFORM;
}

