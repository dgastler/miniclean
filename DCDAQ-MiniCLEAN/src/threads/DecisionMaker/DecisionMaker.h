#ifdef __RATDS__

#ifndef __DECISIONMAKER__
#define __DECISIONMAKER__
/*
This thread takes full events with reduced information and makes a decision
about what data will be saved. 
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>
#include <RAT/DS/HardwareTrigger.hh>

#include <DRPCRatManager.h>

#include <TH2F.h>
#include <TFile.h>

#include <RAT/SoftwareTrigger.hh>
#include <RAT/DRSettings.hh>

#include <StatMessage.h>

#include <sstream>

class DecisionMaker : public DCThread 
{
 public:
  //==============================================================
  //DecisionMaker.cpp
  //==============================================================
  DecisionMaker(std::string Name);
  ~DecisionMaker();
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  bool SetDRSettings(xmlNode * Node, DRSettings &settings);
  
  //==============================================================
  //DecisionMaker_Run.cpp
  //==============================================================
  virtual void MainLoop();
 private:

  //==============================================================
  //DecisionMaker_Run.cpp
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(){};

  //==============================================================
  //DecisionMaker_Processing.cpp
  //==============================================================
  void ComputeDecision(RATEVBlock * event);
  SoftwareTrigger * softwareTrigger;

  //==============================================================
  //Memory manager interface
  //==============================================================
  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  int rawEventRate;
  std::vector<int> reductionLevelCount;
  std::vector<int> triggerCount;
  std::map<int,TH2F*> reductionQvFprompt;
  TH2F* totalQvFprompt;
  TH1F * channelZLEWindows;

  double statfprompt;
  int fpromptCount;
  double statearliestWindowTime;
  double statlatestWindowTime;
  double stattotalQ;
  double statpromptQ;

  //Decode gray code
  uint8_t DecodeGrayCode(uint8_t in);

  int checkHeaders;

  std::vector<HardwareTrigger::ReductionSetting> reductionSettings;

  //  TFile * histFile;

};
#endif

#endif
