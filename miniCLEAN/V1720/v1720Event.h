#include <stdint.h> //uint32_t
#include <vector>

class v1720Window
{
 public:
  v1720Window();
  ~v1720Window();
  uint16_t GetSample(uint32_t i);
  uint32_t GetSize();
  //Give people the raw data if they want it.   Don't keep this pointer though!
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);};
  void Setup(uint32_t * _rawData,uint32_t _size,bool _twoPack);
  uint32_t timeOffset;
 private:
  bool twoPack;
  //This class NEVER owns this data!
  //It is either under the control of the v1720Event class
  //or external code.
  uint32_t * rawData;
  uint32_t size;
  uint16_t GetSample20pk(uint32_t i);
  uint16_t GetSample25pk(uint32_t i);
};

class v1720EventChannel
{
 public:
  v1720EventChannel();
  ~v1720EventChannel();
  std::vector<v1720Window> Window;
  int ProcessChannel(uint32_t * _rawData,uint32_t _size,bool _ZLE ,bool _twoPack,uint8_t _channelID);
  uint8_t GetChannelID(){return(channelID);}
 private:
  bool twoPack;
  bool ZLE;
  //This class doesn't own this.
  uint32_t * rawData;
  uint32_t size;
  uint8_t channelID;
  int ProcessZLE();
  int ProcessFULL();
};

class v1720Event
{
 public:
  v1720Event();
  ~v1720Event();
  uint32_t *header;
  std::vector<v1720EventChannel> Channels;
  //returns a pointer to the next possible position of an event in the _rawData
  //array.
  char * ProcessEvent(char * _rawData, uint32_t _rawSizeMax,bool _twoPack);
 private:
  int ProcessHeader();
  int ProcessZLE(uint8_t channelID,uint32_t &position);
  int ProcessFULL(uint8_t channelID,uint32_t &position);
  uint8_t BitSum(uint32_t data);

  uint32_t * rawData;
  uint32_t size;
  uint32_t rawSizeMax;

  bool ZLE;
  bool twoPack;
  uint32_t boardID;
  uint32_t IOPattern;
  uint32_t channelMask;
  uint8_t  numberOfChannels;
  uint32_t eventNumber;
  uint32_t baseTime;
};
