#include <DCMessageClient.h>

DCMessageClient::DCMessageClient(std::string Name)
{
  SetType("DCMessageClient");
  SetName(Name);
 
  //Initialize Client variables

  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
}

DCMessageClient::~DCMessageClient()
{
  DeleteConnection();
}

bool DCMessageClient::Setup(xmlNode * SetupNode,
			    std::map<std::string,MemoryManager*> &/*MemManager*/)
{  
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  //================================
  //Parse data for Client config and expected clients.
  //================================
  //Use port 1717 for DCMessage servers
  if(!DCDAQServer.Setup(SetupNode))
    //Connection setup failed
    {
      return(false);
    }


  Ready = true;
  return(StartNewConnection());
}

//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DCMessageClient::StartNewConnection()
{
  if(DCDAQServer.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return(true);
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}

    }
  else 
    {
      if(DCDAQServer.fdSocket >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (DCDAQServer.fdSocket);
	  AddWriteFD(DCDAQServer.fdSocket);
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}
    }
  return(true);
}

void DCMessageClient::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(DCDAQServer.fdSocket) || IsWriteReady(DCDAQServer.fdSocket))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(DCDAQServer.fdSocket);
      RemoveWriteFD(DCDAQServer.fdSocket);      
      SetupSelect();

      //If the connect check fails
      if(!DCDAQServer.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %lds\n",DCDAQServer.remoteIP.c_str(),GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  DCDAQServer.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      DCMessage::Message message;
	      message.SetType(DCMessage::STOP);
	      SendMessageOut(message,true);
	    }	  
	}
    }
  
 
  //Check for a new packet
  if((DCDAQServer.thread != NULL)  && 
     IsReadReady(DCDAQServer.thread->GetInPacketManager()->GetFullFDs()[0]))
    {
      RouteRemoteDCMessage();
    }
  //Check for a new message
  if((DCDAQServer.thread != NULL)  && IsReadReady(DCDAQServer.thread->GetMessageOutFDs()[0]))
    {
      ProcessChildMessage();
    }
}

bool DCMessageClient::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Determine if this is local or not
  std::string DestName;
  struct in_addr addr;
  message.GetDestination(DestName,addr);  
  if(addr.s_addr > 0)
    {
      //Message is sent out of this DCDAQ session
      RouteLocalDCMessage(message);
    }
  else if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DCMessageClient::ProcessTimeout()
{
  //Catch a timeout if we don't have a DCNetThread
  if((DCDAQServer.thread == NULL) && (DCDAQServer.fdSocket != -1))
    {
      if(DCDAQServer.fdSocket != BadPipeFD)
	{
	  close(DCDAQServer.fdSocket);
	  DCDAQServer.fdSocket = BadPipeFD;
	}
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(DCDAQServer.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }
}

bool DCMessageClient::ProcessNewConnection()
{  
  bool ret = true;
  if(DCDAQServer.SetupDCNetThread(managerNode))
    {
      //Update local socket information
      socklen_t addrLocalSize = sizeof(DCDAQServer.addrLocal);
      getsockname(DCDAQServer.fdSocket,(sockaddr *) &(DCDAQServer.addrLocal),&addrLocalSize);

      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(DCDAQServer.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(DCDAQServer.thread->GetMessageOutFDs()[0]);      
      DCDAQServer.thread->Start(NULL);
      printf("DCMessage DCDAQServer at: %s\n",DCDAQServer.remoteIP.c_str());
      SetupSelect();

      
    

      //================================================
      //Register this connection with DCMessageProcessor
      //================================================

      //Data structure that holds the routing information
      DCMessage::DCRoute routeData;
      std::vector<uint8_t> data = routeData.SetData("",
						    DCDAQServer.GetRemoteRouteAddr(),
						    GetName());	
      //message of REGISTER_ADDR type to tell the DCMessageProcessor to route
      //messages with this address to this thread
      DCMessage::Message message;
      message.SetType(DCMessage::REGISTER_ADDR);
      message.SetDestination();
      message.SetData(&data[0],data.size());
      SendMessageOut(message,true);
    }
  else
    {
      ret = false;
    }
  return(ret);
}
void DCMessageClient::RouteRemoteDCMessage()
{  
  std::vector<uint8_t> messageData;
  int packetCount = 1;
  //Loop over all the packets that make up this DCMessage
  for(int iPacket = 0; iPacket < packetCount; iPacket++)
    {
      int32_t iDCPacket = FreeFullQueue::BADVALUE;    
      //Try to get a full in packet index
      DCDAQServer.thread->GetInPacketManager()->GetFull(iDCPacket,true);
      //Get the associated packet
      DCNetPacket * packet = DCDAQServer.thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Fail if the packet is bad
      if(packet == NULL)
	{
	  PrintWarning("Bad net packet.\n");
	  return;
	}
     
      //Now we have a valid packet
      
      //Check it's type (if it is a partial packet increase packetCount;
      if(packet->GetType() == DCNetPacket::DCMESSAGE_PARTIAL)
	{
	  packetCount++;
	}
      else if(packet->GetType() != DCNetPacket::DCMESSAGE)
	{	  
	  PrintWarning("Bad incoming packet\n");
	  //return packet as free and return
	  DCDAQServer.thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      
      //Now get the data from the packet and append it to the message data vector
      unsigned int existingMessageDataSize = messageData.size();
      //Resize our vector to hold the new data
      messageData.resize(existingMessageDataSize + packet->GetSize());
      //Check that the packet has a valid data pointer
      if(packet->GetData() == NULL)
	{
	  PrintWarning("Bad packet data\n");
	  //return packet as free and return
	  DCDAQServer.thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      //Copy in the new data
      memcpy(&messageData[existingMessageDataSize],packet->GetData(),packet->GetSize());
      //return our packet
      DCDAQServer.thread->GetOutPacketManager()->AddFree(iDCPacket);
    }
  //create a new DCPacket using the data from the packets
  DCMessage::Message  message;
  message.SetMessage(&messageData[0],messageData.size());

  //Get the destination address of the DCMessage
  std::string name;
  struct in_addr addr;
  message.GetDestination(name,addr);
  
  std::string IP1(INET_ADDRSTRLEN,' ');
  std::string IP2(INET_ADDRSTRLEN,' ');

  printf("%s %s\n",
	 inet_ntop(AF_INET,
		   &addr,
		   &IP1[0],
		   IP1.size()),
	 inet_ntop(AF_INET,
		   &DCDAQServer.addrLocal.sin_addr,
		   &IP2[0],
		   IP2.size())
	 );

  //Check if the address is the local address. 
  if(addr.s_addr == DCDAQServer.addrLocal.sin_addr.s_addr)
    {
      message.SetDestination(name);      
    }
  //The Client will forward broadcast messages to this session  
  SendMessageOut(message,true);
}
bool DCMessageClient::RouteLocalDCMessage(DCMessage::Message &message)
{  
  bool ret = true;
  std::string DestName;
  struct in_addr addr;
  message.GetDestination(DestName,addr);
  //Send to all. 
  if(addr.s_addr == 0xFFFFFFFF)
    {
      ret &= SendMessage(message);
    }
  else
    {      
      if(addr.s_addr == DCDAQServer.addrRemote.sin_addr.s_addr)
	{
	  ret &= SendMessage(message);
	}
    }
  return(ret);
}

bool DCMessageClient::SendMessage(DCMessage::Message &message)
{
  //Get the data for this packet
  std::vector<uint8_t> stream;
  //get a vector of data for this message to send.
  int streamSize = message.StreamMessage(stream);
  if(streamSize < 0)
    return(false);
  uint8_t * dataPtr  = &(stream[0]);	  
  int packetNumber = 0;
  //Send loop over the data until all of it is sent
  while(streamSize >0)
    {
      //Get a free packet index
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      DCDAQServer.thread->GetOutPacketManager()->GetFree(iDCPacket,true);
      //Get the DCNetPacket for that index
      DCNetPacket * packet = DCDAQServer.thread->GetOutPacketManager()->GetPacket(iDCPacket);
      //Fail if our packet is bad
      if(packet == NULL)
	{
	  return(false);
	}
      //Fill packet
      
      if(streamSize > packet->GetMaxDataSize())
	{
	  packet->SetType(DCNetPacket::DCMESSAGE_PARTIAL);
	  packet->SetData(dataPtr,packet->GetMaxDataSize(),packetNumber);
	  dataPtr+=packet->GetMaxDataSize();
	  streamSize-=packet->GetMaxDataSize();
	  packetNumber++;
	}
      else
	{
	  packet->SetType(DCNetPacket::DCMESSAGE);
	  packet->SetData(dataPtr,streamSize,packetNumber);
	  dataPtr+=streamSize;
	  streamSize-=streamSize;
	  packetNumber++;
	}
      //Send off packet
      DCDAQServer.thread->GetOutPacketManager()->AddFull(iDCPacket);
      packet = NULL;
    }
  return(true);
}

void DCMessageClient::ProcessChildMessage()
{
  DCMessage::Message message = DCDAQServer.thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::ERROR)
    {
      //Shut down our current thread
      DeleteConnection();
      //Start trying to get a new connection
      StartNewConnection();
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC) 
    {
      //      SendMessageOut(message,true);
    }
  else
    {
      PrintWarning("Unknown message from DCMessageClient's DCNetThread\n");
    }
}

void DCMessageClient::DeleteConnection()
{
  //Send stop message.
  DCMessage::Message message;
  message.SetType(DCMessage::STOP);
  //Making sure that the thread actually exists
  if(DCDAQServer.thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(DCDAQServer.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(DCDAQServer.thread->GetMessageOutFDs()[0]);
      //Send in a shutdown message.
      DCDAQServer.thread->SendMessageIn(message,true);
      //Wait for thread to finish
      pthread_join(DCDAQServer.thread->GetID(),NULL);
      //delete the thread      
      delete DCDAQServer.thread;
    }
  if(DCDAQServer.fdSocket != BadPipeFD)
    {
      RemoveReadFD(DCDAQServer.fdSocket);
      RemoveWriteFD(DCDAQServer.fdSocket);
      close(DCDAQServer.fdSocket);
    }  
  DCDAQServer.thread = NULL;
  DCDAQServer.fdSocket = BadPipeFD;
  SetupSelect();
}
