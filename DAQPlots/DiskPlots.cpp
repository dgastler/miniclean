#include "DiskPlots.h"

void MakeDiskRatePlot(MYSQL * conn,int /*runNumber*/,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grDiskRate;


  ss << "select time,value from sc_sens_MemBlockWriterDRT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grDiskRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);  

  grDiskRate->SetLineColor(kBlack);

  grDiskRate->GetYaxis()->SetRangeUser(-0.1,2.5);
  grDiskRate->GetYaxis()->SetNdivisions(210);
  //  grDiskRate->GetYaxis()->SetLabelSize  (grDiskRate->GetYaxis()->GetLabelSize()*2);
  //  grDiskRate->GetYaxis()->SetTitleSize  (grDiskRate->GetYaxis()->GetTitleSize()*2);
  //  grDiskRate->GetYaxis()->SetTitleOffset(grDiskRate->GetYaxis()->GetTitleOffset()*0.5);
  grDiskRate->GetYaxis()->SetTitle("Disk Rate (MBps)");
  grDiskRate->GetXaxis()->SetLabelColor(kBlack);
  grDiskRate->GetXaxis()->SetTitleColor(kBlack);
  grDiskRate->GetXaxis()->SetTitle("Run Time (hours)");

  grDiskRate->Draw("AL");

  c1->Print("DiskRates.pdf");
  
  delete c1;
}


