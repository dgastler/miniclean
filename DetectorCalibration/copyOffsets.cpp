#include <mysql/mysql.h>
#include <sstream>
#include <cstddef>
#include <stdio.h>
#include <stdlib.h>

int main(int argv,char ** argc)
{
  MYSQL * conn =mysql_init(NULL);

  conn=mysql_real_connect(conn,"192.168.0.6","control_user","27K2boil","daq_interface",0,NULL,0);
 
  if(!conn)
    {
      fprintf(stderr,"MySQL connection error \n");
      return 0;
    }
  
  std::stringstream query;
  MYSQL_RES * resultDAC=NULL;
  MYSQL_RES * resultSettings=NULL;
  MYSQL_RES * resultTime=NULL;

  query<<"select MAX(time) from DAC_Offsets;";

  if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
    {
      fprintf(stderr,"Error getting latest time\n");
      return 0;
    } 

  resultTime=mysql_store_result(conn);




  MYSQL_ROW row_time=mysql_fetch_row(resultTime);
  int newestTime=atoi(row_time[0]);

  mysql_free_result(resultTime);
  resultTime=NULL;
 
  query.str(std::string());   
  query<<"select * from DAC_Offsets where time="<<newestTime<<";";

  if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
    {
      fprintf(stderr,"Error getting DAC_Offsets\n");
      return 0;
    } 

  resultDAC=mysql_store_result(conn);
 
  
  int nRows=mysql_num_rows(resultDAC);

  for(int iRow=0;iRow<nRows;iRow++)
    {
      MYSQL_ROW row=mysql_fetch_row(resultDAC);
      int pmt_id=atoi(row[0]);
      int wfd_pos=atoi(row[1]);
      int wfd_id=atoi(row[2]);
      int dac_offset=atoi(row[3]);

      query.str(std::string());

      query<<"select wfd_pos,wfd_id from Settings_PMTs where pmt_id ="<<pmt_id<<" and run_number =0;";
      
      if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
	{
	  fprintf(stderr,"Error Checking labeling\n");
	  return 0;
	} 

      resultSettings=mysql_store_result(conn);
      MYSQL_ROW row_settings=mysql_fetch_row(resultSettings);
      if(atoi(row_settings[0])!=wfd_pos || atoi(row_settings[1])!=wfd_id)
	{
	  fprintf(stderr,"Channel labeling problem for channel %i \n",pmt_id);
	  return 0;

	}
      mysql_free_result(resultSettings);
      resultSettings=NULL;
      query.str(std::string());
      query<<"update Settings_PMTs set dac_offset="<<dac_offset<<" where pmt_id = "<<pmt_id<<" and run_number=0;";

      if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
	{
	  fprintf(stderr,"Error Setting DAC_Offsets\n");
	  return 0;
	} 

      
    }

  mysql_free_result(resultDAC);
  resultDAC=NULL;


  return 0;
     







}
