#include <DCMessage.h>

void DCMessage::Message::SetSource(std::string name,
				   struct in_addr addr)
{       
  SourceName = name;
  SourceAddr = addr;
}


void DCMessage::Message::GetSource(std::string & name,
				   struct in_addr & addr)
{
  name = SourceName;
  addr = SourceAddr;
}

void DCMessage::Message::SetDestination(std::string name,
					struct in_addr addr)
{       
  DestinationName = name;
  DestinationAddr = addr;
}


void DCMessage::Message::GetDestination(std::string & name,
					struct in_addr & addr)
{
  name = DestinationName;
  addr = DestinationAddr;
}


void DCMessage::Message::Clear()
{
  type = BLANKMESSAGE;
  timeStamp = 0;

  SourceName.assign("");
  memset(&DestinationAddr,0,sizeof(SourceAddr));

  DestinationName.assign("");
  memset(&DestinationAddr,0,sizeof(DestinationAddr));

  Data.clear();
}


void DCMessage::Message::CopyObj(const DCMessage::Message &rhs)
{
  type = rhs.type;
  timeStamp = rhs.timeStamp;
  Data = rhs.Data;
  //Source information
  SourceName = rhs.SourceName;
  SourceAddr = rhs.SourceAddr;
  //Destination data
  DestinationName = rhs.DestinationName;
  DestinationAddr = rhs.DestinationAddr;
}

void DCMessage::Message::Set(const void * in,size_t bytes,std::vector<uint8_t> & store)
{
  //Make our vector large enough;
  store.resize(bytes);
  if(bytes >0)
    {
      //Copy the data from inData to Data
      memcpy((void *) &(store[0]),in,bytes);
    }
}

void DCMessage::Message::Add(const void * in, size_t bytes,std::vector<uint8_t> & store)
{
  //Make room for copy
  store.resize(store.size() + bytes);
  if(bytes > 0)
    {
      memcpy((uint8_t *)&(store[0]) + (store.size() - bytes),in,bytes);
    }
}
