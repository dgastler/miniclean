#include <DCMessageClient.h>

DCMessageClient::DCMessageClient(std::string Name)
{
  SetType("DCMessageClient");
  SetName(Name);
 
  //Initialize Client variables

  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>1024</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>1024</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
}

DCMessageClient::~DCMessageClient()
{
  DeleteConnection();
}

bool DCMessageClient::Setup(xmlNode * SetupNode,
			    std::map<std::string,MemoryManager*> &MemManager)
{  
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  //================================
  //Parse data for Client config and expected clients.
  //================================

  //Address family
  if(FindSubNode(SetupNode,"AF") == NULL)
    {
      DCDAQServer.SocketFamily = AF_INET;
      PrintWarning("Using default AF_INET family.\n");
    }
  else
    GetXMLValue(SetupNode,"AF",GetType().c_str(),DCDAQServer.SocketFamily);

  //Get Client port.
  if(FindSubNode(SetupNode,"PORT") == NULL)
    {
      DCDAQServer.Port = 1717;
      PrintWarning("Using default listen port 1717.\n");
    }
  else
    GetXMLValue(SetupNode,"PORT",GetType().c_str(),DCDAQServer.Port);
  //Get server address.
  if(FindSubNode(SetupNode,"SERVER") == NULL)
    {
      PrintError("No server address!\n");
      return(false);
    }
  else
    {      
      //Get the address string
      GetXMLValue(SetupNode,
		  "SERVER",
		  GetType().c_str(),
		  DCDAQServer.RemoteIP);
      //correctly size the remoteaddr
      switch (DCDAQServer.SocketFamily)
	{
	case AF_INET:
	  DCDAQServer.addrRemote.resize(sizeof(sockaddr_in));
	  DCDAQServer.addrLocal.resize(sizeof(sockaddr_in));
	  break;
	case AF_INET6:
	  DCDAQServer.addrRemote.resize(sizeof(sockaddr_in6));
	  DCDAQServer.addrLocal.resize(sizeof(sockaddr_in6));
	  break;
	case AF_LOCAL:
	  DCDAQServer.addrRemote.resize(sizeof(sockaddr_un));
	  DCDAQServer.addrLocal.resize(sizeof(sockaddr_un));
	  break;
	default:
	  PrintError("Unknown address family type.");
	  return(false);
	  break;
	}      
      //copy the unix socket path to the sockaddr
      if(DCDAQServer.SocketFamily == AF_LOCAL)
	{
	  //build a pointer to our sockaddr data
	  struct sockaddr_un * sockaddr = (struct sockaddr_un *)&(DCDAQServer.addrRemote[0]);
	  //make sure our path isn't bigger than the sockaddr's size
	  if(DCDAQServer.RemoteIP.size() >= 
	     SUN_LEN(sockaddr) - sizeof(sockaddr->sun_family))
	    {
	      return(false);
	    }
	  else
	    {
	      //copy the path to the sockaddr
	      memcpy(sockaddr->sun_path,
		     DCDAQServer.RemoteIP.c_str(),
		     DCDAQServer.RemoteIP.size());
	    }
	}
      //a inet type 
      else
	{
	  //Move text to the sockaddr structure using inet_pton
	  //This works for both IPV4 and IPV6
	  struct sockaddr_in * addr;
	  struct sockaddr_in6 * addr6 ;
	  switch (DCDAQServer.SocketFamily)
	    {
	    case AF_INET:
	      addr = (struct sockaddr_in  *) &(DCDAQServer.addrRemote[0]);
	      inet_pton(DCDAQServer.SocketFamily,
			DCDAQServer.RemoteIP.c_str(),
			&(addr->sin_addr));
	      addr->sin_family = DCDAQServer.SocketFamily;
	      addr->sin_port = htons(DCDAQServer.Port);
	      break;
	    case AF_INET6:
	      addr6 = (struct sockaddr_in6 *) &(DCDAQServer.addrRemote[0]);
	      inet_pton(DCDAQServer.SocketFamily,
			DCDAQServer.RemoteIP.c_str(),
			&(addr6->sin6_addr));
	      addr6->sin6_family = DCDAQServer.SocketFamily;
	      addr6->sin6_port = htons(DCDAQServer.Port);
	      break;
	    default:
	      break;
	    }
	}      
    }
  Ready = true;
  return(StartNewConnection());
}

//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DCMessageClient::StartNewConnection()
{
  if(AsyncConnectStart(DCDAQServer.fdSocket,
		       DCDAQServer.SocketFamily,
		       DCDAQServer.addrRemote))
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return(true);
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}

    }
  else 
    {
      if(DCDAQServer.fdSocket >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (DCDAQServer.fdSocket);
	  AddWriteFD(DCDAQServer.fdSocket);
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}
    }
  return(true);
}

void DCMessageClient::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(DCDAQServer.fdSocket) || IsWriteReady(DCDAQServer.fdSocket))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(DCDAQServer.fdSocket);
      RemoveWriteFD(DCDAQServer.fdSocket);      
      SetupSelect();

      //If the connect check fails
      if(!AsyncConnectCheck(DCDAQServer.fdSocket))
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %ds\n",DCDAQServer.RemoteIP.c_str(),GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  DCDAQServer.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      DCMessage::Message message;
	      message.SetType(DCMessage::STOP);
	      SendMessageOut(message,true);
	    }	  
	}
    }
  
 
  //Check for a new packet
  if((DCDAQServer.Thread != NULL)  && IsReadReady(DCDAQServer.InPacketManagerFDs[0]))
    {
      RouteRemoteDCMessage();
    }
  //Check for a new message
  if((DCDAQServer.Thread != NULL)  && IsReadReady(ChildMessageFD))
    {
      ProcessChildMessage();
    }
}

bool DCMessageClient::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Determine if this is local or not
  std::string DestName;
  std::vector<uint8_t> addr;
  int32_t AF;
  message.GetDestination(DestName,addr,AF);  
  if(AF != -1)
    {
      //Message is sent out of this DCDAQ session
      RouteLocalDCMessage(message);
    }
  else if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DCMessageClient::ProcessTimeout()
{
  //Catch a timeout if we don't have a DCNetThread
  if((DCDAQServer.Thread == NULL) && (DCDAQServer.fdSocket != -1))
    {
      if(DCDAQServer.fdSocket != BadPipeFD)
	{
	  close(DCDAQServer.fdSocket);
	  DCDAQServer.fdSocket = BadPipeFD;
	}
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(DCDAQServer.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }
}

bool DCMessageClient::ProcessNewConnection()
{  
  //create a DCNetThread for this 
  DCDAQServer.Thread = new DCNetThread();
  if(DCDAQServer.Thread == NULL)
    {      
      close(DCDAQServer.fdSocket);
      DCDAQServer.fdSocket = BadPipeFD;
      return(false);
    }
  else 
    {
      //Setup this thread
      if(!DCDAQServer.Thread->SetupConnection(DCDAQServer.fdSocket,
					      DCDAQServer.SocketFamily,
					      DCDAQServer.addrLocal,
					      DCDAQServer.addrRemote))
	{
	  //Setup failed
	  close(DCDAQServer.fdSocket);
	  delete DCDAQServer.Thread;
	  DCDAQServer.fdSocket = BadPipeFD;
	  return(false);
	}
	 
      //Set up the memory manager for this DCNetThread
      if(!DCDAQServer.Thread->SetupPacketManager(managerNode))
	{
	  //Allocation failed
	  close(DCDAQServer.fdSocket);
	  delete DCDAQServer.Thread;
	  DCDAQServer.fdSocket = BadPipeFD;
	  return(false);
	}
      
      //Get the FDs for the free queue of the outgoing packet manager
      DCDAQServer.Thread->GetInPacketManager()->
	GetFullFDs(DCDAQServer.InPacketManagerFDs);      
      //Add the incoming packet manager fds to list
      AddReadFD(DCDAQServer.InPacketManagerFDs[0]);
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      ChildMessageFD = DCDAQServer.Thread->GetMessageOutFDs()[0];
      AddReadFD(ChildMessageFD);      
      DCDAQServer.Thread->Start(NULL);
      printf("DCMessage DCDAQServer at: %s\n",DCDAQServer.RemoteIP.c_str());
      SetupSelect();
    }
  return(true);
}
void DCMessageClient::RouteRemoteDCMessage()
{  
  std::vector<uint8_t> messageData;
  int packetCount = 1;
  //Loop over all the packets that make up this DCMessage
  for(int iPacket = 0; iPacket < packetCount; iPacket++)
    {
      int32_t iDCPacket = FreeFullQueue::BADVALUE;    
      //Try to get a full in packet index
      DCDAQServer.Thread->GetInPacketManager()->GetFull(iDCPacket,true);
      //Get the associated packet
      DCNetPacket * packet = DCDAQServer.Thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Fail if the packet is bad
      if(packet == NULL)
	{
	  PrintWarning("Bad net packet.\n");
	  return;
	}
     
      //Now we have a valid packet
      
      //Check it's type (if it is a partial packet increase packetCount;
      if(packet->GetType() == DCNetPacket::DCMESSAGE_PARTIAL)
	{
	  packetCount++;
	}
      else if(packet->GetType() != DCNetPacket::DCMESSAGE)
	{	  
	  PrintWarning("Bad incoming packet\n");
	  //return packet as free and return
	  DCDAQServer.Thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      
      //Now get the data from the packet and append it to the message data vector
      unsigned int existingMessageDataSize = messageData.size();
      //Resize our vector to hold the new data
      messageData.resize(existingMessageDataSize + packet->GetSize());
      //Check that the packet has a valid data pointer
      if(packet->GetData() == NULL)
	{
	  PrintWarning("Bad packet data\n");
	  //return packet as free and return
	  DCDAQServer.Thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      //Copy in the new data
      memcpy(&messageData[existingMessageDataSize],packet->GetData(),packet->GetSize());
      //return our packet
      DCDAQServer.Thread->GetOutPacketManager()->AddFree(iDCPacket);
    }
  //create a new DCPacket using the data from the packets
  DCMessage::Message  message;
  message.SetMessage(&messageData[0],messageData.size());
  //The Client will forward broadcast messages to this session  
  SendMessageOut(message,true);
}
bool DCMessageClient::RouteLocalDCMessage(DCMessage::Message &message)
{  
  bool ret = true;
  std::string DestName;
  std::vector<uint8_t> addr;
  int32_t AF;  
  message.GetDestination(DestName,addr,AF);
  //Send to all. 
  if(AF == -2)
    {
      ret &= SendMessage(message);
    }
  else
    {      
      if(AddrCompare(AF,addr,DCDAQServer.addrRemote))
	{
	  ret &= SendMessage(message);
	}
    }
  return(ret);
}

bool DCMessageClient::SendMessage(DCMessage::Message &message)
{
  //Get the data for this packet
  std::vector<uint8_t> stream;
  //get an vector of data for this message to send.
  int streamSize = message.StreamMessage(stream);
  if(streamSize < 0)
    return(false);
  uint8_t * dataPtr  = &(stream[0]);	  
  int packetNumber = 0;
  //Send loop over the data until all of it is sent
  while(streamSize >0)
    {
      //Get a free packet index
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      DCDAQServer.Thread->GetOutPacketManager()->GetFree(iDCPacket,true);
      //Get the DCNetPacket for that index
      DCNetPacket * packet = DCDAQServer.Thread->GetOutPacketManager()->GetPacket(iDCPacket);
      //Fail if our packet is bad
      if(packet == NULL)
	{
	  return(false);
	}
      //Fill packet
      
      if(streamSize > packet->GetMaxDataSize())
	{
	  packet->SetType(DCNetPacket::DCMESSAGE_PARTIAL);
	  packet->SetData(dataPtr,packet->GetMaxDataSize(),packetNumber);
	  dataPtr+=packet->GetMaxDataSize();
	  streamSize-=packet->GetMaxDataSize();
	  packetNumber++;
	}
      else
	{
	  packet->SetType(DCNetPacket::DCMESSAGE);
	  packet->SetData(dataPtr,streamSize,packetNumber);
	  dataPtr+=streamSize;
	  streamSize-=streamSize;
	  packetNumber++;
	}
      //Send off packet
      DCDAQServer.Thread->GetOutPacketManager()->AddFull(iDCPacket);
      packet = NULL;
    }
  return(true);
}

void DCMessageClient::ProcessChildMessage()
{
  DCMessage::Message message = DCDAQServer.Thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::BADCONNECTION)
    {
      //Shut down our current thread
      DeleteConnection();
      //Start trying to get a new connection
      StartNewConnection();
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if((message.GetType() == DCMessage::PACKETSOUT) || 
	  (message.GetType() == DCMessage::PACKETSIN) || 
	  (message.GetType() == DCMessage::DATAOUT) || 
	  (message.GetType() == DCMessage::DATAIN))
    {
      //      SendMessageOut(message,true);
    }
  else
    {
      PrintWarning("Unknown message from DCMessageClient's DCNetThread\n");
    }
}

void DCMessageClient::DeleteConnection()
{
  //Send stop message.
  DCMessage::Message message;
  message.SetType(DCMessage::STOP);
  //Making sure that the thread actually exists
  if(DCDAQServer.Thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(DCDAQServer.InPacketManagerFDs[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(DCDAQServer.Thread->GetMessageOutFDs()[0]);
      //Send in a shutdown message.
      DCDAQServer.Thread->SendMessageIn(message,true);
      //Wait for thread to finish
      pthread_join(DCDAQServer.Thread->GetID(),NULL);
      //delete the thread      
      delete DCDAQServer.Thread;
    }
  if(DCDAQServer.fdSocket != BadPipeFD)
    {
      RemoveReadFD(DCDAQServer.fdSocket);
      RemoveWriteFD(DCDAQServer.fdSocket);
      close(DCDAQServer.fdSocket);
    }  
  DCDAQServer.Thread = NULL;
  DCDAQServer.fdSocket = BadPipeFD;
  SetupSelect();
}
