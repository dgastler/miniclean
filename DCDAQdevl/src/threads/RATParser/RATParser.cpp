#include <RATParser.h>

RATParser::RATParser(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATParser"));
  Block_mem = NULL;
  EV_mem = NULL;
  evBlock = NULL;
  memBlock = NULL;
  evIndex = FreeFullQueue::BADVALUE;
  memBlockIndex = FreeFullQueue::BADVALUE;

  //Input blocks
  BlocksRead = 0;
  TimeStep_BlocksRead= 0;
  
  //Events
  TimeStep_Events = 0;
  BadEvents = 0;
  TimeStep_BadEvents = 0;

}

bool RATParser::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> & MemManager)
{
  bool ret = true;

  //There should be two memory managers
  // MemoryBlockManager:  interface with raw CAEN data
  if(!FindMemoryManager(Block_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  // WFDBlockManager:   interface with the RAT world
  if(!FindMemoryManager(EV_mem,SetupNode,MemManager))
    {
      ret = false;
    }

  if(!EV_mem)
    {
      fprintf(stderr,"Error %s: Missing EV memory manager\n.",GetName().c_str());
      ret = false;
    }
  if(!Block_mem)
    {
      fprintf(stderr,"Error %s: Missing Block memory manager\n.",GetName().c_str());
      ret = false;
    }
  
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


bool RATParser::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the RAT memory manager to shut down.
      EV_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(EV_mem->GetFreeFDs()[0]);
      RemoveReadFD(Block_mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Register the memoryblockmanager's full queue's FD so we know when there are
      AddReadFD(Block_mem->GetFullFDs()[0]);
      
      //Register the WFD/EV block's free queue's FDs with select
      AddReadFD(EV_mem->GetFreeFDs()[0]);
      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}
