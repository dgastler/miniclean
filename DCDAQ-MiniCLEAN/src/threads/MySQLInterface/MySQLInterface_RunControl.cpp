#include <MySQLInterface.h>

void MySQLInterface::CheckShutdown()
{
  //  sleep(5);
  DCMessage::Message message;
  std::string command("listthreads");
  DCMessage::DCString stringParser;
  std::vector<uint8_t> data = stringParser.SetData(command);
  message.SetData(&data[0],data.size());
  message.SetType(DCMessage::COMMAND);
  SendMessageOut(message);
}

bool MySQLInterface::StopCurrentRun()
{
  DCMessage::Message message;
  StoppingRun = true;
  //Send stop to run Control
  message.SetType(DCMessage::STOP);
  message.SetDestination("RunControl");
  SendMessageOut(message);
  return true;
}

bool MySQLInterface::StartNewRun()
{
  XML_Factory xmlFactory(&DAQDB_Connection);
  std::string xml = xmlFactory.LoadRun(0);
  
  DCMessage::Message message;
  message.SetData(xml.c_str(),xml.size());
  message.SetType(DCMessage::LAUNCH);
  SendMessageOut(message);
  return true;  
}

bool MySQLInterface::UpdateRunLockState(DCRunLock::Type type)
{
  std::stringstream ss;
  ss << "UPDATE RUNLOCK SET access = \""
     << GetTypeName(type)
     << "\", run_number = \"";

  if(type == DCRunLock::RUNNING)
    {
      ss << GetRunNumber() ;
    }
  else 
    {
      ss << "0";
    }
  ss << "\";";
  if(DAQDB_Connection.RunQuery(ss.str()))
    {
      return false;
    }
  return true;
}

void MySQLInterface::ProcessCommandMessage(DCMessage::Message &message)
{
  std::string response;
  DCMessage::DCString dcString;
  dcString.GetData(message.GetData(),response);            
  //Handle RUNLOCK changes
  if(response.find("RUNLOCK") != std::string::npos)
    {
      response = response.substr(response.find("RUNLOCK") + 7/*size*/); 
      boost::algorithm::trim(response);
      DCRunLock::Type lockState = DCRunLock::GetType(response.c_str());
      switch (lockState)
	{
	case DCRunLock::FREE:
	case DCRunLock::RUNNING:
	case DCRunLock::ERROR:
	  UpdateRunLockState(lockState);	  
	  break;
	case DCRunLock::STOP:
	  Print("DCDAQ Can't set RUNLOCK to STOP!");
	  break;
	default:
	  Print("Unknown RUNLOCK state\n");
	  break;
	}      
    }
  else if(response.find("RUN_UPDATE") != std::string::npos)
    {
      BackupRun();
    }
}


//return value gives if query worked
//Entry count returned via count
static bool CheckRunEntryCount(DCMySQL * conn,
			       const std::string & table,
			       int run_number_source,
			       int &count)
{
  std::stringstream query;
  query << "select count(*) from " << table << " where run_number=" << run_number_source;
  std::string response;
  if(!conn->GetSingleValue(query.str(),response))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  count = atoi(response.c_str());
  return true;
}

static bool RemoveRunFromTable(DCMySQL * conn, 
			       std::string table,
			       int run_number)
{
  std::stringstream query;
  query << "delete from " << table << " where run_number=" << run_number;
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }

  query.str(std::string());
  return true;
}


static bool CopyRunInTable(DCMySQL * conn, 
		    std::string table,
		    int run_number_source,
		    int run_number_destination)
{
  std::stringstream query;
  //Get columns
  query << "show columns in " << table << " where Field !='run_number'";
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  std::vector<std::vector<std::string> > fields;
  conn->GetTable(fields);
  conn->FreeResult();
  query.str(std::string());


  //BUild copy sql command

  //build the insert command
  query << "insert into " << table
	<< " (run_number";  // include run_number which will be "hard coded"
  //Add names of all other columns in the table
  for(size_t iField = 0; iField < fields.size();iField++)
    {
      query << "," <<  fields[iField][0];
    }
  query << ")";
  //Build select command that hard codes the destination run_number
  //and gets the data from the source run number
  query	<< " select " << run_number_destination;
  for(size_t iField = 0; iField < fields.size();iField++)
    {
      query << "," <<  fields[iField][0];
    }
  query << " from " << table << " where run_number=" << run_number_source;
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  conn->FreeResult();
  query.str(std::string());
  return true;
}

static bool CopyRunToDAQRuns(DCMySQL * conn, 
			     std::string table,
			     int run_number_source,
			     int run_number_destination)
{
  std::stringstream query;
  //Get columns
  query << "show columns in " << table << " where Field !='run_number'";
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  std::vector<std::vector<std::string> > fields;
  conn->GetTable(fields);
  conn->FreeResult();
  query.str(std::string());


  //BUild copy sql command

  //build the insert command
  query << "insert into daq_runs." << table
	<< " (run_number";  // include run_number which will be "hard coded"
  //Add names of all other columns in the table
  for(size_t iField = 0; iField < fields.size();iField++)
    {
      query << "," <<  fields[iField][0];
    }
  query << ")";
  //Build select command that hard codes the destination run_number
  //and gets the data from the source run number
  query	<< " select " << run_number_destination;
  for(size_t iField = 0; iField < fields.size();iField++)
    {
      query << "," <<  fields[iField][0];
    }
  query << " from " << table << " where run_number=" << run_number_source;
  if(!conn->RunQuery(query.str()))
    {
      std::cout << conn->GetError() << std::endl;
      return false;
    }
  conn->FreeResult();
  query.str(std::string());
  return true;
}


void MySQLInterface::BackupRun()
{
  int run_number_source = 0;
  int run_number_destination = GetRunNumber();
  if(run_number_destination <= 0)
    {
      PrintWarning("Backup of run failed because of bad run number\n");
      return;
    }
  //list of tables                                                                                                         
  std::vector<std::string> Tables;
  Tables.push_back("SThread_Data_Compressed");
  Tables.push_back("NetworkConnections");
  Tables.push_back("SThread_DecisionMaker");
  Tables.push_back("Settings_DataReduction");
  Tables.push_back("Memory_MemoryManagers");
  Tables.push_back("Messages_BeforeThreads");
  Tables.push_back("Messages_End");
  Tables.push_back("Messages_Start");
  Tables.push_back("Messages_Shutdown");
  Tables.push_back("Settings_PMTs");
  Tables.push_back("SThread_Data_Raw");
  Tables.push_back("Thread_Threads");
  Tables.push_back("Settings_WFDs");

  //========================================================                                                          
  //Check that the source exists
  //========================================================                       
  bool empty_run = true;
  for(size_t iTable = 0; iTable < Tables.size();iTable++)
    {
      int count = -1;
      //if there are any errors, consider the run empty and end
      if(!CheckRunEntryCount(&DAQDB_Connection,Tables[iTable],run_number_source,count))
	{
	  std::cout << "Failed due to errors" << std::endl;
	  empty_run = true;	 
	  break;
	}        
      if(count > 0)
        {
          empty_run = false;
          break;
        }
    }
  if(empty_run)
    {
      return ;
    }
  //========================================================
  //check for overwrites
  //========================================================
  empty_run = true;
  for(size_t iTable = 0; iTable < Tables.size();iTable++)
    {
      int count = -1;
      if(!CheckRunEntryCount(&DAQDB_Connection,Tables[iTable],run_number_destination,count))
	{
	  PrintWarning("Bad tables for run copy.");
	  empty_run = false;
	  break;
	}
      if((count > 0))
	{
	  empty_run = false;
	  break;
	}
    }
  if(!empty_run)
    {
      PrintWarning("Run already exists!");
      return;
    }
  //========================================================
  //copy the run
  //========================================================
  bool error = false;
  for(size_t iTable = 0; 
      ((iTable < Tables.size())&&(!error)); //stop if we have an error
      iTable++)
    {
      if(RemoveRunFromTable(&DAQDB_Connection, 
			    Tables[iTable],
			    run_number_destination))
	{
	  if(CopyRunInTable(&DAQDB_Connection, 
			     Tables[iTable],
			     run_number_source,
			     run_number_destination))
	    {
	      if(!CopyRunToDAQRuns(&DAQDB_Connection, 
				   Tables[iTable],
				   run_number_source,
				   run_number_destination))
		{
		  error = true;
		}
	    }
	  else
	    {
	      error = true;	      
	    }
	}
      else
	{
	  error = true;	  
	}
    }
  //========================================================
  //if there was an error, delete destination run number entries
  //========================================================
  if(error)
    {
      for(size_t iTable = 0;iTable < Tables.size();iTable++)
	{
	  RemoveRunFromTable(&DAQDB_Connection, 
			     Tables[iTable],
			     run_number_destination);
	}
    }
}
