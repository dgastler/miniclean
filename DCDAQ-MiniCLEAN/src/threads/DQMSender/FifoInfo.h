#ifndef __FifoInfo__
#define __FifoInfo__

#include <sys/types.h>
#include <unistd.h>
#include <string.h>

class FifoInfo
{
 public:
  FifoInfo();
  ~FifoInfo()
    {};

  void SetPID(pid_t num){pid=num;};
  void SetFD(int num){fd=num;};
  void SetName(char * path){name=path;};
  void SetStatus(bool stat){status=stat;};

  pid_t GetPID(){return pid;};
  int GetFD(){return fd;};
  char * GetName(){return name;};
  bool GetStatus(){return status;};
  void Clear();


 private:
  pid_t pid;
  char * name;
  int fd;
  bool status;
  

    


};

#endif
