#ifndef XML_FACTORY
#define XML_FACTORY

//#include <libxml/tree.h>
#include <libxml/parser.h>

#include <boost/algorithm/string.hpp>
#include <vector>
#include <string>
#include <sstream>

#include <MemoryManager_XML.h>
#include <Thread_XML.h>
#include <RunControl_XML.h>
#include <DecisionMaker_XML.h>
#include <V1720_XML.h>
#include <CompressData_XML.h>
#include <NetThread_XML.h>

#include <DCMySQL.h>


class XML_Factory
{
public:
  //Constructor calls SetMySQLConnection
  XML_Factory(DCMySQL * conn);
  
  //Destructor
  ~XML_Factory();
  
  //Load a run
  std::string LoadRun(int runNumber);

  
  bool AddThread(xmlNode * threadsNode,
		 std::string pcName,
		 std::string threadType,
		 std::string threadName,
		 xmlNode * responsesNode = NULL);

  bool AddMemoryManager(xmlNode * mmsNode,
			std::string pcName,
			std::string mmType,
			std::string mmName);

  void CleanUp();
private:  
  DCMySQL * DCMySQLConnection;
  xmlDoc * doc;

  int runNumber;

  std::vector<Thread_XML *> threads;
  std::vector<MemoryManager_XML *> memoryManagers;
};

#endif
