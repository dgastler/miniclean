#include <DRGetter.h>
void DRGetter::ProcessPacket()
{
  //Get new incomming packet
  DCNetThread * thread = DRSender.thread;
  int32_t iDCPacket = FreeFullQueue::BADVALUE;
  thread->GetInPacketManager()->GetFull(iDCPacket,true);
  //Check for bad packet type
  if(iDCPacket != FreeFullQueue::BADVALUE)
    //valid packet
    {
      //Get the next packet
      DCNetPacket * packet = thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Check if this is the correct kind of packet
      if(packet->GetType() != DCNetPacket::DR_DECISION)
	{
	  //ProcessBadPacket cleans up.
	  PrintError("Unexpected packet type!\n");
	  //return packet
	  thread->GetInPacketManager()->AddFree(iDCPacket);
	  //move on
	  return;
	}

      //Get the data from the packet.
      uint8_t * packetData = packet->GetData();
      uint16_t packetDataSize = packet->GetSize();
      //Check if there is enough data for an event block
      if(packetDataSize < sizeof(DRDecisionPacket::Event))
	{
	  PrintError("Packet too small.\n");
	  SendStop();
	  //return packet
	  thread->GetInPacketManager()->AddFree(iDCPacket);
	  //move on
	  return;
	}

      //Get the data from the packet
      DRDecisionPacket::Event * event = (DRDecisionPacket::Event *) packetData;

      //Add this to the DecisionQueue
      EventDecision eventDecision;
      eventDecision.ID = event->ID;
      eventDecision.reductionLevel = event->reductionLevel;
      DecisionQueue.Add(eventDecision,true);
    }
  else
    {
      PrintError("BadPacket? ");
      SendStop();
    }
  //return packet
  thread->GetInPacketManager()->AddFree(iDCPacket);
}
