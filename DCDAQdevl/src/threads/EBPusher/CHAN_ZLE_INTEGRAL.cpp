#include <CHAN_ZLE_INTEGRAL.h>

int CHAN_ZLE_INTEGRAL::AddChannelHeader(RAT::DS::PMT * pmt)
{
  ebChannel = (EBPacket::ChannelData*) currentPtr;
  ebChannel->ID = pmt->GetID();   
  ebChannel->dataBlockCount = pmt->GetRaw()->GetRawIntegralCount();      
  //move forward in our array
  currentPtr += sizeof(EBPacket::ChannelData); 
  //update the free space available. 
  dataSizeLeft -= sizeof(EBPacket::ChannelData); 
  return 0;
}

uint32_t CHAN_ZLE_INTEGRAL::CalculatePMTSize(RAT::DS::Raw * raw)
{
  uint32_t channelSize = (sizeof(EBPacket::ChannelData) + 
			  (raw->GetRawIntegralCount()*sizeof(EBPacket::IntegralBlock)));      
  return(channelSize);
}


int CHAN_ZLE_INTEGRAL::AddData(RAT::DS::Raw * raw)
{
  int rawIntegralCount = raw->GetRawIntegralCount();
  //Add the raw integrals
  for(int iRawIntegral = 0;iRawIntegral < rawIntegralCount;iRawIntegral++)
    {
      //Get the the current RAT rawIntegral
      RAT::DS::RawIntegral * rawIntegral = raw->GetRawIntegral(iRawIntegral);
      //Get a pointer to a EBPacket RawIntegral type that points to the next
      //available spot in the packet array
      EBPacket::IntegralBlock * ebIntegralBlock = (EBPacket::IntegralBlock *) currentPtr;
      
      //Copy data
      ebIntegralBlock->integral    = uint16_t(rawIntegral->GetIntegral());
      ebIntegralBlock->startTime   = rawIntegral->GetStartTime();
      ebIntegralBlock->width       = rawIntegral->GetWidth();
      
      //move forward in our array
      currentPtr += sizeof(EBPacket::IntegralBlock); 
      //update the free space available. 
      dataSizeLeft -= sizeof(EBPacket::IntegralBlock); 
    }  
  return 0;
}
