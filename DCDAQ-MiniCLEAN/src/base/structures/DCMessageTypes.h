#ifndef __DCMESSAGETYPES__
#define __DCMESSAGETYPES__

namespace DCMessage 
{ 
  enum DCMessageType
    {
      //Blank
      BLANK = 0,
            
      //DCLauncher
      LAUNCH,
      LAUNCH_RESPONSE,
      
      //DCMessageProcessor interface
      REGISTER_TYPE,
      UNREGISTER_TYPE,
      REGISTER_ADDR,
      UNREGISTER_ADDR,
            
      //THREAD/MM interface
      END_OF_THREAD,
      STOP,
      PAUSE,
      GO,

      RUN_NUMBER,
      RUN_NUMBER_RESPONSE,
      GET_STATS,
      GET_ROUTE,

      ERROR,
      WARNING,
      STATISTIC,
      HISTOGRAM,
      TEXT,
      
      COMMAND,
      COMMAND_RESPONSE,
      RUN_INFO,

      //Error type for unroutable messages
      CAN_NOT_ROUTE,
      //Misc system
      NETWORK,
      //COUNT (if you delete this I will cut you!)
      COUNT
    };  
  
  const char * GetTypeName(enum DCMessageType type); 
  enum DCMessageType GetTypeFromName(const char * name);
}
#endif
