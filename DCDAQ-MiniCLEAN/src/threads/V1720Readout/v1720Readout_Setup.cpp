#ifdef __CAENLIBS__
#include <v1720Readout.h>

bool v1720Readout::Setup(xmlNode * SetupNode,
			 std::vector<MemoryManager*> &MemManager)
{
  //=================================================
  //Find the MemoryManager we need
  //=================================================
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return false;
    }
  //=================================================
  //Find readout settings
  //=================================================

  //Find max readouts(CAEN events) into a block before we pass it off
  if(FindSubNode(SetupNode,"MaxReadoutsPerBlock") == NULL)
    {
      MaxReadoutsPerBlock = 100;
    }
  else
    {
      GetXMLValue(SetupNode,"MaxReadoutsPerBlock","THREAD",MaxReadoutsPerBlock);
    }
  
  xmlNode * WFDsNode = FindSubNode(SetupNode,"WFDS");
  if(WFDsNode == NULL)
    {
      PrintError("No WFD setup XMLs\n");
      return(false);
    }
  else
    {
      MinimumFreeBufferSpace = 0;
      uint32_t nWFDs = NumberOfNamedSubNodes(WFDsNode,"WFD");
      for(unsigned int iWFD = 0; iWFD < nWFDs;iWFD++)
	{
	  xmlNode * tempNode = FindIthSubNode(WFDsNode,"WFD",iWFD);
	  BaseXMLNodes.push_back(tempNode);
	  WFD.push_back(new v1720());
	  if(WFD.back()->LoadXMLConfig(BaseXMLNodes.back()) != 0)
	    {
	      PrintError("Error in XML configuration\n");
	      return false;
	    }
	  
	  TimeStep_EventCount.push_back(0);
	  
	  TimeStep_ReadoutNumber.push_back(0);
	  
	  TimeStep_ReadoutSize.push_back(0);  

	  TimeStep_Occupancy.push_back(0);

	  MinimumFreeBufferSpace += WFD.back()->GetEventSize();
	}
    }

  currentTime = time(NULL);
  lastTime = time(NULL);
  Ready = true;
  return true ;  
}

#endif
