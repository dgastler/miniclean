#ifndef ROOT_SplineFunction
#define ROOT_SplineFunction
#include <iostream>
#include "TObject.h"
#include "TSpline.h"
#include <vector>
#include "TString.h"

#include <math.h>

class SplineFunction : public TObject
{
 public:
  SplineFunction();
  SplineFunction(std::vector<float> &X,std::vector<float> &Y,float Min_,float Max_);
  void SetSpline(std::vector<float> &X,std::vector<float> &Y,float Min_,float Max_);
  double operator() (double *x,double*p);
  TSpline3 ** GetSpline(){return(&Spline);};
 private:
  TSpline3 * Spline;   //->
  bool SplineSet;
  double rMax;
  double rMin;
  ClassDef(SplineFunction,3)
};
#endif
