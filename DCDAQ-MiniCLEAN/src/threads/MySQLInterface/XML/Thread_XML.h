#ifndef THREAD_XML_H
#define THREAD_XML_H

//#include <libxml/tree.h>
#include <libxml/parser.h>
#include <vector>
#include <string>
#include <sstream>

#include <boost/algorithm/string.hpp>

#include <DCMySQL.h>


class Thread_XML
{
 public:
  //Constructor sets up mysql connection
  //If no connections: will produce segfaults!
  Thread_XML(DCMySQL *conn,int _runNumber);

  //=========================================================
  //General Get_Thread_XML Function used for the majority of threads
  //=========================================================
  virtual bool Generate_XML(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responseNode);
  
protected:
  DCMySQL * DCMySQLConnection;
  int runNumber;  

  xmlNode * threadNode;
private:
  bool FillData(const std::string & pcName,
		const std::string & threadType,
		const std::string & threadName,
		std::vector<std::vector<std::string> > & threadTemplate,
		std::vector<std::vector<std::string> > & threadData,
		std::vector<std::vector<std::string> > & threadDefaults,
		std::vector<std::vector<std::string> > & threadRequired);
  bool GetDetectorInfo(const std::string &pcName,
		       const std::string &threadType,
		       const std::string &threadName);
};

#endif
