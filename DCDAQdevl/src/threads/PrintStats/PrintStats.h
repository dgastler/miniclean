#ifndef __PRINTSTATS__
#define __PRINTSTATS__
/*

 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <math.h> //fabs()

class PrintStats : public DCThread 
{
 public:
  PrintStats(std::string Name);
  ~PrintStats();

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  
  virtual void MainLoop(){};
 private:
  virtual bool ProcessMessage(DCMessage::Message &message);

};
#endif
