#include <DRPCRatManager.h>

DRPCRatManager::DRPCRatManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("DRPCRatManager"));
  //Make sure our memory vector is empty
  Memory.clear();
}

DRPCRatManager::~DRPCRatManager()
{
  Deallocate();
}

void DRPCRatManager::Deallocate()
{
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //delete EV event (check if it's not NULL)
      if(Memory.back().ev)
	{
	  delete Memory.back().ev; 
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
}

bool DRPCRatManager::AllocateMemory(xmlNode * setupNode)
{
  bool ret = true;
  
  int NumEVERR = GetXMLValue(setupNode,"NUMEV","MANAGER",NumEV);
  if(NumEVERR != 0)
    {
      fprintf(stderr,"Error reading NUMEV in DRPCRatManager.\n");
      ret = false;
    }
  int VectorHashSizeERR = GetXMLValue(setupNode,"VECTORHASHSIZE","MANAGER",VectorHashSize);
  if(VectorHashSizeERR != 0)
    {
      fprintf(stderr,"Error reading VECTORHASHSIZE in DRPCRatManager.\n");
      ret = false;
    }

  //Setup the queues and vector-hash
  Setup(VectorHashSize);

  //Allocate the needed RAT EVs
  if(ret)
    {
      for(uint32_t iEV = 0; iEV < NumEV;iEV++)
	{
	  EVBlock evBlock;
	  evBlock.ev = new RAT::DS::EV;
	  evBlock.reductionLevel = -1;	  
	  if(evBlock.ev)
	    {
	      Memory.push_back(evBlock);
	      int32_t index = Memory.size()-1;
	      AddFree(index);
	    }
	  else
	    {
	      ret = false;
	    }
	}     
    }

  return(ret);
}
