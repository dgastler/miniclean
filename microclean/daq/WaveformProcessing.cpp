#include "WaveformProcessing.h"


Int_t Find_Waveform_Maximum(Int_t ChannelSize, Float_t * Y)
  //Finds the position of the maximum of a positive going pulse
  //retuns the array index of this point.
{
  Float_t MaxY = 0; //3;
  Int_t   MaxYlocation = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Y[i] > MaxY)
	{
	  MaxY = Y[i];
	  MaxYlocation = i;
	}
    }
  return(MaxYlocation);
}

Int_t Find_Waveform_Start(Int_t ChannelSize, Float_t * Y)
  //Finds the start of the maximum pulse in the range. 
  //Start is defined as 20% the maxium height.
  //Returns the array index of the start point.
{
  Float_t MaxY = 0;
  Int_t   MaxYlocation = 1;
  for(Int_t i = 1; i < ChannelSize;i++)
    {
      if(Y[i] > MaxY)
	{
	  MaxY = Y[i];
	  MaxYlocation = i;
	}
    }
  //Find 20% peak. 
  for(Int_t i = 0; MaxYlocation + i > 0;i--)
    {
      if(5.0*Y[MaxYlocation + i] < MaxY)
	return(MaxYlocation + i);
    }
  return(0);
}


Float_t MergeWaveforms(Int_t ChannelSize, Float_t * Ya,Float_t *Yb,Float_t Y_Offset,  Float_t Gain = 10)
  //Merge two waveforms with different gains and some unknown
  //offset if the high gain channel goes above 200 adc counts 
  //after rebaselining. 
  //Waveform must be in ADC counts. 
  //returns the time shift between the waveforms in
  //array indicies. 
{
  Float_t start = 0;
  Float_t end = 0;
  Float_t deltaT = 0;
  Float_t trip = 200-Y_Offset;
  //  for(int i = 0; i < ChannelSize;i++)
  //Find position of channel a hitting 200-Y_Offset on the way up and down.
  for(int i = 1; i < ChannelSize;i++)
    {
      if((Ya[i] > trip)&&(start == 0))
	{
	  start = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) + i - 1;
	}
      if((Ya[i] < trip)&&(start !=0))
	{
	  end = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) +i - 1;
	  i = ChannelSize;
	}
      
    }
  
  deltaT = end - start;
  
  //Find B channel MAXes
  Int_t ChannelB_MAX = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Yb[i] >= Yb[ChannelB_MAX])
	{
	  ChannelB_MAX = i;
	}
      if((i == 2*ChannelB_MAX)&&(Yb[ChannelB_MAX] > 5))
	break;
    }
  
  Int_t Tf = ChannelB_MAX;
  Int_t Ti = Tf-1;
  
  while( Tf - Ti < deltaT )
    {
      if(((Tf >= 0) && (Tf < ChannelSize)) && ((Ti >= 0) && (Ti < ChannelSize)))
      //      if ( Yb[Tf] > Yb[Ti] )
	{
	  if(Yb[Tf] > Yb[Ti])
	    {
	      Tf++;
	    }
	  else
	    {
	      Ti--;
	    }
	}
      else
	{
	  break;
	  Ti = 0xefffffff;
	  Tf = 0xefffffff;
	}
    }

	
  Float_t shift;
  shift = (start - Ti + end - Tf)/2.0;
  

  for(int i = 0;i < ChannelSize-1;i++)
    {
      if((i - (int)shift) > 0 && (i - (int)shift) < ChannelSize)
	{
	  if(Ya[i] >= 255 - ceil(Y_Offset) && Gain*Yb[i-(int)shift] > Ya[i])
	    {
	      Ya[i] = Gain * Yb[i-(int)shift];
	    }
	}
    }
  
  return(shift);
}


Float_t Offset(Event * event,Int_t Channel,Int_t Length,Float_t *Sigma = 0)
  //Calculate the offset of the waveform by measureing the mean
  //in the first Length pointf from waveform Channel.
  //If you want the sigma of the mean pass a pointer to an allocated
  //varible as Simga.
{
  Float_t offset = 0;
  Float_t offset_squared = 0;
  Float_t sigma = 0;
  if(Length > event->GetSize(Channel))
    {
      std::cerr << "Length larger than array" << std::endl;
      return(FP_NAN);
    }
  for(Int_t i = 0; i < Length;i++)
    {
      offset += event->GetPoint(Channel,i);
      offset_squared += event->GetPoint(Channel,i)*event->GetPoint(Channel,i);
    }
  if(Sigma != 0)
    {
      sigma = sqrt((offset_squared - offset*offset/1000)/(1000-1));
      Sigma[0] = sigma;
    }
  offset/=1000;
  return(offset);
}

Bool_t CopyWaveform(Event * event,Int_t Channel,Float_t * waveform,Int_t Length,Float_t offset)
{
  Float_t FixGainTrigger = 254 - offset;
  Int_t Size =event->GetSize(Channel); 
  Bool_t ret = false;
  if(Length < Size)
    {
      std::cerr << "Array smaller than waveform size." << std::endl;
      return(kFALSE);
    }
  for(Int_t i = 0 ;i < Size;i++)
    {
      waveform[i] = event->GetPoint(Channel,i) - offset;
      if(waveform[i] >= FixGainTrigger)
	{
	  ret = true;
	}
    }
  return(ret);
}
