#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <boost/algorithm/string.hpp>

#include <signal.h>
#include <pthread.h>

#include "FAKE_Control.h"

volatile bool run;

void signal_handler(int sig)
{
  if(sig == SIGUSR1)
    run = false;  
  if(sig == SIGINT)
    run = false;
}

int main(int argc, char ** argv)
{
  struct sigaction sa;
  sa.sa_handler = signal_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGUSR1,&sa,NULL);
  sigaction(SIGINT,&sa,NULL);

  double freq = 500;
  double dead_Time = 10;
  if(argc < 4)
    {
      printf("usage: %s file1 file2 file3 [rate]\n",argv[0]);
      return -1;
    }

  if(argc > 4)
    {
      freq = atof(argv[4]);
    }
  
  FAKE_Control FAKE;
  std::vector<std::string> files;
  std::vector<std::string> IPs;

  //Board 1
  files.push_back(argv[1]);
  IPs.push_back("192.168.1.2");
  //Board 2
  files.push_back(argv[2]);
  IPs.push_back("192.168.2.2");
  //Board 3
  files.push_back(argv[3]);
  IPs.push_back("192.168.3.2");
  
  FAKE.Setup_Threads(files,IPs);
  FAKE.Setup_Random(freq,dead_Time);
  FAKE.Start();
  
  run = true;
  std::string line;
  while(run)
    {
      line.clear();
      std::cin >> line;
	
      if((std::cin.eof()) || 
	 boost::iequals(line,"stop"))
	{
	  run = false;
	  std::cout << "Stopping run\n";
	}
      else 
	{
	  double freq = atof(line.c_str());
	  if(freq > 0)
	    {
	      FAKE.Setup_Random(freq,dead_Time);
	    }
	  else
	    {
	      std::cout << "Unknown: " << line << std::endl;
	    }
	}
    }
  FAKE.Stop();
  FAKE.Join();
  return 0;
}
