#ifdef __RATDS__
#include <UnPackData.h>

UnPackData::UnPackData(std::string Name)
{
  SetType(std::string("UnPackData"));
  SetName(Name);
  dsIndex = FreeFullQueue::BADVALUE;
  blockIndex = FreeFullQueue::BADVALUE;
  Mem = NULL;
  DS_mem = NULL;
  block = NULL;
  blockCount = 0;
}

UnPackData::~UnPackData()
{
}

bool UnPackData::Setup(xmlNode *SetupNode,std::vector<MemoryManager*> &MemManager)
{
  //Find the memory managers
  if(!FindMemoryManager(DS_mem,SetupNode,MemManager))
    {
      PrintError("Can't find DS_mem Manager");
      return false;
    }
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      PrintError("Can't find memblock Manager");
      return false;
    }
  /*
  RATCopyQueue * monitorQueue = NULL;
  if(FindMemoryManager(monitorQueue,SetupNode,MemManager))
    {
      eventMonitor.Setup(monitorQueue);
    }
  */

  //Load the channel map
  //  uint32_t neededInfo = (InfoTypes::CHANNEL_MAP);
  //uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //if((parsedInfo&neededInfo) != neededInfo)
  //{
  //  PrintError("DetectorInfo not complete");
  //  return false;
  //}

  Ready = true;
  return true;
}

bool UnPackData::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      //Tell the memory managers to shut down.
      Mem->Shutdown();
      DS_mem->Shutdown();

      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
  	     GetTime(&tempTime).c_str(),
  	     GetName().c_str());
      RemoveReadFD(Mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
  	     GetTime(&tempTime).c_str(),
  	     GetName().c_str());
      //Register the FDs we want to listen to
      AddReadFD(Mem->GetFullFDs()[0]);

      SetupSelect();
      Loop = true;
    }
  else
    {ret = false;
    }
  return ret;
}

void UnPackData::ProcessTimeout()
{
  //Set time
  currentTime = time(NULL);
  //Send update
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t timeStep = difftime(currentTime,lastTime);
      DCMessage::Message message;
      StatMessage::StatRate ssRate;

      ssRate.sBase.type = StatMessage::EVENT_RATE;
      ssRate.count = timeStepProcessedEvents;
      ssRate.sInfo.time = currentTime;
      ssRate.sInfo.interval = timeStep;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.units = Units::NUMBER;
      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);
      
      timeStepProcessedEvents = 0;
      lastTime = currentTime;
    }
}

#endif
