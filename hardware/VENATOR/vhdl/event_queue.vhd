--This queue can store up to 4095 words of data. This capability is used to complete
--the abstraction of the register-oriented functionality of the top-level module. That is,
--a user can repeatedly read data out of this queue by requesting to read a single register
--over Ethernet. There is a two clock-cycle delay from the toggling of RD_EN to the display
--of new data on DOUT. However, other modules in this project do not need to worry about
--this subtlety--the top value on the queue always floats to the top. Accordingly, RD_EN
--need only be asserted to indicate that a value has been read (rather than signaling
--an intent to read values in the future).
--Boston University CLEAN project--Conor DuBois.


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity event_queue is Port(
	RESET : in STD_LOGIC;
	CLK : in STD_LOGIC;
	DIN : in unsigned(31 downto 0);
	WR_EN : in STD_LOGIC;
	RD_EN : in STD_LOGIC;
	SAMPLES : out unsigned(11 downto 0);
	DOUT : out unsigned(31 downto 0));
end event_queue;

architecture Behavioral of event_queue is
	signal wen : STD_LOGIC_VECTOR(7 downto 0);
	signal raddr : unsigned(11 downto 0) := x"000";
	signal waddr : unsigned(11 downto 0) := x"000";
	signal addr2 : unsigned(2 downto 0) := "000";
	signal count : unsigned(11 downto 0) := x"000";
	signal next_raddr : unsigned(11 downto 0);
	signal next_waddr : unsigned(11 downto 0);
	signal low_count : unsigned(11 downto 0);
	signal high_count : unsigned(11 downto 0);
	type multiple is ARRAY(7 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
	signal output : multiple;
begin
	wen <= x"00" when WR_EN = '0' or (count = x"FFF" and RD_EN = '0') else
		x"01" when waddr(11 downto 9) = "000" else 
		x"02" when waddr(11 downto 9) = "001" else
		x"04" when waddr(11 downto 9) = "010" else
		x"08" when waddr(11 downto 9) = "011" else
		x"10" when waddr(11 downto 9) = "100" else
		x"20" when waddr(11 downto 9) = "101" else
		x"40" when waddr(11 downto 9) = "110" else x"80";
	DOUT <= unsigned(output(to_integer(addr2))) 
		when count /= x"000" else x"FFFFFFFF";
	SAMPLES <= count;
	next_raddr <= raddr + x"001";
	next_waddr <= waddr + x"001";
	low_count <= count - x"001";
	high_count <= count + x"001";

	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			if RESET = '1' then
				raddr <= x"000";
				waddr <= x"000";
				count <= x"000";
			else
				if RD_EN = '1' and count /= x"000" and WR_EN = '1' then
					raddr <= next_raddr;
					waddr <= next_waddr;
					count <= count;
				elsif RD_EN = '1' and count /= x"000" then
					raddr <= next_raddr;
					waddr <= waddr;
					count <= low_count;
				elsif WR_EN = '1' and count /= x"FFF" then
					raddr <= raddr;
					waddr <= next_waddr;
					count <= high_count;
				else
					raddr <= raddr;
					waddr <= waddr;
					count <= count;
				end if;
			end if;
			addr2 <= raddr(11 downto 9);
		end if;
	end process;

	memories: for i in 0 to 7 generate
		memory0to7: RAMB16BWER generic map(
			DATA_WIDTH_A => 36,
			DATA_WIDTH_B => 36,
			WRITE_MODE_A => "READ_FIRST",
			WRITE_MODE_B => "READ_FIRST") port map(
			CLKA => CLK,
			WEA => x"F",
			ENA => wen(i),
			REGCEA => '0',
			ADDRA(13 downto 5) => STD_LOGIC_VECTOR(waddr(8 downto 0)),
			ADDRA(4 downto 0) => "00000",
			DIA => STD_LOGIC_VECTOR(DIN),
			DIPA => x"0",
			DOA => open,
			DOPA => open,
			RSTA => RESET,
			CLKB => CLK,
			WEB => x"0",
			ENB => '1',
			REGCEB => '0',
			ADDRB(13 downto 5) => STD_LOGIC_VECTOR(raddr(8 downto 0)),
			ADDRB(4 downto 0) => "00000",
			DIB => x"00000000",
			DIPB => x"0",
			DOB => output(i),
			DOPB => open,
			RSTB => RESET);
	end generate;
end Behavioral;

