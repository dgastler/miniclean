#ifndef __SERVERCONNECTION__
#define __SERVERCONNECTION__

#include <vector>
#include <string>
#include <DCNetThread.h>
#include <Connection.h>

class ServerConnection
{
 public:
  ServerConnection(){badFD = -1;Clear();}
  ~ServerConnection();
  void Clear();

  bool Setup(xmlNode * SetupNode);
  bool ProcessConnection(xmlNode * managerNode);
  void DeleteConnection(size_t i);

  size_t Size(){return(client.size());};

  Connection & operator[](size_t i){return(*(client[i]));};
  Connection & Back(){return(*(client.back()));};

  //Variables for the server
  uint16_t             serverPort;
  struct sockaddr_in   addrLocal;
  struct sockaddr_in   addrListen;
  std::string          listenAddress;
  int                  serverSocketFD;

 private:
  //List of cleared remote addresses
  std::vector<uint32_t >  acceptList;
  //List of connected objects
  std::vector<Connection*>      client;  
  //Bad file descriptor value
  int badFD;

  void PrintError(const char * str){fprintf(stderr,"Connection error: %s\n",str);};
  void PrintWarning(const char * str){printf("Connection warning: %s\n",str);};

};

#endif
