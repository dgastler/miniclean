#include <FEPCBadEvent.h>

FEPCBadEvent::FEPCBadEvent(std::string Name)
{
  SetType("FEPCBadEvent");
  SetName(Name);
  //Set initial value for statistics updates
  lastTime = time(NULL);
  currentTime = lastTime;
  badEventCount = 0;
  Ready = false;
  mem = NULL;
}

bool FEPCBadEvent::Setup(xmlNode * SetupNode,
			 std::map<std::string,MemoryManager*> &MemManager)
{  
  bool ret = true;
  // Find and load the FEPC memory interface
  if(!FindMemoryManager(mem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!mem)
    {
      PrintError("Missing FEPC RAT memory manager\n.");
      ret = false;
    }
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}



void FEPCBadEvent::MainLoop()
{
  if(IsReadReady(mem->GetBadEventFDs()[0]))  
    {
      //Get the bad event
      BadEvent::sBadEvent badEvent;
      if(mem->GetBadEvent(badEvent))
	//Get index worked
	{

	  //Count of all bad events
	  badEventCount++;
	  //count of bad events by submitted error code
	  if(errorCodeCount.find(badEvent.errorCode) == 
	     errorCodeCount.end())
	    {
	      errorCodeCount[badEvent.errorCode] = 0;
	    }
	  errorCodeCount[badEvent.errorCode]++;	      
	  
	  if(badEvent.index != mem->GetBadValue())
	    //index and event are valid
	    {
	      //Get the event at this index
	      FEPCBlock * event = mem->GetFEPCBlock(badEvent.index);
	      if(event != NULL)
		{
		
		  //Count of bad events by event status
		  if(stateCount.find(event->state) == stateCount.end())
		    {
		      stateCount[event->state] = 0;
		    }
		  stateCount[event->state]++;
		  
		  //clear the event
		  event->Clear();
		}
	      //Add the event to the free queue
	      if(!mem->AddFree(badEvent.index))
		{
		  //We are probably shutting down in this case
		  PrintError("Can't add to free queue.\n");
		  SendStop();
		}
	      
	    }
	  else
	    {
	      PrintError("Got a bad event index...\n");
	    }
	}
      else
	{
	  PrintError("Failed to get bad event index...\n");
	}
    }
}

void FEPCBadEvent::ProcessTimeout()
{
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      float TimeStep = difftime(currentTime,lastTime);
      DCMessage::TimedCount  timedCount;      
      DCMessage::Message message;
      
      //Send the badCount
      sprintf(timedCount.text,"Bad event count: ");
      timedCount.count = badEventCount;
      timedCount.dt = TimeStep;
      message.SetDataStruct(timedCount);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      badEventCount = 0;

      //StateCount
      for(std::map<int8_t,uint32_t>::iterator it = stateCount.begin(); 
	  it != stateCount.end();
	  it++)
	{
	  sprintf(timedCount.text,"Bad event count @ state %d: ",it->first);
	  timedCount.ID = it->first;
	  timedCount.count = it->second;
	  timedCount.dt = TimeStep;
	  message.SetDataStruct(timedCount);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message,false);
	  it->second = 0;
	}
      //stringCount
      for(std::map<int32_t,uint32_t>::iterator it = errorCodeCount.begin(); 
	  it != errorCodeCount.end();
	  it++)
	{
	  sprintf(timedCount.text,"Bad event error code %d count: ",it->first);
	  timedCount.ID = 0;
	  timedCount.count = it->second;
	  timedCount.dt = TimeStep;
	  message.SetDataStruct(timedCount);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message,false);
	  it->second = 0;
	}

      //record time
      lastTime = currentTime;
    }
}

bool FEPCBadEvent::ProcessMessage(DCMessage::Message & message)
{
  if(message.GetType() == DCMessage::GO)
    {
      if(Ready)
	{
	  AddReadFD(mem->GetBadEventFDs()[0]);
	  SetupSelect();
	  Loop = true;
	}
      return(true);
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      
      RemoveReadFD(mem->GetBadEventFDs()[0]);
      SetupSelect();
      Loop = false;
      return(true);
    }
  return(false);
}
