#include <PacketBuilder.h>

int PacketBuilder::AddEventHeader(FEPCBlock * event)
{
  if(sizeof(EBPacket::Event) < dataSizeLeft)
    //there is enough room for the header
    {
      ebEvent = (EBPacket::Event *) dataPtr;
      ebEvent->ID = event->ev->GetEventID();
      ebEvent->channelCount = 0;
      ebEvent->WFDHeaderCount = 0;
      ebEvent->dataType = event->reductionLevel;
      //move forward in our array
      currentPtr += sizeof(EBPacket::Event); 
      //update the free space available. 	      
      dataSizeLeft -= sizeof(EBPacket::Event); 
    }
  else
    //not enough room for the header.   (BAD!)
    {
      return(-1);
    }
  return(0);
}

bool PacketBuilder::Send(FEPCBlock * event)
{
  //First check that we have a valid packet manager
  if((dynamic_cast<DCNetPacketManager *>(packetManager)) == NULL)
    {
      fprintf(stderr,"DCNetPacketManger not set!\n");
      return(false);
    }

  //====================================
  //Build and fill packet(s) from this EV
  //====================================
  int pmtCount = event->ev->GetPMTCount();    
  for(int iPMT = 0; iPMT < pmtCount;iPMT++)
    //Loop over each PMT, sending packets when needed
    {
      //Get this PMT and it's ID
      RAT::DS::PMT * pmt = event->ev->GetPMT(iPMT);
      //Get the PMT's raw strucutre
      RAT::DS::Raw * raw = pmt->GetRaw();
      
      //Check if there is room left in the packet
      //If there isn't a packet is finished and sent off.
      //A fresh one is then set up.
      if(CheckSizeLeftPMT(raw,event) != 0)
	{
	  //Something bad happened!
	  return(false);
	}
      else{/*We have a packet with enough space*/}
      
      //Add the channel header
      AddChannelHeader(pmt);	
      //Add data to the packet
      if(AddData(raw) == 0)
	{
	  //update the drEvent's channel count
	  ebEvent->channelCount++;	    
	}
      else
	{
	  //Something bad happened!
	  return(false);
	}
    }
  
  //====================================
  //Build and fill packet(s) from this EV's WFD header
  //====================================

  //TEMP, UPDATE AFTER RAT-NEWDAQ
  if(event->ev->GetPMTCount() >0)
    {
      EBPacket::WFDHeader tempHeader;  
      //use only PMT0's WFD header until RAT-newdaq is done
      memcpy(&tempHeader,
	     &(event->ev->GetPMT(0)->GetRaw()->GetHeader()[0]),
	     sizeof(EBPacket::WFDHeader));
      header.resize(size_t(floor(event->ev->GetPMTCount()/8.0)));
      for(size_t i = 0; i < header.size();i++)
	{
	  header[i] = tempHeader;
//	  printf("%08X\n%08X\n%08X\n%08X\n\n",
//		 ((uint32_t*)&header[i])[0],
//		 ((uint32_t*)&header[i])[1],
//		 ((uint32_t*)&header[i])[2],
//		 ((uint32_t*)&header[i])[3]);
	  //Hack to simulate real data size, but let people know
	  //That the full headers aren't in place yet. 
	  //Waiting for RAT-newdaq to be implimented. 
	  //If you see weird numbers in the WFD headers, this is probably why.
	  tempHeader.word[0] = 0xFFFFFFFF;
	  tempHeader.word[1] = 0xFFFFFFFF;
	  tempHeader.word[2] = 0xFFFFFFFF;
	  tempHeader.word[3] = 0xFFFFFFFF;
	}
    }
  //TEMP, UPDATE AFTER RAT-NEWDAQ

  //Add the WFD data to the end of this event's packets
  if(CheckSizeLeftWFD(event) != 0)
    {
      //Something bad happened!
      return(false);
    }
  else
    {
      int headerDataSize = header.size()*sizeof(EBPacket::WFDHeader);
      //Copy header info into the packet
      memcpy(currentPtr,(uint8_t*) &(header[0]),headerDataSize);
      dataSizeLeft -= headerDataSize;
      currentPtr += headerDataSize;
      ebEvent->WFDHeaderCount = header.size();
    }
  
  //Send the last packet if needed
  if(packet != NULL)
    {
      //Send out our packet. 
      if(packetNumber > 0)
	SendPacket(packetTypePartial);
      else
	SendPacket(packetTypeFull);
      ClearPacket();
    }	    
  return(true);
}

void PacketBuilder::ResetPacket()
{
  packet = NULL;         
  dataPtr = NULL;  
  currentPtr = NULL;
  dataSizeLeft = 0; 
  iDCPacket = FreeFullQueue::BADVALUE;
}

void PacketBuilder::ClearPacket()
{
  ResetPacket();
  packetNumber = 0;
}

void PacketBuilder::SendPacket(int packetType)
{  
  //Set the type for this packet
  packet->SetType(packetType);
  //Set the data for this packet
  packet->SetData(dataPtr,
		  uint32_t(currentPtr -
			   dataPtr),
		  packetNumber);	      
  //Send out the current packet
  packetManager->AddFull(iDCPacket);
  //Clear our local packet info
  ResetPacket();
  //Increment packet number
  packetNumber++;  
}

int PacketBuilder::CheckPacket(uint32_t size, FEPCBlock * event)
{
  //Check if we have a packet to send
  if(packet != NULL)
    {
      SendPacket(packetTypePartial);
    }	  
  //Get a free packet
  packetManager->GetFree(iDCPacket,true);
  packet = packetManager->GetPacket(iDCPacket);
  //Set up our arrays
  dataPtr = packet->GetData();
  currentPtr = dataPtr; 
  dataSizeLeft = packet->GetMaxDataSize();         	  
  
  //Add the event header
  if(AddEventHeader(event) != 0)
    {
      return -1 ;
    }
  
  //Check if there is room in the packet for our channel data
  if(size > dataSizeLeft)
    {
      return -1 ;
    }
  return 0;
}

int PacketBuilder::CheckSizeLeftWFD(FEPCBlock * event)
{
  int ret = 0;
  //Calculate how much space we need for this data      
  uint32_t size = header.size() * sizeof(EBPacket::WFDHeader);
  
  if(size > dataSizeLeft)
    //Packet is full, 
    {
      ret = CheckPacket(size,event);
    }
  return ret ;
}
int PacketBuilder::CheckSizeLeftPMT(RAT::DS::Raw * raw,FEPCBlock * event)
{
  int ret = 0;
  //Calculate how much space we need for this data      
  uint32_t channelSize = CalculatePMTSize(raw);
  
  if(channelSize > dataSizeLeft)
    //Packet is full, 
    {
      ret = CheckPacket(channelSize,event);
    }
  return ret ;
}

int PacketBuilder::AddChannelHeader(RAT::DS::PMT * pmt)
{
  ebChannel = (EBPacket::ChannelData*) currentPtr;
  ebChannel->ID = pmt->GetID();   
  ebChannel->dataBlockCount = 0;//pmt->GetRaw()->GetRawIntegralCount();      
  //move forward in our array
  currentPtr += sizeof(EBPacket::ChannelData); 
  //update the free space available. 
  dataSizeLeft -= sizeof(EBPacket::ChannelData); 
  return 0;
}

uint32_t PacketBuilder::CalculatePMTSize(RAT::DS::Raw * raw) {return 0;}
int      PacketBuilder::AddData(RAT::DS::Raw * raw) {return 0;}
