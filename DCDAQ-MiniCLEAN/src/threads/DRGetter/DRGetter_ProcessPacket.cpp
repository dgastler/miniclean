#ifdef __RATDS__
#include <DRGetter.h>
void DRGetter::ProcessPacket()
{
  //Get new incomming packet
  int32_t iDCPacket = FreeFullQueue::BADVALUE;
  DRSender.GetInPacketManager()->GetFull(iDCPacket,true);
  //Check for bad packet type
  if(iDCPacket != FreeFullQueue::BADVALUE)
    //valid packet
    {
      //Get the next packet
      DCNetPacket * packet = DRSender.GetInPacketManager()->GetPacket(iDCPacket);
      //Check if this is the correct kind of packet
      if(packet->GetType() != DCNetPacket::DR_DECISION)
	{
	  //ProcessBadPacket cleans up.
	  PrintWarning("Unexpected packet type!\n");
	  //return packet
	  DRSender.GetInPacketManager()->AddFree(iDCPacket);
	  //move on
	  return;
	}

      //Get the data from the packet.
      uint8_t * packetData = packet->GetData();
      uint16_t packetDataSize = packet->GetSize();
      //Check if there is enough data for an event block
      if(packetDataSize < sizeof(DRDecisionPacket::Event))
	{
	  PrintWarning("Packet too small.\n");
	  //return packet
	  DRSender.GetInPacketManager()->AddFree(iDCPacket);
	  //move on
	  return;
	}

      //Get the data from the packet
      DRDecisionPacket::Event * event = (DRDecisionPacket::Event *) packetData;

      //set the attempts on this event to zero
      event->attempts = 0;

      //Add this to the DecisionQueue
      DecisionQueue.Add(*event,true);
    }
  else
    {
      PrintWarning("BadPacket? ");
    }
  //return packet
  DRSender.GetInPacketManager()->AddFree(iDCPacket);
}
#endif
