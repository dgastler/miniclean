#include <DRAssembler.h>

void DRAssembler::DeleteConnection(size_t iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
 
  //Making sure that the thread actually exists
  if(server[iConn].thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(server[iConn].thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(server[iConn].thread->GetMessageOutFDs()[0]);          
    }
  SetupSelect();
  server.DeleteConnection(iConn);
}

bool DRAssembler::ProcessNewConnection()
{
  //create a DCNetThread for this 
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().thread->GetInPacketManager()->GetFullFDs()[0]);
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().thread->GetMessageOutFDs()[0]);      
      server.Back().thread->Start(NULL);
      printf("FEPC connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().remoteIP.c_str());
      SetupSelect();
    }
 else
   {
     return(false);
   }
  return(true);
}
