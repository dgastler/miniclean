#ifdef __RATDS__

#include <PackData.h>

PackData::PackData(std::string Name)
{
  SetType(std::string("PackData"));
  SetName(Name);
  blockIndex = FreeFullQueue::BADVALUE;
  Mem = NULL;
  EBPC_mem = NULL;
  DRPC_mem = NULL;
  FEPC_mem = NULL;
  RATCopy_mem = NULL;

  writeFEPCBad = false;
  writeFEPCSend = false;
  writeDRPCBad = false;
  writeDRPCRespond = false;
  writeEBPCBad = false;
  writeEBPCDisk = false;


  firstBlock = true;

  timeStepProcessedEvents = 0;
  lastFIFOTime=time(NULL);


}

PackData::~PackData()
{
}

bool PackData::Setup(xmlNode *SetupNode,std::vector<MemoryManager*> &MemManager)
{

  
  //Get the MemBlockManager
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return(false);
    }
  if(FindMemoryManager(RATCopy_mem,SetupNode,MemManager))
    {
      Print("Sending data to DQMSender \n");
    }
  if(Mem != NULL)
    {
      Mem->IncrementWritingThreads();
    }
  //Get DetectorInfo
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::CHANNEL_MAP);
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }

  //Get whichever memory managers we want to listen to
  int rawNodes = NumberOfNamedSubNodes(SetupNode,"RAW");
  for(int iNode=0;iNode<rawNodes;iNode++)
    {
      xmlNode *rawNode = FindIthSubNode(SetupNode,"RAW",iNode);
      if(rawNode == NULL)
	{
	  PrintError("Could not find the RAW subnode.");
	}
      else if(FindMemoryManager(FEPC_mem,rawNode,MemManager))
      	{
      	  if(FindSubNode(rawNode,"BAD"))
      	    {
      	      writeFEPCBad = true;
      	    }
      	  if(FindSubNode(rawNode,"SEND"))
      	    {
      	      writeFEPCSend = true;
      	    }
      	}
      else if(FindMemoryManager(DRPC_mem,rawNode,MemManager))
      	{
      	  if(FindSubNode(rawNode,"BAD"))
      	    {
      	      writeDRPCBad = true;
      	    }
      	  if(FindSubNode(rawNode,"RESPOND"))
      	    {
      	      writeDRPCRespond = true;
      	    }
      	}
      else if(FindMemoryManager(EBPC_mem,rawNode,MemManager))
      	{
      	  if(FindSubNode(rawNode,"BAD"))
      	    {
      	      writeEBPCBad = true;
      	    }
      	  if(FindSubNode(rawNode,"DISK"))
      	    {
      	      writeEBPCDisk = true;
      	    }
      	}
      else
	{
	  PrintWarning("Memory manager of unknown type.");
	}
    }
  if(!(writeFEPCSend || writeFEPCBad || writeDRPCRespond 
       || writeDRPCBad || writeEBPCDisk || writeEBPCDisk))
    {
      PrintWarning("Not listening to any queues.");
      return false;	
    }
  
  EventSizeHist = new TH1F("EventSizeHist","Event size",500,0,10000);
  EventSizeFile = TFile::Open("EventSizeFile.root","RECREATE","",1);

  Ready = true;
  return true;
}

bool PackData::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {

      if(Mem != NULL)
	Mem->Shutdown();
      if(RATCopy_mem != NULL)
	{

	  RATCopy_mem->Shutdown();

	}

      //Mem->Shutdown();

      if(FEPC_mem != NULL)
      	{
      	  RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
      	  RemoveReadFD(FEPC_mem->GetBadEventFDs()[0]);	  
      	}
      if(DRPC_mem != NULL)
      	{
      	  RemoveReadFD(DRPC_mem->GetRespondFDs()[0]);
      	  RemoveReadFD(DRPC_mem->GetBadEventFDs()[0]);
      	}
      if(EBPC_mem != NULL)
      	{
      	  RemoveReadFD(EBPC_mem->GetToDiskFDs()[0]);
      	  RemoveReadFD(EBPC_mem->GetBadEventFDs()[0]);
      	}
         
     
      EndOfRun();
     
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
  	     GetTime(&tempTime).c_str(),
  	     GetName().c_str());
      if(FEPC_mem != NULL)
      	{
      	  RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
      	  RemoveReadFD(FEPC_mem->GetBadEventFDs()[0]);
      	}
      if(DRPC_mem != NULL)
      	{
      	  RemoveReadFD(DRPC_mem->GetRespondFDs()[0]);
      	  RemoveReadFD(DRPC_mem->GetBadEventFDs()[0]);
      	}
      if(EBPC_mem != NULL)
      	{
      	  RemoveReadFD(EBPC_mem->GetToDiskFDs()[0]);
      	  RemoveReadFD(EBPC_mem->GetBadEventFDs()[0]);
      	}
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
  	     GetTime(&tempTime).c_str(),
  	     GetName().c_str());
      //Register the appropriate queues' FDs so we know when we have a new 
      //event block to process
      if(FEPC_mem != NULL)
      	{
      	  if(writeFEPCSend)
      	    {
      	      AddReadFD(FEPC_mem->GetUnSentFDs()[0]);
      	    }
      	  if(writeFEPCBad)
      	    {
      	      AddReadFD(FEPC_mem->GetBadEventFDs()[0]);
      	    }
      	}
      if(DRPC_mem != NULL)
      	{
      	  if(writeDRPCRespond)
      	    {
      	      AddReadFD(DRPC_mem->GetRespondFDs()[0]);
      	    }
      	  if(writeFEPCBad)
      	    {
      	      AddReadFD(DRPC_mem->GetBadEventFDs()[0]);
      	    }
      	}
      if(EBPC_mem != NULL)
      	{
      	  if(writeEBPCDisk)
      	    {
      	      AddReadFD(EBPC_mem->GetToDiskFDs()[0]);
      	    }
      	  if(writeEBPCBad)
      	    {
      	      AddReadFD(EBPC_mem->GetBadEventFDs()[0]);
      	    }
      	}
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void PackData::ProcessTimeout()
{
  //Set time
  currentTime = time(NULL);
  //Send update
  if(currentTime - lastTime > GetUpdateTime())
    {
      DCtime_t timeStep = difftime(currentTime,lastTime);
      DCMessage::Message message;
      StatMessage::StatRate ssRate;

      ssRate.sBase.type = StatMessage::EVENT_RATE;
      ssRate.count = timeStepProcessedEvents;
      ssRate.sInfo.time = currentTime;
      ssRate.sInfo.interval = timeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.units = Units::NUMBER;
      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);
      
      timeStepProcessedEvents = 0;
      lastTime = currentTime;

      //Send event size histogram
//      DCMessage::DCTObject rootObj;
//      std::vector<uint8_t> dataTemp;
//      dataTemp = rootObj.SetData((TObject * )(EventSizeHist),DCMessage::DCTObject::H1F);
//      message.SetDestination();
//      message.SetType(DCMessage::HISTOGRAM);
//      message.SetData(&(dataTemp[0]),dataTemp.size());
//      message.SetTime(currentTime);
//      EventSizeHist->Reset();
//      SendMessageOut(message,false);
		 
    }
}

#endif
