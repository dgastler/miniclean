#include "TH2F.h"
#include <math.h>

static Float_t Volts = (1.0/255.0);
static Float_t seconds = (2e-9);
static Float_t C = (1.0/255.0) * (2e-9) * (1.0/50.0);

Float_t Integrate(Float_t * Y,Int_t Start, Int_t End,TH2F * h1,Float_t Sigma,Int_t Window,Int_t before);
