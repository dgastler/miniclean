#ifndef __MEM_BLOCK_WRITER__
#define __MEM_BLOCK_WRITER__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>
#include <RAT/DS/DCDAQ_DCTime.hh>
#include <RAT/DS/DCDAQ_RAWDataStructs.hh>

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <dirent.h>

#include <TFile.h>
#include <TH1.h>


class MemBlockWriter : public DCThread
{
 public:
  MemBlockWriter(std::string Name);
  ~MemBlockWriter();
  virtual bool Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  uint16_t blocksPerFile;
  uint16_t fileBlockCounter;
  uint8_t writeMode;
  FILE * outfile;
  std::string outputDirectory;
  uint16_t SubRunNumber;

  time_t last;
  time_t current;

  //Statistics
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;
  uint64_t dataWordsWritten;

  //Memory Access
  int32_t memBlockIndex;
  MemoryBlock * memBlock;
  MemoryBlockManager * Block_mem;
  uint64_t memBlockCounter;

  //Histograms
  TH1F * BlockSizeHist;
  TFile * BlockSizeFile;

  //file header
  MemBlockMessage::header blockHeader;

  //Stuff for check that we are still writing.
  //This should eventually be removed and done in RunWatchdog.
  uint32_t noBlocksCounter;
  uint32_t noBlocksThreshold;

};

#endif
