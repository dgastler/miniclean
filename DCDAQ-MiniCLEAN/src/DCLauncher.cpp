#include <DCLauncher.h>

uint64_t DCLauncher::Launch(std::string config)
{
  //Temp containers for new memory managers and threds
  //We will fill these and if everything is sucessful, then
  //we will add them to the DAQ's list.
  //If there is a failure, everythign will be shut down.
  std::vector<DCThread *> tempThread;
  std::vector<MemoryManager*> tempMemoryManager;

  //Parse the configuration string into an XML structure
  xmlDoc * configXML = xmlReadMemory(config.c_str(),
				     (int) config.size(),
				     NULL,
				     NULL,
				     0);
  //Check that the text was correctly parsed into XML
  if(configXML == NULL)
      return(BAD_XML);
  //Get the base node of the xml
  xmlNode * baseNode = xmlDocGetRootElement(configXML);
  

  //Setup any Memory managers
  uint64_t memoryManagerRet = 0;
  if(FindSubNode(baseNode,"MEMORYMANAGERS") != NULL)
    {
      memoryManagerRet = SetupMemoryManagers(FindSubNode(baseNode,"MEMORYMANAGERS"),
					     tempMemoryManager);
      //Return an error and remove the memory managers if
      //There was an error. 
      if(memoryManagerRet != SETUP_OK)
	{
	  DeleteMemoryManagerMap(tempMemoryManager);
	  //Return MMFAIL ored with memoryManagerRet.
	  //Setup can only work with 16 mm at a time. 
	  //the return will use the last 16 bits to specify which
	  //MM failed.   The first 16 give the type of error. 
	  return(memoryManagerRet);
	}
    }
  //Memory managers have been correctly set up at this point.
  

  //Setup threads
  uint64_t threadRet = 0;
  if(FindSubNode(baseNode,"THREADS") != NULL)
    {
      threadRet = SetupThreads(FindSubNode(baseNode,"THREADS"),
			       tempThread,
			       tempMemoryManager);
      //Clean up if we have a failure
      if(threadRet != SETUP_OK)
	{
	  //First shut down and delete all threads
	  DeleteThreadVector(tempThread);
	  //Now delete memoryManagers
	  DeleteMemoryManagerMap(tempMemoryManager);
	  return(threadRet);
	}			       
    }
  
  //MemoryManagers and threads have been correctly set up.
  //Merge memory managers
  if(!AddMemoryManager(tempMemoryManager) ||
     !AddThread(tempThread))
    {
      //This means you messed up code in this class.
      //Fix it now!@
      //First shut down and delete all threads
      DeleteThreadVector(tempThread);
      //Now delete memoryManagers
      DeleteMemoryManagerMap(tempMemoryManager);
      return(FAIL);
    }

  //Call FDupdate to make sure nothing in it is bad
  UpdateFD();

  return(SETUP_OK);
}

void DCLauncher::ShutdownMemoryManagerMap(std::vector<MemoryManager *> &MM)
{
  //Shutdown and remove the managers
  //clean up memory managers
  for(std::vector<MemoryManager *>::iterator it = MM.begin();
      it != MM.end();
      it++)
    {
      fprintf(stderr,"DCLauncher: Shutdown MemoryManager %s\n",
	      (*it)->GetName().c_str());
      (*it)->Shutdown();
    }
}
void DCLauncher::DeleteMemoryManagerMap(std::vector<MemoryManager *> &MM)
{
  //Shutdown and remove the managers
  //clean up memory managers
  for(std::vector<MemoryManager *>::iterator it = MM.begin();
      it != MM.end();
      it++)
    {
      fprintf(stderr,"DCLauncher: De-allocate MemoryManager %s\n",
	      (*it)->GetName().c_str());
      if((*it) != NULL)
	{	  
	  delete (*it);
	  (*it)=NULL;
	}
    }
  MM.clear();
}

void DCLauncher::StopThreads(std::vector<DCThread *> & T)
{
  bool killTimeout = false;
  time_t currentTime = time(NULL);
  if(currentTime > (lastStopCheck+stopWait))
    {
      killTimeout = true;
      lastStopCheck = currentTime;
    }
  
  std::vector<DCThread *>::iterator it;
  //Why don't iterator based functions like reverse iterators? 
  //Or why can't we have overloaded functions for things like erase?
  for(int i = (T.size()-1);
      ((i >= 0)&&(T.size()>0));
      i--)
    {      
      it = T.begin()+i;
      //Send stop message
      DCMessage::Message message;
      message.SetType(DCMessage::STOP);
      if(*it != NULL)
	{
	  //Decide if we are just going to kill this thread.

	  if(threadStopsSent.find(*it) == threadStopsSent.end())
	    {
	      //This thread isn't in our list of sucessfully created threads
	      //T must be a vector of a group of threads that have failed to be created. 

	      //Send in a stop message
	      (*it)->SendMessageIn(message);

	      //Cancel thread if it was actually created.	      
	      if((*it)->GetID() != 0)
		{
		  fprintf(stderr,"DCLauncher: Killing failed thread %s(%p)\n",
			  (*it)->GetName().c_str(),
			  (*it));
		  pthread_cancel((*it)->GetID());
		}
	    }
	  else 
	    {
	      //This thread is a valid thread and needs to be shutdown. 	      
	      if(killTimeout)		
		{
		  //Send in a stop message
		  (*it)->SendMessageIn(message);

		  //If we have gone long enough, we need to update our thread stops send
		  //and force kill the thread if needed.
		  if((++(threadStopsSent[(*it)]) > maxThreadStopsSent))
		    {
		      fprintf(stderr,"DCLauncher: Killing unresponsive thread %s(%p)(%d of %zu)\n",
			      (*it)->GetName().c_str(),
			      (*it),
			      i,T.size());
		      //Cancel thread
		      pthread_cancel((*it)->GetID());
		      //The thread is dead, we need to delete it. 
		      T.erase(it);
		      i++; //make sure our new iterator is at the correct space since we ust lost an entry
		      UpdateFD();
		      
//		      if(pthread_cancel((*it)->GetID()) != 0)
//			{
//			  //The thread is dead, we need to delete it. 
//			  T.erase(it);
//			  i++; //make sure our new iterator is at the correct space since we ust lost an entry
//			  UpdateFD();
//			}
		    }
		}
	      else
		{
		  //This is too early to kill the threads, so we quick
		  return;
		}	       
	    }
	}
    }
}

void DCLauncher::DeleteThreadVector(std::vector<DCThread *> & T)
{
  //Stop all threads
  StopThreads(T);

  //Delete threads
  while(T.size())
    {
      DeleteThread(T.size()-1,T);
    }
  T.clear();
}

//bool DCLauncher::DeleteThread(std::vector<DCThread*>::iterator it,bool verbose)
bool DCLauncher::DeleteThread(size_t pos)
{
  return DeleteThread(pos,thread);
}
bool DCLauncher::DeleteThread(size_t pos,std::vector<DCThread*> & ThreadVector)
{
  if(pos > ThreadVector.size())
    {
      return false;
    }
  std::vector<DCThread*>::iterator it = ThreadVector.begin()+pos;
  if((*it) == NULL)
    {
      ThreadVector.erase(it);
      UpdateFD();
      return false;
    }

  bool ret = true;
  void * ThreadReturn = NULL;

  //Wait for thread to finish.

  fprintf(stderr,"DCLauncher: Deleting thread %s (%p) \n",
	  (*it)->GetName().c_str(),
	  (void*) (*it)->GetID());
  
  if((*it)->GetID() != 0)
    {
      if(pthread_join((*it)->GetID(),&ThreadReturn) != 0)
	{
	  //if(errno == EDEADLK)
	  //  ret = false;
	}	
    }
  //Thread done;delete thread
  if(ret)
    {
      std::string getName = (*it)->GetName();
      //Remote this from the thread stop sents map
      threadStopsSent.erase(*it);
      delete (*it);
      *it = NULL;
      ThreadVector.erase(it);
      UpdateFD();
    }
  return ret;
}

bool DCLauncher::AddMemoryManager(std::vector<MemoryManager *> &MM)
{
  //Shutdown and remove the managers
  //clean up memory managers
  for(std::vector<MemoryManager *>::iterator it = MM.begin();
      it != MM.end();
      it++)
    {
      //Check if this MM already exists.  (This shouldn't happen)
      bool MMexists = false;
      for(std::vector<MemoryManager *>::iterator subIt = memoryManager.begin();
	  subIt != memoryManager.end();
	  subIt++)
	{	  
	  if(boost::algorithm::iequals((*subIt)->GetName(),
				       (*it)->GetName()))
	    {
	      MMexists = true;
	      subIt = memoryManager.end();
	      break;
	    }
	}
      
      if(!MMexists)
	{
	  //Copy the MM from the temp variable to main MM
	  memoryManager.push_back(*it);
	}
      else
	{	 
	  fprintf(stderr,"DCLauncher: Duplicated MM in AddMemoryManager.\n");
	  fprintf(stderr,"DCLauncher: This SHOULD NOT HAPPEN!\n");
	  fprintf(stderr,"DCLauncher: What did you do?\n");
	  return false;
	}
    }
  //Clear out the temp MM.
  MM.clear();
  return true;
}

bool DCLauncher::AddThread(std::vector<DCThread *> & T)
{
  //This is a simple add.  There is nothing to check.
  for(unsigned int iT = 0; iT < T.size();iT++)
    {
      thread.push_back(T[iT]);
      threadStopsSent[T[iT]]=0;
    }
  //Clear out the temp thread vector.
  T.clear();
  return(true);
}

void DCLauncher::Shutdown()
{
  ShutdownMemoryManagerMap(memoryManager);  
  DeleteThreadVector(thread);
  DeleteMemoryManagerMap(memoryManager);  
}

void DCLauncher::UpdateFD()
{
  //Zero out all previous FD data and start fresh
  //This might take some time, but this isn't called often,
  //so I'd rather play it safe.
  FD_ZERO(&ReadSet);
  FD_ZERO(&WriteSet);
  FD_ZERO(&ExceptionSet);
  FDtoThreadMap.clear();
  FDtoMMMap.clear();
  MaxFDPlusOne = 0;

  //Rebuild the data we just cleared
  size_t threadSize = thread.size();
  //Loop over all threads and get their Message out FDs for select
  //If we eventually need to fill the Write and Exception fd_sets then
  //add that here.
  for(size_t i =0; i < threadSize;i++)
    {
      //Get the the MessageOut FD from thread i
//      if((thread[i] != NULL) &&
//	 (thread[i]->IsRunning()))
      if(thread[i] != NULL)
	{
	  int readFD = thread[i]->GetMessageOutFDs()[0];
	  if(readFD > 0)
	    {
	      //Add the FD to the Read fdset
	      FD_SET(readFD,&ReadSet);
	      //and to the FD thread map 
	      FDtoThreadMap[readFD] = i;
	      //Check to see if it's the biggest.
	      if(readFD > MaxFDPlusOne)
		{
		  MaxFDPlusOne = readFD;	      
		}
	    }      
	}
    }
  //Same thing for MMs
  size_t mmSize = memoryManager.size();
  for(size_t i = 0; i < mmSize;i++)
    {
      int readFD = memoryManager[i]->GetMessageOutFDs()[0];
      if(readFD > 0)
	{
	  //Add the FD to the Read fdset
	  FD_SET(readFD,&ReadSet);
	  //and to the FD thread map 
	  FDtoMMMap[readFD] = i;
	  //Check to see if it's the biggest.
	  if(readFD > MaxFDPlusOne)
	    {
	      MaxFDPlusOne = readFD;	      
	    }
	}    
    }  
  MaxFDPlusOne++;  //Move MaxFD up by one (select convention)
}

const std::map<int,size_t> & DCLauncher::GetThreadFDMap()
{
  return(FDtoThreadMap);
}

const std::map<int,size_t> & DCLauncher::GetMMFDMap()
{
  return(FDtoMMMap);
}

void DCLauncher::GetFDSets(fd_set & retReadSet,
			   fd_set & retWriteSet,
			   fd_set & retExceptionSet,
			   int    & retMaxFDPlusOne,
			   struct timeval & retSleepTime)
{
  retReadSet = ReadSet;
  retWriteSet = WriteSet;
  retExceptionSet = ExceptionSet;
  retMaxFDPlusOne = MaxFDPlusOne;
  retSleepTime = SleepTime;
}


void DCLauncher::ProcessTimeout()
{
  time_t currentTime = time(NULL);
  if(difftime(currentTime,lastUpdateTime) > UpdateTime)
    {
      lastUpdateTime = currentTime;
      std::vector<MemoryManager *>::iterator itMem;
      for(itMem = memoryManager.begin();
	  itMem != memoryManager.end();
	  itMem++)
	{
	  if((*itMem) != NULL)
	    (*itMem)->PrintStatus(0);
	}        
    }
}
