#include "TH1F.h"
#include "TF1.h"
#include "TMath.h"
#include "TLine.h"
#include "TList.h"

Double_t function(Double_t *x, Double_t *par);
TF1 * SPEfit(TH1F * h,Char_t * Name);
