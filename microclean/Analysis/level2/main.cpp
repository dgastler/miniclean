//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//Class includes
#include "PEvent.h"
#include "TMath.h"
#include "Cut_Parameters.h"

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"





using namespace std;


Float_t FindPeak(TH1F * h1)
{
  TF1 * f3 = new TF1("f3","[0]*exp([1]*x) + [2]*exp(-1.0*(x-[3])*(x-[3])/([4]*[4]))",0,700000);
  f3->SetParameters(1e3,-1e-7,2000,2e5,5e4);
  h1->Fit("f3","","",20e3,700e3);
  Float_t ret = f3->GetParameter(2);
  delete f3;
  return(ret);
}



int main(int argc, char ** argv)
{
  if(argc < 4)
    {
      cout << argv[0] << " inFile outfile cutfile" << endl;
      return(0);
    }
  cout << argv[1] << endl;
  Float_t Volts = (1.0/255.0);
  Float_t Seconds = 2E-9;

  TFile *InFile = new TFile(argv[1]);
  TTree *ptree  = (TTree*) InFile->Get("ptree");
  Int_t NEvents = ptree->GetEntries();
  PEvent *pevent = 0;
  ptree->Print();
  ptree->SetBranchAddress("PEvent branch",&pevent);
  cout << endl;
  

  XMLNode xMainNode=XMLNode::openFileHelper(argv[3],"PMML");
  Cut_Parameters * Cuts = new Cut_Parameters;
  Cuts->SetCuts(&xMainNode);
  //All Cuts Loaded!
  cout << "All Cuts Loaded!" << endl;

  Float_t LightLimit = 600000*Volts*Seconds;
  Float_t LightLimitE = 0;
  if(Cuts->W0 <= 1.1)
    {
      LightLimitE = LightLimit;
      if(Cuts->V0 <= 1250)
	LightLimit/= 4;
    }
  else
    LightLimitE = 600000;
  //Raw Histograms
  TH2F * rLightvsRatio   = new TH2F("rLightvsRatio","Raw Energy vs. Fast/Slow",100,0,LightLimitE,1000,0,3);
  TH2F * rLightvsfFast   = new TH2F("rLightvsfFast","Raw Energy vs. Fast",100,0,LightLimitE,1000,0,1);
  TH1F * rRatio          = new TH1F("rRatio","Raw Fast/Slow",50,0,5);
  TH1F * rLight          = new TH1F("rLight","Raw Energy",200,0,LightLimitE);
  TH1F * rfFast          = new TH1F("rfFast","Raw fFast",500,0,1);
  TH1F * rfSlow          = new TH1F("rfSlow","Raw fSlow",500,0,1);
  TH1F * rTB             = new TH1F("rTB","Raw Top bottom difference",500,-1,1);
  

  TH1F * rPMT0TIME       = new TH1F("rPMT0TIME","Raw PMT0 start time",500,0*Seconds,13000*Seconds);
  TH1F * rPMT1TIME       = new TH1F("rPMT1TIME","Raw PMT1 start time",500,0*Seconds,13000*Seconds);
  TH1F * rPMT2TIME       = new TH1F("rPMT2TIME","Raw PMT2 start time",500,0*Seconds,13000*Seconds);
  TH1F * rTOF            = new TH1F("rTOF","Raw time of flight",100,-100*Seconds,100*Seconds);

  TH2F * rINTvsMHPMT0    = new TH2F("rINTvsMHPMT0","Raw Integrated waveform VS. max height PMT0",200,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * rINTvsMHPMT1    = new TH2F("rINTvsMHPMT1","Raw Integrated waveform VS. max height PMT1",200,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * rINTvsMHPMT2    = new TH2F("rINTvsMHPMT2","Raw Integrated waveform VS. max height PMT2",200,0,5000*Volts*Seconds,100,0*Volts,200*Volts);

  TH2F * rINTvsScaledH0   = new TH2F("rINTvsScaledH0","Raw Integrated waveform VS. Scaled max height PMT0",200,0,LightLimit,1000,0,10000*Volts);
  TH2F * rINTvsScaledH1   = new TH2F("rINTvsScaledH1","Raw Integrated waveform VS. Scaled max hight PMT1",200,0,LightLimit,1000,0,10000*Volts);

  TH1F * rMaxHeight0     = new TH1F("rMaxHeight0","Raw MaxHeights0",200,0,1000*Volts);
  TH1F * rMaxHeight1     = new TH1F("rMaxHeight1","Raw MaxHeights0",200,0,1000*Volts);


  TH1F * rPMT0Light      = new TH1F("rPMT0Light","Raw Light in PMT0",100,0,LightLimitE);
  TH1F * rPMT1Light      = new TH1F("rPMT1Light","Raw Light in PMT1",100,0,LightLimitE);
  TH1F * rPMT2Light      = new TH1F("rPMT2Light","Raw Light in PMT2",100,0,40000*Volts*Seconds);



  // Histograms with Cuts applied. 
  TH2F * cLightvsRatio   = new TH2F("cLightvsRatio"," Energy vs. Fast/Slow",100,0,LightLimitE,1000,0,3);
  TH2F * cLightvsfFast   = new TH2F("cLightvsfFast"," Energy vs. Fast",100,0,LightLimitE,1000,0,1);
  TH1F * cRatio          = new TH1F("cRatio"," Fast/Slow",50,0,5);
  TH1F * cLight          = new TH1F("cLight"," Energy",200,0,LightLimitE);
  TH1F * cfFast          = new TH1F("cfFast"," fFast",500,0,1);
  TH1F * cfSlow          = new TH1F("cfSlow"," fSlow",500,0,1);
  TH1F * cTB             = new TH1F("cTB"," Top bottom difference",500,-1,1);
  				     
				     
  TH1F * cPMT0TIME       = new TH1F("cPMT0TIME"," PMT0 start time",500,0*Seconds,13000*Seconds);
  TH1F * cPMT1TIME       = new TH1F("cPMT1TIME"," PMT1 start time",500,0*Seconds,13000*Seconds);
  TH1F * cPMT2TIME       = new TH1F("cPMT2TIME"," PMT2 start time",500,0*Seconds,13000*Seconds);
  TH1F * cTOF            = new TH1F("cTOF"," time of flight",100,-100*Seconds,100*Seconds);
				     
  TH2F * cINTvsMHPMT0    = new TH2F("cINTvsMHPMT0"," Integrated waveform VS. max height PMT0",200,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * cINTvsMHPMT1    = new TH2F("cINTvsMHPMT1"," Integrated waveform VS. max height PMT1",200,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * cINTvsMHPMT2    = new TH2F("cINTvsMHPMT2"," Integrated waveform VS. max height PMT2",200,0,5000*Volts*Seconds,100,0*Volts,200*Volts);
				     
  TH2F * cINTvsScaledH0  = new TH2F("cINTvsScaledH0"," Integrated waveform VS. Scaled max height PMT0",200,0,LightLimit,1000,0,10000*Volts);
  TH2F * cINTvsScaledH1  = new TH2F("cINTvsScaledH1"," Integrated waveform VS. Scaled max hight PMT1",200,0,LightLimit,1000,0,10000*Volts);
				     
  TH1F * cMaxHeight0     = new TH1F("cMaxHeight0"," MaxHeights0",200,0,1000*Volts);
  TH1F * cMaxHeight1     = new TH1F("cMaxHeight1"," MaxHeights0",200,0,1000*Volts);
				     
				     
  TH1F * cPMT0Light      = new TH1F("cPMT0Light"," Light in PMT0",100,0,LightLimitE);
  TH1F * cPMT1Light      = new TH1F("cPMT1Light"," Light in PMT1",100,0,LightLimitE);
  TH1F * cPMT2Light      = new TH1F("cPMT2Light"," Light in PMT2",100,0,40000*Volts*Seconds);

  for(int i = 0; i < NEvents ; i++)
    {
      ptree->GetEntry(i);
      Float_t FastLight = (pevent->fFast(0)*Cuts->W0 + pevent->fFast(1)*Cuts->W1);
      Float_t SlowLight = (pevent->fSlow(0)*Cuts->W0 + pevent->fSlow(1)*Cuts->W1);
      Float_t PMT0Int = (pevent->fSlow(0) + pevent->fFast(0));
      Float_t PMT1Int = (pevent->fSlow(1) + pevent->fFast(1));
      Float_t PMT0Light = PMT0Int*Cuts->W0;
      Float_t PMT1Light = PMT1Int*Cuts->W1;
      Float_t TotalLight = FastLight + SlowLight;
      if(Cuts->PassCuts(pevent))
	{
	  cLightvsRatio->Fill(TotalLight*Volts*Seconds,FastLight/SlowLight);
	  cLightvsfFast->Fill(TotalLight*Volts*Seconds,FastLight/TotalLight);
	  
	  cRatio->Fill(FastLight/SlowLight);
	  cLight->Fill(TotalLight*Volts*Seconds);
	  cfFast->Fill(FastLight/TotalLight);
	  cfSlow->Fill(SlowLight/TotalLight);
	  cTB->Fill((PMT0Light - PMT1Light)/TotalLight);
	  
	  cPMT0TIME->Fill(pevent->Delay(0)*Seconds);
	  cPMT1TIME->Fill(pevent->Delay(1)*Seconds);
	  
	  cINTvsMHPMT0->Fill(PMT0Int*Volts*Seconds,pevent->MaxHeight(0)*Volts);
	  cINTvsMHPMT1->Fill(PMT1Int*Volts*Seconds,pevent->MaxHeight(1)*Volts);
	  
	  cMaxHeight0->Fill(pevent->MaxHeight(0)*Volts);
	  cMaxHeight1->Fill(pevent->MaxHeight(1)*Volts);
	  
	  cPMT0Light->Fill(PMT0Light*Volts*Seconds);
	  cPMT1Light->Fill(PMT1Light*Volts*Seconds);
	  
	  cINTvsScaledH0->Fill(PMT0Int*Volts*Seconds,pevent->MaxHeight(0)*Cuts->W0*Volts);
	  cINTvsScaledH1->Fill(PMT1Int*Volts*Seconds,pevent->MaxHeight(1)*Cuts->W1*Volts);
	  if(Cuts->Type != 1)
	    {
	      cPMT2TIME->Fill(pevent->Delay(2)*Seconds);
	      cTOF->Fill((pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	      cINTvsMHPMT2->Fill(pevent->fFast(2)*Volts*Seconds,pevent->MaxHeight(2)*Volts);
	      cPMT2Light->Fill((pevent->fFast(2) + pevent->fSlow(2))*Volts*Seconds);
	      //	      cout << (PMT0Light - PMT1Light)/TotalLight << endl;
	    }
	  else
	    {
	      cTOF->Fill((pevent->Delay(0) - pevent->Delay(1))*Seconds);
	    }
	}
      
      rLightvsRatio->Fill(TotalLight*Volts*Seconds,FastLight/SlowLight);
      rLightvsfFast->Fill(TotalLight*Volts*Seconds,FastLight/TotalLight);
      
      rRatio->Fill(FastLight/SlowLight);
      rLight->Fill(TotalLight*Volts*Seconds);
      rfFast->Fill(FastLight/TotalLight);
      rfSlow->Fill(SlowLight/TotalLight);
      rTB->Fill((PMT0Light - PMT1Light)/TotalLight);
      
      rPMT0TIME->Fill(pevent->Delay(0)*Seconds);
      rPMT1TIME->Fill(pevent->Delay(1)*Seconds);
      
      rINTvsMHPMT0->Fill(PMT0Int*Volts*Seconds,pevent->MaxHeight(0)*Volts);
      rINTvsMHPMT1->Fill(PMT1Int*Volts*Seconds,pevent->MaxHeight(1)*Volts);
      
      rMaxHeight0->Fill(pevent->MaxHeight(0)*Volts);
      rMaxHeight1->Fill(pevent->MaxHeight(1)*Volts);
      
      rPMT0Light->Fill(PMT0Light*Volts*Seconds);
      rPMT1Light->Fill(PMT1Light*Volts*Seconds);
      
      rINTvsScaledH0->Fill(PMT0Int*Volts*Seconds,pevent->MaxHeight(0)*Cuts->W0*Volts);
      rINTvsScaledH1->Fill(PMT1Int*Volts*Seconds,pevent->MaxHeight(1)*Cuts->W1*Volts);
      if(Cuts->Type != 1)
	{
	  rPMT2TIME->Fill(pevent->Delay(2)*Seconds);
	  rTOF->Fill((pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	  rINTvsMHPMT2->Fill(pevent->fFast(2)*Volts*Seconds,pevent->MaxHeight(2)*Volts);
	  rPMT2Light->Fill((pevent->fFast(2) + pevent->fSlow(2))*Volts*Seconds);
	}
      else
	{
	  rTOF->Fill((pevent->Delay(0) - pevent->Delay(1))*Seconds);
	}
    }
  
  TObjArray Histograms(0);
  
  if(Cuts->Type == 1)//Cobalt Run
    {
      //--------------------------------------------------------------------------
      //Cobalt Peak
      //--------------------------------------------------------------------------
      Int_t xMax = cLight->GetMaximumBin();
      Float_t yMax = cLight->GetMaximum();
      Int_t SigmaR = 0;
      Int_t SigmaL = 0;
      for(int i = 0;i + xMax < cLight->GetNbinsX() ;i++)
	{
	  if(2.0 * cLight->GetBinContent(xMax + i) <= yMax)
	    {
	      SigmaR = xMax + i;
	      break;
	    }
	}
      for(int i = 0;i + xMax > 0;i--)
	{
	  if(2.0 * cLight->GetBinContent(xMax + i) <= yMax)
            {
              SigmaL = xMax + i;
              break;
            }
	}
      Float_t Mean = cLight->GetBinCenter(xMax);
      Float_t Sigma = 0; //(cLight->GetBinCenter(SigmaR) - cLight->GetBinCenter(SigmaL))/2.0;
      if(fabs(Mean - cLight->GetBinCenter(SigmaR)) < fabs(Mean - cLight->GetBinCenter(SigmaL)))
	{
	  Sigma = fabs(Mean - cLight->GetBinCenter(SigmaR));
	}
      else
	{
	  Sigma = fabs(Mean - cLight->GetBinCenter(SigmaL));
	}

      TF1 * f1 = new TF1("f1","gaus",0,cLight->GetBinCenter(cLight->GetNbinsX()) - 1);    
      cLight->Fit("f1","","",Mean - Sigma,Mean+Sigma);

      char numbertemp[30];

      float NewWeight = 0.5*(Cuts->W0 + Cuts->W1) * 122e3/f1->GetParameter(1);
      sprintf(numbertemp,"%f",NewWeight);

      //      sprintf(numbertemp,"%f",1.0);

      xMainNode.getChildNode("Header").getChildNode("Weight",0).updateAttribute(numbertemp,NULL,"W");
      xMainNode.getChildNode("Header").getChildNode("Weight",1).updateAttribute(numbertemp,NULL,"W");
      //      xMainNode.getChildNode("Cuts").getChildNode("TBRatio").updateAttribute("1",NULL,"use");

      
      char * t = xMainNode.createXMLString(true);
      ofstream OUTFILE(argv[3],ios::trunc);
      OUTFILE << t << endl;
      //      cout << endl << t << endl;
      OUTFILE.close();
      free(t);
      
      cout << "# ";

      //Find Run Date
      bool WriteToScreen = false;
      for(int i = 0; argv[1][i] != '\0';i++)
	{
	  char * filetemp = argv[1];
	  if(WriteToScreen)
	    {
	      if(filetemp[i] == '-')
		{
		  WriteToScreen = false;
		}
	      else
		cout << filetemp[i] ;
	    }
	  if(filetemp[i] == '_')
	    {
	      WriteToScreen = true;
	    }
	  
	}
      cout << "  " << Cuts->V0 << "  " << Cuts->V1 <<  "  " <<  NewWeight <<endl;
      cout << endl;
      //--------------------------------------------------------------------------
      //Cobalt Peak
      //--------------------------------------------------------------------------
      //      rTB->Fit(f1);
      //      cout << "!" << f1->GetParameter(1) << "  "  << f1->GetParameter(2) << endl;
      delete f1;
    }

  if(Cuts->Type == 3)
    {
      TF1 * f1 = new TF1("f1","landau");
      cLight->Fit(f1);
      char buffer[100];
      Float_t theta = atof(xMainNode.getChildNode("Header").getChildNode("Angle").getAttribute("T"));
      Float_t Energy = 2 * 2800/(41*41)*(41 - cos(theta*3.14/180)*cos(theta*3.14/180) - cos(theta*3.14/180)*sqrt(40*40 - 1 + cos(theta*3.14/180)*cos(theta*3.14/180)));
      sprintf(buffer,"%3.0f degrees  E = %3.0f Erec = %3.0f ",theta,Energy,f1->GetParameter(1)/1000);
      cLight->SetTitle(buffer);
      cout << "#  " << Energy << "  " << f1->GetParameter(1)/(1000 * Energy) << endl;
      
    }



  Histograms.Add(rLightvsRatio); 
  Histograms.Add(rLightvsfFast);
  Histograms.Add(rRatio); 
  Histograms.Add(rLight)   ;     
  Histograms.Add(rfFast)    ;   
  Histograms.Add(rfSlow)     ;   
  Histograms.Add(rTB)         ;   
  Histograms.Add(rPMT0TIME)    ; 
  Histograms.Add(rPMT1TIME)    ;
  Histograms.Add(rPMT2TIME)     ; 
  Histograms.Add(rTOF);
  Histograms.Add(rINTvsMHPMT0)  ;
  Histograms.Add(rINTvsMHPMT1)  ;
  Histograms.Add(rINTvsMHPMT2)  ;
  Histograms.Add(rPMT0Light)    ;
  Histograms.Add(rPMT1Light)    ;
  Histograms.Add(rPMT2Light);
  Histograms.Add(rMaxHeight0);
  Histograms.Add(rMaxHeight1);
  Histograms.Add(rINTvsScaledH0);
  Histograms.Add(rINTvsScaledH1);


  Histograms.Add(cLightvsRatio);
  Histograms.Add(cLightvsfFast);
  Histograms.Add(cRatio); 
  Histograms.Add(cLight)   ;     
  Histograms.Add(cfFast)    ;   
  Histograms.Add(cfSlow)     ;   
  Histograms.Add(cTB)         ;   
  Histograms.Add(cPMT0TIME)    ; 
  Histograms.Add(cPMT1TIME)    ;
  Histograms.Add(cPMT2TIME)     ;                                
  Histograms.Add(cTOF);
  Histograms.Add(cINTvsMHPMT0)  ;
  Histograms.Add(cINTvsMHPMT1)  ;
  Histograms.Add(cINTvsMHPMT2)  ;
  Histograms.Add(cPMT0Light)    ;
  Histograms.Add(cPMT1Light)    ;
  Histograms.Add(cPMT2Light);
  Histograms.Add(cMaxHeight0);
  Histograms.Add(cMaxHeight1);
  Histograms.Add(cINTvsScaledH0);
  Histograms.Add(cINTvsScaledH1);


  
  TFile OutFile(argv[2],"recreate");
  Histograms.Write();
  cout << endl << endl << endl;
  return(0);
}
