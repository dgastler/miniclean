#include <DCMessage.h>

class ConvertGroup
{
public:
  ConvertGroup()
  {
    runNumber = -3;
    running = false;
    remoteSocketWatchDescriptor = -1;
  }
  int runNumber;
  bool running;
  int remoteSocketWatchDescriptor;
  DCMessage::DCAddress Addr;
  DCMessage::DCAddress MemBlockReader;
  //DCMessage::DCAddress RunControl;
  std::queue<std::string> files;
};
