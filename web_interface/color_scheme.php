<?php
  //General colors
$black='#000000';
$white='#FFFFFF';
$dark_gray='#686868';
$med_gray='#B9B9B9';
$light_gray='#D3D3D3';
$pink='#FF6699';
$light_green='#9DC773';
$bright_red='#CC3333';
$light_bright_green='#33FF85';
$bright_green='#00FF66';
$bright_yellow='#FFFF33';
$red='#CC3333';
$bright_red='#FF001A';
$light_bright_red='#FF3348';
$pale_blue='#669999';
$atomic_tangerine='#FF9966';
$purple='#9966FF';
$light_tang='#FFBB99';
$light_purple='#CCB3FF';
$light_yellow='#FFFF80';
$light_red='#D65C5C';

//Specific color schemes
//link color
$link_color=$black;

//pc/thread/mm remove buttons
$remove_text=$white;
$remove_shadow=$dark_gray;
$light_remove_shadow=$med_gray;
$remove_color=$pink;

//start/stop button text
$s_text=$white;
$s_shadow=$light_gray;

//start/stop button colors
$start_color=$light_green;
$stop_color=$bright_red;

//pc buttons
$pc_text=$dark_gray;
$pc_button_top=$white;
$pc_button_bottom=$med_gray;
$pc_shadow=$black;

//load/display run/add thread dropdown menu
$run_menu_text=$dark_gray;
$run_menu_top=$white;
$run_menu_bottom=$med_gray;
$run_shadow=$black;
$run_label=$dark_gray;

//thread buttons
$t_color=$bright_green;
$t_shadow=$black;

//mm buttons
$mm_color=$pale_blue;
$mm_shadow=$black;

//data boxes
$data_text=$black;
$data_color=$atomic_tangerine;
$data_form=$light_tang;
$data_shadow=$black;

//special thread data boxes (within special thread menu)
$sdata_color=$purple;
$sdata_form=$light_purple;
$sdata_shadow=$black;

//run status box (top right)
$status_text=$dark_gray;
$status_color=$med_gray;
$status_shadow=$black;

//error data box
//if there is nothing at that index in the thread but thread_necessary has 'yes'
$error_text=$black;
$error_color=$red;
$error_form=$light_red;
$error_shadow=$black;

//fix/flag data box
//if there is nothing at that index in the thread but thread_defaults has data
$fix_text=$black;
$fix_color=$bright_yellow;
$fix_form=$light_yellow;
$fix_shadow=$black;

//color scheme for an added thread button
$add_text=$dark_gray;
$add_color=$purple;
$add_shadow=$black;

//color scheme for pc up/down buttons
$up_startgrad=$light_bright_green;
$up_endgrad=$bright_green;
$down_startgrad=$light_bright_red;
$down_endgrad=$bright_red;
?>