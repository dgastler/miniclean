#ifndef __EBPC__
#define __EBPC__

#include <LockedQueue.h>
#include <LockedVectorHash.h>

class EBPC
{
 public:
  EBPC(){};
  virtual ~EBPC(){Clear();};

  enum
  {
    BADVALUE = -1,
    LOCKVALUE = -2    
  };

  //Setup queues and define the size of the EventStore.
  void Setup(uint16_t Size)
  {
      Free.Setup(BADVALUE);
      EventStore.Setup(Size,BADVALUE,LOCKVALUE);
      Write.Setup(BADVALUE);
  };


  //clear queues
  void Clear()
  {
    Free.Clear(true);
    Write.Clear(true);
    EventStore.Clear(true);
  };

  //Shutdown all wait options in all Locked objects
  virtual void Shutdown()
  {
    Free.ShutdownWait();
    Write.ShutdownWait();
    EventStore.ShutdownWait();
    WakeUp();
  };
  
  //Wakeup every Locked object (call after Shutdown())
  virtual void WakeUp()
  {
    Free.WakeUp();
    Write.WakeUp();
    EventStore.WakeUp();
  };


  //=======================================================================
  //Queues
  //=======================================================================
  //Get functions.
  virtual bool GetFree(int32_t &index,bool wait = true)
  {    
    return(Free.Get(index,wait));
  };
  virtual bool GetWrite(int32_t &index,bool wait = true)
  {
    return(Write.Get(index,wait));
  };

  //Add functions
  virtual bool AddFree  (int32_t & Index){return(Free.Add(Index,true));};
  virtual bool AddWrite(int32_t & Index){return(Write.Add(Index,true));};

  //=======================================================================
  //Event Store
  //=======================================================================
  virtual bool GetStoredEvent(uint16_t Event, int32_t &index, bool wait = true)
  {
    return(EventStore.Get(Event,index,wait));
  };
  virtual bool StoreEvent(uint16_t Event, int32_t &index)
  {
    return(EventStore.Put(Event,index,true));
  }
  virtual bool ClearEvent(uint16_t Event)
  {
    return(EventStore.Clear(Event,true));
  }

 private:
  LockedQueue<int32_t> Free;  //queue of indicies of free memory blocks
  LockedVectorHash<int32_t> EventStore; //vector-hash of stored events.
  LockedQueue<int32_t> Write; //queue of indicies to write to disk.
};
#endif
