#include "v1720WFD.h"
#include "xmlHelper.h"

v1720::v1720()
{
  Handle = 0;
  BdType = cvV2718;
  Link = 0;
  BdNum = 0;
  VMEBaseAddress = 0;
  ChannelsConfig = 0;
  MemConfig = 0;
  ROCFPGA = 0;
  Ready = false;
}

v1720::~v1720()
{
  
  //Fix this!  We should close this handle, but we need to know it was set!
  //  CAENVME_End(Handle);
  Channels.clear();
  //clean up XML somethings.
}

int v1720::Start()
{
  int ret = 0;
  if(Ready)
    {
      //Make sure we don't mess up Acquisition control by
      //adding bit 2 to an already on bit2.
      ret += a32d32w(VMEBaseAddress + 0x8100,
		     (AcquisitionControl&0xFFFFFFFB) + 0x4);
    }
  else 
    {
      ret++;
    }
  return(ret);
}

int v1720::Stop()
{
  int ret = 0;
  if(Ready)
    {
      //Keep all bits the same except for bit 2
      ret += a32d32w(VMEBaseAddress + 0x8100,
		     AcquisitionControl&0xFFFFFFFB);
    }
  else 
    {
      ret++;
    }
  return(ret);
}

int v1720::SoftwareTrigger()
{
  int ret = 0;
  if(Ready)
    {
      ret += a32d32w(VMEBaseAddress + 0x8108,0x0);
    }
  else
    {
      ret++;
    }
  return(ret);
}

int v1720::Readout(MemoryBlock &block)
{
  goodBERR = 0;
  badBERR = 0;
  VMEtransfers = 0;
  numberOfEvents = 0;
  TransferredBytes = 0;
  int ret = OK;
  if(Ready)
    {
      //Check out how many events are available
      a32d32r(VMEBaseAddress + 0x812C,numberOfEvents);      

      if(numberOfEvents>0)
	{
	  uint32_t transferSize = 0;
	  for(uint32_t ievent = 0; ievent < numberOfEvents;ievent++)
	    {
	      if(EventSize < (block.allocatedSize - block.dataSize))
		{
		  int CAENret = a32d32w(VMEBaseAddress + 0xEF1C,1);
		  CAENret = a32MBLTr(VMEBaseAddress,
				     block.buffer + block.dataSize,
				     EventSize,
				     transferSize);		  
		  //No error or a BERR terminated transfer
		  if((CAENret == 0) || (CAENret == -1))
		    {
		      block.dataSize += transferSize;
		      TransferredBytes += transferSize;
		    }
		  else
		    {
		      printf("ERROR! in v1720 Readout: %d\n",CAENret);
		    }
		}
	      else
		{
		  ret = BUFFERFULL;
		  numberOfEvents = ievent;
		  break;		 
		}
	    }
	}
    }
  else
    {
      ret = NOTREADY;
    }
  return(ret);
}

bool v1720::TwoPack()
{
  bool ret = true;
  if((0x1 & (ChannelsConfig >> 11)) == 0x1)
    {
      ret = false;
    }
  return(ret);
}
