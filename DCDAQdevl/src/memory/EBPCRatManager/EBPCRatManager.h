#ifndef __EBPCRATMANAGER__
#define __EBPCRATMANAGER__

#include <MemoryManager.h>
#include <xmlHelper.h>
#include <EBPCBlock.h>
#include "RAT/DS/EV.hh"

#include <DCVectorHash.h>
#include <DCQueue.h>

//#define LOCK_EVERYTHING

//class EBPCRATManager : public MemoryManager, public LockedObject
class EBPCRATManager : public MemoryManager
{
 public:
  EBPCRATManager(std::string Name);
  ~EBPCRATManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  //Block access
  EBPCBlock * GetEBPCBlock(uint32_t iEBPC) 
    {
      if(iEBPC < Memory.size())
	{
	  return(&(Memory[iEBPC]));
      }
    return(NULL);
    };  
  //FD access
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetToDiskFDs(){return(ToDisk.GetFDs());};
  const int * GetBadEventFDs(){return(BadEvent.GetFDs());};

  //Shutdown
  virtual void Shutdown()
  {
    //Set to shutdown mode.   Now we can't wait on mutex locks
    //This object
    Lock.ShutdownLockWait();
    //Child objects
    Free.ShutdownWait();
    EventStore.ShutdownWait();
    ToDisk.ShutdownWait();
    BadEvent.ShutdownWait();

    //call wakeup to force everyone out of a mutex condition wait.
    WakeUp();
  };

  //Get the defined bad value
  int32_t GetBadValue(){return(EventStore.GetBadValue());};

  bool AddFree(int32_t &index);

  //ToDisk
  bool GetToDisk(int32_t &index);
  bool AddToDisk(int32_t &index);

  //BadEvent queue
  bool GetBadEvent(int32_t &index);
  bool AddBadEventNOFD(int32_t &index);


  //Vector hash access
  int64_t GetEvent(int64_t Event,int32_t &index,uint64_t &password);
  int64_t SetEvent(int64_t Event,int32_t &index,uint64_t password);
  int64_t ClearEvent(int64_t key, uint64_t password);
     
  //Autopassing functions
  int64_t MoveEventToDiskQueue(int64_t Event,int32_t &index,uint64_t password);
  int64_t MoveEventToBadQueue(int64_t Event,int32_t &index,uint64_t password);
 
  virtual void PrintStatus(int indent =0);

  void WakeUp()
  {
    //THis object
    Lock.WakeUpSignal();
    //child objects
    Free.WakeUp();
    EventStore.WakeUp();
    ToDisk.WakeUp();
    BadEvent.WakeUp();
  }

 private: 
  uint32_t NumEBPCBlocks;
  uint32_t VectorHashSize;
  std::vector<EBPCBlock> Memory;

  LockedObject Lock;

  //Locked queues
  //  DCQueue<int32_t,PipeSelect,LockedObject> Free;  //queue of indicies of fre

  //Not locked queues
  DCQueue<int32_t,PipeSelect,LockedObject> Free;  //queue of indicies of fre
  DCVectorHash<int32_t,LockedObject> EventStore; //vector-hash of stored events.
  DCQueue<int32_t,PipeSelect,LockedObject> ToDisk; //queue of indicies to disk
  DCQueue<int32_t,PipeSelect,LockedObject> BadEvent; //queue of indexes that have collided. 

  void Deallocate();
};

#endif
