#ifdef __RATDS__
#include <FEPusher.h>

void FEPusher::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.GetSocketFD()))
    {
      ProcessNewConnection();
    }
  
  //Process a new respond message
  if(IsReadReady(DRPC_mem->GetRespondFDs()[0]))
    {
      //Get the next event to respond to
      int32_t index;
      if(DRPC_mem->GetRespond(index))
	//Get worked
	{
	  //Check that our index is valid
	  if(index != DRPC_mem->GetBadValue())
	    //Valid index
	    {
	      //Get DRPCBLock at this index
	      RATEVBlock * event = DRPC_mem->GetDRPCBlock(index);
	      int sendReturn = SendDecision(event);
	      if(sendReturn == SendDecisionCode::SENT)
		//everything sent correctly
		{
		  //Clear in Free queue add
		  //		  event->Clear();		  
		  //pass index to free?  WE might want some memory on this.. 
		  if(!DRPC_mem->AddFree(index))
		    {
		      //We are probably shutting down in this case
		      PrintError("Can't add to free queue. DCDAQ shutdown?\n");
		    }
		}
	      else if(sendReturn >= SendDecisionCode::FAILED_CONNECTION_0)
		//Something failed and we should move the index to the bad queue
		{
		  //move to bad queue
		  if(!DRPC_mem->AddBadEvent(event->wfdEventID,index,
					    Level::SEND_FAILED))
		    {
		      //We are probably shutting down in this case
		      PrintError("Can't add to bad queue. DCDAQ shutdown?\n");
		    }
		}
	      else			       
		//You didn't address your new error code! 
		{
		  PrintError("Unknown SendDecisionCode.\n");
		}
	    }
	  else
	    //invalid index
	    {
	      PrintError("Bad event number\n");
	      //We are probably shutting down in this case
	    }
	}
      else
	//Get failed
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a respond event.  DCDAQ shutdown?\n");
	}
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < server.Size();iConnection++)
    {

      //Check for a new message
      if(IsReadReady(server[iConnection].GetMessageFD()))
	{
	  ProcessChildMessage(iConnection);
	}

      //Not really expecting any responses right now
      //Later we might open this up for error checking
      //Check for a new packet 
//      if(IsReadReady(server[iConnection].thread->GetInPacketManager()->GetFullFDs()[0]))
//	{
//	}
      

    }  
}

bool FEPusher::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //Since this thread always needs to be running to notice network
      //connections, we don't pause anything.  We do stop paying attention
      //to the UnProcessed queue though. 
      RemoveReadFD(DRPC_mem->GetRespondFDs()[0]);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //We now need to pay attention to the UnProcessed queue
      AddReadFD(DRPC_mem->GetRespondFDs()[0]);
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}

int FEPusher::SendDecision(RATEVBlock * event)
{
  //create a DRDecisionPacket data struct and fill it
  DRDecisionPacket::Event eventData;
  eventData.wfdEventID = event->wfdEventID;
  eventData.eventID = event->eventID;
  eventData.eventTime = event->eventTime;
  eventData.eventTriggers = event->eventTriggers;
  eventData.reductionLevel = event->reductionLevel;
  //Loop over all the connection and send out the decision
  for(unsigned int iConnection = 0; 
      iConnection < server.Size();
      iConnection++)
    {
      //Get the next free outgoing packet for this connection.
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      server[iConnection].GetOutPacketManager()->GetFree(iDCPacket,true);
      DCNetPacket * packet = server[iConnection].GetOutPacketManager()->GetPacket(iDCPacket);
      //Check for a bad packet return
      if(packet == NULL)
	{
	  char * buffer = new char [100];
	  sprintf(buffer,"Packet manager failed on Connection %s\n",
		  server[iConnection].GetRemoteAddress().GetAddrStr().c_str());
	  PrintError(buffer);
	  delete [] buffer;
	  return(SendDecisionCode::FAILED_CONNECTION_0 + iConnection);
	}
      
      //Fill packet
      if(sizeof(DRDecisionPacket::Event) <= packet->GetMaxDataSize())
	//the packet has enough room in it for the Event data
	{
	  packet->SetType(DCNetPacket::DR_DECISION);
	  packet->SetData((uint8_t*)&eventData,sizeof(eventData),0);
	  server[iConnection].GetOutPacketManager()->AddFull(iDCPacket);
	  packet = NULL;
	}
      else
	//The packet is too small.  Normally we would loop over the data
	//we are sending and use multiple packets, but there is no reason
	//for this with these small bits of data.   So we fail.
	{
	  PrintError("Packet size too small for DRDecision packets.  Fix your XML file!\n");
	}
    }
  return(SendDecisionCode::SENT);
}


#endif
