#include "MemPlots.h"

void MakeMemoryPlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grFEPC1free;
  TGraph * grFEPC1storage;
  TGraph * grFEPC2free;
  TGraph * grFEPC2storage;
  TGraph * grFEPC3free;
  TGraph * grFEPC3storage;
  TGraph * grFEPC4free;
  TGraph * grFEPC4storage;
  TGraph * grDRPCfree;
  TGraph * grDRPCstorage;
  TGraph * grEBPCfree;
  TGraph * grEBPCstorage;

  ss << "select time,free from sc_sens_FEPC1_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC1free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC1_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC1storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_FEPC2_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC2free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC2_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC2storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_FEPC3_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC3free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC3_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC3storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_FEPC4_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC4free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC4_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC4storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_DRPC_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grDRPCfree = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,event_store from sc_sens_DRPC_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grDRPCstorage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_EBPC_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grEBPCfree = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,event_store from sc_sens_EBPC_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grEBPCstorage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  TCanvas * c1 = new TCanvas("c1");
  c1->SetFillColor(kWhite);
  gStyle->SetOptTitle(0);  
  TPad * padFEPC = new TPad("padFEPC","FEPC",0,0,1,0.5);
  TPad * padDRPC = new TPad("padDRPC","DRPC",0,0.5,1,0.75);
  TPad * padEBPC = new TPad("padEBPC","EBPC",0,0.75,1,1.00);

  padFEPC->SetFillColor(kWhite);
  padDRPC->SetFillColor(kWhite);
  padEBPC->SetFillColor(kWhite);

  padFEPC->Draw();
  padDRPC->Draw();
  padEBPC->Draw();


  grFEPC1free->SetMarkerStyle(20);
  grFEPC1storage->SetMarkerStyle(20);

  grFEPC2free->SetMarkerStyle(21);
  grFEPC2storage->SetMarkerStyle(21);

  grFEPC3free->SetMarkerStyle(22);
  grFEPC3storage->SetMarkerStyle(22);

  grFEPC4free->SetMarkerStyle(23);
  grFEPC4storage->SetMarkerStyle(23);

  grDRPCfree->SetMarkerStyle(33);
  grEBPCfree->SetMarkerStyle(33);
  grDRPCstorage->SetMarkerStyle(33);
  grEBPCstorage->SetMarkerStyle(33);


  grFEPC1free->SetMarkerColor(kGreen);
  grFEPC1storage->SetMarkerColor(kBlue);

  grFEPC2free->SetMarkerColor(kGreen);
  grFEPC2storage->SetMarkerColor(kRed);
  
  grFEPC3free->SetMarkerColor(kGreen);
  grFEPC3storage->SetMarkerColor(kViolet);

  grFEPC4free->SetMarkerColor(kGreen);
  grFEPC4storage->SetMarkerColor(kYellow);

  grDRPCfree->SetMarkerColor(kGreen);
  grDRPCstorage->SetMarkerColor(kBlue);

  grEBPCfree->SetMarkerColor(kGreen);
  grEBPCstorage->SetMarkerColor(kBlue);
  
  padFEPC->cd();
  grFEPC1free->GetYaxis()->SetRangeUser(0,50000);
  grFEPC1free->Draw("AP");
  grFEPC2free->Draw("P");
  grFEPC3free->Draw("P");
  grFEPC4free->Draw("P");
  grFEPC1storage->Draw("P");
  grFEPC2storage->Draw("P");
  grFEPC3storage->Draw("P");
  grFEPC4storage->Draw("P");

  padDRPC->cd();
  grDRPCfree->GetYaxis()->SetRangeUser(0,50000);
  grDRPCfree->Draw("AP");
  grDRPCstorage->Draw("P");

  padEBPC->cd();
  grEBPCfree->GetYaxis()->SetRangeUser(0,50000);
  grEBPCfree->Draw("AP");
  grEBPCstorage->Draw("P");

  TLegend * leg = new TLegend(0.15,0.6,0.55,0.85);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  leg->SetBorderSize(0);
  //  leg->Draw("SAME");
  c1->Print("mem.pdf");
  

  delete grFEPC1free;
  delete grFEPC1storage;
  delete grFEPC2free;
  delete grFEPC2storage;
  delete grFEPC3free;
  delete grFEPC3storage;
  delete grFEPC4free;
  delete grFEPC4storage;
  delete grDRPCfree;
  delete grDRPCstorage;
  delete grEBPCfree;
  delete grEBPCstorage;
  delete leg;
  delete c1;
}

void MakeMemoryPlotV2(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grFEPC1free;
  TGraph * grFEPC1storage;
  TGraph * grFEPC2free;
  TGraph * grFEPC2storage;
  TGraph * grFEPC3free;
  TGraph * grFEPC3storage;
  TGraph * grFEPC4free;
  TGraph * grFEPC4storage;
  TGraph * grTotalRate;

  ss << "select time,free from sc_sens_FEPC1_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC1free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC1_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC1storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_FEPC2_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC2free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC2_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC2storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_FEPC3_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC3free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC3_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC3storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,free from sc_sens_FEPC4_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC4free = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,active_stored from sc_sens_FEPC4_MM where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grFEPC4storage = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");


  ss << "select time,value from sc_sens_DMRRTRAW where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  grTotalRate = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  TCanvas * c1 = new TCanvas("c1");
  c1->SetFillColor(kWhite);
  gStyle->SetOptTitle(0);  
  TPad * padFEPC = new TPad("padFEPC","FEPC",0,0,1,0.75);
  TPad * padRate = new TPad("padRate","Rate",0,0.75,1,1);

  padFEPC->SetFillColor(kWhite);
  padRate->SetFillColor(kWhite);

  padFEPC->SetBorderSize(0);
  padRate->SetBorderSize(0);
  padFEPC->SetBorderMode(0);
  padRate->SetBorderMode(0);

  padFEPC->SetTopMargin(0);
  padRate->SetBottomMargin(0);
  padFEPC->SetRightMargin(padFEPC->GetRightMargin()*0.3);
  padRate->SetRightMargin(padRate->GetRightMargin()*0.3);

  padFEPC->Draw();
  padRate->Draw();


  grFEPC1free->SetMarkerStyle(20);
  grFEPC1storage->SetMarkerStyle(20);

  grFEPC2free->SetMarkerStyle(21);
  grFEPC2storage->SetMarkerStyle(21);

  grFEPC3free->SetMarkerStyle(22);
  grFEPC3storage->SetMarkerStyle(22);

  grFEPC4free->SetMarkerStyle(23);
  grFEPC4storage->SetMarkerStyle(23);

  grFEPC1free->SetMarkerColor(kGreen);
  grFEPC1storage->SetMarkerColor(kBlue);

  grFEPC2free->SetMarkerColor(kGreen);
  grFEPC2storage->SetMarkerColor(kRed);
  
  grFEPC3free->SetMarkerColor(kGreen);
  grFEPC3storage->SetMarkerColor(kViolet);

  grFEPC4free->SetMarkerColor(kGreen);
  grFEPC4storage->SetMarkerColor(kTeal);

  grFEPC1free->SetLineColor(kGreen);
  grFEPC1storage->SetLineColor(kBlue);

  grFEPC2free->SetLineColor(kGreen);
  grFEPC2storage->SetLineColor(kRed);
  
  grFEPC3free->SetLineColor(kGreen);
  grFEPC3storage->SetLineColor(kViolet);

  grFEPC4free->SetLineColor(kGreen);
  grFEPC4storage->SetLineColor(kTeal);
  
  padFEPC->cd()->SetLogy();
  grFEPC1free->GetYaxis()->SetRangeUser(10,30000);
  grFEPC1free->GetYaxis()->SetTitle("Memory Blocks");
  grFEPC1free->GetYaxis()->SetTitleOffset(grFEPC1free->GetYaxis()->GetTitleOffset()*1.0);
  grFEPC1free->GetXaxis()->SetTitle("Run Time (hours)");
  grFEPC1free->Draw("AP");
  grFEPC2free->Draw("P");
  grFEPC3free->Draw("P");
  grFEPC4free->Draw("P");
  grFEPC1storage->Draw("P");
  grFEPC2storage->Draw("P");
  grFEPC3storage->Draw("P");
  grFEPC4storage->Draw("P");
  TLegend * leg = new TLegend(0.11,0.55,0.45,0.85);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  leg->AddEntry(grFEPC1free,"FEPC# Free","p");
  leg->AddEntry(grFEPC1storage,"FEPC1 Storage","p");
  leg->AddEntry(grFEPC2storage,"FEPC2 Storage","p");
  leg->AddEntry(grFEPC3storage,"FEPC3 Storage","p");
  leg->AddEntry(grFEPC4storage,"FEPC4 Storage","p");
  leg->Draw("SAME");

  padRate->cd();  
  grTotalRate->SetMarkerColor(kRed);
  grTotalRate->SetMarkerStyle(33);
  grTotalRate->GetYaxis()->SetRangeUser(-100,2500);
  grTotalRate->GetYaxis()->SetLabelSize(grTotalRate->GetYaxis()->GetLabelSize()*3);
  grTotalRate->GetYaxis()->SetTitleSize(grTotalRate->GetYaxis()->GetTitleSize()*3);
  grTotalRate->GetYaxis()->SetTitleOffset(grTotalRate->GetYaxis()->GetTitleOffset()*0.5*2.0/3.0);
  grTotalRate->GetYaxis()->SetTitle("Event Rate (hz)");
  grTotalRate->GetXaxis()->SetLabelColor(kWhite);
  grTotalRate->GetXaxis()->SetTitleColor(kWhite);
  grTotalRate->Draw("AP");

  c1->Print("mem2.pdf");
  

  delete grFEPC1free;
  delete grFEPC1storage;
  delete grFEPC2free;
  delete grFEPC2storage;
  delete grFEPC3free;
  delete grFEPC3storage;
  delete grFEPC4free;
  delete grFEPC4storage;
  delete c1;
}

