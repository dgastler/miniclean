#ifndef __V1720FAKEREADOUT__
#define __V1720FAKEREADOUT__

#include <map>
#include <string>

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <cstdlib> //for getenv()
#include <cstring> //for memcpy()
#include <sys/stat.h> //for file stats

class v1720FakeReadout : public DCThread 
{
 public:
  v1720FakeReadout(std::string Name);
  ~v1720FakeReadout();
  virtual bool Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:
  //Input fake data file info and file decriptor
  std::string InFileName;
  FILE * InFile;

  //Statistics
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;
  time_t lastTime;
  time_t currentTime;

  //Loaded fake data
  uint32_t FakeWFDSize;
  uint32_t * FakeWFD;  //all fake data pointer
  std::vector<uint32_t *> FakeEvent;   //Pointer to individual fake events in FAKEWFD
  unsigned int FakeEventCount;
  uint32_t EventCountOffset;

  //Event rate control variables.  (passed on to DCThread
  useconds_t Period; //Period we ask for
  void SetPeriod(useconds_t _Period);

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message &message);
  //Handle select timeout
  virtual void ProcessTimeout();

  //Current event index
  uint32_t iEvent;
  //Single loop tranfer bool
  bool MemoryLoop; // Controls if we keep sending all the events or we 

  //Memory manager access and opperations
  uint32_t EventsPerBlock;
  uint32_t LoopOffset;
  MemoryBlock * block;
  int32_t blockIndex;
  MemoryBlockManager * Mem; // Memory access

  //Event ID
};
#endif
