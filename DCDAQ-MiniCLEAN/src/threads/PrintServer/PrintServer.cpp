#include <PrintServer.h>

PrintServer::PrintServer(std::string Name)
{
  Ready = false;
  SetType("PrintServer");
  SetName(Name);
  typeMap[DCMessage::ERROR] = &PrintServer::PrintError;
  typeMap[DCMessage::STATISTIC] = &PrintServer::PrintStats;
  typeMap[DCMessage::TEXT]  = &PrintServer::PrintText;
  typeMap[DCMessage::WARNING]  = &PrintServer::PrintText;
}
PrintServer::~PrintServer()
{
}

void PrintServer::ThreadCleanUp()
{
  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  
  for(size_t i = 0; i < routes.size();i++)
    {
      //Set message to unregister
      message.SetType(DCMessage::UNREGISTER_TYPE);
      message.SetDestination(routes[i].route); 
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(DCMessage::GetTypeName(routes[i].type));
      message.SetData(&(data[0]),data.size());
      //route it to it's DCDAQ command list
      SendMessageOut(message);
    }
}

bool PrintServer::Setup(xmlNode * SetupNode,
		       std::vector<MemoryManager*> &/*MemManager*/)
{

  //Add the local session to the print status
  sRoute route;

  if(NULL == FindSubNode(SetupNode,"NOAUTOCONFIG"))
    {
      //Errors
      route.type = DCMessage::ERROR;
      route.route.Set();//blank
      routes.push_back(route);
      //stats
      route.type = DCMessage::STATISTIC;
      route.route.Set();//blank
      routes.push_back(route);
      route.type = DCMessage::TEXT;
      route.route.Set();//blank
      routes.push_back(route);
    }

  //Check if we are registering ourselves for other DCDAQ sessions
  xmlNode * remoteNode = NULL;
  if((remoteNode = FindSubNode(SetupNode,"REMOTE")) != NULL)
    {      
      //Get the IP address of the remote print stats
      size_t routeNodes = NumberOfNamedSubNodes(remoteNode,"ROUTE");
      for(size_t i =0 ; i < routeNodes;i++)
	{
	  xmlNode * routeNode = FindIthSubNode(remoteNode,
					       "ROUTE",
					       i);
	  //Get the destination address and route type
	  if(routeNode != NULL)
	    {
	      std::string IP;
	      std::string type;
	      int goodNode = 0;
	      goodNode += GetXMLValue(routeNode,
				      "TYPE",
				      GetName().c_str(),
				      type);
	      goodNode += GetXMLValue(routeNode,
				      "ADDR",
				      GetName().c_str(),
				      IP);
	      //If there were no errors, set up the route
	      if(goodNode == 0)
		{
		  route.type = DCMessage::GetTypeFromName(type.c_str());
		  route.route.Set(DCMessage::DCAddress::BroadcastName,IP);
		  routes.push_back(route);
		}
	    }
	}

    }

  std::vector<std::string> printStatements;  

  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  for(size_t i = 0; i < routes.size();i++)
    {
      message.Clear();

      //Set message to unregister
      message.SetType(DCMessage::REGISTER_TYPE);
      message.SetDestination(routes[i].route); 
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(DCMessage::GetTypeName(routes[i].type));
      message.SetData(&(data[0]),data.size());

      
      std::string buffer("Routing ");
      buffer += DCMessage::GetTypeName(routes[i].type);
      buffer += " from ";
      buffer += routes[i].route.GetStr();
      buffer += " to this session.";
      printStatements.push_back(buffer);
      
      SendMessageOut(message);
    }
  for(size_t i = 0; i < printStatements.size();i++)
   {
     Print(printStatements[i].c_str());
   }
  Ready = true;
  return true;
}

bool PrintServer::ProcessMessage(DCMessage::Message &message)
{
  std::map<DCMessage::DCMessageType,void (PrintServer::*)(DCMessage::Message & message)>::iterator it;
  it = typeMap.find(message.GetType());
  if(it != typeMap.end())
    {
      (*this.*(it->second))(message);
      return true;
    }
  return false;
}

void PrintServer::PrintStats(DCMessage::Message &message)
{
  return;
  std::stringstream ss;
  DCtime_t timeTemp = message.GetTime();
  StatMessage::StatBase ssBase;
  if(!message.GetData(ssBase))
    return;
  if((ssBase.type == StatMessage::EVENT_RATE)||
     (ssBase.type == StatMessage::PACKET_RATE)||
     (ssBase.type == StatMessage::DATA_RATE)||
     (ssBase.type == StatMessage::REDUCTION_RATE)||
     (ssBase.type == StatMessage::BAD_EVENT_RATE)||
     (ssBase.type == StatMessage::READOUT_RATE))
    {
      StatMessage::StatRate ssRate;
      if(!message.GetData(ssRate))
	return;           
      ss << GetTime(&timeTemp) << " "
	 << message.GetSource().GetStr() << " "
	 << StatMessage::GetTypeName(ssRate.sBase.type);
      if(ssRate.sInfo.subID != SubID::BLANK)
	{
	  ss << " " << SubID::GetTypeName(ssRate.sInfo.subID);
	}
      if(ssRate.sInfo.level != Level::BLANK)
	{
	  ss << " " << Level::GetTypeName(ssRate.sInfo.level);
	}
      ss << " " 
	 << double(ssRate.count)/double(ssRate.sInfo.interval) 
	 << Units::GetTypeName(ssRate.units) << "per second" << std::endl;
    }
  else if((ssBase.type == StatMessage::OCCUPANCY)||
	  (ssBase.type == StatMessage::BASELINE)||
	  (ssBase.type == StatMessage::ZLECOUNT))
    {
      StatMessage::StatFloat ssFloat;
      if(!message.GetData(ssFloat))
	return;
      ss << GetTime(&timeTemp) << " "
	 << message.GetSource().GetStr() << " "
	 << StatMessage::GetTypeName(ssFloat.sBase.type);
      if(ssFloat.sInfo.subID != SubID::BLANK)
	{
	  ss << " " << SubID::GetTypeName(ssFloat.sInfo.subID);
	}
      if(ssFloat.sInfo.level != Level::BLANK)
	{
	  ss << " " << Level::GetTypeName(ssFloat.sInfo.level);
	}
      ss << " " 
	 << double(ssFloat.val) << std::endl;
    }
  printf("%s\n",ss.str().c_str());    
}

void PrintServer::PrintError(DCMessage::Message &message)
{
  //Get message source
  DCMessage::DCAddress address = message.GetSource();
  std::string sourceString("");
  DCtime_t timeTemp = message.GetTime();
  sourceString += GetTime(&timeTemp);
  sourceString += "( ";
  sourceString += address.GetStr();
  sourceString += " )";

  
  //Get message time
  DCMessage::SingleUINT64 singleUINT64;
  if(message.GetDataStruct(singleUINT64))
    {
      printf("%s : ERROR: %s\n",
	     sourceString.c_str(),
	     singleUINT64.text
	     );
    }
  else
    {
      printf("%s : ERROR!\n",
	     sourceString.c_str());
    }
}

void PrintServer::PrintText(DCMessage::Message &message)
{
  //Get message source
  DCMessage::DCAddress address = message.GetSource();
  DCtime_t timeTemp = message.GetTime();	        
  std::string sourceString("");
  sourceString += GetTime(&timeTemp);
  sourceString += "( ";
  sourceString += address.GetStr();
  sourceString += " )";

  
  //Get message time
  DCMessage::DCString dcString;
  std::string text;
  std::vector<uint8_t> data = message.GetData();
  if(dcString.GetData(data,
		      text))
    {
      printf("%s : %s\n",
	     sourceString.c_str(),
	     text.c_str()
	     );
    }
}
