#ifndef __DCADDRESS__
#define __DCADDRESS__

#include <string>
#include <netHelper.h>
#include <xmlHelper.h>
#include <boost/algorithm/string/predicate.hpp>

namespace DCMessage
{
  class DCAddress
  {
  public:
    //==================================================================================
    //==========Constructors/destructors
    //==================================================================================
    DCAddress(){Clear();};    
    DCAddress(DCAddress const & rhs){CopyObj(rhs);};    

    DCAddress(std::string const & _name,
	      struct sockaddr const *_addr = NULL,
	      size_t _addrSize=0){Set(_name,_addr,_addrSize);};
    DCAddress(std::string const &_name,std::string const & _addr){Set(_name,_addr);};
    DCAddress & operator=(DCAddress const &rhs){CopyObj(rhs);return *this;};

    ~DCAddress(){Clear();};    

    //Streaming
    int MakeStream(std::vector<uint8_t> & stream) const;        
    void LoadStream(uint8_t * streamSource,unsigned int size);

    //==================================================================================
    //==========Comparison
    //==================================================================================
    bool operator==(DCAddress const &rhs)const;
    bool operator!=(DCAddress const &rhs)const{return !(operator==(rhs));};
    bool operator() (DCAddress const & lhs, DCAddress const & rhs) const; //Used for less than for map sorting
    
    //==================================================================================
    //==========Set address
    //==================================================================================
    //Set address from string and struct
    bool Set(std::string const & _name = BroadcastName,
	     struct sockaddr const * _addr = NULL,
	     size_t sockaddrSize = 0);
    //set address from string and string
    bool Set(std::string const & _name,std::string const & _addr);
    //set address from xml node
    bool Set(xmlNode * setupNode);

    void SetName(std::string const & _name);
    void SetAddr(struct sockaddr const * _addr,
		 size_t sockaddrSize);
    void SetAddr(std::string const & _addr);
    void SetPort(uint16_t _port);
    int16_t GetPort();

    //Set address to broadcast
    void SetBroadcast(){Set(BroadcastName,NULL);((struct sockaddr *) addrMem)->sa_family = AF_MAX; };
    
    //Clear the address
    void Clear();
    void ClearAddr();
    void ClearName();
    //==================================================================================
    //==========Address access and special access
    //==================================================================================
    //Get address info
    //    std::string const & GetName(){return name;} const;
    std::string const & GetName() const {return name;};
    std::string GetAddrStr() const;
    struct sockaddr const * GetAddr() const {return (struct sockaddr *)addrMem;};
    size_t GetAddrSize() const;
    std::string GetStr() const;

    //Special addresses;    
    bool IsBlank() const;
    bool IsDefaultAddr() const;
    bool IsBroadcastAddr() const;
    bool IsEqualAddr(DCMessage::DCAddress const & addr) const;

    //Internal default and broadcast names
    static const std::string BroadcastName;

  private:
    void CopyObj(DCAddress const & rhs);
    //Address data
    std::string name;    
    uint8_t addrMem[sizeof(sockaddr_un)]; //sockaddr_un is the largest struct of the three (sockaddr_in,sockaddr_in6,sockaddr_un)
    //For broadcast addrMem has family AF_MAX
    //For default addrMem has family AF_UNSPEC

  };
}
#endif
