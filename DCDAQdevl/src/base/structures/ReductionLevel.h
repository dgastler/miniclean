#ifndef __REDUCTIONLEVEL__
#define __REDUCTIONLEVEL__
//For reductionLevel
namespace ReductionLevel 
{
  enum
  {
    UNKNOWN            = 0,
    CHAN_FULL_WAVEFORM ,//= 1
    CHAN_ZLE_WAVEFORM  ,//= 2
    CHAN_ZLE_INTEGRAL  ,//= 3
    CHAN_PROMPT_TOTAL  ,//= 4
    EVNT_FULL_WAVEFORM ,//= 5
    EVNT_ZLE_WAVEFORM  ,//= 6
    EVNT_ZLE_INTEGRAL  ,//= 7
    EVNT_PROMPT_TOTAL  ,//= 8
    COUNT               //Always equals the count
    //NEVER PUT THINGS AFTER COUNT!
    //NEVER EVER!
  };
}
#endif
