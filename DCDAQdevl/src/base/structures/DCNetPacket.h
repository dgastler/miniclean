#ifndef __DCNETPACKET__
#define __DCNETPACKET__

//For data types
#include <iostream>
#include <cstdio>

//For struct timeval
#include <sys/time.h>

//Network read/write helper functions
#include <netHelper.h>

//#define DCNETPACKET_HEADER_SIZE 5
#define DCNETPACKET_HEADER_SIZE 8
#define DCNETPACKET_HEADER_WORD 0xA7

#define DCNETPACKET_HEADER_WORD_POS 0
#define DCNETPACKET_HEADER_TYPE_POS 1
#define DCNETPACKET_HEADER_SIZE_POS1 2
#define DCNETPACKET_HEADER_SIZE_POS2 3
#define DCNETPACKET_HEADER_PACKETNUMBER_POS 4

#define SELECT_TIMEOUT 5

class DCNetPacket
{
 public:
  DCNetPacket(int16_t packetDataSize = 1024)
    {
      PacketData = NULL;
      Allocate(packetDataSize);
    };
  ~DCNetPacket() {Deallocate();};

  enum 
  {
    //Packet Types
    BAD_PACKET = 0,
    ECHO = 1,
    FULLCAEN_WAVEFORMS = 2,
    FULLRAT_WAVEFORMS = 3,
    INTEGRATED_WINDOWS_FULL_PACKET = 4,
    INTEGRATED_WINDOWS_PARTIAL_PACKET = 5,
    DCMESSAGE = 6,
    DCMESSAGE_PARTIAL = 7,
    DR_DECISION = 8,    
    EB_INTEGRATED_WINDOWS_FULL_PACKET = 9,
    EB_INTEGRATED_WINDOWS_PARTIAL_PACKET = 10,
    EB_ZLE_WAVEFORMS_FULL_PACKET = 11,
    EB_ZLE_WAVEFORMS_PARTIAL_PACKET = 12,
    EB_PROMPT_TOTAL_FULL_PACKET = 13,
    EB_PROMPT_TOTAL_PARTIAL_PACKET = 14,
    SHUTDOWN = 15
  };

  enum
  {
    //Errors
    PACKET_TIME_OUT         = 0,
    BAD_ALLOCATION          = -1,
    BAD_PACKET_TYPE         = -2,
    PACKET_TOO_LARGE        = -3,
    PACKET_DATA_READ_SIZE   = -4,
    PACKET_BAD_HEADER       = -5,
    PACKET_HEADER_READ_SIZE = -6,
    PACKET_READ_ERROR       = -7
  };



  uint8_t GetType() {if(Data){return(*Type);}else{return(BAD_PACKET);}};
  void SetType(uint8_t type) {if(Data){*Type = type;}};

  const uint8_t & GetPacketNumber() {return(*PacketNumber);};
  const uint16_t & GetSize() {return(DataSize);};
  const uint16_t & GetMaxDataSize(){return(MaxDataSize);};
  uint8_t * GetData() {return(Data);};
  int SetData(uint8_t * data,uint32_t size,uint8_t packetNumber = 0);

  int SendPacket(int socketFD,
		 time_t seconds = 0,
		 suseconds_t useconds = SELECT_TIMEOUT);
  int ListenForPacket(int socketFD,
		      int & error_return,
		      time_t seconds = 0,
		      suseconds_t useconds = SELECT_TIMEOUT);
  void ClearPacket()
  {
    DataSize = 0;
    *Type = BAD_PACKET;
    *PacketNumber = 0;
  };

  void PrintHeader(FILE * fd = stdout);

 private:
  int Deallocate();
  int Allocate(uint32_t packetDataSize);


  uint16_t DataSize; //size of used data in bytes.
  uint16_t MaxDataSize; //Size of packet less the header
  uint16_t MaxPacketSize; //Size of entire packet in bytes in memory



  uint8_t * PacketData;  //Pointer to packet data.  This is the read pointer
  
  uint8_t *HeaderWord; //Position of the header word
  uint8_t *Type; //Packet Type
  uint8_t *PacketSize; //Size of entire packet in bytes for this packet (first of two 8bit values)
  uint8_t *PacketNumber;
  uint8_t * Data;

};

#endif
