#include <iostream>
#include <math.h>

#include "Event.h"
//#include "Riostream.h"

Int_t Find_Waveform_Maximum(Int_t ChannelSize, Float_t * Y);
Int_t Find_Waveform_Start(Int_t ChannelSize, Float_t * Y);
Float_t MergeWaveforms(Int_t ChannelSize, Float_t * Ya,Float_t *Yb,Float_t Y_Offset,Float_t Gain);
Float_t Offset(Event * event,Int_t Channel,Int_t Length,Float_t * Sigma);
Bool_t CopyWaveform(Event * event,Int_t Channel,Float_t * waveform,Int_t Length,Float_t offset);

