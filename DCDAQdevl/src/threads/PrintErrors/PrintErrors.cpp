#include <PrintErrors.h>

PrintErrors::PrintErrors(std::string Name)
{
  Ready = false;
  SetType("PrintErrors");
  SetName(Name);  
}
PrintErrors::~PrintErrors()
{
  //Create a DC id register data type
  DCMessage::DCTypeID typeUnRegisterMessage;
  //UnRoute DCMessage::ERROR messages to this thread
  std::vector<uint8_t> messageData;
  messageData = typeUnRegisterMessage.SetData(DCMessage::ERROR,
					      GetName());
  
  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  message.SetType(DCMessage::UNREGISTER_TYPE);
  message.SetDestination(); //No destination means that DCMessageProcessor 
                            //route it to it's DCDAQ command list
  message.SetData(&messageData[0],messageData.size());
  SendMessageOut(message,true);
}

bool PrintErrors::Setup(xmlNode * /*SetupNode*/,
			std::map<std::string,MemoryManager*> &/*MemManager*/)
{
  //Create a DC id register data type
  DCMessage::DCTypeID typeRegisterMessage;
  //Route DCMessage::ERROR messages to this thread
  std::vector<uint8_t> messageData;
  messageData = typeRegisterMessage.SetData(DCMessage::ERROR,
					    GetName());
  
  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  message.SetType(DCMessage::REGISTER_TYPE);
  message.SetDestination(); //No destination means that DCMessageProcessor 
                            //route it to it's DCDAQ command list
  message.SetData(&messageData[0],messageData.size());
  SendMessageOut(message,true);
  Ready = true;
  return(true);
}

bool PrintErrors::ProcessMessage(DCMessage::Message &message)
{
  if(message.GetType() == DCMessage::ERROR)
    {
      //Get message source
      std::string name;
      struct in_addr addr;
      message.GetSource(name,addr);

      //Get message time
      DCMessage::DCtime_t timeTemp = message.GetTime();	        
      DCMessage::SingleUINT64 singleUINT64;
      if(message.GetDataStruct(singleUINT64))
	{
	  printf("%s (%s): ERROR: %s\n",
		 GetTime(&timeTemp).c_str(),
		 name.c_str(),
		 singleUINT64.text
		 );
	}
      else
	{
	  printf("%s (%s): ERROR!\n",
		 GetTime(&timeTemp).c_str(),
		 name.c_str());
	}
      return(true);
    }
  return(false);
}

