#include <v1720FakeReadout.h>

void  v1720FakeReadout::MainLoop()
{
  if(blockIndex == FreeFullQueue::BADVALUE)
    {
      //Get new memory block
      Mem->GetFree(blockIndex,true);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = Mem->GetBlock(blockIndex);
	  block->dataSize = 0;
	}
    }
  
  if(blockIndex != FreeFullQueue::BADVALUE)
    {
      while(true)
	{
	  uint32_t * event = FakeWFDEvent[iWFDEvent];//get event
	  uint32_t eventSize = event[0]&0x0FFFFFFF;//get this event's size	

	  if(((block->dataSize) + eventSize) <= (block->allocatedSize))
	    {	   	    
	      //Get a pointer to the part of buffer we are going to write to.
	      uint32_t * bufferPointer = (uint32_t * )(block->buffer + 
						       block->dataSize);
	      //Copy from event to bufferPointer eventsize*4 bytes.
	      memcpy(bufferPointer,event,eventSize<<2);

	      //Calculate the real event number
	      uint32_t RealEventID = (event[2]&0xFFFFFF) + EventCountOffset;
	      //Write lowest 8 bits of real event number to header
	      bufferPointer[1] = event[1] & 0xFFFF00FF;
	      bufferPointer[1] = ((RealEventID & 0xFF)<<8) | bufferPointer[1];
	      //Set the WFD event number and increment counter
	      uint32_t WFDID = (event[1]&0xF8000000)>>27;
	      bufferPointer[2] = 0xFFFFFF & WFDEventID[WFDID];
	      WFDEventID[WFDID] +=1;
	      
	      bufferPointer[3] = 0x7FFFFFFF&WFDFakeTime[WFDID];
	      WFDFakeTime[WFDID] += uint32_t(DesiredEventPeriod * 62.5); //Convert to 62.5Mhz clock
	      
	      //increment this time period's number of events
	      TimePeriodEventCount++;

	      //Set block's data size to include this event
	      block->dataSize += eventSize;		  
	      //Move to the next event.
	      iWFDEvent++;
	      //If we are at the end of our loaded events go back to zero.
	      if(iWFDEvent >= FakeWFDEvent.size())
		{		
		  EventCountOffset+=FakeEventCount;
		  if(MemoryLoop)
		    {
		      iWFDEvent = 0;
		    }
		  else
		    {
		      printf("v1720FakeReadout:   Sent all wfd events (%d)\n",iWFDEvent);
		      Loop = false;
		      
		      RemoveReadFD(Mem->GetFreeFDs()[0]);
		      SetupSelect();
		      break;
		    }
		}
	    } //Add event
	  else
	    {
	      break;
	    }	      
	}//Fill block
      //Pass off this full block.
      Mem->AddFull(blockIndex);
    }
}

void  v1720FakeReadout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > TimePeriod)
    {
      //In useconds
      double effectivePeriod = 1E6*(currentTime - lastTime);
      if(TimePeriodEventCount > 0)
	{
	  effectivePeriod *= WFDsPerEvent;
	  effectivePeriod /= TimePeriodEventCount;
	}
      //Update the period used to try to get the correct period
      InternalPeriod = ceil(InternalPeriod*(DesiredEventPeriod/effectivePeriod));
      std::stringstream ss;
      ss << "Updating fake V1720 internal period to " 
	 << InternalPeriod  << "micro-seconds";
      Print(ss.str());
      if(Loop)
	{
	  LoadEventRate();	
	}
      
      //Setup next time
      lastTime = currentTime;
      TimePeriodEventCount = 0;
    }
  if(InternalPeriod!=0)
    {
      MainLoop();
    }
}
