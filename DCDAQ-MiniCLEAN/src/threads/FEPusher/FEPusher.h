#ifdef __RATDS__

#ifndef __FEPUSHER__
#define __FEPUSHER__
/*
This thread takes full events with reduced information and makes a decision
about what data will be saved. 
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <DRPCRatManager.h>

#include <ServerConnection.h>
#include <Connection.h>
#include <DRDecisionPacket.h>

namespace SendDecisionCode
{
  enum
  {
    SENT  = 0,
    //Failed connection errors should always be last
    //this is so we can check for error > FAILED_CONNECTION_0
    FAILED_CONNECTION_0 = 1,
    FAILED_CONNECTION_1 = FAILED_CONNECTION_0 + 1,
    FAILED_CONNECTION_2 = FAILED_CONNECTION_1 + 1,
    FAILED_CONNECTION_3 = FAILED_CONNECTION_2 + 1,
    FAILED_CONNECTION_4 = FAILED_CONNECTION_3 + 1,
    FAILED_CONNECTION_5 = FAILED_CONNECTION_4 + 1,
    FAILED_CONNECTION_6 = FAILED_CONNECTION_5 + 1,
    FAILED_CONNECTION_7 = FAILED_CONNECTION_6 + 1    
  };
}

class FEPusher : public DCThread 
{
 public:
  //==============================================================
  //FEPusher.cpp
  //==============================================================
  FEPusher(std::string Name);
  ~FEPusher(){};
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  void PrintStatus();
  //==============================================================
  //FEPusher_Run.cpp
  //==============================================================
  virtual void MainLoop();
 private:

  //==============================================================
  //FEPusher_Run.cpp
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout(){};
  virtual void ThreadCleanUp();

  //==============================================================
  //FEPusher_Processing.cpp
  //==============================================================
  int SendDecision(RATEVBlock * event);

  //==============================================================
  //FEPusher_Net.cpp
  //==============================================================
  bool ProcessNewConnection();
  void DeleteConnection(unsigned int iConn);
  virtual void ProcessChildMessage(size_t iConn);

  //==============================================================
  //Network connection data members
  //==============================================================
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  ServerConnection server;

  //==============================================================
  //Memory manager interface
  //==============================================================
  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;

};
#endif
#endif
