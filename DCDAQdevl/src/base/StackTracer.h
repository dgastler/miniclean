#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>

#include <cxxabi.h>

/* Obtain a backtrace and print it to stdout. */
void print_trace();
