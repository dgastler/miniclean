#include <RATWrite.h>

bool RATWrite::OpenFile()
{
  bool ret = true;

  //Check for the special case of being out of subruns
  if(SubRunNumber > SubRunNumberMax)
    {
      DCMessage::Message message;      
      //Send a message back to the master and stop the inner run loop
      printf("Max sub runs reached. Shutting down DAQ\n");
      message.SetType(DCMessage::STOP);
      SendMessageOut(message,true);
      return(false);
    }


  char * buffer = new char[1000];
  //Construct the current filename
  sprintf(buffer,"run_%06d_%05d.root",GetRunNumber(),SubRunNumber); 
  OutFile = TFile::Open(buffer,"RECREATE");
  if(!OutFile)
    {
      ret = false;
    }
  else
    {
      //Store the new filename
      OutFileName.assign(buffer);
      printf("Opening file: %s\n",OutFileName.c_str());
      Tree = new TTree("T", "RAT Tree"); // create a new TTree
      //Make a branch in the TTree for the branchDS
      Tree->Branch("ds", BranchDS->ClassName(), &BranchDS, 32000, 99);
      Tree->SetAutoSave(Autosave);

      //Set the subevent number to 1
      SubEventNumber = 1;
      ret = true;
    }
  delete [] buffer;
  if(ret)
    {
      failedFileOpens = 0;
    }
  return(ret);
}

bool RATWrite::CloseFile()
{
  bool ret = false;
  if(OutFile)
    {
      printf("Closing file: %s\n",OutFileName.c_str());
      OutFile->cd();  //Make sure we are in the correct ROOT directory
      //Write trees
      Tree->Write();
      //Write files and close
      OutFile->Write();
      OutFile->Close();
      //Reset our pointers (we can't delete these, but I don't knwo why)
      Tree = NULL;
      delete OutFile;
      OutFile = NULL;
      ret = true;

    }
  return(ret);
}

