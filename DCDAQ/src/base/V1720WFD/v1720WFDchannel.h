#ifndef __V1720_CHANNEL__
#define __V1720_CHANNEL__

//v1720channel struct for CAEN V1720 WFD channels
//Holds channel specific registers.

struct v1720channel
{
  
  //IDs
  uint32_t ChannelID; //ID on this WFD (0-7)
  uint32_t PMTID;  //ID of the PMT digitized


  //==============================================================================
  //Zero suppresion registers
  //Assuming zero length encoding for zero suppression!
  //get the V1720 manual if you want a description of both. 
  //==============================================================================
  //Channel Zero suppression threshold.
  //  bit 31:  0/1 sets (+/-) logic
  //  bits 11-0: 12bit comparison value for good/skip 
  uint32_t ZS_THRESH;  //0x1n24
  //Channel Zero suppression windows
  //  bits 15-00: Number of samples to go back after a crossing of the ZLE threshold.
  //  bits 31-16: Number of sampels to keep after a crossing failure.
  uint32_t ZS_NSAMP; //0x1n28
  

  //==============================================================================
  //Internal triggering registers
  //==============================================================================
  //Channel internal threshold level
  uint32_t Threshold; // 0x1n80
  uint32_t NSamples; //0x1n84

  //==============================================================================
  //Channel DAC setting
  //==============================================================================
  //Sets the 16bit DAC for this channel.
  //  0x0000 ->  ADC range (-1.0V, 0.0V)
  //  0x7FFF ->  ADC range (-0.5V, 0.5V)
  //  0xFFFF ->  ADC range ( 0.0V, 1.0V)
  uint32_t DAC; //0x1n98

  //==============================================================================
  //Channel firmware (AMC FPGA) X.Y
  //==============================================================================
  //  bits: (31-16) Revision date Y/M/DD
  //  bits: (15-08) Firmware revision X
  //  bits: (07-00) Firmware revision Y
  uint32_t AMCFPGA; //0x1n8C
};

#endif
