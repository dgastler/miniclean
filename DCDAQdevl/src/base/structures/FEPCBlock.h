#ifndef __FEPCBLOCK__
#define __FEPCBLOCK__

#include <vector>
#include "RAT/DS/EV.hh"
#include <ReductionLevel.h>
#include <EventState.h>

struct FEPCBlock
{   
  //Pointer to RAT EV structure
  RAT::DS::EV * ev;
  //Reduction level of this event (ReductionLevel.h)
  int8_t reductionLevel;
  //Vector of heard from WFDs
  std::vector<uint8_t> WFD;
  //DRPC communication status
  bool isSent;
  //Event status
  int8_t state;
  int64_t eventID;
  
  FEPCBlock(){ev = NULL;Clear();};
  void Clear()
  {
    isSent = false;
    state = EventState::UNFINISHED;
    eventID = -1;
    reductionLevel = ReductionLevel::UNKNOWN;
    WFD.clear();
    if(ev != NULL)      
      ev->PrunePMT();
  };
  ~FEPCBlock()
  {
  }
};

struct EventDecision
{
  int64_t ID;
  uint8_t reductionLevel;
};
#endif
