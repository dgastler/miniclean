#ifdef __RATDS__

#include <DRPCRatManager.h>



void DRPCRATManager::AddTimeStampHistogram(enum TimeStampCode::Code code)
{
  //build binning logarithmically 
  const int nBins = 500;
  double xBins[2*nBins];
  double rangeMax = 100;
  double rangeMin = 1E-5;
  //Negative bins and positive at the same time
  for(int i = 0; i < nBins;i++)
    {      
      //negative
      xBins[nBins-i-1] = -1*exp(log(rangeMin) + i*(log(rangeMax) - log(rangeMin))/double(nBins));
      //positive
      xBins[i+nBins] = exp(log(rangeMin) + i*(log(rangeMax) - log(rangeMin))/double(nBins));
    }
  hTimeStamp[code] = new TH1F(TimeStampCode::GetTimeStampName(code),
			      TimeStampCode::GetTimeStampName(code),
			      2*nBins-1,xBins);		  
}

void DRPCRATManager::PrintTimeStampHistograms(std::string filename)
{
  TFile * outFile = TFile::Open(filename.c_str(),"RECREATE");
  std::map<enum TimeStampCode::Code,TH1F*>::iterator it;
  for(it = hTimeStamp.begin(); it != hTimeStamp.end();it++)
    {
      outFile->Add(it->second);
      outFile->Write();
      delete it->second;
    }
  hTimeStamp.clear();
  delete outFile;
  printf("Wrote histograms\n");
}

DRPCRATManager::DRPCRATManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("DRPCRATManager"));
  //Make sure our memory vector is empty
  Memory.clear();
  
  AddTimeStampHistogram(TimeStampCode::DR_CLEAR);
  AddTimeStampHistogram(TimeStampCode::DR_DRAssembler_START);
  AddTimeStampHistogram(TimeStampCode::DR_DRAssembler_END);  
  AddTimeStampHistogram(TimeStampCode::DR_DMaker);
  AddTimeStampHistogram(TimeStampCode::DR_BadEvent);
  AddTimeStampHistogram(TimeStampCode::DR_EBPC);
  ShutdownStatus = false;
}

DRPCRATManager::~DRPCRATManager()
{
  Deallocate();
  //Dump timing information to disk
  // char * nameBuffer = new char[100];
  // sprintf(nameBuffer,"DRPCRatManager_%d.root",GetRunNumber());
  // PrintTimeStampHistograms(nameBuffer);
  // delete [] nameBuffer;
  PrintTimeStampHistograms("DRPCRatManager.root");
}

void DRPCRATManager::Deallocate()
{
  //Clear our containers
  Free.Clear(true);
  UnProcessed.Clear(true);
  EventStore.Clear(true);
  Respond.Clear(true);
  BadEvent.Clear(true); 
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //remove it's entry in Memory 
      //dellocations are called in the deconstructors
      Memory.pop_back();
    }
}

int64_t DRPCRATManager::GetEvent(int64_t eventKey,
				 int32_t &index,
				 uint64_t &password)
{  
  //Get the index at Event
  int64_t ret = EventStore.GetEntry(eventKey,index,password);
  if(ret == returnDCVector::EMPTY_KEY)
    //The index was empty
    {      
      //Get a new index from the free queue
      int32_t freeIndex;
      bool gotFree = false;      
      while(!(gotFree = Free.Get(freeIndex)) &&
	    !ShutdownStatus)
	{
	}
      if(gotFree)
	{
	  index = freeIndex;
	  //Add time stamp
	  RATEVBlock * ptr = GetDRPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::DR_DRAssembler_START);  
	  ret = EventStore.UpdateEntry(eventKey,freeIndex,password); 	  
	}
      else
	//Failed to get a free
	{	  
	  index = EventStore.GetBadValue();
	  //Must block to make sure everything is consistent
	  ret = EventStore.ClearEntry(eventKey,password);
	}
    }
  return ret;
}

int64_t DRPCRATManager::SetEvent(int64_t eventKey,
				 int32_t &index,
				 uint64_t password)
{
  int64_t ret = returnDCVector::OK;

  ret = EventStore.UpdateEntry(eventKey,index,password);
  if(ret == returnDCVector::OK)
    {
      ret = EventStore.ClearPassword(eventKey,password);
    }
  return ret;  
}

int64_t DRPCRATManager::ClearEvent(int64_t eventKey, uint64_t password)
{
  int64_t ret = returnDCVector::OK;
  ret = EventStore.ClearEntry(eventKey,password);
  return ret;  
}

bool DRPCRATManager::AddBadEventNOFD(int64_t eventKey, int32_t &index, Level::Type errorCode)
{
  bool ret = false;
  //Read off one from the FD to not cause select to fire on this event.
  fprintf(stderr,"This doesn't do what you think!\n");
  BadEvent::sBadEvent badEvent;
  badEvent.eventKey = eventKey;
  badEvent.index = index;
  badEvent.errorCode = errorCode;
  ret = BadEvent.Add(badEvent);  
  return ret;  
}
bool DRPCRATManager::AddBadEvent(int64_t eventKey, int32_t &index, Level::Type errorCode)
{
  bool ret = false;
  BadEvent::sBadEvent badEvent;
  badEvent.eventKey = eventKey;
  badEvent.index = index;
  badEvent.errorCode = errorCode;
  ret = BadEvent.Add(badEvent);
  return ret;  
}

bool DRPCRATManager::AllocateMemory(xmlNode * setupNode)
{
  bool ret = true;
  
  int NumBlocksERR = GetXMLValue(setupNode,"NUMBLOCKS","MANAGER",NumBlocks);
  if(NumBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMDRPC in DRPCRATManager.\n");
      ret = false;
    }
  int VectorHashSizeERR = GetXMLValue(setupNode,"VECTORHASHSIZE","MANAGER",VectorHashSize);
  if(VectorHashSizeERR != 0)
    {
      fprintf(stderr,"Error reading VECTORHASHSIZE in DRPCRATManager.\n");
      ret = false;
    }

  //Allocate the needed RAT DRPCs
  if(ret)
    {
      for(uint32_t iBlock = 0; iBlock < NumBlocks;iBlock++)
	{
	  RATEVBlock evBlock(TimeStampCode::DR_CLEAR);	  
	  Memory.push_back(evBlock);
	  Memory.back().ev = new RAT::DS::EV;	  
	  Memory.back().timeStampReference = TimeStampCode::DR_DRAssembler_START;	  
	  int32_t index = int32_t(Memory.size())-1;
	  Free.Add(index);
	  if(Memory.back().ev == NULL)
	    {
	      ret = false;
	      break;
	    }
	}     
    }
  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash (-1 emtpy value, -2 bad value)
      EventStore.Setup(VectorHashSize,-1,-2);
    }
  return(ret);
}

int64_t DRPCRATManager::MoveEventToUnProcessedQueue(int64_t eventKey,
						    uint64_t password)
{
  int64_t ret = returnDCVector::BAD_MUTEX;  
  int32_t index = -2;
  //Get the index for this eventKey.
  ret = EventStore.GetEntry(eventKey,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventKey.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVector::OK)||(ret >= 0))
    {
      //Remote this event from the EventStore vector hash
      ret = EventStore.ClearEntry(eventKey,password);
      if(ret == returnDCVector::OK)
	//The event has been cleared from the vector hash
	{
	  //Timestamp the event
	  RATEVBlock * ptr = GetDRPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::DR_DRAssembler_END);	  
	  //Add index to the UnSent queue
	  bool moveWorked = UnProcessed.Add(index);
	  if(moveWorked)
	    {
	      index = EventStore.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVector::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  //We should just return the error we had
	}	
    }
  else
    {
      //Return the error we got from Get
      printf("Got error %"PRId64" from get\n",ret);
    }
  return ret;
}
						    

int64_t DRPCRATManager::MoveEventToBadQueue(int64_t eventKey,
					    uint64_t password,
					    Level::Type errorCode)
{
  int64_t ret = returnDCVector::BAD_MUTEX;  
  int32_t index;
  //Get the index for this eventKey.
  ret = EventStore.GetEntry(eventKey,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventKey.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVector::OK)||(ret >= 0))
    {
      //Remove this event from the EventStore vector hash
      ret = EventStore.ClearEntry(eventKey,password);
      if(ret == returnDCVector::OK)
	//The event has been cleared from the vector hash
	{
	  //Add index to the Bad queue
	  BadEvent::sBadEvent badEvent;
	  badEvent.errorCode = errorCode;
	  badEvent.index = index;
	  badEvent.eventKey = eventKey;
	  //Add time stamp
	  RATEVBlock * ptr = GetDRPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::DR_BadEvent);
	  bool moveWorked = BadEvent.Add(badEvent);
	  if(moveWorked)
	    {
	      index = EventStore.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVector::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  //We should just return the error we had
	}	
    }
  else
    {
      //Return the error we got from Get
    }
  return ret;
}

void DRPCRATManager::PrintStatus(int indent, int verbosity)
{
  StatMessage::StatDRPCMM sStat;
  sStat.sBase.type = StatMessage::DRPC;
  sStat.sInfo.time = time(NULL);
  sStat.sInfo.subID = SubID::BLANK;
  sStat.sInfo.level = Level::BLANK;
  sStat.allocatedSize = Memory.size();
  sStat.eventStore.size   = EventStore.GetSize();
  sStat.eventStore.active = EventStore.GetActiveEntryCount();
  sStat.free.size     = Free.GetSize();
  sStat.free.addCount = Free.GetAddCount();
  sStat.unProcessed.size     = UnProcessed.GetSize();
  sStat.unProcessed.addCount = UnProcessed.GetAddCount();
  sStat.respond.size     = Respond.GetSize();
  sStat.respond.addCount = Respond.GetAddCount();
  sStat.badEvent.size     = BadEvent.GetSize();
  sStat.badEvent.addCount = BadEvent.GetAddCount();
  sStat.EBPC.size = EBPC.GetSize();
  sStat.EBPC.addCount = EBPC.GetAddCount();

  DCMessage::Message message;
  message.SetType(DCMessage::STATISTIC);
  message.SetData(sStat);
  SendMessageOut(message);

#ifdef VERBOSE_MEMORY
  char * textBuffer = new char[5000];
  sprintf(textBuffer,"\n");
  if(indent > 0)
    {
      for(int i = 0; i < indent;i++)
	{
	  sprintf(textBuffer," ");
	}
    }
  char printCommand[] = "DRPCRATManager\n     Size: %04zu\n     Free: %04zu     (%12zu)\n     Active Stored: %04zu(%04zu)\n     UnProcessed: %04zu     (%12zu)\n     Respond: %04zu     (%12zu)\n     BadEvent: %04zu     (%12zu)\n     EBPC: %04zu     (%12zu)\n";
  sprintf(textBuffer,printCommand,
	  sStat.allocatedSize,
	  sStat.free.size,
	  sStat.free.addCount,
	  sStat.eventStore.active,
	  sStat.eventStore.size,
	  sStat.unProcessed.size,
	  sStat.unProcessed.addCount,
	  sStat.respond.size,
	  sStat.respond.addCount,
	  sStat.badEvent.size,
	  sStat.badEvent.addCount,
	  sStat.EBPC.size,
	  sStat.EBPC.addCount);
  Print(textBuffer);
  delete [] textBuffer;
#endif
//  DCMessage::DCTObject rootObj;
//  std::vector<uint8_t> dataTemp;
//  time_t currentTime = time(NULL);
//  std::map<enum TimeStampCode::Code,TH1F*>::iterator it;
//  for(it = hTimeStamp.begin(); it != hTimeStamp.end();it++)
//    {      
//      dataTemp = rootObj.SetData((TObject * )( it->second),
//				 DCMessage::DCTObject::H1F);
//      message.SetDestination();
//      message.SetType(DCMessage::HISTOGRAM);
//      message.SetData(&(dataTemp[0]),dataTemp.size());
//      message.SetTime(currentTime);
//      SendMessageOut(message);
//      it->second->Reset();
//    }
}


bool DRPCRATManager::AddFree(int32_t &index)
{
  bool ret = false;
  
  RATEVBlock * ptr = GetDRPCBlock(index);
  std::map<enum TimeStampCode::Code,TH1F*>::iterator it;
  for(it = hTimeStamp.begin(); it != hTimeStamp.end();it++)
    {
      double dt = ptr->TimeDiff(it->first,TimeStampCode::DR_DRAssembler_START);
      it->second->Fill(dt);      
    }
  
  if(ptr)
    ptr->Clear(TimeStampCode::DR_CLEAR);
  
  ret = Free.Add(index);
  return ret;
}

bool DRPCRATManager::GetUnProcessed(int32_t &index)
{
  bool ret = false;
  ret = UnProcessed.Get(index);
  return(ret);
}

bool DRPCRATManager::GetRespond(int32_t &index)
{
  bool ret = false;
  ret = Respond.Get(index);
  return(ret);
}
bool DRPCRATManager::AddRespond(int32_t &index)
{
  bool ret = false;
  //Add time stamp
  RATEVBlock * ptr = GetDRPCBlock(index);
  if(ptr)
    ptr->AddTimeStamp(TimeStampCode::DR_DMaker);
  ret = Respond.Add(index);
  return(ret);
}
bool DRPCRATManager::GetBadEvent(BadEvent::sBadEvent &badEvent)
{
  bool ret = false;
  ret = BadEvent.Get(badEvent);
  return(ret);
}

bool DRPCRATManager::GetEBPC(int32_t &index)
{
  bool ret = false;
  ret = EBPC.Get(index);
  return(ret);
}
bool DRPCRATManager::AddEBPC(int32_t &index)
{
  bool ret = false;
  //Add time stamp
  RATEVBlock * ptr = GetDRPCBlock(index);
  if(ptr)
    ptr->AddTimeStamp(TimeStampCode::DR_EBPC);
  ret = EBPC.Add(index);
  return(ret);
}

void DRPCRATManager::PrintVectorHash()
{
  char * textBuffer = new char[5000000];
  EventStore.PrintDCVector(textBuffer);
  Print(textBuffer);
  delete [] textBuffer;
}

#endif


