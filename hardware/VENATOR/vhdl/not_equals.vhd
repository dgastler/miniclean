library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity not_equals is Port( 
	A : in  STD_LOGIC_VECTOR(16 downto 0);
	B : in  STD_LOGIC_VECTOR(17 downto 0);
	O : out  STD_LOGIC);
end not_equals;

architecture Behavioral of not_equals is
begin
	O <= '0' when A = B(16 downto 0) else '1';
end Behavioral;