#include <DRAssembler.h>

void DRAssembler::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.serverSocketFD))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < server.Size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(server[iConnection].thread->GetInPacketManager()->GetFullFDs()[0]))
	{
	  ProcessDRPusher(server[iConnection].thread);
	}
      //Check for a new message
      if(IsReadReady(server[iConnection].thread->GetMessageOutFDs()[0]))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

void DRAssembler::ProcessDRPusher(DCNetThread * thread)
{
  //Get new incomming packet
  int32_t iDCPacket = FreeFullQueue::BADVALUE;
  thread->GetInPacketManager()->GetFull(iDCPacket,true);
  //Check for bad packet type
  if(iDCPacket != FreeFullQueue::BADVALUE)
    //valid packet
    {
      //      fprintf(stderr,"iPacket: %d\n",iDCPacket);
      DCNetPacket * packet = thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Check if this is the correct kind of packet
      if(!((packet->GetType() == DCNetPacket::INTEGRATED_WINDOWS_PARTIAL_PACKET) ||
	   (packet->GetType() == DCNetPacket::INTEGRATED_WINDOWS_FULL_PACKET)))
	{
	  //ProcessBadPacket cleans up.
	  PrintError("Unexpected packet type!\n");
	  //return packet
	  thread->GetInPacketManager()->AddFree(iDCPacket);
	  //move on	  
	}
      else
	{
	  //Get the data from the packet.
	  uint8_t * packetData = packet->GetData();
	  uint8_t * currentPointer = packetData;
	  uint16_t packetDataSize = packet->GetSize();
	  uint16_t packetDataLeft = packetDataSize;    
	  //Loop over all the Event structs and their data in this array
	  while(packetDataLeft > 0)
	    {
	      if(ProcessDRPusherEvent(currentPointer,packetDataLeft) < 0)
		{
		  PrintError("Parsing packet error!");
		  SendStop();
		  break;
		}
	    }      
	}
      //return packet
      thread->GetInPacketManager()->AddFree(iDCPacket);      
    }

}

int32_t DRAssembler::ProcessDRPusherEvent(uint8_t * & data,uint16_t &maxSize)
{
  int32_t ret = 0;
  //Check if there is enough data for an event block
  if(maxSize < sizeof(DRPacket::Event))
    {
      return -1;
    }
  
  //Cast this packets event header to drEvent so we can
  //get the event number and move forward in the data array
  DRPacket::Event * drEvent = (DRPacket::Event *) data;
  int64_t eventID = drEvent->ID;
  data += sizeof(DRPacket::Event);
  maxSize -= sizeof(DRPacket::Event);

//  fprintf(stderr,"Processing event %010u with %02u channels (%u:%p)\n",
//	  drEvent->ID,
//	  drEvent->ChannelCount,
//	  maxSize,
//	  data);

  //Get this event from the Event hash
  int32_t evIndex;
  DRPCBlock * evBlock = NULL;
  uint64_t password;
  int64_t retDRPCmem = DRPC_mem->GetEvent(eventID,evIndex,password);
  
  while(retDRPCmem >= 0)
    //If there is a collision
    {
      //Move old event to bad event queue and 
      //address the FEPCs at fault
      ProcessCollision(eventID,retDRPCmem,evIndex,password);
      //Now things should be fixed and we should get our event
      retDRPCmem = DRPC_mem->GetEvent(eventID,evIndex,password);      
    }      
  //No collision
  if(retDRPCmem == returnDCVectorHash::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    {
      //Get our evBlock
      evBlock = DRPC_mem->GetDRPCBlock(evIndex);
      
      if(evBlock->eventID == -1)
	{
	  evBlock->eventID = eventID;
	  evBlock->state = EventState::UNFINISHED; 
	}
	  
      //Process the block
      int32_t toProcessSize = maxSize;
      int channelsLeftToAdd = AddDataToEV(evBlock,
					  data,
					  toProcessSize,
					  drEvent->ChannelCount);
      //Calculate how much data we processed for this event structure
      ret = maxSize - toProcessSize;
      maxSize = toProcessSize;

      if(channelsLeftToAdd != 0)
	{
	  char * buffer = new char[100];
	  sprintf(buffer,"We lost %d PMTs! from event %d",
		  channelsLeftToAdd,
		  drEvent->ID);
	  PrintError(buffer);
	  delete [] buffer;
	}	  
      //Check if we have readout all the PMTs for this event.
      else if(evBlock->ev->GetPMTCount() == int(pmtList.size()))
	{
	  DRPC_mem->MoveEventToUnProcessedQueue(eventID,
						evIndex,
						password);
	}
      else
	//if the channel add was fine, but we haven't fully filled
	//the event, return it to the DRPCRATManager
	{
	  DRPC_mem->SetEvent(eventID,evIndex,password);
	}
    }
  else 
    {
      PrintError("DRPCmem error!\n");
      SendStop();
    }    
  return ret;
}

int DRAssembler::AddDataToEV(DRPCBlock * event,uint8_t * &data,int32_t &dataSizeMax,int pmtCount)
{  
  //Get the EV from the DRPCBlock.
  RAT::DS::EV * ev = event->ev;

  //Build needed casting objects
  DRPacket::Channel * drChannel = NULL;
  DRPacket::RawIntegral * drRawIntegral = NULL;
  
  while((pmtCount > 0) &&  //Only process up to pmtCOunt of pmts
	(dataSizeMax >= int32_t(sizeof(DRPacket::Channel)))) //Only process up to dataSizeMax bytes
    {
      //Cast the ptr to a PMT type and update ptr
      drChannel = (DRPacket::Channel *) data;
      data += sizeof(DRPacket::Channel);
      dataSizeMax -= uint16_t( sizeof(DRPacket::Channel) );

      //Get a new PMT object for this EV,pmt
      RAT::DS::PMT * pmt = ev->AddNewPMT();
      pmt->SetID(drChannel->ID);
      event->PMT.push_back(drChannel->ID);
      //Build/Get a raw for this PMT
      RAT::DS::Raw * raw = pmt->GetRaw();

      for(int iIntegral = 0; 
	  //any integrals left?
	  ((iIntegral < drChannel->RawIntegralCount) &&  
	   //space for a RawIntegral?
	   (dataSizeMax >= int32_t(sizeof(DRPacket::RawIntegral)))); 
	  iIntegral++)
	{
	  //Setup our rawIntegral ptr
	  drRawIntegral = (DRPacket::RawIntegral *) data;
	  data += sizeof(DRPacket::RawIntegral);
	  dataSizeMax -= uint16_t(sizeof(DRPacket::RawIntegral));
	  
	  //Get a new RawIntegral for this RAW
	  RAT::DS::RawIntegral * rawIntegral = raw->AddNewRawIntegral();
	  //Set values
	  rawIntegral->SetIntegral(drRawIntegral->integral);
	  rawIntegral->SetStartTime(drRawIntegral->startTime);
	  rawIntegral->SetWidth(drRawIntegral->width);
	  
	  
	  //rawIntegral->SetBaseline(drRawIntegral->baseline);
	  //rawIntegral->SetBaselineRMS(drRawIntegral->baselineRMS);
	  //Unknown
	  rawIntegral->SetBaseline(-1);
	  rawIntegral->SetBaselineRMS(-1);
	}
      pmtCount--;
    }
  //Return how many pmts were processed
  return pmtCount;
}
  














































//void DRAssembler::ProcessDRPusher(DCNetThread * thread)
//{
//  //Get new incomming packet
//  int32_t iDCPacket = FreeFullQueue::BADVALUE;
//  thread->GetInPacketManager()->GetFull(iDCPacket,true);
//  //Check for bad packet type
//  if(iDCPacket != FreeFullQueue::BADVALUE)
//    //valid packet
//    {
//      DCNetPacket * packet = thread->GetInPacketManager()->GetPacket(iDCPacket);
//      //Check if this is the correct kind of packet
//      if(!((packet->GetType() == DCNetPacket::INTEGRATED_WINDOWS_PARTIAL_PACKET) ||
//	   (packet->GetType() == DCNetPacket::INTEGRATED_WINDOWS_FULL_PACKET)))
//	{
//	  //ProcessBadPacket cleans up.
//	  PrintError("Unexpected packet type!\n");
//	  //return packet
//	  thread->GetInPacketManager()->AddFree(iDCPacket);
//	  //move on
//	  return;
//	}
//
//      //Get the data from the packet.
//      uint8_t * packetData = packet->GetData();
//      uint16_t packetDataSize = packet->GetSize();
//      //Check if there is enough data for an event block
//      if(packetDataSize < sizeof(DRPacket::Event))
//	{
//	  //return packet
//	  thread->GetInPacketManager()->AddFree(iDCPacket);
//	  //move on
//	  return;
//	}
//
//      //Cast this packets event header to drEvent so we can
//      //get the event number
//      DRPacket::Event * drEvent = (DRPacket::Event *) packetData;
//      int64_t eventID = drEvent->ID;
//      //Get this event from the Event hash
//      int32_t evIndex;
//      DRPCBlock * evBlock = NULL;
//      uint64_t password;
//      int64_t retDRPCmem = DRPC_mem->GetEvent(eventID,evIndex,password);
//      
//      while(retDRPCmem >= 0)
//	//If there is a collision
//	{
//	  //Move old event to bad event queue and 
//	  //address the FEPCs at fault
//	  ProcessCollision(eventID,retDRPCmem,evIndex,password);
//	  //Now things should be fixed and we should get our event
//	  retDRPCmem = DRPC_mem->GetEvent(eventID,evIndex,password);      
//	}      
//      
//      //No collision
//      if(retDRPCmem == returnDCVectorHash::OK)		     
//	//We got the index from the vector hash and it's now locked to us. 
//	{
//	  //Get our evBlock
//	  evBlock = DRPC_mem->GetDRPCBlock(evIndex);
//	  
//	  if(evBlock->eventID == -1)
//	    {
//	      evBlock->eventID = eventID;
//	      evBlock->state = EventState::UNFINISHED; 
//	    }
//	  
//	  //Process the block
//	  int addReturn = AddDataToEV(evBlock,
//				      packetData + sizeof(DRPacket::Event),
//				      packetDataSize - uint16_t(sizeof(DRPacket::Event)),
//				      drEvent->ChannelCount);
//	  if(addReturn != drEvent->ChannelCount)
//	    {
//	      char * buffer = new char[100];
//	      sprintf(buffer,"We lost a PMT! %d:%d",
//		      addReturn,
//		      drEvent->ChannelCount);
//	      //	      PrintError("We lost a PMT!");
//	      PrintError(buffer);
//	      ProcessBadPacketLostPMT(packet);
//	      delete [] buffer;
//	    }	  
//	  //Check if we have readout all the PMTs for this event.
//	  else if(evBlock->ev->GetPMTCount() == int(pmtList.size()))
//	    {
//	      DRPC_mem->MoveEventToUnProcessedQueue(eventID,
//						    evIndex,
//						    password);
//	    }
//	  else
//	    //if the channel add was fine, but we haven't fully filled
//	    //the event, return it to the DRPCRATManager
//	    {
//	      DRPC_mem->SetEvent(eventID,evIndex,password);
//	    }
//	}
//      else 
//	{
//	  PrintError("DRPCmem error!\n");
//	  SendStop();
//	}
//      
//      //return packet
//      thread->GetInPacketManager()->AddFree(iDCPacket);
//    }
//}
//
//int DRAssembler::AddDataToEV(DRPCBlock * event,uint8_t * data,uint16_t dataSize,int pmtCount)
//{  
//  //Get the EV from the DRPCBlock.
//  RAT::DS::EV * ev = event->ev;
//
//  //Build needed casting objects
//  DRPacket::Channel * drChannel = NULL;
//  DRPacket::RawIntegral * drRawIntegral = NULL;
//
//  //Loop over each of the PMT structures
//  uint16_t dataSizeLeft = dataSize;
//  int pmtCountLeft = pmtCount;
//  uint8_t * ptr = data;
//  
//  while((dataSizeLeft >= sizeof(DRPacket::Channel)) &&  //room for a channel?
//	(pmtCountLeft > 0))     //Any channels left?
//    {
//      //Cast the ptr to a PMT type and update ptr
//      drChannel = (DRPacket::Channel *) ptr;
//      ptr += sizeof(DRPacket::Channel);
//      dataSizeLeft -= uint16_t( sizeof(DRPacket::Channel) );
//
//      //Get a new PMT object for this EV,pmt
//      RAT::DS::PMT * pmt = ev->AddNewPMT();
//      pmt->SetID(drChannel->ID);
//      event->PMT.push_back(drChannel->ID);
//
//      //Build/Get a raw for this PMT
//      RAT::DS::Raw * raw = pmt->GetRaw();
//
//      //Loop over PMTCoutn      
//      for(int iIntegral = 0; 
//	  ((iIntegral < drChannel->RawIntegralCount) &&  //any integrals left?
//	   (dataSizeLeft > sizeof(DRPacket::RawIntegral))); //space for a RawIntegral?
//	  iIntegral++)
//	{
//	  //Setup our rawIntegral ptr
//	  drRawIntegral = (DRPacket::RawIntegral *) ptr;
//	  ptr += sizeof(DRPacket::RawIntegral);
//	  dataSizeLeft -= uint16_t(sizeof(DRPacket::RawIntegral));
//	  
//	  //Get a new RawIntegral for this RAW
//	  RAT::DS::RawIntegral * rawIntegral = raw->AddNewRawIntegral();
//	  //Set values
//	  rawIntegral->SetIntegral(drRawIntegral->integral);
//	  rawIntegral->SetStartTime(drRawIntegral->startTime);
//	  rawIntegral->SetWidth(drRawIntegral->width);
//	  
//	  
//	  //rawIntegral->SetBaseline(drRawIntegral->baseline);
//	  //rawIntegral->SetBaselineRMS(drRawIntegral->baselineRMS);
//	  //Unknown
//	  rawIntegral->SetBaseline(-1);
//	  rawIntegral->SetBaselineRMS(-1);
//	}
//      pmtCountLeft--;
//    }
//  //Return how many pmts were processed
//  return(pmtCount - pmtCountLeft);
//}

void DRAssembler::ProcessCollision(int64_t newEventID,
				   int64_t oldEventID,
				   int32_t evIndex,
				   uint64_t password)
{
  //We have a conflict
  char * buffer = new char[100];
# if __WORDSIZE == 64
  char formatString[] = "Collision! %ld %ld\n";
# else
  char  formatString[] = "Collision! %lld %lld\n";
# endif
  sprintf(buffer,formatString,oldEventID,newEventID);
  PrintError(buffer);
  delete [] buffer;
  //The data in the vector hash is for another event. 
  //We have a lock on it though!
  //We will move this event to the bad queue
  //Action concerning this bad event will be left to
  //another thread.
  //int64_t retCollision = DRPC_mem->MoveEventToBadQueue(oldEventID,
  DRPC_mem->MoveEventToBadQueue(oldEventID,
				evIndex,
				password);
  
  PrintError("Address collision\n");
}

//void DRAssembler::ProcessBadPacketLostPMT(DCNetPacket *packet)
//{
//  PrintError("Lost a PMT");
//  SendStop();
//}
