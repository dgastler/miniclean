#include <DRGetter.h>

bool DRGetter::ProcessNewConnection()
{  
  bool ret = true;
  if(DRSender.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(DRSender.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(DRSender.thread->GetMessageOutFDs()[0]);      
      DRSender.thread->Start(NULL);
      printf("Connected to DRSender at: %s\n",DRSender.remoteIP.c_str());
      //Update the select set so we pay attention to our new connection.
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}


void DRGetter::ProcessNetThreadMessage()
{
  DCMessage::Message message = DRSender.thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::ERROR)
    {
      //Shut down our current thread
      DeleteConnection();
      //Start trying to get a new connection
      StartNewConnection();
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message,true);
    }
  else
    {
      PrintWarning("Unknown message from DCMessageClient's DCNetThread\n");
    }
}

//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DRGetter::StartNewConnection()
{
  if(DRSender.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return(true);
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  SendStop();
	  return(false);
	}

    }
  else 
    {
      if(DRSender.fdSocket >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (DRSender.fdSocket);
	  AddWriteFD(DRSender.fdSocket);
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  SendStop();
	  return(false);
	}
    }
  return(true);
}

void DRGetter::DeleteConnection()
{
  //Making sure that the thread actually exists
  if(DRSender.thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(DRSender.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(DRSender.thread->GetMessageOutFDs()[0]);
    }
  //See if we are actually connected
  if(DRSender.fdSocket != BadPipeFD)
    {
      //remove any selecting on fdSocket
      RemoveReadFD(DRSender.fdSocket);
      RemoveWriteFD(DRSender.fdSocket);
    }  

  //Shutdown the thread
  DRSender.Shutdown();
  SetupSelect();
}
