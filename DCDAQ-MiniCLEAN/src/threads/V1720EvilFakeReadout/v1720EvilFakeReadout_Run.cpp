#include <v1720EvilFakeReadout.h>

void  v1720EvilFakeReadout::MainLoop()
{
  if(blockIndex == FreeFullQueue::BADVALUE)
    {
      //Get new memory block
      Mem->GetFree(blockIndex,true);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = Mem->GetBlock(blockIndex);
	  block->dataSize = 0;
	}
    }
  
  if(blockIndex != FreeFullQueue::BADVALUE)
    {
      uint32_t eventCount = 0;      
      while(eventCount < EventsPerBlock)
	{
	  SkipThis = false;
	  uint32_t * event = FakeEvent[iEvent];//get event
	  uint32_t eventSize = event[0]&0x0FFFFFFF;//get this event's size	      
	  if(((block->dataSize>>2) + eventSize) <= (block->allocatedSize>>2))
	    {
	       //Get the real event number and board ID
	      RealEventID = (event[2]&0xFFFFFF) + EventCountOffset;
	      WFDID = (event[1]&0xF8000000)>>27;
	      if(WFDID == skipWFD && RealEventID == skipEvent){SkipThis = true;}
	      if(!SkipThis)
		{
		  //Get a pointer to part of the Block buffer we are going to write to.
		  uint32_t * bufferPointer = (uint32_t * )(block->buffer + block->dataSize);
		  //Copy from event to bufferPointer eventsize*4 bytes.
		  memcpy(bufferPointer,event,eventSize<<2);
		  //Write lowest 8 bits of real event number to header
		  bufferPointer[1] = ((RealEventID & 0xFF)<<8) | event[1];
		  //Set the WFD event number and increment counter
		  bufferPointer[2] = 0xFFFFFF & WFDEventID[WFDID-1];
		  WFDEventID[WFDID-1] +=1;
		  
		  //Set block's data size to include this event
		  block->dataSize += (eventSize <<2);		  	
		  eventCount++;
		}
	      //Move to the next event.
	      iEvent++;
	      //If we are at the end of our loaded events go back to zero.
	      if(iEvent >= FakeEvent.size())
		{
		  EventCountOffset+=FakeEventCount;
		  if(MemoryLoop)
		    {
		      iEvent = 0;
		    }
		  else
		    {
		      printf("v1720EvilFakeReadout:   Sent all events (%d)\n",iEvent);
		      Loop = false;
		      
		      RemoveReadFD(Mem->GetFreeFDs()[0]);
		      SetupSelect();
		      eventCount = EventsPerBlock+1;
		    }
		}
	    } //Add event
	  else
	    {
	      break;
	    }	      
	}//Fill block
      //Pass off this full block.
      Mem->AddFull(blockIndex);
      //      fprintf(stderr,"v1720EvilFakeReadout: wrote out a block\n");
      TimeStep_BlocksRead++; 
    }
}

void  v1720EvilFakeReadout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
//      DCMessage::Message message;
//      BlocksRead += TimeStep_BlocksRead;
//      DCMessage::SingleUINT64 Count;
//
//      //Data message
//      sprintf(Count.text,"Blocks read");
//      Count.i=TimeStep_BlocksRead;
//      message.SetDataStruct(Count);
//      message.SetType(DCMessage::STATISTIC);
//      SendMessageOut(message,false);
//
      TimeStep_BlocksRead = 0;
      
      //Setup next time
      lastTime = currentTime;
    }
  if(Period!=0)
    {
      MainLoop();
    }
}
