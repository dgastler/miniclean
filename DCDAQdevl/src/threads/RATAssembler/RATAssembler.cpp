#include <RATAssembler.h>

RATAssembler::RATAssembler(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATAssembler"));

  //FEPC memory manager
  FEPC_mem = NULL;
  collisionCount   = 0;
  lockBlockedCount = 0;

  //WFD  memory manager
  // lockedWFDBlocks.Clear(true);
  // WFD_mem = NULL;
  // wfdBlock = NULL;
  // wfdIndex = WFDBlockManager::BADVALUE;
  // currentWFDIndex = 0;

  //memBlock memory manager
  Block_mem = NULL;
  memBlock = NULL;
  memBlockIndex = FreeFullQueue::BADVALUE;
  
  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  //Reset counters
  timeStepLockBlockedCount = 0;
  collisionCount = 0;


}

bool RATAssembler::Setup(xmlNode * SetupNode,
			 std::map<std::string,MemoryManager*> &MemManager)
{
  bool ret = true;

  //================================
  //Load the map of board IDs and channels 
  //for the WFDs processed by this thread.
  //================================
  if(!detectorInfo.Setup(SetupNode))
    {
      PrintError("No channel map!\n");
      return(false);
    }
  
  //There should be two memory managers
    // MemoryBlockManager:  interface with raw CAEN data
  if(!FindMemoryManager(Block_mem,SetupNode,MemManager))
    {
      ret = false;
    }  
  //FEPCManager:   interface with the RAT world
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  
  if(!FEPC_mem)
    {
      fprintf(stderr,"Error %s: Missing FEPC RAT memory manager\n.",
	      GetName().c_str());
      ret = false;
    }
  if(!Block_mem)
    {
      fprintf(stderr,"Error %s: Missing Block memory manager\n.",GetName().c_str());
      ret = false;
    }
  
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


bool RATAssembler::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the FEPC RAT memory manager to shut down.
      FEPC_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(Block_mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the WFD/EV block's free queue's FDs with select
      AddReadFD(Block_mem->GetFullFDs()[0]);
      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}

void  RATAssembler::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      float TimeStep = difftime(currentTime,lastTime);
      DCMessage::TimedCount Count;
      
      //Data message
      sprintf(Count.text,"Events read");
      Count.count = timeStepLockBlockedCount;
      Count.dt = TimeStep;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);

     
      //Data message
      sprintf(Count.text,"Bad events read");
      Count.count = collisionCount;
      Count.dt = TimeStep;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);

      //Reset counters
      timeStepLockBlockedCount = 0;
      collisionCount = 0;

      //Setup next time
      lastTime = currentTime;
    }
}
