#include "Cut_Parameters.h"

using namespace std;
Cut_Parameters::Cut_Parameters()
{
  Window = 0;

  SPE = 0;
  nSPE = 0;

  PMTVmin = 0;

  PMTLinearity1 = 0;
  PMTLinearity2 = 0;
  
  PMTTimingMin = 0;
  PMTTimingMax = 0;
  TOFmin = 0;
  TOFmax = 0;
  
  PMT2psa0 = 0;
  PMT2psa1 = 0;
  PMT2psa2 = 0;
  PMT2psa3 = 0;
  PMT2psa4 = 0;
  PMT2psaSign = 1;
  
  PMT2psaV20 = 0;
  PMT2psaV21 = 0;

  PMT2max = 0;
  PMT2min = 0;

  TBRatioMin = 0;
  TBRatioMax = 0;
  FastMin = 0;
  FastMax = 0;

  bPMTVmin = false;

  bPMTLinearity1 = false;
  bPMTLinearity2 = false;

  bPMTTimingMin = false;
  bPMTTimingMax = false;
  bTOFmin = false;
  bTOFmax = false;
  
  bPMT2psa = false;
  bPMT2psaV2 = false;
  bPMT2 = false;

  bTBRatioMin = false;
  bTBRatioMax = false;
  bFastMin = false;
  bFastMax = false;
}
bool Cut_Parameters::PassCuts(PEvent * pevent)
{
  Float_t Volts = 1.0/255.0;
  Float_t Seconds = 2e-9;
  bool Pass = true;
  if(bPMTVmin)
    {
      if((pevent->MaxHeight(0)*Volts > PMTVmin)&&(pevent->MaxHeight(1)*Volts > PMTVmin))
	{}
      else
	return(false);
    }
  if(bPMTLinearity1)
    {
      if(pevent->MaxHeight(0)*Volts < PMTLinearity1)
	{}
      else
	return(false);
    }
  if(bPMTLinearity2)
    {
      if(pevent->MaxHeight(1)*Volts < PMTLinearity2)
	{}
      else
	return(false);
    }
  if(bPMTTimingMin)
    {
      if(pevent->Delay(1) - pevent->Delay(0) > PMTTimingMin)
	{}
      else
	return(false);
    }
  if(bPMTTimingMax)
    {
      if(pevent->Delay(1) - pevent->Delay(0) < PMTTimingMax)
	{}
      else
	return(false);
    }
  if(bTOFmax)
    {
      if((pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds  < TOFmax)
	{}
      else
	return(false);
    }
  if(bTOFmin)
    {
      if((pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds > TOFmin)
	{}
      else
	return(false);
    }
  if(bPMT2psa)
    {
      if( (pevent->Fast(2,0) > PMT2psa0)
	  &&(pevent->Fast(2,0) < PMT2psa1)
	  &&  ((PMT2psaSign* pevent->MaxHeight(2)*Volts) < PMT2psaSign*(PMT2psa2 + PMT2psa3*pevent->Fast(2,0) + PMT2psa4*pevent->Fast(2,0)*pevent->Fast(2,0)))
	  )
	{}
      else
	return(false);
    }
  if(bPMT2psaV2)
    {
      if( pevent->MaxHeight(2)*Volts/pevent->Fast(2,0) > PMT2psaV20 &&
	  pevent->MaxHeight(2)*Volts/pevent->Fast(2,0) < PMT2psaV21)
	{}
      else
	return(false);
    }
  if(bPMT2)
    {
      if( pevent->Fast(2,0) + pevent->Slow(2,0)  > PMT2min &&
	  pevent->Fast(2,0) + pevent->Slow(2,0)  < PMT2max)
	{}
      else
	return(false);
    }

  if(bTBRatioMin)
    {
      if( fabs(((pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPE[0] - (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPE[1] ) /
	       (((pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPE[0] + (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPE[1] )) ) > TBRatioMin  )
	{}
      else
	return(false);
    }
  if(bTBRatioMax)
    {
      if( fabs(((pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPE[0] - (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPE[1] ) /
	       (((pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPE[0] + (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPE[1] )) ) < TBRatioMax  )
	{}
      else
	return(false);
    }
  if(bFastMin)
    {
      if( (pevent->Fast(0,Window)/SPE[0] + pevent->Fast(1,Window)/SPE[1])/
	  (((pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPE[0] + (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPE[1] )) > FastMin)
	{}
      else
	return(false);
    }
  if(bFastMax)
    {
      if( (pevent->Fast(0,Window)/SPE[0] + pevent->Fast(1,Window)/SPE[1])/
	  (((pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPE[0] + (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPE[1] )) < FastMax)
	{}
      else
	return(false);
    }
  
  //  return(Pass);
  return(true);
  
}
int Cut_Parameters:: SetCuts(XMLNode *xMainNode,int i,float * SPEs,int n)
{
  Float_t Volts = 1.0/255.0;
  Float_t Seconds = 2e-9;
  Window = i;
  SPE = SPEs;
  nSPE = n;
  //  XMLNode xNode=xMainNode->getChildNode("Cuts");//xMainNode->getChildNode("Header");
  
  cout << "==========================================================" << endl;

  //PMT Linearity Cuts
  PMTVmin = atof(xMainNode->getChildNode("PMTVmin").getChildNode("Parameter").getAttribute("P"));
  bPMTVmin = true&&atoi(xMainNode->getChildNode("PMTVmin").getAttribute("use"));  
  if(bPMTVmin)
      cout << "    ";
  else
      cout << "not ";
  cout << "using PMT0 Linearity cut.     ";
  cout << "using min PMT Voltage.        ";
  if(bPMTVmin)
    cout << " Cut @ " << PMTVmin <<endl;
  else
    cout << endl;
  //PMT Linearity Cuts
  PMTLinearity1 = atof(xMainNode->getChildNode("PMTLinearity1").getChildNode("Parameter").getAttribute("P"));
  bPMTLinearity1 = true&&atoi(xMainNode->getChildNode("PMTLinearity1").getAttribute("use"));  
  if(bPMTLinearity1)
      cout << "    ";
  else
      cout << "not ";
  cout << "using PMT0 Linearity cut.     ";
  if(bPMTLinearity1)
    cout << " Cut @ " << PMTLinearity1 <<endl;
  else
    cout << endl;

  PMTLinearity2 = atof(xMainNode->getChildNode("PMTLinearity2").getChildNode("Parameter").getAttribute("P"));
  bPMTLinearity2 = true&&atoi(xMainNode->getChildNode("PMTLinearity2").getAttribute("use"));
  if(bPMTLinearity2)
      cout << "    "; 
  else
      cout << "not ";
  cout << "using PMT1 Linearity cut.     ";
  if(bPMTLinearity2)
    cout << " Cut @ " << PMTLinearity2 <<endl;
  else
    cout << endl;

  //PMT Timing
  PMTTimingMin = atof(xMainNode->getChildNode("PMTTimingMin").getChildNode("Parameter").getAttribute("P"))/Seconds;
  bPMTTimingMin = true&&atoi(xMainNode->getChildNode("PMTTimingMin").getAttribute("use"));
  if(bPMTTimingMin)
      cout << "    ";
  else
      cout << "not ";
  cout << "using PMT Min Timing cut.         ";
  if(bPMTTimingMin)
    cout << " Cut @ " << PMTTimingMin <<endl;
  else
    cout << endl;
  PMTTimingMax = atof(xMainNode->getChildNode("PMTTimingMax").getChildNode("Parameter").getAttribute("P"))/Seconds;
  bPMTTimingMax = true&&atoi(xMainNode->getChildNode("PMTTimingMax").getAttribute("use"));
  if(bPMTTimingMax)
      cout << "    ";
  else
      cout << "not ";
  cout << "using PMT Max Timing cut.         ";
  if(bPMTTimingMax)
    cout << " Cut @ " << PMTTimingMax <<endl;
  else
    cout << endl;
  //TOF max
  TOFmax = atof(xMainNode->getChildNode("TOFmax").getChildNode("Parameter").getAttribute("P"));
  bTOFmax = true&&atoi(xMainNode->getChildNode("TOFmax").getAttribute("use"));
  if(bTOFmax)
      cout << "    ";
  else
      cout << "not ";
  cout << "using TOFmax cut.             ";
  if(bTOFmax)
    cout << " Cut @ " << TOFmax << endl;
  else
    cout << endl;
  //TOF min
  TOFmin = atof(xMainNode->getChildNode("TOFmin").getChildNode("Parameter").getAttribute("P"));
  bTOFmin = true&&atoi(xMainNode->getChildNode("TOFmin").getAttribute("use"));
  if(bTOFmin)
      cout << "    ";
  else
      cout << "not ";
  cout << "using TOFmin cut.             ";
  if(bTOFmin)
    cout << " Cut @ " << TOFmin << endl ;
  else
    cout << endl;
  //PMT2psa
  PMT2psa0 = atof(xMainNode->getChildNode("PMT2psa").getChildNode("Parameter",0).getAttribute("P"));
  PMT2psa1 = atof(xMainNode->getChildNode("PMT2psa").getChildNode("Parameter",1).getAttribute("P"));
  PMT2psa2 = atof(xMainNode->getChildNode("PMT2psa").getChildNode("Parameter",2).getAttribute("P"));
  PMT2psa3 = atof(xMainNode->getChildNode("PMT2psa").getChildNode("Parameter",3).getAttribute("P"));
  PMT2psa4 = atof(xMainNode->getChildNode("PMT2psa").getChildNode("Parameter",4).getAttribute("P"));
  PMT2psaSign = atof(xMainNode->getChildNode("PMT2psa").getChildNode("Parameter",5).getAttribute("P"));
  if(PMT2psaSign != -1)
    PMT2psaSign = 1;
  bPMT2psa = true&&atoi(xMainNode->getChildNode("PMT2psa").getAttribute("use"));
  if(bPMT2psa)
    cout << "    ";
  else
    cout << "not ";
  cout << "using PMT2 pulse shape cuts.  ";
  if(bPMT2psa)
    cout << " Cuts @ " << PMT2psa0 << " : " << PMT2psa1 << " : " << PMT2psa2 << " : " << PMT2psa3 << " : " << PMT2psa4  << " : " << PMT2psaSign << endl  ;
  else
    cout << endl;
  //PMT2psaV2
  PMT2psaV20 = atof(xMainNode->getChildNode("PMT2psaV2").getChildNode("Parameter",0).getAttribute("P"));
  PMT2psaV21 = atof(xMainNode->getChildNode("PMT2psaV2").getChildNode("Parameter",1).getAttribute("P"));
  bPMT2psaV2 = true&&atoi(xMainNode->getChildNode("PMT2psaV2").getAttribute("use"));
  if(bPMT2psaV2)
    cout << "    ";
  else
    cout << "not ";
  cout << "using PMT2V2 pulse shape cuts.";
  if(bPMT2psaV2)
    cout << " Cuts @ " << PMT2psaV20 << " : " << PMT2psaV21 << endl;
  else
    cout << endl;
  
  //PMT2psaV2
  PMT2min = atof(xMainNode->getChildNode("PMT2").getChildNode("Parameter",0).getAttribute("P"));
  PMT2max = atof(xMainNode->getChildNode("PMT2").getChildNode("Parameter",1).getAttribute("P"));
  bPMT2 = true&&atoi(xMainNode->getChildNode("PMT2").getAttribute("use"));
  if(bPMT2)
    cout << "    ";
  else
    cout << "not ";
  cout << "using PMT2 energy cut         ";
  if(bPMT2)
    cout << " Cuts @ " << PMT2min << " : " << PMT2max << endl;
  else
    cout << endl;


  //TBRatioMax
  TBRatioMax = atof(xMainNode->getChildNode("TBRatioMax").getChildNode("Parameter").getAttribute("P"));
  bTBRatioMax = true&&atoi(xMainNode->getChildNode("TBRatioMax").getAttribute("use"));
  if(bTBRatioMax)
    cout << "    ";
  else
    cout << "not ";
  cout << "using top bottom cut max.     ";
  if(bTBRatioMax)
    cout << " Cut @ " << TBRatioMax << endl;
  else
    cout << endl;
  //TBRatioMin
  TBRatioMin = atof(xMainNode->getChildNode("TBRatioMin").getChildNode("Parameter").getAttribute("P"));
  bTBRatioMin = true&&atoi(xMainNode->getChildNode("TBRatioMin").getAttribute("use"));
  if(bTBRatioMin)
    cout << "    ";
  else
    cout << "not ";
  cout << "using top bottom cut min.     ";
  if(bTBRatioMin)
    cout << " Cut @ " << TBRatioMin << endl;
  else
    cout << endl;
  //Fast Min
  FastMin = atof(xMainNode->getChildNode("FastMin").getChildNode("Parameter").getAttribute("P"));
  bFastMin = true&&atoi(xMainNode->getChildNode("FastMin").getAttribute("use"));
  if(bFastMin)
    cout << "    ";
  else
    cout << "not ";
  cout << "using Fast fraction Min cut.  ";
  if(bFastMin)
    cout << " Cut @ " << FastMin << endl;
  else
    cout << endl;
  //Fast Max
  FastMax = atof(xMainNode->getChildNode("FastMax").getChildNode("Parameter").getAttribute("P"));
  bFastMax = true&&atoi(xMainNode->getChildNode("FastMax").getAttribute("use"));
  if(bFastMax)
    cout << "    ";
  else
    cout << "not ";
  cout << "using Fast fraction Max cut.  ";
  if(bFastMax)
    cout << " Cut @ " << FastMax << endl;
  else
    cout << endl;


  cout << "==========================================================" << endl;

  cout << endl;
  return(0);
}
