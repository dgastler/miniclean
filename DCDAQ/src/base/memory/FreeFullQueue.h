#ifndef __FREEFULLQUEUE__
#define __FREEFULLQUEUE__

#include <LockedQueue.h>

class FreeFullQueue
{
 public:  
  FreeFullQueue(){Setup();};
  void Setup(){Free.Setup(BADVALUE);Full.Setup(BADVALUE);};

  enum 
  {
    BADVALUE = -1
  };
  
  virtual ~FreeFullQueue(){Clear();};

  //clear queues
  void Clear(){Free.Clear(true);Full.Clear(true);};

  //Free/Full get functions.
  virtual bool GetFree(int32_t & index,bool wait = true)
  {    
    return(Free.Get(index,wait));
  };
  virtual bool GetFull(int32_t & index,bool wait = true)
  {
    return(Full.Get(index,wait));
  };
  //Free/Full add functions
  virtual bool AddFree(int32_t & Index){return(Free.Add(Index,true));};
  virtual bool AddFull(int32_t & Index){return(Full.Add(Index,true));};
  
  //FD access functions
  void GetFreeFDs(int * fds){Free.GetFIFOfds(fds);};  
  void GetFullFDs(int * fds){Full.GetFIFOfds(fds);};

  virtual void Shutdown(){Free.ShutdownWait();Full.ShutdownWait();WakeUp();};
  virtual void WakeUp(){Free.WakeUp();Full.WakeUp();};

 private:
  LockedQueue<int32_t> Free;  //queue of indicies of free memory blocks
  LockedQueue<int32_t> Full;  //queue of indicies of full memory blocks
};
#endif
