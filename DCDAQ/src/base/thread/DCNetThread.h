#ifndef __DCNETTHREAD__
#define __DCNETTHREAD__

//DCDAQ
#include <DCThread.h>
#include <DCNetPacketManager.h>

//TCP/IP
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//Time
#include <time.h>

//Select
#include <sys/select.h>

//Errno string errors
#include <string.h>

#define MAXNETERRORS 10
#define BADPACKETTIMEOUT 600


/*
 DCNetThread class:      This class operates the thread that communicates over
                         the network.  It opens a TCP socket and creates an
			 input and output packet manager.  The select properties
			 of this thread are described below. 
  select FDs:
   read:
    MessageIn:           DCDAQ messages from the parent
    PacketOut:           New packet read to be sent
    socketIn:            Data ready from the TCP socket
   write:
    none

  Setup functions()
   DCNetThread():        Initializes comm values to NULL and zeroes 
                         statistics variables
   SetupPacketManager(): This function creates the in and out packetManager
                         objects for this network connection.  This function
			 will set the thread's state to ready if SetupConnection
			 was sucessfully called.  The select FD sets will also
			 be setup if SetupConnect was sucessfully called. 

   SetupConnection():    
 */

class DCNetThread : public DCThread
{
 public:
  DCNetThread();
  ~DCNetThread();

  //  virtual bool Setup(xmlNode * SetupNode);
  virtual bool SetupConnection(const int &_SocketFD,
			       const int &_AddressFamily,
			       const std::vector<uint8_t> &_LocalIP,
			       const std::vector<uint8_t> &_RemoteIP);
  virtual bool SetupPacketManager(xmlNode * _PacketManagerNode);
  virtual void MainLoop();

  //Packet manager interface
  DCNetPacketManager * GetInPacketManager() {return(In);};
  DCNetPacketManager * GetOutPacketManager(){return(Out);};

  //Connection variable requests
  const std::vector<uint8_t> & GetLocalIP(){return(LocalIP);};
  const std::vector<uint8_t> & GetRemoteIP(){return(RemoteIP);};
  const int & GetSocketFD(){return(SocketFD);};
  const int & GetAddressFamily(){return(AddressFamily);};

 protected:

  //Packet interface
  DCNetPacketManager *In;  //Owned by this class.
  DCNetPacketManager *Out; //Owned by this class.  
  int OutPacketManagerFD[2];
  void SendPacket();
  void ListenForPacket();


  //DCDAQ message processing
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
 
  //Error monitoring
  std::deque<time_t> NetErrors;
  time_t BadPacketTimeout;
  uint32_t MaxNetErrorCount;
  void CheckForBadConnection(bool force = false);

  //Connection variables
  int SocketFD;
  std::vector<uint8_t> LocalIP;
  std::vector<uint8_t> RemoteIP;
  int AddressFamily;


  //Statistics variables.
  time_t lastUpdate;
  time_t now;
  uint32_t PacketsIn;
  uint32_t PacketsOut;
  uint64_t DataIn;
  uint64_t DataOut;
  uint32_t TimeStepPacketsIn;
  uint32_t TimeStepPacketsOut;
  uint64_t TimeStepDataIn;
  uint64_t TimeStepDataOut;
  void SendMessages(time_t now,time_t lastUpdate);

  //Parent Communication?
  DCThread * parent;
};

#endif
