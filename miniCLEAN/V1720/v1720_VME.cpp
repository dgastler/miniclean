#include "v1720.h"
#include "xmlHelper.h"

int v1720::a32d32r(uint32_t address,uint32_t &value)
{
  int CAENret = 0;
  CAENret = CAENVME_ReadCycle(Handle,address,(void*)&value,cvA32_U_DATA,cvD32);
  if(CAENret != 0)
    {
      fprintf(stderr,"%s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  return(CAENret);
}

int v1720::a32d32w(uint32_t address,uint32_t value)
{
  int CAENret = 0;
  CAENret = CAENVME_WriteCycle(Handle,address,(void*)&value,cvA32_U_DATA,cvD32);
  if(CAENret != 0)
    {
      fprintf(stderr,"%s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  return(CAENret);
}
int v1720::a32MBLTr(uint32_t address,char * buffer,uint32_t bufferSize,uint32_t &readSize)
{
  int CAENret = 0;
  int CAENreadSize = 0;
  CAENret = CAENVME_MBLTReadCycle(Handle,address,buffer,bufferSize,cvA32_U_MBLT,&CAENreadSize);
  readSize = CAENreadSize;
  if(CAENret != 0)
    {
      fprintf(stderr,"%s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  return(CAENret);
}

int v1720::SetRegister(uint32_t address,uint32_t value,char * Name)
{
  int ret = 0;
  uint32_t RegisterReturn = 0;
  //Write value
  ret = a32d32w(address,value);
  //Read it back
  ret += a32d32r(address,RegisterReturn);
  //Make sure everythign is correct.
  if(RegisterReturn != value)
    {
      fprintf(stderr,"%s write/readback error 0x%08X vs 0x%08X\n",Name,RegisterReturn,value);
      ret++;
    }
  return(ret);
}
