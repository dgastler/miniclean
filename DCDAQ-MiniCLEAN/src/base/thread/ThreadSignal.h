#ifndef __THREADSIGNAL__
#define __THREADSIGNAL__
#include <StackTracer.h>
#include <signal.h> //signals 
#include <unistd.h> //sleep()
#include <sys/types.h>
void thread_signal_handler(int sig);
#endif
