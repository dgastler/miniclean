#ifndef __DCMESSAGE__
#define __DCMESSAGE__

#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <string.h> //for memcpy

//for address types
#include <arpa/inet.h>

//For helpful statistics/error structures (used for data)
#include <MessageStructures.h>

namespace DCMessage 
{
  enum 
  {
    //Blank
    BLANKMESSAGE,

    //============DCDAQ==============
    //Universal
    STOP,
    PAUSE,
    GO,
   
    //DCLauncher
    LAUNCH,
    LAUNCHFAIL,
        
    //Run Number update
    RUN_NUMBER,
    
    //DCMessage interface
    REGISTER_TYPE,
    UNREGISTER_TYPE,
    REGISTER_ADDR,
    UNREGISTER_ADDR,

    GET_STATS,

    //============THREADS==============
    //error and statistic message
    ERROR,
    STATISTIC
  };
  
   
  //Non IPv* address defines
  enum 
  {
    AF_DC_LOCAL = -1,
    AF_DC_BROADCAST = -2
  };

  //This class exists to allow message passing with data 
  //of any size and type.   It is up to you to know what
  //you are putting in it and what you are getting back.
  //If you are using C STYLE STRINGS you can't forget to
  //address the null termination of your string. 
  const struct in_addr DefaultInAddr = {0};
  class Message
  {
  public:
    //===================================
    //Constructors
    //===================================
    Message(){Clear();};
    Message(const Message &message){CopyObj(message);};
    Message & operator=(const Message &rhs){CopyObj(rhs);return *this;};

    //===================================
    //User interfaces
    //===================================

    //Message type
    void SetType(uint16_t _type){type = _type; SetTime(time(NULL));};
    uint16_t GetType(){return(type);}
    
    //Set source and destination
    void SetSource(std::string name = "",
		   struct in_addr sin_addr = DefaultInAddr
		   );
    void GetSource(std::string & name,
		   struct in_addr &sin_addr
		   );


    void SetDestination(std::string name = "",
			struct in_addr sin_addr = DefaultInAddr
			);

    void GetDestination(std::string & name,
			struct in_addr& sin_addr
			);


    //Message time stamp
    void SetTime(time_t _timeStamp){timeStamp = _timeStamp;};
    DCtime_t GetTime(){return(timeStamp);};

    //============Direct interface to data============
    //Message data
    void SetData(const void * inData,size_t bytes){Set(inData,bytes,Data);};
    //Fill a passed vector with the data
    void GetData(std::vector<uint8_t> &returnData){returnData = Data;};
    //============Direct interface to data============

    //============Struct interface to data============
    //Set data using the struct T
    template<class T>
    void SetDataStruct(const T t){Set(&t,sizeof(t),Data);}
    //Get data assuming it is of type T
    template<class T>
    bool GetDataStruct(T & t)
    {
      //Check if the size is correct
      if(sizeof(t) != Data.size())
	return false;
      T * ptr = (T*) &(Data[0]);
      T referenceCopyOfType;
      //Check the magic number (type)
      if(referenceCopyOfType.type != ptr->type)
	return false;
      //Every check is positive so copy the message data and return
      memcpy(&t,&Data[0],sizeof(t));
      return true;
    }
    //============Struct interface to data============
    
    int StreamMessage(std::vector<uint8_t> &stream);
        
    void SetMessage(uint8_t * streamSource,unsigned int size);
    
  private:
    //===================================
    //Constructor clear function
    //===================================
    void Clear();
    //===================================
    //Copy function
    //===================================
    void CopyObj(const Message &rhs);
    //===================================
    //Set internal data
    //===================================
    void Set(const void * in,size_t bytes,std::vector<uint8_t> & store);
    void Add(const void * in, size_t bytes,std::vector<uint8_t> & store);
    //===================================
    //Data members
    //===================================
    uint16_t type;
    DCtime_t timeStamp;
    std::vector<uint8_t> Data;

    std::string DestinationName;
    struct in_addr DestinationAddr;
    
    std::string SourceName;
    struct in_addr SourceAddr;

  };
}
#endif
