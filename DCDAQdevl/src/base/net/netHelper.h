#ifndef __NETHELPER__
#define __NETHELPER__

#include <iostream>
#include <vector>
#include <cstdio>
#include <stdint.h>
#include <errno.h>
#include <string.h>

//read/write
#include <sys/utsname.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/un.h>  // for AF_LOCAL
#include <fcntl.h> //For fcntl and misc

//CheckLocal
#include <net/if.h>
#include <sys/ioctl.h>




#include <sstream>

int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint16_t size,
	       struct timeval timeout);

int32_t readN(int socketFD,
	       uint8_t * ptr,
	       uint16_t maxSize,
	       struct timeval timeout);

bool CheckLocal(std::string IP,int fd);

bool AddrFullCompare(struct sockaddr_in *addr1,
		     struct sockaddr_in *addr2);
bool AddrAddressCompare(struct in_addr *addr1,
			struct in_addr *addr2);



bool AsyncConnectCheck(int & fd);

bool SetNonBlocking(int &fd,bool value);

//bool BuildSockAddrVector(int AF,std::string address,std::vector<uint8_t> &addr);

//bool CompareAddrStructures(std::vector<uint8_t> &addr1,std::vector<uint8_t> & addr2);

//std::vector<uint8_t> FullAddrToAddress(int AF,std::vector<uint8_t> FullAddr);

#endif
