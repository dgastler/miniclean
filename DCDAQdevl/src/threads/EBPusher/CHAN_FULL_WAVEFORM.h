#ifndef __CHAN_FULL_WAVEFORM__
#define __CHAN_FULL_WAVEFORM__

#include <PacketBuilder.h>

class CHAN_FULL_WAVEFORM : public PacketBuilder
{
 public:
  CHAN_FULL_WAVEFORM()
  {
    ClearPacket();
  };
  virtual ~CHAN_FULL_WAVEFORM(){ClearPacket();};
  virtual bool Send(FEPCBlock * event)
  {
    printf("CHAN_FULL_WAVEFORM not written.  FAIL!\n");
    return(false);
  }
};

#endif
