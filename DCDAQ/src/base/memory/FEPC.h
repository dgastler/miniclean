#ifndef __FEPC__
#define __FEPC__

#include <LockedQueue.h>
#include <LockedVectorHash.h>

class FEPC
{
 public:
  FEPC(){};
  virtual ~FEPC(){Clear();};

  enum
  {
    BADVALUE = -1,
    LOCKVALUE = -2    
  };

  //Setup queues and define the size of the EventStore.
  void Setup(uint16_t Size)
  {
      Free.Setup(BADVALUE);
      UnSent.Setup(BADVALUE);          
      EventStore.Setup(Size,BADVALUE,LOCKVALUE);
      ToSend.Setup(BADVALUE);
  };


  //clear queues
  void Clear()
  {
    Free.Clear(true);
    UnSent.Clear(true);
    ToSend.Clear(true);
    EventStore.Clear(true);
  };

  //Shutdown all wait options in all Locked objects
  virtual void Shutdown()
  {
    Free.ShutdownWait();
    UnSent.ShutdownWait();
    ToSend.ShutdownWait();
    EventStore.ShutdownWait();
    WakeUp();
  };
  
  //Wakeup every Locked object (call after Shutdown())
  virtual void WakeUp()
  {
    Free.WakeUp();
    UnSent.WakeUp();
    ToSend.WakeUp();
    EventStore.WakeUp();
  };


  //=======================================================================
  //Queues
  //=======================================================================
  //Get functions.
  virtual bool GetFree(int32_t &index,bool wait = true)
  {    
    return(Free.Get(index,wait));
  };
  virtual bool GetUnSent(int32_t &index,bool wait = true)
  {
    return(UnSent.Get(index,wait));
  };
  virtual bool GetToSend(int32_t &index,bool wait = true)
  {
    return(ToSend.Get(index,wait));
  };

  //Add functions
  virtual bool AddFree  (int32_t & Index){return(Free.Add(Index,true));};
  virtual bool AddUnSent(int32_t & Index){return(UnSent.Add(Index,true));};
  virtual bool AddToSend(int32_t & Index){return(ToSend.Add(Index,true));};

  //=======================================================================
  //Event Store
  //=======================================================================
  virtual bool GetStoredEvent(uint16_t Event, int32_t &index, bool wait = true)
  {
    return(EventStore.Get(Event,index,wait));
  };
  virtual bool StoreEvent(uint16_t Event, int32_t &index)
  {
    return(EventStore.Put(Event,index,true));
  }
  virtual bool ClearEvent(uint16_t Event)
  {
    return(EventStore.Clear(Event,true));
  }

 private:
  LockedQueue<int32_t> Free;  //queue of indicies of free memory blocks
  LockedQueue<int32_t> UnSent;  //queue of indicies of un-sent memory blocks
  LockedVectorHash<int32_t> EventStore; //vector-hash of stored events.
  LockedQueue<int32_t> ToSend; //queue of indicies to send.
};
#endif
