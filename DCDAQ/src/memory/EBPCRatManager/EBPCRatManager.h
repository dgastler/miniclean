#ifndef __EBPCRATMANAGER__
#define __EBPCRATMANAGER__


#include <MemoryManager.h>
#include <EBPC.h>
#include <xmlHelper.h>
#include <EVBlock.h>
#include "RAT/DS/EV.hh"

class EBPCRatManager : public MemoryManager , public EBPC
{
 public:
  EBPCRatManager(std::string Name);
  ~EBPCRatManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  EVBlock * GetEVBlock(uint32_t iEV) 
    {
      if(iEV < Memory.size())
	{
	  return(&(Memory[iEV]));
      }
    return(NULL);
  }
 private: 
  uint32_t NumEV;
  uint32_t VectorHashSize;
  std::vector<EVBlock> Memory;
  void Deallocate();
};

#endif
