#include <MemBlockCompressor.h>

MemBlockCompressor::MemBlockCompressor(std::string Name)
{
  SetName(Name);
  SetType(std::string("MemBlockCompressor"));
  last = time(NULL);
  current = time(NULL);
}

bool MemBlockCompressor::Setup(xmlNode * SetupNode,
			   std::vector<MemoryManager*> &MemManager)
{
  Ready = false;
  xmlNode * rawNode = FindSubNode(SetupNode,"RAW");
  if(rawNode == NULL)
    {
      printf("Can't find RAW tag\n");
      return false;  
    }
  if(!FindMemoryManager(eventMM,rawNode,MemManager))
    {
      return false;
    }
  eventMM->IncrementListeningThreads();
  xmlNode * compressedNode = FindSubNode(SetupNode,"COMPRESSED");
  if(compressedNode == NULL)
    {
      printf("Can't find COMPRESSED tag\n");
      return false;
    }
  if(!FindMemoryManager(compressedMM,compressedNode,MemManager))
    {
      return false;
    } 
  compressedMM->IncrementWritingThreads();

  if(FindSubNode(SetupNode,"LEVEL") == NULL)
    {
      level = Z_DEFAULT_COMPRESSION;
    }
  else
    GetXMLValue(SetupNode,"LEVEL",GetName().c_str(),level);
  
  Ready = true;
  return true;
}

void MemBlockCompressor::MainLoop()
{
  if(IsReadReady(eventMM->GetFullFDs()[0]))
    {
      int32_t eIndex = FreeFullQueue::BADVALUE;
      int32_t cIndex = FreeFullQueue::BADVALUE;
      if(eventMM->GetFull(eIndex,true) &&
	 compressedMM->GetFree(cIndex,true))
	{
	  if(eIndex == FreeFullQueue::SHUTDOWN)
	    //This means that we should shut down
	    {
	      printf("MemBlockCompressor: We've been told to shut down\n");
	      if(eventMM->DeIncrementListeningThreads())
		{
		  eventMM->Shutdown();
		}
	      printf("MemBlockCompressor: De-incremented listening threads\n");
	      if(compressedMM->DeIncrementWritingThreads())
		{
		  printf("MemBlockCompressor: We're the last compressor thread!\n");
		  compressedMM->AddEndOfRun(eIndex);
		}
	      printf("MemBlockCompressor: De-incremented writing threads\n");
	      //Now shut down this thread
	      RemoveReadFD(eventMM->GetFullFDs()[0]);
	      Loop = false;
	      Running = false;
	      return;
	    }
	  if(eIndex < 0)
	    {
	      fprintf(stderr,"MemBlockCompressor: Failed in getting event block with %i\n",eIndex);
	      return;
	    }
	  if(cIndex < 0)
	    {
	      fprintf(stderr,"MemBlockCompressor: Failed in  getting compressed block with %i\n",cIndex);
	      return;
	    }
	  stream.zalloc = Z_NULL;
	  stream.zfree = Z_NULL;
	  stream.opaque = Z_NULL;
	  if (Z_OK == deflateInit(&stream, level))	   
	    {
	      MemoryBlock * event = eventMM->GetBlock(eIndex);
	      MemoryBlock * compressed = compressedMM->GetBlock(cIndex);


	      stream.next_in = (Bytef*) event->buffer;
	      stream.avail_in = (event->dataSize)<< 2;
	      stream.next_out = (Bytef*) compressed->buffer;
	      stream.avail_out = (compressed->allocatedSize)<<2;
	      int ret = deflate(&stream,Z_FINISH);
	      if(ret == Z_STREAM_END)
		{
		  deflateEnd(&stream);
		  compressed->uncompressedSize = event->dataSize;
		  eventMM->AddFree(eIndex);

		  compressed->dataSize = compressed->allocatedSize - (stream.avail_out>>2);
		
		 
		  compressedMM->AddFull(cIndex);
		  event = NULL;
		  compressed = NULL;
		}
	      else if(ret == Z_STREAM_ERROR)
		{
		  PrintWarning("An error happened compressing event, will try again later. ");
		  eventMM->AddFull(eIndex);
		  event = NULL;
		  compressedMM->AddFree(cIndex);
		  compressed = NULL;
		}
	      else 
		{
		  PrintError("An error happened compressing event, fail fail fail");
		  eventMM->AddFull(eIndex);
		  event = NULL;
		  compressedMM->AddFree(cIndex);
		  compressed = NULL;
		}
	    }
	  else
	    {
	      PrintError("GZIP deflateInit failed!\n");	      
	    }
	}      
    }

}

void MemBlockCompressor::ProcessTimeout()
{
  current = time(NULL);
    if(current - last > GetUpdateTime())
      {
	last = current;
      }
}

bool MemBlockCompressor::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      RemoveReadFD(eventMM->GetFullFDs()[0]);
      eventMM->Shutdown();
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      RemoveReadFD(eventMM->GetFullFDs()[0]);
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the Full queue's FD so we know when we have a new 
      //memory block to process
      AddReadFD(eventMM->GetFullFDs()[0]); //Add the FD to this threads mask
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return ret;
}
