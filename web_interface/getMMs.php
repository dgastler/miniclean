<?php
include "ui.class.php";
//corresponds to the getPCMMs function in ui.class.php
//returns the memory managers from a given pc for a given run number
if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_POST);
}

$pcName=$_POST['pcName'];
$runNumber=$_POST['runNumber'];
$MMs=$opt->getPCMMs($pcName,$runNumber);
echo json_encode($MMs) . PHP_EOL;
?>