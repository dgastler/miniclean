#include <MemoryManager.h>

void MemoryManager::PrintError(const std::string &text)
{
  DCMessage::Message message;
  message.SetDestination();
  message.SetType(DCMessage::ERROR);
  DCMessage::DCString dcString;
  std::vector<uint8_t> data = dcString.SetData(text);
  message.SetData(&(data[0]),data.size());
  SendMessageOut(message);
}

void MemoryManager::Print(const std::string &text)
{
  DCMessage::Message message;
  message.SetDestination();
  message.SetType(DCMessage::TEXT);
  DCMessage::DCString dcString;
  std::vector<uint8_t> data = dcString.SetData(text);
  message.SetData(&(data[0]),data.size());
  SendMessageOut(message);
}

DCMessage::Message MemoryManager::GetMessageOut()
{
  DCMessage::Message ret;
  ret.SetType(DCMessage::BLANK);
  MessagesOut.Get(ret);
  return ret;
}

void MemoryManager::SendMessageOut(DCMessage::Message &message)
{
  //Adding source information (default remote address is set by DCMessage)
  message.SetSource(GetName());
  //Add a message to the queue.
  MessagesOut.Add(message);
}
