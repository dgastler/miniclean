#ifdef __RATDS__
#include <PacketBuilder.h>

int PacketBuilder::AddEventHeader(RATEVBlock * event)
{
  if(sizeof(EBPacket::Event) < dataSizeLeft)
    //there is enough room for the header
    {
      ebEvent = (EBPacket::Event *) dataPtr;

      ebEvent->eventID = event->eventID;
      ebEvent->eventTime = event->eventTime;
      ebEvent->eventTriggerCounts = event->eventTriggers;
      ebEvent->eventTriggerPattern = 0;
      ebEvent->boardCount = 0;
            
      ebEvent->dataType = event->reductionLevel;
      //move forward in our array
      currentPtr += sizeof(EBPacket::Event); 
      //update the free space available. 	      
      dataSizeLeft -= sizeof(EBPacket::Event); 
    }
  else 
    //not enough room for the header.   (BAD!)
    {
      return(-1);
    }
  return(0);
}

bool PacketBuilder::Send(RATEVBlock * event)
{
  //First check that we have a valid packet manager
  if((dynamic_cast<DCNetPacketManager *>(packetManager)) == NULL)
    {
      fprintf(stderr,"DCNetPacketManger not set!\n");
      return false;
    }


  bool ret = false; 
  //====================================
  //Build and fill packet(s) from this EV
  //====================================
  int8_t reductionLevel = event->reductionLevel;

   //Split between summary and per channel event
  if((ReductionLevel::EVNT_FULL_WAVEFORM == reductionLevel) ||
     (ReductionLevel::EVNT_ZLE_WAVEFORM  == reductionLevel) ||
     (ReductionLevel::EVNT_ZLE_INTEGRAL  == reductionLevel) ||
     (ReductionLevel::EVNT_PROMPT_TOTAL  == reductionLevel))
    {
      ret = SendSummaryEvent(event);
    }
  else if((ReductionLevel::CHAN_FULL_WAVEFORM == reductionLevel) ||
	  (ReductionLevel::CHAN_ZLE_WAVEFORM  == reductionLevel) ||
	  (ReductionLevel::CHAN_ZLE_INTEGRAL  == reductionLevel) ||
	  (ReductionLevel::CHAN_PROMPT_TOTAL  == reductionLevel))
    
    {
      ret = SendChanEvent(event);

    }
 
  //Send the last packet if needed
  if(packet != NULL)
    {
      //Send out our packet. 
      if(packetNumber > 0)
	SendPacket(packetTypePartial);
      else
	SendPacket(packetTypeFull);
      ClearPacket();
    }	    
  //  return(true);
  return ret;
}

bool PacketBuilder::SendSummaryEvent(RATEVBlock * event)
{
  packetTypeFull    = DCNetPacket::BAD_PACKET;
  packetTypePartial = DCNetPacket::BAD_PACKET;
  fprintf(stderr,"Event level reduction not yet implemented\n!");
  return false;
}

bool PacketBuilder::SendChanEvent(RATEVBlock * event)
{
  bool (PacketBuilder::*CheckBoardSize)( RATEVBlock * block,RAT::DS::Board * board) = NULL;
  void (PacketBuilder::*AddChannel)(RAT::DS::Channel * channel,int16_t PMTID) = NULL;

  switch (event->reductionLevel)
    {
    case ReductionLevel::CHAN_ZLE_WAVEFORM:
    case ReductionLevel::CHAN_FULL_WAVEFORM:
      packetTypeFull    = DCNetPacket::EB_ZLE_WAVEFORMS_FULL_PACKET;
      packetTypePartial = DCNetPacket::EB_ZLE_WAVEFORMS_PARTIAL_PACKET;
      CheckBoardSize = &PacketBuilder::CheckBoardSize_CHAN_ZLE_WAVEFORM;
      AddChannel = &PacketBuilder::AddChannel_CHAN_ZLE_WAVEFORM;
      break;
    case ReductionLevel::CHAN_ZLE_INTEGRAL:
      packetTypeFull    = DCNetPacket::EB_INTEGRATED_WINDOWS_FULL_PACKET;
      packetTypePartial = DCNetPacket::EB_INTEGRATED_WINDOWS_PARTIAL_PACKET;
      CheckBoardSize = &PacketBuilder::CheckBoardSize_CHAN_ZLE_INTEGRAL;
      AddChannel = &PacketBuilder::AddChannel_CHAN_ZLE_INTEGRAL;
      break;
    case ReductionLevel::CHAN_PROMPT_TOTAL:
      packetTypeFull    = DCNetPacket::EB_PROMPT_TOTAL_FULL_PACKET;
      packetTypePartial = DCNetPacket::EB_PROMPT_TOTAL_PARTIAL_PACKET;
      CheckBoardSize = &PacketBuilder::CheckBoardSize_CHAN_PROMPT_TOTAL;
      AddChannel = &PacketBuilder::AddChannel_CHAN_PROMPT_TOTAL;
      break;	
    }


  //Get the Board count for this EV and add them to the packet(s)
  int boardCount = event->ev->GetBoardCount();
  for(int iBoard = 0; iBoard < boardCount;iBoard++)
    {
      //Check if we have room for this board and 
      //send/create new packet if there isn't
      RAT::DS::Board * board = event->ev->GetBoard(iBoard);
      if((*this.*CheckBoardSize)(event,board))
	{
	  //There is enough size in the packet
	  //Setup the board object
	  EBPacket::Board * ebBoard = (EBPacket::Board *)currentPtr;
	  currentPtr += sizeof(EBPacket::Board);
	  dataSizeLeft -= sizeof(EBPacket::Board);
	  
	  ebBoard->header[0] = board->GetHeader()[0];
	  ebBoard->header[1] = board->GetHeader()[1];
	  ebBoard->header[2] = board->GetHeader()[2];
	  ebBoard->header[3] = board->GetHeader()[3];

	  ebBoard->channelPattern = 0;

	  //Loop over channels and add them
	  RAT::DS::Channel * channel = NULL;
	  int channelCount = board->GetChannelCount();
	  //fprintf(stderr,"Loop over channels\n");
	  for(int iChannel = 0; iChannel < channelCount;iChannel++)
	    {
	      channel = board->GetChannel(iChannel);
	      //Add board to packet
	      (*this.*AddChannel)(channel,detectorInfo.GetChannelMap(board->GetID(),iChannel));
	      ebBoard->channelPattern |= uint8_t(0x1 << channel->GetInput());	
	      //fprintf(stderr,"Channel pattern: %u\n",ebBoard->channelPattern);
	    }
	  ebEvent->boardCount++;
	  ebEvent->eventTriggerPattern = uint16_t((ebBoard->header[1]) >> 8);
	}
      else
	{
	  //Something bad happened
	  fprintf(stderr,"CheckBoardSize returned false!\n");
	  return false;
	}
    }
  return true;
}



void PacketBuilder::ResetPacket()
{
  packet = NULL;         
  dataPtr = NULL;  
  currentPtr = NULL;
  dataSizeLeft = 0; 
  iDCPacket = FreeFullQueue::BADVALUE;
}

void PacketBuilder::ClearPacket()
{
  ResetPacket();
  packetNumber = 0;
}

void PacketBuilder::SendPacket(int packetType)
{  
  //fprintf(stderr,"SendPacket called\n");
  //Set the type for this packet
  packet->SetType(packetType);
  //Set the data for this packet
  packet->SetData(dataPtr,
		  uint32_t(currentPtr -
			   dataPtr),
		  packetNumber);	      
  //Send out the current packet
  packetManager->AddFull(iDCPacket);
  //Clear our local packet info
  ResetPacket();
  //Increment packet number
  packetNumber++;  
  //fprintf(stderr,"SendPacket done\n");
}

bool PacketBuilder::CheckPacket(uint32_t size, RATEVBlock * event)
{
  //Check if we have a packet to send
  if(packet != NULL)
    {
      //fprintf(stderr,"Let's send the packet\n");
      SendPacket(packetTypePartial);
    }	  
  //Get a free packet
  packetManager->GetFree(iDCPacket,true);
  packet = packetManager->GetPacket(iDCPacket);
  //Set up our arrays
  dataPtr = packet->GetData();
  currentPtr = dataPtr; 
  dataSizeLeft = packet->GetMaxDataSize();  
  
  //Add the event header
  if(AddEventHeader(event) != 0)
    {
      //fprintf(stderr,"AddEventHeader failed\n");
      return false ;
    }
  
  //Check if there is room in the packet for our channel data
  if(size > dataSizeLeft)
    {
      fprintf(stderr,"Not enough room left for channel data\n");
      fprintf(stderr,"Size: %u; Data size left: %u\n",size,dataSizeLeft);
      return false ;
    }
  return true;
}


#endif
