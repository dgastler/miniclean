
/*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/

/*Waveform.cpp is the source file for the Waveform class, private and public variables and functions are declared in the Waveform.h header file*/

#include "CLEANViewer.h"

//Constructor for Waveform class without PMT ID
Waveform::Waveform(sZLEwindow Raw, bool _DebugSwitch)
{
  DEBUG = _DebugSwitch;

  if(DEBUG) fprintf(stderr,"Waveform::Waveform(sZLEwindow Raw, bool _DebugSwitch)\n");

  zleWindows.push_back(Raw);
  
  
  FindMaxSample(false);
  FindMinSample(false);
  
  FindMaxADC();
  FindMinADC();

  FindMaxQCalib();
  FindMinQCalib();
   
  ID = -1;
}

//Constructor for Waveform class with PMT ID
Waveform::Waveform(sZLEwindow Raw, bool _DebugSwitch, int id)
{
  DEBUG = _DebugSwitch;

  if(DEBUG) fprintf(stderr,"Waveform::Waveform(sZLEwindow Raw, bool _DebugSwitch, int id)\n");
  
  zleWindows.push_back(Raw);
  
  
  FindMaxSample(true);
  FindMinSample(true);
  
  FindMaxSample(false);
  FindMinSample(false);
  
  FindMaxADC();
  FindMinADC();

  FindMaxQCalib();
  FindMinQCalib();
    
  ID = id;
}

//Destructor for Waveform class
Waveform::~Waveform() { };

void Waveform::Process()
{
  FindMaxADC();
  FindMinADC();

  FindMaxQCalib();
  FindMinQCalib();
 

}
//simple function to set/change PMT ID for the Waveform
void Waveform::SetID(int id)
{
  if(DEBUG) fprintf(stderr,"Waveform::SetID(int id)\n");

  ID = id;
}

//function to set QTotal for Waveform

void Waveform::SetQTotal(int qTotal)
{

  QTotal=qTotal;

}

//Returns the PMT ID for the Waveform
int Waveform::GetID()
{


  return ID;
}

//Returns the Max ADC of all ZLE windows for the Waveform
double Waveform::GetMaxADC()
{
  if(DEBUG) fprintf(stderr,"Waveform::GetMaxADC()\n");

  return maxADC;
}

//Returns the Min ADC of all ZLE windows for the Waveform
double Waveform::GetMinADC()
{
  if(DEBUG) fprintf(stderr,"Waveform::GetMinADC()\n");

  return minADC;
}

//Returns the Max Sample of all ZLE windows for the Waveform
double Waveform::GetMaxSample(bool calib)
{
  if(DEBUG) fprintf(stderr,"Waveform::GetMaxSample()\n");
  
 
  return maxSample;
}

//Returns the Min Sample of all ZLE windows for the Waveform
double Waveform::GetMinSample(bool calib)
{
  if(DEBUG) fprintf(stderr,"Waveform::GetMinSample()\n");
 

  return minSample;
}

double Waveform::GetMaxQCalib()
{
  if(DEBUG) fprintf(stderr,"Waveform::GetMaxQCalib()\n");

  return maxQCalib;
}

double Waveform::GetMinQCalib()
{
  if(DEBUG) fprintf(stderr,"Waveform::GetMinQCalib()\n");

  return minQCalib;
}

//Returns QTotal
double Waveform::GetQTotal()
{
  return QTotal;

}

/*Returns the vector of sZLEwindows with the correct coordinates for plotting with a TPolyLine*/
//see Waveform.h for the sZLEwindow struct
std::vector <sTransformZLEwindow> Waveform::GetTransform()
{
  if(DEBUG) fprintf(stderr,"Waveform::GetTransform()\n");

  return transformZLEWindows;
}

std::vector <sTransformCalibwindow> Waveform::GetCalibTransform()
{
  return transformCalibWindows;
}

std::vector<sZLEwindow> Waveform::GetWindows()
{
  return zleWindows;
}

bool Waveform::InsertZLE(sZLEwindow window, size_t index)
{
  if(index<=zleWindows.size())
    {
     
      zleWindows[index]=window;
      
      return true;
    }
  return false;
}

//Function for adding a ZLE window to the Wavform
//see Waveform.h for the sZLEwindow struct
void Waveform::AddZLE(sZLEwindow Raw)
{
  if(DEBUG) fprintf(stderr, "Waveform::AddZLE(sZLEwindow Raw)\n");

  zleWindows.push_back(Raw);
  
  FindMaxSample(true);
  FindMinSample(true);
  FindMaxSample(false);
  FindMinSample(false);
  FindMaxADC();
  FindMinADC();
  FindMaxQCalib();
  FindMinQCalib();
}

/*Transforms the corrdinates from ADCs and Samples to percentage of canvas, This is necessary for plotting with a TPolyLine*/
void Waveform::TransformRaw(int canvasMinX, int canvasMaxX, int nPlots, int whichPMT)
{
  if(DEBUG) fprintf(stderr,"Waveform::TransformRaw(int canvasMinX, int canvasMaxX, int nPlots, int whichPMT)\n");
  
  transformZLEWindows.clear();
 
  double rangeSample = canvasMaxX - canvasMinX;
  /*find the vertical percentage of the canvas that each PMT waveform has to fit into (the total vertical percentage of the entire canvas that will be used is 0.8)*/
  double verticalPlotPercent = 0.8/nPlots;
  double rangeADC = maxADC - minADC;
 
  //copy the zleWindows vector so that the raw data is preserved
 
  for (size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      sTransformZLEwindow tempTransformWindow;
      transformZLEWindows.push_back(tempTransformWindow);
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].ADCs.size(); iIndex++)
	{
	  transformZLEWindows[iZLE].ADCs.push_back(double(zleWindows[iZLE].ADCs[iIndex]));
	}
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].Samples.size(); iIndex++)
	{
	  transformZLEWindows[iZLE].Samples.push_back(double(4.0*zleWindows[iZLE].Samples[iIndex]));
	}
    }
 
  for (size_t iZLE = 0; iZLE < transformZLEWindows.size(); iZLE++)
    {
      //Change the x coordinates from sample numbers to canvas percentage
      //the total canvas percentage used for plotting in the x direction is 0.8
      for (size_t iSample = 0; 
	   iSample < transformZLEWindows[iZLE].Samples.size(); 
	   iSample++)
	{
	  /* X coordinate transformation follows the equation: Xtrans = (((X - Xmin)/deltaX)*0.8) + Xoffset */
	  transformZLEWindows[iZLE].Samples[iSample] = transformZLEWindows[iZLE].Samples[iSample] - canvasMinX;
	  transformZLEWindows[iZLE].Samples[iSample] = (transformZLEWindows[iZLE].Samples[iSample]/rangeSample) * 0.8;
	  /*offset of 0.15 gives a 0.15 left margin, and a 0.05 right margin on the canvas*/
	  transformZLEWindows[iZLE].Samples[iSample] = transformZLEWindows[iZLE].Samples[iSample] + 0.15;
	}
      //Change the y coordinates from ADC counts to canvas percentage
      //the total canvas percentage used for plotting in the y direction is 0.8
      for (size_t iADC = 0; 
	   iADC < transformZLEWindows[iZLE].ADCs.size();
	   iADC++)
	{
	  /* Y coordinate transformation follows the equation: Ytrans = (((Y - Ymin)/deltaY)*(0.8/number of plots)) + Yoffset + (PMT offset)*/
	  transformZLEWindows[iZLE].ADCs[iADC] = transformZLEWindows[iZLE].ADCs[iADC]-minADC;
	  transformZLEWindows[iZLE].ADCs[iADC] = (transformZLEWindows[iZLE].ADCs[iADC]/rangeADC)*verticalPlotPercent;
	  /*offset of 0.1 gives a 0.1 top and bottom margin on the canvas*/
	  transformZLEWindows[iZLE].ADCs[iADC] = transformZLEWindows[iZLE].ADCs[iADC] + 0.1;
	  /*(PMT offset)=(whichPMT * (0.8/nPlots)), vertically seperates the TPolyLines so that they are not plotted ontop of each other*/
	  transformZLEWindows[iZLE].ADCs[iADC] = transformZLEWindows[iZLE].ADCs[iADC] + (whichPMT * verticalPlotPercent);
	}
    }
}

/*Overloaded functino for transforming the corrdinates from ADCs and Samples to percentage of canvas in the case of a common y-axis scale, This is necessary for plotting with a TPolyLine*/
void Waveform::TransformRaw(int canvasMinX, int canvasMaxX, int canvasMinY, int canvasMaxY, int nPlots, int whichPMT)
{
  if(DEBUG) fprintf(stderr,"Waveform::TransformRaw(int canvasMinX, int canvasMaxX, int canvasMinY, int canvasMaxY, int nPlots, int whichPMT)\n");
  
  transformZLEWindows.clear();
 
  double rangeSample = canvasMaxX - canvasMinX;
  /*find the vertical percentage of the canvas that each PMT waveform has to fit into (the total vertical percentage of the entire canvas that will be used is 0.8)*/
  double verticalPlotPercent = 0.8/nPlots;
  double rangeADC = canvasMaxY - canvasMinY;
 
  //copy the zleWindows vector so that the raw data is preserved
 
  for (size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      sTransformZLEwindow tempTransformWindow;
      transformZLEWindows.push_back(tempTransformWindow);
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].ADCs.size(); iIndex++)
	{
	  transformZLEWindows[iZLE].ADCs.push_back(double(zleWindows[iZLE].ADCs[iIndex]));
	}
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].Samples.size(); iIndex++)
	{
	  transformZLEWindows[iZLE].Samples.push_back(double(4.0*zleWindows[iZLE].Samples[iIndex]));
	}
    }
 
  for (size_t iZLE = 0; iZLE < transformZLEWindows.size(); iZLE++)
    {
      //Change the x coordinates from sample numbers to canvas percentage
      //the total canvas percentage used for plotting in the x direction is 0.8
      for (size_t iSample = 0; 
	   iSample < transformZLEWindows[iZLE].Samples.size(); 
	   iSample++)
	{
	  /* X coordinate transformation follows the equation: Xtrans = (((X - Xmin)/deltaX)*0.8) + Xoffset */
	  transformZLEWindows[iZLE].Samples[iSample] = transformZLEWindows[iZLE].Samples[iSample] - canvasMinX;
	  transformZLEWindows[iZLE].Samples[iSample] = (transformZLEWindows[iZLE].Samples[iSample]/rangeSample) * 0.8;
	  /*offset of 0.15 gives a 0.15 left margin, and a 0.05 right margin on the canvas*/
	  transformZLEWindows[iZLE].Samples[iSample] = transformZLEWindows[iZLE].Samples[iSample] + 0.15;
	}
      //Change the y coordinates from ADC counts to canvas percentage
      //the total canvas percentage used for plotting in the y direction is 0.8
      for (size_t iADC = 0; 
	   iADC < transformZLEWindows[iZLE].ADCs.size();
	   iADC++)
	{
	  /* Y coordinate transformation follows the equation: Ytrans = (((Y - Ymin)/deltaY)*(0.8/number of plots)) + Yoffset + (PMT offset)*/
	  transformZLEWindows[iZLE].ADCs[iADC] = transformZLEWindows[iZLE].ADCs[iADC]-canvasMinY;
	  transformZLEWindows[iZLE].ADCs[iADC] = (transformZLEWindows[iZLE].ADCs[iADC]/rangeADC)*verticalPlotPercent;
	  /*offset of 0.1 gives a 0.1 top and bottom margin on the canvas*/
	  transformZLEWindows[iZLE].ADCs[iADC] = transformZLEWindows[iZLE].ADCs[iADC] + 0.1;
	  /*(PMT offset)=(whichPMT * (0.8/nPlots)), vertically seperates the TPolyLines so that they are not plotted ontop of each other*/
	  transformZLEWindows[iZLE].ADCs[iADC] = transformZLEWindows[iZLE].ADCs[iADC] + (whichPMT * verticalPlotPercent);
	}
    }
}

void Waveform::TransformCalib(int canvasMinX, int canvasMaxX, int nPlots, int whichPMT)
{
  if(DEBUG) fprintf(stderr,"Waveform::TransformCalib(int canvasMinX, int canvasMaxX, int nPlots, int whichPMT)\n");
  
  transformCalibWindows.clear();
 
  double rangeSample = canvasMaxX - canvasMinX;
  /*find the vertical percentage of the canvas that each PMT waveform has to fit into (the total vertical percentage of the entire canvas that will be used is 0.8)*/
  double verticalPlotPercent = 0.8/nPlots;
  double rangeQCalib = maxQCalib - minQCalib;
 
  //copy the zleWindows vector so that the raw data is preserved
 
  for (size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      sTransformCalibwindow tempTransformWindow;
      transformCalibWindows.push_back(tempTransformWindow);
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].QCalib.size(); iIndex++)
	{
	  transformCalibWindows[iZLE].QCalib.push_back(double(zleWindows[iZLE].QCalib[iIndex]));

	}
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].CalibSamples.size(); iIndex++)
	{
	  transformCalibWindows[iZLE].CalibSamples.push_back(double(4.0*zleWindows[iZLE].CalibSamples[iIndex]));
	}
    }
 
  for (size_t iZLE = 0; iZLE < transformCalibWindows.size(); iZLE++)
    {
      //Change the x coordinates from sample numbers to canvas percentage
      //the total canvas percentage used for plotting in the x direction is 0.8
      for (size_t iSample = 0; 
	   iSample < transformCalibWindows[iZLE].CalibSamples.size(); 
	   iSample++)
	{
	  /* X coordinate transformation follows the equation: Xtrans = (((X - Xmin)/deltaX)*0.8) + Xoffset */
	  transformCalibWindows[iZLE].CalibSamples[iSample] = transformCalibWindows[iZLE].CalibSamples[iSample] - canvasMinX;
	  transformCalibWindows[iZLE].CalibSamples[iSample] = (transformCalibWindows[iZLE].CalibSamples[iSample]/rangeSample) * 0.8;
	  /*offset of 0.15 gives a 0.15 left margin, and a 0.05 right margin on the canvas*/
	  transformCalibWindows[iZLE].CalibSamples[iSample] = transformCalibWindows[iZLE].CalibSamples[iSample] + 0.15;
	}
      //Change the y coordinates from ADC counts to canvas percentage
      //the total canvas percentage used for plotting in the y direction is 0.8
      for (size_t iQCalib = 0; 
	   iQCalib < transformCalibWindows[iZLE].QCalib.size();
	   iQCalib++)
	{
	  /* Y coordinate transformation follows the equation: Ytrans = (((Y - Ymin)/deltaY)*(0.8/number of plots)) + Yoffset + (PMT offset)*/
	  transformCalibWindows[iZLE].QCalib[iQCalib] = transformCalibWindows[iZLE].QCalib[iQCalib]-minQCalib;
	  transformCalibWindows[iZLE].QCalib[iQCalib] = (transformCalibWindows[iZLE].QCalib[iQCalib]/rangeQCalib)*verticalPlotPercent;
	  /*offset of 0.1 gives a 0.1 top and bottom margin on the canvas*/
	  transformCalibWindows[iZLE].QCalib[iQCalib] = transformCalibWindows[iZLE].QCalib[iQCalib] + 0.1;
	  /*(PMT offset)=(whichPMT * (0.8/nPlots)), vertically seperates the TPolyLines so that they are not plotted ontop of each other*/
	  transformCalibWindows[iZLE].QCalib[iQCalib] = transformCalibWindows[iZLE].QCalib[iQCalib] + (whichPMT * verticalPlotPercent);
	}
    }
}

/*Overloaded functino for transforming the corrdinates from ADCs and Samples to percentage of canvas in the case of a common y-axis scale, This is necessary for plotting with a TPolyLine*/
void Waveform::TransformCalib(int canvasMinX, int canvasMaxX, double canvasMinY, double canvasMaxY, int nPlots, int whichPMT)
{
  if(DEBUG) fprintf(stderr,"Waveform::TransformCalib(int canvasMinX, int canvasMaxX, int canvasMinY, int canvasMaxY, int nPlots, int whichPMT)\n");
  
  transformCalibWindows.clear();
 
  double rangeSample = canvasMaxX - canvasMinX;
  /*find the vertical percentage of the canvas that each PMT waveform has to fit into (the total vertical percentage of the entire canvas that will be used is 0.8)*/
  double verticalPlotPercent = 0.8/nPlots;
  double rangeQCalib = canvasMaxY - canvasMinY;
 
  //copy the zleWindows vector so that the raw data is preserved
 
  for (size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      sTransformCalibwindow tempTransformWindow;
      transformCalibWindows.push_back(tempTransformWindow);
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].QCalib.size(); iIndex++)
	{
	  transformCalibWindows[iZLE].QCalib.push_back(double(zleWindows[iZLE].QCalib[iIndex]));
	 
	}
      for (size_t iIndex = 0; iIndex < zleWindows[iZLE].CalibSamples.size(); iIndex++)
	{
	  transformCalibWindows[iZLE].CalibSamples.push_back(double(4.0*zleWindows[iZLE].CalibSamples[iIndex]));
	}
    }
 
  for (size_t iZLE = 0; iZLE < transformCalibWindows.size(); iZLE++)
    {
     
      //Change the x coordinates from sample numbers to canvas percentage
      //the total canvas percentage used for plotting in the x direction is 0.8
      for (size_t iSample = 0; 
	   iSample < transformCalibWindows[iZLE].CalibSamples.size(); 
	   iSample++)
	{
	  /* X coordinate transformation follows the equation: Xtrans = (((X - Xmin)/deltaX)*0.8) + Xoffset */
	  transformCalibWindows[iZLE].CalibSamples[iSample] = transformCalibWindows[iZLE].CalibSamples[iSample] - canvasMinX;
	  transformCalibWindows[iZLE].CalibSamples[iSample] = (transformCalibWindows[iZLE].CalibSamples[iSample]/rangeSample) * 0.8;
	  /*offset of 0.15 gives a 0.15 left margin, and a 0.05 right margin on the canvas*/
	  transformCalibWindows[iZLE].CalibSamples[iSample] = transformCalibWindows[iZLE].CalibSamples[iSample] + 0.15;
	 
	}
      
      //Change the y coordinates from ADC counts to canvas percentage
      //the total canvas percentage used for plotting in the y direction is 0.8
      for (size_t iQCalib = 0; 
	   iQCalib < transformCalibWindows[iZLE].QCalib.size();
	   iQCalib++)
	{
	  
	  /* Y coordinate transformation follows the equation: Ytrans = (((Y - Ymin)/deltaY)*(0.8/number of plots)) + Yoffset + (PMT offset)*/
	  transformCalibWindows[iZLE].QCalib[iQCalib] = transformCalibWindows[iZLE].QCalib[iQCalib]-canvasMinY;
	  
	  transformCalibWindows[iZLE].QCalib[iQCalib] = (transformCalibWindows[iZLE].QCalib[iQCalib]/rangeQCalib)*verticalPlotPercent;
	 
	  /*offset of 0.1 gives a 0.1 top and bottom margin on the canvas*/
	  transformCalibWindows[iZLE].QCalib[iQCalib] = transformCalibWindows[iZLE].QCalib[iQCalib] + 0.1;
	  /*(PMT offset)=(whichPMT * (0.8/nPlots)), vertically seperates the TPolyLines so that they are not plotted ontop of each other*/
	  transformCalibWindows[iZLE].QCalib[iQCalib] = transformCalibWindows[iZLE].QCalib[iQCalib] + (whichPMT * verticalPlotPercent);
	 
	}
    }
}


//Function for finding the Max ADC of all the ZLE windows in the Waveform
void Waveform::FindMaxADC()
{
  if(DEBUG) fprintf(stderr,"Waveform::FindMaxADC()\n");
  
  /*initially the maxADC is assigned to the first sample of the first ZLE window*/
  maxADC = zleWindows[0].ADCs[0];

  /*loop through all the ZLE windows to find the absolute maxADC for the Waveform*/
  for(size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      for(size_t iADC = 0; iADC < zleWindows[iZLE].ADCs.size(); iADC++)
	{
	  if (zleWindows[iZLE].ADCs[iADC] > maxADC)
	    maxADC = zleWindows[iZLE].ADCs[iADC];
	}
    }


}

//Function for finding the Min ADC of all the ZLE windows in the Waveform
void Waveform::FindMinADC()
{
  if(DEBUG) fprintf(stderr,"Waveform::FindMinADC()\n");

  /*initially the minADC is assigned to the first sample of the first ZLE window*/
  minADC = zleWindows[0].ADCs[0];

  /*loop through all the ZLE windows to find the absolute minADC for the Waveform*/
  for(size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      for(size_t iADC = 0; iADC < zleWindows[iZLE].ADCs.size(); iADC++)
	{
	  if (zleWindows[iZLE].ADCs[iADC] < minADC)
	    minADC = zleWindows[iZLE].ADCs[iADC];
	}
    }
}

/*Function for finding the Max Sample number of all the ZLE windows in the Waveform*/
void Waveform::FindMaxSample(bool calib)
{
  if(DEBUG) fprintf(stderr,"Waveform::FindMaxSample()\n");
 
      /*initially the max sample number is assigned to the last sample of the first ZLE window*/  
      if(zleWindows[0].Samples.size()!=0)
	{
	  maxSample = zleWindows[0].Samples.back();
	}
      
      /*loop through all the ZLE windows to find the absolute max sample number for the Waveform*/
      for(size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
	{
	  if (zleWindows[iZLE].Samples.back() > maxSample)
	    /*Since the last sample number of each ZLE window is gaurenteed to be the max sample number of that window we only need to check those sample numbers for each ZLE window*/
	    {
	      maxSample = zleWindows[iZLE].Samples.back();
	    }
	}
      maxSample=4.0*maxSample; //convert to ns
}
/*Function for finding the Min Sample number of all the ZLE windows in the Waveform*/
void Waveform::FindMinSample(bool calib)
{
  if(DEBUG) fprintf(stderr,"Waveform::FindMinSample()\n");
      /*initially the min sample number is assigned to the first sample of the first ZLE window*/
      minSample = zleWindows[0].Samples[0];

      /*loop through all the ZLE windows to find the absolute min sample number for the Waveform*/
      for(size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
	{
	  if (zleWindows[iZLE].Samples[0] < minSample)
	    /*Since the first sample number of each ZLE window is gaurenteed to be the min sample number of that window we only need to check those sample numbers for each ZLE window*/
	    minSample = zleWindows[iZLE].Samples[0];
	}
      minSample=4.0*minSample;//convert to ns

}

void Waveform::FindMaxQCalib()
{
  if(DEBUG) fprintf(stderr,"Waveform::FindMaxQCalib()\n");
  
  /*initially the maxADC is assigned to the first sample of the first ZLE window*/
  maxQCalib = -999999.9;

  /*loop through all the ZLE windows to find the absolute maxADC for the Waveform*/
  for(size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      for(size_t iQCalib = 0; iQCalib < zleWindows[iZLE].QCalib.size(); iQCalib++)
	{
	  if (zleWindows[iZLE].QCalib[iQCalib] > maxQCalib)
	    maxQCalib = zleWindows[iZLE].QCalib[iQCalib];
	}
    }
}

void Waveform::FindMinQCalib()
{
  if(DEBUG) fprintf(stderr,"Waveform::FindMinQCalib()\n");
  
  /*initially the maxADC is assigned to the first sample of the first ZLE window*/
  minQCalib = 999999.9;

  /*loop through all the ZLE windows to find the absolute maxADC for the Waveform*/
  for(size_t iZLE = 0; iZLE < zleWindows.size(); iZLE++)
    {
      for(size_t iQCalib = 0; iQCalib < zleWindows[iZLE].QCalib.size(); iQCalib++)
	{
	  if (zleWindows[iZLE].QCalib[iQCalib] < minQCalib)
	    minQCalib = zleWindows[iZLE].QCalib[iQCalib];
	}
    }
}
