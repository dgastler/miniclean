#ifdef __RATDS__
#include <DecisionMaker.h>
#include <RAT/DS/ReductionLevel.hh>

void DecisionMaker::ComputeDecision(RATEVBlock * event)
{
  //Calling this function means we have an event and should ++ rawEventRate
  rawEventRate++;

  RAT::DS::EV* ev=event->ev;  
  int64_t eventID=-1;
  
  bool promptTotalMode = false;
  if(event->reductionLevel==4)
    {
      promptTotalMode = true;
    }

  //Update the channel ZLE Window table. 
  for(int iBoard = 0; iBoard < ev->GetBoardCount();iBoard++)
    {
      RAT::DS::Board * board = ev->GetBoard(iBoard);
      for(int iChan = 0; iChan < board->GetChannelCount();iChan++)
	{
	  RAT::DS::Channel * chan = board->GetChannel(iChan);
	  channelZLEWindows->Fill(chan->GetID(),chan->GetRawBlockCount());
	}
    }  

  //Check the trigger pattern of this event to decide what to do with it
  uint8_t triggerPattern = ev->GetTriggerPattern();
  if(checkHeaders!=1)
    {
      //If we are in fake v1720 mode, we don't want to check this. 
      triggerPattern = 2;
    }
  
  //Start reduction level with the software trigger
  int8_t reductionLevel;// = ReductionLevel::COUNT;
  reductionLevel=softwareTrigger->SetReductionLevel(ev,eventID,promptTotalMode);

  //If somehow there is no trigger pattern, save it as a ZLE waveform
  if(triggerPattern == 0)
    {
      reductionLevel = ReductionLevel::CHAN_ZLE_WAVEFORM;
    }
  else
    {
      //fprintf(stderr,"Trigger pattern:0x%08X\n",triggerPattern);
      for(int trig = 0; trig < HardwareTrigger::COUNT; trig++)
	{
	  if(triggerPattern & (1 << trig))
	    {
	      //This trigger is present in the trigger pattern
	      triggerCount[trig]++;
	      if((reductionSettings[trig].reductionLevel < reductionLevel) ||
		 (!reductionSettings[trig].softwareTrigger))
		{
		  reductionLevel = reductionSettings[trig].reductionLevel;
		}
	    }
	}
    }
  event->reductionLevel=reductionLevel;

  //Add decision to our reduction level count	      
  reductionLevelCount[event->reductionLevel]++;
  RAT::DS::TriggerSummary softwareTrig = ev->GetTriggerSummary();
  std::map<int,TH2F*>::iterator it = reductionQvFprompt.find(reductionLevel);
  if(it != reductionQvFprompt.end())
    {
      it->second->Fill(softwareTrig.totalQ,softwareTrig.fprompt);
    }
  totalQvFprompt->Fill(softwareTrig.totalQ,softwareTrig.fprompt);
}

#endif
