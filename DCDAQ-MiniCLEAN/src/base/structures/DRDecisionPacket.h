#ifndef __DRDECISIONPACKET__
#define __DRDECISIONPACKET__

namespace DRDecisionPacket
{
  struct Event
  {
    uint32_t wfdEventID;
    int64_t eventID;
    int64_t eventTime;
    uint32_t eventTriggers;
    uint8_t  reductionLevel;    
    uint8_t  attempts;
    void Print()
    {
      fprintf(stderr,
	      "wfdEventID: 0x%08X\neventID: 0x%016"PRIx64"\neventTime: 0x%016"PRIx64"\neventTriggers: 0x%08X\nreductionLevel: %u\n",
	      wfdEventID,
	      (uint64_t)eventID,
	      (uint64_t)eventTime,
	      eventTriggers,
	      reductionLevel);
    }
  };
}
#endif
