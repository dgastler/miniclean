#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <time.h>

#include <tclap/CmdLine.h>
#include <tclap/ValueArg.h>

#include "TH1F.h"
#include <TGraph2D.h>
#include "TFile.h"
#include "TMinuit.h"
#include "TTree.h"
#include "TF1.h"
#include "TROOT.h"
#include "TLine.h"


#include "SplineFunction.h"

//=======================================================
//Fitting function and the global variables it uses.
//=======================================================
Float_t LY = 5.0;
std::vector<Int_t> NpePMT0_background;
std::vector<Int_t> NpePMT1_background; 
std::vector<Int_t> NpePMT0_signal;
std::vector<Int_t> NpePMT1_signal; 
std::vector<Float_t> PEs_background; 
std::vector<Float_t> PEs_signal; 

TH1F * PMT0SPE;
TH1F * PMT1SPE;

TH1F * hMC;
TH1F * hMCn;
TH1F * hData;
Double_t BaseSmear; //the last full smearing performed.
double BackgroundMultiplier;

bool rebuild;
double smearing;
double linearSmearing;
double SPEsampling = 1000;
double SmearingSampling = 1000;

SplineFunction * NsPointer;
SplineFunction * BsPointer;

bool StandardChiSquared;

TGraph2D * hChiSquared;

void SetMCHistogram(double norm,double leff)
{
  time_t  start = time(NULL);
  fprintf(stderr,"Generating histograms\n");
  BaseSmear = leff;
  TF1 * SmearingFunction = new TF1("SmearingFunction","gaus",0,hMC->GetXaxis()->GetXmax()*5);
  //  SmearingFunction->SetNpx(10000);
  SmearingFunction->SetNpx(5*(hMC->GetXaxis()->GetLast()));
  
  PEs_background.clear(); 
  PEs_signal.clear(); 

  Double_t Light;
  Double_t SmearingFactor = smearing*smearing*(1-leff);
  //  Double_t SmearingFactor = 9;
      
  //Process the signal events.
  Int_t SignalN = NpePMT0_signal.size();
  for(int iSignal = 0;iSignal < SignalN;iSignal++)
    {
      Light = 0;
      //Calculate new PMT0 pe count
      Int_t PMT0 = floor(NpePMT0_signal[iSignal]*leff + 0.5);
      for(int iPMT0 = 0; iPMT0 < PMT0;iPMT0++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      //	      Light+= PMT0SPE->GetRandom()/SPEsampling;
	      Light+= PMT0SPE->GetRandom();
	    }
	}
      //Calculate new PMT0 pe count
      Int_t PMT1 = floor(NpePMT1_signal[iSignal]*leff + 0.5);
      for(int iPMT1 = 0; iPMT1 < PMT1;iPMT1++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      //Light+= PMT1SPE->GetRandom()/SPEsampling;
	      Light+= PMT1SPE->GetRandom();
	    }
	}
      Light /= SPEsampling;
      if(Light > 0 && SmearingFactor > 0)
	{
	  SmearingFunction->SetParameters(1,Light,linearSmearing*Light + sqrt(SmearingFactor*Light));
	}
      else
	{
	  SmearingFunction->SetParameters(1,Light,1);
	}
      for(int iSmearingSampling = 0; iSmearingSampling < SmearingSampling;iSmearingSampling++)
	{
	  PEs_signal.push_back(SmearingFunction->GetRandom());
	}
    }
  //Process the background events.
  Int_t BackgroundN = NpePMT0_background.size();
  for(int iBackground = 0;iBackground < BackgroundN;iBackground++)
    {
      Light = 0;
      //Calculate new PMT0 pe count
      Int_t PMT0 = floor(NpePMT0_background[iBackground]*leff + 0.5);
      for(int iPMT0 = 0; iPMT0 < PMT0;iPMT0++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      //	      Light+= PMT0SPE->GetRandom()/SPEsampling;
	      Light+= PMT0SPE->GetRandom();
	    }
	}
      //Calculate new PMT0 pe count
      Int_t PMT1 = floor(NpePMT1_background[iBackground]*leff + 0.5);
      for(int iPMT1 = 0; iPMT1 < PMT1;iPMT1++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      //	      Light+= PMT1SPE->GetRandom()/SPEsampling;
	      Light+= PMT1SPE->GetRandom();
	    }
	}
      Light /=SPEsampling;
      if(Light > 0)
	{
	  SmearingFunction->SetParameters(1,Light,linearSmearing*Light + sqrt(SmearingFactor*Light));
	}
      else
	{
	  SmearingFunction->SetParameters(1,Light,1);
	}
      for(int iSmearingSampling = 0; iSmearingSampling < SmearingSampling;iSmearingSampling++)
	{
	  PEs_background.push_back(SmearingFunction->GetRandom());
	}

    }
  time_t end = time(NULL);
  fprintf(stderr,"Finished histograms in %dmin %ds\n",int(end-start)/60,int(end-start)%60);


  delete SmearingFunction;
}

























void fcn(int &npar, double *gin, double &f, double *par, int iflag) 
{
  f = 0;  //initialize chi-squared to zero.
  Double_t norm = par[0];
  Double_t effSE = par[1]; //smearing factor the fitter sees.
  Double_t backgroundRatio = par[0];
  if(npar >2)
    {
      backgroundRatio = par[2];
    }

  if(rebuild)
    {
      SetMCHistogram(1,effSE);
      rebuild = false;  
      //}
      hMC->Reset();
      hMCn->Reset();
      //      Double_t PEtoEnergy = 1.0/LY;
      Double_t PEtoEnergy = effSE/(LY*BaseSmear);
      unsigned int PEs_signalSize = PEs_signal.size();
      unsigned int PEs_backgroundSize = PEs_background.size();
      for(unsigned int i = 0; i < PEs_signalSize;i++)
	{
	  //	  hMCn->Fill(PEtoEnergy*PEs_signal[i],1.0/SmearingSampling);
	  hMCn->Fill(PEtoEnergy*PEs_signal[i]);
	}
      hMCn->Scale(1.0/SmearingSampling);
      for(unsigned int i = 0; i < PEs_backgroundSize;i++)
	{
	  //	  hMC->Fill(PEtoEnergy*PEs_background[i],BackgroundMultiplier/SmearingSampling);
	  hMC->Fill(PEtoEnergy*PEs_background[i]);
	}
      hMC->Scale(BackgroundMultiplier/SmearingSampling);

      //Fill vectors for spline function and build an energy calibrated specturm
      std::vector<float> X;
      std::vector<float> NeutronY;
      std::vector<float> BackgroundY;
      for(int ihMC = 1;ihMC <= hMC->GetNbinsX(); ihMC++)
	{
	  X.push_back(hMC->GetBinCenter(ihMC));
	  BackgroundY.push_back(hMC->GetBinContent(ihMC));
	  NeutronY.push_back(hMCn->GetBinContent(ihMC));
	}
      //Delete SFunction if it already exists
      if(BsPointer)
	{
	  delete BsPointer;
	}
      if(NsPointer)
	{
	  delete NsPointer;
	}
//      Double_t End   = hData->GetXaxis()->GetBinLowEdge(hData->GetXaxis()->GetLast());
//      Double_t Start = hData->GetXaxis()->GetBinUpEdge(hData->GetXaxis()->GetFirst());
      Double_t End   = hMC->GetXaxis()->GetBinLowEdge(hMC->GetXaxis()->GetLast());
      Double_t Start = hMC->GetXaxis()->GetBinUpEdge (hMC->GetXaxis()->GetFirst());
      //Create a new SFunction for fitting
      BsPointer = new SplineFunction(X,
				     BackgroundY,
				     Start,
				     End);
      NsPointer = new SplineFunction(X,
				     NeutronY,
				     Start,
				     End);
      
    }
  
  
  
  effSE/=BaseSmear; //Smearing factor relative to the last time we rebuilt the histogram.
  
  Double_t NeutronPassingParameters[2];
  NeutronPassingParameters[0] = norm;
  NeutronPassingParameters[1] = effSE;

  Double_t BackgroundPassingParameters[2];
  BackgroundPassingParameters[0] = backgroundRatio;
  BackgroundPassingParameters[1] = effSE;


  //Calculate ChiSquared
  Int_t EndBin = hData->GetXaxis()->GetLast();
  Int_t StartBin =hData->GetXaxis()->GetFirst();
  for(int bin = StartBin;
      bin <= EndBin;bin++)
    {
      Double_t x = hData->GetBinCenter(bin);
      Double_t MCyB = (*BsPointer)(&x,BackgroundPassingParameters);
      Double_t MCyS = (*NsPointer)(&x,NeutronPassingParameters);
      Double_t MCy =  MCyS+ MCyB;

      if(MCy < 1e-3){MCy=0.0;}

      Double_t DATAy = hData->GetBinContent(bin);      
      if(StandardChiSquared)
	{

	  //Chi squared fitting.  Guassian errors
	  Double_t error = MCy*NeutronPassingParameters[0];
	  error = 0.5 + sqrt(error + 0.25);
	  
	  Double_t fStep;
	  fprintf(stderr,"%%%16.8e %16.8e %16.8e %16.8e\n",x,DATAy,MCy,error);
	  if(error > 0)
	    {
	      fStep= ((DATAy - MCy)*(DATAy - MCy))/fabs(error*error);
	      f+= fStep;
	    }
	}
      else
	{
	  //Chi squared fitting.  Poisson statistics	  
	  Double_t fStep;
	  
	  if(DATAy == 0)
	    {
	      fStep = 2.0*(MCy - DATAy);
	    }
	  else if(DATAy > 0)
	    {
	      if(MCy <= 0)
		{
		  fprintf(stderr,"MC Zero! x= %f(%d) y = %e yB = %e yS = %e\n",
			  x,bin,MCy,MCyB,MCyS);
		  //		  fStep = fabs(2.0*(MCy - DATAy));
		}
	      else
		{
		  //		  fprintf(stderr,"MC Fine! x= %f(%d) y = %e yB = %e yS = %e\n",
		  //			  x,bin,MCy,MCyB,MCyS);
		  fStep = 2.0*(MCy - DATAy);
		  fStep += 2.0*DATAy*log(DATAy/MCy);
	    }
	    }
	  f+=fStep;
	}
      //
    }
  hChiSquared->SetPoint(hChiSquared->GetN(),effSE*BaseSmear,norm,f);
  fprintf(stderr,"#%16.8f %16.8f %16.8f %16.8f %16.8f %16.8f\n",norm,effSE*BaseSmear,backgroundRatio,f,hData->GetBinCenter(StartBin),hData->GetBinCenter(EndBin));
}


























//=======================================================
//Main routine
//=======================================================
int main(int argc, char ** argv)
{
  //MC cuts
  Double_t TOFMax;
  Double_t TOFMin;
  Double_t AsymMax;
  Double_t AsymMin;
  //Fitting guesses
  Double_t SEguess;
  Double_t Nguess;
  Double_t BackgroundRatio = 1;
  bool FitBackground = false;
  Double_t Emin = 0;
  ///File names
  std::string DataFileName;
  std::string RATFileName;
  std::string SPEFileName; 
  //Other
  bool SkipFit = false;
  double nSigmaWidth = 0.0;

  try 
    {      
      TCLAP::CmdLine cmd("Leff fitter with splines",' ',"1.1");

      //Filename arguments
      TCLAP::ValueArg<std::string> DataFileNameArg("d","datafile","output data file name",true,std::string("hi"),"string",cmd);
      TCLAP::ValueArg<std::string> RATFileNameArg("r","RATfile","input RAT file name",true,std::string("hi"),"string",cmd);
      TCLAP::ValueArg<std::string> SPEFileNameArg("p","SPEfile","input SPE file name",true,std::string("hi"),"string",cmd);
      
      //Fit values
      TCLAP::ValueArg<double> SEguessArg("l","leff","leff guess",false,0.3,"double",cmd);
      TCLAP::ValueArg<double> NormArg("n","norm","normalization guess",false,1,"double",cmd);
      TCLAP::ValueArg<double> SmearingArg("s","smearing","MC smearing",false,1,"double",cmd);
      TCLAP::ValueArg<double> BackgroundRatioArg("b","background","background ratio guess",false,-1,"double",cmd); 
      TCLAP::ValueArg<double> EminArg("e","emin","minimum fit energy",false,0,"double",cmd); 
      TCLAP::ValueArg<double> BackgroundMult("m","backgroundmultiple","multiple on the background",false,1,"double",cmd);
      TCLAP::ValueArg<double> LinearSmearingArg("f","linearsmearing","Linear energy smearing",false,0,"double",cmd);
      TCLAP::ValueArg<double> FitRangeArg("y","autofitrange","automatic fit range # sigma",false,0,"double",cmd);

      //MC values
      TCLAP::ValueArg<double> TOFMaxArg("k","TOFmax","TOF max relative to mean",true,6,"double",cmd);
      //MC values
      TCLAP::ValueArg<double> TOFMinArg("t","TOFmin","TOF min relative to mean",true,-6,"double",cmd);
      TCLAP::ValueArg<double> TBWidthArg("a","TBAsymm","Top-Bottom asymmetry",true,0.4,"double",cmd);

      //Other values
      TCLAP::SwitchArg SkipFitArg("x","nofit","Skip fitting",cmd,false);
      TCLAP::SwitchArg PoissonErrorsArg("z","poisson","use poissonian chi-squared",cmd,false);

      
      //Parse!
      cmd.parse(argc,argv);
      //Filename arguments
      DataFileName = DataFileNameArg.getValue();
      RATFileName = RATFileNameArg.getValue();
      SPEFileName = SPEFileNameArg.getValue();
      //Fit values
      SEguess = SEguessArg.getValue();
      Nguess = NormArg.getValue();
      smearing = SmearingArg.getValue();
      if(BackgroundRatioArg.getValue() == -1)
	{
	  BackgroundRatio = 1;
	  FitBackground = false;
	}
      else
	{
	  BackgroundRatio = BackgroundRatioArg.getValue();
	  FitBackground = true;
	}
      Emin = EminArg.getValue();
      BackgroundMultiplier = BackgroundMult.getValue();
      linearSmearing = LinearSmearingArg.getValue();
      nSigmaWidth = FitRangeArg.getValue();

      //MC values
      TOFMax = TOFMaxArg.getValue();
      TOFMin = TOFMinArg.getValue();
      AsymMax = TBWidthArg.getValue();     
      AsymMin = -1.0*TBWidthArg.getValue();      

      //Other
      SkipFit = SkipFitArg.getValue();
      StandardChiSquared = !PoissonErrorsArg.getValue();
    } 
  catch( TCLAP::ArgException & e)
    {
      fprintf(stderr,"Error: %s for arg %s\n",e.error().c_str(),e.argId().c_str());
      return(0);
    }
  
  //Ok command line stuff is done.


  std::cout << DataFileName.c_str() << " " <<  RATFileName.c_str() << " " <<  SPEFileName.c_str() << std::endl;
 
  //Vector of Agle IDs 
  std::vector<float> Energy(20,0);
  Energy[0]=10.6;
  Energy[1]=14.5;
  Energy[2]=19.0;
  Energy[3]=22.3;
  Energy[4]=27.7;
  Energy[5]=32.6;
  Energy[6]=38.9;
  Energy[7]=44.5;
  Energy[8]=51.6;
  Energy[9]=59.1;
  Energy[10]=66.9;
  Energy[11]=72.3;
  Energy[12]=79.2;
  Energy[13]=90.6;
  Energy[14]=97.9;
  Energy[15]=114.2;
  Energy[16]=182.5;
  Energy[17]=190.7;
  Energy[18]=211.3;
  Energy[19]=238.9;

  //Allocate a general purpose buffer
  Int_t bufferSize = 100;
  char * buffer = new char[bufferSize];


  //=======================================================
  //Input Data files
  //=======================================================
  
  //Get the run ID from the filename.  ex: SE_01_pass2.root
  Int_t RunID = atoi( (DataFileName.substr(DataFileName.find("_")+1 ,2)).c_str() );
  RunID = RunID - 1;
  if(RunID >= (Int_t) Energy.size())
    {
      printf("Run ID %08d out of range.\n",RunID);
      delete [] buffer;
      return(0);
    }
  //Check if this is a subrun.   ex: SE_01_1_pass2.root
  Int_t SubRun = atoi( (DataFileName.substr(6,1)).c_str() );
  if(SubRun)
    {
      printf("Processing subrun %d\n",SubRun);
    }
  //Open Data file and get the spectrum.
  TFile * DataFile = TFile::Open(DataFileName.c_str(),"READ");
  if(!DataFile)
    {
      printf("Bad Data file: %s\n",buffer);
      delete [] buffer;
      return(0);
    }
  //Open dat Histogram
  if(SubRun)
    {
      sprintf(buffer,"%05.1f_Neutron_scatters_%02d",Energy[RunID],SubRun);
    }
  else
    {
      sprintf(buffer,"%05.1f_Neutron_scatters",Energy[RunID]);
    }
  hData = (TH1F*) DataFile->Get("cLight");
  gROOT->cd();
  hData = (TH1F*) hData->Clone(buffer);
  if(!hData)
    {
      printf("Bad data histogram\n");
      delete [] buffer;
      return(0);
    }

  //Fit ranges!
  if(Energy[RunID] < 25)
    {
      hData->Rebin(2);
    }
  else if(Energy[RunID] < 50)
    {
      hData->Rebin(2);
    }
  else if(Energy[RunID] < 90)
    {
      hData->Rebin(2);
    }
  else if(Energy[RunID] < 190)
    {
      hData->Rebin(4);
    }
  else
    {
      hData->Rebin(5);
    }

  //=======================================================




  //=======================================================
  //Input SPEfiles
  //=======================================================
  TFile * SPEFile = TFile::Open(SPEFileName.c_str(),"READ");
  if(!SPEFile)
    {
      printf("Bad SPE file: %s\n",SPEFileName.c_str());
      delete [] buffer;
      return(0);
    }
  PMT0SPE = (TH1F*) SPEFile->Get("NormalizedPMT0SPE");
  if(!PMT0SPE)
    {
      fprintf(stderr,"No SPE0 histogram\n");
      delete [] buffer;
      return(0);
    }
  PMT1SPE = (TH1F*) SPEFile->Get("NormalizedPMT1SPE");
  if(!PMT1SPE)
    {
      fprintf(stderr,"No SPE1 histogram\n");
      delete [] buffer;
      return(0);
    }
  //=======================================================




  //=======================================================
  //Input MC files
  //=======================================================
  //Open the MC files
  TFile * MCFile = TFile::Open(RATFileName.c_str(),"READ");
  if(!MCFile)
    {
      printf("Bad MC RAT file: %s\n",RATFileName.c_str());
      delete [] buffer;
      return(0);
    }
  //Open RAT tree
  TTree * T = (TTree *) MCFile->Get("T");
  if(!T)
    {
      fprintf(stderr,"Bad tree.\n");
      delete [] buffer;
      return(0);
    }
  //Variables and branch settings for all the MC values.
  Int_t ID;
  T->SetBranchAddress("LSc_volID",&ID);
  Double_t TOF;
  T->SetBranchAddress("TOF",&TOF);
  Int_t NumPE;
  T->SetBranchAddress("numPe",&NumPE);
  Int_t Num_PMT_0;
  T->SetBranchAddress("num_PMT_0",&Num_PMT_0);
  Int_t Num_PMT_1;
  T->SetBranchAddress("num_PMT_1",&Num_PMT_1);
  Double_t Asym;
  T->SetBranchAddress("asym",&Asym);
  Int_t IntScat;
  T->SetBranchAddress("int_scat",&IntScat);
  Int_t ExtScat;
  T->SetBranchAddress("ext_scat",&ExtScat);
  Char_t  IntProc[20];
  T->SetBranchAddress("int_proc",&IntProc);
  Double_t Edep;
  T->SetBranchAddress("Edep",&Edep);

  //Convert MC IDs to actual IDs.
  std::vector<int> IDs(20,0);
  IDs[0] = 0;
  IDs[1] = 10;
  IDs[2] = 1;
  IDs[3] = 11;
  IDs[4] = 2;
  IDs[5] = 12;
  IDs[6] = 3;
  IDs[7] = 13;
  IDs[8] = 4;
  IDs[9] = 14;
  IDs[10] = 5;
  IDs[11] = 15;
  IDs[12] = 6;
  IDs[13] = 16;
  IDs[14] = 7;
  IDs[15] = 17;
  IDs[16] = 8;
  IDs[17] = 18;
  IDs[18] = 9;
  IDs[19] = 19;

  //=======================================================


  //=======================================================
  //Find MC TOF.
  //=======================================================
  hChiSquared = new TGraph2D();
  //=======================================================



  //=======================================================
  //Find MC TOF.
  //=======================================================
  TH1F * hTOF = new TH1F("hTOF","MC time of flight",200,0,200);
  TH1F * hTOFn = new TH1F("hTOFn","MC time of neutron flight",200,0,200);
  for(int iEvent = 0; iEvent < T->GetEntries();iEvent++)
    {
      //Get the next MC event
      T->GetEntry(iEvent);
      //Make sure the event has the right angle
      if(ID == IDs[RunID])
	{
	  //Do most cuts
	  if( (IntProc[0] == 'H') &&
	      (Asym < AsymMax) &&
	      (Asym > AsymMin) )
	    {
	      hTOF->Fill(TOF);
	      if((IntScat == 1)&&(ExtScat == 0))
		{
		  hTOFn->Fill(TOF);
		}
	    }
	}
    }
  TOFMax += hTOF->GetBinCenter(hTOF->GetMaximumBin());
  TOFMin += hTOF->GetBinCenter(hTOF->GetMaximumBin());
 
  char * TOFfilename = new char[100];
  sprintf(TOFfilename,"MCTOF_%02d.dat",RunID+1);
  FILE * TOFoutfile = fopen(TOFfilename,"w");
  delete [] TOFfilename;
  fprintf(TOFoutfile,"%f\n",TOFMin);
  fprintf(TOFoutfile,"%f\n",TOFMax);
  fclose(TOFoutfile);
 
  TLine * lTOFmin = new TLine(TOFMin,0,TOFMin,hTOF->GetMaximum());
  TLine * lTOFmax = new TLine(TOFMax,0,TOFMax,hTOF->GetMaximum());
  hTOF->GetListOfFunctions()->AddLast(lTOFmin);
  hTOF->GetListOfFunctions()->AddLast(lTOFmax);


  //=======================================================
  //Fill pe vectors for fitting.
  //=======================================================
  for(int iEvent = 0; iEvent < T->GetEntries();iEvent++)
    {
      //Get the next MC event
      T->GetEntry(iEvent);
      //Make sure the event has the right angle
      if(ID == IDs[RunID])
	{
	  //Do most cuts
	  if( (TOF < TOFMax)   &&
	      (TOF > TOFMin)   &&
	      (Asym < AsymMax) &&
	      (Asym > AsymMin) )
	    {
	      //Do background/signal cuts
	      if((IntScat == 1) && (ExtScat == 0))
		{
		  NpePMT0_signal.push_back(Num_PMT_0);
		  NpePMT1_signal.push_back(Num_PMT_1);
		}
	      else
		{
		  NpePMT0_background.push_back(Num_PMT_0);
		  NpePMT1_background.push_back(Num_PMT_1);
		}
	    }
	}
    }
  //=======================================================
  sprintf(buffer,"%s_hMC",hData->GetName());
  hMC = (TH1F*) hData->Clone(buffer);
  hMC->Reset();
  sprintf(buffer,"%s_hMCn",hData->GetName());
  hMCn = (TH1F*) hData->Clone(buffer);
  hMCn->Reset();

  //Set fit ranges
  if(Energy[RunID] < 25)
    {
      hData->GetXaxis()->SetRangeUser(Emin,80); // 80
    }
  else if(Energy[RunID] < 50)
    {
      hData->GetXaxis()->SetRangeUser(Emin,100);//100
    }
  else if(Energy[RunID] < 90)
    {
      hData->GetXaxis()->SetRangeUser(Emin,110);//110
    }
  else if(Energy[RunID] < 180)
    {
      hData->GetXaxis()->SetRangeUser(Emin,160);//160
    }
  else
    {
      hData->GetXaxis()->SetRangeUser(Emin,180); //180
    }
  


  //=======================================================
  //Do the fitting!
  //=======================================================
  





  
  TMinuit * gMinuit = new TMinuit(2);
  gMinuit->SetFCN(fcn);
  
  Double_t arglist[10];
  Int_t ierflg = 0;
  //set maximum printout level
  
//  arglist[0] =1;
//  gMinuit->mnexcm("SET PRI", arglist, 1, ierflg);  


  //setup the fit histograms
  rebuild = true;
  Int_t npar = 2;
  if(FitBackground)
    {
      npar = 3;
    }

  Double_t f;
  Double_t par[3];
  par[0] = Nguess;
  par[1] = SEguess;  
  par[2] = 1;
  fcn(npar,arglist,f,par,ierflg);


  
  //Set error definition
  // 1   : Chi square
  // 0.5 : Negative log likelihood
  //      gMinuit->SetErrorDef(0.5);
  arglist[0] = 1;
  ierflg = 0;
  gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
  if(ierflg) {fprintf(stderr,"mnexcm:SET ERR gave: %d\n",ierflg);}

  //Minimization strategy
  // 0 : fewer function calls (less reliable)
  // 1 : standard
  // 2 : try to improve minimum (slower)
  arglist[0] = 2;
  ierflg = 0;
  gMinuit->mnexcm("SET STR", arglist, 1, ierflg);  
  if(ierflg) {fprintf(stderr,"mnexcm:SET STR gave: %d\n",ierflg);}

  //Set starting values, bounds, and step sizes for parameters

  Double_t vstart[3] = {Nguess,  /*normalization*/
			SEguess,/*SE*/
			BackgroundRatio}; /*Background ratio*/

  //Fit result variables
  Double_t nval     = 0;
  Double_t nvalerrH = 0;
  Double_t nvalerrL = 0;
  
  Double_t eval     = 0;
  Double_t evalerrH = 0;
  Double_t evalerrL = 0;
  
  Double_t BackgroundRatioVal     = 0;
  Double_t BackgroundRatioValErrH = 0;
  Double_t BackgroundRatioValErrL = 0;

  //Needed for minute fit
  TString  name; // ignored
  Double_t bnd1, bnd2; // ignored
  static Double_t step[3]   = {0.1,0.01,1};
  static Double_t minvals[3]= {0,0.1,0};  // no limits
  static Double_t maxvals[3]= {5,0.75,0};  // no limits

  if(Energy[RunID] > 100)
    {      
      maxvals[1] = 0.40;
    }
  
  for(int retry = 0; retry < 100;retry++)
    {           
      vstart[0] = Nguess*(1+retry*float(random())/float(RAND_MAX));  /*normalization*/
      vstart[1] = SEguess*(1+retry*float(random())/float(RAND_MAX));/*SE*/
      vstart[2] = BackgroundRatio; /*Background ratio*/


      if(retry)
	{
	  printf("Retry #%d %f:%f:%f\n",retry,vstart[0],vstart[1],vstart[2]);
	}

      //  static Double_t step[3]   = {Nguess/2.0,0.05,1};

      ierflg = 0;
      gMinuit->mnparm(0,"nb",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
      if(ierflg) {fprintf(stderr,"mnparm nb gave: %d\n",ierflg);}
      ierflg = 0;
      gMinuit->mnparm(1,"e",vstart[1],step[1],minvals[1],maxvals[1],ierflg);
      if(ierflg) {fprintf(stderr,"mnparm e gave: %d\n",ierflg);}
      if(FitBackground)
	{
	  ierflg = 0;
	  gMinuit->mnparm(2,"br",vstart[2],step[2],minvals[2],maxvals[2],ierflg);
	  if(ierflg) {fprintf(stderr,"mnparm e gave: %d\n",ierflg);}
	}

  
      //Do the fitting
      nval     = 0;
      nvalerrH = 0;
      nvalerrL = 0;
      
      eval     = 0;
      evalerrH = 0;
      evalerrL = 0;
      
      BackgroundRatioVal     = 0;
      BackgroundRatioValErrH = 0;
      BackgroundRatioValErrL = 0;
      
      
      arglist[0] = 5000;     //max number of iterations
      arglist[1] = 0.1;

      ierflg = 0;      
      if(!SkipFit)
	{
	  //gMinuit->mnexcm("SIMPLEX", arglist, 2, ierflg);  // Fit with SIMPLEX
	  gMinuit->mnexcm("MINOS", arglist, 1, ierflg);  // Fit with MINOS
	  //gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);  // Fit with MIGRAD
	  if(ierflg) {fprintf(stderr,"mnexcm:MINOS gave: %d\n",ierflg);}
	}
      if(eval != maxvals[1])
	break;
    }

  ierflg = 0;      
  gMinuit->mnpout(0,name,nval,nvalerrH,bnd1,bnd2,ierflg);
  if(ierflg) {fprintf(stderr,"mnpout: 0 gave: %d\n",ierflg);}
  ierflg = 0;      
  gMinuit->mnpout(1,name,eval,evalerrH,bnd1,bnd2,ierflg);
  if(ierflg) {fprintf(stderr,"mnpout: 1 gave: %d\n",ierflg);}
  if(FitBackground)
    {
      ierflg = 0;      
      gMinuit->mnpout(2,name,BackgroundRatioVal,BackgroundRatioValErrH,bnd1,bnd2,ierflg);
      if(ierflg) {fprintf(stderr,"mnpout: 1 gave: %d\n",ierflg);}
    }
  Double_t uselessVariable;
  Double_t nvalparabolicError;
  Double_t evalparabolicError;
  Double_t BackgroundRatioValParabolicError;
  gMinuit->mnerrs(0,nvalerrH,nvalerrL,nvalparabolicError,uselessVariable);  //normalization errors
  gMinuit->mnerrs(1,evalerrH,evalerrL,evalparabolicError,uselessVariable);  // Leff error
  if(FitBackground)
    {
      gMinuit->mnerrs(2,
		      BackgroundRatioValErrH,
		      BackgroundRatioValErrL,
		      BackgroundRatioValParabolicError,
		      uselessVariable);  // Background ratio error
    }


  printf("ID: %02d\n",RunID);
  printf("nvalb: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",nval,nvalparabolicError,nvalerrH,nvalerrL);
  printf("eval: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",eval,evalparabolicError,evalerrH,evalerrL);
  if(FitBackground)
    {
      printf("Background Ratio: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",
	     BackgroundRatioVal,
	     BackgroundRatioValParabolicError,
	     BackgroundRatioValErrH,
	     BackgroundRatioValErrL);
    }














  //Optional Second Fitting.
  double FitMin;
  double FitMax;
  if(nSigmaWidth != 0)
    {
      //Fit the single scatters to a gaussian
      TF1 * NeutronGaussian = new TF1("NeutronGaussian","gaus");
      NeutronGaussian->SetParameters(hMCn->GetMaximum(),
				     hMCn->GetBinCenter(hMCn->GetMaximumBin()),
				     sqrt(hMCn->GetBinCenter(hMCn->GetMaximumBin())));
      hMCn->Fit(NeutronGaussian,"N");
      
      FitMin = NeutronGaussian->GetParameter(1) - fabs(nSigmaWidth*NeutronGaussian->GetParameter(2));
      FitMax = NeutronGaussian->GetParameter(1) + fabs(nSigmaWidth*NeutronGaussian->GetParameter(2));
      
      if(FitMin < Emin)
	{
	  FitMin = Emin;
	}
      //Set the fit range to nSigmaWidth times the width of the gaussian around the mean.
      hData->GetXaxis()->SetRangeUser(FitMin,
				      FitMax); // 80
      
      printf("New fit range: %f - %f\n",
	     FitMin,
	     FitMax);
      //Do the next fit.
      rebuild = true;  
      ierflg = 0;
      //      gMinuit->mnparm(0,"nb",vstart[0],step[0],minvals[0],maxvals[0],ierflg); //2009-12-11
      gMinuit->mnparm(0,"nb",nval,step[0],minvals[0],maxvals[0],ierflg);
      if(ierflg) {fprintf(stderr,"mnparm nb gave: %d\n",ierflg);}
      ierflg = 0;
      //      gMinuit->mnparm(1,"e",vstart[1],step[1],minvals[1],maxvals[1],ierflg); //2009-12-11
      gMinuit->mnparm(1,"e",eval,step[1],minvals[1],maxvals[1],ierflg);
      if(ierflg) {fprintf(stderr,"mnparm e gave: %d\n",ierflg);}
      if(FitBackground)
	{
	  ierflg = 0;
	  gMinuit->mnparm(2,"br",vstart[2],step[2],minvals[2],maxvals[2],ierflg);
	  if(ierflg) {fprintf(stderr,"mnparm e gave: %d\n",ierflg);}
	}
      if(!SkipFit)
	{
	  gMinuit->mnexcm("MINOS", arglist, 1, ierflg);  // Fit with MINOS
	  if(ierflg) {fprintf(stderr,"mnexcm:MINOS gave: %d\n",ierflg);}
	}
      
      
      
      
      //Get back all the values from the fit
      ierflg = 0;      
      gMinuit->mnpout(0,name,nval,nvalerrH,bnd1,bnd2,ierflg);
      if(ierflg) {fprintf(stderr,"mnpout: 0 gave: %d\n",ierflg);}
      ierflg = 0;      
      gMinuit->mnpout(1,name,eval,evalerrH,bnd1,bnd2,ierflg);
      if(ierflg) {fprintf(stderr,"mnpout: 1 gave: %d\n",ierflg);}
      if(FitBackground)
	{
	  ierflg = 0;      
	  gMinuit->mnpout(2,name,BackgroundRatioVal,BackgroundRatioValErrH,bnd1,bnd2,ierflg);
	  if(ierflg) {fprintf(stderr,"mnpout: 1 gave: %d\n",ierflg);}
	}
      gMinuit->mnerrs(0,nvalerrH,nvalerrL,nvalparabolicError,uselessVariable);  //normalization errors
      gMinuit->mnerrs(1,evalerrH,evalerrL,evalparabolicError,uselessVariable);  // Leff error
      if(FitBackground)
	{
	  gMinuit->mnerrs(2,
			  BackgroundRatioValErrH,
			  BackgroundRatioValErrL,
			  BackgroundRatioValParabolicError,
			  uselessVariable);  // Background ratio error
	}
      
      
      printf("ID: %02d\n",RunID);
      printf("nvalb: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",nval,nvalparabolicError,nvalerrH,nvalerrL);
      printf("eval: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",eval,evalparabolicError,evalerrH,evalerrL);
      if(FitBackground)
	{
	  printf("Background Ratio: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",
		 BackgroundRatioVal,
		 BackgroundRatioValParabolicError,
		 BackgroundRatioValErrH,
		 BackgroundRatioValErrL);
	}
      
    }
      

  //Apply new fit values to the histograms and save the output

  arglist[0] = 1;
  arglist[1] = 2;
  arglist[2] = 100;


  rebuild = true;  
  par[0] = nval;
  par[1] = eval;
  par[2] = BackgroundRatioVal;
  if(SkipFit)
    {
      par[0] = Nguess;
      par[1] = SEguess;
      par[2] = BackgroundRatio;
    }
  fcn(npar,arglist,f,par,ierflg);


  
  //Set draw ranges
  if(Energy[RunID] < 25)
    {
      hData->GetXaxis()->SetRangeUser(Emin,80); // 80
    }
  else if(Energy[RunID] < 50)
    {
      hData->GetXaxis()->SetRangeUser(Emin,100);//100
    }
  else if(Energy[RunID] < 90)
    {
      hData->GetXaxis()->SetRangeUser(Emin,110);//110
    }
  else if(Energy[RunID] < 180)
    {
      hData->GetXaxis()->SetRangeUser(Emin,160);//160
    }
  else
    {
      hData->GetXaxis()->SetRangeUser(Emin,180); //180
    }

  if(nSigmaWidth != 0)
    {
      hMC->GetXaxis()->SetRangeUser(FitMin,
				    FitMax); 
      hMCn->GetXaxis()->SetRangeUser(FitMin,
				     FitMax);
    }



  TFile * OutFile;

  OutFile = TFile::Open("NewFit.root","UPDATE");
  
  TTree * FitTree = (TTree*) OutFile->Get("T");

  double dEnergy = Energy[RunID];

  if(!FitTree)
    {
      FitTree = new TTree("T","Fit values");
      FitTree->Branch("chisqr"             ,&f                  ,"chisqr/D"             );
      FitTree->Branch("energy"             ,&dEnergy            ,"energy/D"             );
      FitTree->Branch("smearing"           ,&smearing           ,"smearing/D"           );
      FitTree->Branch("linearsmearing"     ,&linearSmearing     ,"linearSmearing/D"     );
      FitTree->Branch("nval"              ,&nval              ,"nval/D"              );
      FitTree->Branch("nvalerrL"          ,&nvalerrL          ,"nvalerrL/D"          );
      FitTree->Branch("nvalerrH"          ,&nvalerrH          ,"nvalerrH/D"          );
      FitTree->Branch("nvalparabolicError",&nvalparabolicError,"nvalparabolicError/D");
      FitTree->Branch("BackgroundRatioVal"              ,&BackgroundRatioVal              ,"BackgroundRatioVal/D"              );
      FitTree->Branch("BackgroundRatioValErrL"          ,&BackgroundRatioValErrL          ,"BackgroundRatioValErrL/D"          );
      FitTree->Branch("BackgroundRatioValErrH"          ,&BackgroundRatioValErrH          ,"BackgroundRatioValErrH/D"          );
      FitTree->Branch("BackgroundRatioValParabolicError",&BackgroundRatioValParabolicError,"BackgroundRatioValParabolicError/D");
      FitTree->Branch("eval"              ,&eval              ,"eval/D"              );
      FitTree->Branch("evalerrL"          ,&evalerrL          ,"evalerrL/D"          );
      FitTree->Branch("evalerrH"          ,&evalerrH          ,"evalerrH/D"          );
      FitTree->Branch("evalparabolicError",&evalparabolicError,"evalparabolicError/D");
      FitTree->Branch("hData"             ,"TH1F",&hData,128000,0);              
      FitTree->Branch("hMC"               ,"TH1F",&hMC,128000,0);              
      FitTree->Branch("hMCn"              ,"TH1F",&hMCn,128000,0);              
      FitTree->Branch("hTOF"              ,"TH1F",&hTOF,128000,0);              
      FitTree->Branch("hTOFn"              ,"TH1F",&hTOFn,128000,0);              
      FitTree->Branch("hChiSquared"        ,"TGraph2D",&hChiSquared,128000,0);              
      FitTree->Branch("SignalFit"              ,"TSpline3",NsPointer->GetSpline(),128000,0);              
      FitTree->Branch("BackgroundFit"              ,"TSpline3",BsPointer->GetSpline(),128000,0);              
    }
  else
    {
      FitTree->SetBranchAddress("chisqr",&f,0);
      FitTree->SetBranchAddress("energy",&dEnergy,0);
      FitTree->SetBranchAddress("smearing",&smearing,0);
      FitTree->SetBranchAddress("linearSmearing",&linearSmearing,0);
      FitTree->SetBranchAddress("nval",&nval,0);
      FitTree->SetBranchAddress("nvalerrL",&nvalerrL,0);
      FitTree->SetBranchAddress("nvalerrH",&nvalerrH,0);
      FitTree->SetBranchAddress("nvalparabolicError",&nvalparabolicError,0);
      FitTree->SetBranchAddress("BackgroundRatioVal",&BackgroundRatioVal,0);
      FitTree->SetBranchAddress("BackgroundRatioValErrL",&BackgroundRatioValErrL,0);
      FitTree->SetBranchAddress("BackgroundRatioValErrH",&BackgroundRatioValErrH,0);
      FitTree->SetBranchAddress("BackgroundRatioValParabolicError",&BackgroundRatioValParabolicError,0);
      FitTree->SetBranchAddress("eval",&eval,0);
      FitTree->SetBranchAddress("evalerrL",&evalerrL,0);
      FitTree->SetBranchAddress("evalerrH",&evalerrH,0);
      FitTree->SetBranchAddress("evalparabolicError",&evalparabolicError,0);
      FitTree->SetBranchAddress("hData"             ,&hData);              
      FitTree->SetBranchAddress("hMC"               ,&hMC );              
      FitTree->SetBranchAddress("hMCn"              ,&hMCn);              
      FitTree->SetBranchAddress("hTOF"              ,&hTOF);              
      FitTree->SetBranchAddress("hTOFn"              ,&hTOFn);              
      FitTree->SetBranchAddress("hChiSquared"        ,&hChiSquared);              
      FitTree->SetBranchAddress("SignalFit"              ,NsPointer->GetSpline());              
      FitTree->SetBranchAddress("BackgroundFit"              ,BsPointer->GetSpline());              
    }
  FitTree->Fill();
  FitTree->Write();
    //  hData->Write();
    //  hMC->Write();
    //  hMCn->Write();
  OutFile->Close();

  delete [] buffer;
}
