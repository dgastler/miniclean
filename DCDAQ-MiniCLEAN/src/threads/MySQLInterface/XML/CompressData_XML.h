#ifndef COMPRESSDATA_XML_H
#define COMPRESSDATA_XML_H

#include <Thread_XML.h>


class CompressData_XML: public Thread_XML
{

 public:
  CompressData_XML(DCMySQL *conn,int runNumber);
  
  virtual bool Generate_XML(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responseNode);

 private:
  bool GetCompressData(std::string pcName,std::string threadType,std::string threadName);
};

#endif
