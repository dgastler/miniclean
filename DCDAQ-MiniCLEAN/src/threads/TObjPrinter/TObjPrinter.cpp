#include <TObjPrinter.h>

TObjPrinter::TObjPrinter(std::string Name)
{
  Ready = false;
  SetType("TObjPrinter");
  SetName(Name);
  c1 = NULL;
}
TObjPrinter::~TObjPrinter()
{
  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  
  for(size_t i = 0; i < routes.size();i++)
    {
      //Set message to unregister
      message.SetType(DCMessage::UNREGISTER_TYPE);
      message.SetDestination(routes[i].route); 
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(DCMessage::GetTypeName(routes[i].type));
      message.SetData(&(data[0]),data.size());
      //route it to it's DCDAQ command list
      //SendMessageOut(message,true);
      SendMessageOut(message);
    }
  if(c1)
    delete c1;
}

bool TObjPrinter::Setup(xmlNode * SetupNode,
		       std::vector<MemoryManager*> &/*MemManager*/)
{
  c1 = new TCanvas("c1");
  //Add the local session to the print status
  sRoute route;

  if(NULL == FindSubNode(SetupNode,"NOAUTOCONFIG"))
    {
      route.type = DCMessage::HISTOGRAM;
      route.route.Set();//blank
      routes.push_back(route);
    }

  //Check if we are registering ourselves for other DCDAQ sessions
  xmlNode * remoteNode = NULL;
  if((remoteNode = FindSubNode(SetupNode,"REMOTE")) != NULL)
    {      
      //Get the IP address of the remote print stats
      size_t routeNodes = NumberOfNamedSubNodes(remoteNode,"ROUTE");
      for(size_t i =0 ; i < routeNodes;i++)
	{
	  xmlNode * routeNode = FindIthSubNode(remoteNode,
					       "ROUTE",
					       i);
	  //Get the destination address and route type
	  if(routeNode != NULL)
	    {
	      std::string IP;
	      std::string type;
	      int goodNode = 0;
	      goodNode += GetXMLValue(routeNode,
				      "TYPE",
				      GetName().c_str(),
				      type);
	      goodNode += GetXMLValue(routeNode,
				      "IP",
				      GetName().c_str(),
				      IP);
	      //If there were no errors, set up the route
	      if(goodNode == 0)
		{
		  route.type = DCMessage::GetTypeFromName(type.c_str());
		  route.route.Set(DCMessage::DCAddress::BroadcastName,IP);
		  routes.push_back(route);
		}
	    }
	}

    }

  std::vector<std::string> printStatements;  

  //Create a DCMessage to sent out with this data
  DCMessage::Message message;
  for(size_t i = 0; i < routes.size();i++)
    {
      message.Clear();

      //Set message to unregister
      message.SetType(DCMessage::REGISTER_TYPE);
      message.SetDestination(routes[i].route); 
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(DCMessage::GetTypeName(routes[i].type));
      message.SetData(&(data[0]),data.size());

      
      std::string buffer("Routing ");
      buffer += DCMessage::GetTypeName(routes[i].type);
      buffer += " from ";
      buffer += routes[i].route.GetStr();
      buffer += " to this session.";
      printStatements.push_back(buffer);
      
      //      SendMessageOut(message,true);
      SendMessageOut(message);
    }
  for(size_t i = 0; i < printStatements.size();i++)
   {
     Print(printStatements[i].c_str());
   }
  Ready = true;
  return true;
}

bool TObjPrinter::ProcessMessage(DCMessage::Message &message)
{
  if(message.GetType() == DCMessage::HISTOGRAM)
    {
      PrintTObj(message);
      return true;
    }
  return false;
}

void TObjPrinter::PrintTObj(DCMessage::Message &message)
{
  DCMessage::DCTObject rootObj;
  std::vector<uint8_t> tempData  = message.GetData();
  TObject * obj = NULL;
  TFile * outFile;  
  if(rootObj.GetData(tempData,obj))
    {
      std::stringstream ss;
      ss << message.GetSource().GetName() << "_"
	 << obj->GetName() << "_" 
	 << message.GetTime() << ".root";     
      PrintWarning(ss.str());
      outFile = TFile::Open(ss.str().c_str(),"RECREATE");
      if(outFile)
	{
	  outFile->Add(obj);  
	  outFile->Write();
	  outFile->Close();
	  delete outFile;
	}
    }  
  //I guess the TFile does this for us now...
  //Not that the documentation says that.
  //  delete obj;  
}
