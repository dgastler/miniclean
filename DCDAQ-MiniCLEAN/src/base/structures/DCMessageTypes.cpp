#include <DCMessageTypes.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[DCMessage::COUNT] = 
  {
    "BLANK",
    "LAUNCH",
    "LAUNCH_RESPONSE",
    "REGISTER_TYPE",
    "UNREGISTER_TYPE",
    "REGISTER_ADDR",
    "UNREGISTER_ADDR",
    "END_OF_THREAD",
    "STOP",
    "PAUSE",
    "GO",
    "RUN_NUMBER",
    "RUN_NUMBER_RESPONSE",
    "GET_STATS",
    "GET_ROUTE",
    "ERROR",
    "WARNING",
    "STATISTIC",
    "HISTOGRAM",
    "TEXT",
    "COMMAND",
    "COMMAND_RESPONSE",
    "RUN_INFO",
    "CAN_NOT_ROUTE",
    "NETWORK",
  };  

const char * DCMessage::GetTypeName(enum DCMessageType type)
{
  if(type < DCMessage::COUNT)
    return TypeName[type];
  fprintf(stderr,"bad type?\n");
  return NULL;
} 

enum DCMessage::DCMessageType DCMessage::GetTypeFromName(const char * name)
{
  std::string searchString(name);  
  enum DCMessageType ret = DCMessage::BLANK;
  for(enum DCMessageType type = DCMessage::BLANK;
      type < DCMessage::COUNT;
      type = DCMessageType(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
