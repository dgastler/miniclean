#include <iostream>
#include <vector>
#include <map>
#include <string>

#include <string.h> //strerror()

//DCDAQ
#include <DCThread.h>
#include <MemoryManager.h>
#include <DCLauncher.h>
#include <DCMessage.h>
#include <DCMessageProcessor.h>

//Helpers
#include <runNumberHelper.h>

#include <math.h> //floor

#include <signal.h> //signals 
#include <unistd.h> //alarm


volatile bool run = true;
volatile bool ctrlC_exit = false;

void signal_handler(int sig)
{
  if(sig == SIGINT)
    {
      if(ctrlC_exit)
	{
	  run = false;
	  struct sigaction sa;
	  sa.sa_handler = SIG_DFL;
	  sigemptyset(&sa.sa_mask);
	  sigaction(SIGINT, &sa, NULL);
	  fprintf(stderr,"\n=====================================\n");
	  fprintf(stderr,"          Ctrl-C forced exit!\n");
	  fprintf(stderr,"          Once more to kill.");
	  fprintf(stderr,"\n=====================================\n");
	}
      else
	{
	  fprintf(stderr,"\n=====================================\n");
	  fprintf(stderr,"Press Ctrl-C once more to force exit");
	  fprintf(stderr,"\n=====================================\n");
	  alarm(5);
	  ctrlC_exit = true;
	}
    }
  else if (sig == SIGALRM)
    {
      //reset ctrlC_exit after a timeout
      ctrlC_exit = false;
    }
  else if(sig == SIGSEGV)
    {
      fprintf(stderr,"SEGFAULT yo!\n");
      run = false;
    }
}


int main(int argc, char ** argv)
{
  bool CommandLine = false;
  std::string CommandLineXMLString;
  struct sigaction sa;
  sa.sa_handler = signal_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGALRM,&sa, NULL);
  //sigaction(SIGSEGV,&sa, NULL);

  if(argc < 2)
    {
      printf("Warning: Running without setup XML\n");
    }
  else
    {
      //Load the command line XML file
      std::string CommandLineFileName(argv[1]);
      if(!LoadFile(CommandLineFileName,CommandLineXMLString))
	{
	  fprintf(stderr,"Bad input file.\n");
	  return(0);
	}
      else
	{
	  CommandLine = true;
	}
    }


  //Hardcoded config files for the control and netcontrol threads.
  std::string ControlSetupString("<SETUP><THREADS><THREAD><TYPE>Control</TYPE><NAME>Control</NAME></THREAD></THREADS></SETUP>");
  

  //Get the current run number
  DCMessage::SingleUINT64 RunNumber;
  if(!GetRunNumber(RunNumber.i))
    {
      fprintf(stderr,"Run number failed.\n");
      return(0);
    }


  printf("==================================\n");
  printf("=========Setting up DCDAQ=========\n");
  printf("==================================\n");

 
  //Create the launcher for DCDAQ threads and memory managers
  //  DCLauncher * Launcher = new DCLauncher;
  DCLauncher Launcher;

  //Create the local select error processor and add ignored errors
  SelectErrorIgnore<LockedObject> * selectErrorIgnore = Launcher.GetSelectErrorIgnore();
  selectErrorIgnore->AddIgnoreErrno(EINTR); //Interrupted system call

  //Create the message processor for DCDAQ
  DCMessageProcessor MessageProcessor(&Launcher);

  DCMessage::Message message;
  //Start the user interface
  message.SetData(ControlSetupString.c_str(),ControlSetupString.size());
  message.SetType(DCMessage::LAUNCH);
  MessageProcessor.ProcessMessage(message);
  //load the command line requested objects
  if(CommandLine)
    {
      message.SetData(CommandLineXMLString.c_str(),CommandLineXMLString.size());
      message.SetType(DCMessage::LAUNCH);
      MessageProcessor.ProcessMessage(message);
    }
  
  //Set the run number
  message.SetType(DCMessage::RUN_NUMBER);
  message.SetDataStruct(RunNumber);
  MessageProcessor.ProcessMessage(message);


  //Setup select for main DCDAQ loop
  long MaxSleepTime = 600;// 10 minutes
  struct timeval SleepTime;
  SleepTime.tv_sec= MaxSleepTime; 
  SleepTime.tv_usec= 0;   
  int MaxFDPlusOne = 0;
  //FD sets for select. 
  fd_set ReadSet,WriteSet,ExceptionSet;   

  //Program run time
  time_t mainLoopStartTime = time(NULL);
  time_t mainLoopEndTime = mainLoopStartTime;

  printf("===================================\n");
  printf("===========DCDAQ Running===========\n");
  printf("===================================\n");
  printf(">Type \"go\" to start threads.\n");

  //Main loop
  while(run)
    {
      //Wait for a thread message
      SleepTime.tv_sec= MaxSleepTime; 
      SleepTime.tv_usec= 0;
      //Get the fd sets for the running threads
      Launcher.GetFDSets(ReadSet,
			 WriteSet,
			 ExceptionSet,
			 MaxFDPlusOne);
      //Block in select
      int selectReturn = select(MaxFDPlusOne,
				&ReadSet,
				&WriteSet,
				&ExceptionSet,
				&SleepTime);
      
      if(selectReturn > 0)
	{
	  run = MessageProcessor.ProcessMessage(selectReturn,
						ReadSet,
						WriteSet,
						ExceptionSet);
	}
      //Note, selectReturn = 0 just means it timed out.  
      //We dont' really care about that, so we don't catch it in this
      //if statement.    select return negative means a real error
      else if(selectReturn < 0)
	{
	  if(!selectErrorIgnore->IgnoreSelectError(errno))
	    {
	      printf("Error:     DCDAQ select had error %d: %s\n",errno,strerror(errno));
	    }
	}
    }
  //We no longer need this pointer;
  selectErrorIgnore = NULL;
  //Shutdown and clean up all of the memory managers and threads.
  Launcher.Shutdown();

  //Calculate run time
  mainLoopEndTime = time(NULL);
  double mainLoopRunTime = difftime(mainLoopEndTime,mainLoopStartTime);  

  int days =    (int) floor(mainLoopRunTime/(24*60*60));
  mainLoopRunTime -= days*24*60*60;

  int hours =   (int) floor(mainLoopRunTime/(60*60));
  mainLoopRunTime -= hours*60*60;

  int minutes = (int) floor(mainLoopRunTime/60);
  mainLoopRunTime -= minutes*60;

  int seconds = (int) mainLoopRunTime;
  
  printf("\n\nDCDAQ run time: %03ddays %02dhrs %02dmin %02dsec\n\n",
	 days,hours,minutes,seconds);

  return(0);
}
