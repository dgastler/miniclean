<?php
include "ui.class.php";
if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_POST);
}

$runFrom = intval($_POST['runNumber']);
$table = $_POST['table'];
$type = $_POST['type'];
$name = $_POST['name'];
$pc=$_POST['pcName'];
$res = $opt->copyIndividualRow($table,$runFrom,$type,$name,$pc);

$new_table = array();

//run Number
$new_table[0] = 0;
//order id
$new_table[1] = '';
//mm_name = name, and mm_type = type, and pc_name = pc
$new_table[2] = $name;
$new_table[3] = $type;
$new_table[4] = $pc;

for ($i=1; $i< (count($res)/2); $i++) {
  if ($res[$i] == "NULL") {
    $res[$i] == '';
  }     
  $new_table[$i+4] = $res[$i];
}

$res2 = $opt->addCopyRow($table,$new_table);

echo json_encode($res2) . PHP_EOL;
?>