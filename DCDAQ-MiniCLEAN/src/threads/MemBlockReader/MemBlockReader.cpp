#include <MemBlockReader.h>

MemBlockReader::MemBlockReader(std::string Name)
{
  SetName(Name);
  SetType(std::string("MemBlockReader"));
}

bool MemBlockReader::Setup(xmlNode * SetupNode,
			   std::vector<MemoryManager*> &MemManager)
{
  //Get the memory manager
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return false;
    }
  Mem->IncrementWritingThreads();

  //Get the input file names
  int fileNodes = NumberOfNamedSubNodes(SetupNode,"FILE");
  fprintf(stderr,"Number of files: %u\n",fileNodes);
  for(int iNode=0;iNode<fileNodes;iNode++)
    {
      std::string filename;
      GetIthXMLValue(SetupNode,iNode,"FILE",GetName().c_str(),filename);
      InFileNames.Add(filename);
    }

  Ready = true;
  return true;
}

void MemBlockReader::MainLoop()
{
  //Check for a free memory block
  if(IsReadReady(Mem->GetFreeFDs()[0]))
    {      
      //Now remove this FD from the read set
      RemoveReadFD(Mem->GetFreeFDs()[0]);

      int32_t blockIndex;
      //Get a new memory block
      Mem->GetFree(blockIndex);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  MemoryBlock * block;
	  //Get the memory block for this blockIndex
	  block = Mem->GetBlock(blockIndex);

	  
	  //Fill it with one memblock from the file
	  FillReturn::Type fillReturn = FillMemBlock(block);
	  
	  std::stringstream ss;
	  switch (fillReturn)
	    {
	    case FillReturn::OK:	      
	      //Send this MemBlock off
	      if(!Mem->AddFull(blockIndex))
		{
		  PrintError("Failed to add mem block to full queue");
		}
	      AddReadFD(Mem->GetFreeFDs()[0]);
	      break;
	    case FillReturn::END_OF_FILE:
	      CloseFile();
	      //Return empty memblock
	      //if(!Mem->AddFree(blockIndex))
	      if(InFileNames.GetSize()>0)
		{
		  //Return empty memblock 
		  if(!Mem->AddFree(blockIndex)) 
		    {
		      PrintError("Failed to add mem block to free queue");
		      
		    }
		}
	      else
		{
		  //Send empty memblock to signal end of run
		  if(!Mem->AddFull(blockIndex))
		    {
		      PrintError("Failed to add mem block to full queue");
		    }
		}
	      //Look back at the InFileNames queue
	      
	      AddReadFD(InFileNames.GetFDs()[0]);
	      break;
	    case FillReturn::BAD_SIZE:
	      ss << "Read failed with BAD_SIZE";
	      PrintError(ss.str());
	      CloseFile();
	      int32_t tempIndex;
	      if(Mem->GetFree(tempIndex))
		Mem->AddFull(tempIndex);
	      break;
	    case FillReturn::BAD_HEADER:
	      ss << "Read failed with BAD_HEADER";
	      PrintError(ss.str());
	      CloseFile();
	      break;
	    case FillReturn::BAD_READ:
	      ss << "Read failed with BAD_READ";
	      PrintError(ss.str());
	      CloseFile();
	      break;
	    case FillReturn::ERROR:	      
	      ss << "Read failed with value ERROR";
	      PrintError(ss.str());
	      CloseFile();
	      break;
	    default:
	      ss << "Read failed with unknown value: " << fillReturn;
	      PrintError(ss.str());
	      CloseFile();
	      break;
	    }
	}
      //Setup select for next time
      SetupSelect();
    }
  //Check for new file
  if(IsReadReady(InFileNames.GetFDs()[0]))
    {
      //Open new file
      if(OpenFile())
	{
	  //Now remove this FD from the read set
	  RemoveReadFD(InFileNames.GetFDs()[0]);
	  //And add looking for a free block
	  AddReadFD(Mem->GetFreeFDs()[0]);
	}
      else
	{
	  std::stringstream ss;	  
	  ss << "Failed to open file: " << currentFileName;
	  PrintError(ss.str());      
	}
      SetupSelect();
    }
}

FillReturn::Type MemBlockReader::FillMemBlock(MemoryBlock * block)
{
  FillReturn::Type ret = FillReturn::ERROR;
  //Get a pointer to the block buffer we are going to write to.
  uint32_t * bufferPointer = (uint32_t *)(block->buffer + block->dataSize);
  
  //Either the next word is a block header or we are at the EOF.
  MemBlockMessage::header blockHeader;
  int freadret = fread(&blockHeader, sizeof(blockHeader), 1 /*word*/, InFile);
  if(freadret == 1/*word*/)
    {
      if(!feof(InFile)) //Process memblock
	{
	  uint32_t blockSize;
	  //Check for valid header word
	  if(blockHeader.sizeMagic != 0xAA)
	    {
	      PrintError("Incorrect header word");
	      ret = FillReturn::BAD_HEADER;
	      return ret;
	    }
	  blockSize=blockHeader.size;
	  //Find if memBlock compressed
	  if(blockHeader.compressionMagic==0xBB)
	    {
	      block->uncompressedSize=blockHeader.compressionSize;
	    }
	  else
	    {
	      if(blockHeader.compressionMagic != 0xCC)
		{
		  PrintError("Bad Compression Header word");
		  ret = FillReturn::BAD_HEADER;
		  return ret;

		}
	    }
	  //Check if size will fit in mem block
	  if (blockSize <= (block->allocatedSize - block->dataSize))
	    {
	     
	      //Read blockSize words from file into the memory block.
	      freadret = fread(bufferPointer, sizeof(uint32_t), blockSize, InFile);
	      if(uint32_t(freadret) == blockSize)
		{
		  bufferPointer+=blockSize;
		  block->dataSize += blockSize;
		  ret = FillReturn::OK;
		  blockCount++;
		}
	      else
		{
		  ret = FillReturn::BAD_READ;
		  int test = feof(InFile);
		  fprintf(stderr,"Bad read: %d != %u (test: %d)\n",freadret,blockSize,test);
		}
	    }  
	  else
	    {
	      ret = FillReturn::BAD_SIZE;
	    }
	}

    }
  else
    {
      if(ferror(InFile))
	{
	  PrintError("Something went wrong reading the header word!");
	  ret = FillReturn::ERROR;
	}
      else //we are at the end of the file
	{
	  block->uncompressedSize=-1;
	  ret = FillReturn::END_OF_FILE;
	}
    }
  return ret;
}

bool MemBlockReader::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      RemoveReadFD(Mem->GetFreeFDs()[0]);
      RemoveReadFD(InFileNames.GetFDs()[0]);
      SetupSelect();
      Loop = false;
      Running = false;
    }
  else if (message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",GetTime(&tempTime).c_str(), GetName().c_str());
      RemoveReadFD(Mem->GetFreeFDs()[0]);
      RemoveReadFD(InFileNames.GetFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if (message.GetType() == DCMessage::GO)
    {
        DCtime_t tempTime = message.GetTime();
	printf("%s (%s): Starting up thread.\n",
  	     GetTime(&tempTime).c_str(),
  	     GetName().c_str());
	AddReadFD(InFileNames.GetFDs()[0]);
	SetupSelect();

	Loop = true;
    }
  else if (message.GetType() == DCMessage::COMMAND)
    {
      std::string newFileName;
      DCMessage::DCString stringParser;
      if(stringParser.GetData(message.GetData(),newFileName))
	{
	  InFileNames.Add(newFileName);
	}
    }
  else
    {
      ret = false;
    }
  return(ret);
}

bool MemBlockReader::OpenFile()
{
  bool ret = false;
  if(InFileNames.Get(currentFileName))
    {
      //Open the file
      InFile = fopen(currentFileName.c_str(),"rb");
      if(InFile)
	{
	  blockCount = 0;
	  ret = true;
	}
    }
  return ret;
}

bool MemBlockReader::CloseFile()
{
  bool ret;
  if(fclose(InFile) == 0)
    {
      std::stringstream ss;
      ss << "Finish processing file: " << currentFileName
	 << " containing " << blockCount << " blocks";
      Print(ss.str());
      ret = true;
    }
  else
    {
      ret = false;
    }
  return ret;
}
