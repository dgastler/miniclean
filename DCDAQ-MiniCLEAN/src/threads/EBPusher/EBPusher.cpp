#ifdef __RATDS__
#include <EBPusher.h>

EBPusher::EBPusher(std::string Name): packetBuilder(detectorInfo), detectorInfo()
{  
  SetName(Name);
  SetType(std::string("EBPusher"));

  //FEPC memory manager
  FEPC_mem = NULL;

  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  timeStepProcessedEvents = 0;

  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>300000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>300000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
  //Needed for network connetion
  Loop = true;
}

bool EBPusher::Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager)
{
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  bool ret = true;
  //==============================================================
  //Load the FEPC memory manager
  //==============================================================
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  //==============================================================
  //Get DetectorINfo data
  //==============================================================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::PMT_OFFSET | 
			 InfoTypes::EVENT_START|
			 InfoTypes::EVENT_END |
			 InfoTypes::EVENT_PROMPT
			 );
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }

  //==============================================================
  //Set up packet parser vector
  //==============================================================

//  packetBuilder.SetDetectorInfo(detectorInfo.GetEventStartTime(),
//				detectorInfo.GetEventEndTime(),
//				detectorInfo.GetPromptIntegrationEndTime(),
//				detectorInfo.GetPMTOffset()
//				);



  //================================
  //Parse data for Client config and expected clients.
  //================================
  //Use port 9997 for EBAssebler
  if(!EBAssembler.SetupRemoteAddr(SetupNode))    
    ret = false; //Connection setup failed
  
  if(ret)
    Ready = true;
  else
    return false;



  return StartNewConnection();
}

void EBPusher::PrintStatus()
{
  DCThread::PrintStatus();
  if(EBAssembler.IsRunning())
    EBAssembler.PrintStatus();
}
#endif
