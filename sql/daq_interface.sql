-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: daq_interface
-- ------------------------------------------------------
-- Server version	5.5.31-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `daq_interface`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `daq_interface` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `daq_interface`;

--
-- Table structure for table `DAC_Offsets`
--

DROP TABLE IF EXISTS `DAC_Offsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAC_Offsets` (
  `pmt_id` int(11) DEFAULT NULL,
  `wfd_pos` int(11) DEFAULT NULL,
  `wfd_id` int(11) DEFAULT NULL,
  `dac_offset` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DAC_Offsets`
--

LOCK TABLES `DAC_Offsets` WRITE;
/*!40000 ALTER TABLE `DAC_Offsets` DISABLE KEYS */;
/*!40000 ALTER TABLE `DAC_Offsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Memory_Defaults`
--

DROP TABLE IF EXISTS `Memory_Defaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Memory_Defaults` (
  `mm_type` varchar(40) DEFAULT NULL,
  `data_01` varchar(40) DEFAULT NULL,
  `data_02` varchar(40) DEFAULT NULL,
  `data_03` varchar(40) DEFAULT NULL,
  `data_04` varchar(40) DEFAULT NULL,
  `data_05` varchar(40) DEFAULT NULL,
  `data_06` varchar(40) DEFAULT NULL,
  `data_07` varchar(40) DEFAULT NULL,
  `data_08` varchar(40) DEFAULT NULL,
  `data_09` varchar(40) DEFAULT NULL,
  `data_10` varchar(40) DEFAULT NULL,
  `data_11` varchar(40) DEFAULT NULL,
  `data_12` varchar(40) DEFAULT NULL,
  `data_13` varchar(40) DEFAULT NULL,
  `data_14` varchar(40) DEFAULT NULL,
  `data_15` varchar(40) DEFAULT NULL,
  `data_16` varchar(40) DEFAULT NULL,
  `data_17` varchar(40) DEFAULT NULL,
  `data_18` varchar(40) DEFAULT NULL,
  `data_19` varchar(40) DEFAULT NULL,
  `data_20` varchar(40) DEFAULT NULL,
  `data_21` varchar(40) DEFAULT NULL,
  `data_22` varchar(40) DEFAULT NULL,
  `data_23` varchar(40) DEFAULT NULL,
  `data_24` varchar(40) DEFAULT NULL,
  `data_25` varchar(40) DEFAULT NULL,
  `data_26` varchar(40) DEFAULT NULL,
  `data_27` varchar(40) DEFAULT NULL,
  `data_28` varchar(40) DEFAULT NULL,
  `data_29` varchar(40) DEFAULT NULL,
  `data_30` varchar(40) DEFAULT NULL,
  `data_31` varchar(40) DEFAULT NULL,
  `data_32` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Memory_Defaults`
--

LOCK TABLES `Memory_Defaults` WRITE;
/*!40000 ALTER TABLE `Memory_Defaults` DISABLE KEYS */;
INSERT INTO `Memory_Defaults` VALUES ('DRPCRatManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RatDiskBlockManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemoryBlockManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCRatManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RATCopyQueueManager','100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Memory_Defaults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Memory_MemoryManagers`
--

DROP TABLE IF EXISTS `Memory_MemoryManagers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Memory_MemoryManagers` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `mm_name` varchar(40) DEFAULT NULL,
  `mm_type` varchar(40) DEFAULT NULL,
  `pc_name` varchar(40) DEFAULT NULL,
  `data_01` varchar(10) DEFAULT NULL,
  `data_02` varchar(10) DEFAULT NULL,
  `data_03` varchar(10) DEFAULT NULL,
  `data_04` varchar(10) DEFAULT NULL,
  `data_05` varchar(10) DEFAULT NULL,
  `data_06` varchar(10) DEFAULT NULL,
  `data_07` varchar(10) DEFAULT NULL,
  `data_08` varchar(10) DEFAULT NULL,
  `data_09` varchar(10) DEFAULT NULL,
  `data_10` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Memory_MemoryManagers`
--

LOCK TABLES `Memory_MemoryManagers` WRITE;
/*!40000 ALTER TABLE `Memory_MemoryManagers` DISABLE KEYS */;
INSERT INTO `Memory_MemoryManagers` VALUES (-10,1,'MBLK3_MM','MemoryBlockManager','FEPC3','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,4,'EBPC_MM','RatDiskBlockManager','DREBPC','8010','8000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,2,'FEPC2_MM','FEPCRatManager','FEPC2','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,3,'DRPC_MM','DRPCRatManager','DREBPC','8010','8000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,2,'FEPC3_MM','FEPCRatManager','FEPC3','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,2,'FEPC1_MM','FEPCRatManager','FEPC1','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'RATCopy_MM','RATCopyQueueManager','DREBPC','100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'MBLK4_MM','MemoryBlockManager','FEPC4','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,2,'FEPC4_MM','FEPCRatManager','FEPC4','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'MBLK2_MM','MemoryBlockManager','FEPC2','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'MBLK1_MM','MemoryBlockManager','FEPC1','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC4_MM','FEPCRatManager','FEPC4','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'RATCopy_MM','RATCopyQueueManager','DREBPC','100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'MBLK4_MM','MemoryBlockManager','FEPC4','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC1_MM','FEPCRatManager','FEPC1','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC3_MM','FEPCRatManager','FEPC3','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'RAWMBLK_MM','MemoryBlockManager','DREBPC','0x100000','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,3,'DRPC_MM','DRPCRatManager','DREBPC','8010','8000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'CMPMBLK_MM','MemoryBlockManager','DREBPC','0x100000','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC2_MM','FEPCRatManager','FEPC2','8010','4000','4000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'MBLK1_MM','MemoryBlockManager','FEPC1','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'MBLK3_MM','MemoryBlockManager','FEPC3','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'MBLK2_MM','MemoryBlockManager','FEPC2','0x400200','40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,4,'EBPC_MM','RatDiskBlockManager','DREBPC','8010','8000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Memory_MemoryManagers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Memory_Necessary`
--

DROP TABLE IF EXISTS `Memory_Necessary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Memory_Necessary` (
  `mm_type` varchar(40) DEFAULT NULL,
  `data_01` varchar(40) DEFAULT NULL,
  `data_02` varchar(40) DEFAULT NULL,
  `data_03` varchar(40) DEFAULT NULL,
  `data_04` varchar(40) DEFAULT NULL,
  `data_05` varchar(40) DEFAULT NULL,
  `data_06` varchar(40) DEFAULT NULL,
  `data_07` varchar(40) DEFAULT NULL,
  `data_08` varchar(40) DEFAULT NULL,
  `data_09` varchar(40) DEFAULT NULL,
  `data_10` varchar(40) DEFAULT NULL,
  `data_11` varchar(40) DEFAULT NULL,
  `data_12` varchar(40) DEFAULT NULL,
  `data_13` varchar(40) DEFAULT NULL,
  `data_14` varchar(40) DEFAULT NULL,
  `data_15` varchar(40) DEFAULT NULL,
  `data_16` varchar(40) DEFAULT NULL,
  `data_17` varchar(40) DEFAULT NULL,
  `data_18` varchar(40) DEFAULT NULL,
  `data_19` varchar(40) DEFAULT NULL,
  `data_20` varchar(40) DEFAULT NULL,
  `data_21` varchar(40) DEFAULT NULL,
  `data_22` varchar(40) DEFAULT NULL,
  `data_23` varchar(40) DEFAULT NULL,
  `data_24` varchar(40) DEFAULT NULL,
  `data_25` varchar(40) DEFAULT NULL,
  `data_26` varchar(40) DEFAULT NULL,
  `data_27` varchar(40) DEFAULT NULL,
  `data_28` varchar(40) DEFAULT NULL,
  `data_29` varchar(40) DEFAULT NULL,
  `data_30` varchar(40) DEFAULT NULL,
  `data_31` varchar(40) DEFAULT NULL,
  `data_32` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Memory_Necessary`
--

LOCK TABLES `Memory_Necessary` WRITE;
/*!40000 ALTER TABLE `Memory_Necessary` DISABLE KEYS */;
INSERT INTO `Memory_Necessary` VALUES ('DRPCRatManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RatDiskBlockManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemoryBlockManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCRatManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RATCopyQueueManager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Memory_Necessary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Memory_Templates`
--

DROP TABLE IF EXISTS `Memory_Templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Memory_Templates` (
  `mm_type` varchar(40) DEFAULT NULL,
  `data_01` varchar(40) DEFAULT NULL,
  `data_02` varchar(40) DEFAULT NULL,
  `data_03` varchar(40) DEFAULT NULL,
  `data_04` varchar(40) DEFAULT NULL,
  `data_05` varchar(40) DEFAULT NULL,
  `data_06` varchar(40) DEFAULT NULL,
  `data_07` varchar(40) DEFAULT NULL,
  `data_08` varchar(40) DEFAULT NULL,
  `data_09` varchar(40) DEFAULT NULL,
  `data_10` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Memory_Templates`
--

LOCK TABLES `Memory_Templates` WRITE;
/*!40000 ALTER TABLE `Memory_Templates` DISABLE KEYS */;
INSERT INTO `Memory_Templates` VALUES ('DRPCRatManager','NUMBLOCKS','VECTORHASHSIZE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RatDiskBlockManager','NUMBLOCKS','VECTORHASHSIZE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemoryBlockManager','BLOCKSIZE','BLOCKCOUNT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCRatManager','NUMBLOCKS','STORAGEEVENTSIZE','ASSEMBLEDEVENTSIZE',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RATCopyQueueManager','MAXSIZE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Memory_Templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages_BeforeThreads`
--

DROP TABLE IF EXISTS `Messages_BeforeThreads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Messages_BeforeThreads` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `src_ip` varchar(20) DEFAULT NULL,
  `src_name` varchar(20) DEFAULT NULL,
  `dest_ip` varchar(20) DEFAULT NULL,
  `dest_name` varchar(20) DEFAULT NULL,
  `data_type` varchar(20) DEFAULT NULL,
  `data` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages_BeforeThreads`
--

LOCK TABLES `Messages_BeforeThreads` WRITE;
/*!40000 ALTER TABLE `Messages_BeforeThreads` DISABLE KEYS */;
INSERT INTO `Messages_BeforeThreads` VALUES (-10,2,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','NETWORK'),(-10,3,'REGISTER_TYPE',NULL,'Print','BROADCAST',NULL,'TEXT','TEXT'),(-10,1,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','ERROR'),(-1,1,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','ERROR'),(-1,3,'REGISTER_TYPE',NULL,'Print','BROADCAST',NULL,'TEXT','TEXT'),(-1,2,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','NETWORK'),(-1,0,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','STOP');
/*!40000 ALTER TABLE `Messages_BeforeThreads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages_End`
--

DROP TABLE IF EXISTS `Messages_End`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Messages_End` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `src_ip` varchar(20) DEFAULT NULL,
  `src_name` varchar(20) DEFAULT NULL,
  `dest_ip` varchar(20) DEFAULT NULL,
  `dest_name` varchar(20) DEFAULT NULL,
  `data_type` varchar(20) DEFAULT NULL,
  `data` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages_End`
--

LOCK TABLES `Messages_End` WRITE;
/*!40000 ALTER TABLE `Messages_End` DISABLE KEYS */;
INSERT INTO `Messages_End` VALUES (-10,8,'RUN_NUMBER',NULL,NULL,'BROADCAST',NULL,'WORD32','0x1'),(-10,4,'GO',NULL,NULL,'192.168.0.5',NULL,NULL,NULL),(-10,2,'REGISTER_TYPE',NULL,'MC-DAQ','BROADCAST',NULL,'TEXT','WARNING'),(-10,1,'REGISTER_TYPE',NULL,'Print','BROADCAST',NULL,'TEXT','TEXT'),(-10,10,'GO',NULL,NULL,'192.168.0.1',NULL,NULL,NULL),(-10,5,'GO',NULL,NULL,'192.168.0.4',NULL,NULL,NULL),(-10,3,'REGISTER_TYPE',NULL,'MC-DAQ','BROADCAST',NULL,'TEXT','STATISTIC'),(-10,7,'GO',NULL,NULL,'192.168.0.2',NULL,NULL,NULL),(-10,9,'RUN_NUMBER',NULL,NULL,NULL,'MC-DAQ',NULL,NULL),(-10,6,'GO',NULL,NULL,'192.168.0.3',NULL,NULL,NULL),(-1,7,'GO',NULL,NULL,'192.168.0.2',NULL,NULL,NULL),(-1,3,'REGISTER_TYPE',NULL,'MC-DAQ','BROADCAST',NULL,'TEXT','STATISTIC'),(-1,6,'GO',NULL,NULL,'192.168.0.3',NULL,NULL,NULL),(-1,5,'GO',NULL,NULL,'192.168.0.4',NULL,NULL,NULL),(-1,10,'GO',NULL,NULL,'192.168.0.1',NULL,NULL,NULL),(-1,1,'REGISTER_TYPE',NULL,'Print','BROADCAST',NULL,'TEXT','TEXT'),(-1,9,'RUN_NUMBER',NULL,NULL,NULL,'MC-DAQ',NULL,NULL),(-1,4,'GO',NULL,NULL,'192.168.0.5',NULL,NULL,NULL),(-1,8,'RUN_NUMBER',NULL,NULL,'BROADCAST',NULL,'WORD32','0x1'),(-1,2,'REGISTER_TYPE',NULL,'MC-DAQ','BROADCAST',NULL,'TEXT','WARNING');
/*!40000 ALTER TABLE `Messages_End` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages_Shutdown`
--

DROP TABLE IF EXISTS `Messages_Shutdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Messages_Shutdown` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `src_ip` varchar(20) DEFAULT NULL,
  `src_name` varchar(20) DEFAULT NULL,
  `dest_ip` varchar(20) DEFAULT NULL,
  `dest_name` varchar(20) DEFAULT NULL,
  `data_type` varchar(20) DEFAULT NULL,
  `data` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages_Shutdown`
--

LOCK TABLES `Messages_Shutdown` WRITE;
/*!40000 ALTER TABLE `Messages_Shutdown` DISABLE KEYS */;
INSERT INTO `Messages_Shutdown` VALUES (-10,0,'PAUSE',NULL,NULL,'192.168.0.1','DRPC_Trigger',NULL,NULL),(-10,1,'PAUSE',NULL,NULL,'192.168.0.5','FEPC4_V1720',NULL,NULL),(-10,1,'PAUSE',NULL,NULL,'192.168.0.4','FEPC3_V1720',NULL,NULL),(-10,1,'PAUSE',NULL,NULL,'192.168.0.3','FEPC2_V1720',NULL,NULL),(-10,1,'PAUSE',NULL,NULL,'192.168.0.2','FEPC1_V1720',NULL,NULL),(-10,6,'STOP',NULL,NULL,NULL,'RunControl',NULL,NULL),(-1,0,'PAUSE',NULL,NULL,'192.168.0.1','DRPC_Trigger',NULL,NULL),(-1,1,'PAUSE',NULL,NULL,'192.168.0.4','FEPC3_V1720',NULL,NULL),(-1,1,'PAUSE',NULL,NULL,'192.168.0.2','FEPC1_V1720',NULL,NULL),(-1,1,'PAUSE',NULL,NULL,'192.168.0.5','FEPC4_V1720',NULL,NULL),(-1,1,'PAUSE',NULL,NULL,'192.168.0.3','FEPC2_V1720',NULL,NULL),(-1,2,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','STOP'),(-1,6,'STOP',NULL,NULL,'192.168.0.1','PackData1',NULL,NULL);
/*!40000 ALTER TABLE `Messages_Shutdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages_Start`
--

DROP TABLE IF EXISTS `Messages_Start`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Messages_Start` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `src_ip` varchar(20) DEFAULT NULL,
  `src_name` varchar(20) DEFAULT NULL,
  `dest_ip` varchar(20) DEFAULT NULL,
  `dest_name` varchar(20) DEFAULT NULL,
  `data_type` varchar(20) DEFAULT NULL,
  `data` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages_Start`
--

LOCK TABLES `Messages_Start` WRITE;
/*!40000 ALTER TABLE `Messages_Start` DISABLE KEYS */;
INSERT INTO `Messages_Start` VALUES (-10,NULL,'REGISTER_TYPE',NULL,NULL,NULL,NULL,'TEXT','NETWORK'),(-1,NULL,'REGISTER_TYPE',NULL,NULL,NULL,NULL,'TEXT','NETWORK'),(-1,0,'REGISTER_TYPE',NULL,NULL,'BROADCAST',NULL,'TEXT','STOP');
/*!40000 ALTER TABLE `Messages_Start` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NetworkConnections`
--

DROP TABLE IF EXISTS `NetworkConnections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NetworkConnections` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `pc_name` varchar(20) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `port` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NetworkConnections`
--

LOCK TABLES `NetworkConnections` WRITE;
/*!40000 ALTER TABLE `NetworkConnections` DISABLE KEYS */;
INSERT INTO `NetworkConnections` VALUES (-10,4,'FEPC3','192.168.0.4',1717),(-10,3,'FEPC2','192.168.0.3',1717),(-10,2,'FEPC1','192.168.0.2',1717),(-10,1,'DREBPC','192.168.0.1',1717),(-10,5,'FEPC4','192.168.0.5',1717),(-1,5,'FEPC4','192.168.0.5',1717),(-1,1,'DREBPC','192.168.0.1',1717),(-1,2,'FEPC1','192.168.0.2',1717),(-1,3,'FEPC2','192.168.0.3',1717),(-1,4,'FEPC3','192.168.0.4',1717);
/*!40000 ALTER TABLE `NetworkConnections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RUNLOCK`
--

DROP TABLE IF EXISTS `RUNLOCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RUNLOCK` (
  `access` varchar(20) DEFAULT NULL,
  `run_number` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RUNLOCK`
--

LOCK TABLES `RUNLOCK` WRITE;
/*!40000 ALTER TABLE `RUNLOCK` DISABLE KEYS */;
INSERT INTO `RUNLOCK` VALUES ('FREE',0);
/*!40000 ALTER TABLE `RUNLOCK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RunNames`
--

DROP TABLE IF EXISTS `RunNames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RunNames` (
  `run_number` int(11) DEFAULT NULL,
  `run_name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RunNames`
--

LOCK TABLES `RunNames` WRITE;
/*!40000 ALTER TABLE `RunNames` DISABLE KEYS */;
INSERT INTO `RunNames` VALUES (-10,'PI Cali (12WFD)'),(-1,'MiniCLEAN Running');
/*!40000 ALTER TABLE `RunNames` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SThread_Data_Compressed`
--

DROP TABLE IF EXISTS `SThread_Data_Compressed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SThread_Data_Compressed` (
  `run_number` int(11) DEFAULT NULL,
  `thread_type` varchar(20) DEFAULT NULL,
  `thread_name` varchar(20) DEFAULT NULL,
  `pc_name` varchar(20) DEFAULT NULL,
  `compressed` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SThread_Data_Compressed`
--

LOCK TABLES `SThread_Data_Compressed` WRITE;
/*!40000 ALTER TABLE `SThread_Data_Compressed` DISABLE KEYS */;
INSERT INTO `SThread_Data_Compressed` VALUES (-10,'MemBlockCompressor','Compressor4','DREBPC','CMPMBLK_MM'),(-10,'MemBlockCompressor','Compressor3','DREBPC','CMPMBLK_MM'),(-10,'MemBlockCompressor','Compressor2','DREBPC','CMPMBLK_MM'),(-10,'MemBlockCompressor','Compressor1','DREBPC','CMPMBLK_MM'),(-1,'MemBlockCompressor','Compressor1','DREBPC','CMPMBLK_MM'),(-1,'MemBlockCompressor','Compressor2','DREBPC','CMPMBLK_MM'),(-1,'MemBlockCompressor','Compressor3','DREBPC','CMPMBLK_MM'),(-1,'MemBlockCompressor','Compressor4','DREBPC','CMPMBLK_MM');
/*!40000 ALTER TABLE `SThread_Data_Compressed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SThread_Data_Raw`
--

DROP TABLE IF EXISTS `SThread_Data_Raw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SThread_Data_Raw` (
  `run_number` int(11) DEFAULT NULL,
  `thread_type` varchar(20) DEFAULT NULL,
  `thread_name` varchar(20) DEFAULT NULL,
  `pc_name` varchar(20) DEFAULT NULL,
  `raw` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SThread_Data_Raw`
--

LOCK TABLES `SThread_Data_Raw` WRITE;
/*!40000 ALTER TABLE `SThread_Data_Raw` DISABLE KEYS */;
INSERT INTO `SThread_Data_Raw` VALUES (-10,'MemBlockCompressor','Compressor4','DREBPC','RAWMBLK_MM'),(-10,'MemBlockCompressor','Compressor3','DREBPC','RAWMBLK_MM'),(-10,'MemBlockCompressor','Compressor1','DREBPC','RAWMBLK_MM'),(-10,'MemBlockCompressor','Compressor2','DREBPC','RAWMBLK_MM'),(-10,'PackData','PackData1','DREBPC','EBPC_MM'),(-1,'PackData','PackData1','DREBPC','EBPC_MM'),(-1,'MemBlockCompressor','Compressor2','DREBPC','RAWMBLK_MM'),(-1,'MemBlockCompressor','Compressor1','DREBPC','RAWMBLK_MM'),(-1,'MemBlockCompressor','Compressor3','DREBPC','RAWMBLK_MM'),(-1,'MemBlockCompressor','Compressor4','DREBPC','RAWMBLK_MM');
/*!40000 ALTER TABLE `SThread_Data_Raw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SThread_DecisionMaker`
--

DROP TABLE IF EXISTS `SThread_DecisionMaker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SThread_DecisionMaker` (
  `run_number` int(11) DEFAULT NULL,
  `thread_name` varchar(20) DEFAULT NULL,
  `Hardware_Trigger` varchar(40) DEFAULT NULL,
  `ReductionLevel` int(11) DEFAULT NULL,
  `Software_Trigger` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SThread_DecisionMaker`
--

LOCK TABLES `SThread_DecisionMaker` WRITE;
/*!40000 ALTER TABLE `SThread_DecisionMaker` DISABLE KEYS */;
INSERT INTO `SThread_DecisionMaker` VALUES (-10,'Decision_Maker','EVNT',2,0),(-1,'Decision_Maker','EVNT',4,1);
/*!40000 ALTER TABLE `SThread_DecisionMaker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Settings_DataReduction`
--

DROP TABLE IF EXISTS `Settings_DataReduction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Settings_DataReduction` (
  `run_number` int(11) DEFAULT NULL,
  `triggerMode` int(11) DEFAULT NULL,
  `dataFormat` int(11) DEFAULT NULL,
  `intQthresh` int(11) DEFAULT NULL,
  `promptQthresh` float DEFAULT NULL,
  `maxQthresh` float DEFAULT NULL,
  `prescale` float DEFAULT NULL,
  `prescaleIntQ` int(11) DEFAULT NULL,
  `prescalePromptQ` float DEFAULT NULL,
  `prescaleMaxQ` float DEFAULT NULL,
  `presamples` varchar(20) DEFAULT NULL,
  `promptPresamples` float DEFAULT NULL,
  `promptPostsamples` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Settings_DataReduction`
--

LOCK TABLES `Settings_DataReduction` WRITE;
/*!40000 ALTER TABLE `Settings_DataReduction` DISABLE KEYS */;
INSERT INTO `Settings_DataReduction` VALUES (-10,1,4,120000,0.64,0.1,1000,1000,1000,1000,'152',48,75),(-1,1,4,49500,0.64,0.1,1000,1000,1000,1000,'152',48,75);
/*!40000 ALTER TABLE `Settings_DataReduction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Settings_PMTs`
--

DROP TABLE IF EXISTS `Settings_PMTs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Settings_PMTs` (
  `run_number` int(11) DEFAULT NULL,
  `pmt_id` int(11) DEFAULT NULL,
  `wfd_pos` int(11) DEFAULT NULL,
  `wfd_id` int(11) DEFAULT NULL,
  `pc_name` varchar(20) DEFAULT NULL,
  `baseline` float DEFAULT NULL,
  `presamples` int(11) DEFAULT NULL,
  `zs_nsamp_nlbk` varchar(10) DEFAULT NULL,
  `zs_nsamp_nlfwd` varchar(10) DEFAULT NULL,
  `nsamples` int(11) DEFAULT NULL,
  `channeltype` varchar(10) DEFAULT NULL,
  `hitthresh` int(11) DEFAULT NULL,
  `dac_offset` int(11) DEFAULT NULL,
  `zs_thresh_edge_trig` int(11) DEFAULT NULL,
  `zs_thresh_val` int(11) DEFAULT NULL,
  `channel_type` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Settings_PMTs`
--

LOCK TABLES `Settings_PMTs` WRITE;
/*!40000 ALTER TABLE `Settings_PMTs` DISABLE KEYS */;
INSERT INTO `Settings_PMTs` VALUES (-10,90,1,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,89,0,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,80,7,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,77,4,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,79,6,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,78,5,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,75,2,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,91,2,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,9,0,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,10,1,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,11,2,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,12,3,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,13,4,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,15,6,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,14,5,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,16,7,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,39,6,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,40,7,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,38,5,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,37,4,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,36,3,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,35,2,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,33,0,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,34,1,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,62,5,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,63,6,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,61,4,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,64,7,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,60,3,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,59,2,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,57,0,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,58,1,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,88,7,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,85,4,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,87,6,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,86,5,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,83,2,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,84,3,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,1,0,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,81,0,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,82,1,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,3,2,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,2,1,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,4,3,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,5,4,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,94,5,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,96,7,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,95,6,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,93,4,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,92,3,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,6,5,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,7,6,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,8,7,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,26,1,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,27,2,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,25,0,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,28,3,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,29,4,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,32,7,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,41,0,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,30,5,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,31,6,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,42,1,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,43,2,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,44,3,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,45,4,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,46,5,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,47,6,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,48,7,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,49,0,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,50,1,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,51,2,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,52,3,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,53,4,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,56,7,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,55,6,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,54,5,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,65,0,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,66,1,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,67,2,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,69,4,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,70,5,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,68,3,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,71,6,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,72,7,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,73,0,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,76,3,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,74,1,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,17,0,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,18,1,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,19,2,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,20,3,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,21,4,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,22,5,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,23,6,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-10,24,7,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,24,7,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,23,6,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,22,5,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,21,4,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,20,3,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,19,2,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,18,1,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,17,0,3,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,89,0,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,80,7,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,77,4,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,79,6,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,78,5,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,75,2,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,76,3,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,74,1,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,73,0,10,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,72,7,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,71,6,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,68,3,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,70,5,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,69,4,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,67,2,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,66,1,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,65,0,9,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,54,5,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,55,6,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,53,4,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3990,0),(-1,56,7,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,52,3,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,51,2,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,50,1,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,49,0,7,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,48,7,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,46,5,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,45,4,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,47,6,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,44,3,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,43,2,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,42,1,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,31,6,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,41,0,6,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,32,7,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,30,5,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,29,4,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,28,3,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,25,0,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,27,2,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,26,1,4,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,8,7,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,7,6,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,6,5,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,92,3,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,93,4,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,95,6,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,94,5,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,96,7,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,5,4,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,4,3,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,2,1,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,3,2,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,1,0,1,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,82,1,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,81,0,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,84,3,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,83,2,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,86,5,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,87,6,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,85,4,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,88,7,11,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,58,1,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,57,0,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,59,2,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,60,3,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,64,7,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3960,0),(-1,61,4,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,63,6,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,62,5,8,'FEPC3',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,34,1,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,35,2,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,33,0,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,36,3,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,37,4,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,38,5,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,40,7,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,39,6,5,'FEPC2',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,16,7,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,14,5,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,15,6,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,13,4,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,12,3,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,11,2,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,10,1,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,9,0,2,'FEPC1',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,91,2,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0),(-1,90,1,12,'FEPC4',4000,5,'2','2',1,'1',3995,8000,1,3995,0);
/*!40000 ALTER TABLE `Settings_PMTs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Settings_WFDs`
--

DROP TABLE IF EXISTS `Settings_WFDs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Settings_WFDs` (
  `run_number` int(11) DEFAULT NULL,
  `pc_name` varchar(20) DEFAULT NULL,
  `BdType` varchar(10) DEFAULT NULL,
  `VMEBaseAddress` varchar(20) DEFAULT NULL,
  `Link` varchar(10) DEFAULT NULL,
  `VMEControl` varchar(10) DEFAULT NULL,
  `ChannelsConfig` varchar(10) DEFAULT NULL,
  `MemConfig` varchar(10) DEFAULT NULL,
  `FrontPanel` varchar(10) DEFAULT NULL,
  `TriggerSource` varchar(10) DEFAULT NULL,
  `TriggerOutput` varchar(10) DEFAULT NULL,
  `AcquisitionControl` varchar(10) DEFAULT NULL,
  `MonitorMode` varchar(10) DEFAULT NULL,
  `thread_name` varchar(20) DEFAULT NULL,
  `CanStart` tinyint(1) DEFAULT NULL,
  `BoardID` int(11) DEFAULT NULL,
  `BdNum` int(11) DEFAULT NULL,
  `PostSamples` int(11) DEFAULT NULL,
  `InternalDelay` int(11) DEFAULT NULL,
  `CustomSize` int(11) DEFAULT NULL,
  `zs_thresh_edge_trig` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Settings_WFDs`
--

LOCK TABLES `Settings_WFDs` WRITE;
/*!40000 ALTER TABLE `Settings_WFDs` DISABLE KEYS */;
INSERT INTO `Settings_WFDs` VALUES (-10,'FEPC1','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC1_V1720',0,2,1,756,30,0,1),(-10,'FEPC2','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC2_V1720',0,5,1,756,21,0,1),(-10,'FEPC3','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC3_V1720',0,8,1,756,12,0,1),(-10,'FEPC4','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC4_V1720',0,11,1,756,3,0,1),(-10,'FEPC2','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC2_V1720',0,6,2,756,18,0,1),(-10,'FEPC4','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC4_V1720',0,12,2,756,0,0,1),(-10,'FEPC4','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC4_V1720',0,10,0,756,6,0,1),(-10,'FEPC3','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC3_V1720',0,7,0,756,15,0,1),(-10,'FEPC2','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC2_V1720',0,4,0,756,24,0,1),(-10,'FEPC3','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC3_V1720',0,9,2,756,9,0,1),(-10,'FEPC1','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC1_V1720',1,1,0,756,33,0,1),(-10,'FEPC1','PCI','0','0','0x30','0x00010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC1_V1720',0,3,2,756,27,0,1),(-1,'FEPC1','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC1_V1720',0,3,2,831,27,0,1),(-1,'FEPC1','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC1_V1720',1,1,0,831,33,0,1),(-1,'FEPC3','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC3_V1720',0,9,2,831,9,0,1),(-1,'FEPC2','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC2_V1720',0,4,0,831,24,0,1),(-1,'FEPC3','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC3_V1720',0,7,0,831,15,0,1),(-1,'FEPC4','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC4_V1720',0,10,0,831,6,0,1),(-1,'FEPC4','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC4_V1720',0,12,2,831,0,0,1),(-1,'FEPC2','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC2_V1720',0,6,2,831,18,0,1),(-1,'FEPC4','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC4_V1720',0,11,1,831,3,0,1),(-1,'FEPC3','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC3_V1720',0,8,1,831,12,0,1),(-1,'FEPC2','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC2_V1720',0,5,1,831,21,0,1),(-1,'FEPC1','PCI','0','0','0x30','0x20010','0x8','0x0281','0xC0000000','0xC0000000','0xE','0x0','FEPC1_V1720',0,2,1,831,30,0,1);
/*!40000 ALTER TABLE `Settings_WFDs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Thread_Defaults`
--

DROP TABLE IF EXISTS `Thread_Defaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Thread_Defaults` (
  `thread_type` varchar(40) DEFAULT NULL,
  `data_01` varchar(40) DEFAULT NULL,
  `data_02` varchar(40) DEFAULT NULL,
  `data_03` varchar(40) DEFAULT NULL,
  `data_04` varchar(40) DEFAULT NULL,
  `data_05` varchar(40) DEFAULT NULL,
  `data_06` varchar(40) DEFAULT NULL,
  `data_07` varchar(40) DEFAULT NULL,
  `data_08` varchar(40) DEFAULT NULL,
  `data_09` varchar(40) DEFAULT NULL,
  `data_10` varchar(40) DEFAULT NULL,
  `data_11` varchar(40) DEFAULT NULL,
  `data_12` varchar(40) DEFAULT NULL,
  `data_13` varchar(40) DEFAULT NULL,
  `data_14` varchar(40) DEFAULT NULL,
  `data_15` varchar(40) DEFAULT NULL,
  `data_16` varchar(40) DEFAULT NULL,
  `data_17` varchar(40) DEFAULT NULL,
  `data_18` varchar(40) DEFAULT NULL,
  `data_19` varchar(40) DEFAULT NULL,
  `data_20` varchar(40) DEFAULT NULL,
  `data_21` varchar(40) DEFAULT NULL,
  `data_22` varchar(40) DEFAULT NULL,
  `data_23` varchar(40) DEFAULT NULL,
  `data_24` varchar(40) DEFAULT NULL,
  `data_25` varchar(40) DEFAULT NULL,
  `data_26` varchar(40) DEFAULT NULL,
  `data_27` varchar(40) DEFAULT NULL,
  `data_28` varchar(40) DEFAULT NULL,
  `data_29` varchar(40) DEFAULT NULL,
  `data_30` varchar(40) DEFAULT NULL,
  `data_31` varchar(40) DEFAULT NULL,
  `data_32` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Thread_Defaults`
--

LOCK TABLES `Thread_Defaults` WRITE;
/*!40000 ALTER TABLE `Thread_Defaults` DISABLE KEYS */;
INSERT INTO `Thread_Defaults` VALUES ('DCMessageClient','0.0.0.0','NULL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DecisionMaker','DRPC_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPCAssembler','DRPC_MM',NULL,'0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPCBadEvent','DRPC_MM','0','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRTriggerAssembler','0x3E8','0x39D','0x40','DRPC_MM','0','0','0','0xFF','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPCAssembler','EBPC_MM',NULL,NULL,NULL,NULL,'0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPusher','DRPC_MM',NULL,'0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V1720READER','MBLK_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRGetter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPusher',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('PackData',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockCompressor',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockWriter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCAssembler',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPusher',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MySQLPusher',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCBadEvent',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('PrintServer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Control',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('TObjPrinter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RunControl','NULL','NULL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V1720FakeReader',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRFakeTriggerAssembler','DRPC_MM','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DQMSender',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockCompressorTest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPCBadEvent','EBPC_MM','0','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RATWriter','EBPC_MM','0','100000','1048576','1','37000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Thread_Defaults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Thread_Necessary`
--

DROP TABLE IF EXISTS `Thread_Necessary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Thread_Necessary` (
  `thread_type` varchar(40) DEFAULT NULL,
  `data_01` varchar(40) DEFAULT NULL,
  `data_02` varchar(40) DEFAULT NULL,
  `data_03` varchar(40) DEFAULT NULL,
  `data_04` varchar(40) DEFAULT NULL,
  `data_05` varchar(40) DEFAULT NULL,
  `data_06` varchar(40) DEFAULT NULL,
  `data_07` varchar(40) DEFAULT NULL,
  `data_08` varchar(40) DEFAULT NULL,
  `data_09` varchar(40) DEFAULT NULL,
  `data_10` varchar(40) DEFAULT NULL,
  `data_11` varchar(40) DEFAULT NULL,
  `data_12` varchar(40) DEFAULT NULL,
  `data_13` varchar(40) DEFAULT NULL,
  `data_14` varchar(40) DEFAULT NULL,
  `data_15` varchar(40) DEFAULT NULL,
  `data_16` varchar(40) DEFAULT NULL,
  `data_17` varchar(40) DEFAULT NULL,
  `data_18` varchar(40) DEFAULT NULL,
  `data_19` varchar(40) DEFAULT NULL,
  `data_20` varchar(40) DEFAULT NULL,
  `data_21` varchar(40) DEFAULT NULL,
  `data_22` varchar(40) DEFAULT NULL,
  `data_23` varchar(40) DEFAULT NULL,
  `data_24` varchar(40) DEFAULT NULL,
  `data_25` varchar(40) DEFAULT NULL,
  `data_26` varchar(40) DEFAULT NULL,
  `data_27` varchar(40) DEFAULT NULL,
  `data_28` varchar(40) DEFAULT NULL,
  `data_29` varchar(40) DEFAULT NULL,
  `data_30` varchar(40) DEFAULT NULL,
  `data_31` varchar(40) DEFAULT NULL,
  `data_32` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Thread_Necessary`
--

LOCK TABLES `Thread_Necessary` WRITE;
/*!40000 ALTER TABLE `Thread_Necessary` DISABLE KEYS */;
INSERT INTO `Thread_Necessary` VALUES ('DRTriggerAssembler',NULL,NULL,NULL,NULL,'YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRGetter','YES','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DCMessageClient','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DecisionMaker','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPCAssembler','YES','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPCBadEvent','YES','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPusher','YES','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V1720READER','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Control',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPCAssembler',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPusher',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCAssembler',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCBadEvent',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPusher',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockCompressor',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockWriter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MySQLPusher','YES','YES',NULL,'YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('PackData',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('PrintServer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('TObjPrinter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RunControl',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V1720FakeReader','YES','YES','YES','NO','NO','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRFakeTriggerAssembler','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DQMSender','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPCBadEvent','YES','YES','YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RATWriter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockCompressorTest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Thread_Necessary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Thread_Templates`
--

DROP TABLE IF EXISTS `Thread_Templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Thread_Templates` (
  `thread_type` varchar(40) DEFAULT NULL,
  `data_01` varchar(40) DEFAULT NULL,
  `data_02` varchar(40) DEFAULT NULL,
  `data_03` varchar(40) DEFAULT NULL,
  `data_04` varchar(40) DEFAULT NULL,
  `data_05` varchar(40) DEFAULT NULL,
  `data_06` varchar(40) DEFAULT NULL,
  `data_07` varchar(40) DEFAULT NULL,
  `data_08` varchar(40) DEFAULT NULL,
  `data_09` varchar(40) DEFAULT NULL,
  `data_10` varchar(40) DEFAULT NULL,
  `data_11` varchar(40) DEFAULT NULL,
  `data_12` varchar(40) DEFAULT NULL,
  `data_13` varchar(40) DEFAULT NULL,
  `data_14` varchar(40) DEFAULT NULL,
  `data_15` varchar(40) DEFAULT NULL,
  `data_16` varchar(40) DEFAULT NULL,
  `data_17` varchar(40) DEFAULT NULL,
  `data_18` varchar(40) DEFAULT NULL,
  `data_19` varchar(40) DEFAULT NULL,
  `data_20` varchar(40) DEFAULT NULL,
  `data_21` varchar(40) DEFAULT NULL,
  `data_22` varchar(40) DEFAULT NULL,
  `data_23` varchar(40) DEFAULT NULL,
  `data_24` varchar(40) DEFAULT NULL,
  `data_25` varchar(40) DEFAULT NULL,
  `data_26` varchar(40) DEFAULT NULL,
  `data_27` varchar(40) DEFAULT NULL,
  `data_28` varchar(40) DEFAULT NULL,
  `data_29` varchar(40) DEFAULT NULL,
  `data_30` varchar(40) DEFAULT NULL,
  `data_31` varchar(40) DEFAULT NULL,
  `data_32` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Thread_Templates`
--

LOCK TABLES `Thread_Templates` WRITE;
/*!40000 ALTER TABLE `Thread_Templates` DISABLE KEYS */;
INSERT INTO `Thread_Templates` VALUES ('DRGetter','MEMORYMANAGER','ADDR','PORT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPusher','MEMORYMANAGER','ADDR','PORT','EventIntegrationStartTime','EventIntegrationEndTime','PromptIntegrationEndTime',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('PackData','MEMORYMANAGER','MEMORYMANAGER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockCompressor','LEVEL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockWriter','MEMORYMANAGER','WRITEMODE','BLOCKSPERFILE','OUTPUTDIRECTORY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPCBadEvent','MEMORYMANAGER','TIMEWINDOW','EVENTTHRESH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPusher','MEMORYMANAGER','PORT','LISTENADDRESS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPCAssembler','MEMORYMANAGER','EventIntegrationStartTime','EventIntegrationEndTime','PromptIntegrationEndTime','PORT','LISTENADDRESS','MEMORYMANAGER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPCAssembler','MEMORYMANAGER','PORT','LISTENADDRESS','TRIGGER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DCMessageClient','ADDR','PORT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRTriggerAssembler','GATEDURATION','TRIGGEROFFSET','TRIGGERACTIVETIME','MEMORYMANAGER','CLOCKLOW','CLOCKHIGH','NEXTEVENT','TRIGGERMASK','USEEXTERNALCLOCK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCAssembler','MEMORYMANAGER','MEMORYMANAGER','EventIntegrationStartTime','EventIntegrationEndTime','PromptIntegrationEndTime',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRPusher','MEMORYMANAGER','ADDR','PORT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MySQLPusher','SERVER','USER','PASSWORD','DATABASE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DecisionMaker','MEMORYMANAGER','CHECKHEADERS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V1720READER','MEMORYMANAGER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('FEPCBadEvent','MEMORYMANAGER','TIMEWINDOW','EVENTTHRESH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('PrintServer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Control',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('TObjPrinter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RunControl','UPDATE_RUNLOCK_NAME','UPDATE_RUNLOCK_ADDR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V1720FakeReader','MEMORYMANAGER','EVENTSPERBLOCK','FILENAME','MEMORYLOOP','EVENTPERIOD','LOOPOFFSET','EVENTFREQUENCY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DRFakeTriggerAssembler','MEMORYMANAGER','TRIGGERPERIOD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('DQMSender','MEMORYMANAGER','FIFO','ZMQPORT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RATWriter','MEMORYMANAGER','EVENTS','SUBRUNEVENTS','AUTOSAVE','COMPRESSION','BRANCHBUFFER',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('EBPCBadEvent','MEMORYMANAGER','TIMEWINDOW','EVENTTHRESH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('MemBlockCompressorTest','LEVEL','RAW','COMPRESSED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Thread_Templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Thread_Threads`
--

DROP TABLE IF EXISTS `Thread_Threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Thread_Threads` (
  `run_number` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `thread_name` varchar(40) DEFAULT NULL,
  `thread_type` varchar(40) DEFAULT NULL,
  `pc_name` varchar(40) DEFAULT NULL,
  `data_01` varchar(20) DEFAULT NULL,
  `data_02` varchar(20) DEFAULT NULL,
  `data_03` varchar(20) DEFAULT NULL,
  `data_04` varchar(20) DEFAULT NULL,
  `data_05` varchar(20) DEFAULT NULL,
  `data_06` varchar(20) DEFAULT NULL,
  `data_07` varchar(20) DEFAULT NULL,
  `data_08` varchar(20) DEFAULT NULL,
  `data_09` varchar(20) DEFAULT NULL,
  `data_10` varchar(20) DEFAULT NULL,
  `data_11` varchar(20) DEFAULT NULL,
  `data_12` varchar(20) DEFAULT NULL,
  `data_13` varchar(20) DEFAULT NULL,
  `data_14` varchar(20) DEFAULT NULL,
  `data_15` varchar(20) DEFAULT NULL,
  `data_16` varchar(20) DEFAULT NULL,
  `data_17` varchar(20) DEFAULT NULL,
  `data_18` varchar(20) DEFAULT NULL,
  `data_19` varchar(20) DEFAULT NULL,
  `data_20` varchar(20) DEFAULT NULL,
  `data_21` varchar(20) DEFAULT NULL,
  `data_22` varchar(20) DEFAULT NULL,
  `data_23` varchar(20) DEFAULT NULL,
  `data_24` varchar(20) DEFAULT NULL,
  `data_25` varchar(20) DEFAULT NULL,
  `data_26` varchar(20) DEFAULT NULL,
  `data_27` varchar(20) DEFAULT NULL,
  `data_28` varchar(20) DEFAULT NULL,
  `data_29` varchar(20) DEFAULT NULL,
  `data_30` varchar(20) DEFAULT NULL,
  `data_31` varchar(20) DEFAULT NULL,
  `data_32` varchar(20) DEFAULT NULL,
  `channel_map` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Thread_Threads`
--

LOCK TABLES `Thread_Threads` WRITE;
/*!40000 ALTER TABLE `Thread_Threads` DISABLE KEYS */;
INSERT INTO `Thread_Threads` VALUES (-10,2,'FEPC4_Assembler','FEPCAssembler','FEPC4','MBLK4_MM','FEPC4_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,13,'DRPC_BadEvent','DRPCBadEvent','DREBPC','DRPC_MM','10','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'FEPC1_V1720','V1720READER','FEPC1','MBLK1_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,4,'FEPC4_DRGetter','DRGetter','FEPC4','FEPC4_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,9,'EBPC_Assembler_1','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9997','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,0,'FEPC4','DCMessageClient','local_conn','192.168.0.5','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'DQMSender','DQMSender','DREBPC','RATCopy_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,5,'DRPC_Assembler4','DRPCAssembler','DREBPC','DRPC_MM','9992','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,2,'FEPC3_Assembler','FEPCAssembler','FEPC3','MBLK3_MM','FEPC3_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,4,'FEPC1_DRGetter','DRGetter','FEPC1','FEPC1_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,5,'RunControl','RunControl','local','MC-DAQ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,NULL,'EBPCWrite','RATWRiter','DREBPC',NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'FEPC3_V1720','V1720READER','FEPC3','MBLK3_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,3,'FEPC3_DRPusher','DRPusher','FEPC3','FEPC3_MM','192.168.0.1','9994',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,3,'FEPC1_DRPusher','DRPusher','FEPC1','FEPC1_MM','192.168.0.1','9999',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,5,'FEPC4_EBPusher','EBPusher','FEPC4','FEPC4_MM','192.168.1.1','9991','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,4,'FEPC3_DRGetter','DRGetter','FEPC3','FEPC3_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,11,'EBPC_Assembler_3','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9993','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,5,'FEPC2_EBPusher','EBPusher','FEPC2','FEPC2_MM','192.168.1.1','9995','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,5,'FEPC3_EBPusher','EBPusher','FEPC3','FEPC3_MM','192.168.1.1','9993','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,4,'FEPC2_DRGetter','DRGetter','FEPC2','FEPC2_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,6,'FEPC3_BadEvent','FEPCBadEvent','FEPC3','FEPC3_MM','10','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,8,'FE_Pusher','FEPusher','DREBPC','DRPC_MM','9998','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,0,'FEPC3','DCMessageClient','local_conn','192.168.0.4','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,2,'FEPC2_Assembler','FEPCAssembler','FEPC2','MBLK2_MM','FEPC2_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,1,'FEPC4_V1720','V1720READER','FEPC4','MBLK4_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,6,'Decision_Maker','DecisionMaker','DREBPC','DRPC_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,3,'DRPC_Assembler2','DRPCAssembler','DREBPC','DRPC_MM','9996','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,5,'FEPC1_EBPusher','EBPusher','FEPC1','FEPC1_MM','192.168.1.1','9997','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,4,'DRPC_Assembler3','DRPCAssembler','DREBPC','DRPC_MM','9994','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,3,'FEPC2_DRPusher','DRPusher','FEPC2','FEPC2_MM','192.168.0.1','9996',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,2,'FEPC1_Assembler','FEPCAssembler','FEPC1','MBLK1_MM','FEPC1_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,6,'FEPC2_BadEvent','FEPCBadEvent','FEPC2','FEPC2_MM','10','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,1,'FEPC2_V1720','V1720READER','FEPC2','MBLK2_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,0,'FEPC1','DCMessageClient','local_conn','192.168.0.2','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,10,'EBPC_Assembler_2','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9995','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,0,'DREBPC','DCMessageClient','local_conn','192.168.0.1','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,3,'FEPC4_DRPusher','DRPusher','FEPC4','FEPC4_MM','192.168.0.1','9992',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,6,'FEPC1_BadEvent','FEPCBadEvent','FEPC1','FEPC1_MM','10','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,12,'EBPC_Assembler_4','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9991','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,2,'DRPC_Assembler1','DRPCAssembler','DREBPC','DRPC_MM','9999','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,1,'DRPC_Trigger','DRTriggerAssembler','DREBPC','0x7D0','0x100','0x10','DRPC_MM','0',NULL,NULL,'0x80',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-10,14,'EBPC_BadEvent','EBPCBadEvent','DREBPC','EBPC_MM','0','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,0,'FEPC2','DCMessageClient','local_conn','192.168.0.3','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-10,6,'FEPC4_BadEvent','FEPCBadEvent','FEPC4','FEPC4_MM','10','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,16,'Compressor2','MemBlockCompressor','DREBPC','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'FEPC1_V1720','V1720READER','FEPC1','MBLK1_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,3,'FEPC1_DRPusher','DRPusher','FEPC1','FEPC1_MM','192.168.0.1','9999',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,1,'FEPC3_V1720','V1720READER','FEPC3','MBLK3_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,14,'EBPC_BadEvent','EBPCBadEvent','DREBPC','EBPC_MM','0','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'DRPC_Assembler1','DRPCAssembler','DREBPC','DRPC_MM','9999','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,5,'FEPC1_EBPusher','EBPusher','FEPC1','FEPC1_MM','192.168.1.1','9997','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,0,'FEPC4','DCMessageClient','local_conn','192.168.0.5','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'FEPC4_V1720','V1720READER','FEPC4','MBLK4_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,18,'Compressor4','MemBlockCompressor','DREBPC','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'DQMSender','DQMSender','DREBPC','RATCopy_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC4_Assembler','FEPCAssembler','FEPC4','MBLK4_MM','FEPC4_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,17,'Compressor3','MemBlockCompressor','DREBPC','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,1,'DRPC_Trigger','DRTriggerAssembler','DREBPC','0x7D0','0x100','0x10','DRPC_MM','0',NULL,NULL,'0x82',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,0,'FEPC2','DCMessageClient','local_conn','192.168.0.3','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,12,'EBPC_Assembler_4','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9991','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,4,'DRPC_Assembler3','DRPCAssembler','DREBPC','DRPC_MM','9994','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,3,'FEPC4_DRPusher','DRPusher','FEPC4','FEPC4_MM','192.168.0.1','9992',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,13,'DRPC_BadEvent','DRPCBadEvent','DREBPC','DRPC_MM','10','1000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,6,'FEPC4_BadEvent','FEPCBadEvent','FEPC4','FEPC4_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,9,'EBPC_Assembler_1','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9997','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,10,'EBPC_Assembler_2','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9995','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,3,'FEPC2_DRPusher','DRPusher','FEPC2','FEPC2_MM','192.168.0.1','9996',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,14,'PackData1','PackData','DREBPC','RAWMBLK_MM','RATCopy_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,0,'DREBPC','DCMessageClient','local_conn','192.168.0.1','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,6,'FEPC1_BadEvent','FEPCBadEvent','FEPC1','FEPC1_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,6,'FEPC2_BadEvent','FEPCBadEvent','FEPC2','FEPC2_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,0,'FEPC1','DCMessageClient','local_conn','192.168.0.2','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC1_Assembler','FEPCAssembler','FEPC1','MBLK1_MM','FEPC1_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,1,'FEPC2_V1720','V1720READER','FEPC2','MBLK2_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,5,'FEPC3_EBPusher','EBPusher','FEPC3','FEPC3_MM','192.168.1.1','9993','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,5,'FEPC2_EBPusher','EBPusher','FEPC2','FEPC2_MM','192.168.1.1','9995','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,6,'FEPC3_BadEvent','FEPCBadEvent','FEPC3','FEPC3_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,8,'FE_Pusher','FEPusher','DREBPC','DRPC_MM','9998','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,6,'Decision_Maker','DecisionMaker','DREBPC','DRPC_MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,11,'EBPC_Assembler_3','EBPCAssembler','DREBPC','EBPC_MM','192','10000','300','9993','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,15,'Compressor1','MemBlockCompressor','DREBPC','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,0,'FEPC3','DCMessageClient','local_conn','192.168.0.4','1717',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,3,'DRPC_Assembler2','DRPCAssembler','DREBPC','DRPC_MM','9996','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,4,'FEPC2_DRGetter','DRGetter','FEPC2','FEPC2_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC2_Assembler','FEPCAssembler','FEPC2','MBLK2_MM','FEPC2_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,5,'FEPC4_EBPusher','EBPusher','FEPC4','FEPC4_MM','192.168.1.1','9991','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,19,'MemBlockWriter','MemBlockWriter','DREBPC','CMPMBLK_MM','0','40','../data/',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,5,'RunControl','RunControl','local','MC-DAQ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,3,'FEPC3_DRPusher','DRPusher','FEPC3','FEPC3_MM','192.168.0.1','9994',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,4,'FEPC3_DRGetter','DRGetter','FEPC3','FEPC3_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,2,'FEPC3_Assembler','FEPCAssembler','FEPC3','MBLK3_MM','FEPC3_MM','190','10000','300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,4,'FEPC4_DRGetter','DRGetter','FEPC4','FEPC4_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(-1,5,'DRPC_Assembler4','DRPCAssembler','DREBPC','DRPC_MM','9992','0.0.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'YES'),(-1,4,'FEPC1_DRGetter','DRGetter','FEPC1','FEPC1_MM','192.168.0.1','9998',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Thread_Threads` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-18 16:58:50
