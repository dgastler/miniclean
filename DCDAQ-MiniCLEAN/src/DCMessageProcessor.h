#ifndef __DCMESSAGEPROCESSOR__
#define __DCMESSAGEPROCESSOR__

#include <DCThread.h>
#include <DCMessage.h>
#include <MessageStructures.h>

#include <RAT/DS/DCDAQ_timeHelper.hh>
#include <netHelper.h> //BuildSockAddrVector

#include <boost/algorithm/string.hpp> //For iequal

#include <sstream> //stringstrem

#include <DCLauncher.h>

class DCMessageProcessor
{
 public:
  DCMessageProcessor(DCLauncher * _Launcher);
  ~DCMessageProcessor(){};
  //Find and process all the DCMessages from the threads.
  //Return will update the run status of the DAQ
  bool ProcessSelect(int selectReturn,
		     fd_set ReadSet,
		     fd_set WriteSet,
		     fd_set ExceptionSet,
		     bool run);

  bool ProcessMessage(DCMessage::Message message);
  const std::string & GetName(){return(Name);};
 private:
  DCLauncher * Launcher;

  void RegisterCommands();
  //Message processing functions
  void Launch(DCMessage::Message & message,bool & run);
  void RegisterAddr(DCMessage::Message & message,bool & run);
  void UnRegisterAddr(DCMessage::Message & message,bool & run);
  void RegisterType(DCMessage::Message & message,bool & run);
  void UnRegisterType(DCMessage::Message & message,bool & run);
  void Stop(DCMessage::Message & message,bool & run);
  void Go(DCMessage::Message & message,bool & run);
  void Pause(DCMessage::Message & message,bool & run);
  void RunNumber(DCMessage::Message & message,bool & run);
  void GetStats(DCMessage::Message & message,bool & run);
  void GetRoute(DCMessage::Message & message,bool & run);
  void GetStatsVectorHash(DCMessage::Message & message,bool & run);
  void InstallPrintThread(DCMessage::Message & message,bool &run);
  void ProcessCommand(DCMessage::Message & message, bool &run);

  DCThread * FindThread(std::string);

  //Map of remote addresses to local thread names
  //the index is a pair of AF type and address vectors
  std::map<DCMessage::DCAddress, std::string,DCMessage::DCAddress> messageAddrToNameMap;
  void RemoveThreadRoute(const std::string & threadName);
  std::vector<DCThread *> RemoteRoutesForAddress(const DCMessage::DCAddress & dest);
  bool RouteRemoteMessage(DCMessage::Message message);
  bool RouteLocalMessage(DCMessage::Message message);

  //Map of remote addresses to registered functions
  //the index is a pair of AF type and address vectors
  std::map< uint16_t, DCMessage::DCAddress> messageTypeToAddressMap;
  void RemoveThreadTypeRoute(const std::string & threadName);
  //Map of message types to registered functions the index is a DCMessageType
  std::map<uint16_t, void (DCMessageProcessor::*)(DCMessage::Message &,bool &) > messageTypeToFunctionMap ;
  bool RouteMessageType(DCMessage::Message message);
  bool TypeRoute(DCMessage::Message & message);
  bool TypeFunction(DCMessage::Message &message, void (DCMessageProcessor::*  & func)(DCMessage::Message &, bool &));

  void BuildError(DCMessage::SingleUINT64 errorData);


  //Display of routing information
  void PrintRemoteRouteTables();
  void PrintTypeThreadRouteTables();
  void PrintTypeFunctionRouteTables();

  std::string Name;
};

#endif
