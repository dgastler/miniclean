VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan2e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL XLXN_14
        SIGNAL XLXN_32
        SIGNAL XLXN_121
        SIGNAL XLXN_128
        SIGNAL Add16(15:0)
        SIGNAL XLXN_142(15:0)
        SIGNAL PULSECOUNTBus(15:0)
        SIGNAL XLXN_152
        SIGNAL XLXN_153(15:0)
        SIGNAL FreqCount(15:0)
        SIGNAL XLXN_226(15:0)
        SIGNAL XLXN_227(15:0)
        SIGNAL XLXN_245
        SIGNAL XLXN_254
        SIGNAL XLXN_257
        SIGNAL XLXN_260
        SIGNAL XLXN_210
        SIGNAL XLXN_211
        SIGNAL PulseCount4
        SIGNAL PulseCount3
        SIGNAL PulseCount2
        SIGNAL PULSECOUNTBus(3)
        SIGNAL PULSECOUNTBus(4)
        SIGNAL PULSECOUNTBus(2)
        SIGNAL PulseCount0
        SIGNAL PULSECOUNTBus(0)
        SIGNAL PulseCount1
        SIGNAL PULSECOUNTBus(7:5)
        SIGNAL PULSECOUNTBus(7:0)
        SIGNAL LED(7:0)
        SIGNAL PULSECOUNTBus(1)
        SIGNAL XLXN_217
        SIGNAL PULSE
        SIGNAL FreqCount(11)
        SIGNAL FreqCount(9)
        SIGNAL FreqCount(8)
        SIGNAL FreqCount(7)
        SIGNAL FreqInput5
        SIGNAL FreqInput4
        SIGNAL FreqInput3
        SIGNAL FreqInput2
        SIGNAL FreqInput1
        SIGNAL FreqInput0
        SIGNAL FreqInput6
        SIGNAL FreqCount(13)
        SIGNAL FreqCount(12)
        SIGNAL FreqCount(10)
        SIGNAL TRIG
        SIGNAL FreqCount(15:14)
        SIGNAL FreqCount(6:0)
        SIGNAL CLOCK
        SIGNAL XLXN_264
        SIGNAL XLXN_266
        SIGNAL XLXN_267
        SIGNAL XLXN_268
        SIGNAL XLXN_269
        SIGNAL XLXN_270
        SIGNAL XLXN_271
        PORT Input PulseCount4
        PORT Input PulseCount3
        PORT Input PulseCount2
        PORT Input PulseCount0
        PORT Input PulseCount1
        PORT Output LED(7:0)
        PORT Output PULSE
        PORT Input FreqInput5
        PORT Input FreqInput4
        PORT Input FreqInput3
        PORT Input FreqInput2
        PORT Input FreqInput1
        PORT Input FreqInput0
        PORT Input FreqInput6
        PORT Output TRIG
        PORT Input CLOCK
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF constant
            TIMESTAMP 2006 1 1 10 10 10
            RECTANGLE N 0 0 112 64 
            LINE N 144 32 112 32 
        END BLOCKDEF
        BEGIN BLOCKDEF fdc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            RECTANGLE N 64 -320 320 -64 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF cb16ce
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 384 -192 320 -192 
            RECTANGLE N 320 -268 384 -244 
            LINE N 384 -256 320 -256 
            LINE N 0 -192 64 -192 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 384 -128 320 -128 
            RECTANGLE N 64 -320 320 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF compm16
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 384 -256 320 -256 
            LINE N 384 -192 320 -192 
            RECTANGLE N 64 -384 320 -64 
            RECTANGLE N 0 -332 64 -308 
            LINE N 0 -320 64 -320 
            RECTANGLE N 0 -140 64 -116 
            LINE N 0 -128 64 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF comp16
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -384 320 -64 
            LINE N 0 -128 64 -128 
            RECTANGLE N 0 -140 64 -116 
            LINE N 0 -320 64 -320 
            RECTANGLE N 0 -332 64 -308 
            LINE N 384 -224 320 -224 
        END BLOCKDEF
        BEGIN BLOCKDEF cb4ce
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -512 320 -64 
            LINE N 0 -32 64 -32 
            LINE N 0 -128 64 -128 
            LINE N 384 -256 320 -256 
            LINE N 384 -320 320 -320 
            LINE N 384 -384 320 -384 
            LINE N 384 -448 320 -448 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 384 -128 320 -128 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 0 -192 64 -192 
            LINE N 384 -192 320 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF add16
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 448 -128 384 -128 
            LINE N 448 -64 384 -64 
            LINE N 240 -64 384 -64 
            LINE N 240 -124 240 -64 
            RECTANGLE N 0 -204 64 -180 
            RECTANGLE N 0 -332 64 -308 
            LINE N 0 -320 64 -320 
            LINE N 0 -192 64 -192 
            LINE N 448 -256 384 -256 
            RECTANGLE N 384 -268 448 -244 
            LINE N 0 -448 64 -448 
            LINE N 128 -448 64 -448 
            LINE N 128 -416 128 -448 
            LINE N 64 -288 64 -432 
            LINE N 128 -256 64 -288 
            LINE N 64 -224 128 -256 
            LINE N 64 -80 64 -224 
            LINE N 384 -160 64 -80 
            LINE N 384 -336 384 -160 
            LINE N 384 -352 384 -336 
            LINE N 64 -432 384 -352 
            LINE N 336 -128 336 -148 
            LINE N 384 -128 336 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF ibuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF inv
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 160 -32 
            LINE N 64 -64 128 -32 
            LINE N 128 -32 64 0 
            LINE N 64 0 64 -64 
            CIRCLE N 128 -48 160 -16 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf8
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            RECTANGLE N 0 -44 64 -20 
            RECTANGLE N 128 -44 224 -20 
        END BLOCKDEF
        BEGIN BLOCKDEF cb2ce
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -384 320 -64 
            LINE N 384 -128 320 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -128 64 -128 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 0 -192 64 -192 
            LINE N 384 -192 320 -192 
            LINE N 384 -256 320 -256 
            LINE N 384 -320 320 -320 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCK XLXI_11 vcc
            PIN P XLXN_14
        END BLOCK
        BEGIN BLOCK XLXI_19 vcc
            PIN P XLXN_32
        END BLOCK
        BEGIN BLOCK XLXI_50 fdc
            PIN C XLXN_254
            PIN CLR XLXN_264
            PIN D XLXN_267
            PIN Q XLXN_128
        END BLOCK
        BEGIN BLOCK XLXI_53 constant
            BEGIN ATTR CValue "0000"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Hexadecimal
            END ATTR
            PIN O XLXN_142(15:0)
        END BLOCK
        BEGIN BLOCK XLXI_59 cb16ce
            PIN C XLXN_267
            PIN CE XLXN_14
            PIN CLR XLXN_268
            PIN CEO
            PIN Q(15:0) XLXN_226(15:0)
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_61 compm16
            PIN A(15:0) PULSECOUNTBus(15:0)
            PIN B(15:0) XLXN_226(15:0)
            PIN GT
            PIN LT XLXN_264
        END BLOCK
        BEGIN BLOCK XLXI_62 comp16
            PIN A(15:0) XLXN_227(15:0)
            PIN B(15:0) Add16(15:0)
            PIN EQ XLXN_268
        END BLOCK
        BEGIN BLOCK XLXI_63 comp16
            PIN A(15:0) XLXN_226(15:0)
            PIN B(15:0) XLXN_142(15:0)
            PIN EQ XLXN_121
        END BLOCK
        BEGIN BLOCK XLXI_64 cb4ce
            PIN C XLXN_254
            PIN CE XLXN_32
            PIN CLR XLXN_152
            PIN CEO
            PIN Q0 XLXN_260
            PIN Q1 XLXN_271
            PIN Q2
            PIN Q3
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_65 gnd
            PIN G XLXN_152
        END BLOCK
        BEGIN BLOCK XLXI_66 add16
            PIN A(15:0) FreqCount(15:0)
            PIN B(15:0) XLXN_153(15:0)
            PIN CI XLXN_245
            PIN CO
            PIN OFL
            PIN S(15:0) Add16(15:0)
        END BLOCK
        BEGIN BLOCK XLXI_67 constant
            BEGIN ATTR CValue "03E8"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Hexadecimal
            END ATTR
            PIN O XLXN_153(15:0)
        END BLOCK
        BEGIN BLOCK XLXI_69 gnd
            PIN G XLXN_245
        END BLOCK
        BEGIN BLOCK XLXI_141 cb16ce
            PIN C XLXN_260
            PIN CE XLXN_270
            PIN CLR XLXN_268
            PIN CEO
            PIN Q(15:0) XLXN_227(15:0)
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_142 vcc
            PIN P XLXN_270
        END BLOCK
        BEGIN BLOCK XLXI_143 cb2ce
            PIN C XLXN_271
            PIN CE XLXN_270
            PIN CLR XLXN_257
            PIN CEO
            PIN Q0 XLXN_266
            PIN Q1 XLXN_267
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_144 and2
            PIN I0 XLXN_267
            PIN I1 XLXN_266
            PIN O XLXN_269
        END BLOCK
        BEGIN BLOCK XLXI_145 or2
            PIN I0 XLXN_269
            PIN I1 XLXN_268
            PIN O XLXN_257
        END BLOCK
        BEGIN BLOCK XLXI_103 ibuf
            PIN I PulseCount3
            PIN O XLXN_210
        END BLOCK
        BEGIN BLOCK XLXI_102 ibuf
            PIN I PulseCount4
            PIN O PULSECOUNTBus(3)
        END BLOCK
        BEGIN BLOCK XLXI_106 inv
            PIN I XLXN_210
            PIN O PULSECOUNTBus(4)
        END BLOCK
        BEGIN BLOCK XLXI_107 inv
            PIN I XLXN_211
            PIN O PULSECOUNTBus(2)
        END BLOCK
        BEGIN BLOCK XLXI_104 ibuf
            PIN I PulseCount2
            PIN O XLXN_211
        END BLOCK
        BEGIN BLOCK XLXI_82 constant
            BEGIN ATTR CValue "000"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Binary
            END ATTR
            PIN O PULSECOUNTBus(7:5)
        END BLOCK
        BEGIN BLOCK XLXI_91 ibuf
            PIN I PulseCount0
            PIN O PULSECOUNTBus(0)
        END BLOCK
        BEGIN BLOCK XLXI_92 ibuf
            PIN I PulseCount1
            PIN O XLXN_217
        END BLOCK
        BEGIN BLOCK XLXI_138 obuf8
            PIN I(7:0) PULSECOUNTBus(7:0)
            PIN O(7:0) LED(7:0)
        END BLOCK
        BEGIN BLOCK XLXI_140 inv
            PIN I XLXN_217
            PIN O PULSECOUNTBus(1)
        END BLOCK
        BEGIN BLOCK XLXI_8 obuf
            PIN I XLXN_128
            PIN O PULSE
        END BLOCK
        BEGIN BLOCK XLXI_125 ibuf
            PIN I FreqInput0
            PIN O FreqCount(7)
        END BLOCK
        BEGIN BLOCK XLXI_126 ibuf
            PIN I FreqInput1
            PIN O FreqCount(8)
        END BLOCK
        BEGIN BLOCK XLXI_127 ibuf
            PIN I FreqInput2
            PIN O FreqCount(9)
        END BLOCK
        BEGIN BLOCK XLXI_128 ibuf
            PIN I FreqInput3
            PIN O FreqCount(10)
        END BLOCK
        BEGIN BLOCK XLXI_129 ibuf
            PIN I FreqInput4
            PIN O FreqCount(11)
        END BLOCK
        BEGIN BLOCK XLXI_130 ibuf
            PIN I FreqInput5
            PIN O FreqCount(12)
        END BLOCK
        BEGIN BLOCK XLXI_137 ibuf
            PIN I FreqInput6
            PIN O FreqCount(13)
        END BLOCK
        BEGIN BLOCK XLXI_42 obuf
            PIN I XLXN_121
            PIN O TRIG
        END BLOCK
        BEGIN BLOCK XLXI_73 constant
            BEGIN ATTR CValue "00"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Binary
            END ATTR
            PIN O FreqCount(15:14)
        END BLOCK
        BEGIN BLOCK XLXI_81 constant
            BEGIN ATTR CValue "0000000"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Binary
            END ATTR
            PIN O FreqCount(6:0)
        END BLOCK
        BEGIN BLOCK XLXI_7 ibufg
            PIN I CLOCK
            PIN O XLXN_254
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN BRANCH XLXN_121
            WIRE 3072 1968 3136 1968
        END BRANCH
        BEGIN BRANCH Add16(15:0)
            WIRE 2128 2448 2208 2448
            WIRE 2208 1360 2224 1360
            WIRE 2208 1360 2208 2448
        END BRANCH
        BEGIN BRANCH XLXN_142(15:0)
            WIRE 2560 2064 2688 2064
        END BRANCH
        BEGIN BRANCH XLXN_153(15:0)
            WIRE 1616 2512 1680 2512
        END BRANCH
        BEGIN BRANCH XLXN_226(15:0)
            WIRE 1552 784 2048 784
            WIRE 2048 784 2112 784
            WIRE 2048 784 2048 1872
            WIRE 2048 1872 2688 1872
        END BRANCH
        BEGIN BRANCH XLXN_254
            WIRE 16 880 240 880
            WIRE 16 880 16 1040
            WIRE 16 1040 16 1056
            WIRE 16 1056 496 1056
            WIRE 496 1056 496 1152
            WIRE 16 1040 2592 1040
            WIRE 432 1152 496 1152
            WIRE 2592 768 2656 768
            WIRE 2592 768 2592 1040
        END BRANCH
        INSTANCE XLXI_103 528 304 R0
        INSTANCE XLXI_102 528 224 R0
        INSTANCE XLXI_106 912 304 R0
        INSTANCE XLXI_107 912 384 R0
        INSTANCE XLXI_104 528 384 R0
        BEGIN BRANCH XLXN_210
            WIRE 752 272 912 272
        END BRANCH
        BEGIN BRANCH XLXN_211
            WIRE 752 352 912 352
        END BRANCH
        BEGIN BRANCH PulseCount4
            WIRE 320 192 528 192
        END BRANCH
        BEGIN BRANCH PulseCount3
            WIRE 320 272 528 272
        END BRANCH
        BEGIN BRANCH PulseCount2
            WIRE 320 352 528 352
        END BRANCH
        BEGIN BRANCH PULSECOUNTBus(3)
            WIRE 752 192 1760 192
            WIRE 1760 192 1760 512
        END BRANCH
        BEGIN BRANCH PULSECOUNTBus(4)
            WIRE 1136 272 1680 272
            WIRE 1680 272 1680 512
        END BRANCH
        BEGIN BRANCH PULSECOUNTBus(2)
            WIRE 1136 352 1600 352
            WIRE 1600 352 1600 512
        END BRANCH
        BEGIN INSTANCE XLXI_82 112 48 R0
        END INSTANCE
        INSTANCE XLXI_91 2752 336 M0
        BEGIN BRANCH PulseCount0
            WIRE 2752 304 3248 304
        END BRANCH
        BEGIN BRANCH PULSECOUNTBus(0)
            WIRE 2000 304 2000 512
            WIRE 2000 304 2528 304
        END BRANCH
        INSTANCE XLXI_92 2752 256 M0
        BEGIN BRANCH PulseCount1
            WIRE 2752 224 3248 224
        END BRANCH
        BEGIN BRANCH PULSECOUNTBus(7:5)
            WIRE 256 80 1840 80
            WIRE 1840 80 1840 512
        END BRANCH
        INSTANCE XLXI_138 2576 464 R0
        BEGIN BRANCH PULSECOUNTBus(7:0)
            WIRE 2080 432 2576 432
            WIRE 2080 432 2080 512
        END BRANCH
        BEGIN BRANCH LED(7:0)
            WIRE 2800 432 3296 432
        END BRANCH
        INSTANCE XLXI_140 2400 192 R180
        BEGIN BRANCH PULSECOUNTBus(1)
            WIRE 1920 224 2176 224
            WIRE 1920 224 1920 512
        END BRANCH
        BEGIN BRANCH XLXN_217
            WIRE 2400 224 2528 224
        END BRANCH
        IOMARKER 320 192 PulseCount4 R180 28
        IOMARKER 320 272 PulseCount3 R180 28
        IOMARKER 320 352 PulseCount2 R180 28
        IOMARKER 3248 304 PulseCount0 R0 28
        IOMARKER 3248 224 PulseCount1 R0 28
        IOMARKER 3296 432 LED(7:0) R0 28
        BEGIN BRANCH XLXN_128
            WIRE 3040 640 3072 640
        END BRANCH
        INSTANCE XLXI_61 2112 912 R0
        BEGIN BRANCH PULSECOUNTBus(15:0)
            WIRE 1584 592 2112 592
        END BRANCH
        INSTANCE XLXI_50 2656 896 R0
        INSTANCE XLXI_8 3072 672 R0
        BEGIN BRANCH PULSE
            WIRE 3296 640 3328 640
        END BRANCH
        IOMARKER 3328 640 PULSE R0 28
        INSTANCE XLXI_125 3040 2704 R0
        INSTANCE XLXI_126 3040 2624 R0
        INSTANCE XLXI_127 3040 2544 R0
        INSTANCE XLXI_128 3040 2464 R0
        INSTANCE XLXI_129 3040 2384 R0
        INSTANCE XLXI_130 3040 2304 R0
        BEGIN BRANCH FreqCount(11)
            WIRE 3264 2352 3472 2352
        END BRANCH
        BEGIN BRANCH FreqCount(9)
            WIRE 3264 2512 3472 2512
        END BRANCH
        BEGIN BRANCH FreqCount(8)
            WIRE 3264 2592 3472 2592
        END BRANCH
        BEGIN BRANCH FreqCount(7)
            WIRE 3264 2672 3472 2672
        END BRANCH
        BEGIN BRANCH FreqInput5
            WIRE 2928 2272 3040 2272
        END BRANCH
        BEGIN BRANCH FreqInput4
            WIRE 2928 2352 3040 2352
        END BRANCH
        BEGIN BRANCH FreqInput3
            WIRE 2928 2432 3040 2432
        END BRANCH
        BEGIN BRANCH FreqInput2
            WIRE 2928 2512 3040 2512
        END BRANCH
        BEGIN BRANCH FreqInput1
            WIRE 2928 2592 3040 2592
        END BRANCH
        BEGIN BRANCH FreqInput0
            WIRE 2928 2672 3040 2672
        END BRANCH
        INSTANCE XLXI_137 3040 2224 R0
        BEGIN BRANCH FreqInput6
            WIRE 2928 2192 3040 2192
        END BRANCH
        BEGIN BRANCH FreqCount(13)
            WIRE 3264 2192 3472 2192
        END BRANCH
        BEGIN BRANCH FreqCount(12)
            WIRE 3264 2272 3472 2272
        END BRANCH
        BEGIN BRANCH FreqCount(10)
            WIRE 3264 2432 3472 2432
        END BRANCH
        IOMARKER 2928 2272 FreqInput5 R180 28
        IOMARKER 2928 2352 FreqInput4 R180 28
        IOMARKER 2928 2432 FreqInput3 R180 28
        IOMARKER 2928 2512 FreqInput2 R180 28
        IOMARKER 2928 2592 FreqInput1 R180 28
        IOMARKER 2928 2672 FreqInput0 R180 28
        IOMARKER 2928 2192 FreqInput6 R180 28
        INSTANCE XLXI_63 2688 2192 R0
        INSTANCE XLXI_42 3136 2000 R0
        BEGIN BRANCH TRIG
            WIRE 3360 1968 3392 1968
        END BRANCH
        IOMARKER 3392 1968 TRIG R0 28
        BEGIN INSTANCE XLXI_73 3152 1728 R0
        END INSTANCE
        BEGIN BRANCH FreqCount(15:14)
            WIRE 3296 1760 3456 1760
        END BRANCH
        BEGIN INSTANCE XLXI_81 3152 1840 R0
        END INSTANCE
        BEGIN BRANCH FreqCount(6:0)
            WIRE 3296 1872 3456 1872
        END BRANCH
        BEGIN INSTANCE XLXI_53 2416 2032 R0
        END INSTANCE
        INSTANCE XLXI_59 1168 1040 R0
        BEGIN BRANCH XLXN_14
            WIRE 1056 848 1168 848
        END BRANCH
        INSTANCE XLXI_11 1056 784 M90
        INSTANCE XLXI_62 2224 1488 R0
        INSTANCE XLXI_64 240 1008 R0
        BEGIN BRANCH XLXN_32
            WIRE 176 816 240 816
        END BRANCH
        INSTANCE XLXI_19 176 752 M90
        INSTANCE XLXI_65 64 912 R90
        BEGIN BRANCH XLXN_152
            WIRE 192 976 240 976
        END BRANCH
        INSTANCE XLXI_7 208 1184 R0
        BEGIN BRANCH CLOCK
            WIRE 176 1152 208 1152
        END BRANCH
        IOMARKER 176 1152 CLOCK R180 28
        INSTANCE XLXI_69 1520 2336 M270
        INSTANCE XLXI_66 1680 2704 R0
        BEGIN BRANCH XLXN_245
            WIRE 1648 2272 1664 2272
            WIRE 1664 2256 1680 2256
            WIRE 1664 2256 1664 2272
        END BRANCH
        BEGIN BRANCH FreqCount(15:0)
            WIRE 1520 2384 1680 2384
        END BRANCH
        BEGIN INSTANCE XLXI_67 1472 2480 R0
        END INSTANCE
        INSTANCE XLXI_145 1648 1888 R0
        INSTANCE XLXI_142 -32 2320 M180
        INSTANCE XLXI_144 992 2528 R0
        INSTANCE XLXI_143 320 2560 R0
        BEGIN BRANCH XLXN_257
            WIRE 240 2096 240 2528
            WIRE 240 2528 320 2528
            WIRE 240 2096 1920 2096
            WIRE 1904 1792 1920 1792
            WIRE 1920 1792 1920 2096
        END BRANCH
        BEGIN BRANCH XLXN_264
            WIRE 2496 720 2512 720
            WIRE 2512 720 2512 864
            WIRE 2512 864 2656 864
        END BRANCH
        BEGIN BRANCH XLXN_266
            WIRE 704 2240 912 2240
            WIRE 912 2240 912 2400
            WIRE 912 2400 992 2400
        END BRANCH
        BEGIN BRANCH XLXN_267
            WIRE 704 2304 848 2304
            WIRE 848 2304 848 2464
            WIRE 848 2464 992 2464
            WIRE 848 912 1168 912
            WIRE 848 912 848 1568
            WIRE 848 1568 848 2304
            WIRE 848 1568 2656 1568
            WIRE 2656 640 2656 1568
        END BRANCH
        BEGIN BRANCH XLXN_268
            WIRE 128 1616 128 1632
            WIRE 128 1632 1024 1632
            WIRE 1024 1632 1024 1760
            WIRE 1024 1760 1648 1760
            WIRE 1024 1632 1168 1632
            WIRE 1168 1632 2736 1632
            WIRE 1168 1008 1168 1632
            WIRE 2608 1264 2736 1264
            WIRE 2736 1264 2736 1632
        END BRANCH
        BEGIN BRANCH XLXN_269
            WIRE 1248 2432 1440 2432
            WIRE 1440 1824 1440 2432
            WIRE 1440 1824 1648 1824
        END BRANCH
        BEGIN BRANCH XLXN_271
            WIRE 176 1744 176 2432
            WIRE 176 2432 320 2432
            WIRE 176 1744 704 1744
            WIRE 624 624 704 624
            WIRE 704 624 704 1744
        END BRANCH
        BEGIN BRANCH XLXN_227(15:0)
            WIRE 512 1392 560 1392
            WIRE 560 1168 560 1392
            WIRE 560 1168 2224 1168
        END BRANCH
        BEGIN BRANCH XLXN_260
            WIRE 64 1520 64 1680
            WIRE 64 1680 640 1680
            WIRE 64 1520 128 1520
            WIRE 624 560 640 560
            WIRE 640 560 640 1680
        END BRANCH
        BEGIN BRANCH XLXN_270
            WIRE 32 1456 128 1456
            WIRE 32 1456 32 1744
            WIRE 32 1744 112 1744
            WIRE 112 1744 112 2368
            WIRE 112 2368 320 2368
            WIRE 32 1744 32 2320
        END BRANCH
        INSTANCE XLXI_141 128 1648 R0
    END SHEET
END SCHEMATIC
