#ifndef __RUNCONTROL__
#define __RUNCONTROL__
/*

 */

#include <DCThread.h>
#include <xmlHelper.h>

#include <OpBlock.h>
#include <sstream>

#include <NetworkStatus.h>

#include <boost/regex.hpp>

class RunControl : public DCThread 
{
 public:
  RunControl(std::string Name);
  ~RunControl(){};

  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  
  virtual void MainLoop();
 private:
  virtual bool ProcessMessage(DCMessage::Message &message);  
  bool ProcessMessageNormal(DCMessage::Message &message);  
  bool ProcessMessageDuringShutdown(DCMessage::Message &message);  
  void ProcessError(DCMessage::Message &message);
  void ProcessWarning(DCMessage::Message &message);
  void ProcessNetwork(DCMessage::Message &message);
  DCMessage::DCAddress ErrorRoute;
  DCMessage::DCAddress WarningRoute;

  virtual void ProcessTimeout();

  //Run state control
  bool RunSetupComplete;
  void Start();
  void Stop(bool _run_error);
  void SendoutStopMessages();
  void StopRunControl();

  //  void CheckDone(std::vector<OpBlock> & Blocks,DCQueue<OpBlock> & Queue);
  void CheckDone();
  void UpdateShutdownList(DCMessage::Message &message);

  //Safe storage of DAQ setup operations
  std::vector<OpBlock> setupBlocks;
  std::vector<OpBlock> StopBlocks;
  
  //DCQueue of blocks to be processed
  OpBlock currentOp;
  DCQueue<OpBlock> opQueue;
  //  DCQueue<OpBlock> stopBlockQueue;

  //SHutdown control
  //  bool StopOpBlocks;  
  bool inShutdownScript;
  bool shuttingDownStopAddresses;
  struct timeval lastTime;
  std::vector<DCMessage::DCAddress> stopAddresses;
  int threadShutdownCount;
  int maxThreadShutdownCount;
  struct timeval waitTime;
  struct timeval waitTimeBase;

  bool run_error; //if we are shutting down due to an error or not
  bool autostop;
  DCMessage::DCAddress updateRUNLOCK;
};
#endif
