#include <DCLauncher.h>


//Threads

//Control processor
#include <Control.h>
//DCMessageServer
#include <DCMessageServer.h>
//DCMessageClient
#include <DCMessageClient.h>
//Throw Away processor
#include <ThrowAway.h>
//Speed Test processor
#include <SpeedTest.h>
//Raw disk write processor
//#include <RawDiskWrite.h>
//RAT write processor
#include <RATWrite.h>
//V1720 readout processor
#include <v1720Readout.h>
//V1720 readout processor
#include <v1720FakeReadout.h>
//RAT Block write processor
//#include <RATBlockWrite.h>
//RAT Parser
#include <RATParse.h>
//RAT Throw away
#include <RATThrowAway.h>
//TCPMemBlockServer
//#include <TCPMemBlockServer.h>
//TCPMemBlockClient
//#include <TCPMemBlockClient.h>


//=============================================================================
//=============================================================================
uint32_t DCLauncher::SetupThreads(xmlNode * baseNode,
				std::vector<DCThread*> &tempThread,
				std::map<std::string,MemoryManager*> &tempMM
				)
{
  //Check how many mangers we are suppose to set up.
  uint32_t ThreadCount = NumberOfNamedSubNodes(baseNode,"THREAD");
  //Fail if it os over 16.
  if(ThreadCount > 16)
    {
      return(THREAD_TOO_MANY);
    }

  uint32_t ret = 0;
  std::string errName("SetupThreads");
  //Loop over all THREAD nodes
  for(uint32_t node = 0; node < ThreadCount;node++)
    {
      xmlNode* threadNode = FindIthSubNode(baseNode,"THREAD",node);
      if(threadNode) // IF the pointer isn't NULL.
	{
	  
	  //Check if the all the requested memory blocks have been created
	  bool memoryIsAllocated = true;
	  for(unsigned int iMMName = 0; iMMName < NumberOfNamedSubNodes(threadNode,"MEMORYMANAGER");iMMName++)
	    {
	      std::string tempString("");
	      //Get the name of the current MM
	      GetIthXMLValue(threadNode,iMMName,"MEMORYMANAGER",errName.c_str(),tempString);
	      //Search the two memory manager maps for this name
	      std::map<std::string,MemoryManager*>::iterator itTempMM = tempMM.find(tempString);
	      std::map<std::string,MemoryManager*>::iterator itMM = memoryManager.find(tempString);
	      //Check that one of the maps had the MM we were looking for.
	      if(( itMM == memoryManager.end()) &&
		 ( itTempMM== tempMM.end())
		 )
		{
		  ret = THREAD_MISSING_MM;
		  ret |= 0x1<<node;
		  memoryIsAllocated = false;
		  fprintf(stderr,"Error: Memory manager %s not found\n",tempString.c_str());
		  break;
		}
	    }
	  //If the memory managers are loaded for this thread
	  if(memoryIsAllocated)
	    {

	      //Get the name and type for this thread
	      std::string threadName("");
	      std::string threadType("");
	      if(FindSubNode(threadNode,"NAME") == NULL)
		{
		  ret = THREAD_MISSING_NAME;
		  ret |= 0x1<<node;
		  break;
		}
	      else
		GetXMLValue(threadNode,"NAME",errName.c_str(),threadName);
	      if(FindSubNode(threadNode,"TYPE") == NULL)
		{
		  ret = THREAD_MISSING_TYPE;
		  ret |= 0x1<<node;
		  break;
		}
	      else
		GetXMLValue(threadNode,"TYPE",errName.c_str(),threadType);


	      bool KnownThread = true;
	      //Using "TYPE", allocate a new thread
	      //KnownThread will be false if we get to the end of this if-else struct
	      if(threadType == "ThrowAway")
		{
		  tempThread.push_back(new ThrowAway(threadName));
		}
	      else if(threadType == "SpeedTest")
		{
		  tempThread.push_back(new SpeedTest(threadName));
		}
	      else if(threadType == "RATWrite")
		{
		  tempThread.push_back(new RATWrite(threadName));
		}
	      else if(threadType == "RATParse")
		{
		  tempThread.push_back(new RATParse(threadName));
		}
	      else if(threadType == "RATThrowAway")
		{
		  tempThread.push_back(new RATThrowAway(threadName));
		}
	      else if(threadType == "Control")
		{
		  tempThread.push_back(new Control(threadName));
		}
	      else if(threadType == "DCMessageServer")
		{
		  tempThread.push_back(new DCMessageServer(threadName));
		}
	      else if(threadType == "DCMessageClient")
		{
		  tempThread.push_back(new DCMessageClient(threadName));
		}
	      else if(threadType == "V1720Reader")
		{
		  tempThread.push_back(new v1720Readout(threadName));
		}
	      else if(threadType == "V1720FakeReader")
		{
		  tempThread.push_back(new v1720FakeReadout(threadName));
		}	      
//	      else if(threadType == "TCPMemBlockServer")
//		{
//		  tempThread.push_back(new TCPMemBlockServer(threadName));
//		}
//	      else if(threadType == "TCPMemBlockClient")
//		{
//		  tempThread.push_back(new TCPMemBlockClient(threadName));
//		}
	      else
		{
		  KnownThread = false;
		}

	      //Setup the new thread if it exists
	      if(!KnownThread) //thread type unknown
		{
		  printf("Thread node %d unknown: %s\n",node,threadType.c_str());
		  ret = THREAD_UNKNOWN;
		  ret |= 0x1<<node;
		  break;
		}
	      else //Thread type known
		{
		  //To set up a thread we need to send it a memory manger map.
		  //Since we are in the process of setting things up, the 
		  //MMs are in two vectors, ones that have been set up fine
		  //and another with the MMs we have set up in this setup call.
		  //We need to make a temporary map that merges them for
		  //the thread setup function.
		  std::map<std::string,MemoryManager*> tempMemoryMap;
		  //Add the existing memoryManager entries
		  tempMemoryMap.insert(memoryManager.begin(),
				       memoryManager.end());		  
		  //Add the tempMM entries
		  tempMemoryMap.insert(tempMM.begin(),
				       tempMM.end());
		  //Run the thread setup function and check for fail
		  if(!(tempThread.back())->Setup(threadNode,
					     tempMemoryMap))
		    {
		      printf("Bad setup in node %d: %s(%s)\n",
			     node,
			     tempThread.back()->GetName().c_str(),
			     tempThread.back()->GetType().c_str());
		      ret = THREAD_SETUP;
		      ret |= 0x1<<node;
		      break;

		    }
		  else
		    {
		      printf("Setting up node %d: %s(%s)\n",
			     node,
			     tempThread.back()->GetName().c_str(),
			     tempThread.back()->GetType().c_str());
		      tempThread.back()->Start(NULL);
		    }
		}
	    }
	}
      else
	{
	  fprintf(stderr,"Bad thread node # %d\n",node);	 
	  ret = THREAD_BAD_NODE;
	  ret |= 0x1<<node;
	  break;
	}
    }  
  return(ret);
}
