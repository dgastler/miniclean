#ifdef __RATDS__

#ifndef __UNPACKDATA__
#define __UNPACKDATA__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <RATDiskBlockManager.h>
#include <xmlHelper.h>
#include <DetectorInfo.h>
#include <RunInfo.h>
#include <RAT/DS/DCDAQ_DCDAQEvent.hh>

#include <math.h>

#include <RAT/DS/Root.hh>
#include <RAT/DS/EV.hh>

#include <EventMonitor.h>
#include <RATCopyQueue.h>
#include <RAT/DS/DCDAQ_EventStreamHelper.hh>

class UnPackData : public DCThread
{
public:
  UnPackData(std::string Name);
  ~UnPackData();

  bool Setup(xmlNode * SetupNode, std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();


private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  virtual uint32_t * ProcessEvent(uint32_t * data);
  virtual uint32_t * ParseRunStart(uint32_t * data);
 
  //Memory managers
  RATDiskBlockManager * DS_mem;
  RATDSBlock * dsBlock;
  int32_t dsIndex;

  int32_t blockIndex;
  MemoryBlock * block;
  MemoryBlockManager * Mem;
  uint32_t * currentDataPointer;
  uint64_t memBlockCounter;
  //  uint32_t sizeAdded;

  std::vector<uint32_t> boardHeader;
  uint8_t blockCount;

  //Detector information
  DetectorInfo detectorInfo;

  uint32_t runNumber;
  time_t runStartTime;

  //Statistics
  time_t lastTime;
  time_t currentTime;
  uint32_t timeStepProcessedEvents;

  //Monitoring
  EventMonitor eventMonitor;

};

#endif
#endif
