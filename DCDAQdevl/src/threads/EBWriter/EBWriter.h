#ifndef __EBWRITER__
#define __EBWRITER__
/*
  Thread description
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>


#include <TFile.h>
#include <TTree.h>
#include <RAT/DS/Root.hh>

#include <EBPCRatManager.h>

class EBWriter : public DCThread 
{
 public:
  EBWriter(std::string Name);
  ~EBWriter();

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  
  virtual void MainLoop();
 private:

  //==============================================================
  //Basic DCThread things to implement
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  
  virtual void ProcessTimeout();


  //==============================================================
  //Memory manager interface
  //==============================================================
  EBPCRATManager * EBPC_mem;  


  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  uint32_t timeStepProcessedEvents;



  //Config file and variables
  std::string Settings;
  uint32_t EventNumber;
  uint32_t SubEventNumber;
  uint16_t SubRunNumber;
  uint16_t SubRunNumberMax;
  uint32_t SubRunMaxEvents;
  uint32_t MaxRunEvents;
  time_t RunEndTime; //seconds


  //The ROOT output filename and the pointer to the file.
  bool fileReady;
  int  failedFileOpens;
  int  maxFailedFileOpens;
  bool OpenFile();
  bool CloseFile();
  std::string OutFileName;
  TFile * OutFile;
  int Autosave;
  int CompressionLevel;
  int BranchBufferSize;

  //The ROOT tree pointer and the pointer to the current DS::Root data structure
  TTree *Tree;
  RAT::DS::Root * BranchDS;

  std::string ThreadName;
  
    

};
#endif
