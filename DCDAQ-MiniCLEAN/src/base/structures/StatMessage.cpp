#include <StatMessage.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[StatMessage::COUNT] = 
  {
      "BLANK",

      "EVENT_RATE",
      "PACKET_RATE",
      "READOUT_RATE",
      "DATA_RATE",
      "REDUCTION_RATE",
      "BAD_EVENT_RATE",     
      "TRIGGER_RATE",

      "OCCUPANCY",

      "FEPC",
      "DRPC",
      "RAT_DISK",
      "FREE_FULL",
      
      "BASELINE",
      "ZLECOUNT"
  };  

const char * StatMessage::GetTypeName(StatMessage::Type type)
{
  if(type < StatMessage::COUNT)
    return TypeName[type];
  return NULL;
} 

StatMessage::Type StatMessage::GetType(const char * name)
{
  std::string searchString(name);  
  StatMessage::Type ret = StatMessage::Type(0);
  for(StatMessage::Type type = StatMessage::Type(0);
      type < StatMessage::COUNT;
      type = StatMessage::Type(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
