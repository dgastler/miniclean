#include <string>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <sstream>

#include <mysql.h>

#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TStyle.h>

#include "CPUPlots.h"
#include "MemPlots.h"
#include "NetPlots.h"
#include "RatePlots.h"
#include "DiskPlots.h"
#include "WFDPlots.h"

#include "Helper.h"

int main(int argc, char ** argv)
{
  //Get the times for the run number desired.
  int runNumber = -1;
  if(argc > 1)
    {
      runNumber = atoi(argv[1]);
    }

  //Setup SQL connection
  MYSQL * conn = NULL;
  conn =  mysql_init(NULL);
  if(conn == NULL)
    {
      printf("Could not initialize MYSQL structures.\n");
      return -1;
    }
  if(mysql_real_connect(conn, 
			"cleanpc6.bu.edu", 
			"control_user", 
			"27K2boil", 
			"control", 
			3306, NULL, 0)
     == NULL)
    {
      printf("Could not connect to MYSQL server.\n");
      return -1;
    }

  if(runNumber < 0)
    {
      runNumber = GetLastRunNumber(conn);
    }

  time_t start,end;
  GetRunTimes(conn,runNumber,start,end);

  if((start == badTime) || (end == badTime))
    {
      printf("Bad run times %ld %ld\n",start,end);
      return -1;
    }

  MakeRatePlot(conn,runNumber,start,end);
  MakeRatePlotV2(conn,runNumber,start,end);
 
  MakeMemoryPlot(conn,runNumber,start,end);
  
  MakeMemoryPlotV2(conn,runNumber,start,end);
  
  MakeNetworkRatePlot(conn,runNumber,start,end);
  
  MakeCPUPlot(conn,runNumber,start,end);
  
  MakeDiskRatePlot(conn,runNumber,start,end);

  //WFD plots
  MakeReadOutRatePlot(conn,runNumber,start,end);
  MakeDataRatePlot(conn,runNumber,start,end);
  MakeOccupancyPlot(conn,runNumber,start,end);
  MakeEventRatePlot(conn,runNumber,start,end);

  mysql_close(conn);
  return 0;
}
