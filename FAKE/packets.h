#ifndef __PACKET__
#define __PACKET__
#include <stdint.h>
#include <stdio.h>

typedef struct
{  
  unsigned result_Code:2;
  unsigned direction:1; //0 on sent, 1 on reply
  unsigned type:5; // 0x3 read, 0x4 write
  unsigned words:16; //words - 1
  unsigned transaction:4;
  unsigned version:4;
  void Print()
  {
    printf("Header\n");
    printf("  Result:      0x%8X\n",result_Code);
    printf("  Direction:   0x%8X\n",direction);
    printf("  Type:        0x%8X\n",type);
    printf("  Words:       0x%8X\n",words);
    printf("  Transaction: 0x%8X\n",transaction);
    printf("  Version:     0x%8X\n",version);
  };
} Packet_Header;

typedef struct
{
  Packet_Header header;
  uint32_t address;
  uint32_t data;
  void Print()
  {
    printf("Control packet\n");
    header.Print();
    printf("\n");
    printf("  Address:     0x%8X\n",address);
    printf("  Data:        0x%8X\n",data);
  }
} Control_Packet;

typedef struct
{
  Packet_Header header;
  int time:27; //time to next event (signed)
  unsigned parity:1; //100 event parity bit
  unsigned current_Addr_LSB:3;
  unsigned block_Write_Status:1;

  void Print()
  {
    printf("Response packet\n");
    header.Print();
    printf("\n");
    printf("  Time:        0x%8X\n",time);
    printf("  Parity:      0x%8X\n",parity);
    printf("  Addr LSB(3): 0x%8X\n",current_Addr_LSB);
    printf("  blk w stat:  0x%8X\n",block_Write_Status);
  }  
} Response_Packet;

//Everything in the cpacket structs is stored in network byte order
typedef struct
{
  Packet_Header header;

  uint32_t memBlockAddress;//location in the FPGA memory where this 
                           //event will be saved. 
  uint32_t eventTimeStamp; //based on a 1Mhz clock
  uint32_t promptBitTwoPattern[5]; //The MSB patterns for the prompt region
  uint32_t promptBitOnePattern[5];   //The MidSB patterns for the prompt region
  uint32_t bitZeroPattern[245];      //the lsb pattern for 5+240 clock ticks
}  FAKE_Data_Packet1;
typedef struct
{
  Packet_Header header;

  uint32_t memBlockAddress;//location in the FPGA memory where this 
                           //event will be sent. 
  uint32_t bitZeroPattern[256];  //the last 256 lsb patterns for this board
} FAKE_Data_Packet2;

typedef struct
{
  //raw packet structures
  FAKE_Data_Packet1 part1;
  FAKE_Data_Packet2 part2;
} ATLYS_Packet;


#endif
