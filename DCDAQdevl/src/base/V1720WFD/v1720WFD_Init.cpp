#include "v1720WFD.h"
#include "xmlHelper.h"

int v1720::LoadXMLConfig(xmlNode * WFDNode)
{
  int ret = 0;
  //==============================================================================
  //Find the connection xml node.
  //==============================================================================
  if(FindSubNode(WFDNode,"Comm") == NULL)
    {
      ret++;
      fprintf(stderr,"WFD Comm XML lookup failed.\n");
      return(ret);
    }
  //Found the XML Node, passing it to CommInit to setup CONET link.
  ret += CommInit(FindSubNode(WFDNode,"Comm"));
  //If the comm setup is correct
  if(ret == 0)
    {
      //==============================================================================
      //Find the Board xml node.
      //==============================================================================
      if(FindSubNode(WFDNode,"Board") == NULL)
	{
	  ret++;
	  fprintf(stderr,"WFD Board XML lookup failed.\n");
	  return(ret);
	}
      //Found the XML Node, passing it to BoardInit to setup this V1720
      ret += BoardInit(FindSubNode(WFDNode,"Board"));
    }

  printf("\tV1720 Board %u\n",BdNum);

  return(ret);
}


















int v1720::CommInit(xmlNode * commNode)
{
  int ret = 0;
  //==============================================================================
  //Look for this V1720's XML Link ID
  //==============================================================================
  ret += GetXMLValue(commNode,"Link","  Comm",Link);
  if(ret > 0)
    {
      return(ret);
    }

  //==============================================================================
  //Look for this V1720's XML BdNum
  //==============================================================================
  ret += GetXMLValue(commNode,"BdNum","  Comm",BdNum);
  if(ret > 0)
    {
      return(ret);
    }
  
  //==============================================================================
  //Look for this V1720's XML VME address
  //==============================================================================
  ret += GetXMLValue(commNode,"VMEBaseAddress","  Comm",VMEBaseAddress);
  if(ret > 0)
    {
      return(ret);
    }
  //==============================================================================
  //Setup and check the CONET connection
  //==============================================================================
  int CAENret = CAENVME_Init(BdType,Link,BdNum,&Handle);
  if(CAENret != cvSuccess)
    {
      fprintf(stderr,"%s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  ret+=CAENret;
  return(ret);
}

















int v1720::BoardInit(xmlNode *boardNode)
{
  int ret = 0;
  int CAENret = 0;
  uint32_t RegisterReturn;
  RegisterReturn = 0;


  //==============================================================================
  //Apply a software reset to the V1720
  //==============================================================================
  CAENret = a32d32w(VMEBaseAddress+0xEF24,0x0);
  
  
  //==============================================================================
  //Check that there is a valid board at this location.
  //==============================================================================
  //read the ROCFPGA date/version 
  CAENret = a32d32r(VMEBaseAddress+0x8124,RegisterReturn);
  if(CAENret == 0)
    {
      xmlNode * newNode = AddNewSubNode(boardNode,"ROCFPGA");
      char * buffer = new char[100];
      sprintf(buffer,"0x%08X",RegisterReturn);     
      printf("\t\tROCFPGA firmware: %02u.%02u from: %01u/%01u/%02x\n",
	     (RegisterReturn >> 8 )&0xFF,
	     (RegisterReturn >> 0 )&0xFF,
	     (RegisterReturn >> 28)&0xF,
	     (RegisterReturn >> 24 )&0xF,
	     (RegisterReturn >> 16 )&0xFF
	     );
      xmlNodeSetContent(newNode,(xmlChar*)buffer);
    }
  ret+=CAENret;
  if(ret > 0)
    {
      return(ret);
    }


  //==============================================================================
  //Look for the BoardID setting.
  //==============================================================================
  //Check if the value exists in the XML
  xmlNode * boardID_Node = FindSubNode(boardNode,"BoardID");
  if(boardID_Node != NULL)
    //Value exists
    {
      ret += GetXMLValue(boardNode,"BoardID","  Board",BoardID);
      //Write value and then do a read-back check
      ret += SetRegister(VMEBaseAddress + 0xEF08,BoardID,"BoardID");
      if(ret > 0)
	  return(ret);
    }  
  else
    {
      //Get the value from the board (GEO for VME64X, something random from any other board)
      CAENret = a32d32r(VMEBaseAddress+0xEF08,BoardID);
      if(CAENret > 0)
	return(CAENret);
    }
  
  //==============================================================================
  //Look for the VMEControl settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"VMEControl","  Board",VMEControl);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check.
  ret += SetRegister(VMEBaseAddress+ 0xEF00,VMEControl,"VMEControl");


  //==============================================================================
  //Look for the ChannelsConfig settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"ChannelsConfig","  Board",ChannelsConfig);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8000,ChannelsConfig,"ChannelsConfig");


  //==============================================================================
  //Look for the MemConfig settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"MemConfig","  Board",MemConfig);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x800C,MemConfig,"MemConfig");



  //==============================================================================
  //Look for the PostSamples settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"PostSamples","  Board",PostSamples);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8114,PostSamples,"PostSamples");

  //==============================================================================
  //Look for the TriggerSource settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"TriggerSource","  Board",TriggerSource);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x810C,TriggerSource,"TriggerSource");


  //==============================================================================
  //Look for the Acquisition control  settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"AcquisitionControl","  Board",AcquisitionControl);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8100,AcquisitionControl,"AcquisitionControl");

  //==============================================================================
  //Look for the Monitor Mode  settings.
  //==============================================================================
  //Get value from XML
  ret += GetXMLValue(boardNode,"MonitorMode","  Board",MonitorMode);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x8144,MonitorMode,"MonitorMode");

  

  //==============================================================================
  //Setup each V1720 channel
  //==============================================================================
  int numberOfChannels = NumberOfNamedSubNodes(boardNode,"Channel");
  for(int ichan = 0; ichan < numberOfChannels;ichan++)
    {
      //==========================================================================
      //Find the ichanth Channel xml node.
      //==========================================================================
      if(FindIthSubNode(boardNode,"Channel",ichan) == NULL)
	{
	  ret++;
	  fprintf(stderr,"    Channel  XML lookup failed.\n");
	  return(ret);
	}
      //Found the XML Node, passing it to BoardInit to setup this V1720
      ret += ChannelInit(FindIthSubNode(boardNode,"Channel",ichan));
    }

  //==============================================================================
  //Setup channel mask
  //==============================================================================
  //Calculate the channel mask;
  ChannelMask = GetChannelMask();
  //write channel mask to WFD
  CAENret = SetRegister(VMEBaseAddress + 0x8120,ChannelMask,"ChannelMask");
  //Add channel mask to the output XML string.
  if(CAENret == 0)
    {
      xmlNode * newNode = AddNewSubNode(boardNode,"ChannelMask");
      char * buffer = new char[100];
      sprintf(buffer,"0x%08X",ChannelMask);
      xmlNodeSetContent(newNode,(xmlChar*)buffer);
    }
  ret+=CAENret;  


  //==============================================================================
  //Use this information to setup EventSize.
  //Event size in bytes.
  //==============================================================================
  //           ( # channels      *Maximum Size)                      Header size
  EventSize = ((numberOfChannels * 1024 * 1024) >> (MemConfig + 1)) + 0x4; //32bit words
  EventSize = EventSize << 2; //Convert 32bit words to bytes
  
  //==============================================================================
  //Check if everything has been setup correctly.
  //==============================================================================
  if(ret == 0)
    {
      Ready = true;
    }
  else
    {
      Ready = false;
    }
  return(ret);
}


















int v1720::ChannelInit(xmlNode *channelNode)
{
  int ret = 0;
  //  v1720channel * Channel = &(Channels[Channels.size()-1]);
  v1720channel channel;
  int channelOffset = 0x0;
  uint32_t registerReturn = 0;

  //==============================================================================
  //Setup this channel's channel ID
  //==============================================================================  
  ret += GetXMLValue(channelNode,"ChannelID","    Channel",channel.ChannelID);
  if(ret > 0)
    {
      return(ret);
    }
  //Using channel ID set the channel specific VME offset.
  channelOffset = 0x100*channel.ChannelID;

  //==============================================================================
  //Setup this channel's PMT ID
  //==============================================================================  
  ret += GetXMLValue(channelNode,"PMTID","    Channel",channel.PMTID);
  if(ret > 0)
    {
      return(ret);
    }


  //==============================================================================
  //Setup this channel's ZS_THRESH
  //==============================================================================  
  ret += GetXMLValue(channelNode,"ZS_THRESH","    Channel",channel.ZS_THRESH);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1024 + channelOffset,channel.ZS_THRESH,"channel.ZS_THRESH");

  //==============================================================================
  //Setup this channel's ZS_NSAMP
  //==============================================================================  
  ret += GetXMLValue(channelNode,"ZS_NSAMP","    Channel",channel.ZS_NSAMP);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1028 + channelOffset,channel.ZS_NSAMP,"channel.ZS_NSAMP");
  
  //==============================================================================
  //Setup this channel's Threshold
  //==============================================================================  
  ret += GetXMLValue(channelNode,"Threshold","    Channel",channel.Threshold);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1080 + channelOffset,channel.Threshold,"channel.Threshold");

  //==============================================================================
  //Setup this channel's NSamples
  //==============================================================================  
  ret += GetXMLValue(channelNode,"NSamples","    Channel",channel.NSamples);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1084 + channelOffset,channel.NSamples,"channel.NSamples");

  //==============================================================================
  //Setup this channel's DAC
  //==============================================================================  
  ret += GetXMLValue(channelNode,"DAC","    Channel",channel.DAC);
  if(ret > 0)
    {
      return(ret);
    }
  //Write value and then do a read-back check
  ret += SetRegister(VMEBaseAddress + 0x1098 + channelOffset,channel.DAC,"channel.DAC");

  //==============================================================================
  //Read the AMCFPGA version/date
  //==============================================================================  
  int  CAENret = a32d32r(VMEBaseAddress+0x108c +channelOffset,registerReturn);
  if(CAENret == 0)
    {
      xmlNode * newNode = AddNewSubNode(channelNode,"AMCFPGA");
      char * buffer = new char[100];
      sprintf(buffer,"0x%08X",registerReturn);
      printf("\t\t\tAMCFPGA chan %d firmware: %02u.%02u from: %01u/%0u/%02x\n",
	     channelOffset,
	     (registerReturn >> 8 )&0xFF,
	     (registerReturn >> 0 )&0xFF,
	     (registerReturn >> 28)&0xF,
	     (registerReturn >> 24 )&0xF,
	     (registerReturn >> 16 )&0xFF
	     );
      xmlNodeSetContent(newNode,(xmlChar*)buffer);
    }
  ret+=CAENret;
  
  Channels.push_back(channel);

  return(ret);
}

uint32_t v1720::GetChannelMask()
{
  //Initial mask is all channels off
  uint32_t channelMask = 0x0;
  unsigned int numberOfChannels = Channels.size();
  for(unsigned int iChan = 0;iChan<numberOfChannels;iChan++)
    {
      //Add this channel to the channel mask.
      //Enable channel by setting it's bit (7-0) to one.
      channelMask += (0x1 << Channels[iChan].ChannelID);
    }
  return(channelMask);
}
