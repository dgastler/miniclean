#include "WriteTime.h"

int writeT(int fd,uint8_t* ptr,uint32_t size,int nTimeouts)
{
  timeval timeV;
  
  int nTime=0;
  
  int nret;

  while((nret=writeN(fd,ptr,size,timeV))<0 && nTime<nTimeouts)
		{
		  printf("error in writeT: %s",strerror(errno));
		  if(errno==EAGAIN)
		    {
		      nTime+=1;
		      usleep(1000);
		      printf("nTime %i, nTimeouts %i",nTime,nTimeouts);
		    }
		  else
		    return nret;
		}

  return nret;

}
