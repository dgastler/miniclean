<?php

class DAQ_UI
{
  protected $conn;
  
  public function __construct()
  {
    $this->DbConnect();
  }

  //Establish a mysql connection
  protected function DbConnect()
  {
    include "db_config.php";
    $this->conn = mysql_connect($host,$user,$password) OR die("Unable to connect to the database");
    mysql_select_db($db,$this->conn) OR die("can not select the database $db");
    return TRUE;
  }

  //Check the current run status
  public function getStatus() {
    $sql = "SELECT * FROM RUNLOCK";
    $res = mysql_query($sql,$this->conn) OR die ("Query did not run");
    $row = mysql_fetch_array($res);
    $status[] = (string)$row['access'] . ":" . (string)$row['run_number'];
    return $status;
  }

  //==============================
  //Functions for uploading a run
  //==============================

  //Upload all the PCs for a given run
    public function getPCs($runNumber)
  {
    $sql = "SELECT * FROM NetworkConnections WHERE run_number=" . $runNumber . " ORDER BY pc_name desc";
    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");

    while($row = mysql_fetch_array($res))
      {
	$pcNames = (string)$row['pc_name'];
	$IP=(string)$row['ip'];
	$Port=(string)$row['port'];
	$pc[]=$pcNames . ":" . $IP . ":" . $Port;
      }
    return $pc;
  }

    //Public function to get all of the IP addresses for each PC
    public function getIPs($runNumber)
    {
      $sql = "SELECT DISTINCT * FROM NetworkConnections WHERE run_number=" . $runNumber;
      $res = mysql_query($sql,$this->conn) OR die("Could not get IPs.");

      while($row = mysql_fetch_array($res)) {
	$ip1 = (string)$row['ip'];
	$ip2 = (string)$row['pc_name'];
	$ips[] = $ip1 . ":" . $ip2;
      }

      return $ips;
    }
    
  //Upload all the threads for a given run and pc name
  public function getPCThreads($pcName,$runNumber)
  {
    $sql = "SELECT * FROM Thread_Threads WHERE run_number=" . $runNumber . " AND pc_name='" . $pcName . "' ORDER BY order_id";
    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");
    while($row = mysql_fetch_array($res))
      {
	$threadTypes = (string)$row['thread_type'];
	$threadNames = (string)$row['thread_name'];
	$thread[] = $threadTypes . ":" . $threadNames;
      }
    return $thread;
  }

  //Upload all the memory managers for a given run and pc name
  public function getPCMMs($pcName,$runNumber)
  {
    $sql = "SELECT * FROM Memory_MemoryManagers WHERE run_number=" . $runNumber . " AND pc_name='" . $pcName . "' ORDER BY order_id";
    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");
    while($row = mysql_fetch_array($res))
      {
	$mmTypes = (string)$row['mm_type'];
	$mmNames = (string)$row['mm_name'];
	$mm[] = $mmTypes . ":" . $mmNames;
      }
    return $mm;
  }

  //Upload all the thread templates
  public function getThreadTemplate($table)
  {
    //get the first row from the table; these are all the thread names
    $threadsql = "SELECT thread_type FROM " . $table . "'";
    $threadres = mysql_query($threadsql,$this->conn) OR die("Query did not run.");
    $num_rows = mysql_num_rows($threadres);
    $threadrow = mysql_fetch_array($threadres);
    
    for ($i=0;$i<$num_rows;$i++) {
      $threadType = (string)$threadrow[$i];

    $sql = "SELECT * FROM " . $table . + " WHERE thread_type='" . $threadType . "'";
    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");
    $num_fields=mysql_num_fields($res);
    $row=mysql_fetch_array($res);
    for($j=0;$j<$num_fields;$j++)
      {
	if(strcmp((string)$row[$j],$threadType)!=0)
	$threadTemplate[$i][$j] = (string)$row[$j];
      }    
    return $threadTemplate;
  }
  }

  //Get the string of thread types from a given table
  public function getThreadType($table)
  {
    if ($table == 'Thread_Threads') {
      $sql = "SELECT DISTINCT thread_type FROM " . $table;
    } else {
      $sql = "SELECT DISTINCT mm_type FROM " . $table;
    }
    $res = mysql_query($sql,$this->conn) OR die("Query did not run");
    $num_rows = mysql_num_rows($res);
    $thread_type = array();
    
    while ($r = mysql_fetch_array($res))
      {
	if ($table == 'Thread_Threads') {
	  $thread_type[] = $r["thread_type"];
	} else {
	  $thread_type[] = $r["mm_type"];
	}
      }

    /*
    for ($i=0; $i<$num_rows; $i++) 
      {
	$thread_type[$i] = (string)$row[$i];
      }
    */
    return $thread_type;
  }
    

  //Get the data for a given thread
  public function getData($pcName,$type,$table,$runNumber)
  {
    //$sql = "SHOW COLUMNS FROM Thread_Threads";
    //$res = mysql_query($sql,$this->conn) OR die("Query did not run.");    
    $index=0;
    $startIndex=0;

    if ($table == 'Thread_Threads') {
    $sql = "SELECT * FROM " . $table . " WHERE thread_type='" . $type . "' AND pc_name='" . $pcName . "' AND run_number='" . $runNumber . "'";
    } else if (($table == 'Thread_Templates') || ($table == 'Thread_Defaults') || ($table == 'Thread_Necessary')) {
      $sql = "SELECT * FROM " . $table . " WHERE thread_type='" . $type . "'";
    } else if ($table == 'Memory_MemoryManagers') {
      $sql = "SELECT * FROM " . $table . " WHERE mm_type='" . $type . "' AND pc_name='" . $pcName . "' AND run_number='" . $runNumber . "'";
    } else if (($table == 'Memory_Templates') || ($table == 'Memory_Defaults') || ($table == 'Memory_Necessary')) {
      $sql = "SELECT * FROM " . $table . " WHERE mm_type='" . $type . "'";
    } else if (($table == 'SThread_Data_Compressed') ||($table=='SThread_Data_Raw')) {
      $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $runNumber . " AND pc_name='" . $pcName . "' AND thread_name='" . $type . "'";
    } else if ($table == 'SThread_DecisionMaker') {
      $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $runNumber . " AND thread_name='" . $type . "'";
      } else {
	$sql = "SELECT * FROM " . $table . " WHERE thread_type='" . $type . "' AND pc_name='" . $pcName . "' AND run_number=" . $runNumber;
      }

    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");
    $num_fields=mysql_num_fields($res);
    $row=mysql_fetch_array($res);
    for($i=$startIndex;$i<($num_fields);$i++)
      {
	$threadData[$i] = (string)$row[$i];
      }

    return $threadData;
  }

  
  public function processData($pcName,$nameID,$table,$data,$dataindex)
  {
    if (($table=='Thread_Threads')) {
      $sql = "UPDATE " . $table . " SET data_0" . $dataindex . "='" . $data . "' WHERE thread_name REGEXP '" . $nameID . "' AND pc_name REGEXP '" . $pcName . "' AND run_number=0";
    } else if(($table=='Settings_WFDs')) {
      $sql = "UPDATE " . $table . " SET " . $dataindex . "='" . $data . "' WHERE run_number=0 AND BoardID REGEXP " . $nameID;
    } else if ($table=='Settings_PMTs') {
      $sql = "UPDATE " . $table . " SET " . $dataindex . "='" . $data . "' WHERE wfd_id REGEXP " . $nameID . " AND wfd_pos REGEXP " . $pcName . " AND run_number=0";
    } else if ($table=='SThread_Data_Compressed') {
      $sql = "UPDATE " . $table . " SET Compressed='" . $data . "' WHERE pc_name REGEXP '" . $pcName . "' AND thread_name REGEXP '" . $nameID . "'";
    } else if ($table=='SThread_Data_Raw') {
      $sql = "UPDATE " . $table . " SET Raw='" . $data . "' WHERE pc_name REGEXP '" . $pcName . "' AND thread_name REGEXP '" . $nameID . "'";
    } else if ($table=='SThread_DecisionMaker') {
      $sql = "UPDATE " . $table . " SET " . $dataindex . "='" . $data . "' WHERE run_number=0 AND thread_name REGEXP '" . $nameID . "'";
    } else {
      $sql = "UPDATE " . $table . " SET data_0" . $dataindex . "='" . $data . "' WHERE mm_name REGEXP '" . $nameID . "' AND pc_name REGEXP '" . $pcName . "' AND run_number=0";
    }
    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");
      return $sql;
  }

  public function getColumnNames($pcName,$type,$table,$runNumber)
  {
    if (($table == 'SThread_Data_Compressed') || ($table == 'SThread_Data_Raw')) {
      $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $runNumber . " AND pc_name='" . $pcName . "' AND thread_name='" . $type . "'";
    } else if ($table == 'Settings_WFDs') {
      $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $runNumber . " AND BoardID=" . $type;
    } else if ($table == 'Settings_PMTs') {
      $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $runNumber . " AND wfd_id=" . $type;
    } else {
      $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $runNumber . " AND thread_name='" . $type . "'";
    }
    
    $res = mysql_query($sql,$this->conn) OR die("Query did not run.");
    $num_fields = mysql_num_fields($res);
    for ($i=0; $i<$num_fields; $i++) {
      $column_names[] = mysql_field_name($res,$i);
    }
    return $column_names;

  }


  //Get all of the run numbers
  public function getRunNumbers($table)
  {
    $runLookup =''; // added to remove errors... 2014-05-01
    if ($table == 'RunNames') {
      $sql = "SELECT DISTINCT * FROM $table ORDER BY run_number DESC";
      $res = mysql_query($sql,$this->conn) OR die("Could not get run names!");
      
      while($row = mysql_fetch_array($res))
	{
	  $runNumbers = (string)$row['run_number'];
	  $runNames = (string)$row['run_name'];
	  $runLookup[] = $runNumbers . ":" . $runNames;
	}
    } else {
      $sql = "SELECT DISTINCT run_number from $table WHERE run_number > 0 ORDER BY run_number ASC";
      $res = mysql_query($sql,$this->conn) OR die("Could not get run numbers!");
      
      while($row = mysql_fetch_array($res)) {
	$runNumbers = (string)$row['run_number'];
	$runLookup[] = $runNumbers;
      }
    }
      return $runLookup;
    }


  //================================
  //Function to stop a current run
  //================================
  public function stopRun() {
    $sql = "UPDATE RUNLOCK SET access='STOP'";
    $err = "Could not stop run!";
    $res = mysql_query($sql,$this->conn) OR die($err);
    return $res;
  }

  //===============================
  //Functions for updating settings
  //===============================
  public function getWFDs($run_number) {
    $sql = "SELECT BoardID FROM Settings_WFDs WHERE run_number=" . $run_number . " ORDER BY BoardID asc";
    $res = mysql_query($sql,$this->conn) OR die("Query did not run");

    while ($r = mysql_fetch_array($res))
      {
	$wfds[] = $r["BoardID"];
      } 

    return $wfds;
  }

  public function getWFDSettings($run_number, $boardID, $table) {
     $sql = "SELECT * FROM " . $table . " WHERE run_number=" . $run_number . " AND BoardID=" . $boardID;
 
    $res = mysql_query($sql, $this->conn) OR die("Query did not run");
    return mysql_fetch_array($res);
  }

  public function getPMTs($run_number, $boardID) {
    $sql="SELECT wfd_pos FROM Settings_PMTs WHERE run_number=" . $run_number . " AND wfd_id=" . $boardID . " ORDER BY wfd_pos asc";
    $res=mysql_query($sql,$this->conn) OR die("Query did not run");

    while ($r = mysql_fetch_array($res))
      {
	$pmts[] = $r["wfd_pos"];
      }
    return $pmts;
  }

  public function getPMTSettings($run_number,$boardID,$channel) {
    $sql = "SELECT * FROM Settings_PMTs WHERE run_number=" . $run_number . " AND wfd_pos=" . $channel . " AND wfd_id=" . $boardID;

    $res = mysql_query($sql,$this->conn) OR die("Query did not run");
    return mysql_fetch_array($res);
  }

  //=====================================================
  //Function to update RUNLOCK table when run is starting
  //=====================================================
  public function startRun() {
    $sql = "UPDATE RUNLOCK SET access='NEW_RUN'";
    $err = "Could not update table!";
    $res = mysql_query($sql,$this->conn) OR die($err);
    return $res;
  }


  //============================
  //Functions to update run 0
  //============================

  //Make a new PC
  //DELETE THIS???
  public function createPC($pcName,$IP,$Port)
  {
    $runTorun=-99999;
    
      
    $sql = "INSERT INTO NetworkConnections(run_number,pc_name,ip,port) VALUES (" . $runTorun . ",'" . $pcName . "','" . $IP . "'," . $Port . ")";
    $err ="Query did not run uhoh. run " . $runTorun . "";
    $res = mysql_query($sql,$this->conn) OR die($err);
    
  }

  //==========================================================
  //Function to clear out whatever was left over in run zero.
  //==========================================================

  
  public function clearRunZero()
  {

$table_array = array();
$table_array[0] = "Memory_MemoryManagers";
$table_array[1] = "Messages_BeforeThreads";
$table_array[2] = "Messages_End";
$table_array[3] = "Messages_Shutdown";
$table_array[4] = "Messages_Start";
$table_array[5] = "NetworkConnections";
$table_array[6] = "RunNames";
$table_array[7] = "SThread_Data_Compressed";
$table_array[8] = "SThread_Data_Raw";
$table_array[9] = "SThread_DecisionMaker";
$table_array[10] = "Settings_DataReduction";
$table_array[11] = "Settings_PMTs";
$table_array[12] = "Thread_Threads";
$table_array[13] = "Settings_WFDs";

$length = count($table_array);

for($i=0 ; $i<$length ; $i++)
      {
	$table = $table_array[$i];
	$sql = "DELETE FROM $table WHERE run_number = 0";
	$res = mysql_query($sql,$this->conn) OR die("Query did not run - could not clear zero!");
      }

	return $res;
  }


//=========================================================
// Function for copying a run (into run 0)
//=========================================================

public function copyRun($run_number) {  

  $run_from = $run_number;
  //  $run_from=-5;
  $run_to = 0;
  
  //Making an array of the tables to touch
$table_array = array();
$table_array[0] = "Memory_MemoryManagers";
$table_array[1] = "Messages_BeforeThreads";
$table_array[2] = "Messages_End";
$table_array[3] = "Messages_Shutdown";
$table_array[4] = "Messages_Start";
$table_array[5] = "NetworkConnections";
$table_array[6] = "RunNames";
$table_array[7] = "SThread_Data_Compressed";
$table_array[8] = "SThread_Data_Raw";
$table_array[9] = "SThread_DecisionMaker";
$table_array[10] = "Settings_DataReduction";
$table_array[11] = "Settings_PMTs";
$table_array[12] = "Thread_Threads";
$table_array[13] = "Settings_WFDs";

$length = count($table_array);

//Go through each table to copy the data to its new location
for ($i = 0; $i < $length; $i++) {
  //Get the name of the current table
  $table = $table_array[$i];

  //Now make a temporary table to copy the row of the run number want to copy from
  $temp_query = "CREATE TEMPORARY TABLE temp_$i SELECT * FROM $table WHERE run_number = $run_from";
  $res1 = mysql_query($temp_query,$this->conn) OR die("Query did not run - could not copy!");
  //Change the run_number field to the number you want to make a copy into 
  $num_change = "UPDATE temp_$i SET run_number = $run_to";
  $res2 = mysql_query($num_change,$this->conn) OR die("Query did not run - could not copy!");
  //Insert the new line (with the changed run_number) back into the original table
  $insert_query = "INSERT INTO $table SELECT * FROM temp_$i WHERE run_number=0";
  $res3 = mysql_query($insert_query,$this->conn) OR die("Query did not run - could not copy!");
}

return $res1;

}

//========================================
//Copying an individual thread/mm/channel
//========================================
public function copyIndividualRow($table,$runFrom,$type,$name,$pc) {
  //Copy the run into a temporary table from the selected run
  if ($table == 'Thread_Threads') {
    //$make_temp = "CREATE TEMPORARY TABLE temp SELECT * from Thread_Defaults WHERE thread_type='$type'";
    $make_temp = "SELECT * FROM Thread_Defaults WHERE thread_type='$type'";
  } else {
    //$make_temp = "CREATE TEMPORARY TABLE temp SELECT * from $table Memory_Defaults WHERE mm_type= '$type'";
    $make_temp = "SELECT * FROM Memory_Defaults WHERE mm_type='$type'";
  }

  $res1 = mysql_query($make_temp,$this->conn) OR die("Query did not run - could not copy!");

  return mysql_fetch_array($res1);
}

public function checkClash($table,$pc,$name) {
  //Query the db for that name
  if ($table == 'Thread_Threads') {
    $checkquery = "SELECT * FROM Thread_Threads WHERE run_number=0 AND pc_name='$pc' AND thread_name='$name'";
  } else if ($table == 'Memory_MemoryManagers') {
    $checkquery = "SELECT * FROM Memory_MemoryManagers WHERE run_number=0 and pc_name='$pc' AND mm_name='$name'";
  } else if ($table == 'Settings_PMTs') {
    $checkquery = "SELECT * FROM Settings_PMTs WHERE run_number=0 AND wfd_id=$pc AND wfd_pos=$name";
  } else {
    $checkquery = "SELECT * FROM Settings_WFDs WHERE run_number=0 and BoardID=$name";
  }
  $res = mysql_query($checkquery,$this->conn) OR die("Query did not run - invalid syntax");
  if (count(mysql_fetch_row($res)) <= 1) {
    return (1);
  } else {
    return (0);
  }
}

public function addCopyRow($table,$new_table) {

  if ($table=='Thread_Threads') {
    $sql = "INSERT INTO $table(run_number,order_id,thread_name,thread_type,pc_name) VALUES($new_table[0],'$new_table[1]','$new_table[2]','$new_table[3]','$new_table[4]')";
  } else {
    $sql = "INSERT INTO $table(run_number,order_id,mm_name,mm_type,pc_name) VALUES($new_table[0],'$new_table[1]','$new_table[2]','$new_table[3]','$new_table[4]')";
  }
  $res = mysql_query($sql,$this->conn) OR die("Could not copy!");
  
  for ($i=1; $i< count($new_table)-4; $i++) {
    $datai = $i+4;
    if ($i < 10) {
      $i = "0$i";
    }
    if ($new_table[$datai] == '') {
      $new_table[$datai]='NULL';
    }
    if ($table=='Thread_Threads') {
      $sqli = "UPDATE $table SET data_$i = '$new_table[$datai]' WHERE run_number=0 AND thread_name='$new_table[2]' AND thread_type='$new_table[3]' AND pc_name='$new_table[4]'";
    } else {
      $sqli = "UPDATE $table SET data_$i = '$new_table[$datai]' WHERE run_number=0 AND mm_name='$new_table[2]' AND mm_type='$new_table[3]' AND pc_name='$new_table[4]'";
    }
    echo $sqli;
    $resi = mysql_query($sqli,$this->conn) OR die("Could not copy data!");
  }
  return $resi;
}

//End pcBox class
}

//instantiating a pcbox object
$opt = new DAQ_UI();
 

?>