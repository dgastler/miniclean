// crate_lib.h
// VME Crate I/O class definition
// Dan Gastler 07-20-2005

#if !defined(VME_CRATE_LIB)
#define VME_CRATE_LIB

#include <sys/types.h>


//----------------------------------------------------------------------------
// Struck C librarys
//---------------------------------------------------------------------------
extern "C" 
{
  //#include "/home/clean/V2.02/dev/pci/sis1100_var.h"
  //#include "/home/clean/V2.02/sis3100_calls/sis3100_vme_calls.h"
  #include <dev/pci/sis1100_var.h>
  #include <sis3100_calls/sis3100_vme_calls.h>
}  
//-----------------------------------------------------------------------------

//#define cratedev  "/tmp/sis1100_00remote"
#define cratedev "/dev/sis1100"
#define crcsr 0x2f
#define a32d32 0x9
#define a32mblt64 0x8


class VME_Crate 
{
 public:
  VME_Crate();
  ~VME_Crate();
  int reset();
  
  int controlread(int offset,u_int32_t* data);
  int controlwrite(int offset, u_int32_t data);

  int Iack(u_int32_t irq, u_int8_t * data);

  int geowrite(int slot, int offset, u_int8_t value);
  int georead(int slot, int offset, u_int8_t* value);

  int a32d32write(u_int32_t addr, u_int32_t value);
  int a32d32read(u_int32_t addr, u_int32_t* value);

  int a32d16write(u_int32_t addr, u_int16_t value);
  int a32d16read(u_int32_t addr, u_int16_t* value);

  int a32mblt64write(u_int32_t addr, u_int32_t* buffer, u_int32_t count);
  int a32mblt64read(u_int32_t addr, u_int32_t* buffer, u_int32_t count);

  int getcratefd() {return(cratefd);}
 private:
  int cratefd;
  
};

#endif
