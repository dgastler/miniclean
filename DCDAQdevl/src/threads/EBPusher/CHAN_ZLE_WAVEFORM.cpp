#include <CHAN_ZLE_WAVEFORM.h>

int CHAN_ZLE_WAVEFORM::AddChannelHeader(RAT::DS::PMT * pmt)
{
  ebChannel = (EBPacket::ChannelData*) currentPtr;
  ebChannel->ID = pmt->GetID();
  ebChannel->dataBlockCount = pmt->GetRaw()->GetRawWaveformCount();
  //move forward in our array
  currentPtr += sizeof(EBPacket::ChannelData);
  dataSizeLeft -= sizeof(EBPacket::ChannelData);
  return 0;
}

uint32_t CHAN_ZLE_WAVEFORM::CalculatePMTSize(RAT::DS::Raw * raw)
{
  //Calculate how much space we need for this data
  int rawWaveformCount = raw->GetRawWaveformCount();
  //This is the ChannelDATA structure size plus the
  //sum of all the WaveformBlock structures for this PMT
  uint32_t channelSize = (sizeof(EBPacket::ChannelData) + 
			  (rawWaveformCount*sizeof(EBPacket::WaveformBlock)));          
  //Calculate how big the waveforms are and add it to the channelSize
  for(int iRawWaveform = 0; iRawWaveform < rawWaveformCount;iRawWaveform++)
    {
      RAT::DS::RawWaveform * rawWaveform = raw->GetRawWaveform(iRawWaveform);
      channelSize += rawWaveform->GetSamples().size()*sizeof(uint16_t);
    }
  return channelSize ;
}

int CHAN_ZLE_WAVEFORM::AddData(RAT::DS::Raw* raw)
{
  //Calculate how much space we need for this data
  int rawWaveformCount = raw->GetRawWaveformCount();  
  //Add the raw waveforms
  for(int iRawWaveform = 0;iRawWaveform < rawWaveformCount;iRawWaveform++)
    {
      //Get the the current RAT rawWaveform header
      RAT::DS::RawWaveform * rawWaveform = raw->GetRawWaveform(iRawWaveform);
      //Get a pointer to a EBPacket RawWaveform type that points to the next
      //available spot in the packet array
      EBPacket::WaveformBlock * ebWaveformBlock = (EBPacket::WaveformBlock *) currentPtr;

      //Set data
      ebWaveformBlock->startTime = rawWaveform->GetTimeOffset();	  
      ebWaveformBlock->dataBytes = rawWaveform->GetSamples().size()*sizeof(uint16_t);
      
      //move forward in our array
      currentPtr += sizeof(EBPacket::WaveformBlock); 
      //update the free space available. 
      dataSizeLeft -= sizeof(EBPacket::WaveformBlock); 
	  
      //Copy the waveform data to the packet
      memcpy((uint8_t *)currentPtr,
	     (uint8_t *)(&(rawWaveform->GetSamples()[0])),
	     ebWaveformBlock->dataBytes);
	  
      //move forward in our array
      currentPtr += ebWaveformBlock->dataBytes; 
      //update the free space available. 	 
      dataSizeLeft -= ebWaveformBlock->dataBytes; 
    }
  return 0;
}
