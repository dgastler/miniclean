#ifdef __RATDS__
#include <DREBBridge.h>

DREBBridge::DREBBridge(std::string Name)
{
  SetType("DREBBridge");
  SetName(Name);
  //Set initial value for statistics updates
  lastTime = time(NULL);
  currentTime = lastTime;
  bridgeCount = 0;
  Ready = false;
  DRPCmem = NULL;
  RATDiskmem = NULL;
}

bool DREBBridge::Setup(xmlNode * SetupNode,
			 std::vector<MemoryManager*> &MemManager)
{  
  bool ret = true;
  // Find and load the DRPC memory interface
  if(!FindMemoryManager(DRPCmem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!DRPCmem)
    {
      PrintError("Missing DRPC RAT memory manager\n.");
      ret = false;
    }
  // Find and load the RATDisk memory interface
  if(!FindMemoryManager(RATDiskmem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!RATDiskmem)
    {
      PrintError("Missing RATDisk memory manager\n.");
      ret = false;
    }
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}



void DREBBridge::MainLoop()
{
  if(IsReadReady(DRPCmem->GetEBPCFDs()[0]))  
    {
      //Get a free RATDiskMem
      int32_t indexRatDisk = RATDiskmem->GetBadValue();
      if(RATDiskmem->GetFree(indexRatDisk))
	{
	  //Get bridge event
	  int32_t indexDRPC = DRPCmem->GetBadValue();
	  if(!DRPCmem->GetEBPC(indexDRPC))
	    {
	      //	      DCMessage::Message message;
	      SendStop();
	    }
	  RATEVBlock * DRPCBlock = DRPCmem->GetDRPCBlock(indexDRPC);
	  RATDSBlock * EBPCBlock = RATDiskmem->GetRATDiskBlock(indexRatDisk);

	  //copy
	  EBPCBlock->eventID = DRPCBlock->eventID;
	  EBPCBlock->eventTime = DRPCBlock->eventTime;
	  EBPCBlock->eventTriggers = DRPCBlock->eventTriggers;

	  *(EBPCBlock->ds->GetEV(0)) = *(DRPCBlock->ev);

	  EBPCBlock->reductionLevel = DRPCBlock->reductionLevel;
	  
	  //insert into the disk queue
	  DRPCmem->AddFree(indexDRPC);
	  RATDiskmem->AddToDisk(indexRatDisk);
	  bridgeCount++;
	}
    }
}

void DREBBridge::ProcessTimeout()
{
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      bridgeCount = 0;

      //record time
      lastTime = currentTime;
    }
}

bool DREBBridge::ProcessMessage(DCMessage::Message & message)
{
  if(message.GetType() == DCMessage::GO)
    {
      if(Ready)
	{
	  AddReadFD(DRPCmem->GetEBPCFDs()[0]);
	  SetupSelect();
	  Loop = true;
	}
      return(true);
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      
      RemoveReadFD(DRPCmem->GetEBPCFDs()[0]);
      SetupSelect();
      Loop = false;
      return(true);
    }
  return(false);
}
#endif
