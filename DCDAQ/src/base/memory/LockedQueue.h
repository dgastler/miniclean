#ifndef __LOCKEDQUEUE__
#define __LOCKEDQUEUE__

#include <iostream>
#include <pthread.h>
#include <cstdio>
#include <errno.h>
#include <cstring> //for strerr

#include <queue>

#include <LockCleanup.h>
#include <LockedObject.h>



//==========================================================================
//Functions you care about:
//
//Get( Type, wait):  Gets the next Type.  Waits if specified 
//                   returns true if a message was returned in Type
//                   returns false if no message to return.  returns bad Type
//
//Add( Type, wait):  Adds the next Type.  Waits if specified
//
//Size():            Returns queue size
//
//Clear():           Clears all data from queue
//
//ShutdownWait():    Removes the wait option on queues (used when shutting down DCDAQ)
//
//WakeUp():          Uses condition variable to wake anyone stuck waiting on data.
//                   Used after ShutdownWait() when shutting down DCDAQ.
//Setup(Type):       Sets the value returned by Get if there is not data to be returned.
//
//GetFIFOfds():      Return the fds for this queues FIFO
//==========================================================================

template< class T > 
class LockedQueue: public LockedObject
{
 public:
  LockedQueue()
    {
      //Setup the Queue
      //Allocate this queue's mutex lock.
      if(pthread_mutex_init(&mutex,NULL))
	{
	  fprintf(stderr,"Error creating mutex!\n");
	}
      //Allocate this queue's condition variable
      if(pthread_cond_init(&cond,NULL))
	{
	  fprintf(stderr,"Error creating condition variable!\n");
	}

      //Get our FIFO FDs
      int pipeReturn = pipe(QueueFIFO);   //Setup the FIFO
      if(pipeReturn == -1)                //Check if something went wrong
	{
	  printf("Warning:  Locked Queue failed to make FIFO! (error:%s)\n",
		 strerror(errno));
	  //Make sure the FIFOs are assigned to -1 if thing went wrong
	  QueueFIFO[0] = -1;
	  QueueFIFO[1] = -1;
	}
      badValueSet = false;
    };

  ~LockedQueue()
    {
      Clear(true);
      //close our FIFO
      if(QueueFIFO[1] != -1)
	{
	  close(QueueFIFO[1]);   //Close the FIFO
	  QueueFIFO[1] = -1;     //Make sure we think it's closed
	}
      if(QueueFIFO[0] != -1)
	{
	  close(QueueFIFO[0]);   //Close the FIFO
	  QueueFIFO[0] = -1;     //Make sure we think it's closed
	}
    };

  void Setup(T _badValue)
  {
    badValue = _badValue;
    badValueSet = true;
  };

  uint16_t GetSize(){return(Queue.size());};

  void GetFIFOfds(int * fds) 
  {
    //Let the 
    fds[0] = QueueFIFO[0];
    fds[1] = QueueFIFO[1];
  };
  
  bool Get( T &Obj,bool Wait)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef LOCKED_QUEUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"LockedQueue badValue not set!\n");
      }
#endif

    //Needed for later, but due to some MACRO scope issues
    //this needs to be declaired here.
    bool notEmpty;
    
    //Shutdown is used to shut down this object.
    if(Wait && (!GetShutdown())) //Shutdown stops a WAIT
      {
	//Get the mutex.
	pthread_mutex_lock(&mutex);
      }
    else // Don't wait
      {
	//try to get the mutex.
	int status = pthread_mutex_trylock(&mutex);
	if(status == EBUSY) // Mutex is already locked
	  {
	    Obj = badValue;
	    return(false); //Not waiting for unlock
	  }
      }

    //We now have the mutex!

    //Add a pthread cleanup routine in case pthread_cancel is called.
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "{"s
    pthread_cleanup_push(CleanupMutex,&mutex);

    //if we are waiting for a new block wait on a condition variable.
    if(Wait)
      {
	while(Queue.empty())
	  {
	    if(GetShutdown()){break;} //If we need to stop waiting, break here. (this isn't thread safe...)	    
	    pthread_cond_wait(&cond,&mutex);//condition wait
	  }
      }
    notEmpty = !Queue.empty();    
    if(notEmpty)
      {
	//Set obj to the element at the front of the queue
	Obj = Queue.front();
	//Remove the front object.
	Queue.pop();
	//read from FIFO
	if(QueueFIFO[0] != -1)  //Check that FIFO exists
	  {
	    //Read one int32_t from the pipe to show that we got the message
	    int32_t readValue= -1;   //-1 for convention right now.
	    //This value doesn't matter, just that it exists in the pipe
	    int readReturn = read(QueueFIFO[0],&readValue,sizeof(int32_t));
	    //Check for any read errors
	    if(readReturn == -1)
	      {
		fprintf(stderr,
			"Error in queue FIFO read: %s\n",
			strerror(errno)
			);
	      }
	  }
	else
	  {
	    printf("Warning:   Locked queue missing it's pipe\n");
	  }
      }
    else
      {
	//Set the object to badValue;
	Obj = badValue;
      }
    //    pthread_cond_signal(&cond);
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "}"s
    pthread_cleanup_pop(1);
    return(notEmpty);
  };

  bool Add(T &Obj,bool Wait)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef LOCKED_QUEUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"LockedQueue badValue not set!\n");
      }
#endif

    if(Wait)
      {
	//Get the mutex.
	pthread_mutex_lock(&mutex);
      }
    else // Don't wait
      {
	//try to get the mutex.
	int status = pthread_mutex_trylock(&mutex);
	if(status == EBUSY) // Mutex is already locked
	  {
	    return(false); //Not waiting for unlock
	  }
      }
    
    //We now have the mutex!
    //Add a pthread cleanup routine in case pthread_cancel is called.
    pthread_cleanup_push(CleanupMutex,&mutex);

    //Add obj to the end of the queue
    T PushObj = Obj;   //Make a copy so the Adder can't
                       //change it later.
    Queue.push(PushObj);
    //write to FIFO
    if(QueueFIFO[1] != -1)
      {
	//Write the value 0 into the queue.  
	//This value's value isn't used for anything besides feeding the pipe
	int32_t writeValue = 0;
	int writeReturn = write(QueueFIFO[1],&writeValue,sizeof(int32_t));
	//Check that our write worked
	if(writeReturn == -1)
	  {
	    fprintf(stderr,
		    "Error in queue FIFO write: %s\n",
		    strerror(errno)
		    );
	  }
      }
    else
      {
	printf("Warning:   Locked queue missing it's pipe\n");
      }
    //Now that the Object has been added to the queue, return back to the user the badValue.
    Obj = badValue;
    //signal so that if anyone is in cond_wait in Get() they wake up
    pthread_cond_signal(&cond);
    //release the mutex.
    pthread_cleanup_pop(1);
    return(true);
  };

  virtual void WakeUp(){pthread_cond_broadcast(&cond);};  

  bool Clear(bool wait)
  {
    if(wait)
      {
	//Get the mutex.
	pthread_mutex_lock(&mutex);
      }
    else // Don't wait
      {
	//try to get the mutex.
	int status = pthread_mutex_trylock(&mutex);
	if(status == EBUSY) // Mutex is already locked
	  {
	    return(false); //Not waiting for unlock
	  }
      }
    
    //We now have the mutex!
    
    //clear all objects in the queue
    while(Queue.size())
      {
	Queue.pop();
	//read from FIFO
	if(QueueFIFO[0] != -1)  //Check that FIFO exists
	  {
	    //Read one int32_t from the pipe to show that we got the message
	    int32_t readValue= -1;   //-1 for convention right now.
	    //This value doesn't matter, just that it exists in the pipe
	    int readReturn = read(QueueFIFO[0],&readValue,sizeof(int32_t));
	    //Check for any read errors
	    if(readReturn == -1)
	      {
		fprintf(stderr,
			"Error in queue FIFO read: %s\n",
			strerror(errno)
			);
	      }
	  }
      }
	  
    //no nead to signal the condition variable since no one wants to know the
    //queue is empty. 
    
    //unlock mutex
    pthread_mutex_unlock(&mutex);
    return(true);
  };

 private:  
  
  int QueueFIFO[2];        //FIFO for this queue
  std::queue< T > Queue;   //The free queue
  pthread_mutex_t mutex;   // Mutex lock for queue
  pthread_cond_t cond;     //Condition variable for mutex
  T badValue;
  volatile bool badValueSet;
};

#endif
