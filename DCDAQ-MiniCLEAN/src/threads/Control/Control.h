#ifndef __CONTROL__
#define __CONTROL__

#include <DCThread.h>

#include <unistd.h>
#include <fcntl.h>

#include <vector>
#include <string>
#include <algorithm> // for transform
#include <cctype> // for tolower
#include <fstream> //opening files
#include <boost/algorithm/string.hpp> //for iequals
#include <sstream>

#include <DCAddress.h>

#include <readline/readline.h>
#include <readline/history.h>

class Control : public DCThread 
{
 public:
  Control(std::string Name);
  ~Control();
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);  
  virtual void MainLoop();

  //ProcessCommand must be a public member function so that it can be accessed using the this pointer in the Callback function of cl_helper.cpp
  std::string ProcessCommand(std::string Command,
			     DCMessage::DCAddress destAddress = DCMessage::DCAddress());

 private:
  void LowerCase(std::string & Text);
  void RemoveEOL(std::string & Text);
  int bufferSize;
  char * buffer;

  //Commands
  void Quit     (std::string arg, DCMessage::DCAddress address);
  void Start    (std::string arg, DCMessage::DCAddress address);
  void Pause    (std::string arg, DCMessage::DCAddress address);
  void Help     (std::string arg, DCMessage::DCAddress address);
  void Echo     (std::string arg, DCMessage::DCAddress address);
  void SendOut  (std::string arg, DCMessage::DCAddress address);
  void SendMesg (std::string arg, DCMessage::DCAddress address);
  void Status   (std::string arg, DCMessage::DCAddress address);
  void Route    (std::string arg, DCMessage::DCAddress address);
  void Load     (std::string arg, DCMessage::DCAddress address);
  void RunNumber(std::string arg, DCMessage::DCAddress address);
  void CmdLoop  (std::string arg, DCMessage::DCAddress address);
  void Command  (std::string arg, DCMessage::DCAddress address);
  void SendCommand  (std::string arg, DCMessage::DCAddress address);
  void Send  (std::string arg, DCMessage::DCAddress address);
  std::string loopCommand;
  DCMessage::DCAddress loopAddress;
  time_t nextLoopTime;
  int loopTime;

  std::vector<std::string> Commands;
  std::vector<void (Control::*)(std::string arg,
				DCMessage::DCAddress address
				)> CommandFunctions;

  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
};


#endif
