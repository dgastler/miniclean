#ifdef __RATDS__
#include <DRPCBadEvent.h>

DRPCBadEvent::DRPCBadEvent(std::string Name)
{
  SetType("DRPCBadEvent");
  SetName(Name);
  //Set initial value for statistics updates
  lastTime = time(NULL);
  currentTime = lastTime;
  badEventCount = 0;
  Ready = false;
  mem = NULL;
}

bool DRPCBadEvent::Setup(xmlNode * SetupNode, 
			 std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;
  if(!FindMemoryManager(mem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!mem)
    {
      PrintError("Missing DRPC RAT memory manager\n.");
      ret = false;
    }
  if(FindSubNode(SetupNode,"TIMEWINDOW") == NULL)
    {
      badEventWindow = 0;
    }
  else
    {
      GetXMLValue(SetupNode,"TIMEWINDOW",GetName().c_str(),badEventWindow);
    }
  if(FindSubNode(SetupNode,"EVENTTHRESH") == NULL)
    {
      badEventThresh = 0;
    }
  else
    {
      GetXMLValue(SetupNode,"EVENTTHRESH",GetName().c_str(),badEventThresh);
    }
  shutdownCheck = false;
  if(badEventWindow != 0 && badEventThresh != 0)
    {
      shutdownCheck = true;
    }
  badEventTimes.resize(badEventThresh,0);
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


void DRPCBadEvent::MainLoop()
{
  if(IsReadReady(mem->GetBadEventFDs()[0]))
    {
      badEventTimes.push_back(time(NULL));
      badEventTimes.pop_front();
      //Get the bad event
      BadEvent::sBadEvent badEvent;
      if(mem->GetBadEvent(badEvent))
	//Get index worked
	{
	  //Count of all bad events
	  badEventCount++;
	  //count of bad events by submitted error code
	  if(errorCodeCount.find(badEvent.errorCode) == 
	     errorCodeCount.end())
	    {
	      errorCodeCount[badEvent.errorCode] = 0;
	    }
	  errorCodeCount[badEvent.errorCode]++;	      
	  
	  if(badEvent.index != mem->GetBadValue())
	    //index and event are valid
	    {
	      //Get the event at this index
	      RATEVBlock * event = mem->GetDRPCBlock(badEvent.index);
	      if(event != NULL)
		{
		  //Count of bad events by event status
		  if(stateCount.find(event->state) == stateCount.end())
		    {
		      stateCount[event->state] = 0;
		    }
		  stateCount[event->state]++;		  
		}
	      //Add the event to the free queue
	      if(!mem->AddFree(badEvent.index))
		{
		  //We are probably shutting down in this case
		  PrintError("Can't add to free queue.\n");
		  //		  SendStop();
		}
	      //Check whether we have gotten enough bad events in the
	      //last badEventWindow seconds that we should shut down.
	      if((int(badEventTimes.back() - badEventTimes.front()) < int(badEventWindow)) 
		 && shutdownCheck)
		{		 
		  PrintError("Too many bad events in time window.\n");
		  shutdownCheck = false;
		  //		  SendStop();
		}
	    }
	  else
	    {
	      PrintError("Got a bad event index...\n");
	    }
	}
      else
	{
	  PrintError("Failed to get bad event index...\n");
	}
    }
}

void DRPCBadEvent::ProcessTimeout()
{
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t TimeStep = currentTime-lastTime;

      StatMessage::StatRate ssRate;
      DCMessage::Message message;
      
      //Send the badCount
      ssRate.sBase.type = StatMessage::BAD_EVENT_RATE;
      ssRate.count = badEventCount;
      ssRate.sInfo.time = currentTime;
      ssRate.sInfo.interval = TimeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.units = Units::NUMBER;

      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);
      badEventCount = 0;

      for(std::map<Level::Type,uint32_t>::iterator it = errorCodeCount.begin(); 
	  it != errorCodeCount.end();
	  it++)
	{
	  ssRate.sBase.type = StatMessage::BAD_EVENT_RATE;
	  ssRate.count = it->second;
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::BLANK;
	  ssRate.sInfo.level = it->first;
	  ssRate.units = Units::NUMBER;
	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);	  
	  
	  it->second = 0;
	}

//      //StateCount
//      for(std::map<int8_t,uint32_t>::iterator it = stateCount.begin(); 
//	  it != stateCount.end();
//	  it++)
//	{
//	  sprintf(timedCount.text,"Bad event count @ state %d: ",it->first);
//	  timedCount.ID = it->first;
//	  timedCount.count = it->second;
//	  timedCount.dt = TimeStep;
//	  message.SetDataStruct(timedCount);
//	  message.SetType(DCMessage::STATISTIC);
//	  SendMessageOut(message,false);
//	  it->second = 0;
//	}
//      //stringCount
//      for(std::map<Level::Type,uint32_t>::iterator it = errorCodeCount.begin(); 
//	  it != errorCodeCount.end();
//	  it++)
//	{
//	  sprintf(timedCount.text,"Bad event error code %d count: ",it->first);
//	  timedCount.ID = 0;
//	  timedCount.count = it->second;
//	  timedCount.dt = TimeStep;
//	  message.SetDataStruct(timedCount);
//	  message.SetType(DCMessage::STATISTIC);
//	  SendMessageOut(message,false);
//	  it->second = 0;
//	}

      //record time
      lastTime = currentTime;
    }
}

bool DRPCBadEvent::ProcessMessage(DCMessage::Message & message)
{
  if(message.GetType() == DCMessage::GO)
    {
      if(Ready)
	{
	  AddReadFD(mem->GetBadEventFDs()[0]);
	  SetupSelect();
	  Loop = true;
	}
      return(true);
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      
      RemoveReadFD(mem->GetBadEventFDs()[0]);
      SetupSelect();
      Loop = false;
      return(true);
    }
  return(false);
}
#endif
