#include <string>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <sstream>

#include <mysql.h>

#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TStyle.h>

#include "Helper.h"

void MakeRatePlot(MYSQL * conn,int runNumber,time_t start,time_t end);
void MakeRatePlotV2(MYSQL * conn,int runNumber,time_t start,time_t end);
