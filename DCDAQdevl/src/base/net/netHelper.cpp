#include <netHelper.h>

int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint16_t size,
	       struct timeval /*timeout*/)
{
  int32_t ret = 0;

  //From Unix Network Programming: Stevens.
  uint8_t * writePtr = ptr;
  int32_t nLeft = size;
  
  int32_t nWritten = 0;

  //Loop over what is left to write
  while(nLeft >0)
    {
      //take a shot at writing the data
      if( (nWritten = write(socketFD,writePtr,nLeft)) <=0)
	//Process any errors
	{
	  if(errno == EINTR)
	    {
	      nWritten = 0;
	    }
	  else if(errno == EPIPE)
	    {
	      //	      ret = -2;
	      if(errno < 0)
		{
		  fprintf(stderr,"Negative errno!\n");
		}
	      ret = -errno;
	      break;
	    }
	  else
	    {
	      printf("Socket write error(%d): %s\n",errno,strerror(errno));
	      //	      ret = -1;
	      if(errno < 0)
		{
		  fprintf(stderr,"Negative errno!\n");
		}
	      ret = -errno;
	      break;
	    }
	}
      //remote the transmitted data from the send "queue" and keep going
      nLeft -= nWritten;
      //      ptr += nWritten;
      writePtr += nWritten;
    }
  if(ret >= 0)
    ret = size;
  return(ret);
}

int32_t readN(int socketFD,
	      uint8_t * ptr,
	      uint16_t maxSize,
	      struct timeval /*timeout*/)
{
   int32_t ret = 0;

   //From Unix Network Programming: Stevens.
   uint8_t * readPtr = ptr;
   int32_t nLeft = maxSize;
   
   int32_t nRead = 0;
  
   while(nLeft >0)
     {
       //Read some data
       if( (nRead = read(socketFD,readPtr,nLeft)) < 0)
	 //process errors
	 {
	   if(errno == EINTR)
	     {
	       nRead = 0;
	     }
	   else if(errno == EPIPE)
	     {
	       //	       ret = -2;
	       if(errno < 0)
		 {
		   fprintf(stderr,"Negative errno!\n");
		 }
	       ret = -errno;
	       break;
	     }
	   else
	     {
	       if(errno < 0)
		 {
		   fprintf(stderr,"Negative errno!\n");
		 }
	       ret = -errno;
	       //	       ret = -1;
	       break;
	     }
	 }
       else if (nRead == 0)
	 {
	   break;
	 }
       //move along in our read to queue for our read
       nLeft -= nRead;
       //       ptr += nRead;
       readPtr += nRead;
     }
   
   if(ret >= 0)
     ret = maxSize - nLeft;   
   
   return (ret);
}

bool CheckLocal(std::string IP,int fd)
{
  bool ret = false;
  //Structure that stores all of our connections
  struct ifconf ifc;
  char * buffer;
  int bufferSize;
  int lastBufferSize;
  bufferSize = 10*sizeof(struct ifreq);
  lastBufferSize = 0;
  
  //Find out how big to make the buffer in ifc
  while(true)
    {
      //Allocate room for the ifreq objects in ifconf object
      buffer = new char[bufferSize];
      ifc.ifc_len = bufferSize;
      ifc.ifc_ifcu.ifcu_buf = buffer;
      
      if(ioctl(fd,SIOCGIFCONF,&ifc,sizeof(struct ifconf)) < 0)
	{
	  //ioctl error with SIOCGIFCONF
	  fprintf(stderr,"Error in SIOCGIFCONF\n");
	  delete [] buffer;
	  buffer = NULL;
	  break;
	}
      else
	{
	  if(ifc.ifc_len == lastBufferSize)
	    {
	      //Our buffer is big enough.
	      break;
	    }
	  else
	    {
	      //Record the current buffer size for the next loop.
	      lastBufferSize = ifc.ifc_len;
	      bufferSize = bufferSize << 2;		      
	    }
	}            
      //Clean up our buffer (doesn't happen when we sucessfully find the size)
      delete [] buffer;
    }//Finding buffer size and getting data.
  


  //At this point we either have valid data in buffer or it is NULL and things have failed.
  if(buffer)
    {
      unsigned int textBufferSize = INET6_ADDRSTRLEN; //size for ipv6, ipv4 is smaller.
      char * textBuffer = new char[textBufferSize];
      //Loop over the returned objects and see if our IP address is there.
      int size = (ifc.ifc_len)/(sizeof(struct ifreq));  //Number of ifc_req structs
      for(int i = 0; i < size; i++)
	{
	  //Check the current ifc_req object's IP address is equal to IP
	  struct sockaddr_in * addr_ptr = (struct sockaddr_in *) &(ifc.ifc_ifcu.ifcu_req[i].ifr_ifru.ifru_addr);
	  inet_ntop(addr_ptr->sin_family,&(addr_ptr->sin_addr),textBuffer,textBufferSize);
	  if(IP.compare(textBuffer) == 0)
	    {
	      //We found the IP we were looking for. BREAK!
	      ret = true;
	      break;
	    }
	}
      //clean up.
      delete [] buffer;
      delete [] textBuffer;
    }//Analyzing the returned data
  
  return(ret);
}

bool AddrFullCompare(struct sockaddr_in * addr1, struct sockaddr_in * addr2)
{
  bool ret = true;

  //Check that both addr have the correcty type
  if((addr1->sin_family != AF_INET) ||
     (addr2->sin_family != AF_INET))
    {
      ret = false;
    }
  //Check that both addrs use the same port
  if(addr1->sin_port != addr2->sin_port)
    {
      ret = false;
    }
  //Check that both addrs have the same address
  if(addr1->sin_addr.s_addr != addr2->sin_addr.s_addr)
    {
      ret = false;
    }
  return(ret);
}



bool SetNonBlocking(int &fd,bool value)
{
  //Get the previous flags
  int currentFlags = fcntl(fd,F_GETFL,0);
  if(currentFlags < 0)
    {
      return(false);
    }
  //Make the socket non-blocking
  if(value)
    {
      currentFlags |= O_NONBLOCK;
    }
  else
    {
      currentFlags &= ~O_NONBLOCK;
    }

  int currentFlags2 = fcntl(fd,F_SETFL,currentFlags);
  if(currentFlags2 < 0)
    {
      return(false);
    }
  return(true);
}


