#ifndef __DCMESSAGESERVER__
#define __DCMESSAGESERVER__

#include <DCThread.h>
#include <DCNetThread.h>

#include <ServerConnection.h>
#include <Connection.h>
#include <netHelper.h>

class DCMessageServer : public DCThread 
{
 public:
  DCMessageServer(std::string Name);
  ~DCMessageServer(){};
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);  
  virtual void MainLoop();
 private:

  void ProcessChildMessage(unsigned int iConn);

  //Process messages in/out
  void RouteRemoteDCMessage(size_t iConn);
  DCMessage::Message ReadRemoteDCMessage(size_t iConn);
  bool RouteLocalDCMessage(DCMessage::Message &message);
  bool SendMessage(size_t iConn,DCMessage::Message &message);

  //Handle connections
  bool ProcessNewConnection();
  void DeleteConnection(unsigned int iConn);
  ServerConnection server;

  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;

  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  
  virtual void PrintStatus();

  virtual void  ThreadCleanUp();
  //  time_t HACK;
  //  int HACKos;
};


#endif
