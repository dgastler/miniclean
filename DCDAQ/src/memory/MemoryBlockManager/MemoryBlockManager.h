#ifndef __MEMORYBLOCKMANAGER__
#define __MEMORYBLOCKMANAGER__

#include <MemBlocks.h>
#include <MemoryManager.h>
#include <FreeFullQueue.h> 
#include <xmlHelper.h>

class MemoryBlockManager : public MemoryManager , public FreeFullQueue
{
 public:
  MemoryBlockManager(std::string Name);
  ~MemoryBlockManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  std::vector<MemoryBlock> Memory;
 private:
  void Deallocate();
};

#endif
