#ifdef __RATDS__
#include <PackData.h>


void PackData::MainLoop()
{
  //Fix select code here!
  if((blockIndex == FreeFullQueue::BADVALUE) && (Mem != NULL))
    {
      //Get new memory block
      Mem->GetFree(blockIndex,true);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = Mem->GetBlock(blockIndex);
	  block->dataSize = 0;
	}
      else
	{
	  PrintError("Unable to get memblock!");
	  //Stop this thread
	  Loop = false;
	  return;
	}
      
      //Get a pointer to the block buffer we are going to write to.
      bufferPointer = (uint32_t *)(block->buffer + block->dataSize);

      //Add run information 
      if(firstBlock)  //This probably fails if there are multiple PackData threads... I'll add it to my list of crap I need to fix
	{
	  //bufferPointer = WriteRunHeader(bufferPointer);
	  // DCDAQEvent::RunStart *runStart = (DCDAQEvent::RunStart *) bufferPointer;
	  // runStart->magic = DCDAQEvent::RUNSTART;
	  // runStart->version = DCDAQEventVersion;
	  // runStart->runNumber = GetRunNumber();
	  // bufferPointer += sizeof(DCDAQEvent::RunStart);
	  bufferPointer = WriteSimpleRunHeader(bufferPointer);
	  block->dataSize = (uint32_t)(bufferPointer-block->buffer);
	  firstBlock = false;
	}
      else
	{
	  DCDAQEvent::RunContinue *runContinue = new (bufferPointer) DCDAQEvent::RunContinue();
	  runContinue->runNumber=GetRunNumber();
	  //Ceiling of size in 32bit words
	  int wordsWritten = (sizeof(DCDAQEvent::RunContinue)/sizeof(uint32_t)) + ((sizeof(DCDAQEvent::RunContinue) % sizeof(uint32_t)) > 0 ? 1 :0);
	  bufferPointer += wordsWritten;
	  block->dataSize += wordsWritten*sizeof(uint32_t);
	}
      
    }
  if((blockIndex != FreeFullQueue::BADVALUE) || (RATCopy_mem != NULL))
    {
      //We have a valid memblock, or are writing to FIFO
      CheckFEPCBad();
      CheckFEPCSend();
      CheckDRPCBad();
      CheckDRPCRespond();
      CheckEBPCBad();
      CheckEBPCDisk();         
    }
}

void PackData::CheckFEPCBad()
{
  if(FEPC_mem!=NULL && writeFEPCBad && IsReadReady(FEPC_mem->GetBadEventFDs()[0]))
    {
      BadEvent::sBadEvent badEvent;
      if(!FEPC_mem->GetBadEvent(badEvent))
	{
	  PrintError("Failed to get FEPC_mem index");
	  Loop = false;
	}
      else
	{
	  RATEVBlock *Event = FEPC_mem->GetFEPCBlock(badEvent.index);
	  ProcessRATBlock(Event);
	  FEPC_mem->AddFree(badEvent.index);
	}
    }
}

void PackData::CheckFEPCSend()
{
  if(FEPC_mem!=NULL && writeFEPCSend && IsReadReady(FEPC_mem->GetUnSentFDs()[0]))
    {
      int32_t index;
      if(!FEPC_mem->GetUnSent(index))
	{
	  PrintError("Failed to get FEPC_mem index");
	  Loop=false;
	}  
      else
	{
	  RATEVBlock *Event = FEPC_mem->GetFEPCBlock(index);
	  ProcessRATBlock(Event);   
	  if(!FEPC_mem->AddFree(index))
	    {
	      PrintError("Failed to return block to free");
	    }
	}
    }
}

void PackData::CheckDRPCBad()
{
  if(DRPC_mem!=NULL && writeDRPCBad && IsReadReady(DRPC_mem->GetBadEventFDs()[0]))
    {
      BadEvent::sBadEvent badEvent;
      if(!DRPC_mem->GetBadEvent(badEvent))
	{
	  PrintError("Failed to get DRPC_mem index");
	  Loop = false;
	}
      else
	{
	  RATEVBlock *Event = DRPC_mem->GetDRPCBlock(badEvent.index);
	  ProcessRATBlock(Event);
	  DRPC_mem->AddFree(badEvent.index);
	}
    }
}

void PackData::CheckDRPCRespond()
{
  if(DRPC_mem!=NULL && writeDRPCRespond && IsReadReady(DRPC_mem->GetRespondFDs()[0]))
    {
      int32_t index;
      if(!DRPC_mem->GetRespond(index))
	{
	  PrintError("Failed to get DRPC_mem index");
	  Loop = false;
	}
      else
	{
	  RATEVBlock *Event = DRPC_mem->GetDRPCBlock(index);
	  ProcessRATBlock(Event);
	  DRPC_mem->AddFree(index);
	}
    }
}

void PackData::CheckEBPCBad()
{
  if(EBPC_mem!=NULL && writeEBPCBad && IsReadReady(EBPC_mem->GetBadEventFDs()[0]))
    {
      BadEvent::sBadEvent badEvent;
      if(!EBPC_mem->GetBadEvent(badEvent))
	{
	  PrintError("Failed to get EBPC_mem index");
	  Loop =false;
	}
      else
	{
	  RATDSBlock *Event = EBPC_mem->GetRATDiskBlock(badEvent.index);
	  ProcessRATBlock(Event);
	  EBPC_mem->AddFree(badEvent.index);
	}
    }
}

void PackData::CheckEBPCDisk()
{

  if(EBPC_mem!=NULL && writeEBPCDisk && IsReadReady(EBPC_mem->GetToDiskFDs()[0]))
    {
      int32_t index;
      if(!EBPC_mem->GetToDisk(index))
	{
	  fprintf(stderr,"GetToDisk error\n");
	  PrintError("Failed to get EBPC_mem index");
	  Loop = false;//Stop this thread
	}
      else
	{
	  RATDSBlock *Event = EBPC_mem->GetRATDiskBlock(index);
	  ProcessRATBlock(Event);
	  EBPC_mem->AddFree(index);
	}
    }
}

void PackData::ProcessRATBlock(RATEVBlock * event)
{
  ProcessEV(event->ev);
}
void PackData::ProcessRATBlock(RATDSBlock * event)
{
  ProcessEV(event->ds->GetEV(0));
}


//void PackData::ProcessEV(RATEVBlock  *event)
void PackData::ProcessEV(RAT::DS::EV  *ev)
{
  //  printf("procEV(EV) \n");
  //  RAT::DS::EV * ev=event->ev;
  if(Mem != NULL)
    {
      uint32_t spaceRemaining=block->allocatedSize-block->dataSize;
      uint32_t wordsWritten;
      int64_t eventSize=0;

      //      DCDAQEvent::Event * tempEvent = (DCDAQEvent::Event *)bufferPointer;

      if((eventSize=StreamEVtoBuff(ev,(uint8_t*)bufferPointer,spaceRemaining))<0)
	{
	  // printf("-1 returned");
	  if(eventSize<-1)
	    {
	      PrintError("Bad Reduction Level");	      
	      return;
	    }
	  //Send this MemBlock off and get a new one
	  if(!Mem->AddFull(blockIndex))
	    {
	      PrintError("Failed to add mem block to full queue");
	    }
	  // printf("sent off MemBlock \n");

	  blockIndex = FreeFullQueue::BADVALUE;
	  if(Mem->GetFree(blockIndex,true))
	    {
	      block = Mem->GetBlock(blockIndex);
	      block->dataSize = 0;
	      
	      //Get a pointer to the block buffer we are going to write to.
	      bufferPointer = (uint32_t *)(block->buffer + block->dataSize);
	      
	      //Add run information
	      DCDAQEvent::RunContinue *runContinue = new(bufferPointer) DCDAQEvent::RunContinue();
	      runContinue->runNumber=GetRunNumber();
	      //Compute the number of 32bit words in this struct (round up)
	      wordsWritten=  (sizeof(DCDAQEvent::RunContinue)/sizeof(uint32_t)) + ((sizeof(DCDAQEvent::RunContinue) % sizeof(uint32_t)) > 0 ? 1 :0);
	      bufferPointer += wordsWritten;
	      block->dataSize += wordsWritten;
	      spaceRemaining=block->allocatedSize-block->dataSize;
	      eventSize=StreamEVtoBuff(ev,(uint8_t*)bufferPointer,spaceRemaining);
	      if(eventSize < 0)
		{
		  PrintWarning("Failed to add event\n");
		  eventSize = 0;
		}
	    }
	  else
	    {
	      PrintError("Failed to get Memblock\n");
	      Loop = false;
	    }
	
	}
      else
	{
	  //	  fprintf(stderr,"TimeCode test: 0x%016zX\n",tempEvent->eventTime);
	}

      EventSizeHist->Fill(eventSize);
      wordsWritten = (eventSize/sizeof(uint32_t)) + ((eventSize % sizeof(uint32_t)) > 0 ? 1 :0);
      bufferPointer += wordsWritten;
      block->dataSize += wordsWritten;
    }
  if(RATCopy_mem != NULL)
    {
     
      RATCopy_mem->CheckEntriesSocket(ev,false);
      
    }
  timeStepProcessedEvents++;
}


//void PackData::ProcessEV(RATDSBlock *event)
//{
//  RAT::DS::EV * ev=event->ds->GetEV(0);
//  uint32_t wordsWritten;
//  if(Mem != NULL)
//    {
//      uint32_t spaceRemaining=block->allocatedSize-block->dataSize;
//
//      int64_t eventSize=0;
//      //DCDAQEvent::Event * tempEvent = (DCDAQEvent::Event *)bufferPointer;
//      if((eventSize=StreamEVtoBuff(ev,(uint8_t*)bufferPointer,spaceRemaining))<0)
//	{
//	  if(eventSize<-1)
//	    {
//	      PrintError("Bad Reduction Level");
//	      return;
//	    }
//	  //Send this MemBlock off and get a new one
//	  if(!Mem->AddFull(blockIndex))
//	    {
//	      PrintError("Failed to add mem block to full queue");
//	    }
//	  blockIndex = FreeFullQueue::BADVALUE;
//	  if(Mem->GetFree(blockIndex,true))
//	    {
//	      
//	      block = Mem->GetBlock(blockIndex);
//	      block->dataSize = 0;
//	      
//	      //Get a pointer to the block buffer we are going to write to.
//	      bufferPointer = (uint32_t *)(block->buffer + block->dataSize);
//	      
//	      
//	      //Add run information
//	      DCDAQEvent::RunContinue *runContinue =new (bufferPointer) DCDAQEvent::RunContinue();
//	      runContinue->runNumber=GetRunNumber();
//	      //Compute the number of 32bit words in this struct (round up)
//	      wordsWritten = (sizeof(DCDAQEvent::RunContinue)/sizeof(uint32_t)) + ((sizeof(DCDAQEvent::RunContinue) % sizeof(uint32_t)) > 0 ? 1 :0);
//	      bufferPointer += wordsWritten;
//	      block->dataSize+= wordsWritten;
//	      spaceRemaining=block->allocatedSize-block->dataSize;
//	      eventSize=StreamEVtoBuff(ev,(uint8_t*)bufferPointer,spaceRemaining);
//	    }
//	}
//      else
//	{
//	  //	  fprintf(stderr,"TimeCode test: 0x%016zX\n",tempEvent->eventTime);
//	}
//      wordsWritten = int(ceil(eventSize/sizeof(uint32_t)));
//      bufferPointer += wordsWritten;  
//      block->dataSize += wordsWritten;
//    }
//
//  if(RATCopy_mem != NULL)
//    {
//     
//      RATCopy_mem->CheckEntriesSocket(ev,false);
//     
//    }
//  timeStepProcessedEvents++;
//}






uint32_t * PackData::WriteSimpleRunHeader(uint32_t * bufferPointer)
{
  DCDAQEvent::RunStart *runStart =new (bufferPointer) DCDAQEvent::RunStart();
  runStart->runNumber = GetRunNumber();
  runStart->startTime = GetRunStartTime();

  //Write the channel map as a string 
  std::stringstream ss;
 
  std::vector <std::vector< int16_t > > ChannelMap = detectorInfo.GetChannelMap();
 

  ss << "<XML>";
  ss << "<CHANNELMAP>";
  //Loop over boards
  for(uint iBoard = 0; iBoard < ChannelMap.size(); iBoard++)
    {
      if(ChannelMap[iBoard].size() > 0)
	{
	  ss << "<WFD>";
	  ss << "<BOARDID>" << iBoard << "</BOARDID>";
	  ss << "<BOARDDELAY>" << detectorInfo.GetInternalDelay(iBoard) << "</BOARDDELAY>";
	  //Loop over channels
	  for(uint iChan = 0; iChan < ChannelMap[iBoard].size(); iChan++)
	    {
	      ss << "<PMT>";
	      ss << "<WFDPOS>" << iChan << "</WFDPOS>";
	      ss << "<PMTID>" << ChannelMap[iBoard][iChan] << "</PMTID>";
	      ss << "<ADC_OFFSET>" << detectorInfo.GetPMTOffset(ChannelMap[iBoard][iChan])  << "</ADC_OFFSET>";
	      ss << "<PRESAMPLES>" << detectorInfo.GetPMTPreSamples(ChannelMap[iBoard][iChan])  << "</PRESAMPLES>";
	      ss << "</PMT>";
	    }
	  ss << "</WFD>";
	}
    }
  ss << "</CHANNELMAP>";
  ss << "</XML>";

  //Pad the end of the string to 32 bits
  size_t channelMapSize = ss.str().size() + 1; //+1 for NULL terminator (in BYTES)
  if((channelMapSize) % 4 != 0)
    {
      //Up the size to the next multiple of 32bits
      channelMapSize += 4-(channelMapSize%4); //BYTES
    }

  runStart->channelMapSize = (channelMapSize >> 2); //(32bit words)

  bufferPointer += (sizeof(DCDAQEvent::RunStart)/sizeof(uint32_t)) + ((sizeof(DCDAQEvent::RunStart) % sizeof(uint32_t)) > 0 ? 1 :0);//int(ceil(sizeof(DCDAQEvent::RunStart)/sizeof(uint32_t)));

  memcpy(bufferPointer,ss.str().c_str(),channelMapSize);  //BYTES

  bufferPointer += (runStart->channelMapSize); //(32bit words)

  return bufferPointer;
}

uint32_t * PackData::WriteRunHeader(uint32_t * bufferPointer)
{
  RunInfo::sRunInfo_001 runHeader;
  runHeader.version = 1;
  runHeader.run_number = GetRunNumber();
  runHeader.run_time = 0;
  string str1 ("Test");
  runHeader.run_xml = new char[str1.size()];
  strcpy(runHeader.run_xml,str1.c_str());
  runHeader.xml_size = sizeof(runHeader.run_xml);

  string str2 ("Test");
  runHeader.run_text = new char[str2.size()];
  strcpy(runHeader.run_text,str2.c_str());
  runHeader.text_size = sizeof(runHeader.run_text);

  runHeader.run_type = -2;
  // runHeader.run_presamples = presamples;
  // runHeader.run_start_prompt = start_prompt;
  // runHeader.run_end_prompt = end_prompt;
  
  runHeader.size_word = runHeader.size_word | (sizeof(runHeader) & 0xFFFFFF);

  std::vector<uint8_t> headerVector = runHeader.StreamData();
  

  memcpy(bufferPointer,&(headerVector[0]),headerVector.size());
  bufferPointer += sizeof(runHeader)/sizeof(uint32_t);
  return bufferPointer;
}

void PackData::EndOfRun()
{
  //Make sure we have stopped listening for events
  if(FEPC_mem != NULL)
    {
      RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
      RemoveReadFD(FEPC_mem->GetBadEventFDs()[0]);
    }
  if(DRPC_mem != NULL)
    {
      RemoveReadFD(DRPC_mem->GetRespondFDs()[0]);
      RemoveReadFD(DRPC_mem->GetBadEventFDs()[0]);
    }
  if(EBPC_mem != NULL)
    {
      RemoveReadFD(EBPC_mem->GetToDiskFDs()[0]);
      RemoveReadFD(EBPC_mem->GetBadEventFDs()[0]);
    }

  if(Mem != NULL)
    { 
      if(Mem->DeIncrementWritingThreads())
	//We are the last PackData thread.
	{
	  //	  printf("We are the last PackData thread\n");
	  Print("We are the last PackData thread\n");
	  Mem->GetFree(blockIndex,true);
	  if(blockIndex != FreeFullQueue::BADVALUE)
	    {
	      block = Mem->GetBlock(blockIndex);
	      block->dataSize = 0;
	    }
	  else
	    {
	      
	    }
	  bufferPointer = (uint32_t *)(block->buffer + block->dataSize);
	  DCDAQEvent::RunEnd *runEnd = (DCDAQEvent::RunEnd *) bufferPointer;
	  runEnd->magic = DCDAQEvent::RUNEND;

	  if(!Mem->AddEndOfRun(blockIndex))
	    {
	      PrintError("Failed to add end of run block");
	    }

	  Print("Wrote the RunEnd word\n");


	  block->dataSize += sizeof(DCDAQEvent::RUNEND);
	}
      //Send the memblock off
      if(!Mem->AddFull(blockIndex))
	{
	  PrintError("Failed to add mem block to full queue");
	}
      else
	//If we have a MemBlock, send it off.
	{
	  if(blockIndex != FreeFullQueue::BADVALUE)
	    {
	      if(!Mem->AddFull(blockIndex))
		{
		  PrintError("Failed to add mem block to full queue");
		}
	    }
	}
  
      //Now send an empty memblock, telling the next thread we are shutting down.
      // Mem->GetFree(blockIndex,true);
      // if(blockIndex != FreeFullQueue::BADVALUE)
      //   {
      //     block = Mem->GetBlock(blockIndex);
      //     block->dataSize = 0;
      //     Mem->AddFull(blockIndex);
      //   }
      // Mem->Shutdown();

    }

  //printf("PackData shut down succesfully\n");
    
}
#endif
