 #include <EBAssembler.h> 
int EBAssembler::ProcessCHAN_ZLE_WAVEFORM(uint8_t *data, uint16_t size,EBPCBlock * event)
{
  //parsed channels (-1 for an error)
  int ret = 0; 

  //Parsing pointer and size left.
  uint8_t * dataPtr = data;
  uint16_t  dataSizeLeft = size;

  //Check to see if we have enough space in the data for a EBPacket::Event
  if(sizeof(EBPacket::Event) > dataSizeLeft)
    {
      PrintError("Packet too small for EBPacket::Event (CHAN_ZLE_WAVEFORM packet).\n");
      ret = -1;
    }
  else
    //We have enough data to have a Event header packet.
    { 
      
      EBPacket::Event * eventHeader = (EBPacket::Event * ) dataPtr;
      dataPtr += sizeof(EBPacket::Event);        //move forward in dataPtr
      dataSizeLeft -= sizeof(EBPacket::Event);   //decrease remaining size
      uint16_t channelCount = eventHeader->channelCount;
      uint16_t channelsParsed = 0;

      //RAT::DS::EV * ev = event->ds->AddNewEV();

      for(uint16_t iChannel = 0; iChannel < channelCount;iChannel++)
	//Parse each channel
	{

	  //check that we have enough space left for a EBPacket::ChannelData block
	  if(sizeof(EBPacket::ChannelData) > dataSizeLeft)
	    {
	      PrintError("Packet too small for EBPacket::ChannelData (CHAN_ZLE_WAVEFORM) packet.\n");
	      break;
	    }
	  else
	    //We have enough data to have a channel data header packet;
	    {
	      EBPacket::ChannelData * channelHeader = (EBPacket::ChannelData *) dataPtr;
	      dataPtr += sizeof(EBPacket::ChannelData);      //move forward in dataPtr
	      dataSizeLeft -= sizeof(EBPacket::ChannelData); //decrease remaining size
	      
	      //Add a new PMT to this EBBlock's EV data structure
	      for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
		{pmtCopies[iCopies] = ev->AddNewPMT();}
	      if(pmtCopies[0] == NULL) {PrintError("Failed to get a new PMT from EV\n");SendStop();break;}
	      else
		{
		  //Set the PMT ID
		  for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
		    {
		      pmtCopies[iCopies]->SetID(nCopies*channelHeader->ID+iCopies);
		      rawCopies[iCopies] = pmtCopies[iCopies]->GetRaw();
		      if(rawCopies[0] == NULL)
			{PrintError("Failed to get a new RAW from PMT\n");SendStop();break;}
		    }

		  uint8_t dataBlockCount = channelHeader->dataBlockCount;
		  for(uint8_t iBlock = 0;iBlock < dataBlockCount;iBlock++)
		    {
		      if(sizeof(EBPacket::WaveformBlock) > dataSizeLeft)
			{
			  PrintError("Packet too small for EBPacket::WaveformBlock (CHAN_ZLE_WAVEFORM packet).\n");
			  iChannel = channelCount; //Stop outer for loop
			  break;
			}
		      else
			{
			  EBPacket::WaveformBlock * waveformBlock = (EBPacket::WaveformBlock *) dataPtr;
			  dataPtr += sizeof(EBPacket::WaveformBlock);
			  dataSizeLeft -= sizeof(EBPacket::WaveformBlock);
			  
			  if(waveformBlock->dataBytes > dataSizeLeft)
			    {
			      PrintError("Packet too small for EBPacket::WaveformBlock.dataBytes (CHAN_ZLE_WAVEFORM packet).\n");
			      iChannel = channelCount; //Stop outer for loop
			      break;
			    }
			  else			    
			    {
			      //Fill the waveform data into the Raw structure
			      for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
				{
				  rawWaveformCopies[iCopies] = rawCopies[iCopies]->AddNewRawWaveform();
	
			      if(rawWaveformCopies[iCopies] == NULL){PrintError("Failed to get a new RawWaveform from RAW\n");SendStop();break;}

			    //set the start time of the RawWaveform
			      
			      rawWaveformCopies[0]->SetTimeOffset(waveformBlock->startTime);
				}
			   

			      //Copy and set the samples for the raw waveform 
			      //There is possibly a way to do this with one less copy...
			      std::vector<uint16_t> samples;
			      samples.resize(waveformBlock->dataBytes/sizeof(uint16_t));
			      memcpy((uint8_t *) &samples[0],dataPtr,waveformBlock->dataBytes);
			      for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
				{rawWaveformCopies[0]->SetSamples(samples);}		
			      dataPtr      +=waveformBlock->dataBytes;
			      dataSizeLeft -=waveformBlock->dataBytes;
			      			      
			    }
			}
		    }
		  //Now that we have successfully parsed this channel we need to note that in two places
		  channelsParsed++;
		  event->PMT.push_back(channelHeader->ID);
		}
	    }
	}
      ret = channelsParsed;
      


      //Deal with the WFD headers
      uint16_t wfdHeaderCount = eventHeader->WFDHeaderCount;
      EBPacket::WFDHeader tempHeader;
      for(uint16_t i = 0; i < wfdHeaderCount;i++)
	{
	  memcpy(&tempHeader,dataPtr,sizeof(EBPacket::WFDHeader));	  
	  //	  printf("%08X\n%08X\n%08X\n%08X\n\n",
	  //		 tempHeader.word[0],
	  //		 tempHeader.word[1],
	  //		 tempHeader.word[2],
	  //		 tempHeader.word[3]);
	  dataPtr+=sizeof(EBPacket::WFDHeader);
	  dataSizeLeft -=sizeof(EBPacket::WFDHeader);
	  //fill the first PMT with valid header data
	  //Skipping the others to make sure people notice this code
	  //hasn't been updated to RAT-newdaq yet. 
	  if(i == 0)
	    {
	      if(ev->GetPMTCount() > 0)
		{
		  ev->GetPMT(0)->GetRaw()->SetHeader(tempHeader.word,4*sizeof(uint32_t));
		}
	    }
	}
    }
  return(ret);
}
