#include "NetPlots.h"

void MakeNetworkRatePlot(MYSQL * conn,int runNumber,time_t start,time_t end)
{
  std::vector<double> time;
  std::vector<double> val;
  std::stringstream ss;

  //Setup root plots
  TGraph * grDRPusher01;
  TGraph * grDRPusher02;
  TGraph * grDRPusher03;
  TGraph * grDRPusher04;

  TGraph * grDRGetter01;
  TGraph * grDRGetter02;
  TGraph * grDRGetter03;
  TGraph * grDRGetter04;

  TGraph * grEBPusher01;
  TGraph * grEBPusher02;
  TGraph * grEBPusher03;
  TGraph * grEBPusher04;


  ss << "select time,value from sc_sens_FE1_DRPDRTNET_02OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grDRPusher01 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE2_DRPDRTNET_03OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grDRPusher02 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE3_DRPDRTNET_04OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grDRPusher03 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE4_DRPDRTNET_05OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grDRPusher04 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");


  ss << "select time,value from sc_sens_FE1_DRGDRTNET_02IN where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-3);
  grDRGetter01 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE2_DRGDRTNET_03IN where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-3);
  grDRGetter02 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE3_DRGDRTNET_04IN where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-3);
  grDRGetter03 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE4_DRGDRTNET_05IN where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-3);
  grDRGetter04 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");




  ss << "select time,value from sc_sens_FE1_EBPDRTNET_02OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grEBPusher01 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE2_EBPDRTNET_03OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grEBPusher02 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE3_EBPDRTNET_04OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grEBPusher03 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");

  ss << "select time,value from sc_sens_FE4_EBPDRTNET_05OUT where time > "
     << start << " and time <" << end;
  GetXYData(conn,ss.str(),time, val);
  ReZeroTimeVector(time,start,1.0/3600);
  ReZeroTimeVector(val,0,1E-6);
  grEBPusher04 = new TGraph(time.size(),&time[0],&val[0]);
  ss.str("");





  TCanvas * c1 = new TCanvas("c1");
  gStyle->SetOptTitle(0);  
  TPad * padDRPusher = new TPad("padDRPusher","DRPusher",0.0,0.6,1.0,1.0);
  TPad * padDRGetter = new TPad("padDRGetter","DRGetter",0.0,0.4,1.0,0.6);
  TPad * padEBPusher = new TPad("padEBPusher","EBPusher",0.0,0.0,1.0,0.4);

  padDRPusher->SetBorderSize(0);
  padDRPusher->SetBorderMode(0);
  padDRGetter->SetBorderSize(0);
  padDRGetter->SetBorderMode(0);
  padEBPusher->SetBorderSize(0);
  padEBPusher->SetBorderMode(0);


  padDRPusher->SetRightMargin(padDRPusher->GetRightMargin()*0.3);
  padDRGetter->SetRightMargin(padDRGetter->GetRightMargin()*0.3);
  padEBPusher->SetRightMargin(padEBPusher->GetRightMargin()*0.3);

  padDRPusher->SetFillColor(kWhite);
  padDRGetter->SetFillColor(kWhite);
  padEBPusher->SetFillColor(kWhite);

  padDRPusher->SetBottomMargin(0);
  padDRGetter->SetBottomMargin(0);

  padDRGetter->SetTopMargin(0);
  padEBPusher->SetTopMargin(0);
  padEBPusher->SetBottomMargin(padEBPusher->GetBottomMargin()*2.0);

  padDRPusher->Draw();
  padDRGetter->Draw();
  padEBPusher->Draw();


  grDRPusher01->SetLineColor(kBlack);
  grDRPusher02->SetLineColor(kRed);
  grDRPusher03->SetLineColor(kBlue);
  grDRPusher04->SetLineColor(kViolet);

  grDRGetter01->SetLineColor(kBlack);
  grDRGetter02->SetLineColor(kRed);
  grDRGetter03->SetLineColor(kBlue);
  grDRGetter04->SetLineColor(kViolet);

  grEBPusher01->SetLineColor(kBlack);
  grEBPusher02->SetLineColor(kRed);
  grEBPusher03->SetLineColor(kBlue);
  grEBPusher04->SetLineColor(kViolet);


  //padDRPusher->cd()->SetLogy();  
  padDRPusher->cd();  

  //  grDRPusher01->GetYaxis()->SetRangeUser(-10,2000);
  grDRPusher01->GetYaxis()->SetRangeUser(-0.1,2.5);
  grDRPusher01->GetYaxis()->SetNdivisions(210);
  grDRPusher01->GetYaxis()->SetLabelSize(grDRPusher01->GetYaxis()->GetLabelSize()*2);
  grDRPusher01->GetYaxis()->SetTitleSize(grDRPusher01->GetYaxis()->GetTitleSize()*2);
  grDRPusher01->GetYaxis()->SetTitleOffset(grDRPusher01->GetYaxis()->GetTitleOffset()*0.5);
  grDRPusher01->GetYaxis()->SetTitle("Data Rate (MBps)");
  grDRPusher01->GetXaxis()->SetLabelColor(kWhite);
  grDRPusher01->GetXaxis()->SetTitleColor(kWhite);

  grDRPusher01->Draw("AL");

  grDRPusher02->Draw("L");
  grDRPusher03->Draw("L");
  grDRPusher04->Draw("L");

  TLegend * leg = new TLegend(0.75,0.15,0.95,0.65);
  ss << "Run #" << runNumber;
  leg->SetHeader(ss.str().c_str());
  ss.str("");
  leg->SetBorderSize(1);
  leg->SetShadowColor(0);
  leg->SetFillColor(0);
  leg->SetHeader("Reduction data");
  leg->AddEntry(grDRPusher01,"FEPC1#rightarrowDRPC","l");
  leg->AddEntry(grDRPusher02,"FEPC2#rightarrowDRPC","l");
  leg->AddEntry(grDRPusher03,"FEPC3#rightarrowDRPC","l");
  leg->AddEntry(grDRPusher04,"FEPC4#rightarrowDRPC","l");
  leg->Draw("SAME");


  padDRGetter->cd();
  grDRGetter01->GetYaxis()->SetRangeUser(-10,90);
  grDRGetter01->GetYaxis()->SetNdivisions(010);
  grDRGetter01->GetYaxis()->SetLabelSize(grDRGetter01->GetYaxis()->GetLabelSize()*2*(0.4/0.2));
  grDRGetter01->GetYaxis()->SetTitleSize(grDRGetter01->GetYaxis()->GetTitleSize()*2*(0.4/0.2));
  grDRGetter01->GetYaxis()->SetTitleOffset(grDRGetter01->GetYaxis()->GetTitleOffset()*0.5*(0.2/0.4));
  grDRGetter01->GetYaxis()->SetTitle("(KBps)");
  grDRGetter01->GetXaxis()->SetTickLength(grDRGetter01->GetXaxis()->GetTickLength()*2);
  grDRGetter01->Draw("AL");
  grDRGetter02->Draw("L");
  grDRGetter03->Draw("L");
  grDRGetter04->Draw("L");

  TLegend * leg2 = new TLegend(0.75,0.1,0.95,0.75);
  leg2->SetBorderSize(1);
  leg2->SetShadowColor(0);
  leg2->SetFillColor(0);
  leg2->SetHeader("Reduction decision");
  leg2->AddEntry(grDRGetter01,"DRPC#rightarrowFEPC1","l");
  leg2->AddEntry(grDRGetter02,"DRPC#rightarrowFEPC2","l");
  leg2->AddEntry(grDRGetter03,"DRPC#rightarrowFEPC3","l");
  leg2->AddEntry(grDRGetter04,"DRPC#rightarrowFEPC4","l");
  leg2->Draw("SAME");

  padEBPusher->cd();
  grEBPusher01->Draw("AL");
  grEBPusher01->GetYaxis()->SetRangeUser(-0.01,1.1);
  grEBPusher01->GetYaxis()->SetNdivisions(210);
  grEBPusher01->GetYaxis()->SetLabelSize(grEBPusher01->GetYaxis()->GetLabelSize()*2);
  grEBPusher01->GetYaxis()->SetTitleSize(grEBPusher01->GetYaxis()->GetTitleSize()*2);
  grEBPusher01->GetYaxis()->SetTitleOffset(grEBPusher01->GetYaxis()->GetTitleOffset()*0.5);
  grEBPusher01->GetYaxis()->SetTitle("(MBps)");
  grDRPusher01->GetXaxis()->SetTitle("Run Time (hours)");
  grEBPusher01->GetXaxis()->SetTitle("Run Time (hours)");
  grEBPusher01->GetXaxis()->SetLabelSize(grEBPusher01->GetXaxis()->GetLabelSize()*2);
  grEBPusher01->GetXaxis()->SetTitleSize(grEBPusher01->GetXaxis()->GetTitleSize()*2);
  grEBPusher02->Draw("L");
  grEBPusher03->Draw("L");
  grEBPusher04->Draw("L");

  TLegend * leg3 = new TLegend(0.75,0.25,0.95,0.725);
  leg3->SetBorderSize(1);
  leg3->SetShadowColor(0);
  leg3->SetFillColor(0);
  leg3->SetHeader("Event data");
  leg3->AddEntry(grDRPusher01,"FEPC1#rightarrowEBPC","l");
  leg3->AddEntry(grDRPusher02,"FEPC2#rightarrowEBPC","l");
  leg3->AddEntry(grDRPusher03,"FEPC3#rightarrowEBPC","l");
  leg3->AddEntry(grDRPusher04,"FEPC4#rightarrowEBPC","l");
  leg3->Draw("SAME");



  c1->Print("DataRates.pdf");
  

  delete c1;
}


