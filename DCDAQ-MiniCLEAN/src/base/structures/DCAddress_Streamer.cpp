#include <DCAddress.h>

static void Add(const void * in, size_t bytes,std::vector<uint8_t> & store)
{
  //Make room for copy
  if(bytes > 0)
    {
      store.resize(store.size() + bytes);
      memcpy((uint8_t *)&(store[0]) + (store.size() - bytes),
	     in,bytes);
    }
}

int DCMessage::DCAddress::MakeStream(std::vector<uint8_t> &stream) const
{
  uint32_t size = 0;
  stream.clear();

  //=========Add a total size marker=========
  Add(&size,sizeof(size),stream);
  //=========Add name=========
  size= name.size();
  Add(&size,sizeof(size),stream);
  Add(name.c_str(),size,stream);
  //=========Add sockaddr=========
  size=sizeof(addrMem);
  Add(&size,sizeof(size),stream);
  Add(addrMem,size,stream);

  //=========Update a total size marker=========
  size = stream.size();
  memcpy(&(stream[0]),&size,sizeof(size));

  return size;
}
void DCMessage::DCAddress::LoadStream(uint8_t * streamSource,
				      unsigned int size)
{
  //Setup our parser
  uint8_t * ptr = streamSource;
  uint32_t pos = 0;
  uint32_t TotalSize = 0;
  unsigned int subSize;
  uint32_t vectorSize;
  //Get total size.
  if(size >= sizeof(uint32_t))
    {
      memcpy(&TotalSize,streamSource,sizeof(uint32_t));
      ptr+=sizeof(uint32_t);
      pos+=sizeof(uint32_t);
    }
  else
    {
      fprintf(stderr,"DCAddress: Size is too small to start\n");
      Clear();
      return;
    }
  //Size is incorrect.
  if(TotalSize != size)
    {
      fprintf(stderr,"DCAddress: Size is wrong\n");
      Clear();
      return;
    }



  //=========Parse the name=========
  subSize = sizeof(uint32_t);	
  if(pos + subSize <= TotalSize)
    {
      vectorSize = 0;
      memcpy(&vectorSize,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      fprintf(stderr,"DCAddress: Name size word is wrong\n");
      Clear();
      return;
    }
  if(pos + vectorSize <= TotalSize)
    {
      if(vectorSize == 0)
	name.assign("");
      else
	name.assign((char*) ptr,vectorSize);
      ptr+= vectorSize;
      pos+= vectorSize;
    }
  else
    {
      fprintf(stderr,"DCAddress: Name size is wrong\n");
      Clear();
      return;
    }
  //=========Parse the sockaddr=========
  subSize = sizeof(uint32_t);	
  if(pos + subSize <= TotalSize)
    {
      vectorSize = 0;
      memcpy(&vectorSize,ptr,subSize);
      ptr+= subSize;
      pos+= subSize;
    }
  else
    {
      fprintf(stderr,"DCAddress: Addr size word is wrong\n %u %u %u %u",pos,subSize,pos+subSize,TotalSize);
      Clear();
      return;
    }
  if(pos + vectorSize <= TotalSize)
    {
      memcpy(addrMem,ptr,vectorSize);
      ptr+= vectorSize;
      pos+= vectorSize;
    }
  else
    {
      fprintf(stderr,"DCAddress: Addr size is wrong\n %u %u %u %u",pos,vectorSize,pos+vectorSize,TotalSize);
      Clear();
      return;
    }
}
