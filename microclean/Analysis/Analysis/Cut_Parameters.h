#ifndef CUTPARAMETERS
#define CUTPARAMETERS

#include <iostream>
#include <math.h>

#include "xmlParser.h"
#include "PEvent.h"


class Cut_Parameters 
{
 private:

 public:
  int Window;

  float * SPE;
  int   nSPE;

  float PMTLinearity1;
  float PMTLinearity2;
  
  int   PMTTiming;
  int   TOFmin;
  int   TOFmax;
  
  float PMT2psa0;
  float PMT2psa1;
  float PMT2psa2;
  float PMT2psa3;

  float PMT2psaV20;
  float PMT2psaV21;

  float PMT2min;
  float PMT2max;

  float TBRatioMin;
  float TBRatioMax;
  float FastMin;
  float FastMax;

  bool  bPMTLinearity1;
  bool  bPMTLinearity2;

  bool  bPMTTiming;
  bool  bTOFmin;
  bool  bTOFmax;
  
  bool  bPMT2psa;
  bool  bPMT2psaV2;
  bool  bPMT2;

  bool  bTBRatioMin;
  bool  bTBRatioMax;
  bool  bFastMin;
  bool  bFastMax;

  Cut_Parameters();
  virtual ~Cut_Parameters() {};
  // i = windows to use; SPEs = array of singlephotoelectoncs; n = # of SPEs
  int SetCuts(XMLNode *xMainNode,int i,float * SPEs,int n); 
  bool PassCuts(PEvent * pevent);
};

#endif
