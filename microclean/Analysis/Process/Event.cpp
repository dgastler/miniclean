#include <iostream>
#include <TDirectory.h>
#include <TProcessID.h>

#include "Event.h"


using namespace std;


ClassImp(EventHeader)
ClassImp(Event)
ClassImp(RawWave)



//______________________________________________________________________________
Event::Event() : fIsValid(kFALSE)
{
   // Create an Event object.
   // When the constructor is invoked for the first time, the class static
   // variable fgTracks is 0 and the TClonesArray fgTracks is created.
  RawWaves = new TClonesArray("RawWave",0);
    //RawWaves = new TObjArray(0);
  //  RawWaves->BypassStreamer(kFALSE);
  iRaw = 0;
  iRealRaw = 0;
  
  
}

//______________________________________________________________________________
Event::~Event()
{
  fIsValid = false;
  char temp[30];
  SetHeader(0,0,temp,30);
  

  iRealRaw = 0;
  iRaw = 0;
  RawWaves->Delete();
  RawWaves = 0;

}

//______________________________________________________________________________

void Event::Clear(Option_t *option)
{
  
  fIsValid = false;
  char temp[30];
  SetHeader(0,0,temp,30);
  iRaw = 0;
  RawWaves->Clear("C");
}

//______________________________________________________________________________
/*
void Event::Reset()
{
// Static function to reset all static objects for this event
//   fgTracks->Delete(option);
  RawWaves->Delete();
  RawWaves = 0;
  iRealRaw = 0;
}
*/
//______________________________________________________________________________
RawWave*  Event::GetWave(Int_t i)
{
  if(i < iRaw)
    {
      RawWave *wave = (RawWave *) RawWaves->AddrAt(i);
      //RawWave *wave = (RawWave *) RawWaves->At(i);

      return(wave);
    }
  else
    {
      cerr << "out of Range\n";
    }
  return(0);
}


//______________________________________________________________________________


RawWave* Event::AddWave()
{
  //  TClonesArray &waves = *RawWaves;
  
  TObjArray &waves = *RawWaves;
  RawWave *wave =0;
  if(iRaw < iRealRaw)
    {
      iRaw++;
      //     wave = waves[iRaw];
    }
  else
    {
      wave = new(waves[iRaw++]) RawWave();
      //wave = new RawWave();
      //      RawWaves->AddLast(wave);
      iRealRaw++;
    }
  return wave;
}
void Event::SetID(Int_t i,Int_t iID)
{
  RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
  //RawWave *wave = (RawWave*) &RawWaves[i];//->At(i);
  wave->ID(iID);
}
void Event::SetTrig(Int_t i,Int_t iTrig)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  wave->Trig(iTrig);
}
void Event::SetVOffset(Int_t i,Float_t fVOffset)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  wave->Voffset(fVOffset);
}
void Event::SetVMult(Int_t i,Float_t fVMult)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  wave->VMult(fVMult);
}
void Event::SetTick(Int_t i,Float_t fTick)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  wave->Tick(fTick);
}
Int_t Event::GetSize(Int_t i)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->Size());
}




// "i" is the channel position in array
void Event::SetWaveform(Int_t i,Int_t n, UChar_t *fY)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  wave->Build(fY,n);
}
void Event::SetWaveform(Int_t i,Int_t n, UInt_t *fY)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  wave->Build(fY,n);
}


Int_t        Event::GetID(Int_t i)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->ID());
}

Int_t        Event::GetTrig(Int_t i)
{
  RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
  //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->Trig());
}
Float_t      Event::GetVOffset(Int_t i)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->Voffset());
}
Float_t      Event::GetVMult(Int_t i)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->VMult());
}
Float_t      Event::GetTick(Int_t i)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->Tick());
}

UChar_t     Event::GetPoint(Int_t i, Int_t n)
{
    RawWave *wave = (RawWave*) RawWaves->AddrAt(i);
    //RawWave *wave = (RawWave*) &RawWaves[i];
  return(wave->Waveform(n));
}













//______________________________________________________________________________
void Event::SetHeader(Int_t i, Int_t run,Char_t *aDate,Int_t aDate_size)
{
   fEvtHdr.Set(i, run,aDate,aDate_size);
}
//______________________________________________________________________________
void EventHeader::Set(Int_t i, Int_t r, Char_t * d, Int_t n)
{
  iEvent = i;
  iRun = r;
  for(int j = 0; j < DATESIZE && j < n; j++)
    {
      aDate[j] = d[j];
    }
}


















//-------------------------------------------------------------------------
void RawWave::Build(UChar_t *fData,Int_t N,Float_t fvoffset,Float_t fvmult,Float_t ftick,Int_t ID)
{
  iSize = N;
  iID = ID;
  fVOffset =  fvoffset;
  fVMult =    fvmult;
  fTick =     ftick;
  
  Wave = fData;
}
//-------------------------------------------------------------------------
void RawWave::Build(UChar_t *fData,Int_t N)
{
  iSize = N;
  Wave = fData;
  
}
//-------------------------------------------------------------------------
void RawWave::Build(UInt_t *fData,Int_t N)
{
  
  iSize = N*4;
  //This converts the 32bit int into 4 8 bit ints. Up to the existing size of Wave[].
  //  Wave = (UChar_t *) fData;
  Wave = reinterpret_cast<u_int8_t*> (fData);
}

//-------------------------------------------------------------------------
UChar_t RawWave::Waveform(Int_t i)
{
  if(i < iSize)
    {
      return(Wave[(i/4)*4 + 3 - i%4]);
    }
  cerr << "Wave(i) out of range" << endl;
  return(-1);
}
//--------------------------------------------------------------------------------
void RawWave::Waveform(Int_t i, UChar_t data)
{
  if(i < iSize)
    {
      Wave[(i/4)*4 + 3 - i%4] = data;
    }
  cerr << "Wave(Int_t,Char_t) out of range" << endl;
}

void RawWave::Clear(Option_t *option)
{
  Wave     = 0;
  iSize    = 0;
  iID      = 0;
  iTrig    = 0;
  fVOffset = 0;
  fVMult   = 0;
  fTick    = 0;
}
