#ifndef __LOCKEDVECTORHASH__
#define __LOCKEDVECTORHASH__

#include <pthread.h>

#include <vector>

#include <LockCleanup.h>

//==========================================================================
//Functions you care about:
//
//SetupVector(size,badValue,lockValue):
//                   Setups up the size of the vector and specifies
//                   what will be returned for an empty slot (badValue)
//                   or an in-use value (lockValue).
//
//Get(key, Type, wait):  
//                   Gets the Type with key.  Waits if specified
//
//Put(key, Type, wait):  
//                   Puts Type in key position.  Waits if specified.
//
//Size():            Returns vector size
//
//Clear():           Clears all data from vector and removes all locks.
//
//Clear(key, wait):  Clears data @ key and removes lock.  Wait if specified.
//
//ShutdownWait():    Removes the wait option on Get/Put (used when shutting down DCDAQ)
//
//WakeUp():          Uses condition variable to wake anyone stuck waiting on data.
//                   Used after ShutdownWait() when shutting down DCDAQ.
//==========================================================================


template< class T >
class LockedVectorHash: public LockedObject
{
 public:
  LockedVectorHash()
    {
      //Allocate this object's mutex lock.
      if(pthread_mutex_init(&mutex,NULL))
	{
	  fprintf(stderr,"Error creating mutex!\n");
	}
      //Allocate this object's mutex lock.
      if(pthread_cond_init(&cond,NULL))
	{
	  fprintf(stderr,"Error creating condition variable!\n");
	}
    }
  ~LockedVectorHash(){Clear();};
  void Setup(uint16_t size, T _badValue, T _lockValue) 
  {
    //Set return value for no data.
    badValue = _badValue;
    //Set return value for locked data.
    lockValue = _lockValue;
    //Allocate data for the vectors
    for(uint16_t i = 0; i < size;i++)
      {
	//Set data vector to the badValue value.
	data.push_back(badValue);
	//Set each block as not locked.
	lock.push_back(false);
      }
  };
  
  bool Get(uint16_t key, T & Obj, bool Wait)
  {    
    //Shutdown is used to shut down this object.
    if(Wait && (!GetShutdown())) //Shutdown stops a WAIT
      {
	//Get the mutex.
	pthread_mutex_lock(&mutex);
      }
    else // Don't wait
      {
	//try to get the mutex.
	int status = pthread_mutex_trylock(&mutex);
	if(status == EBUSY) // Mutex is already locked
	  {
	    Obj = badValue;
	    return(false); //Not waiting for unlock
	  }
      }
    
    //We now have the mutex!

    //return value
    bool ret = false;
    
    //Add a pthread cleanup routine in case pthread_cancel is called.
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "{"s
    pthread_cleanup_push(CleanupMutex,&mutex);    

    //Get the hash value for our key
    uint16_t hash = hashFunction(key);
    //Check if the object is locked
    if(lock[hash])
      {
	Obj = lockValue;
	ret = false;
      }
    else //No lock
      {
	Obj = data[hash];
	lock[hash] = true;
	ret = true;
      }
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "}"s
    pthread_cleanup_pop(1);
    return(ret);
  };
  
  bool Put(uint16_t key, T & Obj, bool wait)
  {
    if(wait)
      {
	//Get the mutex.
	pthread_mutex_lock(&mutex);
      }
    else // Don't wait
      {
	//try to get the mutex.
	int status = pthread_mutex_trylock(&mutex);
	if(status == EBUSY) // Mutex is already locked
	  {
	    return(false); //Not waiting for unlock
	  }
      }


    //Return value
    bool ret = false;
    
    //We now have the mutex!
    //Add a pthread cleanup routine in case pthread_cancel is called.
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "{"
    pthread_cleanup_push(CleanupMutex,&mutex);

    //Get the hash value for our key
    uint16_t hash = hashFunction(key);
    //Set data
    data[hash] = Obj;
    //Make sure the object's lock is off
    lock[hash] = false;
    //Set T to badValue, so the user doesn't know what it is anymore.  (if they made a copy they can go to hell)
    Obj = badValue;    
    ret = true;
    //signal so that if anyone is in cond_wait in Get() they wake up
    pthread_cond_signal(&cond);
    //release the mutex.
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "}"s
    pthread_cleanup_pop(1);
    return(ret);
  };

  virtual bool Clear(uint16_t key, bool wait)
  {
    return(Put(key,badValue,wait));
  };

  virtual bool Clear(bool wait = true)
  {
    if(wait)
      {
	//Get the mutex.
	pthread_mutex_lock(&mutex);
      }
    else // Don't wait
      {
	//try to get the mutex.
	int status = pthread_mutex_trylock(&mutex);
	if(status == EBUSY) // Mutex is already locked
	  {
	    return(false); //Not waiting for unlock
	  }
      }

    //We have the mutex

    //Add a pthread cleanup routine in case pthread_cancel is called.
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "{"s
    pthread_cleanup_push(CleanupMutex,&mutex);
    
    for(uint16_t i = 0; i < data.size();i++)
      {
	//Reset all data to badValue.
	data[i] = badValue;
	//Unlock everything.
	lock[i] = false;
      }
    
    //release the mutex.
    //WATCH OUT FOR THESE FUNCTIONS!  THEY ARE EVIL!  They are macros with unmatched "}"s
    pthread_cleanup_pop(1);
    return(true);
  };
  
  uint16_t GetSize(){return(data.size());};
 private:
  //Hash functions used to turn an integer into a hash integer.
  uint16_t hashFunction(uint16_t i){return(i%data.size());};

  std::vector< T >  data;   //Vector of data
  T                 badValue; //Value to return if there is no data for our hash.
  T                 lockValue;//Value to return if the data you are looking for is held by someone else.
  std::vector<bool> lock;  //Vector of locks for this data
  pthread_mutex_t   mutex; //Mutex lock for vector access
  pthread_cond_t    cond; //Condition variable for the mutex
};

#endif
