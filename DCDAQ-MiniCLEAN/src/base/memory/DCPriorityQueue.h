#ifndef __DCPRIORITYQUEUE__
#define __DCPRIORITYQUEUE__

#include <iostream>
#include <cstdio>
#include <cstring> //for strerr

#include <queue>

//ObjectModels
#include <LockedObject.h>
#include <NotLockedObject.h>

//SelectModels
#include <PipeSelect.h>
#include <NBPipeSelect.h>
#include <POSIXQSelect.h>
#include <NoSelect.h>
#include <SelectValues.h>


//==========================================================================
//Functions you care about:
//
//Get( Type, wait):  Gets the next Type.  Waits if specified 
//                   returns true if a message was returned in Type
//                   returns false if no message to return.  returns bad Type
//
//Add( Type, wait):  Adds the next Type.  Waits if specified
//
//SingleReadFD(wait):Call this to do one read on the queue's pipe.
//                   This is helpful for when you call Get, and then
//                   want to Add the result back to the end of the queue. 
//
//GetSize():            Returns queue size
//
//Clear():           Clears all data from queue
//
//ShutdownWait():    Removes the wait option on queues (used when shutting down DCDAQ)
//
//WakeUp():          Uses condition variable to wake anyone stuck waiting on data.
//                   Used after ShutdownWait() when shutting down DCDAQ.
//Setup(Type):       Sets the value returned by Get if there is not data to be returned.
//

//==========================================================================

template< 
  class T , 
  //  class size_t (* Priority) (T&),
  class SelectModel = PipeSelect,
  class ObjectModel = LockedObject> 
class DCPriorityQueue: public SelectModel
{
public:
  DCPriorityQueue(size_t (* fT) (T&)):SelectModel(),Priority(fT)
  {
    //Setup the Queue
    badValueSet = false;
    addCount = 0;
    printStuff = false;
    entryCount = 0;
    maxEntryCount = (512*8*16)/sizeof(uint8_t)  -1 ;
  };
  
  virtual ~DCPriorityQueue()
    {
      Clear(true);
    };

  void SetPrintStuffTrue()
  {
    printStuff = true;
  }

  void ShutdownWait()
  {
    Lock.ShutdownLockWait();
  }

  void WakeUp()
  {
    Lock.WakeUpSignal();
  }

  void Setup(T _badValue)
  {
    badValue = _badValue;
    badValueSet = true;
  };

  size_t GetSize()
  {
//    size_t size = 0;
//    for(typename std::vector< std::deque<T> >::iterator it = Queues.begin();
//	it < Queues.end();
//	it++)
//      {
//	size+=it->size();
//      }
//    return(size);
    return entryCount;
  };

  uint64_t GetAddCount(){return addCount;};

  bool Get( T &Obj,bool Wait = true)
  {
    bool ret = false;
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCQUEUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCPriorityQueue badValue not set!\n");
      }
#endif
    
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	Obj = badValue;
	return(false);
      }

    //======================
    //We now have the mutex!
    //======================

    //if we are waiting for a new block wait on a condition variable.
    if(Wait)
      {
	//	while((!GetSize()) && Lock.ConditionWait())
	while((!entryCount) && Lock.ConditionWait())
	  {
	    if(printStuff)
	      {
		//printf("This is the free queue!\n");
	      }
	    //We are in condition wait with the understanding
	    //that a ShutdownLockWait() and a wakeup will
	    //cause this loop to terminate
	  }
      };    
    //    if(GetSize()) //The queue isnt' empty, so process an entry
    if(entryCount) //The queue isnt' empty, so process an entry
      {
	//Read from select model
	if(SelectValues::OK==SelectModel::Read())
	  {
	    //REad worked
	    //Set obj to the element at the front of the queue
	    typename std::vector< std::deque<T> >::reverse_iterator it = Queues.rbegin();
	    for(;
		it < Queues.rend();
		it++)
	      {
		if(it->size() > 0)
		  {
		    Obj = it->front();
		    //Remove the front object.
		    it->pop_front();
		    entryCount--;
		    ret = true;
		    break;
		  }
	      }
	  }	
	else
	  {
	    //Read failed
	    ret = false;
	    Obj=badValue;
	  }
      }
    else //The queue was empty, return badValue and false;
      {
	//Set the object to badValue;
	Obj = badValue;
	ret = false;
      }
    //Unlock 
    Lock.UnlockObject();
    return ret;
  };

  bool Add(T &Obj,bool Wait = true)
  {
    bool ret;
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCQUEUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCPriorityQueue badValue not set!\n");
      }
#endif

    //Lock
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	return false;
      }
    
    //======================
    //We now have the mutex!
    //======================

    //Add 1 to the add count
    addCount++;
    //Add 1 to the entry count
    entryCount++;

    //Add obj to the end of the queue
    T PushObj = Obj;   //Make a copy so the Adder can't
                       //change it later.
    
    size_t priorityIndex = Priority(Obj);

    //Resize Queue vector to hold this priority
    if(priorityIndex >= (Queues.size()))
      {
	Queues.resize(priorityIndex+1);
      }
    Queues[priorityIndex].push_back(PushObj);

    //Check if there is room for a new entry
    if((entryCount < maxEntryCount) || Wait)
      {
	//Write to our select model
	if(SelectModel::Write()==SelectValues::OK)
	  {
	    //Everything worked!
	    //Now that the Object has been added to the queue, return back to the user the badValue.
	    Obj = badValue;
	    ret = true;
	  }
	else
	  {
	    //Failure in the pipe
	    //We didn't really add, so undo the above
	    addCount--;
	    entryCount--;
	    Queues[priorityIndex].pop_back();
	    ret = false;
	  }
      }
    else
      {
	ret = false;
	//Search for a message that is a lower priority than the current add value
	//Start with the lowest priority value
	for(size_t iQueue = 0;iQueue < priorityIndex;iQueue++)	
	  {	    
	    if(Queues[iQueue].size() > 0)
	      {
		//We found an event of lower priority, destroying the oldest
		Queues[iQueue].pop_front();
		ret = true;
		Obj = badValue;
		break;
	      }
	  }
	//If we didn't find an entry of a lower priority, drop this message
	if(!ret)
	  {
	    addCount--;
	    entryCount--;
	    Queues[priorityIndex].pop_back();
	  }
      }
    
        
    //signal so that if anyone is in cond_wait in Get() they wake up
    Lock.WakeUpSignal();
    //release the mutex.
    Lock.UnlockObject();
    return ret;
  };



  bool Clear(bool wait = true)
  {
    //Lock
    if(!Lock.LockObject(wait))
      {
	//Not waiting for unlock
	return(false);
      }
    
    //We now have the mutex!
    
    //clear all objects in the queue
    for(typename std::vector< std::deque<T> >::iterator it = Queues.begin();
	it < Queues.end();
	it++)
      {
	while(it->size())
	  {
	    //Remove an entry
	    it->pop_front();
	    //Read from our Select model
	    SelectModel::Read();
	  }
      }
	  
    //no nead to signal the condition variable since no one wants to know the
    //queue is empty. 
    
    //unlock mutex
    Lock.UnlockObject();
    return(true);
  };

  const T GetBadValue(){return(badValue);};
  
 private:  
  ObjectModel Lock;
  size_t (*Priority) (T&);
  std::vector<std::deque< T > > Queues;   //The free queue
  T badValue;
  volatile bool badValueSet;
  uint64_t addCount;
  bool printStuff;
  size_t entryCount;
  size_t maxEntryCount;

  DCPriorityQueue();
};

#endif
