--Original, slower, 8-bit, Verilog version by Jeremy Mans.
--Better, 32-bit, VHDL version by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ip_checksum_32bit is Port(
	CLK : in STD_LOGIC;
	VALID : in STD_LOGIC_VECTOR(2 downto 0);
	RESET : in STD_LOGIC_VECTOR(1 downto 0);
	CHECKSUM : out unsigned(15 downto 0);
	DATA : in unsigned(31 downto 0));
end ip_checksum_32bit;

architecture Behavioral of ip_checksum_32bit is
	signal csum_intl : unsigned(17 downto 0) := "00" & x"0000";
	signal csum_add : unsigned(17 downto 0) := "00" & x"0000";
	signal frag_intl : unsigned(17 downto 0) := "00" & x"0000";
	signal frag_add : unsigned(17 downto 0) := "00" & x"0000";
begin
	csum_add <= ("00" & csum_intl(15 downto 0)) + (x"0000" & csum_intl(17 downto 16)) + ("00" & 
		DATA(15 downto 0)) + ("00" & DATA(31 downto 16)) when VALID(2 downto 1) = "11" else ("00" & 
		csum_intl(15 downto 0)) + (x"0000" & csum_intl(17 downto 16)) + ("00" & DATA(31 downto 16)) 
		when VALID(2 downto 1) = "10" else ("00" & csum_intl(15 downto 0)) + (x"0000" & 
		csum_intl(17 downto 16)) + ("00" & DATA(15 downto 0)) when VALID(2 downto 1) = "01" else (
		"00" & csum_intl(15 downto 0)) + (x"0000" & csum_intl(17 downto 16));
	frag_add <= ("00" & frag_intl(15 downto 0)) + (x"0000" & frag_intl(17 downto 16)) when VALID(0) 
		= '0' or VALID(2 downto 1) = "00" else ("00" & frag_intl(15 downto 0)) + (x"0000" & 
		frag_intl(17 downto 16)) + ("00" & DATA(15 downto 0)) + ("00" & DATA(31 downto 16)) when 
		VALID(2 downto 1) = "11" else ("00" & frag_intl(15 downto 0)) + (x"0000" & frag_intl(17 downto 16)) 
		+ ("00" & DATA(31 downto 16)) when VALID(2 downto 1) = "10" else ("00" & frag_intl(15 downto 0)) 
		+ (x"0000" & frag_intl(17 downto 16)) + ("00" & DATA(15 downto 0));
	CHECKSUM <= not (csum_intl(15 downto 0) + (x"000" & "00" & csum_intl(17 downto 16)));

	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			if RESET = "11" then
				csum_intl <= "00" & x"0000";
				frag_intl <= "00" & x"0000";
			elsif RESET = "01" then
				csum_intl <= frag_intl;
			else
				csum_intl <= csum_add;
				frag_intl <= frag_add;
			end if;
		end if;
	end process;
end Behavioral;
