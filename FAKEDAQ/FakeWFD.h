/*==Fake WFD==============
  This class has a list of PMTs that it pulls out of the RAT event and
  converts to the raw CAEN V1720 data format.
  After parsing it can return a pointer to the raw CAEN event for writing.
==Fake WFD==============*/
#ifndef __FAKEWFD__
#define __FAKEWFD__

#include <iostream>

#include <RAT/DS/Root.hh>

class FakeWFD
{
 public:
  FakeWFD(){};
  ~FakeWFD(){};
  const uint32_t * GetData(uint32_t & size){};
  int Process(RAT::DS::Root * ds);
  Setup(int,std::vector<int>){};
  int Setup(uint32_t _boardID, vector<int> _PMTID);
 private:
  //Raw data
  std::vector<uint32_t> data;
  bool validData;
  uint32_t boardID;
  std::vector<int> PMTID;
  uint8_t dataFormat; 
  //data format format
  //  bit     meaning
  //   0      ZLE on/off
  //   1      2.5packing on/off
  //   2      data format valid true/false

  //Header helper functions

  int FillTriggerTimeTag(time_t triggerTime);
  int FillEventCounter(uint32_t eventCounter);
  int FillChannelMask();
  int FillBoardID();
  int FillEventSize();
  int FillHeaderWord();
  int FillDataFormat();
  
  int ProcessWaveforms(RAT::DS::Root *ds);
  int Process_25_Packing(RAT::DS::RawWaveform * rawWave);
  int Process_20_Packing(RAT::DS::RawWaveform * rawWave);
  RAT::DS::PMT * FindPMT(RAT::DS::Root * ds, int id);
  int ProcessZLEWindows(RAT::DS::PMT * pmt, int samplesPer64BitWord);


};

#endif
