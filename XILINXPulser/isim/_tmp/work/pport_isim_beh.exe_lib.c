/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                         */
/*  \   \        Copyright (c) 2003-2007 Xilinx, Inc.                 */
/*  /   /        All Right Reserved.                                  */
/* /---/   /\                                                         */
/* \   \  /  \                                                        */
/*  \___\/\___\                                                       */
/**********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *UNISIM_P_0947159679;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_1242562249;
char *IEEE_P_2717149903;
char *STD_TEXTIO;

int isim_run(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    std_textio_init();
    ieee_p_2717149903_init();
    unisim_p_0947159679_init();
    unisim_a_2111758543_2393828108_init();
    work_a_2397585485_3212880686_init();
    unisim_a_0682165616_1492584465_init();
    work_a_0850468234_3212880686_init();
    unisim_a_0622721826_2014779070_init();
    unisim_a_3828308815_1222000726_init();
    unisim_a_2211589156_2274105955_init();
    work_a_0032339771_3212880686_init();
    unisim_a_4207005572_0559031411_init();
    unisim_a_2661327437_0605893366_init();
    unisim_a_2312877582_0635394241_init();
    unisim_a_3988446151_0546328132_init();
    unisim_a_2562466605_1496654361_init();
    work_a_0795811483_3212880686_init();
    unisim_a_2782630213_1361109519_init();
    work_a_2996315140_3212880686_init();
    work_a_2466425713_3212880686_init();
    unisim_a_2988077518_2751630626_init();
    unisim_a_2472646025_1397528790_init();
    unisim_a_2867636556_1359619727_init();
    unisim_a_4147737283_2967259552_init();
    unisim_a_1801614988_1818890047_init();
    work_a_0569237888_3212880686_init();
    work_a_1407708527_3212880686_init();


    xsi_register_tops("work_a_1407708527_3212880686");

    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    IEEE_P_2717149903 = xsi_get_engine_memory("ieee_p_2717149903");
    STD_TEXTIO = xsi_get_engine_memory("std_textio");

    return xsi_run_simulation(argc, argv);

}
