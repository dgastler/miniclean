#ifndef __DCMESSAGE__
#define __DCMESSAGE__

#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <string.h> //for memcpy
#include <boost/algorithm/string/split.hpp> //for split
#include <boost/algorithm/string/classification.hpp> //for is_any_of



//for address types
#include <arpa/inet.h>

#include <DCMessageTypes.h>

#include <DCAddress.h>

//For helpful statistics/error structures (used for data)
#include <MessageStructures.h>

namespace DCMessage 
{  
  //This class exists to allow message passing with data 
  //of any size and type.   It is up to you to know what
  //you are putting in it and what you are getting back.
  //If you are using C STYLE STRINGS you can't forget to
  //address the null termination of your string. 
  class Message
  {
  public:
    //===================================
    //Constructors
    //===================================
    Message(){Clear();};
    Message(xmlNode * setupNode);
    Message(Message const & message){CopyObj(message);};
    Message & operator=(Message const & rhs){CopyObj(rhs);return *this;};

    //===================================
    //User interfaces
    //===================================
    bool Setup(xmlNode * setupNode);

    //Print pretty
    std::string PrintPretty();

    //Message type
    void SetType(DCMessageType _type){type = _type; SetTime(time(NULL));};
    DCMessageType GetType(){return(type);}
    
    //Set source and destination
    void SetSource(std::string const & name = DCAddress::BroadcastName,
		   struct sockaddr const * addr = NULL,
		   size_t addrSize = 0);
    
    void SetDestination(std::string const & name = DCAddress::BroadcastName,
			struct sockaddr const * addr = NULL,
			size_t addrSize=0);

    const DCAddress & GetSource() const;
    void SetSource(DCAddress const & address);

    const DCAddress & GetDestination() const;
    void SetDestination(DCAddress const & address);

    //Message time stamp
    void SetTime(time_t _timeStamp){timeStamp = _timeStamp;};
    DCtime_t GetTime(){return(timeStamp);};

    //============Direct interface to data============
    //Message data

    //Fill a passed vector with the data
    std::vector<uint8_t> GetData(){return Data;};
    template<class T>
    bool GetData(T & t)
    {      
      if((Data.size() < sizeof(T)) ||
	 (Data.size() == 0))
	return false;
      memcpy(&t,&Data[0],sizeof(T));
      return true;
    };

    bool GetData(void * ptr,size_t size)
    {
      if((Data.size() < size) ||
	 (Data.size() == 0))
	return false;
      memcpy(ptr,&Data[0],size);
      return true;
    };

    template<class T>
    void SetData(T const & t)
    {
      Data.resize(sizeof(T));
      memcpy(&Data[0],&t,Data.size());
    };
    void SetData(void const * inData,size_t bytes){Set(inData,bytes,Data);};


    //============Struct interface to data============
    //Set data using the struct T
    template<class T>
    void SetDataStruct(const T t){Set(&t,sizeof(t),Data);}
    //Get data assuming it is of type T
    template<class T>
    bool GetDataStruct(T & t)
    {
      //Check if the size is correct
      if(sizeof(t) != Data.size())
	return false;
      T * ptr = (T*) &(Data[0]);
      T referenceCopyOfType;
      //Check the magic number (type)
      if(referenceCopyOfType.type != ptr->type)
	return false;
      //Every check is positive so copy the message data and return
      memcpy(&t,&Data[0],sizeof(t));
      return true;
    }
    
    //============Streaming============    
    int StreamMessage(std::vector<uint8_t> &stream);        
    void SetMessage(uint8_t * streamSource,unsigned int size);

    //===================================
    //Constructor clear function
    //===================================
    void Clear();    

  private:
    //===================================
    //Copy function
    //===================================
    void CopyObj(Message const &rhs);
    //===================================
    //Set internal data
    //===================================
    void Set(void const * in,size_t bytes,std::vector<uint8_t> & store);
    void Add(void const * in, size_t bytes,std::vector<uint8_t> & store);
    //===================================
    //Data members
    //===================================
    DCMessageType type;
    DCtime_t timeStamp;
    std::vector<uint8_t> Data;

    DCAddress Destination;
    DCAddress Source;
  };
}
#endif
