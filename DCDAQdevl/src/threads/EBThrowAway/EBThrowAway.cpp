#include <EBThrowAway.h>

EBThrowAway::EBThrowAway(std::string Name)
{
  SetName(Name);
  SetType(std::string("EBThrowAway"));

  //EBPC memorymanager
  EBPC_mem = NULL;
  
  //statistics
  lastTime = time(NULL);
  currentTime = time(NULL);
  
  timeStepProcessedEvents = 0;
}

bool EBThrowAway::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager)
{
 
  //================================
  //Get the EBPC memory manager
  //================================
  if(!FindMemoryManager(EBPC_mem,SetupNode,MemManager))
    {
      return(false);
    }

  Ready = true;
  return(true);
}
