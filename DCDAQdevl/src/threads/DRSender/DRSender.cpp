#include <DRSender.h>

DRSender::DRSender(std::string Name)
{
  Ready = false;
  SetType("DRSender");
  SetName(Name);
  Loop = true;      //This thread should start asap
                    //This is so the network connections can be made
  
  //Default packet manager string.
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
}

DRSender::~DRSender()
{
  //Clean up what's left of our network connections.
  //this should already have happened in ThreadCleanUp, but we 
  //are making sure.
  server.Clear();
}

void DRSender::ThreadCleanUp()
{
  //Clean up what's left of our network connections.
  server.Clear();
}

bool DRSender::Setup(xmlNode * SetupNode,
			  std::map<std::string,MemoryManager*> &MemManager)
{
  bool ret = true;
  //==============================================================
  //Load the DRPC memory manager
  //==============================================================
  if(!FindMemoryManager(DRPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
 
  //==============================================================
  //Parse data for server config and expected clients.
  //==============================================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  if(ret && server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.serverSocketFD);
      SetupSelect();
    }
  else
    {
      ret = false;
    }


  //If everything in setup worked, then set Ready to true;
  if(ret == true)
    {
      Ready = true;
    }
  return(ret);
}

void DRSender::PrintStatus()
{
  DCThread::PrintStatus();
  for(size_t i = 0; i <server.Size();i++)
    {
      if(server[i].thread != NULL)
	{
	  server[i].thread->PrintStatus();
	}
    }
}
