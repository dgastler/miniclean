#ifdef __RATDS__
#include <EBPCAssembler.h>

EBPCAssembler::EBPCAssembler(std::string Name)
{
  SetType("EBPCAssembler");
  SetName(Name);
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>300000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>300000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  reductionDataRate.resize(ReductionLevel::COUNT,0);
}

void EBPCAssembler::ThreadCleanUp()
{
  //Close our server listening socket
  //Close all of our connections to remote DCDAQs 
  server.Clear();
}

bool EBPCAssembler::Setup(xmlNode * SetupNode,
			  std::vector<MemoryManager*> &MemManager)
{  
  Ready = false;

  //================================
  //Get the EBPC memory manager
  //================================
  if(!FindMemoryManager(EBPC_mem,SetupNode,MemManager))
    {
      return(false);
    }

  RATCopyQueue * monitorQueue = NULL;
  if(FindMemoryManager(monitorQueue,SetupNode,MemManager))
    {
      eventMonitor.Setup(monitorQueue);
    }


  //==============================================================
  //Get DetectorINfo data
  //==============================================================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::PMT_COUNT |
			 InfoTypes::EVENT_START|
			 InfoTypes::EVENT_END |
			 InfoTypes::EVENT_PROMPT
			 );
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      detectorInfo.PrintMissingTypes(neededInfo);
      std::stringstream ss;
      ss << "DetectorInfo not complete ("  
	 << hex << parsedInfo << ":" 
	 << hex << neededInfo << ")";
      PrintError(ss.str());
      return false;
    }

  //================================
  //Parse data for server config and expected clients.
  //================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  if(server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.GetSocketFD());
      SetupSelect();
      Ready=true;
    }
  else
    {
      PrintError("Server setup failed.\n");
      return false;
    }

  EBPCEventCount = 0;

  for(int i = 0; i < ReductionLevel::COUNT;i++)
    {
      std::stringstream ss;
      ss << "redLevel_" << i << "_TotalQ";
      TotalQHist[i] = new TH1F(ss.str().c_str(),
				       ss.str().c_str(),
				       1000,0,1E6);
      fprintf(stdout,"Created histogram %s\n",ss.str().c_str());
      if(TotalQHist[i] == NULL)
	return false;
    }

  //ZLEWaveformQ = new TH1F("ZLEWaveformQ","ZLE Waveform Total Q",100,0,1000000);
  //ZLEIntegralQ = new TH1F("ZLEIntegralQ","ZLE Integral Total Q",100,0,1000000);
  //PromptTotalQ = new TH1F("PromptTotalQ","Prompt/Total Total Q",100,0,1000000);

  return true;
}

void EBPCAssembler::ProcessChildMessage(size_t iConn)
{
  if(iConn >= server.Size())
    return;

  DCMessage::Message message = server[iConn].GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      std::stringstream ss;
      ss << "Lost connection with " << server[iConn].GetRemoteAddress().GetAddrStr();
      PrintError(ss.str().c_str());

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::DISCONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      DeleteConnection(iConn);
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);      
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}

bool EBPCAssembler::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void EBPCAssembler::ProcessTimeout()
{
  //Get current Time                                                            
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t TimeStep = currentTime-lastTime;
      StatMessage::StatRate ssRate;
      DCMessage::Message message;
      //Send all reduction level event rates
      for(size_t iLevel = 0;
          iLevel < reductionDataRate.size();
          iLevel++)
        {
          message.SetType(DCMessage::STATISTIC);
          message.SetDestination();

          ssRate.sBase.type = StatMessage::DATA_RATE;
          ssRate.count = reductionDataRate[iLevel];
          ssRate.sInfo.time = currentTime;
          ssRate.sInfo.interval = TimeStep;
          ssRate.sInfo.subID = SubID::BLANK;

          switch (iLevel)
            {
            case ReductionLevel::CHAN_FULL_WAVEFORM :
              ssRate.sInfo.level = Level::CHAN_FULL_WAVEFORM;
              break;
            case ReductionLevel::CHAN_ZLE_WAVEFORM  :
              ssRate.sInfo.level = Level::CHAN_ZLE_WAVEFORM;
              break;
            case ReductionLevel::CHAN_ZLE_INTEGRAL  :
              ssRate.sInfo.level = Level::CHAN_ZLE_INTEGRAL;
              break;
            case ReductionLevel::CHAN_PROMPT_TOTAL  :
              ssRate.sInfo.level = Level::CHAN_PROMPT_TOTAL;
              break;
            case ReductionLevel::EVNT_FULL_WAVEFORM :
              ssRate.sInfo.level = Level::EVNT_FULL_WAVEFORM;
              break;
            case ReductionLevel::EVNT_ZLE_WAVEFORM  :
              ssRate.sInfo.level = Level::EVNT_ZLE_WAVEFORM;
              break;
            case ReductionLevel::EVNT_ZLE_INTEGRAL  :
              ssRate.sInfo.level = Level::EVNT_ZLE_INTEGRAL;
              break;
            case ReductionLevel::EVNT_PROMPT_TOTAL  :
              ssRate.sInfo.level = Level::EVNT_PROMPT_TOTAL;
              break;

            default:
              ssRate.sInfo.level = Level::UNKNOWN;
              break;
            }
          ssRate.units = Units::NUMBER;
	  message.SetData(ssRate);
          SendMessageOut(message);
	  reductionDataRate[iLevel] = 0;
	  
        }


//      DCMessage::DCTObject rootObj;
//      std::vector<uint8_t> dataTemp;
//      std::map<int,TH1F*>::iterator histIt;
//      for(histIt = TotalQHist.begin();
//	  histIt != TotalQHist.end();
//	  histIt++)
//	{
//	  dataTemp = rootObj.SetData((TObject * )(histIt->second),DCMessage::DCTObject::H1F);
//	  message.SetDestination();
//	  message.SetType(DCMessage::HISTOGRAM);
//	  message.SetData(&(dataTemp[0]),dataTemp.size());
//	  message.SetTime(currentTime);
//	  printf("Sending histogram %s with size %zu\n",histIt->second->GetTitle(),dataTemp.size());
//	  histIt->second->Reset();
//	  SendMessageOut(message,false);
//	}
      
      lastTime = currentTime;
    }  
}

void EBPCAssembler::PrintStatus()
{
  DCThread::PrintStatus();
  for(size_t i = 0; i <server.Size();i++)
    {
      if(server[i].IsRunning())
	server[i].PrintStatus();
    }
}


#endif
