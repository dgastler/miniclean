#include <v1720Readout.h>

bool v1720Readout::Setup(xmlNode * SetupNode,
			 std::map<std::string,MemoryManager*> &MemManager)
{
  //=================================================
  //Find the MemoryManager we need
  //=================================================
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return false;
    }

  //=================================================
  //Find readout settings
  //=================================================

  //Find max readouts(CAEN events) into a block before we pass it off
  if(FindSubNode(SetupNode,"MaxReadoutsPerBlock") == NULL)
    {
      MaxReadoutsPerBlock = 100;
    }
  else
    {
      GetXMLValue(SetupNode,"MaxReadoutsPerBlock","THREAD",MaxReadoutsPerBlock);
    }

  //Find minimum free space in a block before we just pass it off
  if(FindSubNode(SetupNode,"MinimumFreeBufferSpace") == NULL)
    {
      MinimumFreeBufferSpace = 0x20010 * 2;
    }
  else
    {
      GetXMLValue(SetupNode,"MinimumFreeBufferSpace","THREAD",MaxReadoutsPerBlock);
    }

  
  xmlNode * WFDsNode = FindSubNode(SetupNode,"WFDS");
  if(WFDsNode == NULL)
    {
      PrintError("No WFD setup XMLs\n");
      return(false);
    }
  else
    {
      uint32_t nWFDs = NumberOfNamedSubNodes(WFDsNode,"WFD");
      for(unsigned int iWFD = 0; iWFD < nWFDs;iWFD++)
	{
	  xmlNode * tempNode = FindIthSubNode(WFDsNode,"WFD",iWFD);
	  BaseXMLNodes.push_back(tempNode);
	  WFD.push_back(new v1720());
	  WFD.back()->LoadXMLConfig(BaseXMLNodes.back());
	  
	  WFDEventCount.push_back(0);
	  
	  WFDReadoutNumber.push_back(0);         
	  WFDTimeStep_ReadoutNumber.push_back(0);
	  
	  WFDReadoutSize.push_back(0);           
	  WFDTimeStep_ReadoutSize.push_back(0);  
	}
    }

  currentTime = time(NULL);
  lastTime = time(NULL);
  Ready = true;
  return(true);  
}
