#ifndef __VENATOREVENT__
#define __VENATOREVENT__
struct sEvent
{
  uint64_t timeStamp;
  uint32_t triggerCounterNibbles;
  uint32_t eventID;
  bool has_been_blocked;
};
#endif
