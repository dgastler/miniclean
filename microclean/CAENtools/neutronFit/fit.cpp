#include <iostream>


#include "TFile.h"
#include "TSpline.h"
#include "TF1.h"
#include "TH1F.h"
#include "TGraphErrors.h"
#include "TKey.h"
#include "SplineFunction.h"
#include "TCanvas.h"
#include "THStack.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TMinuit.h"
#include "TROOT.h"

TH1F * hPointer;
SplineFunction * sPointer;
void fcn(int &npar, double *gin, double &f, double *par, int iflag) 
{
  f = 0;
  double x = 0;
  double y = 0;
  for(int i = 1; 
      (i <= hPointer->GetEntries()) && 
	(hPointer->GetBinCenter(i) <= hPointer->GetXaxis()->GetXmax()) &&
	(hPointer->GetBinCenter(i) >= hPointer->GetXaxis()->GetXmin());
      i++)
    {
      x = hPointer->GetBinCenter(i);
      y = hPointer->GetBinContent(i);
      if(y != 0.0f)
	f += ((y - (*sPointer)(&x,par))*(y - (*sPointer)(&x,par)))/fabs(y);
      else
	f += ((y - (*sPointer)(&x,par))*(y - (*sPointer)(&x,par)));
    }
};

int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      fprintf(stderr,"%s SplineFile OutFile InFiles\n",argv[0]);
      return(0);
    }
  TFile * SplineFile = TFile::Open(argv[1],"READ");
  if(!SplineFile)
    {
      fprintf(stderr,"Bad spline file.\n");
      return(0);
    }

  std::vector<float> Energy(20,0);
  Energy[0]=10.6;
  Energy[1]=14.5;
  Energy[2]=19.0;
  Energy[3]=22.3;
  Energy[4]=27.7;
  Energy[5]=32.6;
  Energy[6]=38.9;
  Energy[7]=44.5;
  Energy[8]=51.6;
  Energy[9]=59.1;
  Energy[10]=66.9;
  Energy[11]=72.3;
  Energy[12]=79.2;
  Energy[13]=90.6;
  Energy[14]=97.9;
  Energy[15]=114.2;
  Energy[16]=182.5;
  Energy[17]=190.7;
  Energy[18]=211.3;
  Energy[19]=238.9;

  //  SplinePointer = 0;

  std::vector<TH1F*> Histograms;
  //  std::vector<TF1*> Fits;
  std::vector<int> Position;
  std::vector<float> SciEff;
  std::vector<float> SciEffErr;

  std::vector<int> Position2;
  std::vector<float> SciEff2;
  std::vector<float> SciEffErr2;
  

  char * Buffer = new char[100];
  TCanvas * c1 = new TCanvas("c1","c1",1000,1000);  
  TCanvas * c2 = new TCanvas("c2","c2",1000,1000);  
  c1->SetFillColor(0);
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);



  TH1F * Light;
  TH1F * TOF;
  THStack * stack;
  TH1F * s0;
  TH1F * s1;
  TH1F * s2;
  TH1F * s3;
  TLegend * sLegend;
  TF1 * functionPointer;
  TFile * InFile;
  TGaxis * a2;
  TPaveText * newTitle;
  TH1F * FitHistogram;
  SplineFunction * SFunction;
  SplineFunction * SFunction2;
  TF1 * FitFunction;
  TF1 * FitFunction2;


  for(int filenumber = 3;filenumber < argc;filenumber++)
    {

      //Figure out energy
      std::string Title(argv[filenumber]);
      std::string::size_type start = Title.find("SE_")+3;
      Title = Title.substr(start,Title.size());
      std::string::size_type end = Title.find("_");
      Title = Title.substr(0,end);
      printf("%s\n",Title.c_str());
      if((start == std::string::npos)||(end == std::string::npos))
	{
	  fprintf(stderr,"Bad input file name\n");
	  return(0);
	}
      int ID = atoi(Title.c_str()) - 1;
      if((ID < 0) || (ID > (Energy.size() -1)))
	{
	  fprintf(stderr,"Bad input file name ID\n");
	  return(0);
	}
      //Open Data files
      InFile = TFile::Open(argv[filenumber],"READ");
      if(!InFile)
	{
	  fprintf(stderr,"Bad data file %s.\n",argv[3]);
	  return(0);
	}







      //========================================================================================================================================
      //Generate TOF plots!
      //========================================================================================================================================
      c1->cd();
      c1->Clear();
      

      //Load histograms
      TOF = (TH1F*) InFile->Get("cTOF")->Clone();
      if(!TOF)
	{
	  fprintf(stderr,"Bad histogram.\n");
	  return(0);
	}     

      sprintf(Buffer,"TOF_pos%02d",ID);
      stack = (THStack *) SplineFile->Get(Buffer)->Clone();
      if(!stack)
	{
	  fprintf(stderr,"Bad histogram.\n");
	  return(0);
	}
      
      //Setup Data TOF histogram
      TOF->Scale(1.0/TOF->Integral());
      TOF->GetListOfFunctions()->Clear();
      TOF->SetLineWidth(4);
      TOF->SetLineColor(kBlack);
 
      //Get MC TOF histograms from THStack
      s0 = ((TH1F*) stack->GetHists()->At(0));
      s1 = ((TH1F*) stack->GetHists()->At(1));
      s2 = ((TH1F*) stack->GetHists()->At(2));
      s3 = ((TH1F*) stack->GetHists()->At(3));
      if(!s0 || !s1 || !s2 || !s3)
	{
	  fprintf(stderr,"Error in histogram stack\n");
	  return(0);
	}
      //Remove the gaussian fit to the neutrons.
      functionPointer = (TF1*) s0->GetListOfFunctions()->FindObject("gaus");
      s0->GetListOfFunctions()->Remove(functionPointer);

      //Add the DATA TOF histogram to the MC Legend
      sLegend = (TLegend*) s0->GetListOfFunctions()->At(0);
      sLegend->AddEntry(TOF,"microCLEAN data");

      //Find MC scale factor
      Double_t TOFScale = 1.0/(s0->Integral() +
			       s1->Integral() +
			       s2->Integral() +
			       s3->Integral());
      s0->Scale(TOFScale);
      s1->Scale(TOFScale);
      s2->Scale(TOFScale);
      s3->Scale(TOFScale);

      //Draw both histograms so we set the maximum and the Xaxis is built
      TOF->Draw();
      stack->Draw();


      //Setup Axis Ranges
      stack->GetXaxis()->SetRangeUser(0,200e-9);
      stack->GetXaxis()->SetAxisColor(0);
      stack->GetXaxis()->SetLabelColor(0);
      stack->GetXaxis()->SetTitleColor(0);

      TOF->GetXaxis()->SetRangeUser(0,200e-9);
      TOF->GetXaxis()->SetAxisColor(0);
      TOF->GetXaxis()->SetLabelColor(0);
      TOF->GetXaxis()->SetTitleColor(0);



      //Do the final draw of this histograms
      if(stack->GetMaximum() > TOF->GetMaximum())
	{
	  stack->Draw("");
	  TOF->Draw("same");
	}
      else
	{
	  TOF->Draw();
	  stack->Draw("same");
	  TOF->Draw("same");
	}

      a2 = new TGaxis(0,0,200e-9,0,0,200,510,"");
      a2->SetTitle("Neutron time of flight (ns)");
      a2->SetTextColor(kBlack);
      a2->Draw();

      newTitle = new TPaveText(.1,.9,.9,1,"blNDC");
      newTitle->SetTextColor(kBlack);
      newTitle->SetFillColor(0);
      sprintf(Buffer,"Position %2d: %3.3fkeVr",ID,Energy[ID]);
      newTitle->AddText(Buffer);
      newTitle->Draw("same");
      sprintf(Buffer,"TOF_%02d.png",ID);
      c1->Print(Buffer);
      c1->Clear();


      delete newTitle;
      delete a2;
      delete stack;
      delete s0;
      delete s1;
      delete s2;
      delete s3;
      delete TOF;


      //========================================================================================================================================
      //End of TOF plots!
      //========================================================================================================================================



      c2->cd();
 
      sprintf(Buffer,"%05.1f Neutron scatters",Energy[ID]);
      Light = (TH1F*) InFile->Get("cLight");
      gROOT->cd();
      Light = (TH1F*) (Light)->Clone(Buffer);

      if(!Light)
	{
	  fprintf(stderr,"Bad histogram.\n");
	  return(0);
	}
      hPointer = Light;
//      if(ID > 11)
//	{
//	  Light->Rebin(2);
//	}


      //GetSplinePointer for fitting function
      sprintf(Buffer,"%02d_NeutronSpectrum",ID);
      FitHistogram = (TH1F*) ((TH1F*)SplineFile->Get(Buffer))->Clone(Buffer);
      if(!FitHistogram)
	{
	  fprintf(stderr,"Bad spline histogram: %d\n",ID);
	  return(0);
	}

      std::vector<float> X;
      std::vector<float> Y;
      std::vector<float> Y2;
      //Fill vectors for spline function and build an energy calibrated specturm.
      double Y2sum = 0;
      double Ysum = 0;
      for(int ihtemp = 1;ihtemp <= FitHistogram->GetNbinsX(); ihtemp++)
	{
	  X.push_back(FitHistogram->GetBinCenter(ihtemp));
	  Y.push_back(FitHistogram->GetBinContent(ihtemp));
	  if(FitHistogram->GetBinCenter(ihtemp) < 20)
	    {
	      Y2.push_back(FitHistogram->GetBinContent(ihtemp) * (8.5*FitHistogram->GetBinCenter(ihtemp) + 20)/(2*FitHistogram->GetBinCenter(ihtemp) + 180)     );
	    }
	  else
	    {
	      Y2.push_back(FitHistogram->GetBinContent(ihtemp));
	    }
	  Y2sum += Y2[Y2.size()-1];
	  Ysum += Y[Y.size()-1];
	}


      Light->GetXaxis()->SetRangeUser(2,Light->GetXaxis()->GetXmax()/2.0);


      //Allocate fit functions.
      SFunction = new SplineFunction(X,Y,0,Light->GetXaxis()->GetXmax());
      sPointer = SFunction;
      SFunction2 = new SplineFunction(X,Y2,0,Light->GetXaxis()->GetXmax());
      FitFunction = new TF1("FitFunction",SFunction,Light->GetXaxis()->GetXmin(),Light->GetXaxis()->GetXmax(),2);
      FitFunction2 = new TF1("FitFunction2",SFunction2,Light->GetXaxis()->GetXmin(),Light->GetXaxis()->GetXmax(),2);
      FitFunction->SetParNames("Norm","S.E.");
      FitFunction->SetParameters((Light->Integral()/FitHistogram->Integral()),
				 0.2,
				 0,Light->GetXaxis()->GetXmax()
				 );
      FitFunction->SetParLimits(1,0.01,1.0);
      FitFunction->SetParLimits(0,1,4000);
      FitFunction->SetNumberFitPoints(1000);
      FitFunction->SetLineColor(kRed);

      FitFunction2->SetParNames("Norm","S.E.");
      FitFunction2->SetParameters((Light->Integral()/FitHistogram->Integral())*(Ysum/Y2sum),
				 0.2,
				 0,Light->GetXaxis()->GetXmax()
				 );
      FitFunction2->SetParLimits(1,0.01,1.0);
      FitFunction2->SetParLimits(0,1,4000);
      FitFunction2->SetNumberFitPoints(1000);
      FitFunction2->SetLineColor(kBlue);


      //===============================================
      ///FITS
      //===============================================
     
      //      //First pass
      //      Light->Fit(FitFunction,"NE","",1,Light->GetXaxis()->GetXmax());
      //      Light->Fit(FitFunction2,"NE","",1,Light->GetXaxis()->GetXmax());
      //      //Second pass
      //Light->Fit(FitFunction,"ME+","",1,Light->GetXaxis()->GetXmax());
      //Light->Fit(FitFunction2,"ME+","",1,Light->GetXaxis()->GetXmax());

 
      
      TMinuit * gMinuit = new TMinuit(2);
      gMinuit->SetFCN(fcn);


      Double_t arglist[10];
      Int_t ierflg = 0;
      
      //Set error definition
      // 1   : Chi square
      // 0.5 : Negative log likelihood
      //      gMinuit->SetErrorDef(0.5);
      //Can instead use following 2 lines... if you want
      arglist[0] = 1;
      gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
      
      //Minimization strategy
      // 0 : fewer function calls (less reliable)
      // 1 : standard
      // 2 : try to improve minimum (slower)
      arglist[0] = 2;
      gMinuit->mnexcm("SET STR", arglist, 1, ierflg);
      
      //If you want to fix or release parameters (BEWARE numbering)
      // arglist[0] = 1;
      // gMinuit->mxexcm("FIX ", arglist, 1, ierflg);
      // OR use
      // gMinuit->FixParameter(0);

      //Set starting values, bounds, and step sizes for parameters
      static Double_t vstart[2] = {Light->Integral()/FitHistogram->Integral(),0.2};
      static Double_t step[2]   = {1, 0.001};
      static Double_t minvals[2]= {1, 0.01};
      static Double_t maxvals[2]= {10000, 1.0};
      gMinuit->mnparm(0,"n",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
      gMinuit->mnparm(1,"e",vstart[1],step[1],minvals[1],maxvals[1],ierflg);
 


      //Call MIGRAD with 5000 iterations maximum
      arglist[0] = 5000;     //max number of iterations
      arglist[1] = 0.001;    //tolerance
      gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
    

       //Fill fitted variables and associated errors
      Double_t nval, sval, nvalerr, svalerr;
      gMinuit->GetParameter(0,nval,nvalerr);
      gMinuit->GetParameter(1,sval,svalerr);

      FitFunction->SetParameter(0,nval);
      FitFunction->SetParameter(1,sval);
      FitFunction->SetParError(0,nvalerr);
      FitFunction->SetParError(1,svalerr);
      FitFunction->SetParLimits(0,nval,nval+1e-9);
      FitFunction->SetParLimits(1,sval,sval+1e-9);
      Light->Fit(FitFunction,"ME","",Light->GetXaxis()->GetXmin(),Light->GetXaxis()->GetXmax());
      FitFunction->SetParameter(0,nval);
      FitFunction->SetParameter(1,sval);
      FitFunction->SetParError(0,nvalerr);
      FitFunction->SetParError(1,svalerr);

      SciEff.push_back(sval);
      SciEffErr.push_back(svalerr);

      sPointer = SFunction2;
      gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);

      gMinuit->GetParameter(0,nval,nvalerr);
      gMinuit->GetParameter(1,sval,svalerr);

      FitFunction2->SetParameter(0,nval);
      FitFunction2->SetParameter(1,sval);
      FitFunction2->SetParError(0,nvalerr);
      FitFunction2->SetParError(1,svalerr);
      FitFunction2->SetParLimits(0,nval,nval+1e-9);
      FitFunction2->SetParLimits(1,sval,sval+1e-9);
      Light->Fit(FitFunction2,"ME+","",Light->GetXaxis()->GetXmin(),Light->GetXaxis()->GetXmax());
      FitFunction2->SetParameter(0,nval);
      FitFunction2->SetParameter(1,sval);
      FitFunction2->SetParError(0,nvalerr);
      FitFunction2->SetParError(1,svalerr);

      SciEff2.push_back(sval);
      SciEffErr2.push_back(svalerr);

      Light->GetXaxis()->SetRangeUser(0,120);





      Histograms.push_back(Light);

      Position.push_back(ID);
      //      SciEff.push_back(FitFunction->GetParameter(1));
      //      SciEffErr.push_back(FitFunction->GetParError(1));



      Position2.push_back(ID);
      //SciEff2.push_back(FitFunction2->GetParameter(1));
      //SciEffErr2.push_back(FitFunction2->GetParError(1));





      delete gMinuit;
      //      delete Light;
      delete FitHistogram;
      //      delete FitFunction;
      //      delete FitFunction2;
      //      delete SFunction;
      //      delete SFunction2;
      
      InFile->Close();
      delete InFile;
    }

  SplineFile->Close();
  delete SplineFile;

  hPointer = 0;
  sPointer = 0;
  
  TFile * OutFile = TFile::Open(argv[2],"CREATE");
  if(!OutFile)
    {
      fprintf(stderr,"Output file error.\n");
      return(0);
    }
  TGraphErrors * gr = new TGraphErrors(SciEff.size());
  TGraphErrors * gr2 = new TGraphErrors(SciEff.size());

  TGraphErrors * grb = new TGraphErrors(SciEff.size());
  TGraphErrors * grb2 = new TGraphErrors(SciEff.size());

  for(int i = 0; i < Histograms.size();i++)
    {
      Histograms[i]->Write();
    }
  for(int i = 0; i < Position.size();i++)
    {
      gr->SetPoint(i,Energy[Position[i]],SciEff[i]);
      gr->SetPointError(i,3,SciEffErr[i]);
      
      gr2->SetPoint(i,Energy[Position[i]],SciEff[i]*Energy[Position[i]]);
      gr2->SetPointError(i,3,SciEffErr[i]*Energy[Position[i]]);
      
      grb->SetPoint(i,Energy[Position2[i]],SciEff2[i]);
      grb->SetPointError(i,3,SciEffErr2[i]);
      
      grb2->SetPoint(i,Energy[Position2[i]],SciEff2[i]*Energy[Position2[i]]);
      grb2->SetPointError(i,3,SciEffErr2[i]*Energy[Position2[i]]);

    }
  gr->SetName("SE");
  gr->GetXaxis()->SetTitle("Recoil energy (keVr)");
  gr->GetYaxis()->SetTitle("Scintillation efficiency");
  gr->Write();
  
  gr2->SetName("SEEnergy");
  gr2->GetXaxis()->SetTitle("Recoil energy (keVr)");
  gr2->GetYaxis()->SetTitle("Measured energy (keVee)");
  gr2->Write();

  grb->SetName("SEb");
  grb->GetXaxis()->SetTitle("Recoil energy (keVr)");
  grb->GetYaxis()->SetTitle("Scintillation efficiency");
  grb->Write();
  
  grb2->SetName("SEEnergyb");
  grb2->GetXaxis()->SetTitle("Recoil energy (keVr)");
  grb2->GetYaxis()->SetTitle("Measured energy (keVee)");
  grb2->Write();
  
  delete [] Buffer;
  delete c1;
  return(0);
}
