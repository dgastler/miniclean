#ifdef __RATDS__
#include <EBPusher.h>

void EBPusher::CheckForNewConnection()
{
  if(IsReadReady(EBAssembler.GetSocketFD()) || 
     IsWriteReady(EBAssembler.GetSocketFD()))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(EBAssembler.GetSocketFD());
      RemoveWriteFD(EBAssembler.GetSocketFD());      
      SetupSelect();

      //If the connect check fails
      if(!EBAssembler.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  const size_t bufferSize = 100;
	  char buffer[bufferSize];
	  snprintf(buffer,bufferSize,"Can not connect to server %s: retry in %lds\n",
		   EBAssembler.GetRemoteAddress().GetAddrStr().c_str(),
		   GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  EBAssembler.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      SendStop();
	    }	  
	}
    }
}

bool EBPusher::ProcessNewConnection()
{  
  bool ret = true;
  if(EBAssembler.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(EBAssembler.GetFullPacketFD());
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(EBAssembler.GetMessageFD());      

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::CONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);
      Print("Connected");

      //If Loop is true then we need to add the FEPCRatMemory manager
      //to the select set. 
      //It wouldn't have been allowed to be added in the absence of a 
      //net thread connection
      if(Loop)
	{
	  AddReadFD(FEPC_mem->GetToSendFDs()[0]);      
	}
      SetupSelect();
      //Setup the PacketBuilders

      packetBuilder.SetPacketManager(EBAssembler.GetOutPacketManager());
    }
  else
    {
      ret = false;
    }
  return(ret);
}
void EBPusher::DeleteConnection()
{

  //Remove the incoming packet manager fds from list
  RemoveReadFD(EBAssembler.GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(EBAssembler.GetMessageFD());

  //remove any selecting on fdSocket
  RemoveReadFD(EBAssembler.GetSocketFD());
  RemoveWriteFD(EBAssembler.GetSocketFD());

  //stop listening to the FEPC memory manager, since we can't process anymore events
  RemoveReadFD(FEPC_mem->GetToSendFDs()[0]);
  SetupSelect();

  //Shutdown the thread
  EBAssembler.Shutdown();
}

void EBPusher::ProcessChildMessage()
{
  DCMessage::Message message = EBAssembler.GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      DeleteConnection();
      SendStop();
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCMessageServer's DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}


//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool EBPusher::StartNewConnection()
{
  if(EBAssembler.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return true;
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  SendStop(false);
	  return(false);
	}
    }
  else 
    {
      if(EBAssembler.GetSocketFD() >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (EBAssembler.GetSocketFD());
	  AddWriteFD(EBAssembler.GetSocketFD());
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  SendStop(false);
	  return false;
	}
    }
  return true;
}
#endif
