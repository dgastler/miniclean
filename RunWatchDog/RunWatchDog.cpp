#include <mysql/mysql.h>
#include <mysql/errmsg.h>
#include <signal.h>
#include <tclap/CmdLine.h>
#include <boost/algorithm/string.hpp>



volatile bool run;
void signal_handler(int sig)
{
  if(sig==SIGINT)
    {
      run=false;

    }

}
int main(int argc, char ** argv)
{
  struct sigaction sa;
  sa.sa_handler=signal_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGINT,&sa,NULL);
  bool _Err;
  bool _Free;
  std::string _Host="";
  TCLAP::CmdLine cmd("WatchDog for DCDAQ",' ',"0.0");
  try{
  
    TCLAP::UnlabeledValueArg<std::string> HostArg("Host",
						  "MySQL_Host",
						  false,
						  _Host,
						  "host",
						  cmd
						  );

    TCLAP::SwitchArg ErrSwitch("E",
			       "Error_Switch",
 			       "Switch to toggle restart on Error",
			       cmd,
			       false);
 
    TCLAP::SwitchArg FreeSwitch("F",
				"Free_Switch",
				"Switch to toggle restart on Free",
				cmd,
				false);

    cmd.parse(argc,argv);
    _Err=ErrSwitch.getValue();
    _Free=FreeSwitch.getValue();
    _Host=HostArg.getValue();
  }

  catch(TCLAP::ArgException &e)
    {
      fprintf(stderr, "Error %s for arg %s\n",
	     e.error().c_str(), e.argId().c_str());
      return 0;
    }

 if((!_Free) && (!_Err))
   {
     fprintf(stderr,"Well that's a silly way to use me. Guess I'm done. \n");
     return 0;

   }
 std::string host;
 if(_Host.compare("") != 0)
   host=_Host;
 else
   host.assign("cleanpc6.bu.edu");

 run=true;
 MYSQL *ConnectionCheck,conn;
 MYSQL_RES *result;
  
 result=NULL;
 mysql_init(&conn);
 ConnectionCheck=mysql_real_connect(&conn,host.c_str(),"control_user","27K2boil","daq_interface",0,NULL,0);
 if(ConnectionCheck==NULL)
   {
     std::cout << "MySQL Connection error:\t";
     std::cout <<mysql_error(&conn) << std::endl;

     return 0;

   }


 std::string access;
 std::stringstream Query;
 size_t query_length;
 int query_state;
 Query<<"Select access from RUNLOCK";

 //Time printing
 time_t now;
 char timeBuffer[20];
 while(run)
   {
     //Current Time
     now = time(NULL);
     strftime(timeBuffer,sizeof(timeBuffer),
	      "%Y-%m-%d %H:%M:%S",
	      localtime(&now));


     query_length=Query.str().size();
     query_state=mysql_real_query(&conn,Query.str().c_str(),query_length);

     if(query_state!=0)
       {

	 std::cout << "MySQL Query error:\t";
	 std::cout <<mysql_error(&conn) << std::endl;

	 return 0;

       }

     else
       {
	 result=mysql_store_result(&conn);
       }
      
     MYSQL_ROW row=mysql_fetch_row(result);

     access=row[0];

     if(boost::iequals(access,"FREE"))
       {
	  
	 if(_Free)
	   {
	     fprintf(stderr,"%s: Continuing new run from FREE \n",timeBuffer);
	     Query.str(std::string());
	     Query<<"update RUNLOCK set access='NEW_RUN'";
	     query_length=Query.str().size();
	     query_state=mysql_real_query(&conn,Query.str().c_str(),query_length);

	     if(query_state!=0)
	       {
	       
		 std::cout << "MySQL Query error:\t";
		 std::cout <<mysql_error(&conn) << std::endl;

		 return 0;

	       }
	     Query.str(std::string());
	     Query<<"Select access from RUNLOCK";
	   }
	 else
	   fprintf(stderr,"%s: See FREE, but not continuing \n",timeBuffer);
       }

     else if(boost::iequals(access,"ERROR"))
       {
	  
	 if(_Err)
	   {
	     fprintf(stderr,"%s: Continuing new run from ERROR \n",timeBuffer);
	     Query.str(std::string());
	     Query<<"update RUNLOCK set access='NEW_RUN'";
	     query_length=Query.str().size();
	     query_state=mysql_real_query(&conn,Query.str().c_str(),query_length);

	     if(query_state!=0)
	       {
	       
		 std::cout << "MySQL Query error:\t";
		 std::cout <<mysql_error(&conn) << std::endl;

		 return 0;

	       }
	     Query.str(std::string());
	     Query<<"Select access from RUNLOCK";

	   }
	 else
	   fprintf(stderr,"%s: See Error, but not continuing \n",timeBuffer);
       }

     sleep(3);
     mysql_free_result(result);
     result=NULL;
      

   }


 if(result)
   mysql_free_result(result);
 result=NULL;


 mysql_close(&conn);

 return 0;

}
