#include <FEPCAssembler.h>

FEPCAssembler::FEPCAssembler(std::string Name)
{
  SetName(Name);
  SetType(std::string("FEPCAssembler"));

  //FEPC memory manager
  FEPC_mem = NULL;
  collisionCount   = 0;
  lockBlockedCount = 0;

  //memBlock memory manager
  Block_mem = NULL;
  memBlock = NULL;
  memBlockIndex = FreeFullQueue::BADVALUE;
  
  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  //Reset counters
  timeStepLockBlockedCount = 0;
  collisionCount = 0;


}

bool FEPCAssembler::Setup(xmlNode * SetupNode,
			 std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;

  //================================
  //Load the map of board IDs and channels 
  //for the WFDs processed by this thread.
  //================================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::PMT_COUNT |
			 InfoTypes::CHANNEL_MAP
			 );
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }
  
  //There should be two memory managers
  // MemoryBlockManager:  interface with raw CAEN data
  if(!FindMemoryManager(Block_mem,SetupNode,MemManager))
    {
      ret = false;
    }  
  //FEPCManager:   interface with the RAT world
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  
  if(!FEPC_mem)
    {
      fprintf(stderr,"Error %s: Missing FEPC RAT memory manager\n.",
	      GetName().c_str());
      ret = false;
    }
  if(!Block_mem)
    {
      fprintf(stderr,"Error %s: Missing Block memory manager\n.",GetName().c_str());
      ret = false;
    }
  
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


bool FEPCAssembler::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the FEPC RAT memory manager to shut down.
      FEPC_mem->Shutdown();      
      //Tell the memmory block manager to shut down
      Block_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(Block_mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the WFD/EV block's free queue's FDs with select
      AddReadFD(Block_mem->GetFullFDs()[0]);
      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return ret;
}

void  FEPCAssembler::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      float TimeStep = difftime(currentTime,lastTime);
      
      for(std::map<int,size_t>::iterator it=pmtMap.begin(); it!=pmtMap.end(); it++)
	{
	  float averageBaseline = 0;
	  float averageBaselineRMS = 0;
	  float averageIntegral = 0;
	  float averageZLECount = 0;
	  size_t vectorIndex = it->second;	 
	  if((TimeStep_ZLECount.size() > vectorIndex) && (TimeStep_ZLECount[vectorIndex] > 0))
	    //Calculate statistics
	    {
	      averageBaseline = float(TimeStep_Baseline[vectorIndex])/float(TimeStep_ZLECount[vectorIndex]);
	      averageBaselineRMS = float(TimeStep_BaselineRMS[vectorIndex])/float(TimeStep_ZLECount[vectorIndex]);
	      averageIntegral = float(TimeStep_Integral[vectorIndex])/float(TimeStep_ZLECount[vectorIndex]);
	      averageZLECount = float(TimeStep_ZLECount[vectorIndex])/float(TimeStep_EventCount[vectorIndex]);
	      //fprintf(stderr,"TimeStep_ZLECount: %i, TimeStep_EventCount: %i, averageZLECount: %f\n",TimeStep_ZLECount[vectorIndex],TimeStep_EventCount[vectorIndex],averageZLECount);
	      //fprintf(stderr,"TimeStep_Baseline: %i; averageBaseline: %f\n",TimeStep_Baseline[vectorIndex],averageBaseline);

	      StatMessage::StatFloat ssFloat;
	      
	      uint pmt_id = it->first;
	      //fprintf(stderr,"pmt_id: %02u\n",pmt_id);
	      char PMTID[] = "PMT_00";	  
	      sprintf(PMTID,"PMT_%02u",pmt_id);

	      ssFloat.sBase.type = StatMessage::BASELINE;
	      ssFloat.val = averageBaseline;	 
	      ssFloat.sInfo.time = currentTime;
	      ssFloat.sInfo.interval = TimeStep;
	      ssFloat.sInfo.subID = SubID::GetType(PMTID);
	      ssFloat.sInfo.level = Level::BLANK;
	      message.SetData(ssFloat);
	      message.SetType(DCMessage::STATISTIC);
	      SendMessageOut(message);
	      
	      ssFloat.sBase.type = StatMessage::ZLECOUNT;
	      ssFloat.val = averageZLECount;	 
	      ssFloat.sInfo.time = currentTime;
	      ssFloat.sInfo.interval = TimeStep;
	      ssFloat.sInfo.subID = SubID::GetType(PMTID);
	      ssFloat.sInfo.level = Level::BLANK;
	      message.SetData(ssFloat);
	      message.SetType(DCMessage::STATISTIC);
	      SendMessageOut(message);
	      
	    }
	  
	  TimeStep_Baseline[vectorIndex] = 0;
	  TimeStep_BaselineRMS[vectorIndex] = 0;
	  TimeStep_Integral[vectorIndex] = 0;
	  TimeStep_ZLECount[vectorIndex] = 0;
	  TimeStep_EventCount[vectorIndex] = 0;
	  
	  
	}
      
      //Data message
      
      //      message.SetDataStruct(Count);
      //      message.SetType(DCMessage::STATISTIC);
      //      SendMessageOut(message,false);
      //
      //     
      //      //Data message
      //      sprintf(Count.text,"Bad events read");
      //      Count.count = collisionCount;
      //      Count.dt = TimeStep;
      //      message.SetDataStruct(Count);
      //      message.SetType(DCMessage::STATISTIC);
      //      SendMessageOut(message,false);
      
      //Reset counters
      timeStepLockBlockedCount = 0;
      collisionCount = 0;

      //Setup next time
      lastTime = currentTime;
    }
}
