#ifndef __BASICRATDSMANAGER__
#define __BASICRATDSMANAGER__


#include <MemoryManager.h>
#include <FreeFullQueue.h>
#include <xmlHelper.h>
#include <DSBlock.h>
#include "RAT/DS/Root.hh"

class BasicRATDSManager : public MemoryManager , public FreeFullQueue
{
 public:
  BasicRATDSManager(std::string Name);
  ~BasicRATDSManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  DSBlock * GetDSBlock(uint32_t iDS) 
  {
    if(iDS < Memory.size())
      {
	return(&(Memory[iDS]));
      }
    return(NULL);
  }
  virtual void Shutdown(){ShutdownQueues();};
  virtual void WakeUp(){WakeUpQueues();};
  
  virtual void PrintStatus(int indent = 0);
 private: 
  std::vector<DSBlock> Memory;
  void Deallocate();
};

#endif
