#include <MemoryBlockManager.h>

MemoryBlockManager::MemoryBlockManager(std::string Name)
{
  //Make sure our memory block is empty.
  SetName(Name);
  SetType(std::string("MemoryBlockManager"));
  Memory.clear();
}

MemoryBlockManager::~MemoryBlockManager()
{
  Deallocate();
}

void MemoryBlockManager::Deallocate()
{
    //Loop over allocated blocks and delete them.
  while(Memory.size())
    {
      MemoryBlock block = Memory.back(); // Get the last element of the vector
      if(block.buffer) //Check if the pointer is allocated
	{
	  delete [] block.buffer;
	}
      Memory.pop_back(); //remove the last element in the vector.
    }
}

bool MemoryBlockManager::AllocateMemory(xmlNode * SetupNode)
{
  //Get the block size;
  uint32_t blockSize;
  int blockSizeERR = GetXMLValue(SetupNode,"BLOCKSIZE","MANAGER",blockSize);
  //Get the number of Blocks
  uint32_t nBlocks;
  int blockCountERR = GetXMLValue(SetupNode,"BLOCKCOUNT","MANAGER",nBlocks);
    
  Deallocate();  
  if(blockSizeERR || blockCountERR)
    {
      printf("Error:    MemoryBlockManager XML parse error.\n");
      return(false);
    }
  MemoryBlock block;
  for(unsigned int i = 0; i < nBlocks;i++)
    {
      block.buffer = NULL;
      block.buffer = new uint8_t[blockSize];
      if(!block.buffer)
	{
	  fprintf(stderr,"Error:  Block allocation failed!");
	  return(false);
	}
      block.allocatedSize = blockSize;
      block.dataSize = 0;
      Memory.push_back(block);
      int32_t index = Memory.size()-1;
      AddFree(index);
    }
  return(true);
}


