#include <Control.h>
Control::Control(std::string Name)
{
  SetType(std::string("Control"));
  SetName(Name);
  bufferSize = 1000;
  buffer = new char[bufferSize];
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
};
Control::~Control()
{
  delete [] buffer;
}

void Control::LowerCase(std::string &Text)
{
  std::transform(Text.begin(),
		 Text.end(),
		 Text.begin(),
		 tolower);
}
void Control::RemoveEOL(std::string &Text)
{
  size_t pos = 0;
  while((pos = Text.find('\n',0)) != std::string::npos)
    {
      Text.erase(pos,1);
    }
}


void Control::ProcessCommand(std::string Command)
{
  //Remove EOL
  RemoveEOL(Command);

  //Find the base of the command
  size_t CommandEnd = Command.find(' ');
  std::string CommandBase = Command.substr(0,CommandEnd);
  //Find the arguments if any.
  std::string CommandArg;

  //Convert all commands to lower case
  LowerCase(CommandBase);

  if((CommandEnd == Command.size()) ||
     (CommandEnd == std::string::npos))
    {
      CommandArg = "";
    }
  else
    {
      CommandArg = Command.substr(CommandEnd+1,
				  Command.size() - CommandEnd);
    }


  //check base against all commands.
  std::vector<std::string>::iterator it = find(Commands.begin(),Commands.end(),CommandBase);
  if(it != Commands.end())
    {
      (*this.*(CommandFunctions[distance(Commands.begin(),it)]))(CommandArg);
      fprintf(stdout,"DCDAQ>");
      fflush(stdout);
    }
  else    
    {
      fprintf(stdout,"Unknown: %s\nDCDAQ>",CommandBase.c_str());
      fflush(stdout);
    }
}

void Control::MainLoop()
{
  if(IsReadReady(STDIN_FILENO))
    {
      int readSize = 0;
      readSize = read(STDIN_FILENO,buffer,bufferSize-1);
      if(readSize >0)
	{
	  if(buffer[readSize-1] == '\n')
	    {
	      readSize--;
	    }
	  buffer[readSize] = '\0';
	  if(readSize > 0)
	    {
	      std::string command(buffer,readSize);	  
	      ProcessCommand(command);
	    }
	  else
	    {
	      fprintf(stdout,"DCDAQ>");
	      fflush(stdout);
	    }
	}  
    }
}

void Control::ProcessTimeout()
{
  //not needed for this thread
}

bool Control::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //We shouldn't pause the human interface thread
      // Loop = false;
      printf("Warning:    Control thread ignoring pause request.\n");
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This shouldn't be needed since this thread
      //should never have Loop=false, but it doesn't hurt.
      Loop = true;
      //Register the STDIN_FILENO fd with DCThread
      AddReadFD(STDIN_FILENO);
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}
