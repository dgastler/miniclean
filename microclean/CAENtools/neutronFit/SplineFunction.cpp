#include "SplineFunction.h"

ClassImp(SplineFunction)

SplineFunction::SplineFunction()
{
  SplineSet=false;
  rMax = 0;
  rMin = 0;
}

//SplineFunction::SplineFunction(char * name,std::vector<float> X,std::vector<float> Y,float Min_ =0,float Max_=0)
SplineFunction::SplineFunction(std::vector<float> &X,std::vector<float> &Y,float Min_ =0,float Max_=0)
{
  SplineSet=false;
  rMax = 0;
  rMin = 0;
  SetSpline(X,Y,Min_,Max_);
}

//void SplineFunction::SetSpline(char * name,std::vector<float> X,std::vector<float> Y,float Min_ =0,float Max_=0)
void SplineFunction::SetSpline(std::vector<float> &X,std::vector<float> &Y,float Min_ =0,float Max_=0)
{
  assert(X.size() == Y.size());
  int Size = X.size();
  Double_t dx = 0;
  Double_t * Xs = new Double_t[Size];
  Double_t * Ys = new Double_t[Size];
  if(Size > 2)
    dx = fabs(X[1] - X[0]);
  double VectorIntegral = 0;
  for(int i = 0; i < Size;i++)
    {
      Xs[i] = X[i];
      Ys[i] = Y[i];
      //      VectorIntegral += Y[i]*dx;
      //      VectorIntegral += Y[i];
    }
//  for(int i = 0; i < Size;i++)
//    {
//      Ys[i] /= VectorIntegral;
//    }  


  //  TGraph * gr = new TGraph(Size,Xs,Ys);
  //                 //  gr->Smooth(Size,Xs,Ys,1000);
  //                 //  Spline = new TSpline1("EnToAng",Xs,Ys,Size,"", 2,2);
  //  Spline = new TMVA::TSpline1("EnToAng",gr);
  //                 //  TString name("EnToAng");
  //Spline = new TSpline3("EnToAng",gr,"",1,0);




  Spline = new TSpline3("EnToAng",Xs,Ys,Size,"",0,0);




  Spline->SetNpx(Spline->GetNpx()*1000);
  SplineSet = true;
//  for(float x = 0; x < Xs[Size-1];x+=Xs[Size-1]/Size)
//    {
//      printf("%f %f\n",x,Spline->Eval(x));
//    }
  if((Min_ == 0) && (Max_ == 0))
    {
      rMin = Spline->GetXmin();
      rMax = Spline->GetXmax();
    }
  else
    {
      rMin = Min_;
      rMax = Max_;
    }
  //  delete Xs;
  //  delete Ys;
  //  delete gr;
}

double SplineFunction::operator() (double *x,double*p)
{
  if(!SplineSet)
    {
      return(0);
    }
  
  double t = x[0]/p[1];
  //  if((t < rMin)||(t > rMax))
  //  if((t < Spline->GetXmin())||(t>Spline->GetXmax()))
  if((t < rMin)||(t>rMax))
    {
      return(0);
    }

  double ret = p[0]*Spline->Eval(t);
  return(ret);
}
