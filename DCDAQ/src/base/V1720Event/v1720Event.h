#ifndef __V1720_EVENT__
#define __V1720_EVENT__

#include <stdint.h> //uint32_t
#include <cstdio>
#include <vector>

#include <v1720EventChannel.h>

enum
  {
    V1720EVENT_OK = 0,
    V1720EVENT_SMALL_EVENT = -1,
    V1720EVENT_CORRUPT_EVENT = -2,
    V1720EVENT_OTHER = -3
  };

class v1720Event
{
 public:
  v1720Event();
  ~v1720Event();
  uint32_t *header;
  std::vector<v1720EventChannel> Channels;
  //returns the BYTE(!) offset of the next possible position of an event in the _rawData array.
  int32_t ProcessEvent(char * _rawData, uint32_t _rawSizeMax,bool BasicParseOnly = false);
  bool IsZLE(){return(ZLE);};
  bool IstwoPack(){return(twoPack);};
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);};//returns 32bit pointer and 32bit size.
 private:
  int32_t ProcessHeader();
  int ProcessZLE(uint8_t channelID,uint32_t &position);
  int ProcessFULL(uint8_t channelID,uint32_t &position);
  uint8_t BitSum(uint32_t data);
  void WriteCorruptEvent();

  uint32_t * rawData;
  uint32_t size;
  uint32_t rawSizeMax;

  bool ZLE;
  bool twoPack;
  uint32_t boardID;
  uint32_t IOPattern;
  uint32_t channelMask;
  uint8_t  numberOfChannels;
  uint32_t eventNumber;
  uint32_t baseTime;
};

#endif
