#include <PacketBuilder.h>
#include <EBPusher.h>

bool PacketBuilder::CheckBoardSize_CHAN_PROMPT_TOTAL(RATEVBlock * event,RAT::DS::Board * board)
{
  //Calculate the size for this RAT::DS::Board in EBPacket structures
  uint32_t boardSize = sizeof(EBPacket::Board);
  int channelCount = board->GetChannelCount();  
  boardSize += channelCount * (sizeof(EBPacket::Channel) + 
			       sizeof(EBPacket::IntegralBlock));

  //See if we have room
  if(boardSize > dataSizeLeft || packet==NULL)
    {
      return CheckPacket(boardSize,event);
      //return true;
    }
  return true;
}

void PacketBuilder::AddChannel_CHAN_PROMPT_TOTAL(RAT::DS::Channel * channel,int16_t PMTID)
{
  //Calculate the prompt and total integrals for this waveform
  float promptIntegral = 0;
  double totalIntegral = 0;

  //Add channel
  EBPacket::Channel * ebChannel = (EBPacket::Channel *) currentPtr;
  currentPtr += sizeof(EBPacket::Channel);
  dataSizeLeft -= sizeof(EBPacket::Channel);
  ebChannel->dataBlockCount = 1; //prompt total only has one block
  EBPacket::PromptTotalBlock * ebPromptTotal = (EBPacket::PromptTotalBlock *) currentPtr;
  currentPtr += sizeof(EBPacket::PromptTotalBlock);
  dataSizeLeft -= sizeof(EBPacket::PromptTotalBlock);      

  uint32_t eventStartTime = detectorInfo.GetEventStartTime();
  uint32_t promptIntegrationEndTime = detectorInfo.GetPromptIntegrationEndTime();
  double channelOffset = detectorInfo.GetPMTOffset(PMTID);

  for(int iRawBlock = 0; iRawBlock < channel->GetRawBlockCount();iRawBlock++)
    {
      RAT::DS::RawBlock * rawBlock = channel->GetRawBlock(iRawBlock);
      std::vector<uint16_t> waveform = rawBlock->GetSamples();
      uint32_t sampleTime = rawBlock->GetOffset();
  
      for(size_t iSample = 0; iSample < waveform.size();iSample++)
      	{
      	  //If we are in the valid event time of the waveform
      	  if(sampleTime > eventStartTime)	    
	    {	      
      	      float sample = channelOffset - waveform[iSample];
      	      //Add charge to prompt integral if in prompt time
      	      if(sampleTime < promptIntegrationEndTime)
      		{
      		  promptIntegral+=sample;
      		}
      	      //Add charge to total integral
      	      totalIntegral+=sample;

	    }
	  sampleTime++;
      	}
    }

  //Copy data
  ebPromptTotal->prompt  = promptIntegral;
  ebPromptTotal->total   = totalIntegral;      
  TotalQHist[4]->Fill(ebPromptTotal->total);
}
