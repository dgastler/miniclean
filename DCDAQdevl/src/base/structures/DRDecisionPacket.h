#ifndef __DRDECISIONPACKET__
#define __DRDECISIONPACKET__

namespace DRDecisionPacket
{
  struct Event
  {
    uint32_t ID;
    uint8_t reductionLevel;
  };
}
#endif
