#include <DRGetter.h>

DRGetter::DRGetter(std::string Name)
{
  Ready = false;
  SetType("DRGetter");
  SetName(Name);
  Loop = true; //This thread should start asap
               //This is so the network connections can be made

  //FEPC memory manager
  FEPC_mem = NULL;

  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  //Default packet manager string.
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
}

DRGetter::~DRGetter()
{
  //Shutdown the net thread and close the network socket
  DRSender.Shutdown();
}

void DRGetter::ThreadCleanUp()
{
  DRSender.Shutdown();
}

bool DRGetter::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager)
{
  bool ret = true;
  //==============================================================
  //Load the DRPC memory manager
  //==============================================================
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  //==============================================================
  //Parse data for the client packet MM
  //==============================================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     int(managerSettings.size()),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  //==============================================================
  //Setup connection class 
  //==============================================================
  if(!DRSender.Setup(SetupNode))
    //Connection setup failed
    {
      ret = false;
    }
  //==============================================================
  //Listen to the local DCQueue
  //==============================================================
  AddReadFD(DecisionQueue.GetFDs()[0]);
  SetupSelect();

  //If everything in setup worked, then set Ready to true;
  if(ret == true)
    {
      Ready = true;
    }
  else
    {
      return false;
    }

  //This starts the connection class
  //it will keep trying to connect, but allow the thread
  //to run.   
  return(StartNewConnection());
}

void DRGetter::PrintStatus()
{
  DCThread::PrintStatus();
  #if __WORDSIZE == 64
  char decisionQueueStatus[] = " DecisionQueue   Size: %04lu\n";
  #else
  char decisionQueueStatus[] = " DecisionQueue   Size: %04u\n";
  #endif
  printf(decisionQueueStatus,DecisionQueue.GetSize());
  if(DRSender.thread != NULL)
    {
      DRSender.thread->PrintStatus();
    }  
}
