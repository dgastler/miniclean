#include <DRPusher.h>

void DRPusher::ProcessUnSentEvent()
{
  //Get next unsent event
  int32_t index;      
  if(FEPC_mem->GetUnSent(index)) 
    //We got a new "Un-sent" event.
    {
      //Pointer to FEPCBlock
      FEPCBlock * block = NULL;
      //Check and get the FEPCBlock
      if(index != FEPC_mem->GetBadValue() &&
	 ((block = FEPC_mem->GetFEPCBlock(index)) != NULL))
	//A valid event
	{	  
	  //FEPC interface values
	  uint64_t password = FEPC_mem->GetStorageBlankPassword(); 
	  int32_t tempIndex = FEPC_mem->GetBadValue();
	  //Get a place in the storage VecHash for this event
	  int64_t retFEPC= FEPC_mem->GetStorageEvent(block->eventID,
						     tempIndex,
						     password);
	  if(retFEPC == returnDCVectorHash::EMPTY_KEY)
	    {
	      ParseEvent(index,password);
	    }
	  else if(retFEPC >= returnDCVectorHash::COLLISION)		  
	    {
	      MoveBad(retFEPC,tempIndex,BadEvent::STORAGE_COLLISION,
		      index,password);
	    }
	  else if(retFEPC == returnDCVectorHash::BAD_PASSWORD ||
		  retFEPC == returnDCVectorHash::BAD_MUTEX)
	    {
	      if(!FEPC_mem->AddUnSent(index))
		{
		  //The only way for a failure here is that the mutex 
		  //lock has been turned off and we should shutdown.
		  char * buffer = new char[100];
# if __WORDSIZE == 64
		  sprintf(buffer,"Unable to return event %ld to the UnSent DCQueue",
			  block->eventID);
# else
		  sprintf(buffer,"Unable to return event %lld to the UnSent DCQueue",
			  block->eventID);
#endif
		  PrintError(buffer);
		  delete [] buffer;
		}   
	    }
	  else if(retFEPC == returnDCVectorHash::OK)
	    {
	      MoveBad(retFEPC,tempIndex,BadEvent::STORAGE_COLLISION,
		      block->eventID,index);
	    }
	  else
	    {
	      PrintError("Unknown error in GetStorageEvent");
	      SendStop();
	    }
	  //No matter what happens in this function we need to 
	  //forget all the info we were passed. (it's no longer valid)
	  block = NULL;
	  index = FEPC_mem->GetBadValue();
	}
      else
	{
	  PrintError("bad block");
	}		
    }
  else
    {
      PrintError("Error getting unsent event.");
    }  
}

void DRPusher::MoveBad(int64_t badEventID,int32_t badIndex,int32_t errorCode,
		       int32_t index,uint64_t password)
{
  if(FEPC_mem->AddBadEvent(badEventID,
			   badIndex,
			   errorCode))
    {
      ParseEvent(index,password);
    }
  else
    {
      char * buffer = new char[100];
# if __WORDSIZE == 64
      sprintf(buffer,"Failed to move bad event %ld",badEventID);
# else 
      sprintf(buffer,"Failed to move bad event %lld",badEventID);
# endif
      PrintError(buffer);
      delete [] buffer;
      SendStop();
    }
}


void DRPusher::ParseEvent(int32_t index, uint64_t password)
{
  //If we have a new event, we need to add it to the end of the LocalDeque
  if(index != FEPC_mem->GetBadValue())   
    {      
      AddToLocalDeque(FEPC_mem->GetFEPCBlock(index),
		      index,
		      password,
		      time(NULL),
		      0); //number of channels processed
      //Clear our copies of index and password
      //This 
      index = FEPC_mem->GetBadValue();
      password = FEPC_mem->GetStorageBlankPassword();
    }

  //Object to hold the last event in the LocalDeque
  sLocal currentLocalEvent;
  //Bool to control if a packet is ready to be sent
  bool sendPacket = !CheckParseStatus(); 
  if(!sendPacket)
    {
      //Get the next channel to add. 
      LocalDeque.LockDeque();       //=============GOT LOCK===================
      currentLocalEvent = LocalDeque.Back();      
      while(int(currentLocalEvent.channelsProcessed) < 
	    currentLocalEvent.block->ev->GetPMTCount())
	{
	  RAT::DS::PMT * pmt = currentLocalEvent.block->ev->GetPMT(currentLocalEvent.channelsProcessed);
	  //Get the PMT's raw strucutre
	  RAT::DS::Raw * raw = pmt->GetRaw();
	  //Calculate how much space we need for this data
	  int rawIntegralCount = raw->GetRawIntegralCount();
	  uint32_t channelSize = (sizeof(DRPacket::Channel) + 
				  (rawIntegralCount*sizeof(DRPacket::RawIntegral)));
	  if(channelSize <= packetSizeLeft)
	    {
	      AddToPacket(raw,pmt->GetID());
	      currentLocalEvent.channelsProcessed++;
	    }
	  else
	    {
	      //Packet is full
	      sendPacket = true;
	      break;	      
	    }
	}
      LocalDeque.UnlockDeque();    //============RELEASED LOCK===============
      LocalDeque.Back() = currentLocalEvent;
      currentLocalEvent = LocalDeque.GetBadValue();
    }
  
  if(sendPacket)
    {
      //      SendCurrentPacket();
      //      fprintf(stderr,"Sent Packet!\n");
      SendData();
    }
  //We are either done with this event or we just sent out a packet. 
  //Either way the drEvent pointer is no longer valid, so we set it to NULL
  //  if(drEvent != NULL)
  //    {
      //      fprintf(stderr,"Event #%u has %u channels\n",drEvent->ID,drEvent->ChannelCount);
  //}
  drEvent = NULL;      
}

bool DRPusher::AddToLocalDeque(FEPCBlock *block,
			       int32_t index,
			       uint64_t password,
			       time_t time,
			       uint32_t channelsProcessed)
{
  sLocal tempStruct;
  tempStruct.block = block;
  tempStruct.index = index;
  tempStruct.password = password;
  tempStruct.time = time;
  tempStruct.channelsProcessed = channelsProcessed;
  return(LocalDeque.Add(tempStruct));
}
