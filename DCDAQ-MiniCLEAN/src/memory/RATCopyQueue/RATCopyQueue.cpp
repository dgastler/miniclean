#include <RATCopyQueue.h>

RATCopyQueue::RATCopyQueue(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATCopyQueueManager"));
  ShutdownStatus=false;
  currentTime=time(NULL);
}

RATCopyQueue::~RATCopyQueue()
{}
  
bool RATCopyQueue::AllocateMemory(xmlNode * SetupNode)
{
  int maxSizeERR = GetXMLValue(SetupNode,"MAXSIZE","MANAGER",maxSize);
  if(maxSizeERR)
    {
      printf("Error:   RATCopyQueue XML Parse Error, MAXSIZE not found \n");
      return false;
    }
  else 
    return true;
}

bool RATCopyQueue::Add(EventToPorts * ev)
{
  bool ret;
  if(GetSize()>=maxSize)
    {
      ret=false;
    }
  else
    ret=DCQueue<EventToPorts*>::Add(ev);
  return ret;
}

bool RATCopyQueue::AddEntrySocket(uint16_t port,zmqInfo * socketInfo,bool Wait)
{

 
  if(!Lock.LockObject(Wait))
    {
      return false;
    }

  zmqSocketMap[port]=zmqInfoSet_t();
  zmqSocketMap[port].push_back(socketInfo);

  Lock.WakeUpSignal();
  Lock.UnlockObject();
  return true;
}

bool RATCopyQueue::RemoveEntrySocket(uint16_t port,bool Wait)
{
 
  if(!Lock.LockObject(Wait))
    {
      return false;
    }
  if(zmqSocketMap.find(port)==zmqSocketMap.end())
    {
      return true;
    }
  zmqSocketMap[port].clear();
  zmqSocketMap.erase(port);
   
  Lock.WakeUpSignal();
  Lock.UnlockObject();
 
  return true;
}

bool RATCopyQueue::EditEntrySocket(uint16_t port,size_t filterNum,StreamFilter * filter,bool Wait)
{
 
  
  if(!Lock.LockObject(Wait))
    {
      return false;
    }
  while(filterNum+1>zmqSocketMap[port].size())
    {
      zmqSocketMap[port].push_back(new zmqInfo);
    }
  delete zmqSocketMap[port][filterNum]->filter;
  zmqSocketMap[port][filterNum]->filter=filter;
 
   
  Lock.WakeUpSignal(); 
  Lock.UnlockObject();
  return true;
}

bool RATCopyQueue::RemoveEntryFilter(uint16_t port,size_t filterNum,bool Wait)
{

   if(!Lock.LockObject(Wait))
    {
      return false;
    }
 
   if(filterNum>zmqSocketMap[port].size())
     return true;

   zmqSocketMap[port].erase(zmqSocketMap[port].begin()+filterNum);

   
   Lock.WakeUpSignal(); 
   Lock.UnlockObject();
   return true;
}


bool RATCopyQueue::PauseEntrySocket(uint16_t port,bool Wait)
{

  if(!Lock.LockObject(Wait))
    {
      return false;
    }
  for(size_t i=0;i<zmqSocketMap[port].size();i++)
    {
      zmqSocketMap[port][i]->status=false;
    }
 
  Lock.WakeUpSignal();
  Lock.UnlockObject();
  return true;
}

bool RATCopyQueue::GoEntrySocket(uint16_t port,bool Wait)
{
  if(!Lock.LockObject(Wait))
    {
      return false;
    }
  for(size_t i=0;i<zmqSocketMap[port].size();i++)
    {
      zmqSocketMap[port][i]->status=true;
    }
  Lock.WakeUpSignal();
  Lock.UnlockObject();
  return true;
}

bool RATCopyQueue::CheckEntriesSocket(RAT::DS::EV * ev,bool Wait)
{
  
    if(!Lock.LockObject(Wait))
    {
      return false;
    }

    EventToPorts * evToPorts=NULL;

    currentTime=time(NULL);
    bool sentToThisPort;
    for(zmqInfoSetMap_t::iterator zmq_iter=zmqSocketMap.begin();zmq_iter!=zmqSocketMap.end();zmq_iter++)
      {
	sentToThisPort=false;
	
	//This following if statement is a hack!! should not be committed!!!  TriggerPattern setting for MC should be handled somewhere else!!!!!!!  If you find this and you didn't write it that's not good! yell at chris/dan (unless it's commented out, then please don't yell)
	/*
       	if(ev->GetTriggerPattern()==0)
	  {
	    ev->SetTriggerPattern((Long_t)2<<40);
	    }*/
	//check on status and filter.  If event passes filters, send it off to client
	for(size_t i=0;i<zmq_iter->second.size();i++)
	  {
	    if((zmq_iter->second[i]->status)
	       &&(zmq_iter->second[i]->filter->ReducLevels & (1<<ev->GetReductionLevel()))
	       &&(zmq_iter->second[i]->filter->HardwareTrig & ev->GetTriggerPattern()))
	  
	      {

		if((!sentToThisPort)&&((currentTime-zmq_iter->second[i]->timeLastSent)>zmq_iter->second[i]->filter->period)
		   &&((currentTime-zmq_iter->second[i]->timeLastSent)>1))
		  {
		    sentToThisPort=true;
		    fprintf(stderr,"Setting stp true filter %zu\n",i);

		    if(!evToPorts)
		      {
			evToPorts=new EventToPorts(*ev);
		      }
		    evToPorts->ports.push_back(zmq_iter->first);
		
		  }
	      }
	  }
	if(sentToThisPort)
	  {
	    for(size_t i=0;i<zmq_iter->second.size();i++)
	      {
		zmq_iter->second[i]->timeLastSent=currentTime;
	      }
	  }
	
      }
    //if anything there, send off to Queue
    if(evToPorts)
      Add(evToPorts);

    
    Lock.WakeUpSignal();
    Lock.UnlockObject();
    return true;
	
}
void RATCopyQueue::PrintStatus(int indent, int verbosity)
{
  std::stringstream ss;
  ss << "RATCopyQueue\n     Size: " << GetSize() 
     << "  (" <<GetAddCount() << ")";
  Print(ss.str());
//  printf("RATCopyQueue\n     Size: %04zu   (%12zu)\n", 
//	 GetSize(),
//	 GetAddCount());

}
  
