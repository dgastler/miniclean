VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan2e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL LED(7:0)
        SIGNAL XLXN_14
        SIGNAL PPORT(7:0)
        SIGNAL CLOCK
        SIGNAL XLXN_32
        SIGNAL XLXN_33
        SIGNAL PULSE
        SIGNAL TRIG
        SIGNAL XLXN_103
        SIGNAL XLXN_121
        SIGNAL XLXN_128
        SIGNAL XLXN_131
        SIGNAL XLXN_138(15:0)
        SIGNAL XLXN_139(15:0)
        SIGNAL XLXN_142(15:0)
        SIGNAL PULSECOUNT(7:0)
        SIGNAL PULSECOUNT(15:0)
        SIGNAL XLXN_146
        SIGNAL XLXN_148
        SIGNAL XLXN_151
        SIGNAL XLXN_152
        PORT Output LED(7:0)
        PORT Input PPORT(7:0)
        PORT Input CLOCK
        PORT Output PULSE
        PORT Output TRIG
        BEGIN BLOCKDEF ibuf8
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 128 -44 224 -20 
            LINE N 224 -32 128 -32 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf8
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            RECTANGLE N 0 -44 64 -20 
            RECTANGLE N 128 -44 224 -20 
        END BLOCKDEF
        BEGIN BLOCKDEF ibufg
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 224 -32 128 -32 
            LINE N 0 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF obuf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 0 64 -64 
            LINE N 128 -32 64 0 
            LINE N 64 -64 128 -32 
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF constant
            TIMESTAMP 2006 1 1 10 10 10
            RECTANGLE N 0 0 112 64 
            LINE N 144 32 112 32 
        END BLOCKDEF
        BEGIN BLOCKDEF fdc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            RECTANGLE N 64 -320 320 -64 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF cb16ce
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 384 -192 320 -192 
            RECTANGLE N 320 -268 384 -244 
            LINE N 384 -256 320 -256 
            LINE N 0 -192 64 -192 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 384 -128 320 -128 
            RECTANGLE N 64 -320 320 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF compm16
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 384 -256 320 -256 
            LINE N 384 -192 320 -192 
            RECTANGLE N 64 -384 320 -64 
            RECTANGLE N 0 -332 64 -308 
            LINE N 0 -320 64 -320 
            RECTANGLE N 0 -140 64 -116 
            LINE N 0 -128 64 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF comp16
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -384 320 -64 
            LINE N 0 -128 64 -128 
            RECTANGLE N 0 -140 64 -116 
            LINE N 0 -320 64 -320 
            RECTANGLE N 0 -332 64 -308 
            LINE N 384 -224 320 -224 
        END BLOCKDEF
        BEGIN BLOCKDEF cb4ce
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -512 320 -64 
            LINE N 0 -32 64 -32 
            LINE N 0 -128 64 -128 
            LINE N 384 -256 320 -256 
            LINE N 384 -320 320 -320 
            LINE N 384 -384 320 -384 
            LINE N 384 -448 320 -448 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 384 -128 320 -128 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 0 -192 64 -192 
            LINE N 384 -192 320 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 ibuf8
            PIN I(7:0) PPORT(7:0)
            PIN O(7:0) PULSECOUNT(7:0)
        END BLOCK
        BEGIN BLOCK XLXI_2 obuf8
            PIN I(7:0) PULSECOUNT(7:0)
            PIN O(7:0) LED(7:0)
        END BLOCK
        BEGIN BLOCK XLXI_11 vcc
            PIN P XLXN_14
        END BLOCK
        BEGIN BLOCK XLXI_7 ibufg
            PIN I CLOCK
            PIN O XLXN_33
        END BLOCK
        BEGIN BLOCK XLXI_19 vcc
            PIN P XLXN_32
        END BLOCK
        BEGIN BLOCK XLXI_21 and2
            PIN I0 XLXN_146
            PIN I1 XLXN_148
            PIN O XLXN_151
        END BLOCK
        BEGIN BLOCK XLXI_8 obuf
            PIN I XLXN_128
            PIN O PULSE
        END BLOCK
        BEGIN BLOCK ALLONES constant
            BEGIN ATTR CValue "0035"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Hexadecimal
            END ATTR
            PIN O XLXN_138(15:0)
        END BLOCK
        BEGIN BLOCK XLXI_42 obuf
            PIN I XLXN_121
            PIN O TRIG
        END BLOCK
        BEGIN BLOCK XLXI_50 fdc
            PIN C XLXN_33
            PIN CLR XLXN_131
            PIN D XLXN_151
            PIN Q XLXN_128
        END BLOCK
        BEGIN BLOCK XLXI_53 constant
            BEGIN ATTR CValue "0000"
                DELETE all:1 sym:0
                EDITNAME all:1 sch:0
                VALUETYPE BitVector 32 Hexadecimal
            END ATTR
            PIN O XLXN_142(15:0)
        END BLOCK
        BEGIN BLOCK XLXI_59 cb16ce
            PIN C XLXN_151
            PIN CE XLXN_14
            PIN CLR XLXN_103
            PIN CEO
            PIN Q(15:0) XLXN_139(15:0)
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_61 compm16
            PIN A(15:0) PULSECOUNT(15:0)
            PIN B(15:0) XLXN_139(15:0)
            PIN GT
            PIN LT XLXN_131
        END BLOCK
        BEGIN BLOCK XLXI_62 comp16
            PIN A(15:0) XLXN_139(15:0)
            PIN B(15:0) XLXN_138(15:0)
            PIN EQ XLXN_103
        END BLOCK
        BEGIN BLOCK XLXI_63 comp16
            PIN A(15:0) XLXN_139(15:0)
            PIN B(15:0) XLXN_142(15:0)
            PIN EQ XLXN_121
        END BLOCK
        BEGIN BLOCK XLXI_64 cb4ce
            PIN C XLXN_33
            PIN CE XLXN_32
            PIN CLR XLXN_152
            PIN CEO
            PIN Q0
            PIN Q1 XLXN_148
            PIN Q2 XLXN_146
            PIN Q3
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_65 gnd
            PIN G XLXN_152
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 608 752 R0
        INSTANCE XLXI_2 1600 752 R0
        BEGIN BRANCH LED(7:0)
            WIRE 1824 720 2032 720
        END BRANCH
        IOMARKER 2032 720 LED(7:0) R0 28
        INSTANCE XLXI_11 624 1216 R0
        BEGIN BRANCH XLXN_14
            WIRE 688 1216 688 1328
            WIRE 688 1328 768 1328
            WIRE 768 1328 784 1328
        END BRANCH
        IOMARKER 400 720 PPORT(7:0) R180 28
        BEGIN BRANCH PPORT(7:0)
            WIRE 400 720 608 720
        END BRANCH
        BEGIN BRANCH CLOCK
            WIRE 176 1792 208 1792
        END BRANCH
        INSTANCE XLXI_7 208 1824 R0
        IOMARKER 176 1792 CLOCK R180 28
        INSTANCE XLXI_19 0 1104 R0
        INSTANCE XLXI_21 560 1840 R0
        BEGIN BRANCH XLXN_33
            WIRE 64 1424 64 1680
            WIRE 64 1680 288 1680
            WIRE 288 1680 448 1680
            WIRE 448 1680 448 1792
            WIRE 64 1424 128 1424
            WIRE 288 1536 288 1680
            WIRE 288 1536 1776 1536
            WIRE 432 1792 448 1792
            WIRE 1776 1216 1776 1536
            WIRE 1776 1216 2560 1216
        END BRANCH
        BEGIN BRANCH XLXN_32
            WIRE 64 1104 64 1120
            WIRE 64 1120 64 1360
            WIRE 64 1360 128 1360
        END BRANCH
        INSTANCE XLXI_8 3088 1216 R0
        BEGIN BRANCH PULSE
            WIRE 3312 1184 3344 1184
        END BRANCH
        IOMARKER 3344 1184 PULSE R0 28
        BEGIN INSTANCE ALLONES 1136 1792 R0
        END INSTANCE
        INSTANCE XLXI_42 3088 1328 R0
        BEGIN BRANCH TRIG
            WIRE 3312 1296 3344 1296
        END BRANCH
        IOMARKER 3344 1296 TRIG R0 28
        BEGIN BRANCH XLXN_103
            WIRE 784 1488 784 1504
            WIRE 784 1504 784 1520
            WIRE 784 1520 1024 1520
            WIRE 1024 1520 1024 1952
            WIRE 1024 1952 1792 1952
            WIRE 1696 1728 1792 1728
            WIRE 1792 1728 1792 1952
        END BRANCH
        BEGIN BRANCH XLXN_121
            WIRE 2656 1968 2960 1968
            WIRE 2960 1296 3072 1296
            WIRE 3072 1296 3088 1296
            WIRE 2960 1296 2960 1968
        END BRANCH
        INSTANCE XLXI_50 2560 1344 R0
        BEGIN BRANCH XLXN_128
            WIRE 2944 1088 3008 1088
            WIRE 3008 1088 3008 1184
            WIRE 3008 1184 3088 1184
        END BRANCH
        BEGIN BRANCH XLXN_131
            WIRE 1696 1200 2128 1200
            WIRE 2128 1200 2128 1312
            WIRE 2128 1312 2560 1312
        END BRANCH
        BEGIN INSTANCE XLXI_53 1824 2032 R0
        END INSTANCE
        INSTANCE XLXI_59 784 1520 R0
        INSTANCE XLXI_61 1312 1392 R0
        INSTANCE XLXI_62 1312 1952 R0
        INSTANCE XLXI_63 2272 2192 R0
        BEGIN BRANCH XLXN_138(15:0)
            WIRE 1280 1824 1296 1824
            WIRE 1296 1824 1312 1824
        END BRANCH
        BEGIN BRANCH XLXN_139(15:0)
            WIRE 1168 1264 1232 1264
            WIRE 1232 1264 1312 1264
            WIRE 1232 1264 1232 1424
            WIRE 1232 1424 1232 1632
            WIRE 1232 1632 1312 1632
            WIRE 1232 1424 1808 1424
            WIRE 1808 1424 1808 1872
            WIRE 1808 1872 2272 1872
        END BRANCH
        BEGIN BRANCH XLXN_142(15:0)
            WIRE 1968 2064 1984 2064
            WIRE 1984 2064 2272 2064
        END BRANCH
        BEGIN BRANCH PULSECOUNT(7:0)
            WIRE 832 720 1600 720
        END BRANCH
        BEGIN BRANCH PULSECOUNT(15:0)
            WIRE 1184 944 1184 1072
            WIRE 1184 1072 1312 1072
        END BRANCH
        INSTANCE XLXI_64 128 1552 R0
        BEGIN BRANCH XLXN_146
            WIRE 512 1232 544 1232
            WIRE 544 1232 544 1776
            WIRE 544 1776 560 1776
        END BRANCH
        BEGIN BRANCH XLXN_148
            WIRE 512 1168 576 1168
            WIRE 576 1168 576 1600
            WIRE 512 1600 576 1600
            WIRE 512 1600 512 1712
            WIRE 512 1712 560 1712
        END BRANCH
        BEGIN BRANCH XLXN_151
            WIRE 704 1392 784 1392
            WIRE 704 1392 704 1568
            WIRE 704 1568 896 1568
            WIRE 896 1568 896 1744
            WIRE 896 1744 896 1936
            WIRE 896 1936 1760 1936
            WIRE 816 1744 896 1744
            WIRE 1760 1088 1760 1936
            WIRE 1760 1088 2560 1088
        END BRANCH
        INSTANCE XLXI_65 64 1680 R0
        BEGIN BRANCH XLXN_152
            WIRE 128 1520 128 1552
        END BRANCH
    END SHEET
END SCHEMATIC
