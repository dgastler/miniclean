#include "v1720WFD.h"
#include "xmlHelper.h"

int v1720::a32d32r(uint32_t address,uint32_t &value)
{
  int CAENret = 0;
  CAENret = CAENVME_ReadCycle(Handle,address,(void*)&value,cvA32_U_DATA,cvD32);
  if(CAENret != 0)
    {
      fprintf(stderr,"%s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  return(CAENret);
}

int v1720::a32d32w(uint32_t address,uint32_t value)
{
  int CAENret = 0;
  CAENret = CAENVME_WriteCycle(Handle,address,(void*)&value,cvA32_U_DATA,cvD32);
  if(CAENret != 0)
    {
      fprintf(stderr,"%s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  return(CAENret);
}
//int v1720::a32MBLTr(uint32_t address,char * buffer,uint32_t bufferSize,uint32_t &readSize)
int v1720::a32MBLTr(uint32_t address,uint8_t * buffer,uint32_t bufferSize,uint32_t &readSize)
{
  int CAENret = 0;
  int CAENreadSize = 0;
  int CAENMBLTSize = 0xFF; ///MAKES THINGS WORK. DON'T change without figuring out what this means.
  uint32_t position = 0;
  //Read out transfers in blocks of CAENMBLTSize.
  //Stop on full read out or BERR terminated transfer
  while(position < bufferSize)
    {
      VMEtransfers++;
      //      CAENreadSize = 0;
      if((bufferSize - position) >= (uint32_t) CAENMBLTSize)
	{
	  CAENret = CAENVME_MBLTReadCycle(Handle,
					  address,
					  buffer + position,
					  CAENMBLTSize,
					  cvA32_U_MBLT,
					  &CAENreadSize);
	}
      else
	{
	  CAENret = CAENVME_MBLTReadCycle(Handle,
					  address,
					  buffer + position,
					  bufferSize - position,
					  cvA32_U_MBLT,
					  &CAENreadSize);
	}
      position += CAENreadSize; 


      if(CAENret == cvBusError)
	{
	  if(CAENreadSize > 0)
	    {
	      //BERR terminated MBLT
	      goodBERR++;
	      break;
	    }
	  else
	    {
	      badBERR++;
	    }
	}
    }
  readSize = position;
  if(CAENret != 0 && CAENret != cvBusError)
    {
      fprintf(stderr,"CAEN: %s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
    }
  return(CAENret);
  



  //  CAENret = CAENVME_BLTReadCycle(Handle,address,buffer,bufferSize,cvA32_S_BLT,cvD32,&CAENreadSize);
  //  int size = bufferSize;
  //  CAENret = CAENVME_BLTReadCycle(Handle,address,buffer,size,cvA32_U_BLT,cvD32,&CAENreadSize);
//  readSize = CAENreadSize;
//  if(CAENret != 0)
//    {
//      fprintf(stderr,"CAEN: %s\n",CAENVME_DecodeError( (CVErrorCodes) CAENret) );
//    }
//  return(CAENret);
}

int v1720::SetRegister(uint32_t address,uint32_t value,const char * Name)
{
  int ret = 0;
  uint32_t RegisterReturn = 0;
  //Write value
  ret = a32d32w(address,value);
  //Read it back
  ret += a32d32r(address,RegisterReturn);
  //Make sure everythign is correct.
  if(RegisterReturn != value)
    {
      fprintf(stderr,"%s write/readback error 0x%08X vs 0x%08X\n",Name,RegisterReturn,value);
      ret++;
    }
  return(ret);
}
