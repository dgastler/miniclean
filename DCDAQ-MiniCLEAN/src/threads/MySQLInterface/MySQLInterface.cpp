#include <MySQLInterface.h>

MySQLInterface::MySQLInterface(std::string Name)
{
  Ready = false;
  SetType("MySQLInterface");
  SetName(Name);  
  typeMap[DCMessage::ERROR]      = &MySQLInterface::SendError;
  typeMap[DCMessage::WARNING]    = &MySQLInterface::SendWarning;
  typeMap[DCMessage::STATISTIC]  = &MySQLInterface::SendStats;
  typeMap[DCMessage::TEXT]       = &MySQLInterface::SendText;
  typeMap[DCMessage::RUN_NUMBER] = &MySQLInterface::ProcessRunNumber;
  typeMap[DCMessage::COMMAND]    = &MySQLInterface::ProcessCommandMessage;
  polling_Interval = 1;
  SetSelectTimeout(polling_Interval,0);
  next_Time = time(NULL);
  StoppingRun = false;
  SettingUpNewRun = false;
}


bool MySQLInterface::Setup(xmlNode * SetupNode,
			   std::vector<MemoryManager*> &/*MemManager*/)
{

  if(FindSubNode(SetupNode,"DAQDB")==NULL)
    {
      PrintError("Faild to find DAQ DB");
    }
  else
    {
      DAQDB_Connection.SetConnect(FindSubNode(SetupNode,"DAQDB"));
      std::string MySQLErrorCheck = DAQDB_Connection.OpenConnect();
  
      //MySQLErrorCheck is a std::string, 
      //if it does not return "success" 
      //there is an error with the connection or query
      if(!MySQLErrorCheck.empty())
	{
	  PrintError(MySQLErrorCheck.c_str());
	  return false;
	}
    }

  if(FindSubNode(SetupNode,"SCDB")==NULL)
    {
      PrintError("Faild to find Slow Control (SC) DB");
    }
  else
    {
      SCDB_Connection.SetConnect(FindSubNode(SetupNode,"ScDB"));
      std::string MySQLErrorCheck = SCDB_Connection.OpenConnect();
  
      //MySQLErrorCheck is a std::string, 
      //if it does not return "success" 
      //there is an error with the connection or query
      if(!MySQLErrorCheck.empty())
	{
	  PrintError(MySQLErrorCheck.c_str());
	  return false;
	}
    }


  //Add the local session to the print status
  sRoute route;
  //Errors

  //Check if we are registering ourselves for other DCDAQ sessions
  xmlNode * remoteNode = NULL;
  if((remoteNode = FindSubNode(SetupNode,"REMOTE")) != NULL)
    {      
      //Get the IP address of the remote print stats
      size_t routeNodes = NumberOfNamedSubNodes(remoteNode,"ROUTE");
      for(size_t i =0 ; i < routeNodes;i++)
	{
	  xmlNode * routeNode = FindIthSubNode(remoteNode,
					       "ROUTE",
					       i);
	  //Get the destination address and route type
	  if(routeNode != NULL)
	    {
	      std::string IP;
	      std::string type;
	      int goodNode = 0;
	      goodNode += GetXMLValue(routeNode,
				      "TYPE",
				      GetName().c_str(),
				      type);
	      goodNode += GetXMLValue(routeNode,
				      "ADDR",
				      GetName().c_str(),
				      IP);
	      //If there were no errors, set up the route
	      if(goodNode == 0)
		{
		  route.type = DCMessage::GetTypeFromName(type.c_str());
		  route.route.Set(DCMessage::DCAddress::BroadcastName,IP);
		  routes.push_back(route);
		}
	    }
	}

    }
  
  for(size_t i = 0; i < routes.size();i++)
    {
      //Create a DCMessage to sent out with this data
      DCMessage::Message message;
      message.SetType(DCMessage::REGISTER_TYPE);
      message.SetDestination(routes[i].route); 
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(DCMessage::GetTypeName(routes[i].type));
      message.SetData(&(data[0]),data.size());

      std::string buffer("Routing ");
      buffer += DCMessage::GetTypeName(routes[i].type);
      buffer += " from ";
      buffer += routes[i].route.GetStr();
      buffer += " to this session.\n";
      Print(buffer.c_str());
      
      SendMessageOut(message);
    }
  Ready = true;
  return true;
}
