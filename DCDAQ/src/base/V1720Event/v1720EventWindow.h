#ifndef __V1720_EVENT_WINDOW__
#define __V1720_EVENT_WINDOW__

#include <iostream>
#include <stdint.h> //uint32_t
#include <vector>
#include <cstdio>

class v1720EventWindow
{
 public:
  v1720EventWindow();
  ~v1720EventWindow();
  uint16_t GetSample(uint32_t i);
  uint32_t GetSize(); // size in samples.  NOT MEMORY!
  //Give people the raw data if they want it.   Don't keep this pointer though!
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);}; // size in memory NOT SAMPLES!
  void Setup(uint32_t * _rawData,uint32_t _size,bool _twoPack);
  uint32_t timeOffset;
 private:
  bool twoPack;
  //This class NEVER owns this data!
  //It is either under the control of the v1720Event class
  //or external code.
  uint32_t * rawData;
  uint32_t size;
  uint16_t GetSample20pk(uint32_t i);
  uint16_t GetSample25pk(uint32_t i);
};

#endif
