#include <DCMessageProcessor.h>

std::vector<DCThread *> DCMessageProcessor::RemoteRoutesForAddress(const DCMessage::DCAddress & dest)
{
 //An iterator for the messageAddrToNameMap
  std::map<DCMessage::DCAddress , std::string >::iterator it;

  //Search for remote destinations to send this to. 
  //and add them to a vector to actually have messages sent to them
  //this is to stop a race condtion invalidating out iterator
  std::vector<DCThread *> remoteThreads;
  for(it = messageAddrToNameMap.begin(); 
      it != messageAddrToNameMap.end();
      it++)
    {
      //If we have an address match
      //it->first is the uint32_t part of a struct in_addr	     
      if(
	 //Message destination is a broadcast 
	 dest.IsBroadcastAddr() ||
	 //This route says route everythign here
	 (it->first).IsBroadcastAddr() ||
	 //Message destination matches this route
	 dest.IsEqualAddr(it->first)
	 )
	{
	  remoteThreads.push_back(FindThread(it->second));
	  if(remoteThreads.back() == NULL)
	    {			     
	      DCMessage::SingleUINT64 errorData;
	      snprintf(errorData.text,
		       DCMessage::textSize,
		       "Can not find routing thread %s.\n",
		       it->second.c_str());
	      BuildError(errorData);
	    }
	}
    }   
  return remoteThreads;
}

bool DCMessageProcessor::RouteRemoteMessage(DCMessage::Message message)
{
  bool runReturn = true;

  //Get the routing information
  DCMessage::DCAddress dest = message.GetDestination();

  std::vector<DCThread *> remoteThreads = RemoteRoutesForAddress(dest);

  bool messageSent = false;
  for(size_t iThread = 0; iThread < remoteThreads.size();iThread++)
    {
      if(remoteThreads[iThread] != NULL)
	{
	  DCMessage::Message copyMessage = message;
	  messageSent = remoteThreads[iThread]->SendMessageIn(copyMessage);
	}
    }

  //If it is a broadcast message, route it here as well
  if(message.GetDestination().IsBroadcastAddr())
    {
      messageSent = true;
      DCMessage::Message copyMessage = message;
      message.SetSource("DCDAQ");
      //Route this message locally with the same name as before, but with
      //the default remote address
      copyMessage.SetDestination((message.GetDestination()).GetName());
      runReturn = ProcessMessage(copyMessage);
    }

  //If the message could not be routed send an error
  if(!messageSent)
    {
      //Send an CAN_NOT_ROUTE message back to the source
      DCMessage::Message response;
      response.SetDestination(message.GetSource());
      //Set the source of this message as the destination of the failed thread      
      response.SetSource(message.GetDestination());
      response.SetType(DCMessage::CAN_NOT_ROUTE);

      
      std::stringstream ss;
      ss << "Can not route address " 
	 << message.GetDestination().GetStr();
      DCMessage::Message warning;
      warning.SetSource("DCDAQ");
      warning.SetType(DCMessage::WARNING);
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(ss.str());
      warning.SetData(&(data[0]),data.size());

      ProcessMessage(response);
      ProcessMessage(warning);
    }
  return runReturn;
}

void DCMessageProcessor::PrintRemoteRouteTables()
{
  //Build a streamer to put the list in.
  std::stringstream ss;
  //       1234567890123456789012345678901234567890123456789
  ss << "\n------------------------------------------------\n";
  ss << "Route                        Connection Thread\n";
  ss << "------------------------------------------------\n";
  std::map<DCMessage::DCAddress , std::string >::iterator it;
  for(it = messageAddrToNameMap.begin(); 
      it != messageAddrToNameMap.end();
      it++)
    {
      std::string route = it->first.GetStr();      
      //Add Route info and whitespace
      ss << " " << route << std::string(25-route.size()-1,' ');
      //Add the local thread handling this route
      ss << it->second << std::endl;
    }
  ss << "------------------------------------------------\n";
  //Print
  DCMessage::Message message;
  message.SetDestination();
  message.SetType(DCMessage::TEXT);
  DCMessage::DCString dcString;
  std::vector<uint8_t> data = dcString.SetData(ss.str());
  message.SetData(&(data[0]),data.size());
  ProcessMessage(message);
}
