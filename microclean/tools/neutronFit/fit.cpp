#include <iostream>


#include "TFile.h"
#include "TSpline.h"
#include "TF1.h"
#include "TH1F.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TKey.h"
#include "SplineFunction.h"
#include "TCanvas.h"
#include "THStack.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TMinuit.h"
#include "TROOT.h"

#include "/home/dgastler/.rootlogon.C"

TH1F * hPointer;
SplineFunction * sPointer;
void fcn(int &npar, double *gin, double &f, double *par, int iflag) 
{
  f = 0;
  double x = 0;
  double y = 0;
  double fy = 0;
  //Loop over each bin
  for(int i = 1;(i <= hPointer->GetNbinsX());i++)
    {
      //if "i" is in our plotted range.
      if((i <= hPointer->GetXaxis()->GetLast()) &&
	 (i >= hPointer->GetXaxis()->GetFirst()))
	{
	  x = hPointer->GetBinCenter(i);
	  y = hPointer->GetBinContent(i);
	  fy = (*sPointer)(&x,par);
	  if(y != 0.0f)
	    {
	      f += ((y - fy)*(y - fy))/fabs(y);
	    }
	  else
	    {
	      //	      fprintf(stderr,"HELP! I wanted to divide by zero! %f\t%f\t%f\t%f\t%f \n",x,y,fy,par[0],par[1]);
	    }
	}
    }
}

int main(int argc, char ** argv)
{
  rootlogon();
  if(argc < 2)
    {
      fprintf(stderr,"%s SplineFile OutFile InFiles\n",argv[0]);
      return(0);
    }
  TFile * SplineFile = TFile::Open(argv[1],"READ");
  if(!SplineFile)
    {
      fprintf(stderr,"Bad spline file.\n");
      return(0);
    }

  std::vector<float> Energy(20,0);
  Energy[0]=10.6;
  Energy[1]=14.5;
  Energy[2]=19.0;
  Energy[3]=22.3;
  Energy[4]=27.7;
  Energy[5]=32.6;
  Energy[6]=38.9;
  Energy[7]=44.5;
  Energy[8]=51.6;
  Energy[9]=59.1;
  Energy[10]=66.9;
  Energy[11]=72.3;
  Energy[12]=79.2;
  Energy[13]=90.6;
  Energy[14]=97.9;
  Energy[15]=114.2;
  Energy[16]=182.5;
  Energy[17]=190.7;
  Energy[18]=211.3;
  Energy[19]=238.9;

  std::vector< TH1F * > Histograms;
  std::vector<int> Position;
  std::vector<float> SciEff;
  std::vector<float> SciEffErr;
  std::vector<float> SciEffErrp;
  std::vector<float> SciEffErrm;

  std::vector<int> Position2;
  std::vector<float> SciEff2;
  std::vector<float> SciEffErr2;

  std::vector<float> DataTOF;
  std::vector<float> MCTOF;
  std::vector<float> DataTOFerr;
  std::vector<float> MCTOFerr;

  char * Buffer = new char[100];
  TCanvas * c1 = new TCanvas("c1","c1",500,500);  
  TCanvas * c2 = new TCanvas("c2","c2",500,500);  
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetFillColor(0);
  gStyle->SetCanvasColor(0);



  TH1F * Light;
  TH1F * TOF;
  THStack * stack;
  TH1F * s0;
  TH1F * s1;
  TH1F * s2;
  TH1F * s3;
  TLegend * sLegend;
  TF1 * functionPointer;
  TFile * InFile;
  TGaxis * a2;
  TPaveText * newTitle;
  TH1F * FitHistogram;
  SplineFunction * SFunction;
  TF1 * FitFunction;
  
  

  for(int filenumber = 3;filenumber < argc;filenumber++)
    {

      //Figure out energy
      std::string Title(argv[filenumber]);
      std::string::size_type start = Title.find("SE_")+3;
      Title = Title.substr(start,Title.size()-start);
      std::string::size_type end = Title.find("_");
      std::string TitleEnd = Title.substr(end+1,(Title.substr(end+1,Title.size()-(end+1))).find("_"));
      int subrun = atoi(TitleEnd.c_str());
      if(subrun)
	{
	  printf("Processing position %s sub run %d\n",Title.c_str(),subrun);
	}
      //      fprintf(stderr,"%s => %d\n",TitleEnd.c_str(),atoi(TitleEnd.c_str()));
      Title = Title.substr(0,end);
      printf("%s\n",Title.c_str());
      if((start == std::string::npos)||(end == std::string::npos))
	{
	  fprintf(stderr,"Bad input file name\n");
	  return(0);
	}
      int ID = atoi(Title.c_str()) - 1;
      if((ID < 0) || (((unsigned int) ID) > (Energy.size() -1)))
	{
	  fprintf(stderr,"Bad input file name ID\n");
	  return(0);
	}
      //Open Data files
      InFile = TFile::Open(argv[filenumber],"READ");
      if(!InFile)
	{
	  fprintf(stderr,"Bad data file %s.\n",argv[3]);
	  return(0);
	}







      //========================================================================================================================================
      //Generate TOF plots!
      //========================================================================================================================================
      c1->cd();
      c1->Clear();
      

      //Load histograms
      TOF = (TH1F*) InFile->Get("cTOF")->Clone();
      if(!TOF)
	{
	  fprintf(stderr,"Bad histogram.\n");
	  return(0);
	}     

      sprintf(Buffer,"TOF_pos%02d",ID);
      stack = (THStack *) SplineFile->Get(Buffer)->Clone();
      if(!stack)
	{
	  fprintf(stderr,"Bad histogram.\n");
	  return(0);
	}
      
      //Get TOF for this 
      DataTOF.push_back(TOF->GetMean());
      DataTOFerr.push_back(TOF->GetMeanError());

      //Setup Data TOF histogram
      TOF->Scale(1.0/TOF->Integral());      
      TOF->GetListOfFunctions()->Clear();
      TOF->SetLineWidth(4);
      TOF->SetLineColor(kBlack);
 
      //Get MC TOF histograms from THStack
      s0 = ((TH1F*) stack->GetHists()->At(0));
      s1 = ((TH1F*) stack->GetHists()->At(1));
      s2 = ((TH1F*) stack->GetHists()->At(2));
      s3 = ((TH1F*) stack->GetHists()->At(3));
      if(!s0 || !s1 || !s2 || !s3)
	{
	  fprintf(stderr,"Error in histogram stack\n");
	  return(0);
	}
      //Get TOF for neutrons
      MCTOF.push_back(s0->GetMean());
      MCTOFerr.push_back(s0->GetMeanError());

      //Remove the gaussian fit to the neutrons.
      functionPointer = (TF1*) s0->GetListOfFunctions()->FindObject("gaus");
      s0->GetListOfFunctions()->Remove(functionPointer);

      //Add the DATA TOF histogram to the MC Legend
      sLegend = (TLegend*) s0->GetListOfFunctions()->At(0);
      //      sLegend->AddEntry(TOF,"microCLEAN data");

      //Find MC scale factor
      Double_t TOFScale = 1.0/(s0->Integral() +
			       s1->Integral() +
			       s2->Integral() +
			       s3->Integral());
      s0->Scale(TOFScale);
      s1->Scale(TOFScale);
      s2->Scale(TOFScale);
      s3->Scale(TOFScale);

      //Draw both histograms so we set the maximum and the Xaxis is built
      TOF->Draw();
      stack->Draw();


      //Setup Axis Ranges
      stack->GetXaxis()->SetRangeUser(0,200e-9);
      stack->GetXaxis()->SetAxisColor(0);
      stack->GetXaxis()->SetLabelColor(0);
      stack->GetXaxis()->SetTitleColor(0);

      TOF->GetXaxis()->SetRangeUser(0,200e-9);
      TOF->GetXaxis()->SetAxisColor(0);
      TOF->GetXaxis()->SetLabelColor(0);
      TOF->GetXaxis()->SetTitleColor(0);

      stack->Draw("");

      a2 = new TGaxis(0,0,200e-9,0,0,200,510,"");
      a2->SetTitle("Neutron time of flight (ns)");
      a2->SetTextColor(kBlack);

      newTitle = new TPaveText(.1,.91,.9,1,"blNDC");
      newTitle->SetTextColor(kBlack);
      newTitle->SetFillColor(0);
      sprintf(Buffer,"Position %2d: %3.3fkeVr",ID,Energy[ID]);
      newTitle->AddText(Buffer);

      a2->Draw("");
      if(subrun)
	{
	  sprintf(Buffer,"TOF_%02d_%02d.eps",ID,subrun);
	}
      else
	{
	  sprintf(Buffer,"TOF_%02d.eps",ID);
	}
      c1->Print(Buffer);
      c1->Clear();



      delete newTitle;
      delete a2;
      delete stack;
      delete s0;
      delete s1;
      delete s2;
      delete s3;
      delete TOF;


      //========================================================================================================================================
      //End of TOF plots!
      //========================================================================================================================================



      c2->cd();

      //Get the data for fitting
      if(subrun)
	{
	  sprintf(Buffer,"%05.1f_Neutron_scatters_%02d",Energy[ID],subrun);
	}
      else
	{
	  sprintf(Buffer,"%05.1f_Neutron_scatters",Energy[ID]);
	}
      Light = (TH1F*) InFile->Get("cLight");
      gROOT->cd();
      Light = (TH1F*) (Light)->Clone(Buffer);

      if(!Light)
	{
	  fprintf(stderr,"Bad histogram.\n");
	  return(0);
	}
      //      hPointer = Light; // 2009-01-12


      
      //New ReBin
//      if(Energy[ID] > 40)
//	{
//	  Light->Rebin();
//	  if(Energy[ID] > 150)
//	    {
//	      Light->Rebin();
//	    }
//	}
      
      if(Energy[ID] < 25)
	Light->Rebin(2);
      else if(Energy[ID] < 50)
	Light->Rebin(2);
      else if(Energy[ID] < 90)
	Light->Rebin(2);
      else if(Energy[ID] < 190)
	Light->Rebin(4);
      else
	Light->Rebin(5);
      


      //GetSplinePointer for fitting function
      sprintf(Buffer,"%02d_NeutronSpectrum",ID);
      FitHistogram = (TH1F*) ((TH1F*)SplineFile->Get(Buffer))->Clone(Buffer);
      if(!FitHistogram)
	{
	  fprintf(stderr,"Bad spline histogram: %d\n",ID);
	  return(0);
	}

      std::vector<float> X;
      std::vector<float> Y;
      //Fill vectors for spline function and build an energy calibrated specturm.
      for(int ihtemp = 1;ihtemp <= FitHistogram->GetNbinsX(); ihtemp++)
	{
	  X.push_back(FitHistogram->GetBinCenter(ihtemp));
	  Y.push_back(FitHistogram->GetBinContent(ihtemp));
	}

      
      double Emin = 2;
      double Emax = Energy[ID];
      //      if(Emax < 100)
      //      Emax = (100 + Emax)/2.0;
      //      Emax = (100 + Emax)/2.0;
      if(Emax < 25)
	Emax = 80;
      else if(Emax < 50)
	Emax = 100;
      else if(Emax < 79)
	Emax = 110;
      else if(Emax < 180)
	Emax = 160;
      else
	Emax = 180;

      Light->GetXaxis()->SetRangeUser(Emin,Emax);

            
      //Allocate fit functions.
      gROOT->cd();
      //The range for the SplineFunction must be larger than the fit range.
      //Otherwise there is some strange fit-range effects.
      SFunction = new SplineFunction(X,Y,0,Light->GetXaxis()->GetXmax());
      sPointer = SFunction;
      if(subrun)
	{
	  sprintf(Buffer,"%02d_Spline_%02d",ID,subrun);
	}
      else
	{
	  sprintf(Buffer,"%02d_Spline",ID);
	}
      FitFunction = new TF1(Buffer,SFunction,
			    Emin,
			    Emax,
			    2);
      FitFunction->SetParNames("Norm","S.E.");
      FitFunction->SetNpx(10000);
      FitFunction->SetLineColor(kRed);

      //Set the global hPointer to Light for function fcn
      hPointer = Light;
      TMinuit * gMinuit = new TMinuit(2);
      gMinuit->SetFCN(fcn);


      Double_t arglist[10];
      Int_t ierflg = 0;
      
      //Set error definition
      // 1   : Chi square
      // 0.5 : Negative log likelihood
      //      gMinuit->SetErrorDef(0.5);
      //Can instead use following 2 lines... if you want
      arglist[0] = 1;
      gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
      
      //Minimization strategy
      // 0 : fewer function calls (less reliable)
      // 1 : standard
      // 2 : try to improve minimum (slower)
      arglist[0] = 1;
      gMinuit->mnexcm("SET STR", arglist, 1, ierflg);
      


      //Set starting values, bounds, and step sizes for parameters
      static Double_t vstart[2] = {2*Light->GetEntries(),0.2};
      switch (ID)
	{
	case 0:
	  vstart[0] = 1800;
	  vstart[1] = .35;
	  break;
	case 1:
	  vstart[0] = 3700;
	  vstart[1] = .35;
	  break;
	case 2:
	  vstart[0] = 5000;
	  vstart[1] = .35;
	  break;
	case 3:
	  vstart[0] = 6000;
	  vstart[1] = .35;
	  break;
	case 4:
	  vstart[0] = 1700;
	  vstart[1] = .3;
	  break;
	case 5:
	  vstart[0] = 2855;
	  vstart[1] = .25;
	  break;
	case 6:
	  vstart[0] = 2100;
	  vstart[1] = .3;
	  break;
	case 7:
	  vstart[0] = 4200;
	  vstart[1] = .3;
	  break;
	case 8:
	  vstart[0] = 2250;
	  vstart[1] = .28;
	  break;
	case 9:
	  vstart[0] = 2700;
	  vstart[1] = .3;
	  break;
	case 10:
	  vstart[0] = 5000;
	  vstart[1] = .25;
	  break;
	case 11:
	  vstart[0] = 2500;
	  vstart[1] = .26;
	  break;
	case 12:
	  vstart[0] = 2500;
	  vstart[1] = .25;
	  break;
	case 13:
	  vstart[0] = 2400;
	  vstart[1] = .3;
	  break;
	case 14:
	  vstart[0] = 3000;
	  vstart[1] = .25;
	  break;
	case 15:
	  vstart[0] = 2200;
	  vstart[1] = .3;
	  break;
	case 16:
	  vstart[0] = 1400;
	  vstart[1] = .3;
	  break;
	case 17:
	  vstart[0] = 2000;
	  vstart[1] = .3;
	  break;
	case 18:
	  vstart[0] = 2000;
	  vstart[1] = .26;
	  break;
	case 19:
	  vstart[0] = 3700;
	  vstart[1] = .25;
	  break;
	default:
	  vstart[1] = 0.3;
	  vstart[0] = 1.2*Light->GetEntries()*Light->GetBinWidth(1);
	}
      
      if(subrun)
	{
	  vstart[1] = 0.3;
	  vstart[0] = 1.2*Light->GetEntries()*Light->GetBinWidth(1);
	}

      printf("Initital %d(%f) Norm guess: %f\n",ID,Energy[ID],vstart[0]);

      //      static Double_t step[2]   = {10, 0.05};
      static Double_t step[2]   = {1, 0.001};
      //      static Double_t minvals[2]= {Light->GetEntries()/2.0, 0.1};
      static Double_t minvals[2]= {0, 0};
      //      static Double_t maxvals[2]= {50000, 0.6};
      static Double_t maxvals[2]= {0, 0};
      gMinuit->mnparm(0,"n",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
      gMinuit->mnparm(1,"e",vstart[1],step[1],minvals[1],maxvals[1],ierflg);


      //Fill fitted variables and associated errors
      Double_t nval, sval, nvalerr, svalerr;

      arglist[0] = 5000;     //max number of iterations
      arglist[1] = 0.01;    //tolerance
      gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);

      TString  name; // ignored
      Double_t bnd1, bnd2; // ignored
      //      gMinuit->mnpout(0,name,nval,nvalerr,bnd1,bnd2,ierflg);
      //      gMinuit->mnpout(1,name,sval,svalerr,bnd1,bnd2,ierflg);
      ierflg = 0;      
      arglist[0] = 5000;     //max number of iterations
      gMinuit->mnexcm("MINOS", arglist, 1, ierflg);
      if(ierflg)
	{	  
	  fprintf(stderr,"WTF MATE? 3.2 %d %d\n",ierflg,ID);
	  ierflg = 0;
	  //	  return(0);
	}


      //      gMinuit->GetParameter(0,nval,nvalerr);
      //      gMinuit->GetParameter(1,sval,svalerr);
      gMinuit->mnpout(0,name,nval,nvalerr,bnd1,bnd2,ierflg);
      gMinuit->mnpout(1,name,sval,svalerr,bnd1,bnd2,ierflg);
      Double_t uselessVariable1;
      Double_t uselessVariable2;
      gMinuit->mnerrs(1,bnd1,bnd2,uselessVariable1,uselessVariable2);
      printf("%03.0f -> %05.3f +%05.3f %05.3f   +/- %05.3f\n",Energy[ID],sval,bnd1,bnd2,svalerr);

      



      

      //display values of the chi squared.
      double f = 0;
      double par[2] = {nval,sval};
      sprintf(Buffer,"chiOut%03.0f_%01d.dat",Energy[ID],subrun);
      FILE * chiOutFile = fopen(Buffer,"w");
      //      for(par[1] = sval - svalerr ; par[1] < sval + svalerr ; par[1] += 2.0*svalerr/100.0)
      for(par[1] =0 ; par[1] < sval + 0.1 ; par[1] += 0.001)
	{
	  f = 0;
	  int npar = 2;
	  double gin = 0;
	  int iflag = 0;
	  fcn(npar,&gin,f,par,iflag) ;
	  fprintf(chiOutFile,"%f\t%f\n",par[1],f);
	}


      FitFunction->SetParameter(0,nval);
      FitFunction->SetParameter(1,sval);
      FitFunction->SetParError(0,nvalerr);
      //      FitFunction->SetParError(1,svalerr);
      if(fabs(bnd1) > fabs(bnd2))
	{
	  FitFunction->SetParError(1,fabs(bnd1));
	}
      else
	{
	  FitFunction->SetParError(1,fabs(bnd2));
	}
      Light->GetListOfFunctions()->AddLast(FitFunction);

      //Add a image of the MC neutrons
      sprintf(Buffer,"SmearedEnergySpectrum_1_0_%02d",ID);
      TH1F * MCNeutronsRaw = (TH1F*)((TH1F*)SplineFile->Get(Buffer))->Clone();
      sprintf(Buffer,"SmearedEnergySpectrum_1_n_%02d",ID);
      TH1F * MCBackgrounds1Raw = (TH1F*)((TH1F*)SplineFile->Get(Buffer))->Clone();
      sprintf(Buffer,"SmearedEnergySpectrum_n_n_%02d",ID);
      TH1F * MCBackgrounds2Raw = (TH1F*)((TH1F*)SplineFile->Get(Buffer))->Clone();

      if(ID < 4) //25keVee
	{
	  MCNeutronsRaw->Rebin(2);
	}
      else if(ID < 8) //50keVee
	{
	  MCNeutronsRaw->Rebin(4);
	}
      else if(ID < 13) //90
	MCNeutronsRaw->Rebin(4);
      else if(ID < 16) //190
	MCNeutronsRaw->Rebin(5);
      else
	MCNeutronsRaw->Rebin(8);

      MCNeutronsRaw->Smooth(1);
      //      MCNeutronsRaw->Scale(1.0/MCNeutronsRaw->Integral());      


      //      TH1F * MCNeutrons = (TH1F*)MCNeutronsRaw->Clone();
      //      TH1F * MCNeutrons = (TH1F*)Light->Clone();

      sprintf(Buffer,"%05.1f_Neutrons",Energy[ID]);
      TH1F * MCNeutrons = new TH1F(Buffer,Buffer,MCNeutronsRaw->GetNbinsX(),0,sval*MCNeutronsRaw->GetXaxis()->GetXmax());
      MCNeutrons->SetTitle(Buffer);
      MCNeutrons->SetName(Buffer);
      MCNeutrons->GetListOfFunctions()->Clear();
      for(int i = 0; i <= MCNeutrons->GetNbinsX();i++)
	{
	  MCNeutrons->SetBinContent(i,0);
	}
      //Fill a new copy of the Light histogram with theneutron data
      //      Double_t MCNeutronScaling = Double_t(MCNeutronsRaw->Integral())/(MCNeutronsRaw->Integral() + MCBackgrounds1Raw->Integral() + MCBackgrounds2Raw->Integral());
      Double_t MCNeutronScaling = 1.0/(MCNeutronsRaw->Integral() + MCBackgrounds1Raw->Integral() + MCBackgrounds2Raw->Integral());
      for(int i = 0; i < MCNeutronsRaw->GetNbinsX();i++)
	{
	  MCNeutrons->Fill(
			   //New energy bin
			   sval*MCNeutronsRaw->GetBinCenter(i), 
			   //Properly scaled to be like the fit function.
			   MCNeutronsRaw->GetBinContent(i)*MCNeutronScaling*nval*sval
			   );
	}
      //      MCNeutrons->SetDrawOption("SAME");
      //      Light->GetListOfFunctions()->AddLast(MCNeutrons);
      Histograms.push_back(MCNeutrons);

      SciEff.push_back(sval);
      SciEffErr.push_back(svalerr);
      SciEffErrp.push_back(bnd1);
      SciEffErrm.push_back(bnd2);

      Histograms.push_back(Light);
      Position.push_back(ID);


      delete gMinuit;
      delete FitHistogram;
      InFile->Close();
      delete InFile;
    }



  hPointer = 0;
  sPointer = 0;
  
  TFile * OutFile = TFile::Open(argv[2],"CREATE");
  if(!OutFile)
    {
      fprintf(stderr,"Output file error.\n");
      return(0);
    }

  for(unsigned int i = 0; i < Histograms.size();i++)
    {
      ((TH1F*) Histograms[i])->Write();      
    }


  TGraphErrors * gr = new TGraphErrors(SciEff.size());
  TGraphErrors * gr2 = new TGraphErrors(SciEff.size());

  TGraphErrors * grb = new TGraphErrors(SciEff.size());
  TGraphErrors * grb2 = new TGraphErrors(SciEff.size());

  TGraphErrors * grTOF = new TGraphErrors(DataTOF.size());
  TGraphAsymmErrors * grA = new TGraphAsymmErrors(SciEff.size());


  for(unsigned int i = 0; i < DataTOF.size();i++)
    {
      grTOF->SetPoint(i,DataTOF[i],MCTOF[i]);
      grTOF->SetPointError(i,DataTOFerr[i],MCTOFerr[i]);
    }
  grTOF->SetName("TOF");
  grTOF->GetXaxis()->SetTitle("Data neutron TOF (ns)");
  grTOF->GetYaxis()->SetTitle("MC neutron TOF (ns)");
  grTOF->Fit("pol1");
  grTOF->Write();


  for(unsigned int i = 0; i < Position.size();i++)
    {
      gr->SetPoint(i,Energy[Position[i]],SciEff[i]);
      gr->SetPointError(i,3,SciEffErr[i]);
      grA->SetPoint(i,Energy[Position[i]],SciEff[i]);
      //      grA->SetPointError(i,3,3,-1.0*SciEffErrm[i],SciEffErrp[i]);
      grA->SetPointError(i,3,3,fabs(SciEffErrm[i]),fabs(SciEffErrp[i]));
      

      gr2->SetPoint(i,Energy[Position[i]],SciEff[i]*Energy[Position[i]]);
      gr2->SetPointError(i,3,SciEffErr[i]*Energy[Position[i]]);
      
    }
  gr->SetName("SE");
  gr->GetXaxis()->SetTitle("Recoil energy (keVr)");
  gr->GetYaxis()->SetTitle("Scintillation efficiency");
  gr->Write();


  
  gr2->SetName("SEEnergy");
  gr2->GetXaxis()->SetTitle("Recoil energy (keVr)");
  gr2->GetYaxis()->SetTitle("Measured energy (keVee)");
  gr2->Write();

  grb->SetName("SEb");
  grb->GetXaxis()->SetTitle("Recoil energy (keVr)");
  grb->GetYaxis()->SetTitle("Scintillation efficiency");
  grb->Write();
  
  grb2->SetName("SEEnergyb");
  grb2->GetXaxis()->SetTitle("Recoil energy (keVr)");
  grb2->GetYaxis()->SetTitle("Measured energy (keVee)");
  grb2->Write();

  grA->SetName("SEA");
  grA->GetXaxis()->SetTitle("Recoil energy (keVr)");
  grA->GetYaxis()->SetTitle("Scintillation efficiency");
  grA->Write();

  SplineFile->Close();
  delete SplineFile;  
  delete [] Buffer;
  delete c1;
  return(0);
}
