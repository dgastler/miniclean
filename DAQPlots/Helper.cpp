#define __DEFINE_GLOBALS__
#include "Helper.h"

double FindTGraphMax(TGraph * gr)
{
  double ret = -1E99;
  for(int i = 0; i < gr->GetN();i++)
    {
      if(gr->GetY()[i] > ret)
	ret = gr->GetY()[i];
    }
  return ret;
}

int  GetLastRunNumber(MYSQL * conn)
{
  int runNumber = -1;
  char query[] = "select max(num) from runs";
  MYSQL_RES *result;
  int query_state = mysql_query(conn,query);  
  if (query_state != 0)
    {
      printf("MySQL Query error:\t%s\n",mysql_error(conn));
      return false;
    }
  else
    {
      //Parse the returned MYSQL data
      result = mysql_store_result(conn); 
      unsigned int num_fields = mysql_num_fields(result);
      if(num_fields == 1)
	{
	  MYSQL_ROW row = mysql_fetch_row(result); 
	  unsigned long *lengths;
	  lengths = mysql_fetch_lengths(result);	 

	  char * stringEnd = row[0]+lengths[0];
	  runNumber = strtol(row[0],&stringEnd,0);
	}
      mysql_free_result(result);
    }
  return runNumber;
}
bool GetRunTimes(MYSQL * conn,int runNumber,time_t & start, time_t & end)
{
  bool ret = false;

  //make the times bad
  start = badTime;
  end = badTime;

  //MYSQL command to get the start and end times for run runNumber
  std::stringstream ss;
  ss << "select start_t,end_t from runs where num = "
     << runNumber;
  MYSQL_RES *result;
  int query_state = mysql_real_query(conn, ss.str().c_str(), ss.str().size());  
  if (query_state != 0)
    {
      printf("MySQL Query error:\t%s\n",mysql_error(conn));
      return false;
    }
  else
    {
      //Parse the returned MYSQL data
      result = mysql_store_result(conn); 
      MYSQL_ROW row;  
      unsigned int num_fields = mysql_num_fields(result);
      if(num_fields < 2)
	{
	  return false;
	}
      while ((row = mysql_fetch_row(result)))
	{
	  ret = true;

	  unsigned long *lengths;
	  lengths = mysql_fetch_lengths(result);	 

	  char * endrow = row[0]+lengths[0];
	  start = strtoul(row[0],&endrow,0);
	  endrow = row[1] + lengths[1];
	  end = strtoul(row[1],&endrow,0);
	  //If the end time wasn't set, use the start time of the next run
	  if(end == 0)
	    {
	      time_t nextStart,nextEnd;
	      GetRunTimes(conn,runNumber+1,nextStart,nextEnd);
	      if(nextStart != badTime)
		{
		  end = nextStart;
		}
	      else
		{
		  ret = false;
		}		
	    }	  
	  printf("Run %d went from %ld to %ld\n",runNumber,start,end);
	}
      mysql_free_result(result);
    }
  return false;
}

bool GetXYData(MYSQL * conn,const std::string & query,std::vector<double> &time, std::vector<double> &val)
{
  MYSQL_RES *result;
  time.resize(0);
  val.resize(0);
  int query_state = mysql_real_query(conn, query.c_str(), query.size());  
  if (query_state != 0)
    {
      printf("MySQL Query error:\t%s\n",mysql_error(conn));
      return false;
    }
  else
    {
      result = mysql_store_result(conn);   
      MYSQL_ROW row;
      unsigned int num_fields = mysql_num_fields(result);
      if(num_fields < 2)
	{
	  return false;
	}
      while ((row = mysql_fetch_row(result)))
	{
	  unsigned long *lengths;
	  lengths = mysql_fetch_lengths(result);	 

	  char * rowend = row[0]+lengths[0];
	  time.push_back(strtoul(row[0],&rowend,0));
	  rowend = row[1] + lengths[1];
	  val.push_back(strtod(row[1],&rowend));
	}
      mysql_free_result(result);
    }
  return true;
}

void ReZeroTimeVector(std::vector<double> & times,double newZero = 0,double rescale = 1)
{
  for(size_t i = 0; i < times.size();i++)
    {
      times[i] = rescale*(times[i] - newZero);
    }
}
