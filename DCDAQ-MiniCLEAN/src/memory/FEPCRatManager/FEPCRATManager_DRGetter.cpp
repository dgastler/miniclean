#ifdef __RATDS__
#include <FEPCRATManager.h>

bool FEPCRATManager::AddSend(int32_t &index)
{
  bool ret = false;
  //Add time stamp
  RATEVBlock * ptr = GetFEPCBlock(index);
  if(ptr)
    ptr->AddTimeStamp(TimeStampCode::FE_DRGetter);
  //Add to the Send queue
  ret = Send.Add(index);
  return(ret);
}
#endif

