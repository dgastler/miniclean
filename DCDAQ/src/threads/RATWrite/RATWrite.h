#ifndef __RATWRITE__
#define __RATWRITE__
//RATWrite processer takes in A full event,
//adds it to a RAT DS structure and then passes
//it off to ROOT to be written to disk.
//At the moment this is designed for microCLEAN,
//so there is goign to be no event construction
//besides that done on the V1720 module.  The
//memory manager will have to be adjusted to
//deal with an intermediate event construtor
//thread combining CAEN events from multiple
//V1720s to make one physics event.  The RATWrite
//thread should only deal with stuffing physics
//events into RATDS.

#include <DCThread.h>
#include <BasicRATDSManager.h>
#include <xmlHelper.h>

#include "TFile.h"
#include "TTree.h"
#include "RAT/DS/Root.hh"

#include <v1720Event.h>



class RATWrite : public DCThread 
{
 public:
  RATWrite(std::string Name);
  ~RATWrite();

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:
  bool ProcessMessage(DCMessage::Message &message);
  void ProcessTimeout();

  //Memory manager interface
  BasicRATDSManager * Mem;
  int32_t dsIndex;
  DSBlock *dsBlock;
  unsigned int iDS; //Index in dsBlock

  int BasicRATDSManagerFullFDs[2];


  //Statisticas
  //Events
  //Thread statistics
  time_t lastTime;
  time_t currentTime;
  uint64_t TimeStep_Events;

  //Config file and variables
  std::string Settings;
  uint32_t EventNumber;
  uint32_t SubEventNumber;
  uint16_t SubRunNumber;
  uint16_t SubRunNumberMax;
  uint32_t SubRunMaxEvents;
  uint32_t MaxRunEvents;
  time_t RunEndTime; //seconds

  //The ROOT output filename and the pointer to the file.
  bool fileReady;
  int  failedFileOpens;
  int  maxFailedFileOpens;
  bool OpenFile();
  bool CloseFile();
  std::string OutFileName;
  TFile * OutFile;
  int Autosave;
  bool AutoMoveFile;
  std::string MoveDirectory;

  //The ROOT tree pointer and the pointer to the current DS::Root data structure
  TTree *Tree;
  RAT::DS::Root * BranchDS;
};


#endif
