#ifndef __RATPARSER__
#define __RATPARSER__
//RATParser processer takes in a raw CAEN event,
//parses the raw data into the EV structure and then
//passes that ds structure on to another processor.

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <WFDBlockManager.h>
#include <xmlHelper.h>

#include <DSBlock.h>
#include "RAT/DS/EV.hh"

#include <v1720Event.h>



class RATParser : public DCThread 
{
 public:
  RATParser(std::string Name);
  ~RATParser(){};

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  //Memory Block manager interface
  MemoryBlockManager * Block_mem; //Input manager
  MemoryBlock * memBlock;//Local data structure that holds a pointer to data
  int32_t memBlockIndex;//Index to find our block from the block manager
  uint32_t currentDataIndex;

  //RATDS manager
  WFDBlockManager * EV_mem; //Output manager
  WFDBlock * evBlock;
  int32_t evIndex;

  //Statisticas
  time_t lastTime;
  time_t currentTime;
  //Input blocks
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;
  //Events
  uint32_t Events;
  uint32_t TimeStep_Events;
  uint32_t BadEvents;
  uint32_t TimeStep_BadEvents;

  //Put one event into a 
  void ProcessOneEvent(v1720Event &Event,RAT::DS::EV * EV);
  //Event parsing vectors
  std::vector<uint32_t> WindowHeaderVector;
  std::vector<uint16_t> WindowDataVector;

  //v1720Event parser
  v1720Event CurrentEvent;

};


#endif
