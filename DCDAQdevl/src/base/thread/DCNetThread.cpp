#include <DCNetThread.h>

DCNetThread::DCNetThread()
{  
  SetType("DCNetThread");
  parent = NULL;
  SocketFD = BadPipeFD;      
  MaxNetErrorCount = MAXNETERRORS;
  BadPacketTimeout = BADPACKETTIMEOUT;
  DataIn = DataOut = TimeStepDataIn = TimeStepDataOut = 0;
  PacketsIn = PacketsOut = TimeStepPacketsIn = TimeStepPacketsOut = 0;
  In = Out = NULL;
  lastUpdate = time(NULL);
}
DCNetThread::~DCNetThread()
{
  In->Shutdown();
  Out->Shutdown();
  if(In != NULL)
    {
      delete In; In = NULL;
    }
  if(Out == NULL)
    {
      delete Out; Out = NULL;
    }
  if(SocketFD != BadPipeFD)
    close(SocketFD);
}

bool DCNetThread::SetupConnection(const int &_SocketFD,
				  const struct sockaddr_in &_LocalIP,
				  const struct sockaddr_in &_RemoteIP)
{
  //==============================
  //Assign network variables
  //==============================      
  SocketFD = _SocketFD;
  LocalIP = _LocalIP;
  RemoteIP = _RemoteIP;
  
  //Add the socket fd to our read set
  if(SocketFD != BadPipeFD)
    {
      AddReadFD(SocketFD);
      SetupSelect();
    }

  //If the In and Out managers work and the socket is setup
  //set the Ready status to be true
  if((In != NULL) && (Out != NULL) && (SocketFD != BadPipeFD))
    {
      Ready = true;
      Loop = true;
    }
  return(true);
}

bool DCNetThread::SetupPacketManager(xmlNode * _PacketManagerNode)
{
  //Find In and Out pakcet manager nodes.
  xmlNode *  inPacketManagerNode = FindSubNode(_PacketManagerNode,"IN");
  if(inPacketManagerNode == NULL){return(false);}

  xmlNode * outPacketManagerNode = FindSubNode(_PacketManagerNode,"OUT");
  if(outPacketManagerNode == NULL){return(false);}


  //==============================
  //packet manager setup.
  //==============================      
  char * packetManagerName = new char[100];


  //Create In packet manager
  if(In) //Check for an existing manager
    {
      printf("Warning:   Deleting existing inPacketManager.\n");
      delete In;
    }
  sprintf(packetManagerName,"FD%02dIN",SocketFD); //name for thread
  In = new DCNetPacketManager(packetManagerName); 
  if(In == NULL) //Check that new worked
    {
      printf("Error:   Unable to allocate inPacketManager.\n");
      return(false);
    }
  if(!In->AllocateMemory(inPacketManagerNode)) 
    //Check that memory was allocated correctly
    {
      printf("Error:   Unable to allocate inPacketManager memory.\n");
      return(false);
    }      
  



  //Create Out packet manager
  if(Out) //Check for an existing manager
    {
      printf("Warning:   Deleting existing outPacketManager.\n");
      delete Out;
    }
  sprintf(packetManagerName,"FD%02dOUT",SocketFD); //name for thread
  Out = new DCNetPacketManager(packetManagerName);
  if(Out == NULL) //Check that new worked
    {
      printf("Error:   Unable to allocate outPacketManager.\n");
      return(false);
    }
  if(!Out->AllocateMemory(outPacketManagerNode))
    //Check that memory was allocated correctly
    {
      printf("Error:   Unable to allocate outPacketManager memory.\n");
      return(false);
    }      
  //Add the outgoing packet manager's FD to our thread's readset.
  AddReadFD(Out->GetFullFDs()[0]);    

  //Clean up our char array
  delete [] packetManagerName;
  
  //Set select to listen to our packetManager's FDs
  SetupSelect();
  
  //If the In and Out managers work and the socket is setup
  //set the Ready status to be true
  if((In != NULL) && (Out != NULL) && (SocketFD != BadPipeFD))
    {
      Ready = true;
      Loop = true;
    }
  return(true);
}

void DCNetThread::CheckForBadConnection(bool force)
{
  //Get Current time and add it to our deque
  time_t now = time(NULL);
  NetErrors.push_back(now);
  //Set oldest time to care about.
  time_t timeCut = now - BadPacketTimeout;

  //Loop over NetErrors and remove any times
  //we dont' care about.
  while(NetErrors.size())
    {
      if(NetErrors.front() < timeCut)
	{
	  //Remove old times
	  NetErrors.pop_front();
	}
      else
	{
	  //break when we get to current times
	  break;
	}
    }
  //Return true if there are too many errors
  if((NetErrors.size() > MaxNetErrorCount)||force)
    {
      DCMessage::Message message;

      DCMessage::SingleINT64 errorData;
      sprintf(errorData.text,"Bad connection");
      errorData.i = SocketFD;

      message.SetType(DCMessage::ERROR);      
      message.SetDataStruct(errorData);
      SendMessageOut(message,true);

      //stop listeing to the broken connection
      RemoveReadFD(SocketFD);
      RemoveReadFD(Out->GetFullFDs()[0]);
      SetupSelect();
    }  
}

void DCNetThread::ProcessTimeout()
{
  //Get current time
  now = time(NULL);  
  //check if we should be updating our parent.
  if((now - lastUpdate) > GetUpdateTime())
    {
      //Send a message with network transfer stats
      SendMessages(now,lastUpdate);
      lastUpdate = now;
    }
}

void DCNetThread::MainLoop()
{
  //================================
  //Process Out message
  //================================
  if(IsReadReady(Out->GetFullFDs()[0]))
    {
      SendPacket();
    }
  //================================
  //Process In message
  //================================
  if(IsReadReady(SocketFD))
    {
      ListenForPacket();
    }
}

void DCNetThread::SendMessages(time_t now,time_t lastUpdate)
{
  DCMessage::Message message;
  float dt = difftime(now,lastUpdate);
  DCMessage::TimedCount data;

  //PacketsOut message
  sprintf(data.text,"Packets out");
  data.count = TimeStepPacketsOut;
  data.dt = dt;
  message.SetDataStruct(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message,false);

  PacketsOut += TimeStepPacketsOut;
  TimeStepPacketsOut = 0;

  //PacketsIn message
  sprintf(data.text,"Packets in");
  data.count = TimeStepPacketsIn;
  data.dt = dt;
  message.GetDataStruct(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message,false);

  PacketsIn += TimeStepPacketsIn;
  TimeStepPacketsIn = 0;

  //DatasOut message
  sprintf(data.text,"Bytes out");
  data.count = TimeStepDataOut;
  data.dt = dt;
  message.GetDataStruct(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message,false);

  DataOut += TimeStepDataOut;
  TimeStepDataOut = 0;

  //DatasIn message
  sprintf(data.text,"Bytes in");
  data.count = TimeStepDataIn;
  data.dt = dt;
  message.SetDataStruct(data);
  message.SetType(DCMessage::STATISTIC);
  SendMessageOut(message,false);

  DataIn += TimeStepDataIn;
  TimeStepDataIn = 0;
}

inline void DCNetThread::SendPacket()
{
  DCNetPacket * packet;
  int32_t memManagerIndex;
  
  //Get a packet
  Out->GetFull(memManagerIndex,false);
  if(memManagerIndex >= 0)
    {
      packet = Out->GetPacket(memManagerIndex);
      //send packet
      int32_t sendReturn = packet->SendPacket(SocketFD);
      if(sendReturn > 0)
	{
	  //Everything is ok
	  Out->AddFree(memManagerIndex); //Add the freed packet to the free queue     
	  TimeStepPacketsOut++;
	  TimeStepDataOut += sendReturn;
	}
      else if(sendReturn == 0)
	{		  
	  //Data didn't send due to a timeout.
	  //Add the current packet back to the queue.
	  Out->AddFull(memManagerIndex);
	}
      else
	{
	  //A real error has occured.
	  
	  //Data didn't send due to a timeout.
	  //Add the current packet back to the queue.
	  Out->AddFull(memManagerIndex);
	  
	  //Check if we should consider the connection a failure.
	  CheckForBadConnection(true);	      
	  //CheckForBadConnection();	     
	}
    }  
}

inline void DCNetThread::ListenForPacket()
{
  DCNetPacket * packet;
  int32_t memManagerIndex;
  //Get a packet
  In->GetFree(memManagerIndex ,false);
  if(memManagerIndex >= 0)
    {
      packet = In->GetPacket(memManagerIndex);
      //listen for a  packet
      int error_return = 0;
      int32_t listenReturn = packet->ListenForPacket(SocketFD,error_return);
      if(listenReturn > 0)
	{
	  if(packet->GetType() != DCNetPacket::SHUTDOWN)
	    {
	      //Everything worked.
	      In->AddFull(memManagerIndex);
	      TimeStepPacketsIn++;
	      TimeStepDataIn+=listenReturn;
	    }
	  else
	    {
	      DCMessage::Message message;
	      
	      DCMessage::SingleINT64 errorData;
	      sprintf(errorData.text,"SHUTDOWN");
	      errorData.i = SocketFD;
	      
	      message.SetType(DCMessage::ERROR);      
	      message.SetDataStruct(errorData);
	      SendMessageOut(message,true);
	      
	      //stop listeing to the broken connection
	      RemoveReadFD(SocketFD);
	      RemoveReadFD(Out->GetFullFDs()[0]);
	      SetupSelect();
	    }
	}
      else
	{
	  //Error
	  //return the packet to the free queue
	  In->AddFree(memManagerIndex);	  
	  if(listenReturn == DCNetPacket::PACKET_TOO_LARGE)
	    {
	      PrintError("Error ListenForPacket:   Packet too large!\n");
	    }
	  else if(listenReturn == DCNetPacket::PACKET_DATA_READ_SIZE)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: Bad data read size. %d\n",error_return);
	      PrintError(buffer);
	      delete [] buffer;
	    }
	  else if(listenReturn == DCNetPacket::PACKET_BAD_HEADER)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: Bad header word. 0x%X\n",error_return);
	      PrintError(buffer);
	      delete [] buffer;
	    }
	  else if(listenReturn == DCNetPacket::PACKET_TIME_OUT)
	    {
	      PrintError("ListenForPacket: Timeout\n");
	    }
	  else if(listenReturn == DCNetPacket::PACKET_HEADER_READ_SIZE)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: Packet header read wrong size (%dbytes)!\n",error_return);
	      PrintError(buffer);
	      delete [] buffer;
	    }
	  else if(listenReturn == DCNetPacket::PACKET_READ_ERROR)
	    {
	      char * buffer = new char[1000];
	      sprintf(buffer,"ListenForPacket: %s\n",strerror(error_return));
	      PrintError(buffer);
	      delete [] buffer;
	    }

	  //Check if the connection is effectively dead.
	  CheckForBadConnection();
	}
    }	  	   
}

bool DCNetThread::ProcessMessage(DCMessage::Message &message)
{
  //We should wait for a message because select told us we have a message.
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      //stop listeing to the broken connection
      if(SocketFD)
	{
	  RemoveReadFD(SocketFD);
	}
      if(Out->GetFullFDs()[0])
	{
	  RemoveReadFD(Out->GetFullFDs()[0]);
	}
      SetupSelect();
      //Shutdown packet manager queues
      In->Shutdown();
      Out->Shutdown();       
      
      Loop = false;
      Running = false;
      
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //stop listeing to the broken connection
      RemoveReadFD(SocketFD);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      if(SocketFD != BadPipeFD)
	{
	  AddReadFD(SocketFD);
	  SetupSelect();
	}
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DCNetThread::PrintStatus()
{
  printf(" InPacketManager:");
  if(In != NULL)
    {      
      In->PrintStatus(3);
    }
  else
    {
      printf(" NULL\n");
    }
  printf(" OutPacketManager:");
  if(Out != NULL)
    {      
      Out->PrintStatus(3);
    }
  else
    {
      printf(" NULL\n");
    }
}
