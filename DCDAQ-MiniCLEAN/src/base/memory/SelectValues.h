#ifndef __SELECTVALUES__
#define __SELECTVALUES__
namespace SelectValues
{
  const uint8_t OK = uint8_t(0x0);
  const uint8_t FAIL= uint8_t(0x1);
  const uint8_t EMPTY= uint8_t(0x2);
  const uint8_t FULL= uint8_t(0x3);
};
#endif
