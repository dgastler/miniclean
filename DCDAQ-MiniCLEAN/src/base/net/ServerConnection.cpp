#include <ServerConnection.h>

const int ServerConnection::badFD = -1;

bool ServerConnection::RouteToLocal(DCMessage::DCAddress addr)
{
  if(addr.IsBroadcastAddr())
    return true;
  for(size_t iConn = 0; iConn < client.size();iConn++)
    {
      if(client[iConn]->RouteToLocal(addr))
	return true;
    }
  return false;
}

ServerConnection::~ServerConnection()
{
  Clear();
}

void ServerConnection::Clear()
{
  //Close the server socket
  if(serverSocketFD!= badFD)
    {
      CloseSockFD(serverSocketFD);
    }
  //  shutdown(serverSocketFD,SHUT_RD);
  //Close all the connections
  while(client.size() > 0)
    {
      DeleteConnection(client.size()-1);      
    }  
  
  //unlink bind address if it already exists (only AF_LOCAL)
  if(listenAddr.GetAddr()->sa_family == AF_LOCAL)
    {
      unlink(((struct sockaddr_un *) listenAddr.GetAddr())->sun_path);
    }

  serverPort = 0;
  serverSocketFD = badFD;
  listenAddr.Clear();
  acceptList.clear();
}

bool ServerConnection::Setup(xmlNode * SetupNode)
{
  //================================
  //Parse data for server config and expected clients.
  //================================

  //Get server port.
  if(FindSubNode(SetupNode,"PORT") == NULL)
    {
      PrintError("No port specified\n");
      return false;
    }
  else
    {
      uint32_t tempServerPort;
      GetXMLValue(SetupNode,"PORT","ServerConnection",tempServerPort);
      serverPort = tempServerPort;
    }


  //Get server listen address.
  std::string listenAddress;
  if(FindSubNode(SetupNode,"LISTENADDRESS") == NULL)
    {
      PrintError("No listen address specified.  Using default\n");
    }
  else
    {
      GetXMLValue(SetupNode,"LISTENADDRESS","ServerConnection",listenAddress);
    }

  if(listenAddress.empty())  //Default      
    {
      listenAddr.SetName(std::string(""));
      listenAddr.SetAddr(std::string("0.0.0.0"));
    }
  else                       //Specified
    {
      listenAddr.SetName(std::string(""));
      listenAddr.SetAddr(listenAddress);
    }

  listenAddr.SetPort(serverPort);

  printf("Waiting for connection on %s:%d\n",listenAddr.GetStr().c_str(),listenAddr.GetPort());

  //Create server socket
  if((listenAddr.GetAddr()->sa_family == AF_UNSPEC) ||
     (listenAddr.GetAddr()->sa_family == AF_MAX))
    {
      PrintError("Invalid sa_family for running a server\n");
      return false;
    }

  serverSocketFD = socket(listenAddr.GetAddr()->sa_family,SOCK_STREAM,0); 
  if(serverSocketFD < 0)
    {
      std::stringstream ss;
      ss << "Socket failes with arguments (" 
	 << listenAddr.GetAddr()->sa_family << ":" << SOCK_STREAM << ":" << 0
	 << ") error (" << errno << ") " << strerror(errno);
      PrintError(ss.str().c_str());
      return false;
    }
  //Set SO_REUSEADDR
  int optval = 1;
  setsockopt(serverSocketFD,listenAddr.GetAddr()->sa_family,
	     SO_REUSEADDR,&optval,sizeof(optval));

  //unlink bind address if it already exists (only AF_LOCAL)
  if(listenAddr.GetAddr()->sa_family == AF_LOCAL)
    {
      unlink(((struct sockaddr_un *) listenAddr.GetAddr())->sun_path);
    }

  //bind our socket to listen address and listen for connections
  int bindRet = bind(serverSocketFD,listenAddr.GetAddr(),listenAddr.GetAddrSize()); 
  if(bindRet != 0)
   {
      fprintf(stderr,"Bind gave error(d%d) %s\n",errno,strerror(errno));
      return false;
    }
  int listenRet = listen(serverSocketFD,14); //(#14 for linux)  P98 Unix Network Programming  
  if(listenRet != 0)
    {
      fprintf(stderr,"Listen gave error(d%d) %s\n",errno,strerror(errno));
      return false;
    }
  return true;
}
bool ServerConnection::ProcessConnection(xmlNode * managerNode)
{
  //================================
  //We should have a waiting connection, so we need to accept it
  //================================
  int fdRemoteSocket = badFD; //fd for incoming connection
  struct sockaddr_un addrRemote; //address info for incoming connection
  socklen_t addrRemoteLength = sizeof(addrRemote);
  
  fdRemoteSocket = accept(serverSocketFD,
			  (sockaddr *) &addrRemote,
			  &addrRemoteLength);
  if(fdRemoteSocket == badFD)
    {	  
      return false;
    }

  DCMessage::DCAddress remoteAddress("",(sockaddr * )&addrRemote,addrRemoteLength);
  if(((struct sockaddr *)&addrRemote)->sa_family == AF_LOCAL)
    {
      remoteAddress = listenAddr;
    }

  //Check if this is in our netCclearList
  if(acceptList.size() > 0)
    {
      std::vector<DCMessage::DCAddress>::iterator it;
      for(it = acceptList.begin();
	  it != acceptList.end();
	  it++)
	{
	  if(remoteAddress.IsEqualAddr(*it))
	    break;
	}
      //if we made it to the iterator's end then we 
      //should block this connection.
      if(it == acceptList.end())
	{
	  CloseSockFD(fdRemoteSocket);
	  fdRemoteSocket = badFD;
	}
      return false;
    }
  
  //================================
  //Build a Connection object for this socket
  //================================

  //Fillin our connection settings.
  Connection * connection = new Connection();

  //Setup the DCNetThread
  if(connection->SetupDCNetThread(fdRemoteSocket,
				  remoteAddress,
				  serverPort,
				  managerNode))
    {
      //on sucess add this connection to our list
      client.push_back(connection);
    }
  else
    {
      //fail 
      delete connection;
      return false;
    }	
  return true;
}

void ServerConnection::DeleteConnection(size_t i)
{
  //Check that iConn is physical
  if(i >= client.size())
    return;

  //This will shutdown the thread and close the connection
  delete client[i];
  client.erase(client.begin()+i);
}
