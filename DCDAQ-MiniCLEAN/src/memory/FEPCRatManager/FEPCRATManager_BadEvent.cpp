#ifdef __RATDS__

#include <FEPCRATManager.h>

bool FEPCRATManager::AddUnSent(int32_t &index)
{
  bool ret = false;
  ret = UnSent.Add(index);
  return(ret);
}



bool FEPCRATManager::GetBadEvent(BadEvent::sBadEvent &badEvent)
{
  bool ret = false;
  ret = BadEvent.Get(badEvent);
  return(ret);
}


bool FEPCRATManager::AddBadEvent(int64_t eventKey,
				 int32_t &index,
				 Level::Type errorCode)
{  
  //Add time stamp
  RATEVBlock * ptr = GetFEPCBlock(index);
  if(ptr)
    ptr->AddTimeStamp(TimeStampCode::FE_BadEvent);

  BadEvent::sBadEvent badEvent;
  badEvent.eventKey = eventKey;
  badEvent.index = index;
  badEvent.errorCode = errorCode;

  return BadEvent.Add(badEvent);
}

#endif
