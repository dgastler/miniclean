#include <MySQLInterface.h>

bool MySQLInterface::ProcessMessage(DCMessage::Message & message)
{
  bool ret = false;
  switch (StoppingRun)
    {
    case true:
      if(message.GetType() == DCMessage::COMMAND_RESPONSE)
	{
	  std::string response;
	  DCMessage::DCString dcString;
	  dcString.GetData(message.GetData(),response);      
	  if(response.find("Thread_Name Thread_Type") != std::string::npos)
	    {
	      if(response.find("RunControl") == std::string::npos)
		{
		  StoppingRun = false;
		  if(!SettingUpNewRun)
		    {
		      //Update the state machine to SETUP
		      UpdateRunLockState(DCRunLock::FREE);
		    }
		}
	      ret = true;
	      break;
	    }	  
	}
    default:
      //normal message processor
      std::map<DCMessage::DCMessageType,void (MySQLInterface::*)(DCMessage::Message & message)>::iterator it;
      it = typeMap.find(message.GetType());
      if(it != typeMap.end())
	{
	  (*this.*(it->second))(message);
	  ret = true;
	}
      else if(message.GetType() == DCMessage::STOP)
	{
	  //Stop the current run
	  if(!StoppingRun)
	    StopCurrentRun();
	  else
	    CheckShutdown();
	}
      break;
    }
  return ret;
}

void MySQLInterface::ProcessTimeout()
{
  time_t current_Time = time(NULL);
  if(current_Time > next_Time)
    {
      next_Time += polling_Interval;
      //Poll DAQ DB for state change

      //This is where DCDAQ's runs are controlled in MiniCLEAN
      if(DAQDB_Connection.IsConnected())
	{
	  std::string stateStr;
	  DAQDB_Connection.GetSingleValue(std::string("SELECT * from RUNLOCK"),stateStr);
	  DCRunLock::Type DAQ_State = DCRunLock::GetType(stateStr.c_str());

	  //read state
	  switch (DAQ_State)
	    {
	    case DCRunLock::NEW_RUN:
	      //A new run is requested.  
	      //We need to stop the current run if it exists and then build a new one.	      

	      //1)
	      //The first time through, we aren't setting up a new run or stopping the current one
	      //so we just state that we will be setting up a new run and go on to the STOP case
	      //2)
	      //For the next N times we check, we are waiting for the run to shut down (so StoppingRun)
	      //is true which stops the if statement.
	      //3)
	      //Now the previous run has been shut down, so now !StoppingRun is true along with
	      //SettingUpNewRun, so we will set up the new run.
	      if(SettingUpNewRun && !StoppingRun)
		{
		  //Build new run XML and send it out
		  if(!StartNewRun())
		    {
		      fprintf(stderr,"Error starting new run: Shutting down\n");
		      Running = false;
		      Loop = false;
		    }
		  SettingUpNewRun = false;
		  //In either case, update to FREE state
		  UpdateRunLockState(DCRunLock::RUNNING);
		  break;
		}
	      else
		{
		  SettingUpNewRun = true;
		}
	    case DCRunLock::STOP:
	      //Stop the current run
	      if(!StoppingRun)
		StopCurrentRun();
	      else
		CheckShutdown();
	      break;
	    case DCRunLock::RUNNING: //ignore
	    case DCRunLock::ERROR: //ignore
	    case DCRunLock::FREE: //ignore
	    default:
	      break;
	    };
	}
      else
	{
	  DAQDB_Connection.CheckConnectionError();
	}
    }

}
