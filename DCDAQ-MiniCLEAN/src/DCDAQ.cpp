#include <iostream>
#include <vector>
#include <map>

#include <string>
#include <cstring> //strerror()

#include <math.h> //floor

#include <signal.h> //signals 
#include <unistd.h> //alarm



//DCDAQ
#include <DCThread.h>
#include <MemoryManager.h>
#include <DCLauncher.h>
#include <DCMessage.h>
#include <DCMessageProcessor.h>
#include <StackTracer.h>

//TCLAP
#include <tclap/CmdLine.h>


volatile bool run = true;
volatile bool ctrlC_exit = false;

void signal_handler(int sig)
{  
  if(sig == SIGINT)
    {
      if(ctrlC_exit)
	{
	  run = false;
	  //struct sigaction sa;
	  //sa.sa_handler = SIG_DFL;
	  //sigemptyset(&sa.sa_mask);
	  //sigaction(SIGINT, &sa, NULL);
	  fprintf(stderr,"\n=====================================\n");
	  fprintf(stderr,"          Ctrl-C forced exit!\n");
	  //fprintf(stderr,"          Once more to kill.");
	  fprintf(stderr,"\n=====================================\n");
	}
      else
	{
	  fprintf(stderr,"\n=====================================\n");
	  fprintf(stderr,"Press Ctrl-C once more to force exit");
	  fprintf(stderr,"\n=====================================\n");
	  alarm(5);
	  ctrlC_exit = true;
	}
    }
  else if (sig == SIGALRM)
    {
      //reset ctrlC_exit after a timeout
      ctrlC_exit = false;
    }
  else if(sig == SIGSEGV)
    {
      print_trace();
      fprintf(stderr,"SEGFAULT yo!\n");
      print_trace();
      run = false;
      struct sigaction sa;
      sa.sa_handler = SIG_DFL;
      sigemptyset(&sa.sa_mask);
      sigaction(SIGSEGV, &sa, NULL);
    }
  else if(sig == SIGPIPE)
    {
      print_trace();
      fprintf(stderr,"SIGPIPE yo!\n");
    }
  else if(sig == SIGUSR1)
    {
      fprintf(stderr,"Thread issuing SIGUSR1.  Shutting down!\n");
      run = false;
    }
  else
    {
      fprintf(stderr,"Unprocesses signal: %d\n",sig);
    }
}


int main(int argc, char ** argv)
{  
  bool CommandLine = false;
  std::string CommandLineXMLString;
  struct sigaction sa;
  sa.sa_handler = signal_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGALRM,&sa, NULL);
  sigaction(SIGSEGV,&sa, NULL);
  sigaction(SIGPIPE,&sa, NULL);
  sigaction(SIGUSR1,&sa, NULL);

  std::string address;

  unsigned port;

  try
    {
      TCLAP::CmdLine cmd("DCDAQ (Dan's Cool DAQ) for the DEAP CLEAN experiments.",
			 ' ',
			 "DCDAQ v0");
      //Load xml file with no flag
      TCLAP::UnlabeledValueArg<std::string> xmlArg("RunXML",
						   "A DCDAQ XML file for direct loading.",
						   false,
						   "",
						   "xml file",
						   cmd);
      TCLAP::ValueArg<unsigned> portNumberArg("p",
					      "port",
					      "port number for DCMessageServer",
					      false,
					      1717,
					      "port number for DCMessageServer",
					      cmd);
      TCLAP::ValueArg<std::string> addressArg("a",
					      "address",
					      "address for DCMessageServer",
					      false,
					      "0.0.0.0",
					      "Address for DCMessageServer",
					      cmd);

      cmd.parse(argc,argv);

      //Check for command line file.
      if(xmlArg.getValue().size() == 0)
	{
	  CommandLine = false;
	}
      else
	{
	  //Load the command line XML file
	  std::string CommandLineFileName(xmlArg.getValue());
	  if(!LoadFile(CommandLineFileName,CommandLineXMLString))
	    {
	      fprintf(stderr,"Bad input file.\n");
	      return 0;
	    }
	  else
	    {
	      CommandLine = true;
	    }
	}
      //CHeck for port number
      port = portNumberArg.getValue();
      address = addressArg.getValue();
    }
  catch (TCLAP::ArgException &e)
    {
      fprintf(stderr, "Error %s for arg %s\n",
	      e.error().c_str(), e.argId().c_str());
      return 0;
    }

  //Hardcoded config files for the control and netcontrol threads.
  std::stringstream ssControlSetup;
  ssControlSetup << "<SETUP><MEMORYMANAGERS></MEMORYMANAGERS><THREADS><THREAD><TYPE>PrintServer</TYPE><NAME>PrintServer</NAME></THREAD><THREAD><TYPE>Control</TYPE><NAME>Control</NAME></THREAD><THREAD><TYPE>DCMessageServer</TYPE><NAME>DCMessageServer</NAME><PORT>" 
		 << port 
		 << "</PORT><LISTENADDRESS>"
		 << address 
		 << "</LISTENADDRESS></THREAD></THREADS></SETUP>";
  

  printf("==================================\n");
  printf("=========Setting up DCDAQ=========\n");
  printf("==================================\n");

 
  //Create the launcher for DCDAQ threads and memory managers
  //  DCLauncher * Launcher = new DCLauncher;
  DCLauncher Launcher;

  //Create the local select error processor and add ignored errors
  SelectErrorIgnore<LockedObject> * selectErrorIgnore = Launcher.GetSelectErrorIgnore();
  selectErrorIgnore->AddIgnoreErrno(EINTR); //Interrupted system call

  //Create the message processor for DCDAQ
  DCMessageProcessor MessageProcessor(&Launcher);

  DCMessage::Message message;
  //load the command line requested objects
  if(CommandLine)
    {
      message.SetData(CommandLineXMLString.c_str(),CommandLineXMLString.size());
      message.SetType(DCMessage::LAUNCH);
      run = MessageProcessor.ProcessMessage(message);
    }
  else
    {
      //Start the user interface / network interface if no user xml provided
      message.SetData(ssControlSetup.str().c_str(),ssControlSetup.str().size());
      message.SetType(DCMessage::LAUNCH);
      run = MessageProcessor.ProcessMessage(message);
    }
  


  //Setup select for main DCDAQ loop
  struct timeval SleepTime;
  int MaxFDPlusOne;
  //FD sets for select. 
  fd_set ReadSet,WriteSet,ExceptionSet;   

  //Program run time
  time_t mainLoopStartTime = time(NULL);
  time_t mainLoopEndTime = mainLoopStartTime;

  printf("===================================\n");
  printf("===========DCDAQ Running===========\n");
  printf("===================================\n");
  printf(">Type \"go\" to start threads.\n");

  //Main loop
  //This loop lasts until there are no more DCThreads running
  while(Launcher.GetThreads().size())
    {
      //Shutdown and clean up all of the memory managers and threads.
      if(!run)
	{	  
	  Launcher.StopAllThreads();
	}

      //Get the fd sets for the running threads
      Launcher.GetFDSets(ReadSet,
			 WriteSet,
			 ExceptionSet,
			 MaxFDPlusOne,
			 SleepTime);

      
      //Block in select
      int selectReturn = select(MaxFDPlusOne,
				&ReadSet,
				&WriteSet,
				&ExceptionSet,
				&SleepTime);
      if(selectReturn > 0)
	{
	  run &= MessageProcessor.ProcessSelect(selectReturn,
						ReadSet,
						WriteSet,
						ExceptionSet,
						run);
	  Launcher.ProcessTimeout();
	}
      //Note, selectReturn = 0 just means it timed out.
      //Call DCLauncher's timeout function
      else if(selectReturn == 0)
	{
	  Launcher.ProcessTimeout();
	}
      //negative select means an error.
      else
	{
	  if(!selectErrorIgnore->IgnoreSelectError(errno))
	    {
	      printf("Error:     DCDAQ select had error %d: %s\n",errno,strerror(errno));
	    }
	}
    }
  //We no longer need this pointer;
  selectErrorIgnore = NULL;
  //Shutdown and clean up all of the memory managers and threads.
  Launcher.Shutdown();

  //Calculate run time
  mainLoopEndTime = time(NULL);
  double mainLoopRunTime = difftime(mainLoopEndTime,mainLoopStartTime);  

  int days =    (int) floor(mainLoopRunTime/(24*60*60));
  mainLoopRunTime -= days*24*60*60;

  int hours =   (int) floor(mainLoopRunTime/(60*60));
  mainLoopRunTime -= hours*60*60;

  int minutes = (int) floor(mainLoopRunTime/60);
  mainLoopRunTime -= minutes*60;

  int seconds = (int) mainLoopRunTime;
  
  printf("\n\nDCDAQ run time: %03ddays %02dhrs %02dmin %02dsec\n\n",
	 days,hours,minutes,seconds);

  return(0);
}
