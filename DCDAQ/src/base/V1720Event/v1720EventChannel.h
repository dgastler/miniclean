#ifndef __V1720_EVENT_CHANNEL__
#define __V1720_EVENT_CHANNEL__

#include <stdint.h> //uint32_t
#include <vector>

#include <v1720EventWindow.h>

class v1720EventChannel
{
 public:
  v1720EventChannel();
  ~v1720EventChannel();
  std::vector<v1720EventWindow> Window;
  int ProcessChannel(uint32_t * _rawData,uint32_t _size,bool _ZLE ,bool _twoPack,uint8_t _channelID);
  uint8_t GetChannelID(){return(channelID);}
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);};
 private:
  bool twoPack;
  bool ZLE;
  //This class doesn't own this.
  uint32_t * rawData;
  uint32_t size;
  uint8_t channelID;
  int ProcessZLE();
  int ProcessFULL();
};

#endif
