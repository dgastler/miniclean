#ifndef __BADEVENTSTRUCT__
#define __BADEVENTSTRUCT__
#include <Level.h>

namespace BadEvent
{

//enum
//{
//  NO_ERROR = 0,
//  STORAGE_COLLISION,
//  STORAGE_EXISTING_FULL_EVENT,
//  RATASSEMBLER_TOO_MANY_WFDS,
//  RATASSEMBLER_COLLISION,
//  STORAGE_EMPTY,
//  DR_SEND_DECISION_FAILED,
//  DRASSEMBLER_COLLISION,
//  DR_TRIGGER_COLLISION,
//  DR_LVDSMATCH,
//  EBASSEMBLER_COLLISION,
//  BENCH_ASSEMBLER_COLLISION,
//  BENCH_ASSEMBLER_TOO_MANY_WFDS,
//  ERROR_COUNT //count of error codes
//};

struct sBadEvent
{
  int64_t eventKey;
  int32_t index;
  Level::Type errorCode;
  sBadEvent()
  {
    errorCode = Level::BLANK;
  }
};

}
#endif
