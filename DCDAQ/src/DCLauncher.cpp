#include <DCLauncher.h>

uint32_t DCLauncher::Launch(std::string config)
{
  //Temp containers for new memory managers and threds
  //We will fill these and if everything is sucessful, then
  //we will add them to the DAQ's list.
  //If there is a failure, everythign will be shut down.
  std::vector<DCThread *> tempThread;
  std::map<std::string,MemoryManager*> tempMemoryManager;

  //Parse the configuration string into an XML structure
  xmlDoc * configXML = xmlReadMemory(config.c_str(),
				     config.size(),
				     NULL,
				     NULL,
				     0);
  //Check that the text was correctly parsed into XML
  if(configXML == NULL)
      return(BAD_XML);
  //Get the base node of the xml
  xmlNode * baseNode = xmlDocGetRootElement(configXML);


  //Setup any Memory managers
  uint32_t memoryManagerRet = 0;
  if(FindSubNode(baseNode,"MEMORYMANAGERS") != NULL)
    {
      memoryManagerRet = SetupMemoryManagers(FindSubNode(baseNode,"MEMORYMANAGERS"),
					     tempMemoryManager);
      //Return an error and remove the memory managers if
      //There was an error. 
      if(memoryManagerRet != SETUP_OK)
	{
	  DeleteMemoryManagerMap(tempMemoryManager);
	  //Return MMFAIL ored with memoryManagerRet.
	  //Setup can only work with 16 mm at a time. 
	  //the return will use the last 16 bits to specify which
	  //MM failed.   The first 16 give the type of error. 
	  return(memoryManagerRet);
	}
    }
  //Memory managers have been correctly set up at this point.
  

  //Setup threads
  uint32_t threadRet = 0;
  if(FindSubNode(baseNode,"THREADS") != NULL)
    {
      threadRet = SetupThreads(FindSubNode(baseNode,"THREADS"),
			       tempThread,
			       tempMemoryManager);
      //Clean up if we have a failure
      if(threadRet != SETUP_OK)
	{
	  //First shut down and delete all threads
	  DeleteThreadVector(tempThread);
	  //Now delete memoryManagers
	  DeleteMemoryManagerMap(tempMemoryManager);
	  return(threadRet);
	}			       
    }
  
  //MemoryManagers and threads have been correctly set up.
  //Merge memory managers
  if(!AddMemoryManager(tempMemoryManager) ||
     !AddThread(tempThread))
    {
      //This means you messed up code in this class.
      //Fix it now!@
      return(FAIL);
    }

  //Call FDupdate to make sure nothing in it is bad
  UpdateFD();

  return(SETUP_OK);
}

void DCLauncher::DeleteMemoryManagerMap(std::map<std::string,MemoryManager *> &MM)
{
  //Shutdown and remove the managers
  //clean up memory managers
  for(std::map<std::string,MemoryManager *>::iterator it = MM.begin();
      it != MM.end();
      it++)
    {
      delete (*it).second;
    }
}

//void DCLauncher::DeleteThreadVector(std::vector<DCThread *> & T,bool verbose=false)
void DCLauncher::DeleteThreadVector(std::vector<DCThread *> & T,bool verbose)
{
  void * ThreadReturn = NULL;
  for(unsigned int i = 0; i < T.size();i++)
    {
      //Send stop message
      DCMessage::Message message;
      message.SetType(DCMessage::STOP);
      T[i]->SendMessageIn(message,true);
      //Wait for thread to finish.
      if(verbose)
	{
	  fprintf(stderr,"Thread %s (%p) ",
		  T[i]->GetName().c_str(),
		  (void*) T[i]->GetID());
	}
      pthread_join(T[i]->GetID(),&ThreadReturn);
      //delete thread
      delete T[i];
      if(verbose)
	{
	  printf("finished.\n");
	}
    }
  T.clear();
}

bool DCLauncher::AddMemoryManager(std::map<std::string,MemoryManager *> &MM)
{
  //Shutdown and remove the managers
  //clean up memory managers
  for(std::map<std::string,MemoryManager *>::iterator it = MM.begin();
      it != MM.end();
      it++)
    {
      //Check if this MM already exists.  (This shouldn't happen)
      if(memoryManager.find(it->first) == memoryManager.end())
	{
	  //Copy the MM from the temp variable to main MM
	  memoryManager[it->first] = it->second;
	}
      else
	{
	  fprintf(stderr,"Duplicated MM in AddMemoryManager.\n");
	  fprintf(stderr,"This SHOULD NOT HAPPEN!\n");
	  fprintf(stderr,"What did you do?\n");
	  return(false);
	}
    }
  //Clear out the temp MM.
  MM.clear();
  return(true);
}

bool DCLauncher::AddThread(std::vector<DCThread *> & T)
{
  //This is a simple add.  There is nothing to check.
  for(unsigned int iT = 0; iT < T.size();iT++)
    {
      thread.push_back(T[iT]);
    }
  //Clear out the temp thread vector.
  T.clear();
  return(true);
}

void DCLauncher::Shutdown()
{
  DeleteThreadVector(thread,true);
  DeleteMemoryManagerMap(memoryManager);  
}

void DCLauncher::UpdateFD()
{
  //Zero out all previous FD data and start fresh
  //This might take some time, but this isn't called often,
  //so I'd rather play it safe.
  FD_ZERO(&ReadSet);
  FD_ZERO(&WriteSet);
  FD_ZERO(&ExceptionSet);
  FDtoThreadMap.clear();
  MaxFDPlusOne = 0;

  //Rebuild the data we just cleared
  unsigned int threadSize = thread.size();
  //Loop over all threads and get their Message out FDs for select
  //If we eventually need to fill the Write and Exception fd_sets then
  //add that here.
  for(unsigned int i =0; i < threadSize;i++)
    {
      //Get the the MessageOut FD from thread i
      int readFD = thread[i]->GetMessageOutFDs()[0];
      if(readFD > 0)
  	{
	  //Add the FD to the Read fdset
	  FD_SET(readFD,&ReadSet);
	  //and to the FD thread map 
  	  FDtoThreadMap[readFD] = i;
	  //Check to see if it's the biggest.
  	  if(readFD > MaxFDPlusOne)
  	    {
  	      MaxFDPlusOne = readFD;	      
  	    }
  	}      
    }
  MaxFDPlusOne++;  //Move MaxFD up by one (select convention)
}

const std::map<int,int> & DCLauncher::GetFDMap()
{
  return(FDtoThreadMap);
}
void DCLauncher::GetFDSets(fd_set & retReadSet,
			   fd_set & retWriteSet,
			   fd_set & retExceptionSet,
			   int    & retMaxFDPlusOne)
{
  retReadSet = ReadSet;
  retWriteSet = WriteSet;
  retExceptionSet = ExceptionSet;
  retMaxFDPlusOne = MaxFDPlusOne;
}

