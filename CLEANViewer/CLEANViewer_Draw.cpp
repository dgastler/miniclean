//*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/

#include "CLEANViewer.h"


//Function for drawing histograms to the histogram tab.
void CLEANViewer::DrawHistogram()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::DrawHistogram(UInt_t iBoard, UInt_t iChannel, UInt_t ChannelID)\n");




}

//Function used for intial drawing of individual PMT data, not utilized when the zoom is being adjusted
void CLEANViewer::DrawPolyLine()
{
  if(DEBUG) fprintf(stderr,"CLEANViewer::DrawPolyLine(vector <UInt_t> ChannelIDs)\n");
  
  rawTCanvas->SetEditable(kTRUE);
  calibTCanvas->SetEditable(kTRUE);

  rawTCanvas->Clear();
  calibTCanvas->Clear();

  rawTCanvas->Range(0,0,1,1);
  calibTCanvas->Range(0,0,1,1);


  
  //Clearing polylines to make room for new ones
  for(UInt_t iPVector = 0;
      iPVector < PolyLineVector.size();
      iPVector++)
    {
      delete PolyLineVector[iPVector];
    }
  PolyLineVector.clear();
  PolyLineCalibVector.clear();
  //Clearing Axes to make room for new ones
  for(UInt_t iAVector = 0;
      iAVector < AxisVector.size();
      iAVector++)
    {
      delete AxisVector[iAVector];
    }

  AxisVector.clear();
  AxisVectorCalib.clear();
  //Clearing Labels to make room for new ones
  for(UInt_t iLVector = 0;
      iLVector < PmtLabelVector.size();
      iLVector++)
    delete PmtLabelVector[iLVector];
  PmtLabelVector.clear();
  //cd to the raw canvas so that the polylines are drawn in the correct tab
  calibTCanvas->cd();
  calibTCanvas->Clear();
  rawTCanvas->cd();
  rawTCanvas->Clear();
  
  int nPlots = int(PlotIDsVector.size());

  canvasMinY = 9999999;
  canvasMaxY = -9999999;

  canvasMinYCalib = 9999999.9;
  canvasMaxYCalib = -9999999.9;
  //only set the canvasMinX and canvasMaxX if the zoom hasn't been adjusted
  if(!zoomAdjusted)
    {
      canvasMinX = 9999999; 
      canvasMaxX = -9999999;
    }
  
  //loop for finding the canvasMin and canvasMax
  for (UInt_t iChannel = 0; iChannel < PlotIDsVector.size(); iChannel++)
    {
      for (UInt_t iWaveform = 0; 
	   iWaveform < WaveformVector.size(); 
	   iWaveform++)
	{
	  if (WaveformVector[iWaveform].GetID() == int(PlotIDsVector[iChannel].ChannelID))
	    {
	      if(!zoomAdjusted)
		{
		  if (WaveformVector[iWaveform].GetMinSample(true) < canvasMinX)
		    canvasMinX = int(WaveformVector[iWaveform].GetMinSample(true));
		  if (WaveformVector[iWaveform].GetMaxSample(true) > canvasMaxX)
		    canvasMaxX = int(WaveformVector[iWaveform].GetMaxSample(true));

		}
	      if(WaveformVector[iWaveform].GetID()>0)
		{
		  if (WaveformVector[iWaveform].GetMinADC() < canvasMinY)
		    canvasMinY = int(WaveformVector[iWaveform].GetMinADC());
		  if (WaveformVector[iWaveform].GetMaxADC() > canvasMaxY)
		    canvasMaxY = int(WaveformVector[iWaveform].GetMaxADC());
		  if (WaveformVector[iWaveform].GetMinQCalib() < canvasMinYCalib)
		    canvasMinYCalib = WaveformVector[iWaveform].GetMinQCalib();
		  if (WaveformVector[iWaveform].GetMaxQCalib() > canvasMaxYCalib)
		    canvasMaxYCalib = WaveformVector[iWaveform].GetMaxQCalib();

		}
	    }
	}
    }

  //just a check so that the axis doesn't change everytime a new pmt is checked
  
  zoomSlider->SetPosition(float(canvasMinX), float(canvasMaxX));
  //loop for plotting the waveforms (if it's confusing, it's because it is kind of)
 
  for (UInt_t iChannel = 0; iChannel < PlotIDsVector.size(); iChannel++)
    {
      //by declaring the max/minADC within the loop it causes the y-axis for pmts without any ZLE windows to range from 0-1.
      double maxADC = 1;
      double minADC = 0;
      double maxQCalib=1;
      double minQCalib=0;
      for (UInt_t iWaveform = 0; 
	   iWaveform < WaveformVector.size(); 
	   iWaveform++)
	{
	  //Plot only the waveforms with the same ID as the PMTs checked
	  if (WaveformVector[iWaveform].GetID() == int(PlotIDsVector[iChannel].ChannelID))
	    {
	      //maxADC and minADC have different assignments depending on if all the y-axises have the same scale or not.
	      if ((commonScale) & (WaveformVector[iWaveform].GetID()>0))
		{
		  WaveformVector[iWaveform].TransformRaw(canvasMinX, canvasMaxX,
							 canvasMinY, canvasMaxY,
							 nPlots, iChannel);
		  WaveformVector[iWaveform].TransformCalib(canvasMinX,canvasMaxX                                                        ,canvasMinYCalib, 
                                                        canvasMaxYCalib,							        nPlots, iChannel);

		  maxADC = canvasMaxY;
		  minADC = canvasMinY;
		  maxQCalib=canvasMaxYCalib;
		  minQCalib=canvasMinYCalib;
		}
	      else
		{
		  WaveformVector[iWaveform].TransformRaw(canvasMinX, canvasMaxX, 
							 nPlots, iChannel);

		  WaveformVector[iWaveform].TransformCalib(canvasMinX, canvasMaxX, 
							 nPlots, iChannel);
		  maxADC = WaveformVector[iWaveform].GetMaxADC();
		  minADC = WaveformVector[iWaveform].GetMinADC();
		  maxQCalib=WaveformVector[iWaveform].GetMaxQCalib();
		  minQCalib=WaveformVector[iWaveform].GetMinQCalib();
		}
	      for (UInt_t iZLE = 0;
		   iZLE < WaveformVector[iWaveform].GetTransform().size();
		   iZLE++)
		{

		  Int_t nPoints = WaveformVector[iWaveform].GetTransform()[iZLE].Samples.size();
		  TPolyLine * tempPolyLine = new TPolyLine();
		  TPolyLine * tempPolyLineCalib=new TPolyLine();
		  vector <double> TempXVector = 
		    WaveformVector[iWaveform].GetTransform()[iZLE].Samples;
		  vector <double> TempYVector = 
		    WaveformVector[iWaveform].GetTransform()[iZLE].ADCs;
		  vector <double> TempYVectorCalib;
		  vector <double> TempXVectorCalib;
		  //		  fprintf(stderr,"TempXVector %zu \n",TempXVector.size());
		  if(iZLE<WaveformVector[iWaveform].GetCalibTransform().size())
		    {
		      TempYVectorCalib = 
			WaveformVector[iWaveform].GetCalibTransform()[iZLE].QCalib;
		      TempXVectorCalib=WaveformVector[iWaveform].GetCalibTransform()[iZLE].CalibSamples;
		
		    }
		  for (Int_t iPoint = 0; iPoint < nPoints; iPoint++)
		    {
		      if (TempXVector[iPoint] >= 0.12 && TempXVector[iPoint] <= 0.95)
			{
			  
			  tempPolyLine->SetNextPoint(TempXVector[iPoint], 
						     TempYVector[iPoint]);
			 
			}
		    }
		  //	  fprintf(stderr,"TempYCalibSize %zu, TempXcalibsize %zu \n",TempYVectorCalib.size(),TempXVectorCalib.size());
		  for(size_t iPoint=0;iPoint<TempYVectorCalib.size();iPoint++)
		    {
		      if(TempXVectorCalib[iPoint] >= 0.12 && TempXVectorCalib[iPoint] <= 0.95)
			{
			  tempPolyLineCalib->SetNextPoint(TempXVectorCalib[iPoint],TempYVectorCalib[iPoint]);
			}
		      
		    }

		  PolyLineVector.push_back(tempPolyLine);
		  if(calibrated)//if(TempYVector.size()==TempYVectorCalib.size())
		    PolyLineCalibVector.push_back(tempPolyLineCalib);
		  
		  if(drawFill)
		    {
		      if(WaveformVector[iWaveform].GetID()>0)
			{
			  PolyLineVector.back()->SetFillColor(1);
			  PolyLineVector.back()->Draw("f");
			}
		      calibTCanvas->cd();
		      if(PolyLineCalibVector.size()>0)
			{
			  PolyLineCalibVector.back()->SetFillColor(1);
			  PolyLineCalibVector.back()->Draw("f");
			}
			  rawTCanvas->cd();
		    }
		  else
		    {
		      if(WaveformVector[iWaveform].GetID()>0)
			{
			  PolyLineVector.back()->Draw();
			}
		      calibTCanvas->cd();
		      if(PolyLineCalibVector.size()>0)//TempYVector.size()==TempYVectorCalib.size())
			PolyLineCalibVector.back()->Draw();
		      rawTCanvas->cd();
		    }
		}
	    }
	}
      
      if(!calibrated)
	{
	  calibTCanvas->cd();
	  TText* notCalibText=new TText();
	  notCalibText->DrawText(.1,.5,"No Calibrated Data.  PMT Select and Display Select");
	  notCalibText->DrawText(.1,.4," may be imperfect because of this");
	  rawTCanvas->cd();
	}

      if(DEBUG){fprintf(stderr,"ChannelID:%3i /n",PlotIDsVector[iChannel].ChannelID);}
      if(DEBUG){fprintf(stderr,"EventID:%1i /n",ratEV->GetEventID());}
      Double_t AxisMin = ((0.8/nPlots)*iChannel)+0.1;
      Double_t AxisMax = ((0.8/nPlots)*(iChannel+1))+0.1;
      if(DEBUG){fprintf(stderr,"AxisMin %4f",AxisMin);
	fprintf(stderr,"AxisMax %4f",AxisMax);}
      //Canvas PMT Labels 
      if (nPlots <= 50)
	{

	  char TempPMTLabelBuff [100];
	  if(int(PlotIDsVector[iChannel].ChannelID)==-100)
	    sprintf(TempPMTLabelBuff,"Summed");
	  else if((!labelByBoard))
	    sprintf(TempPMTLabelBuff, "PMT %3i", PlotIDsVector[iChannel].ChannelID);
	  else
	    sprintf(TempPMTLabelBuff, "WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);

	  TText *TempPMTLabel = new TText(0.01, 
					  AxisMin+(AxisMax-AxisMin)*.3, 
					  TempPMTLabelBuff);
	  TempPMTLabel->SetTextSize(0.020);
	  PmtLabelVector.push_back(TempPMTLabel);
	  PmtLabelVector.back()->Draw();
	  calibTCanvas->cd();
	  PmtLabelVector.back()->Draw();
	  rawTCanvas->cd();
	}
      else if (nPlots <= 64)
	{

	  char TempPMTLabelBuff [100];
	  if(int(PlotIDsVector[iChannel].ChannelID)==-100)
	    sprintf(TempPMTLabelBuff,"Summed");
	  else if(!labelByBoard)
	    sprintf(TempPMTLabelBuff, "PMT %3i", PlotIDsVector[iChannel].ChannelID);
	  else
	    sprintf(TempPMTLabelBuff, "WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);
	  TText *TempPMTLabel = new TText(0.01, 
					  AxisMin+(AxisMax-AxisMin)*.3, 
					  TempPMTLabelBuff);
	  TempPMTLabel->SetTextSize(0.015);
	  PmtLabelVector.push_back(TempPMTLabel);
	  PmtLabelVector.back()->Draw();
	  calibTCanvas->cd();
	  PmtLabelVector.back()->Draw();
	  rawTCanvas->cd();
	}
      else
	{


	  char TempPMTLabelBuff [100];
	  if(int(PlotIDsVector[iChannel].ChannelID)==-100)
	    sprintf(TempPMTLabelBuff,"Summed");
	  else if(!labelByBoard)
	    sprintf(TempPMTLabelBuff, "PMT %3i", PlotIDsVector[iChannel].ChannelID);
	  else
	    sprintf(TempPMTLabelBuff, "WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);
	  TText *TempPMTLabel = new TText(0.01, 
					  AxisMin+(AxisMax-AxisMin)*.3, 
					  TempPMTLabelBuff);
	  TempPMTLabel->SetTextSize(0.01);
	  PmtLabelVector.push_back(TempPMTLabel);
	  PmtLabelVector.back()->Draw();
	  calibTCanvas->cd();
	  PmtLabelVector.back()->Draw();
	  rawTCanvas->cd();
	}
    
      //y-axis
      if(maxADC==0)maxADC=1;
      if(maxADC==minADC)maxADC++;
      if (gridLinesY)
	{
	  if (nPlots > 4 && nPlots < 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 4, "NWI");
	      AxisVector.push_back(yAxis);
	      
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  4, "NWI");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else if (nPlots >= 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 2, "WU");
	      AxisVector.push_back(yAxis);
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  2, "WU");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }

	  else
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 1010, "W");
	      AxisVector.push_back(yAxis);
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  1010, "W");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  AxisVector.back()->SetLabelSize(.02);
	  AxisVector.back()->SetGridLength(0.83);
	  if(calibrated)
	    {
	      AxisVectorCalib.back()->SetLabelSize(.02);
	      AxisVectorCalib.back()->SetGridLength(0.83);
	    }
	}
      else
	{
	  if (nPlots > 4 && nPlots < 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 4, "NI");
	      AxisVector.push_back(yAxis);

	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  4, "NI");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else if (nPlots >= 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 2, "U");
	      AxisVector.push_back(yAxis);
	      
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  2, "U");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 1010);
	      AxisVector.push_back(yAxis);

	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  1010);
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  
	  AxisVector.back()->SetLabelSize(.02);
	  if(calibrated)
	    AxisVectorCalib.back()->SetLabelSize(.02);
	}
      AxisVector.back()->Draw();
      if((reductionLevelTrue==ReductionLevel::CHAN_ZLE_INTEGRAL)&(nPlots<=30))
	{
	  TLine * TempGroundLine=new TLine(.12,AxisMin,.95,AxisMin);
	  TempGroundLine->Draw();
	}
      if(drawSeperator)
	{
	  TLine * TempSeperatorLine=new TLine(.12,AxisMin,.95,AxisMin);
	  TempSeperatorLine->SetLineStyle(2);
	  TempSeperatorLine->Draw();
	}

      calibTCanvas->cd();
      if(calibrated) 
	{
	  AxisVectorCalib.back()->Draw();
	  if((reductionLevelTrue==ReductionLevel::CHAN_ZLE_INTEGRAL)&(nPlots<=30))
	    {
	      TLine * TempGroundLine=new TLine(.12,AxisMin,.95,AxisMin);
	      TempGroundLine->Draw();
	    }
	  if(drawSeperator)
	    {
	      TLine * TempSeperatorLine=new TLine(.12,AxisMin,.95,AxisMin);
	      TempSeperatorLine->SetLineStyle(2);
	      TempSeperatorLine->Draw();
	    }
	}

      rawTCanvas->cd();
      

    }


  
  if(DEBUG){fprintf(stderr,"CanvasMinX %4d",canvasMinX);fprintf(stderr," CanvasMaxX %4d",canvasMaxX);fprintf(stderr,"/n CanvasMinY %4d",canvasMinY);fprintf(stderr," CanvasMaxY %4d",canvasMaxY);}
  // Bottom X-axis
  if (canvasMinX != 9999999)
    {
      if (gridLinesX)
	{
	  TGaxis *xAxis = new TGaxis(0.15, 0.07,
				     0.95, 0.07,
				     canvasMinX, 
				     canvasMaxX, 
				     1212, "W");
	  xAxis->SetLabelSize(.02);
	  xAxis->SetTitle("ns");
	  xAxis->SetTitleSize(.025);
	  xAxis->SetGridLength(0.86);
	  AxisVector.push_back(xAxis);
	  AxisVector.back()->Draw();
	  AxisVectorCalib.push_back(xAxis);
	  calibTCanvas->cd();
	  AxisVectorCalib.back()->Draw();
	  rawTCanvas->cd();
	  
	}
      else
	{
	  TGaxis *xAxis = new TGaxis(0.15, 0.07,
				     0.95, 0.07,
				     canvasMinX, 
				     canvasMaxX, 
				     1212);
	  xAxis->SetLabelSize(.02);
	  xAxis->SetTitle("ns");
	  xAxis->SetTitleSize(.025);
	  AxisVector.push_back(xAxis);
	  AxisVector.back()->Draw();
	  AxisVectorCalib.push_back(xAxis);
	  calibTCanvas->cd();
	  AxisVectorCalib.back()->Draw();
	  rawTCanvas->cd();
	}
    }
  //Top X-Axis
  if (canvasMinX != 9999999)
    {
      TGaxis *xAxis = new TGaxis(0.15, 0.93,
				 0.95, 0.93,
				 canvasMinX, 
				 canvasMaxX, 
				 1212, "-");
      xAxis->SetLabelSize(.02);
      AxisVector.push_back(xAxis);
      AxisVector.back()->Draw();
      AxisVectorCalib.push_back(xAxis);
      calibTCanvas->cd();
      AxisVectorCalib.back()->Draw();
      rawTCanvas->cd();
    }
  if(labelByBoard)
    {
      yLabel->SetY(.92);
      yLabel->SetX(.03);
      yLabel->SetTextAngle(0);

      yLabelCalib->SetY(.92);
      yLabelCalib->SetX(.03);
      yLabelCalib->SetTextAngle(0);
    }
  else
    {
      yLabel->SetY(.77);
      yLabel->SetX(.08);
      yLabel->SetTextAngle(90);

      yLabelCalib->SetY(.77);
      yLabelCalib->SetX(.08);
      yLabelCalib->SetTextAngle(90);
    }
  yLabel->Draw();
  calibTCanvas->cd();
  yLabelCalib->Draw();
  rawTCanvas->cd();

  char evInfoBuffLine1[1000];
  sprintf(evInfoBuffLine1, 
	  "Event ID: %i;  Trigger Pattern: %#x;",
	  ratEV->GetEventID(), ratEV->GetTriggerPattern() 
	  );
  TText *evInfoTextLine1 = new TText(.005, .026, (const char* )evInfoBuffLine1);
  evInfoTextLine1->SetNDC();
  evInfoTextLine1->SetTextSize(0.02);
  evInfoTextLine1->SetTextColor(kBlue+3);
  evInfoTextLine1->Draw();
  calibTCanvas->cd();
  evInfoTextLine1->Draw();
  rawTCanvas->cd();
  

  char evInfoBuffLine2[1000];
  if(ratEV->GetBoardCount()>0)
    {
      sprintf(evInfoBuffLine2, 
	      "Trigger time: %lld; WFD time: %i; Reduction Level: %i; ZLE: %i;",
	      (long long)(UTCToTriggerTime(ratEV->GetUTC())),
	      (int)(ratEV->GetBoard(0)->GetHeader()[3]&0x7FFFFFFF),
	      ratEV->GetReductionLevel(),(int)(0x1 & ratEV->GetBoard(0)->GetHeader()[1]>>24));
    }
  TText *evInfoTextLine2 = new TText(.005, .005, (const char* )evInfoBuffLine2);
  evInfoTextLine2->SetNDC();
  evInfoTextLine2->SetTextSize(0.02);
  evInfoTextLine2->SetTextColor(kBlue+3);
  evInfoTextLine2->Draw();
  calibTCanvas->cd();
  evInfoTextLine2->Draw();
  rawTCanvas->cd();

  char runInfoBuff[1000];
  sprintf(runInfoBuff, 
	  "Run ID: %i;",
	  ratDS->GetRunID());
  TText *runInfoText = new TText(.005, .955, (const char* )runInfoBuff);
  runInfoText->SetNDC();
  runInfoText->SetTextSize(0.02);
  runInfoText->SetTextColor(kBlue+3);
  runInfoText->Draw();
  calibTCanvas->cd();
  runInfoText->Draw();
  rawTCanvas->cd();

  char fileInfoBuff[1000];
  sprintf(fileInfoBuff, 
	  "File: %s;",
	  dataFile);
  TText *fileInfoText = new TText(.005, .976, (const char* )fileInfoBuff);
  fileInfoText->SetNDC();
  fileInfoText->SetTextSize(0.02);
  fileInfoText->SetTextColor(kBlue+3);
  fileInfoText->Draw();
  calibTCanvas->cd();
  fileInfoText->Draw();
  rawTCanvas->cd();

  rawTCanvas->SetFillColor(kWhite);
  rawTCanvas->SetEditable(kFALSE);
  calibTCanvas->SetFillColor(kWhite);
  calibTCanvas->SetEditable(kFALSE);

}


void CLEANViewer::PolyLineZoomAdjust()
{


  
  rawTCanvas->SetEditable(kTRUE);
  calibTCanvas->SetEditable(kTRUE);

  //Clearing polylines to make room for new ones
 
  for(UInt_t iPVector = 0;
      iPVector < PolyLineVector.size();
      iPVector++)
    {
      delete PolyLineVector[iPVector];
    }
  PolyLineVector.clear();
  PolyLineCalibVector.clear();
  //Clearing Axes to make room for new ones
 
  for(UInt_t iAVector = 0;
      iAVector < AxisVector.size();
      iAVector++)
    {
      delete AxisVector[iAVector];
    }

  AxisVector.clear();
  AxisVectorCalib.clear();
  //Clearing Labels to make room for new ones
 
  for(UInt_t iLVector = 0;
      iLVector < PmtLabelVector.size();
      iLVector++)
    delete PmtLabelVector[iLVector];
  PmtLabelVector.clear();
  //cd to the raw canvas so that the polylines are drawn in the correct tab
  calibTCanvas->cd();
  calibTCanvas->Clear();
  rawTCanvas->cd();
  rawTCanvas->Clear();
  
  int nPlots = int(PlotIDsVector.size());

  canvasMinX = int(zoomSlider->GetMinPosition());
  canvasMaxX = int(zoomSlider->GetMaxPosition());

  canvasMinY = 9999999;
  canvasMaxY = -9999999;

  canvasMinYCalib = 9999999.9;
  canvasMaxYCalib = -9999999.9;
  //only set the canvasMinX and canvasMaxX if the zoom hasn't been adjusted
 
  //loop for finding the canvasMin and canvasMax
 
  for (UInt_t iChannel = 0; iChannel < PlotIDsVector.size(); iChannel++)
    {
      for (UInt_t iWaveform = 0; 
	   iWaveform < WaveformVector.size(); 
	   iWaveform++)
	{
	  if (WaveformVector[iWaveform].GetID() == int(PlotIDsVector[iChannel].ChannelID))
	    {
	      if(!zoomAdjusted)
		{
		  if (WaveformVector[iWaveform].GetMinSample(true) < canvasMinX)
		    canvasMinX = int(WaveformVector[iWaveform].GetMinSample(true));
		  if (WaveformVector[iWaveform].GetMaxSample(true) > canvasMaxX)
		    canvasMaxX = int(WaveformVector[iWaveform].GetMaxSample(true));

		}
	      if (WaveformVector[iWaveform].GetMinADC() < canvasMinY)
		canvasMinY = int(WaveformVector[iWaveform].GetMinADC());
	      if (WaveformVector[iWaveform].GetMaxADC() > canvasMaxY)
		canvasMaxY = int(WaveformVector[iWaveform].GetMaxADC());
	      if (WaveformVector[iWaveform].GetMinQCalib() < canvasMinYCalib)
		canvasMinYCalib = WaveformVector[iWaveform].GetMinQCalib();
	      if (WaveformVector[iWaveform].GetMaxQCalib() > canvasMaxYCalib)
		canvasMaxYCalib = WaveformVector[iWaveform].GetMaxQCalib();
	    }
	}
    }
  //just a check so that the axis doesn't change everytime a new pmt is checked
  

  //loop for plotting the waveforms (if it's confusing, it's because it is kind of)
 
  for (UInt_t iChannel = 0; iChannel < PlotIDsVector.size(); iChannel++)
    {
      //by declaring the max/minADC within the loop it causes the y-axis for pmts without any ZLE windows to range from 0-1.
      double maxADC = 1;
      double minADC = 0;
      double maxQCalib=1;
      double minQCalib=0;
      for (UInt_t iWaveform = 0; 
	   iWaveform < WaveformVector.size(); 
	   iWaveform++)
	{
	  //Plot only the waveforms with the same ID as the PMTs checked
	  if (WaveformVector[iWaveform].GetID() == int(PlotIDsVector[iChannel].ChannelID))
	    {
	      //maxADC and minADC have different assignments depending on if all the y-axises have the same scale or not.
	      if ((commonScale)&(WaveformVector[iWaveform].GetID()>0))
		{
		  WaveformVector[iWaveform].TransformRaw(canvasMinX, canvasMaxX,
							 canvasMinY, canvasMaxY,
							 nPlots, iChannel);
		  WaveformVector[iWaveform].TransformCalib(canvasMinX,canvasMaxX                                                        ,canvasMinYCalib, 
                                                        canvasMaxYCalib,							        nPlots, iChannel);

		  maxADC = canvasMaxY;
		  minADC = canvasMinY;
		  maxQCalib=canvasMaxYCalib;
		  minQCalib=canvasMinYCalib;
		}
	      else
		{
		  WaveformVector[iWaveform].TransformRaw(canvasMinX, canvasMaxX, 
							 nPlots, iChannel);

		  WaveformVector[iWaveform].TransformCalib(canvasMinX, canvasMaxX, 
							 nPlots, iChannel);
		  maxADC = WaveformVector[iWaveform].GetMaxADC();
		  minADC = WaveformVector[iWaveform].GetMinADC();
		  maxQCalib=WaveformVector[iWaveform].GetMaxQCalib();
		  minQCalib=WaveformVector[iWaveform].GetMinQCalib();
		  
		}

	      for (UInt_t iZLE = 0;
		   iZLE < WaveformVector[iWaveform].GetTransform().size();
		   iZLE++)
		{
		  Int_t nPoints = WaveformVector[iWaveform].GetTransform()[iZLE].Samples.size();
		  TPolyLine * tempPolyLine = new TPolyLine();
		  TPolyLine * tempPolyLineCalib=new TPolyLine();
		  vector <double> TempXVector = 
		    WaveformVector[iWaveform].GetTransform()[iZLE].Samples;
		  vector <double> TempYVector = 
		    WaveformVector[iWaveform].GetTransform()[iZLE].ADCs;
		  vector <double> TempYVectorCalib;
		  vector <double> TempXVectorCalib;
		  if(iZLE<WaveformVector[iWaveform].GetCalibTransform().size())
		    {
		      TempYVectorCalib = 
			WaveformVector[iWaveform].GetCalibTransform()[iZLE].QCalib;
		      TempXVectorCalib = WaveformVector[iWaveform].GetCalibTransform()[iZLE].CalibSamples;
		    }
		  for (Int_t iPoint = 0; iPoint < nPoints; iPoint++)
		    {
		      if (TempXVector[iPoint] >= 0.12 && TempXVector[iPoint] <= 0.95)
			{
			  tempPolyLine->SetNextPoint(TempXVector[iPoint], 
						     TempYVector[iPoint]);
			}
		    }
		  for(size_t iPoint=0;iPoint<TempYVectorCalib.size();iPoint++)
		    {
		      if(TempXVectorCalib[iPoint] >= 0.12 && TempXVector[iPoint] <= 0.95)
			{
			  tempPolyLineCalib->SetNextPoint(TempXVectorCalib[iPoint],TempYVectorCalib[iPoint]);
			}
		    }

		  PolyLineVector.push_back(tempPolyLine);
		  if(calibrated)//TempYVector.size()==TempYVectorCalib.size())
		    PolyLineCalibVector.push_back(tempPolyLineCalib);
		  
		  if(drawFill)
		    {
		      if(WaveformVector[iWaveform].GetID()>0)
			{
			  PolyLineVector.back()->SetFillColor(1);
			  PolyLineVector.back()->Draw("f");
			}
		      calibTCanvas->cd();
		      if(PolyLineCalibVector.size()>0)
			{
			  PolyLineCalibVector.back()->SetFillColor(1);
			  PolyLineCalibVector.back()->Draw("f");
			}
		      rawTCanvas->cd();
		    }
		  else
		    {
		      if(WaveformVector[iWaveform].GetID()>0)
			{
			  PolyLineVector.back()->Draw();
			}
		      calibTCanvas->cd();
		      if(PolyLineCalibVector.size()>0)
			PolyLineCalibVector.back()->Draw();
		      rawTCanvas->cd();
		    }
		}
	    }
	}
      

      if(DEBUG){fprintf(stderr,"ChannelID:%3i /n",PlotIDsVector[iChannel].ChannelID);}
      if(DEBUG){fprintf(stderr,"EventID:%1i /n",ratEV->GetEventID());}
      Double_t AxisMin = ((0.8/nPlots)*iChannel)+0.1;
      Double_t AxisMax = ((0.8/nPlots)*(iChannel+1))+0.1;
      if(DEBUG){fprintf(stderr,"AxisMin %4f",AxisMin);
	fprintf(stderr,"AxisMax %4f",AxisMax);}
      //Canvas PMT Labels 
     
      if (nPlots <= 50)
	{
	  char TempPMTLabelBuff [100];
	  if(int(PlotIDsVector[iChannel].ChannelID)==-100)
	    sprintf(TempPMTLabelBuff,"Summed");
	  else if(!labelByBoard)
	    sprintf(TempPMTLabelBuff, "PMT %3i", PlotIDsVector[iChannel].ChannelID);
	  else
	    sprintf(TempPMTLabelBuff, "WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);
	  TText *TempPMTLabel = new TText(0.01, 
					  AxisMin+(AxisMax-AxisMin)*.3, 
					  TempPMTLabelBuff);
	  TempPMTLabel->SetTextSize(0.020);
	  PmtLabelVector.push_back(TempPMTLabel);
	  PmtLabelVector.back()->Draw();
	  calibTCanvas->cd();
	  PmtLabelVector.back()->Draw();
	  rawTCanvas->cd();
	}
      else if (nPlots <= 64)
	{
	  char TempPMTLabelBuff [100];
	  if(int(PlotIDsVector[iChannel].ChannelID)==-100)
	    sprintf(TempPMTLabelBuff,"Summed");
	  else if(!labelByBoard)
	    sprintf(TempPMTLabelBuff, "PMT %3i", PlotIDsVector[iChannel].ChannelID);
	  else
	    sprintf(TempPMTLabelBuff, "WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);
	  TText *TempPMTLabel = new TText(0.01, 
					  AxisMin+(AxisMax-AxisMin)*.3, 
					  TempPMTLabelBuff);
	  TempPMTLabel->SetTextSize(0.015);
	  PmtLabelVector.push_back(TempPMTLabel);
	  PmtLabelVector.back()->Draw();
	  calibTCanvas->cd();
	  PmtLabelVector.back()->Draw();
	  rawTCanvas->cd();
	}
      else
	{
	 
	  char TempPMTLabelBuff [100];
	  
	  if(int(PlotIDsVector[iChannel].ChannelID)==-100)
	    sprintf(TempPMTLabelBuff,"Summed");
	  else if(!labelByBoard)
	    sprintf(TempPMTLabelBuff, "PMT %3i", PlotIDsVector[iChannel].ChannelID);
	  else
	    sprintf(TempPMTLabelBuff, "WFD %i,In %i",PlotIDsVector[iChannel].WFDID,PlotIDsVector[iChannel].InputID);

	  
	 
	  TText *TempPMTLabel = new TText(0.01, 
					  AxisMin+(AxisMax-AxisMin)*.3, 
					  TempPMTLabelBuff);
	 
	  TempPMTLabel->SetTextSize(0.01);
	  PmtLabelVector.push_back(TempPMTLabel);
	 
	  PmtLabelVector.back()->Draw();
	  calibTCanvas->cd();
	  PmtLabelVector.back()->Draw();
	  rawTCanvas->cd();
	}
    
    
      //y-axis
      if(maxADC==minADC)maxADC++;
      if (gridLinesY)
	{
	  if (nPlots > 4 && nPlots < 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 4, "NWI");
	      AxisVector.push_back(yAxis);
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  4, "NWI");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else if (nPlots >= 10)
	    {
	     
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 2, "WU");
	      AxisVector.push_back(yAxis);
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  2, "WU");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 1010, "W");
	      AxisVector.push_back(yAxis);
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  1010, "W");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  AxisVector.back()->SetLabelSize(.02);
	  AxisVector.back()->SetGridLength(0.83);
	  if(calibrated)
	    {
	      AxisVectorCalib.back()->SetLabelSize(.02);
	      AxisVectorCalib.back()->SetGridLength(0.83);
	    }
	}
      else
	{
	  if (nPlots > 4 && nPlots < 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 4, "NI");
	      AxisVector.push_back(yAxis);
	      
	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  4, "NI");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else if (nPlots >= 10)
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 2, "U");
	      AxisVector.push_back(yAxis);

	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  2, "U");
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  else
	    {
	      TGaxis *yAxis = new TGaxis(0.12, AxisMin,
					 0.12, AxisMax,
					 minADC, 
					 maxADC, 
					 1010);
	      AxisVector.push_back(yAxis);

	      if(calibrated)
		{
		  TGaxis *yAxisCalib = new TGaxis(0.12, AxisMin,
						  0.12, AxisMax,
						  minQCalib, 
						  maxQCalib, 
						  1010);
		  AxisVectorCalib.push_back(yAxisCalib);
		}
	    }
	  
	  AxisVector.back()->SetLabelSize(.02);
	  if(calibrated)
	    AxisVectorCalib.back()->SetLabelSize(.02);
	}
      AxisVector.back()->Draw();
      if((reductionLevelTrue==ReductionLevel::CHAN_ZLE_INTEGRAL)&(nPlots<=30))
	{
	  TLine * TempGroundLine=new TLine(.12,AxisMin,.95,AxisMin);
	  TempGroundLine->Draw();
	}
      if(drawSeperator)
	{
	  TLine * TempSeperatorLine=new TLine(.12,AxisMin,.95,AxisMin);
	  TempSeperatorLine->SetLineStyle(2);
	  TempSeperatorLine->Draw();
	}
      calibTCanvas->cd();
      if(calibrated)
	AxisVectorCalib.back()->Draw();
      if((reductionLevelTrue==ReductionLevel::CHAN_ZLE_INTEGRAL)&(nPlots<=30))
	{
	  TLine * TempGroundLine=new TLine(.12,AxisMin,.95,AxisMin);
	  TempGroundLine->Draw();
	}
      if(drawSeperator)
	{
	  TLine * TempSeperatorLine=new TLine(.12,AxisMin,.95,AxisMin);
	  TempSeperatorLine->SetLineStyle(2);
	  TempSeperatorLine->Draw();
	}
      rawTCanvas->cd();
    }
  
  if(DEBUG){fprintf(stderr,"CanvasMinX %4d",canvasMinX);fprintf(stderr," CanvasMaxX %4d",canvasMaxX);fprintf(stderr,"/n CanvasMinY %4d",canvasMinY);fprintf(stderr," CanvasMaxY %4d",canvasMaxY);}
  // Bottom X-axis
  if (canvasMinX != 9999999)
    {
      if (gridLinesX)
	{
	  TGaxis *xAxis = new TGaxis(0.15, 0.07,
				     0.95, 0.07,
				     canvasMinX, 
				     canvasMaxX, 
				     1212, "W");
	  xAxis->SetLabelSize(.02);
	  xAxis->SetTitle("ns");
	  xAxis->SetTitleSize(.025);
	  xAxis->SetGridLength(0.86);
	  AxisVector.push_back(xAxis);
	  AxisVector.back()->Draw();
	  AxisVectorCalib.push_back(xAxis);
	  calibTCanvas->cd();
	  AxisVectorCalib.back()->Draw();
	  rawTCanvas->cd();
	  
	}
      else
	{
	  TGaxis *xAxis = new TGaxis(0.15, 0.07,
				     0.95, 0.07,
				     canvasMinX, 
				     canvasMaxX, 
				     1212);
	  xAxis->SetLabelSize(.02);
	  xAxis->SetTitle("ns");
	  xAxis->SetTitleSize(.025);
	  AxisVector.push_back(xAxis);
	  AxisVector.back()->Draw();
	  AxisVectorCalib.push_back(xAxis);
	  calibTCanvas->cd();
	  AxisVectorCalib.back()->Draw();
	  rawTCanvas->cd();
	}
    }
  //Top X-Axis
  if (canvasMinX != 9999999)
    {
      TGaxis *xAxis = new TGaxis(0.15, 0.93,
				 0.95, 0.93,
				 canvasMinX, 
				 canvasMaxX, 
				 1212, "-");
      xAxis->SetLabelSize(.02);
      AxisVector.push_back(xAxis);
      AxisVector.back()->Draw();
      AxisVectorCalib.push_back(xAxis);
      calibTCanvas->cd();
      AxisVectorCalib.back()->Draw();
      rawTCanvas->cd();
    }
  
  yLabel->Draw();
  calibTCanvas->cd();
  yLabelCalib->Draw();
  rawTCanvas->cd();

  char evInfoBuffLine1[1000];
  sprintf(evInfoBuffLine1, 
	  "Event ID: %i;  Trigger Pattern: %#x;",
	  ratEV->GetEventID(), ratEV->GetTriggerPattern() 
	  );
  TText *evInfoTextLine1 = new TText(.005, .026, (const char* )evInfoBuffLine1);
  evInfoTextLine1->SetTextSize(0.02);
  evInfoTextLine1->SetTextColor(kBlue+3);
  evInfoTextLine1->Draw();
  calibTCanvas->cd();
  evInfoTextLine1->Draw();
  rawTCanvas->cd();
  

  char evInfoBuffLine2[1000];
  if(ratEV->GetBoardCount()>0)
    {
      sprintf(evInfoBuffLine2, 
	      "Trigger time: %llu; WFD time: %X; Reduction Level: %i; ZLE: %i; ",
	      (long long unsigned int)UTCToTriggerTime(ratEV->GetUTC()),
	      ratEV->GetBoard(0)->GetHeader()[3]&0x7FFFFFFF,
	      ratEV->GetReductionLevel(),(int)(0x1 & ratEV->GetBoard(0)->GetHeader()[1]>>24));
    }
  TText *evInfoTextLine2 = new TText(.005, .005, (const char* )evInfoBuffLine2);
  evInfoTextLine2->SetTextSize(0.02);
  evInfoTextLine2->SetTextColor(kBlue+3);
  evInfoTextLine2->Draw();
  calibTCanvas->cd();
  evInfoTextLine2->Draw();
  rawTCanvas->cd();

  char runInfoBuff[1000];
  sprintf(runInfoBuff, 
	  "Run ID: %i;",
	  ratDS->GetRunID());
  TText *runInfoText = new TText(.005, .955, (const char* )runInfoBuff);
  runInfoText->SetTextSize(0.02);
  runInfoText->SetTextColor(kBlue+3);
  runInfoText->Draw();
  calibTCanvas->cd();
  runInfoText->Draw();
  rawTCanvas->cd();

  char fileInfoBuff[1000];
  sprintf(fileInfoBuff, 
	  "File: %s;",
	  dataFile);
  TText *fileInfoText = new TText(.005, .976, (const char* )fileInfoBuff);
  fileInfoText->SetTextSize(0.02);
  fileInfoText->SetTextColor(kBlue+3);
  fileInfoText->Draw();
  calibTCanvas->cd();
  fileInfoText->Draw();
  rawTCanvas->cd();

  zoomAdjusted=kTRUE;

  rawTCanvas->SetFillColor(kWhite);
  rawTCanvas->SetEditable(kFALSE);
  calibTCanvas->SetFillColor(kWhite);
  calibTCanvas->SetEditable(kFALSE);
}


void CLEANViewer::DrawPromptTotalHist(Int_t activeChannels)
{
  rawTCanvas->SetEditable(kTRUE);

  rawTCanvas->cd();
  rawTCanvas->Clear();

  drawStyle = new TStyle();
 
  PromptChannelsHist->SetFillColor(2);
  LateChannelsHist->SetFillColor(1);

  PromptChannelsHist->SetTitle("");
  LateChannelsHist->SetTitle("");
  if(calibrated)
    {
      PromptCalibChannelsHist->SetFillColor(2);
      LateCalibChannelsHist->SetFillColor(1);
      
      PromptCalibChannelsHist->SetTitle("");
      LateCalibChannelsHist->SetTitle("");
    }	  
  
  //Set vertical range to size of histograms
 

  PromptChannelsHist->GetXaxis()->SetRange(1,activeChannels+1);
  LateChannelsHist->GetXaxis()->SetRange(1,activeChannels+1);

  //Set the horizontal range from the largest bin
  double maxValue = -1*(PromptChannelsHist->GetMinimum());

  

  if(LateChannelsHist->GetMaximum() > (-1*PromptChannelsHist->GetMinimum()))
    maxValue = LateChannelsHist->GetMaximum();



  double maxRange = maxValue;

  PromptChannelsHist->SetMaximum(maxRange);
  PromptChannelsHist->SetMinimum(-1*maxRange);
  LateChannelsHist->SetMaximum(maxRange);
  LateChannelsHist->SetMinimum(-1*maxRange);

  PromptChannelsHist->GetXaxis()->SetTickLength(0);
  LateChannelsHist->GetXaxis()->SetTickLength(0);

  rawTCanvas->SetPad(0,0,1,1);

  drawStyle->SetOptStat("");
  drawStyle->SetHistMinimumZero();
  drawStyle->cd();


  PromptChannelsHist->Draw("hbar");
  LateChannelsHist->Draw("hbarAsame");
  
  //Draw the axes (MEMORY LEAK(maybe), FIX!!!!!)


  TGaxis *lateAxis = new TGaxis(0,
				gPad->GetUymin(),
				maxRange,gPad->GetUymin(),
				0,maxRange,510);


  lateAxis->SetLabelSize(.02);
  lateAxis->SetTitleOffset(1.25);
  lateAxis->SetTitle("Late Charge (ADC Counts)");
  lateAxis->SetTitleSize(.025);
  lateAxis->SetGridLength(0.86);
  lateAxis->Draw();



  TGaxis *promptAxis = new TGaxis(0,
				  gPad->GetUymin(),
				  -maxRange,gPad->GetUymin(),
				  0,maxRange,510,"-");
  promptAxis->SetLabelSize(.02);
  promptAxis->SetLabelOffset(-0.018);
  promptAxis->SetTitleOffset(1.25);
  promptAxis->SetTitle("Prompt Charge (ADC Counts)");
  promptAxis->SetTitleSize(.025);
  promptAxis->SetGridLength(0.86);
  promptAxis->Draw();


 
  if(calibrated)
    {
      calibTCanvas->SetEditable(kTRUE);

      calibTCanvas->cd();
      calibTCanvas->Clear();

      drawStyle = new TStyle();


    

      //Set vertical range to size of histograms
     
     
      PromptCalibChannelsHist->GetXaxis()->SetRange(0,activeChannels);
      LateCalibChannelsHist->GetXaxis()->SetRange(0,activeChannels);

      //Set the horizontal range from the largest bin
      maxValue = -1*(PromptCalibChannelsHist->GetMinimum());



      if(LateCalibChannelsHist->GetMaximum() > (-1*PromptCalibChannelsHist->GetMinimum()))
	maxValue = LateCalibChannelsHist->GetMaximum();

 

      maxRange = maxValue;

      PromptCalibChannelsHist->SetMaximum(maxRange);
      PromptCalibChannelsHist->SetMinimum(-1*maxRange);
      LateCalibChannelsHist->SetMaximum(maxRange);
      LateCalibChannelsHist->SetMinimum(-1*maxRange);

      PromptCalibChannelsHist->GetXaxis()->SetTickLength(0);
      LateCalibChannelsHist->GetXaxis()->SetTickLength(0);

      calibTCanvas->SetPad(0,0,1,1);

      drawStyle->SetOptStat("");
      drawStyle->SetHistMinimumZero();
      drawStyle->cd();


      PromptCalibChannelsHist->Draw("hbar");
      LateCalibChannelsHist->Draw("hbarAsame");
  
      //Draw the axes


      lateAxis = new TGaxis(0,
			    gPad->GetUymin(),
			    maxRange,gPad->GetUymin(),
			    0,maxRange,510);


      lateAxis->SetLabelSize(.02);
      lateAxis->SetTitleOffset(1.25);
      lateAxis->SetTitle("Late Charge (pC)");
      lateAxis->SetTitleSize(.025);
      lateAxis->SetGridLength(0.86);
      lateAxis->Draw();

   

      promptAxis = new TGaxis(0,
			      gPad->GetUymin(),
			      -maxRange,gPad->GetUymin(),
			      0,maxRange,510,"-");
      promptAxis->SetLabelSize(.02);
      promptAxis->SetLabelOffset(-0.018);
      promptAxis->SetTitleOffset(1.25);
      promptAxis->SetTitle("Prompt Charge (pC)");
      promptAxis->SetTitleSize(.025);
      promptAxis->SetGridLength(0.86);
      promptAxis->Draw();
    }
    
  rawTCanvas->cd();

  char evInfoBuffLine1[1000];
  sprintf(evInfoBuffLine1, 
	  "Event ID: %i;  Trigger Pattern: %#x; ",
	  ratEV->GetEventID(), ratEV->GetTriggerPattern() 
	  );
  TText *evInfoTextLine1 = new TText(.005, .026, (const char* )evInfoBuffLine1);
  evInfoTextLine1->SetNDC();
  evInfoTextLine1->SetTextSize(0.02);
  evInfoTextLine1->SetTextColor(kBlue+3);
  evInfoTextLine1->Draw();
  calibTCanvas->cd();
  evInfoTextLine1->Draw();
  rawTCanvas->cd();
  

  char evInfoBuffLine2[1000];
  if(ratEV->GetBoardCount()>0)
    {
      sprintf(evInfoBuffLine2, 
	      "Trigger time: %lld; WFD time: %i; Reduction Level: %i; ZLE: %i; ",
	      (long long)(UTCToTriggerTime(ratEV->GetUTC())),
	      (int)(ratEV->GetBoard(0)->GetHeader()[3]&0x7FFFFFFF),
	      ratEV->GetReductionLevel(),(int)(0x1 & ratEV->GetBoard(0)->GetHeader()[1]>>24));
    }
  TText *evInfoTextLine2 = new TText(.005, .005, (const char* )evInfoBuffLine2);
  evInfoTextLine2->SetNDC();
  evInfoTextLine2->SetTextSize(0.02);
  evInfoTextLine2->SetTextColor(kBlue+3);
  evInfoTextLine2->Draw();
  calibTCanvas->cd();
  evInfoTextLine2->Draw();
  rawTCanvas->cd();

  char runInfoBuff[1000];
  sprintf(runInfoBuff, 
	  "Run ID: %i;",
	  ratDS->GetRunID());
  TText *runInfoText = new TText(.005, .955, (const char* )runInfoBuff);
  runInfoText->SetNDC();
  runInfoText->SetTextSize(0.02);
  runInfoText->SetTextColor(kBlue+3);
  runInfoText->Draw();
  calibTCanvas->cd();
  runInfoText->Draw();
  rawTCanvas->cd();

  char fileInfoBuff[1000];
  sprintf(fileInfoBuff, 
	  "File: %s;",
	  dataFile);
  TText *fileInfoText = new TText(.005, .976, (const char* )fileInfoBuff);
  fileInfoText->SetNDC();
  fileInfoText->SetTextSize(0.02);
  fileInfoText->SetTextColor(kBlue+3);
  fileInfoText->Draw();
  calibTCanvas->cd();
  fileInfoText->Draw();
  rawTCanvas->cd();

  calibTCanvas->SetEditable(kFALSE);
  rawTCanvas->SetEditable(kFALSE); 
}


/*void CLEANViewer::DrawEventDisplay()
{
  displayTabTCanvas->SetEditable(kTRUE);
  displayTabTCanvas->cd();
  displayTabTCanvas->Clear();

  displayTCanvas->SetEditable(kTRUE);
  displayTCanvas->cd();
  displayTCanvas->Clear();

  
  ChannelDisplayVector.clear();
 

  col.clear();


  std::vector<TVector3> posVector;
  RAT::MCcassetteInfo* CI=ratDC->GetMCcassetteInfo();


  int nHit;
  for(int iBoard=0;iBoard<ratEV->GetBoardCount();iBoard++)
    {
      for(int iChannel=0;iChannel<ratEV->GetBoard(iBoard)->GetChannelCount())
	{
	  nHit=0;
	  for(int iRB=0;iRB<ratEV->GetBoard(iBoard)->GetChannel(iChannel))
	    {
	      
	  if(AvgDraw)
	    col.push_back(int(SummedChargeVector[iPMT]));
	  else
	    col.push_back(int(ratEV->GetPMT(iPMT)->GetTotalQ()));
	}
      else
	col.push_back(0);
    }
 

    double Red[4]={0,0,1,1};
    double Green[4]={0,1,0,0};
    double Blue[4]={1,0,1,0};
    double Stops[4]={0,.3,.67,1};

    int colMax=0;
    
                      
    for(unsigned int iCol=0;iCol<col.size();iCol++)
      {
	if(col[iCol]>colMax) colMax=col[iCol];
      }

    if((!AutoScaling)&(colMax<300))
      colMax=300;
	  
    
   
   
    ColI=TColor::CreateGradientColorTable(4,Stops,Red,Green,Blue,colMax+1);
   
    TArc* TempDisplayArc=new TArc();
    
    for(unsigned int iPMT=0;iPMT<pmtIDsVector.size();iPMT++)
      {
	int PMTID=pmtIDsVector[iPMT]-1000;
	
	if(PMTID<92 && PMTID>-1)
	  {
	    displayTCanvas->cd();
	    TArc* TempDisplayArc=new TArc(ChannelLocsXCentered[PMTID-PMTIDStart],ChannelLocsYCentered[PMTID-PMTIDStart],.02);
	    TArc* TempSelectArc=new TArc(ChannelLocsXCentered[PMTID-PMTIDStart],ChannelLocsYCentered[PMTID-PMTIDStart],.023);
	    TArc* TempArc=new TArc();
	    TempArc->SetFillColor(0);
	    TempSelectArc->SetFillColor(0);
	    TempDisplayArc->SetFillColor(ColI+col[PMTID-PMTIDStart]);
	
	
	    if(pmtCheckInfoVector.size()>=col.size())
	      {
	    
		int vecIndex=idGroupVectorMap[PMTID+1000];
	   

		if(pmtGroupVector[vecIndex]->GetButton(1000+PMTID)->IsOn())
		  {
		    TempArc->SetFillColor(2);
		    TempSelectArc->SetFillColor(1);
		  }
	  
	      }
	


	    char numberbuff[25];
	    sprintf(numberbuff,"%i",PMTID-PMTIDStart);
	    TText * PMTIDDisplayText=new TText(ChannelLocsX[PMTID-PMTIDStart]-.012,ChannelLocsY[PMTID-PMTIDStart]-.012,(const char* )numberbuff);
	    PMTIDDisplayText->SetTextSize(.03);

	    TempSelectArc->Draw();
	    PMTIDDisplayText->SetUniqueID(PMTID+1000);
	    TempDisplayArc->SetUniqueID(PMTID+1000);
	    TempDisplayArc->Draw();
	    displayTabTCanvas->cd();

	    TempArc->DrawArc(ChannelLocsX[PMTID-PMTIDStart],ChannelLocsY[PMTID-PMTIDStart],.025);

	    TText * PMTIDText=new TText(ChannelLocsX[PMTID-PMTIDStart]-.012,ChannelLocsY[PMTID-PMTIDStart]-.012,(const char* )numberbuff);
	    PMTIDText->SetTextSize(.03);
	    PMTIDText->Draw();
	    PMTIDText->SetUniqueID(PMTID+1000);
	  }	  
      }
    TempDisplayArc->IsA()->GetMenuList()->RemoveLast();
    TClassMenuItem *TempMenuItem=new TClassMenuItem(0,CLEANViewer::Class(),"Center On This PMT","DrawCentered",this,"TObject*",0);
    TempDisplayArc->IsA()->GetMenuList()->AddFirst(TempMenuItem);
    displayTabTCanvas->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
                                  "CLEANViewer", this,
                                  "PMTClicked(Int_t,Int_t,Int_t,TObject*)");

    displayTCanvas->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
                                  "CLEANViewer", this,
                                  "PMTDisplayClicked(Int_t,Int_t,Int_t,TObject*)");

   

      if(!calibrated)
	{
	  TText* notCalibDisplayText=new TText();
	  notCalibDisplayText->DrawText(.1,.5,"No Calibrated Data.  PMT Select and Display Select");
	  notCalibDisplayText->DrawText(.1,.4," may be imperfect because of this");
	  displayTCanvas->cd();

	  TText* notCalibText=new TText();
	  notCalibText->DrawText(.1,.5,"No Calibrated Data.  PMT Select and Display Select");
	  notCalibText->DrawText(.1,.4," may be imperfect because of this");


	}

     if(AvgDraw)
	{
	  displayTCanvas->cd();

	  TText* AvgDrawText=new TText();
	  AvgDrawText->DrawText(.05,.9,"Average of All Events");
	}

 
 
      displayTCanvas->SetEditable(kFALSE);
      displayTabTCanvas->SetEditable(kFALSE);
}

void CLEANViewer::DrawEventDisplay(int iPMT)
{
 
  displayTCanvas->SetEditable(kTRUE);
  displayTabTCanvas->SetEditable(kTRUE);

  TArc* TempDisplayArc=new TArc(ChannelLocsXCentered[iPMT],ChannelLocsYCentered[iPMT],.02);
  TArc* TempSelectArc=new TArc(ChannelLocsXCentered[iPMT],ChannelLocsYCentered[iPMT],.023);
  TArc* TempArc=new TArc();
 
  displayTCanvas->cd();
  TempArc->SetFillColor(0);
  TempSelectArc->SetFillColor(0);
  TempDisplayArc->SetFillColor(ColI+col[iPMT-PMTIDStart]);

 
  int vecIndex=idGroupVectorMap[iPMT+1000];


  if(pmtGroupVector[vecIndex]->GetButton(iPMT+1000)->IsOn())
    {
      TempArc->SetFillColor(2);
      TempSelectArc->SetFillColor(1);
    }

  TempSelectArc->Draw();
  TempDisplayArc->SetUniqueID(iPMT+1000);
  TempDisplayArc->Draw();

  TempDisplayArc->IsA()->GetMenuList()->RemoveLast();
  TClassMenuItem *TempMenuItem=new TClassMenuItem(0,CLEANViewer::Class(),"Center On This PMT","DrawCentered",this,"TObject*",0);
  TempDisplayArc->IsA()->GetMenuList()->AddFirst(TempMenuItem);

  displayTabTCanvas->cd();
    
  TempArc->SetUniqueID(iPMT+1000);
  TempArc->DrawArc(ChannelLocsX[iPMT],ChannelLocsY[iPMT],.025);
  char numberbuff[25];
  sprintf(numberbuff,"%i",iPMT);
  TText * PMTIDText=new TText(ChannelLocsX[iPMT]-.012,ChannelLocsY[iPMT]-.012,(const char* )numberbuff);
  PMTIDText->SetTextSize(.03);
  PMTIDText->Draw();
  PMTIDText->SetUniqueID(iPMT+1000);

  displayTabTCanvas->SetEditable(kFALSE);
  displayTCanvas->SetEditable(kFALSE);


}



void CLEANViewer::DrawCentered(TObject* pmt)
{ 
  int pmtID=pmt->GetUniqueID()-1000-PMTIDStart;
  CreateCenteredArrays(pmtID) ;
  DrawEventDisplay();
  displayTCanvas->Update();
  displayTCanvas->Modified();
 
}
*/
void CLEANViewer::CreateInfoWindow()
 {

   if(!InfoFrame)
     {
       fprintf(stderr,"InfoFrame\n");
       InfoFrame=v->GetLeftVerticalFrame();
  



       InfoECanvas=new TRootEmbeddedCanvas("Info",InfoFrame,195,300);
       InfoFrame->AddFrame(InfoECanvas,tabLayout);
       InfoCanvas=InfoECanvas->GetCanvas();
       Frames3D=new TGVerticalFrame(InfoFrame);
       InfoFrame->AddFrame(Frames3D,tabLayout);
       PMTSelect3D=new TGGroupFrame(Frames3D,"PMT Highlight",kHorizontalFrame);
       PMTSelect3D->SetTitlePos(TGGroupFrame::kLeft);
       Frames3D->AddFrame(PMTSelect3D, new TGLayoutHints(kLHintsTop | 
								 kLHintsCenterX |
								 kLHintsExpandX,
								 10,10,10,10));
	   
       pmtSelectEntry3D=new TGNumberEntry(PMTSelect3D,0,4,1001,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,1000);
       pmtSelectEntry3D->SetState(kTRUE);
       pmtSelectEntry3D->SetNumber(0);
       selectPMT=0;
       pmtSelectEntry3D->Connect("ValueSet(Long_t)", "CLEANViewer", 
				 this, "pmtSelectChanged3D()");
       PMTSelect3D->AddFrame(pmtSelectEntry3D,tabLayout);
       highLightPMTButton=new TGCheckButton(PMTSelect3D,"Highlight");
       highLightPMTButton->Connect("Clicked()","CLEANViewer",this,"highLight()");
       PMTSelect3D->AddFrame(highLightPMTButton,tabLayout);
       if(ZMQ)
	 {
	   fprintf(stderr,"STREAM3D/n");
	   StreamFrame3D=new TGGroupFrame(Frames3D,"Event Stream",kHorizontalFrame);
	   StreamFrame3D->SetTitlePos(TGGroupFrame::kLeft);
	   Frames3D->AddFrame(StreamFrame3D,tabLayout);
	   StopButton3D=new TGPictureButton(StreamFrame3D,"icons/17.png");
	   StreamFrame3D->AddFrame(StopButton3D,tabLayout);
	   StartButton3D=new TGPictureButton(StreamFrame3D,"icons/162.png");
	   StreamFrame3D->AddFrame(StartButton3D,tabLayout);
	   StopButton3D->Connect("Clicked()","CLEANViewer",this,"StreamStop()");
	   StartButton3D->Connect("Clicked()","CLEANViewer",this,"StreamGo()");
	 }
       else
	 { 
	   eventGroup3D = new TGGroupFrame(Frames3D, "Event Select");
	   eventGroup3D->SetTitlePos(TGGroupFrame::kLeft);
	   Frames3D->AddFrame(eventGroup3D, new TGLayoutHints(kLHintsTop | 
								 kLHintsCenterX |
								 kLHintsExpandX,
								 10,10,10,10));
	   
	   //Construct and set initials for event select widgets
	   //number Entry widget
	   eventNumberEntry3D = new TGNumberEntry(eventGroup3D, 0, 15, 4000,
						  TGNumberFormat::kNESInteger,
						TGNumberFormat::kNEANonNegative,
						TGNumberFormat::kNELLimitMinMax,
						0,0);
	   
	   eventNumberEntry3D->SetState(kTRUE);
	   eventNumberEntry3D->SetNumber(ratEV->GetEventID());
	   eventNumberEntry3D->SetLimitValues(ratEV->GetEventID(),ratEV->GetEventID()+9999999);
	   eventNumberEntry3D->Connect("ValueSet(Long_t)", "CLEANViewer", 
				 this, "EventChange3D()");
	   eventGroup3D->AddFrame(eventNumberEntry3D, 
				     new TGLayoutHints(kLHintsTop | kLHintsLeft));
	   eventHorizontal3D = new TGHorizontalFrame(eventGroup3D, 180, 50);
	   eventGroup3D->AddFrame(eventHorizontal3D, new TGLayoutHints(kLHintsCenterX |
								   kLHintsTop,
								   5, 5, 5, 5));

	   //larger prev and next event buttons because ed doesn't have as young of eyes as us
	   prevEvent3D = new TGTextButton(eventHorizontal3D, "  <<  ", 4001);
	   prevEvent3D->Connect("Pressed()", "CLEANViewer", 
			      this, "PrevEventPressed()");
	   prevEvent3D->SetEnabled(kFALSE);
	   eventHorizontal3D->AddFrame(prevEvent3D,
				     new TGLayoutHints(kLHintsTop | kLHintsCenterX));
	   
	   nextEvent3D = new TGTextButton(eventHorizontal3D, 
					"  >>  ", 
					4002);
	   nextEvent3D->Connect("Pressed()", "CLEANViewer", 
			      this, "NextEventPressed()");
	   

	   eventHorizontal3D->AddFrame(nextEvent3D,
				 new TGLayoutHints(kLHintsTop | 
				 kLHintsRight));
	  
	 }
       ScaleSelectGroup=new TGGroupFrame(Frames3D,"Scaling");
       ScaleSelectGroup->SetTitlePos(TGGroupFrame::kLeft);
       Frames3D->AddFrame(ScaleSelectGroup, new TGLayoutHints(kLHintsTop | 
								 kLHintsCenterX |
								 kLHintsExpandX,
								 10,10,10,10));
       TGLabel * colorLabel=new TGLabel(ScaleSelectGroup,"Color:");
       ScaleSelectGroup->AddFrame(colorLabel,new TGLayoutHints(kLHintsTop | 
								 kLHintsLeft |
								 kLHintsExpandX,
								 0,0,0,0));
       ColorScaleSelectBox=new TGComboBox(ScaleSelectGroup,100);
       ScaleSelectGroup->AddFrame(ColorScaleSelectBox,tabLayout);
       ColorScaleSelectBox->AddEntry("Off",0);
       ColorScaleSelectBox->AddEntry("Total Q",1);
       ColorScaleSelectBox->AddEntry("Time",2);

       ColorScaleSelectBox->Select(colorScaleState,kFALSE);
       ColorScaleSelectBox->Connect("Selected(Int_t)", "CLEANViewer", 
			      this, "ColorScaleSelected(Int_t)");
       colorScaleHoriz=new TGHorizontalFrame(ScaleSelectGroup);
       ScaleSelectGroup->AddFrame(colorScaleHoriz,tabLayout);
       colorScaleMaxEntry=new TGNumberEntry(colorScaleHoriz, 0, 6, 1002,
						  TGNumberFormat::kNESRealOne,
						TGNumberFormat::kNEANonNegative,
						TGNumberFormat::kNELLimitMinMax,
					    0,10000);
       colorScaleMaxEntry->SetState(kTRUE);
       colorScaleMaxEntry->SetNumber(colorScaleMax);
       
       colorScaleMaxEntry->Connect("ValueSet(Long_t)", "CLEANViewer", 
				 this, "ColorScaleChange()");
       colorScaleHoriz->AddFrame(colorScaleMaxEntry,tabLayout);
       colorUnitsText=new TGLabel(colorScaleHoriz,"ns");
       colorScaleHoriz->AddFrame(colorUnitsText,tabLayout);


       TGLabel * sizeLabel=new TGLabel(ScaleSelectGroup,"Size:");
       ScaleSelectGroup->AddFrame(sizeLabel,tabLayout);
       SizeScaleSelectBox=new TGComboBox(ScaleSelectGroup,100);
       ScaleSelectGroup->AddFrame(SizeScaleSelectBox,tabLayout);
       SizeScaleSelectBox->AddEntry("Off",0);
       SizeScaleSelectBox->AddEntry("Total Q",1);
       SizeScaleSelectBox->AddEntry("Prompt Q",2);
       SizeScaleSelectBox->Select(sizeScaleState,kFALSE);
       SizeScaleSelectBox->Connect("Selected(Int_t)", "CLEANViewer", 
			      this, "SizeScaleSelected(Int_t)");
       sizeScaleHoriz=new TGHorizontalFrame(ScaleSelectGroup);
       ScaleSelectGroup->AddFrame(sizeScaleHoriz,tabLayout);
       sizeScaleMaxEntry=new TGNumberEntry(sizeScaleHoriz, 0, 6, 1002,
						  TGNumberFormat::kNESRealOne,
						TGNumberFormat::kNEANonNegative,
						TGNumberFormat::kNELLimitMinMax,
					    0,10000);
       sizeScaleMaxEntry->SetState(kTRUE);
       sizeScaleMaxEntry->SetNumber(sizeScaleMax);
       
       sizeScaleMaxEntry->Connect("ValueSet(Long_t)", "CLEANViewer", 
				 this, "SizeScaleChange()");
       sizeScaleHoriz->AddFrame(sizeScaleMaxEntry,tabLayout);
       sizeUnitsText=new TGLabel(sizeScaleHoriz,"pC");
       sizeScaleHoriz->AddFrame(sizeUnitsText,tabLayout);

       InfoFrame->Layout();
       InfoFrame->MapSubwindows();
   
     }
 
       InfoCanvas->cd();

       if(InfoTopText)
	 {

	   delete InfoTopText;
	   InfoTopText=NULL;
	 }
       
       if(!InfoTextPad)
	 {
	   InfoTextPad=new TPad("TextPad","TextPad",0,.35,1,1);
	   InfoTextPad->Draw();
	 }
       if(InfoPlotPad)
	 {

	   delete InfoPlotPad;
	 }
       InfoPlotPad=new TPad("PlotPad","PlotPad",0,0,1,.35);
       InfoPlotPad->Draw();
       InfoPlotPad->SetBorderMode(0);
       InfoPlotPad->SetFillColor(kWhite);
       InfoTextPad->Clear();
       InfoPlotPad->Clear();


       InfoTextPad->cd();
       InfoTopText=new TPaveText(.005,.995,.995,.005);
       
       
       char InfoBuff[100];
       sprintf(InfoBuff,"Data Source: %s",dataFile);
       InfoTopText->AddText(InfoBuff);
       
       sprintf(InfoBuff,"Run ID: %i",ratDS->GetRunID());
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"Event ID: %i",ratEV->GetEventID());
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"Trig Pattern: %#x",ratEV->GetTriggerPattern());
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"Reduction Level: %i",ratEV->GetReductionLevel());
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"Total Q in Event: %.2f pC",ratEV->GetTotalQ());
       InfoTopText->AddText(InfoBuff);
       //       InfoTopText->SetTextSize(0.02);

      

       
       sprintf(InfoBuff,"-----------------------------");
       InfoTopText->AddText(InfoBuff);
       
       sprintf(InfoBuff,"PMT ID : Summed");
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"WFD :");
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"Input :");
       InfoTopText->AddText(InfoBuff);
       sprintf(InfoBuff,"Total Q : %.2f pC",ratEV->GetTotalQ());
       InfoTopText->AddText(InfoBuff);
       InfoTopText->SetTextAlign();
       InfoTopText->SetFillColor(0);
       InfoTopText->SetBorderSize(0);
       
       InfoTopText->Draw();

       InfoTopText->SetTextSize(0.075);
       DrawMiniWaveform(-100);
       InfoCanvas->cd();
   
       InfoCanvas->Update();
       InfoCanvas->Modified();
       
 }
void CLEANViewer::DrawColorScale(int numCols)
{
  fprintf(stderr,"DrawCS\n");
  if(!ColorScaleWin)
    {
      fprintf(stderr,"Make WIndow\n");
      ColorScaleWin=new TGMainFrame(gClient->GetRoot(),100,300);
      ColorScaleWin->SetWindowName("Color Scale");
      colorScaleFrame=new TGCompositeFrame(ColorScaleWin,100,300);
      ColorScaleWin->AddFrame(colorScaleFrame,tabLayout);
      colorScaleECan=new TRootEmbeddedCanvas("Color Scale",colorScaleFrame,100,300);
      colorScaleFrame->AddFrame(colorScaleECan,tabLayout);
      colorScaleCanvas=colorScaleECan->GetCanvas();
      ColorScaleWin->Connect("CloseWindow()","CLEANViewer",this,"ColorScaleClosed()");
      ColorScaleWin->DontCallClose();
      ColorScaleWin->MapSubwindows();
      ColorScaleWin->MapWindow();
    }

  colorScaleCanvas->cd();
  colorScaleCanvas->Clear();
  TText *ScaleLabelText=new TText(.1,.9,"Total Q (pC)");
  ScaleLabelText->SetTextSize(.15);
  ScaleLabelText->Draw();
  double x[5]={.2,.995,.995,.2,.2};
  for(int iBox=0;iBox<10;iBox++)
    {
      double y[5]={.08*(iBox+1),.08*(iBox+1),.08*(iBox+2),.08*(iBox+2),.08*(iBox+1)};
      
      TPolyLine *TempBox=new TPolyLine(5,x,y);
      TempBox->SetFillColor(ColI+iBox*numCols/9);
      TempBox->Draw("f");
      TempBox->Draw();
      char valuebuff[25];
      sprintf(valuebuff,"%3i",iBox*numCols/9);
      TText *valueText=new TText(.025,.08*(iBox+1.5),(const char*)valuebuff);
      valueText->SetTextSize(.1);
      valueText->Draw();
      
    }

  colorScaleCanvas->Update();
  colorScaleCanvas->Modified();

}
void CLEANViewer::DrawEventDisplay3D()
{

  int id=-1;
  if(highLightPMTButton && highLightPMTButton->IsDown()) id=selectPMT;
  displayTCanvas->cd();
 
  if(Draw3D and calibrated)
    {
      fprintf(stderr,"calibrated\n");
      if(man) 
	{
	  man->ClearPhysicalNodes(true);
	  delete man;
	  man=NULL;
	  }
      /*      if(v)
	{
	  v->Close();
	  }*/

     
      man=new TGeoManager();
      TGeoVolume * World=man->MakeBox("world",NULL,400,400,400);
      man->SetTopVolume(World);
      
      

      
      RAT::MCcassetteInfo* CI=ratDC->GetMCcassetteInfo();
      vector<int> ids= ratDC->GetPMTIDs();


  
      double Red[5]={0,0,0.87,1,0.51};
      double Green[5]={0,0.81,1,0.2,0};
      double Blue[5]={0.51,1,0.12,0,0};
      double Stops[5]={0,.34,.61,0.84,1};
      int colMax=100;
      if(ColI<0)
	{
	  ColI=TColor::CreateGradientColorTable(5,Stops,Red,Green,Blue,colMax+1);
	}
      
    
      if(ratEV->ExistCentroid())
	{
	  TGeoVolume * Sphere=man->MakeSphere("Centroid",NULL,0,5,0);
	  const TVector3 pos=ratEV->GetCentroid()->GetPosition();
	  TGeoTranslation *transPos=new TGeoTranslation(pos.X(),pos.Y(),pos.Z());
	  Sphere->SetLineColor(0);
	  World->AddNode(Sphere,0,transPos);
	  Sphere->SetUniqueID(10000);

	}
      if(ratEV->ExistLikeFit())
	{
	  TGeoVolume * Sphere=man->MakeSphere("LikeFit",NULL,0,5,0);
	  const TVector3 pos=ratEV->GetLikeFit()->GetPosition();
	  TGeoTranslation *transPos=new TGeoTranslation(pos.X(),pos.Y(),pos.Z());
	  Sphere->SetLineColor(0);
	  Sphere->SetUniqueID(10001);
	  World->AddNode(Sphere,0,transPos);

	}
      
      if(ratEV->ExistShellFit())
	{
	  TGeoVolume * Sphere=man->MakeSphere("ShellFit",NULL,0,5,0);
	  const TVector3 pos=ratEV->GetShellFit()->GetPosition();
	 
	  TGeoTranslation *transPos=new TGeoTranslation(pos.X(),pos.Y(),pos.Z());
	  Sphere->SetLineColor(0);
	  Sphere->SetUniqueID(10002);
	  World->AddNode(Sphere,0,transPos);

	}
      if(ratEV->ExistMBLikelihood())
	{
	  TGeoVolume * Sphere=man->MakeSphere("MBLikelihood",NULL,0,5,0);
	  const TVector3 pos=ratEV->GetMBLikelihood()->GetPosition();
	  TGeoTranslation *transPos=new TGeoTranslation(pos.X(),pos.Y(),pos.Z());
	  Sphere->SetLineColor(0);
	  Sphere->SetUniqueID(10003);
	  World->AddNode(Sphere,0,transPos);
	  
	}

      for(size_t i=0;i<ids.size();i++)
	{
	  
	  int pmtID=ids[i];
	  int ipmt=i;

	  if(ratEV->GetPMT(ipmt)->GetID()!=pmtID)
	    {
	      ipmt=-1;
	      for(int j=0;j<ratEV->GetPMTCount();j++)
		{
		  if(ratEV->GetPMT(j)->GetID()==pmtID)
		    {
		      ipmt=j;
		      break;
		    }
		}

	    }

	  if (ipmt<0) return;
	 
	 

	  vector<TVector3> vertices=CI->GetCassetteVertices(pmtID);


	  TVector3 pos=CI->GetCassettePosition(pmtID);
	  
	  TRotation r=TRotation();
	  r.SetToIdentity();
	  r.RotateY(pos.Theta());
	  r.RotateZ(pos.Phi());
	  char name[100];
	  char nameIn[100];
	  sprintf(name,"Cassette %zu",i);
	  sprintf(nameIn,"CassetteIn %zu",i);
	  TGeoVolume * Cassette=man->MakeXtru(name,NULL,2);
	 
	  Cassette->SetUniqueID(ratEV->GetPMT(ipmt)->GetChannelID());
	  TGeoVolume * CassetteIn=man->MakeXtru(nameIn,NULL,2);
	  CassetteIn->SetUniqueID(1000+ratEV->GetPMT(ipmt)->GetChannelID());
	  TGeoXtru *xtru=(TGeoXtru*)Cassette->GetShape();
	  TGeoXtru *xtruIn=(TGeoXtru*)CassetteIn->GetShape();
	  Double_t x[vertices.size()];
	  Double_t y[vertices.size()];
	  Double_t xin[vertices.size()];
	  Double_t yin[vertices.size()];
	  TGeoTranslation trans=TGeoTranslation(pos.X(),pos.Y(),pos.Z());
	  float rad=pos.Mag();
	  float rSmall=rad-0.5;
	  TGeoTranslation transSmall=TGeoTranslation(pos.X()*rSmall/rad,pos.Y()*rSmall/rad,pos.Z()*rSmall/rad);
	  char nameRot[100];
	  sprintf(nameRot,"rotation%i",pmtID);
	  TGeoRotation rot=TGeoRotation("",0,0,0);
	  rot.RotateY(pos.Theta()*180/3.14);
	  rot.RotateZ(pos.Phi()*180/3.14);
	  
	  TGeoCombiTrans *fullTrans=new TGeoCombiTrans(trans,rot);
	  TGeoCombiTrans *fullTransSmall=new TGeoCombiTrans(transSmall,rot);

	  float maxR=0;
	  for(size_t j=0;j<vertices.size();j++)
	    {
	      vertices[j].Transform(r.Inverse());
	      x[j]=vertices[j].X();
	      y[j]=vertices[j].Y();
	      TVector3 v(x[j],y[j],0);
	      if(v.Mag()>maxR) maxR=v.Mag();
	    }

	  
	  if(ratEV->GetPMT(ipmt)->GetChannelID()==id)
	    {
	     
	      TGeoVolume * marker=man->MakeTube("markerTube",NULL,maxR+.1,maxR+5,.5);
	      marker->SetLineColor(1);
	      marker->SetUniqueID(15001);
	      World->AddNode(marker,1000,fullTrans);
	    }


	  float weightSize=1;
	  switch(sizeScaleState)
	    {
	    case 0:
	      weightSize=1;
	      break;
	    case 1:
	      weightSize=ratEV->GetPMT(ipmt)->GetTotalQ()/sizeScaleMax;
	      break;
	    case 2:
	      weightSize=ratEV->GetPMT(ipmt)->GetPromptQ()/sizeScaleMax;
	      break;
	    }
	  if(weightSize>1.0)
	    {
	      weightSize=1;
	    }

	  if(weightSize==0 or weightSize<0)
	    {
	      continue;
	    }
	   for(size_t j=0;j<vertices.size();j++)
	    {
	      x[j]=weightSize*x[j];
	      y[j]=weightSize*y[j];
	    }
	   
	   for(size_t iv=0;iv<vertices.size();iv++)
	     {
	       xin[iv]=x[iv]*.8;
	       yin[iv]=y[iv]*.8;
	     }
	   xtru->DefinePolygon(vertices.size(),x,y);
	   xtru->DefineSection(0,-.5,0,0,1);
	  // xtru->DefineSection(1,5,0,0,.6);
	  xtru->DefineSection(1,0.5,0,0,1);

	  xtruIn->DefinePolygon(vertices.size(),xin,yin);
	   xtruIn->DefineSection(0,-.5,0,0,1);
	  // xtru->DefineSection(1,5,0,0,.6);
	  xtruIn->DefineSection(1,0.5,0,0,1);

	  // int col=ColI+int(ratEV->GetPMT(i)->GetPromptQ()*100.0/ratEV->GetPMT(i)->GetTotalQ());
	  float weightCol=1;
	  float time=-1;
	  if(colorScaleState==2 && ratEV->GetReductionLevel()==ReductionLevel::CHAN_ZLE_WAVEFORM)
	    {
	      if(ratEV->GetPMT(ipmt)->GetBlockCount()==0)
		{
		  time=0;
		}
	      else
		{
		  int iWaveform=i;
		  if(WaveformVector[i].GetID()!=ratEV->GetPMT(ipmt)->GetChannelID())
		    {
		      iWaveform=-1;
		      
		      for(size_t j=0;j<WaveformVector.size();j++)
			{
			  
			  if(WaveformVector[j].GetID()==ratEV->GetPMT(ipmt)->GetChannelID())
			    {
			      iWaveform=j;
			      break;
			    }
			}
		      
		    }

		  if(iWaveform<0) return;

		  float SumV=0;
		  float SumW=0;

		  for(size_t k=0;k<WaveformVector[iWaveform].GetWindows()[0].QCalib.size();k++)
		    {
		      SumV+=WaveformVector[iWaveform].GetWindows()[0].QCalib[k];
		      SumW+=WaveformVector[iWaveform].GetWindows()[0].QCalib[k]*float(WaveformVector[iWaveform].GetMinSample(true)+4.0*k);
		      

		    }
		  time=SumW/SumV;
		}
	    }

	  

	  switch(colorScaleState)
	    {
	    case 0:
	      weightCol=1;
	      break;
	    case 1:
	      weightCol=ratEV->GetPMT(ipmt)->GetTotalQ()/colorScaleMax;
	      break;
	    case 2:
	      weightCol=time/colorScaleMax;
	    }
	  if(weightCol>1.0 or weightCol<0)
	    {
	      weightCol=1;
	    }
	  int col=ColI+int(weightCol*100);
	  Cassette->SetLineColor(col);
	  Cassette->SetLineWidth(3);

	  CassetteIn->SetLineColor(0);
	
	  World->AddNode(Cassette,pmtID,fullTrans);
	  World->AddNode(CassetteIn,pmtID,fullTransSmall);
	  
	}
      //Make sizer spheres
      TGeoTranslation * transSphere;
      for(int i=0;i<6;i++)
	{
	  char nameSphere[100];
	  sprintf(nameSphere,"sizer%i",i);
	  TGeoVolume * Sphere=man->MakeSphere(nameSphere,NULL,0,.01);
	  float x=0;
	  float y=0;
	  float z=0;
	  switch(i)
	    {
	    case 0:
	      x=475;
	      break;
	    case 1:
	      x=-475;
	      break;
	    case 2:
	      y=475;
	      break;
	    case 3:
	      y=-475;
	      break;
	    case 4:
	      z=475;
	      break;
	    case 5:
	      z=-475;
	      break;
	    }
	  
	  transSphere=new TGeoTranslation(x,y,z);
	  Sphere->SetUniqueID(90001);
	  World->AddNode(Sphere,i,transSphere);

	}
      if(!v)
	{

	  World->Draw("ogl"); 
	  

	  //DrawColorScale(colMax);
      
	  v=(TGLSAViewer*)(displayTCanvas->GetViewer3D("ogl"));//new TGLViewer(displayTCanvas,0,0,600,800);
	  v->SetClearColor(1);
	  v->SetGuideState(2,kTRUE,kFALSE,NULL);
	  v->SetResetCamerasOnUpdate(kFALSE);
	  v->Connect("Destroyed()","CLEANViewer",this,"Viewer3DClosed()");
	  v->Connect("MouseOver(TObject*,UInt_t)","CLEANViewer",this,"MouseOver3DViewer(TObject*,UInt_t)");
	  v->GetGedEditor()->CloseWindow();
	  v->GetLeftVerticalFrame()->RemoveFrame(v->GetGedEditor());
	  CreateInfoWindow();
	  if(eventGroup3D)
	    {
	      //	      v->GetLeftVerticalFrame()->Resize(195,v->GetLeftVerticalFrame()->GetHeight()-Frames3D->GetHeight());
	  //v->GetLeftVerticalFrame()->Resize(195,v->GetLeftVerticalFrame()->GetHeight());
	    }
	}
      else
	{

	  World->Draw("sameogl");
	  //DrawColorScale(colMax);
	  CreateInfoWindow();
	  v->UpdateScene();

	}



    }

}

/*void CLEANViewer::Draw3D()
{

  TEveManager::Create();

  ifstream file;
  file.open("mc_cassette_vertices.txt");
  //  TRandom3* rndm = new TRandom3();
  std::vector<TVector3> vec;
  const int maxpe = 10;
  int count = 0;
  vector<int> hits;
  vector<vector<TVector3> > vertices;
  vector<TVector3> centers;

  for(string line; getline(file, line); ){
    if(line.size() == 0 && vec.size() > 0){
      stringstream ss;
      ss << count;
      count ++;
      TEveTriangleSet* tri = 
	new TEveTriangleSet(vec.size()+1, vec.size(), kFALSE, kFALSE);
      tri->SetElementName(("PMT" + ss.str()).c_str());
      TVector3 center(0.0, 0.0, 0.0);
      vector<TVector2*> proj;
      for(unsigned i=0; i<vec.size(); i++)
	center += vec[i];
      center.SetMag(center.Mag() / vec.size());
      tri->SetVertex(0, center.X(), center.Y(), center.Z());
      tri->SetVertex(1, vec[0].X(), vec[0].Y(), vec[0].Z());
      for(unsigned i=1; i<vec.size(); i++){
	tri->SetVertex(i+1, vec[i].X(), vec[i].Y(), vec[i].Z());
	tri->SetTriangle(i, 0, i, i+1);
      }
      tri->SetTriangle(0, 0, vec.size(), 1);
      //      int npe = (int) (maxpe / 2 * (1 + cos(center.Theta())));
      //npe += (int) (pow(-1, (int) (gRandom->Rndm() * 2)) * 
      //		    gRandom->Poisson(0.5 * sqrt(npe)));
      tri->SetMainColor(ColI + col[count-1]);//TMath::Min(50 + (npe * 50 / maxpe), 100));
      tri->SetPickable(kTRUE);
      if(npe > 0)
	gEve->AddElement(tri);
      hits.push_back(TMath::Max(0, npe));
      vec.push_back(vec.front());
      vertices.push_back(vec);
      centers.push_back(center);
      vec.clear();
    }
    else{
      istringstream iss(line);
      double x, y, z;
      iss >> x >> y >> z;
      if(x == 0.0 && y == 0.0 && z == 0.0)
	continue;
      vec.push_back(TVector3(x, y, z));
    }
  }




}
*/


void CLEANViewer::DrawMiniWaveform(int pmtID)
{

  if(ratEV->GetReductionLevel()==ReductionLevel::CHAN_ZLE_WAVEFORM)
    {
      InfoPlotPad->cd();
      InfoPlotPad->Clear();

      double MaxQCalib=1;
      double MinQCalib=0;
      int canvasMinX=9999999;
      int canvasMaxX=-9999999;
  
  
      for(UInt_t iWaveform=0;iWaveform<WaveformVector.size();iWaveform++)
	{
	  /*	if((commonScale)&(WaveformVector[iWaveform].GetID()!=-100))
		{
		if(canvasMinX>int(WaveformVector[iWaveform].GetMinSample(true)))
		canvasMinX=int(WaveformVector[iWaveform].GetMinSample(true));
		if(canvasMaxX<int(WaveformVector[iWaveform].GetMaxSample(true)))
		canvasMaxX=int(WaveformVector[iWaveform].GetMaxSample(true));
		if(canvasMinY>int(WaveformVector[iWaveform].GetMinADC()))
		canvasMinY=int(WaveformVector[iWaveform].GetMinADC());
		if(canvasMaxY<int(WaveformVector[iWaveform].GetMaxADC()))
		canvasMaxY=int(WaveformVector[iWaveform].GetMaxADC());
		}*/
	  if(WaveformVector[iWaveform].GetID()==pmtID)
	    {
	      canvasMinX=int(WaveformVector[iWaveform].GetMinSample(true));
	      canvasMaxX=int(WaveformVector[iWaveform].GetMaxSample(true));
	      MaxQCalib=WaveformVector[iWaveform].GetMaxQCalib();
	      MinQCalib=WaveformVector[iWaveform].GetMinQCalib();
	    }
	}


      for(UInt_t iWaveform=0;iWaveform<WaveformVector.size();iWaveform++)
	{

	  if(WaveformVector[iWaveform].GetID()==pmtID)
	    {


	      WaveformVector[iWaveform].TransformCalib(canvasMinX,canvasMaxX,MinQCalib,MaxQCalib,1,0);
	      for(UInt_t iZLE=0;iZLE<WaveformVector[iWaveform].GetCalibTransform().size();iZLE++)
		{
		  vector<double> TempX=WaveformVector[iWaveform].GetCalibTransform()[iZLE].CalibSamples;
		  vector<double> TempY=WaveformVector[iWaveform].GetCalibTransform()[iZLE].QCalib;
		  TPolyLine* TempMiniLine=new TPolyLine();
		  for(UInt_t iSample=0;iSample<TempX.size();iSample++)
		    {
		      TempMiniLine->SetNextPoint(.8*((TempX[iSample]-.15)/.8)+.1,.85*(TempY[iSample]-.1)/.8+.1);
		 
		  
		  
		    }
		  TempMiniLine->Draw();
	      
		}
	    }
      
	  
	
	} 

      gPad->RecursiveRemove(miniyAxis);
      gPad->RecursiveRemove(minixAxis);
      gPad->RecursiveRemove(miniLateAxis);
      gPad->RecursiveRemove(miniPromptAxis);
      miniyAxis = new TGaxis(0.1,.1 ,
			     0.1, .95,
			     MinQCalib, 
			     MaxQCalib, 
			     4, "NI");
      miniyAxis->SetLabelSize(.04);
      miniyAxis->SetTitle("mV");
      miniyAxis->Draw();
      
      minixAxis = new TGaxis(.1,.1,.9,.1,canvasMinX,canvasMaxX,404,"NI");
      minixAxis->SetLabelSize(.04);
      minixAxis->SetTitle("ns");
      //      minixAxis->SetLabelOffset(-.01);
      minixAxis->SetTitleOffset(.5);
  
      minixAxis->Draw();
    }
  else if(ratEV->GetReductionLevel()==ReductionLevel::CHAN_PROMPT_TOTAL)
    {

      InfoPlotPad->cd();
      InfoPlotPad->Clear();
      if(miniPromptHist) delete miniPromptHist;
      miniPromptHist=new TH1F;
      if(miniLateHist) delete miniLateHist;
      miniLateHist=new TH1F;

      char iChannelStr[100];
      sprintf(iChannelStr,"%i",pmtID);
      miniPromptHist->Fill(iChannelStr,-1*(PromptCalibHist->GetBinContent(ChannelIDOrderMap[pmtID])));
      miniLateHist->Fill(iChannelStr,LateCalibHist->GetBinContent(ChannelIDOrderMap[pmtID]));
      miniLateHist->GetXaxis()->SetBinLabel(1,iChannelStr);
      miniPromptHist->GetXaxis()->SetBinLabel(1,iChannelStr);
      miniPromptHist->GetYaxis()->SetLabelSize(0);

      miniPromptHist->SetFillColor(2);
      miniLateHist->SetFillColor(1);
      miniPromptHist->SetTitle("");
      miniLateHist->SetTitle("");

      //      miniLateHist->GetXaxis()->SetRange(1,2);
      //miniPromptHist->GetXaxis()->SetRange(1,2);
      double max=-1*(miniPromptHist->GetMinimum());
      if(miniLateHist->GetMaximum()>max) max=miniLateHist->GetMaximum();
      
      miniPromptHist->SetMaximum(max);
      miniPromptHist->SetMinimum(-1*max);
      miniLateHist->SetMaximum(max);
      miniLateHist->SetMinimum(-1*max);
      miniPromptHist->SetStats(kFALSE);
      miniLateHist->SetStats(kFALSE);
      miniPromptHist->Draw("hbar");
      miniLateHist->Draw("hbarAsame");

      gPad->RecursiveRemove(miniPromptAxis);
      gPad->RecursiveRemove(miniLateAxis);
      if(miniPromptAxis) delete miniPromptAxis;
      if(miniLateAxis) delete miniLateAxis;
      
      miniPromptAxis=new TGaxis(0,gPad->GetUymin(),-max,gPad->GetUymin(),0,max,502,"-");
      miniPromptAxis->SetLabelOffset(-0.019);
      
      miniPromptAxis->SetTitle("Prompt Charge (pC)");
      miniLateAxis=new TGaxis(0,gPad->GetUymin(),max,gPad->GetUymin(),0,max,502);
      miniLateAxis->SetTitle("Late Charge (pC)");
      miniLateAxis->Draw();
      miniPromptAxis->Draw();
     
    }

}


