#include <DecisionMaker.h>

void DecisionMaker::MainLoop()
{
  if(IsReadReady(DRPC_mem->GetUnProcessedFDs()[0]))
    {
      //Get the next unprocessed event. 
      int32_t index;
      if(DRPC_mem->GetUnProcessed(index))
	//got the next unprocessed
	{
	  //Check that the index returned doesn't give an error
	  if(index != DRPC_mem->GetBadValue())
	    {
	      //Get the DRPCBlock for this event
	      DRPCBlock * event = DRPC_mem->GetDRPCBlock(index);
	      //Pass the block to the ComputeDecision function
	      ComputeDecision(event);	  	  
	      //Add decision to our reduction level count	      
	      reductionLevelCount[event->reductionLevel]++;
	      //Send event on to the respond queue
	      if(!DRPC_mem->AddRespond(index))
		{
		  //We are probably shutting down in this case
		  PrintError("Can't add to respond. DCDAQ shutdown?\n");
		  SendStop();
		}
	    }
	  else
	    {
	      PrintError("Bad event number\n");
	      //We are probably shutting down in this case
	    }
	}
      else
	//We couldn't get the next unprocessed
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a UnProcessed event.  DCDAQ shutdown?\n");
	  SendStop();
	}
    }
}

bool DecisionMaker::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      Loop = false;
      //We should no longer pay attention to the UnProcessed queue
      RemoveReadFD(DRPC_mem->GetUnProcessedFDs()[0]);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      Loop = true;
      //We now need to pay attention to the UnProcessed queue
      AddReadFD(DRPC_mem->GetUnProcessedFDs()[0]);
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}
