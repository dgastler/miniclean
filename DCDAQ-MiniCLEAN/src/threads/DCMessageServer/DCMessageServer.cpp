#include <DCMessageServer.h>

DCMessageServer::DCMessageServer(std::string Name)
{
  SetType("DCMessageServer");
  SetName(Name);
 
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
}



void DCMessageServer::ThreadCleanUp()
{
  //Close our server listening socket
  //Close all of our connections to remote DCDAQs 
  server.Clear();
}

void DCMessageServer::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.GetSocketFD()))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < server.Size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(server[iConnection].GetFullPacketFD()))
	{
	  RouteRemoteDCMessage(iConnection);
	}
      //Check for a new message
      if(IsReadReady(server[iConnection].GetMessageFD()))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

bool DCMessageServer::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Determine if this is local or not
  if(!message.GetDestination().IsDefaultAddr())
    {
      RouteLocalDCMessage(message);
    }
  else if(message.GetType() == DCMessage::STOP)
    {
      //Stop all the network connections before we close
      server.Clear();
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return ret;
}

//Parse and route a remote message
void DCMessageServer::RouteRemoteDCMessage(size_t iConn)
{  
  //create a new DCPacket using the data from the packets
  DCMessage::Message  message = ReadRemoteDCMessage(iConn);
 
  //Get the destination address of the DCMessage
  DCMessage::DCAddress messageAddr = message.GetDestination();

  if(server.RouteToLocal(messageAddr))
    {
      //Strip remote address since we not longer need it.
      message.SetDestination(messageAddr.GetName());
      //The Client will forward broadcast messages to this session  
      ForwardMessageOut(message);
    }
}

//Message is coming from this process and out to the network
bool DCMessageServer::RouteLocalDCMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Get destination of this packet
  DCMessage::DCAddress destAddr = message.GetDestination();
  
  //Get the message's source
  DCMessage::DCAddress sourceAddr = message.GetSource();

  //Send to all. 
  if(destAddr.IsBroadcastAddr())
    {
      //Loop over all the connections
      for(unsigned int i = 0; i < server.Size();i++)
	{	  
	  //Don't send it back to the source
	  if(!server[i].RouteToRemote(sourceAddr))
	    {
	      DCMessage::Message tempMessage = message;	
	      //If this broadcast originated here, update the source addr to here
	      if(sourceAddr.IsDefaultAddr())
		{
		  DCMessage::DCAddress tempAddr;
		  tempAddr.Set(sourceAddr.GetName(),
			       server[i].GetLocalAddress().GetAddr(),
			       server[i].GetLocalAddress().GetAddrSize());
		  tempMessage.SetSource(tempAddr);
		}
	      ret &= SendMessage(i,tempMessage);
	    }
	}      
    }
  else
    {
      for(unsigned int i = 0; i < server.Size();i++)
	{
	  if(server[i].RouteToRemote(destAddr))
	    {
	      DCMessage::Message tempMessage = message;	
	      //If this broadcast originated here, update the source addr to here
	      if(sourceAddr.IsDefaultAddr())
		{
		  DCMessage::DCAddress tempAddr;
		  tempAddr.Set(sourceAddr.GetName(),
			       server[i].GetLocalAddress().GetAddr(),
			       server[i].GetLocalAddress().GetAddrSize());
		  tempMessage.SetSource(tempAddr);
		}
	      ret &= SendMessage(i,tempMessage);
	    }
	}
    }
  return ret;
}


void DCMessageServer::ProcessTimeout()
{
}

void DCMessageServer::PrintStatus()
{
  DCThread::PrintStatus();
  for(size_t i = 0; i <server.Size();i++)
    {
      server[i].PrintStatus();
    }
}
