--This module streams a UDP response packet out--including the payload information provided by
--sub_transactor.vhd, whatever addresses need to be swapped in the header, a couple of IP 
--checksums (calculated by ip_checksum_32bit.vhd), and other miscellaneous data fields. The 
--formatting of UDP packets is notably more involved than the creation of ARP and Ping responses.
--Original code by Jeremy Mans.
--Completely redesigned by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sub_packetresp is Port(
	CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	SEND_CKE : in STD_LOGIC;
	TX_REQ : out STD_LOGIC;
	TX_SEND : out STD_LOGIC;
	DONE : in STD_LOGIC_VECTOR(1 downto 0);
	TX_OK : in STD_LOGIC;
	CHECKSUM_OK : in STD_LOGIC_VECTOR(4 downto 0);
	TXD : out unsigned(7 downto 0);
	LEN : in unsigned(10 downto 0);
	RESPDATA : in unsigned(31 downto 0);
	RESPADDR : in unsigned(8 downto 0);
	RESPWE : in STD_LOGIC);
end sub_packetresp;

architecture Behavioral of sub_packetresp is
	signal write_half : STD_LOGIC := '0';
	signal read_half : STD_LOGIC := '1';
	signal was_fragmented : STD_LOGIC := '0';
	signal rplen : unsigned(10 downto 0);
	signal rdAddr : unsigned(10 downto 0);
	signal rdData : unsigned(7 downto 0);
	signal checksum_ip : unsigned(15 downto 0);
	signal checksum_udp : unsigned(15 downto 0);
	signal checksum_storage : unsigned(31 downto 0);
	signal copy_source_next : unsigned(10 downto 0);
	signal txaNext : unsigned(10 downto 0);
	signal txdForAddr : unsigned(7 downto 0);
	signal udp_csum_input : unsigned(7 downto 0);
	signal iureset : STD_LOGIC_VECTOR(1 downto 0);
	signal atx : unsigned(10 downto 0);
	type state_type is (st_idle, st_wait, st_wait2, st_send);
	signal state : state_type := st_idle;

	component ip_checksum_32bit port(
		CLK : in STD_LOGIC;
		VALID : in STD_LOGIC_VECTOR(2 downto 0);
		RESET : in STD_LOGIC_VECTOR(1 downto 0);
		CHECKSUM : out unsigned(15 downto 0);
		DATA : in unsigned(31 downto 0));
	end component;

	component dpbr2 port(
		RESET : in STD_LOGIC;
		CLKA : in STD_LOGIC;
		WEA : in STD_LOGIC;
		ADDRA : in STD_LOGIC_VECTOR(9 downto 0);
		DINA : in STD_LOGIC_VECTOR(31 downto 0);
		CLKB : in STD_LOGIC;
		CLKEN : in STD_LOGIC;
		ADDRB : in STD_LOGIC_VECTOR(11 downto 0);
		DOUTB : out STD_LOGIC_VECTOR(7 downto 0));
	end component;
begin
	inconvenient_buffer : dpbr2 port map(
		RESET => RESET,
		CLKA => CLK,
		WEA => RESPWE,
		ADDRA(9) => write_half,
		ADDRA(8 downto 0) => STD_LOGIC_VECTOR(RESPADDR),
		DINA => STD_LOGIC_VECTOR(RESPDATA),
		CLKB => CLK,
		CLKEN => SEND_CKE,
		ADDRB(11) => read_half,
		ADDRB(10 downto 0) => STD_LOGIC_VECTOR(rdAddr),
		unsigned(DOUTB) => rdData);

	ip_csum : ip_checksum_32bit port map(
		CLK => CLK,
		VALID(2 downto 1) => CHECKSUM_OK(4 downto 3),
		VALID(0) => CHECKSUM_OK(0),
		RESET => iureset,
		CHECKSUM => checksum_ip,
		--data sent out earliest (closest to beginning of packet) used as
		--more significant bytes for the purposes of checksum arithmetic
		DATA(31 downto 24) => RESPDATA(7 downto 0),
		DATA(23 downto 16) => RESPDATA(15 downto 8),
		DATA(15 downto 8) => RESPDATA(23 downto 16),
		DATA(7 downto 0) => RESPDATA(31 downto 24));

	udp_csum : ip_checksum_32bit port map(
		CLK => CLK,
		VALID(2 downto 1) => CHECKSUM_OK(2 downto 1),
		VALID(0) => CHECKSUM_OK(0),
		RESET => iureset,
		CHECKSUM => checksum_udp,
		DATA(31 downto 24) => RESPDATA(7 downto 0),
		DATA(23 downto 16) => RESPDATA(15 downto 8),
		DATA(15 downto 8) => RESPDATA(23 downto 16),
		DATA(7 downto 0) => RESPDATA(31 downto 24));

	TX_REQ <= '0' when state = st_idle else '1';
	copy_source_next <= "000" & x"02" when rdAddr = "000" & x"0D" else
		"000" & x"0E" when rdAddr = "000" & x"07" else rdAddr + ("000" & x"01");
	txaNext <= atx + ("000" & x"01");

	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			if RESET = '1' then
				TX_SEND <= '0';
				write_half <= '0';
				read_half <= '1';
				iureset <= "11";
				state <= st_idle;
			else
				case state is 
				when st_idle =>
					if DONE(0) = '1' then
						was_fragmented <= DONE(1);
						if was_fragmented = '0' then--change the reading location at the onset
							read_half <= not read_half;--of a fragmented datagram
						end if;
						if DONE(1) = '0' then--change the writing location at the end
							write_half <= not write_half;--of a fragmented datagram
							iureset <= "11";
						else
							iureset <= "01";
						end if;
						--must swap Ethernet addresses
						rdAddr <= "000" & x"08";
						atx <= "000" & x"00";
						checksum_storage <= checksum_ip & checksum_udp;
						TX_SEND <= '0';
						rplen <= LEN;
						state <= st_wait;
					else
						TX_SEND <= '0';
						iureset <= "00";
						state <= st_idle;
					end if;
				when st_wait =>--states st_wait and st_wait2 prepare the pipelined output streaming...
					TX_SEND <= '0';
					iureset <= "00";
					if TX_OK = '1' and SEND_CKE = '1' then
						rdAddr <= copy_source_next;--...by advancing rdAddr ahead of TXD and atx
						state <= st_wait2;
					else
						state <= st_wait;
					end if;
				when st_wait2 =>
					iureset <= "00";
					if SEND_CKE = '1' then
						TX_SEND <= '1';
						TXD <= rdData;
						rdAddr <= copy_source_next;
						state <= st_send;
					else
						TX_SEND <= '0';
						state <= st_wait2;
					end if;
 				when st_send =>
					iureset <= "00";
 					if SEND_CKE = '0' then
						TX_SEND <= '1';
						state <= st_send;
					elsif txaNext = rplen then
 						TX_SEND <= '0';
 						state <= st_idle;
 					else
 						TX_SEND <= '1';
 						state <= st_send;
 						atx <= txaNext;
						if txaNext = x"18" then
							TXD <= checksum_storage(31 downto 24);
						elsif txaNext = x"19" then
							TXD <= checksum_storage(23 downto 16);
						elsif txaNext = x"28" then
							TXD <= checksum_storage(15 downto 8);
						elsif txaNext = x"29" then
							TXD <= checksum_storage(7 downto 0);
						else
							TXD <= rdData;
						end if;
 						rdAddr <= copy_source_next;
 					end if;
				end case;
			end if;
		end if;
	end process;
end Behavioral;
