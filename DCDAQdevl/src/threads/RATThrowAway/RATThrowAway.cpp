#include <RATThrowAway.h>

RATThrowAway::RATThrowAway(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATThrowAway"));
   
  EventNumber = 1; //Starting event number
  
  dsIndex = FreeFullQueue::BADVALUE;

  lastTime = time(NULL);
  currentTime = time(NULL);
  
  //Events
  TimeStep_Events = 0;
}

RATThrowAway::~RATThrowAway()
{
}


bool RATThrowAway::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager)
{
  //Check to see if there is a memorymanger node in the setup xml
  if(FindSubNode(SetupNode,"MEMORYMANAGER") == NULL)
    {
      PrintError("No memory manger node found");
      return(false);
    }

  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return  false;
    }
  
  if(Mem == NULL)
    {
      PrintError("Bad memory manager type");
      return(false);
    }
  //Set thread state to go and change it to fail if anything goes wrong.
  Ready = true;
  return(true);
}







bool RATThrowAway::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the RAT memory manager to shut down.
      Mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      RemoveReadFD(Mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      
      //Register the RAT DS block's free queue's FDs with select
      AddReadFD(Mem->GetFullFDs()[0]);
      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}
