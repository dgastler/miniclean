#ifndef __WFDBLOCKMANAGER__
#define __WFDBLOCKMANAGER__

//This memory manager stores arrays of RAT::EV classes to store
//parsed WFD data.   Each EV will store the data from ONE wfd.

#include <MemoryManager.h>
#include <FreeFullQueue.h>
#include <xmlHelper.h>
#include <WFDBlock.h>

#include "RAT/DS/EV.hh"

class WFDBlockManager : public MemoryManager , public FreeFullQueue
{
 public:
  WFDBlockManager(std::string Name);
  ~WFDBlockManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  WFDBlock * GetWFDBlock(uint32_t iWFD) 
  {
    if(iWFD < Memory.size())
      {
	return(&(Memory[iWFD]));
      }
    return(NULL);
  }
  virtual void Shutdown(){ShutdownQueues();};
  virtual void WakeUp(){WakeUpQueues();};
  virtual void PrintStatus(int indent =0);
 private: 
  std::vector<WFDBlock> Memory;
  void Deallocate();
};

#endif
