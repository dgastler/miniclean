#include <SubID.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[SubID::COUNT] = 
  {
    "BLANK",
    "WFD_01",
    "WFD_02",
    "WFD_03",
    "WFD_04",
    "WFD_05",
    "WFD_06",
    "WFD_07",
    "WFD_08",
    "WFD_09",
    "WFD_10",
    "WFD_11",
    "WFD_12",
    "WFD_13",
    "WFD_14",
    "WFD_15",
    "NET_00",
    "NET_01",
    "NET_02",
    "NET_03",
    "NET_04",
    "NET_05",
    "NET_06",
    "PMT_00",
    "PMT_01",
    "PMT_02",
    "PMT_03",
    "PMT_04",
    "PMT_05",
    "PMT_06",
    "PMT_07",
    "PMT_08",
    "PMT_09",
    "PMT_10",
    "PMT_11",
    "PMT_12",
    "PMT_13",
    "PMT_14",
    "PMT_15",
    "PMT_16",
    "PMT_17",
    "PMT_18",
    "PMT_19",
    "PMT_20",
    "PMT_21",
    "PMT_22",
    "PMT_23",
    "PMT_24",
    "PMT_25",
    "PMT_26",
    "PMT_27",
    "PMT_28",
    "PMT_29",
    "PMT_30",
    "PMT_31",
    "PMT_32",
    "PMT_33",
    "PMT_34",
    "PMT_35",
    "PMT_36",
    "PMT_37",
    "PMT_38",
    "PMT_39",
    "PMT_40",
    "PMT_41",
    "PMT_42",
    "PMT_43",
    "PMT_44",
    "PMT_45",
    "PMT_46",
    "PMT_47",
    "PMT_48",
    "PMT_49",
    "PMT_50",
    "PMT_51",
    "PMT_52",
    "PMT_53",
    "PMT_54",
    "PMT_55",
    "PMT_56",
    "PMT_57",
    "PMT_58",
    "PMT_59",
    "PMT_60",
    "PMT_61",
    "PMT_62",
    "PMT_63",
    "PMT_64",
    "PMT_65",
    "PMT_66",
    "PMT_67",
    "PMT_68",
    "PMT_69",
    "PMT_70",
    "PMT_71",
    "PMT_72",
    "PMT_73",
    "PMT_74",
    "PMT_75",
    "PMT_76",
    "PMT_77",
    "PMT_78",
    "PMT_79",
    "PMT_80",
    "PMT_81",
    "PMT_82",
    "PMT_83",
    "PMT_84",
    "PMT_85",
    "PMT_86",
    "PMT_87",
    "PMT_88",
    "PMT_89",
    "PMT_90",
    "PMT_91",
  };  

const char * SubID::GetTypeName(SubID::Type type)
{
  if(type < SubID::COUNT)
    return TypeName[type];
  return NULL;
} 

SubID::Type SubID::GetType(const char * name)
{
  std::string searchString(name);  
  SubID::Type ret = SubID::Type(0);
  for(SubID::Type type = SubID::Type(0);
      type < SubID::COUNT;
      type = SubID::Type(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
