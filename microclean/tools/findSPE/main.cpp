//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//Class includes
#include "PEvent.h"
#include "TextArray.h"
#include "xmlParser.h"

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TGraph.h"
#include "TLine.h"

using namespace std;

Float_t Volts = 0;
Float_t Seconds = 0;
Float_t C = 0;

//Find Single photo electron by sliding through projections of
//the SPE integral vs. SPE height 2d histogram. 
Float_t FindSPE(TH2F * h2,TH1D * &h1,Float_t &HeightCut,char * Name,TH1D * g1,Float_t Voltage)
{
  Int_t Range = 0;
//  if(Voltage < 1300)
//    Range = 10;
//  else
//    Range = 40;

//Lets fit an exponential 
  
  TH1D * YProjection = (TH1D*) h2->ProjectionY("YProjection");
  TF1 * Expo = new TF1("Expo",
		       "expo",
		       YProjection->GetYaxis()->GetXmin(),
		       YProjection->GetYaxis()->GetXmax());
  YProjection->Fit(Expo);
  Range = YProjection->GetXaxis()->FindBin(-1.0 * Expo->GetParameter(0)/Expo->GetParameter(1));
  
  Int_t N = Range;
  //  Int_t Step = 5;
  Float_t *x = new Float_t[N];
  Float_t *dy = new Float_t[N];
  Float_t *y = new Float_t[N];
  Float_t yMax = 0;
  Int_t xMax= 0;
  Float_t SPE = 0;

  

  x[0] = 0;
  dy[0] = 0;
  y[0] = 0;
  x[1] = 0;
  dy[1] = 0;
  y[1] = 0;

  g1->SetBinContent(1,0);
  //  g1->SetBinContent(1,0);
  
  // Watch how the maximum peak of the historam shifts.
  // Look for the first big jump in this value. 
  // that should be the SPE peak. 
  for(int i = 1; i < N; i++)
    {
      h1 = h2->ProjectionX(Name,i,h2->GetNbinsY());
      x[i] = h2->GetYaxis()->GetBinCenter(i);
      y[i] = h1->GetMaximumBin();
      if(i -1 == 0)
	dy[i] = 0;
      else
	dy[i] = (y[i] - y[i-1])/(i - 1);
      g1->SetBinContent(i+1,dy[i]);
      delete h1;
      if(yMax < dy[i])
	{
	  yMax = dy[i];
	  xMax = i;
	}
    }

  Float_t Multiple = 0.9;
  if(Voltage < 1300)
    Multiple = 0.5;

  for(int i = 2; i < N;i++)
    {
      if(dy[i] >= Multiple* yMax)
	{
	  xMax = i;
	  break;
	}
    }
  //  xMax++;
  

  h1 = h2->ProjectionX("h1",xMax,h2->GetNbinsY());
  HeightCut = h2->GetYaxis()->GetBinCenter(xMax);
  Int_t Cut0 = 0;
  Int_t Cut1 = 0;
  for(int i = h1->GetMaximumBin(); i >0 ;i--)
    {
      if(h1->GetBinContent(i) < 0.7*h1->GetMaximum())
	{
	  Cut0 = i;
	  break;
	}
    }
  for(int i = h1->GetMaximumBin(); i < h1->GetNbinsX();i++)
    {
      if(h1->GetBinContent(i) < 0.7*h1->GetMaximum())
	{
	  Cut1 = i;
	  break;
	}
    }
  if(fabs(h1->GetMaximumBin() - Cut0) > fabs(h1->GetMaximumBin() - Cut1))
    {
      Cut0 = 2*h1->GetMaximumBin() - Cut1;
    }
  else
    {
      Cut1 = 2*h1->GetMaximumBin() - Cut0;
    }
  //  TF1 * myfit = new TF1("myfit","gaus");
  TF1 * myfit = new TF1("myfit","pol4");
  h1->Fit(myfit,"F","",
	  h1->GetXaxis()->GetBinCenter(Cut0),
	  h1->GetXaxis()->GetBinCenter(Cut1));
  //  SPE = myfit->GetParameter(1);
  SPE = myfit->GetMaximumX(h1->GetXaxis()->GetBinCenter(Cut0),h1->GetXaxis()->GetBinCenter(Cut1));

  g1->Draw("APL");

  delete x;
  delete y;
  delete dy;
  delete myfit;

  return(SPE);
}



int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      cout << argv[0] << " inFile" << endl;
      return(0);
    }

  TFile *InFile = new TFile(argv[1],"UPDATE");
  TTree *TextTree = (TTree*) InFile->Get("TextTree");
  TextArray *RootText = 0;
  TextTree->SetBranchAddress("Run Notes",&RootText);
  TextTree->GetEntry(1);
  char * TempPointer = RootText->GetArray();

  XMLNode DAQNode=XMLNode::parseString(TempPointer,"DAQ");

  Int_t CurrentPMT = 0;
  int NameSize = 100;
  char * Name = new char[NameSize];
  char * Channel = new char[NameSize];
  Bool_t NotEnd = true;
  do
    {
      sprintf(Name,"PMT%dSPE",CurrentPMT);
      sprintf(Channel,"%d",CurrentPMT);
      Float_t Voltage = 0;
      for(Int_t WFD =0; WFD < DAQNode.getChildNode("Setup").nChildNode();WFD++)
	{
	  XMLNode WFDNode = DAQNode.getChildNode("Setup").getChildNode("WFD",WFD);
	  for(Int_t Channel = 0; Channel < WFDNode.nChildNode();Channel++)
	    {
	      XMLNode ChannelNode = WFDNode.getChildNode("Channel",Channel);
	      std::string Read(ChannelNode.getAttribute("PMTID"));
	      std::string Nothing("");
	      if((Read != Nothing) && (atoi(ChannelNode.getAttribute("PMTID")) == CurrentPMT))
		{
		  Voltage = atof(ChannelNode.getAttribute("PMTVoltage"));
		}
	    }
	}
      //IF this channel is in the File. 
      //If a pmt is missing then I guess this will stop at that PMT.
      printf("%s\n",Name);
      if(InFile->Get(Name))
	{
	  TH2F * SPE2D = (TH2F *) InFile->Get(Name);
	  sprintf(Name,"CUTPMT%d",CurrentPMT);
	  TH1D * SPECut = new TH1D(Name,Name,40,0,40);
	  TH1D * SPE = 0;
	  Float_t HeightCut = 0;
	  sprintf(Name,"PMT%dSPE_Fit",CurrentPMT);
	  Float_t Integral = FindSPE(SPE2D,SPE,HeightCut,Name,SPECut,Voltage);
	  TLine * HeightCutLine = new TLine(SPE2D->GetXaxis()->GetXmin(),HeightCut,SPE2D->GetXaxis()->GetXmax(),HeightCut);
	  HeightCutLine->SetLineColor(kRed);
	  SPE2D->GetListOfFunctions()->AddFirst(HeightCutLine);
	  sprintf(Name,"PMT%dSPE;1",CurrentPMT);
	  InFile->Delete(Name);
	  SPE2D->Write();
	  sprintf(Name,"PMT: %03d Voltage: %08fV SPE: %08EC Height Cut: %fV",CurrentPMT,Voltage,Integral,HeightCut);
	  printf("%s\n",Name);
	  SPE->SetTitle(Name);
	  sprintf(Name,"PMT%dSPE_Fit",CurrentPMT);
	  SPE->Write(Name);
	  SPECut->Write();
	  CurrentPMT++;
	}
      else
	{
	  NotEnd = false;
	}
      
    }while(NotEnd);
    
  delete [] Name;


  InFile->Close();
  
  return(0);
}
