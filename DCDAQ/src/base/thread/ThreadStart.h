#ifndef __THREADSTART__
#define __THREADSTART__

#include <pthread.h>
#include <DCThread.h>

void * start_thread(void * _pointer);
//Function to run thread.
//void * start_thread(void * pointer);

#endif
