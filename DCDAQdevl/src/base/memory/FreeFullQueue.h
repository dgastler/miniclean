#ifndef __FREEFULLQUEUE__
#define __FREEFULLQUEUE__

#include <DCQueue.h>
#include <MemoryManager.h>
class FreeFullQueue //: public MemoryManager
{
 public:  
  FreeFullQueue(){Setup();};
  void Setup(){Free.Setup(BADVALUE);Full.Setup(BADVALUE);};

  enum 
 {
    BADVALUE = -1
  };
  
  virtual ~FreeFullQueue(){Clear();};

  //clear queues
  void Clear(){Free.Clear(true);Full.Clear(true);};

  //Free/Full get functions.
  virtual bool GetFree(int32_t & index,bool wait = true)
  {    
    return(Free.Get(index,wait));
  };
  virtual bool GetFull(int32_t & index,bool wait = true)
  {
    return(Full.Get(index,wait));
  };
  //Free/Full add functions
  virtual bool AddFree(int32_t & Index){return(Free.Add(Index,true));};
  virtual bool AddFull(int32_t & Index){return(Full.Add(Index,true));};
  
  //FD access functions
  const int * GetFreeFDs(){return(Free.GetFDs());};  
  const int * GetFullFDs(){return(Full.GetFDs());};

  virtual void ShutdownQueues(){Free.ShutdownWait();Full.ShutdownWait();WakeUpQueues();};
  virtual void WakeUpQueues(){Free.WakeUp();Full.WakeUp();};

  virtual size_t FreeSize(){return(Free.GetSize());};
  virtual size_t FullSize(){return(Full.GetSize());};

 private:
  DCQueue<int32_t> Free;  //queue of indicies of free memory blocks
  DCQueue<int32_t> Full;  //queue of indicies of full memory blocks
};
#endif
