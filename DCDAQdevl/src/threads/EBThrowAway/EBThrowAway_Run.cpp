#include <EBThrowAway.h>

void EBThrowAway::MainLoop()
{
  if(IsReadReady(EBPC_mem->GetToDiskFDs()[0]))
    { 
      //Get the complete event's index
      int32_t index;
      if(EBPC_mem->GetToDisk(index))
	//We got an index from the 
	{
	  if(index != EBPC_mem->GetBadValue())
	    //A valid event
	    {
	      //	      BranchDS = new RAT::DS::Root;
	      
	      EBPCBlock * event = EBPC_mem->GetEBPCBlock(index);
	
	      //Copy existing EV to the DS structure for writing.
	      //	      *(BranchDS->AddNewEV())=*(event->ev);
	      //Now we need to clear the existing EBPCBLock
	      //event->Clear();//ev->PrunePMT();
	      event->ds->GetEV(0)->PrunePMT();

	      timeStepProcessedEvents++;  //Incriment the time step event count

	      //add index to the free queue
	      if(!EBPC_mem->AddFree(index))
		{
		  PrintError("Lost a EVBlock\n");
		}
	      	      	      
	      //Delete the Branch DS object.
	      //	      delete BranchDS;
	    }
	  else
	    {
	      //We are probably shutting down now. 
	      PrintError("Bad event index\n");
	      SendStop();
	    }
	}
      else
	//We couldn't get a new ToDisk
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a ToDisk event.  DCDAQ shutdown?\n");
	  SendStop();
	}
    }
}


bool EBThrowAway::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(EBPC_mem->GetToDiskFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the to disk queue with select
      AddReadFD(EBPC_mem->GetToDiskFDs()[0]);      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}


void EBThrowAway::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      float timeStep = float(difftime(currentTime,lastTime));
      DCMessage::Message message;
      DCMessage::TimedCount Count;
      sprintf(Count.text,"Events read");
      Count.count = timeStepProcessedEvents;
      Count.dt = timeStep;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      timeStepProcessedEvents = 0;

      //Setup next time
      lastTime = currentTime;
    }
}

