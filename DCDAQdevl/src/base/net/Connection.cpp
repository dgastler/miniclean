#include <Connection.h>
 
void Connection::Clear() 
{ 
  localIP  = "";
  remoteIP = "";
  port = 0;
  memset(&addrLocal,0,sizeof(struct sockaddr_in));
  memset(&addrRemote,0,sizeof(struct sockaddr_in));
  fdSocket = badFD;
  thread = NULL;
  retryCountDown=2;
  retry = false;
}

Connection::~Connection()
{
  //Shut down our netThread and close the socket
  Shutdown();
  //reset all values
  Clear();
}

void Connection::Shutdown()
{ 
  if(thread != NULL)
    {
      //Send stop message.
      DCMessage::Message message;
      message.SetType(DCMessage::STOP);
      thread->SendMessageIn(message,true);
      //cancel the thread
      //      pthread_cancel(thread->GetID());
      //Wait for thread to finish
      pthread_join(thread->GetID(),NULL);
      //delete the thread      
      delete thread;
    }
  if(fdSocket != badFD)
    {
      close(fdSocket);
    }

  //Clear connection values
  //Leaves IP/ports values intacted. 
  fdSocket = badFD;
  thread = NULL;
  retryCountDown=2;
  retry = false;
}

void Connection::SetRetry(int countdown)
{
  retryCountDown = countdown;
  retry = true;
}
bool Connection::Retry()
{
  if(retry)
    {
      //If our retry countdown has gotten to zero return true
      if(retryCountDown <= 0)
	{
	  retry = false;
	  return(true);
	}
      //If we are above zero decrimate countdown and return false;
      else
	retryCountDown--;
    }    
  return(false);
}

bool Connection::Setup(xmlNode * SetupNode)
{
  //================================
  //Parse data for Client config and expected clients.
  //================================


  //================================
  //Get Client port.
  //================================
  if(FindSubNode(SetupNode,"PORT") == NULL)
    {
      PrintError("No port specified?\n");
      return(false);
    }
  else
    {
      GetXMLValue(SetupNode,"PORT","Connection",port);
    }

  //================================
  //Get server address.
  //================================
  if(FindSubNode(SetupNode,"SERVER") == NULL)
    {
      PrintError("No server address!\n");
      return(false);
    }
  else
    {      
      //Get the address string
      GetXMLValue(SetupNode,
		  "SERVER",
		  "Connection",
		  remoteIP);
      
      //Move text to the sockaddr structure using inet_pton
      inet_pton(AF_INET,
		remoteIP.c_str(),
		&(addrRemote.sin_addr));
      addrRemote.sin_family = AF_INET;
      addrRemote.sin_port = htons(port);
    }
  return(true);
}


//Start a asyncronous socket connect
//true if connection works instantly. 
//false on error or connection in progress.
//if false and fd == -1, then you have a problem. 
//if false and fd >= 0, then you are waiting for the connection to finish
bool Connection::AsyncConnectStart(int type, int protocol)
{
  //Open a socket
  fdSocket = socket(AF_INET,type,protocol);
  if(fdSocket < 0)
    {
      return(false);
    }
  //Set socket to non-blocking
  if(!SetNonBlocking(fdSocket,true))
    {
      close(fdSocket);
      fdSocket = badFD;
      return(false);      
    }

  //fd is now non-blocking
  int connectReturn = connect(fdSocket,
			      (sockaddr *) &(addrRemote),
			      sizeof(addrRemote));
  //The connection happened in the blink of a transistor's eye.
  if(connectReturn == 0)
    {
      //Turn off non-blocking
      //(fail if it didn't work)
      if(!SetNonBlocking(fdSocket,false))
	{
	  close(fdSocket);
	  fdSocket = badFD;
	  return(false);      
	}

      //return that everything worked as well as possible
      return(true);
    }
  else if(errno == EINPROGRESS)
    {
      //We are waiting for the connection to finish
      //You should go wait in select for read/write access to fd!
      return(false);
    }
  
  //An error occured
  close(fdSocket);
  fdSocket = badFD;
  return(false);
}

//Check that our newly connected socket works. 
bool Connection::AsyncConnectCheck()
{
  //Check the socket option (SO_ERROR)
  int val_SO_ERROR;
  socklen_t val_SO_ERROR_Size = sizeof(val_SO_ERROR);
  int getSockOptReturn = getsockopt(fdSocket,SOL_SOCKET,SO_ERROR,
				    &val_SO_ERROR,&val_SO_ERROR_Size);
  //make sure getsockop didn't fail
  if(getSockOptReturn != 0)
    {
      close(fdSocket);
      fdSocket=badFD;
      return(false);
    }

  //Check if connect actually connected
  //Reset blocking mode if we got a connections
  if(val_SO_ERROR == 0)
    {
      //Turn off non-blocking mode
      //(fail if it didn't work)
      if(!SetNonBlocking(fdSocket,false))
	{
	  close(fdSocket);
	  fdSocket = badFD;
	  return(false);      
	}
      //Yay!
      return(true);
    }
  //There was an error, so close the socket are 
  //start another search
  close(fdSocket);
  fdSocket = badFD;
  return(false);
}

bool Connection::SetupDCNetThread(xmlNode * managerNode)
{
  //create a DCNetThread for this 
  thread = new DCNetThread();
  if(thread == NULL)
    {      
      close(fdSocket);
      fdSocket = badFD;
      return(false);
    }
  else
    {
      //Setup this thread
      if(!(thread->SetupConnection(fdSocket,				   
				   addrLocal,
				   addrRemote)))
	{
	  //Setup failed
	  close(fdSocket);
	  delete thread;
	  fdSocket = badFD;
	  return(false);
	}
      
      //Set up the memory manager for this DCNetThread
      if(!(thread->SetupPacketManager(managerNode)))
	{
	  //Allocation failed
	  close(fdSocket);
	  delete thread;
	  fdSocket = badFD;
	  return(false);
	}     
    }
  return(true);
}

