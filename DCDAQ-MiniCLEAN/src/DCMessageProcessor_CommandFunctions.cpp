#include <DCMessageProcessor.h>


void DCMessageProcessor::RegisterCommands()
{
  //Supplied message functions
  messageTypeToFunctionMap[DCMessage::LAUNCH]         
    = &DCMessageProcessor::Launch;          
  messageTypeToFunctionMap[DCMessage::REGISTER_ADDR]  
    = &DCMessageProcessor::RegisterAddr; 
  messageTypeToFunctionMap[DCMessage::UNREGISTER_ADDR]  
    = &DCMessageProcessor::UnRegisterAddr; 
  messageTypeToFunctionMap[DCMessage::REGISTER_TYPE]  
    = &DCMessageProcessor::RegisterType; 
  messageTypeToFunctionMap[DCMessage::UNREGISTER_TYPE]  
    = &DCMessageProcessor::UnRegisterType; 
  messageTypeToFunctionMap[DCMessage::STOP]           
    = &DCMessageProcessor::Stop;
  messageTypeToFunctionMap[DCMessage::GO]             
    = &DCMessageProcessor::Go;
  messageTypeToFunctionMap[DCMessage::PAUSE]          
    = &DCMessageProcessor::Pause;
  messageTypeToFunctionMap[DCMessage::RUN_NUMBER]     
    = &DCMessageProcessor::RunNumber;
  messageTypeToFunctionMap[DCMessage::GET_STATS]
    = &DCMessageProcessor::GetStats;
  messageTypeToFunctionMap[DCMessage::GET_ROUTE]
    = &DCMessageProcessor::GetRoute;
  messageTypeToFunctionMap[DCMessage::ERROR]
    = &DCMessageProcessor::InstallPrintThread;
  messageTypeToFunctionMap[DCMessage::WARNING]
    = &DCMessageProcessor::InstallPrintThread;
  messageTypeToFunctionMap[DCMessage::TEXT]
    = &DCMessageProcessor::InstallPrintThread;
  messageTypeToFunctionMap[DCMessage::COMMAND]
    = &DCMessageProcessor::ProcessCommand;
}

//Commands
void DCMessageProcessor::Stop(DCMessage::Message & message,
			      bool & run)
{
  run = false;
  uint64_t timeTemp = message.GetTime();
  
  printf("%s (%s): STOP!\n",
  	 GetTime(&(timeTemp)).c_str(),
  	 message.GetSource().GetName().c_str());      
}

void DCMessageProcessor::Go(DCMessage::Message & /*message*/,
			    bool & /*run*/)
{
  std::vector<DCThread*> threads = Launcher->GetThreads();
  //Send all threads the GO message
  for(unsigned int i = 0; i < threads.size();i++)
    {	  
      DCMessage::Message GOmessage;
      GOmessage.SetType(DCMessage::GO);
      if(threads[i] != NULL)
	{
	  //	threads[i]->SendMessageIn(GOmessage,true);
	  threads[i]->SendMessageIn(GOmessage);
	}
    }
}
void DCMessageProcessor::Pause(DCMessage::Message & /*message*/,
			       bool & /*run*/)
{
  std::vector<DCThread*> threads = Launcher->GetThreads();
  //Send all threads the PAUSE message
  for(unsigned int i = 0; i < threads.size();i++)
    {
      DCMessage::Message PAUSEmessage;
      PAUSEmessage.SetType(DCMessage::PAUSE);
      if(threads[i] != NULL)
	{
	  //	  threads[i]->SendMessageIn(PAUSEmessage,true);
	  threads[i]->SendMessageIn(PAUSEmessage);
	}
    }
}

void DCMessageProcessor::Launch(DCMessage::Message & message,    
				bool & run)             
{  
  std::vector<uint8_t> data = message.GetData();
  //Printout source if it is from another session, or it is
  // not from the DCDAQ main() 
  if(!message.GetSource().IsDefaultAddr() || !message.GetSource().GetName().empty())
    {
      printf("(DCMessageProcessor::Launch) Launch from (%s)\n",
	     message.GetSource().GetStr().c_str());
    }


  if(data.size() >0)
    {
      //copy string data.
      std::string setupText((char*)&data[0],data.size());
      
      //Parse the configuration string into an XML structure
      xmlDoc * configXML = xmlReadMemory(setupText.c_str(),
				     (int) setupText.size(),
				     NULL,
				     NULL,
				     0);
      //Check that the text was correctly parsed into XML
      if(configXML != NULL)
	{
	  //Get the base node of the xml
	  xmlNode * baseNode = xmlDocGetRootElement(configXML);
	  
	  //Parse the XML in this file and check the baseNode's name
	  if(boost::iequals(std::string((char*)baseNode->name),std::string("Setup")))
	    //Local Launch message
	    {
	      DCMessage::Message response;
	      uint64_t launchReturn = Launcher->Launch(setupText);
	      response.SetData(launchReturn);
	      response.SetType(DCMessage::LAUNCH_RESPONSE);
	      response.SetDestination(message.GetSource());
	      //Printout source and respond if it is from another session, or it is
	      // not from the DCDAQ main() 
	      if(!message.GetSource().IsDefaultAddr() || !message.GetSource().GetName().empty())
		{
		  printf("(DCMessageProcessor::Launch) Sending launch repsonse (0x%016"PRIx64") to (%s)\n",
			 launchReturn,message.GetSource().GetStr().c_str());
		  ProcessMessage(response);
		}
//	      //if the launch failed and came from DCDAQ and not a thread, fail
//	      if((launchReturn != 0) &&
//		 message.GetSourceAddress().IsBlank())
//	      if(launchReturn != 0)
//		{
//		  run = false;
//		}
	    }
	  else if(boost::iequals(std::string((char*) baseNode->name),std::string("RemoteSetup")))
	    {
	      DCMessage::SingleUINT64 errorData;
	      sprintf(errorData.text,"RemoteSetup tag no longer supported! Use RunControl!\n");
	      BuildError(errorData);
	    }
	}
    }
}

void DCMessageProcessor::RunNumber(DCMessage::Message & message,
				   bool & /*run*/)
{
  uint32_t runNumber = -1;  
  if(message.GetData(runNumber))
    {
      std::vector<DCThread*> threads = Launcher->GetThreads();
      //forward the RUN_NUMBER message to everyone.
      for(unsigned int i = 0; i < threads.size();i++)
	{
	  //Copy message's data into RunNumberMessage and send out.
	  DCMessage::Message RunNumberMessage = message;      
	  if(threads[i] != NULL)
	    {
	      //	      threads[i]->SendMessageIn(RunNumberMessage,true);
	      threads[i]->SendMessageIn(RunNumberMessage);
	    }
	}      
    }
}

void DCMessageProcessor::GetStats(DCMessage::Message & message,    
				  bool & /*run*/)             
{
  //Get the verbosity level
  int verbosity = 0;
  message.GetData(verbosity);

  std::vector<MemoryManager *> mem = Launcher->GetMemoryManagers();
  std::vector<MemoryManager *>::iterator itMem;
  for(itMem = mem.begin();itMem != mem.end();itMem++)
    {
      if((*itMem) != NULL)
	(*itMem)->PrintStatus(0,verbosity);
    }  
  
  std::vector<DCThread*> threads = Launcher->GetThreads();
  for(size_t i = 0;i < threads.size();i++)
    {      
      if((threads[i] != NULL) && (threads[i]->IsRunning()))
	threads[i]->PrintStatus();
    }
}

void DCMessageProcessor::GetRoute(DCMessage::Message & message,    
				  bool & /*run*/)             
{
  //Get the level of reporting, default to all for now
  int verbosity = 0;
  message.GetData(verbosity);

  PrintRemoteRouteTables();
  PrintTypeThreadRouteTables();
  PrintTypeFunctionRouteTables();
}


void DCMessageProcessor::RegisterAddr(DCMessage::Message & message,
				      bool & /*run*/)
{
  std::vector<uint8_t> data;
  DCMessage::DCAddress Addr;
  std::string Thread;
  DCMessage::DCRoute routeInfo;
 
  //Get the data from the DCMessage
  data = message.GetData();
  //Parse the data using our DCRoute GetData function
  if(routeInfo.GetData(data,Addr,Thread))
    {  
      //Add this route to the map
      messageAddrToNameMap[Addr] = Thread;
    }
}

void DCMessageProcessor::UnRegisterAddr(DCMessage::Message & message,
					bool & /*run*/)
{
  DCMessage::DCAddress Addr;
  std::string Thread;
  DCMessage::DCRoute routeInfo;
 
  //Get the data from the DCMessage
  std::vector<uint8_t> data = message.GetData();
  //Parse the data using our DCRoute GetData function
  if(routeInfo.GetData(data,Addr,Thread))
    {
  
      //Find this entry in the map
      std::map<DCMessage::DCAddress,std::string>::iterator it;
      it = messageAddrToNameMap.find(Addr);
      if(it != messageAddrToNameMap.end())
	{
	  messageAddrToNameMap.erase(it);
	}
    }
}


void DCMessageProcessor::RegisterType(DCMessage::Message & message,
				      bool & /*run*/)
{
  //Get the data from the DCMessage
  DCMessage::DCString dcString;
  std::string text;

  if(dcString.GetData(message.GetData(),text))
    {
      DCMessage::DCAddress address = message.GetSource();
      //Add this route to the map      
      DCMessage::DCMessageType type = DCMessage::GetTypeFromName(text.c_str());
      messageTypeToAddressMap[type] = address;
      printf("DCMessageProcessor: Routing type %s to %s\n",
	     DCMessage::GetTypeName(type),
	     address.GetStr().c_str());        
    }
}

void DCMessageProcessor::UnRegisterType(DCMessage::Message & message,
					bool & /*run*/)
{
  //Get the data from the DCMessage
  DCMessage::DCString dcString;
  std::string text;

  if(dcString.GetData(message.GetData(),text))
    {
      DCMessage::DCMessageType type = DCMessage::GetTypeFromName(text.c_str());
      //Find this entry in the map
      std::map<uint16_t,DCMessage::DCAddress>::iterator it;
      it = messageTypeToAddressMap.find(type);
      if(it != messageTypeToAddressMap.end())
	{
	  messageTypeToAddressMap.erase(it);
	}      
    }
}

void DCMessageProcessor::InstallPrintThread(DCMessage::Message & message,
					    bool & run)
{
  //Look to see if there is a print server running
  const std::vector<DCThread *> threads = Launcher->GetThreads();
  DCThread * foundThread = NULL;
  bool foundPrintThread = false;
  for(size_t iThread = 0; iThread < threads.size();iThread++)
    {
      if(threads[iThread] != NULL)
	{
	  if(boost::iequals("PrintServer",threads[iThread]->GetType()))
	    {
	      foundPrintThread = true;
	      foundThread = threads[iThread];
	      break;
	    }
	}
    }
  DCMessage::DCAddress address;
  if(!foundPrintThread)
    {
      printf("No print handler installed.  Installing default local thread\n");
      //Hardcoded config to start the basic local ErrorPrinter
      std::string setupString("<SETUP><THREADS><THREAD><TYPE>PrintServer</TYPE><NAME>LocalPrinter</NAME><NOAUTOCONFIG/></THREAD></THREADS></SETUP>");
      DCMessage::Message setupMessage;
      setupMessage.SetData(setupString.c_str(),setupString.size());
      setupMessage.SetType(DCMessage::LAUNCH);
      //We need to set these up before we call ProcessMessage
      bool Failed;
      Launch(setupMessage,Failed);
      if(Failed)
	{
	  printf("Error installing PrintServer\n");
	  run = false;
	}
      address.Set("LocalPrinter");
    }
  else
    {
      if(foundThread != NULL)
	address.Set(foundThread->GetName());
      else
	{
	  printf("DCLauncher: somehow we found a thread and then didn't.\n");
	  run = false;
	}
    }

  //setup route to thread
  messageTypeToAddressMap[message.GetType()] = address;
  //Now process old error
  ProcessMessage(message);
}

void DCMessageProcessor::ProcessCommand(DCMessage::Message & message,bool &/*run*/)
{
  DCMessage::Message response;  
  std::string command;
  DCMessage::DCString stringParser;
  bool respond = false;
  if(stringParser.GetData(message.GetData(),command))
    {
      if(boost::iequals(command,"listthreads"))
	{
	  std::stringstream response_Stringstream;
	  response_Stringstream << "Thread_Name Thread_Type" << std::endl;
	  const std::vector<DCThread *> threads = Launcher->GetThreads();
	  for(size_t i = 0; i < threads.size();i++)
	    {
	      if(threads[i] != NULL)
		response_Stringstream <<  threads[i]->GetName() << " " << threads[i]->GetType() << std::endl;
	    }	  
	  std::vector<uint8_t> data = stringParser.SetData(response_Stringstream.str());
	  response.SetData(&(data[0]),data.size());
	  respond = true;
	}
      else if(boost::iequals(command,"listmms"))
	{
	  std::stringstream response_Stringstream;
	  const std::vector<MemoryManager *> memoryManagers = Launcher->GetMemoryManagers();
	  for(size_t i = 0; i < memoryManagers.size();i++)
	    {
	      if(memoryManagers[i] != NULL)
		response_Stringstream <<  memoryManagers[i]->GetName() << " " << memoryManagers[i]->GetType() << std::endl;
	    }	  
	  std::vector<uint8_t> data = stringParser.SetData(response_Stringstream.str());
	  response.SetData(&(data[0]),data.size());
	  respond = true;
	}
      else if(boost::iequals(command,"listroutes"))
	{
	  std::stringstream response_Stringstream;
	  //addr type routes
	  for(std::map<DCMessage::DCAddress,std::string>::iterator addrIt = messageAddrToNameMap.begin();
	      addrIt != messageAddrToNameMap.end();
	      addrIt++)
	    {
	      response_Stringstream << "Addr " 	       
				    << addrIt->first.GetAddrStr()
				    << addrIt->second << std::endl;
	    }
	  //message type routes
	  for(std::map<uint16_t,DCMessage::DCAddress>::iterator addrIt = messageTypeToAddressMap.begin();
	      addrIt != messageTypeToAddressMap.end();
	      addrIt++)
	    {
	      response_Stringstream << "Type " << DCMessage::GetTypeName(DCMessage::DCMessageType(addrIt->first))
				    << " "
				    << addrIt->second.GetAddrStr()
				    << addrIt->second.GetName() << std::endl;
	    }
	  std::vector<uint8_t> data = stringParser.SetData(response_Stringstream.str());
	  response.SetData(&(data[0]),data.size());
	  respond = true;	  
	}

      if(respond)
	{
	  response.SetDestination(message.GetSource());
	  response.SetType(DCMessage::COMMAND_RESPONSE);
	  ProcessMessage(response);
	}
    }
}

