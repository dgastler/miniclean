#ifdef __RATDS__

#ifndef __EBPCASSEMBLER__
#define __EBPCASSEMBLER__
//EBPCAssembler 

#include <DCThread.h>
#include <xmlHelper.h>

#include <ServerConnection.h>
#include <Connection.h>

#include <DetectorInfo.h>

#include <EBDataPacket.h>
#include <RAT/DS/Root.hh>
#include <RAT/DS/EV.hh>
#include <RAT/DS/ReductionLevel.hh>

#include <RATDiskBlockManager.h>

#include <TFile.h>
#include <TH1.h>

#include <sstream>

#include <EventMonitor.h>
#include <RATCopyQueue.h>

class EBPCAssembler : public DCThread 
{
 public:
  EBPCAssembler(std::string Name);
  ~EBPCAssembler(){};

  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  enum RATAssemberErrorCodes
  {
    OK = 0,
    LOCK_BLOCKED    = -1,
    WFD_NOT_PROCESSED = -2,
    FATAL_ERROR       = -3,
    BAD_WFD_HEADER    = -4
  };

  
  //======================================
  //EBPCAssembler.cpp
  //======================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ProcessChildMessage(size_t iConn);
  virtual void PrintStatus();
  //======================================

  //======================================
  //EBPCAssembler_Run.cpp
  //======================================
  void ProcessEBPusher(size_t iConn);
  void ProcessCollision(int64_t newEventID,
			int64_t oldEventID,
			int32_t evIndex,
			uint64_t password);
  void ProcessBadPacketLostPMT(size_t iConn);
  //======================================

  //======================================
  //EBPCAssembler_ProcessCHAN_ZLE_INTEGRAL.cpp
  //======================================
  bool ProcessCHAN_ZLE_INTEGRAL(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessCHAN_ZLE_WAVEFORM.cpp
  //======================================
  bool ProcessCHAN_ZLE_WAVEFORM(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessCHAN_PROMPT_TOTAL.cpp
  //======================================
  bool ProcessCHAN_PROMPT_TOTAL(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessEVNT_ZLE_WAVEFORM.cpp
  //======================================
  bool ProcessEVNT_ZLE_WAVEFORM(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessEVNT_ZLE_INTEGRAL.cpp
  //======================================
  bool ProcessEVNT_ZLE_INTEGRAL(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessEVNT_PROMPT_TOTAL.cpp
  //======================================
  bool ProcessEVNT_PROMPT_TOTAL(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessEVNT_FULL_WAVEFORM.cpp
  //======================================
  bool ProcessEVNT_FULL_WAVEFORM(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_ProcessCHAN_FULL_WAVEFORM.cpp
  //======================================
  bool ProcessCHAN_FULL_WAVEFORM(uint8_t * data, uint32_t size,RATDSBlock * event);

  //======================================
  //EBPCAssembler_Net.cpp
  //======================================
  bool ProcessNewConnection();
  void DeleteConnection(size_t iConn);
  //======================================


  //Network data members
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  ServerConnection server;
  
  //Event Builder PC memory manager
  RATDiskBlockManager * EBPC_mem;

  //Detector information
  DetectorInfo detectorInfo;

  //DataProcessing values
  uint32_t promptStartTime;
  uint32_t lateStartTime;

  std::vector<int> reductionDataRate;
  time_t currentTime;
  time_t lastTime;
//  std::vector<RAT::DS::PMT*> pmtCopies;
//  std::vector<RAT::DS::Raw*> rawCopies;
//  std::vector<RAT::DS::RawIntegral*> rawIntegralCopies;
//  std::vector<RAT::DS::RawWaveform*> rawWaveformCopies;
//  RAT::DS::EV * ev; 

  double EventTotalQ;
  std::map<int,TH1F*> TotalQHist;


  //TH1F * ZLEWaveformQ;
  //TH1F * ZLEIntegralQ;
  //TH1F * PromptTotalQ;
  
  int EBPCEventCount;

  //Monitoring
  EventMonitor eventMonitor;


  virtual void  ThreadCleanUp();

};


#endif

#endif
