#include <FEPCRATManager.h>

bool FEPCRATManager::GetSend(int32_t &index)
{
  bool ret = false;
  ret = Send.Get(index);
  return(ret);
}

bool FEPCRATManager::AddFree(int32_t &index)
{
  bool ret = false;
  ret = Free.Add(index);
  //signal so that if anyone is in cond_wait in Get() they wake up
  return(ret);
}
