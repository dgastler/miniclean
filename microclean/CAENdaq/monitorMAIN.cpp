#include <iostream>

#include "monitor.h"
#include "Event.h"

#include "TROOT.h"
#include "TApplication.h"


using namespace std;


int main(int argc, char **argv)
{
  if(argc < 1)
    {
      cerr << argv[0] << " filename" << endl;
      return(0);
    }
  Char_t * filename  = argv[0];
  Char_t * options = "APL";
  if(argc > 1)
    options = argv[1];

  TApplication theApp("App", &argc, argv);
  
  if (gROOT->IsBatch()) {
    cerr << argv[0] << "cannot run in batch mode\n";
    return (1);
  }
  Viewer viewer(gClient->GetRoot(),filename,options);
  theApp.Run();
  
  return(0);
}
