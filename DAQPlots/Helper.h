#include <string>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <sstream>

#include <mysql.h>

#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TStyle.h>

#ifdef __DEFINE_GLOBALS__
time_t badTime = 0;
#else
extern time_t badTime;
#endif

int  GetLastRunNumber(MYSQL * conn);
bool GetRunTimes(MYSQL * conn,int runNumber,time_t & start, time_t & end);
bool GetXYData(MYSQL * conn,const std::string & query,std::vector<double> &time, std::vector<double> &val);
void ReZeroTimeVector(std::vector<double> & times,double newZero,double rescale);
double FindTGraphMax(TGraph * gr);
