#ifndef __LOWRESWAVEFORM__
#define __LOWRESWAVEFORM__

#include <DS/Root.hh>
#include <iostream>
#include <vector>
#include <math.h>
#include <iostream>

class LowResWaveform
{
 public:
  LowResWaveform(){};
  LowResWaveform(size_t samples)
    {
      waveformLow.resize(samples);
      waveformMid.resize(samples);
      waveformHigh.resize(samples);      
    };
  LowResWaveform(const LowResWaveform & rhs){copy(rhs);};
  LowResWaveform & operator= (const LowResWaveform & rhs){copy(rhs);return *this;};
  
  double finalPromptPEs;
  double finalLatePEs;

  void Process(RAT::DS::Channel * chan,
	       float ADCthreshLow,
	       float ADCthreshMid,
	       float ADCthreshHigh,
	       int grouping = 5,
	       int size = 500,
	       int preSamples = 250,
	       int promptSamples = 20,
	       float baseline = 4073,
	       bool saveReal = false)
  {

    if(saveReal)
      {
	realWaveform.assign(size*grouping,0);
      }

    //Setup a lookup table for the nonlinear 3 bit ADCthreshs
    std::vector<double> levels(7);
    std::vector<uint8_t> high(7),mid(7),low(7);

    levels[0] = grouping *(ADCthreshHigh + ADCthreshMid + ADCthreshLow);       
    high[0] = 1; mid[0] = 1; low[0] = 1;

    levels[1] = grouping *(ADCthreshHigh + ADCthreshMid);
    high[1] = 1; mid[1] = 1; low[1] = 0;

    levels[2] = grouping *(ADCthreshHigh + ADCthreshLow);
    high[2] = 1; mid[2] = 0; low[2] = 1;

    levels[3] = grouping *(ADCthreshHigh);
    high[3] = 1; mid[3] = 0; low[3] = 0;

    levels[4] = grouping *(ADCthreshMid + ADCthreshLow);
    high[4] = 0; mid[4] = 1; low[4] = 1;

    levels[5] = grouping *(ADCthreshMid);
    high[5] = 0; mid[5] = 1; low[5] = 0;

    levels[6] = grouping *(ADCthreshLow);
    high[6] = 0; mid[6] = 0; low[6] = 1;

    double promptAreaRAT = 0;
    double promptAreaMCB = 0;
    double lateAreaRAT = 0;
    double lateAreaMCB = 0;

    ZeroWaveform();
    //Loop over all the zle windows in the channel
    for(int iRaw = 0; iRaw < chan->GetRawBlockCount();iRaw++)
      {
	RAT::DS::RawBlock * block = chan->GetRawBlock(iRaw);
	int offset = block->GetOffset();
	std::vector<uint16_t> & samples = block->GetSamples();
	bool foundSomething = false;
	for(int iSample = 0; iSample < int(samples.size());iSample+=grouping)
	  {	    
	    //Set the real (full MC)  waveform
	    int realPosition = (offset + iSample);	    

	    if(saveReal)
	      {
		realWaveform[realPosition + 0] = (baseline - samples[iSample + 0]);
		realWaveform[realPosition + 1] = (baseline - samples[iSample + 1]); 
		realWaveform[realPosition + 2] = (baseline - samples[iSample + 2]); 
		realWaveform[realPosition + 3] = (baseline - samples[iSample + 3]); 
		realWaveform[realPosition + 4] = (baseline - samples[iSample + 4]);	    
	      }

	    //Calculate the area of this 5sample block for the low-res waveform
	    double area = ((baseline - samples[iSample + 0]) + 
			   (baseline - samples[iSample + 1]) + 
			   (baseline - samples[iSample + 2]) + 
			   (baseline - samples[iSample + 3]) + 
			   (baseline - samples[iSample + 4]));	    
	    int waveformPos = (realPosition)/grouping;
	    if((waveformPos > 0) && (waveformPos < int(waveformLow.size())))
	      {
		//check if we are in the prompt time or not
		if((realPosition >= preSamples) && (realPosition < (preSamples + promptSamples)))
		  {
		    foundSomething = true; //we never fix the prompt in this way
		    for(int iCut = 0; iCut < int(levels.size());iCut++)
		      {
			if(area > levels[iCut])
			  {
			    waveformHigh[waveformPos] = high[iCut];
			    waveformMid[waveformPos]  = mid[iCut];
			    waveformLow[waveformPos]  = low[iCut];		
			    //			    promptAreaMCB += levels[iCut];
			    break;
			  }
		      }
		    promptAreaRAT += area;
		  }
		//not in prompt time
		else
		  {			  
		    lateAreaRAT += area;
		    //if this is big enough to be extended to the next bin
		    //for the time being, we won't extend past the next sample
		    if(area > (2*ADCthreshLow*grouping))
		      {		
			foundSomething = true;
			waveformLow[waveformPos] = 1;
			//Set the next sample to 1 if it is safe to
			if((waveformPos + 1) < waveformLow.size())
			  waveformLow[waveformPos+1] = 1;
		      }
		    //We just need to set this bin to 1, but we should check that
		    //the last bin didn't already do that. 
		    else if(area > ADCthreshLow*grouping)
		      {
			foundSomething = true;
			//Check if we have already set this timing bin to one
			if(
			   (waveformLow[waveformPos] == 1) && 
			   ((waveformPos + 1) < waveformLow.size())
			   )
			  {
			    //Set the next one to 1
			    waveformLow[waveformPos+1] = 1;
			  }
			//Normal state, just set it to 1.
			else
			  {
			    waveformLow[waveformPos] = 1;
			  }
		      }		    
		  }
	      }
	    //sample loop
	    if(!foundSomething)
	      {
		//We had a ZLE window, but nothing crossed the threshold. 
		int position = (offset/grouping + 1);	    		
		//TO keep the ZLE count correct, we are going to put a single pulse here. 
		waveformLow[position] = 1;
	      }
	  }
      }    


    double promptSamplesEnd  = (promptSamples + preSamples)/grouping;
    promptAreaMCB = lateAreaMCB = 0;
    //Calculate our fake prompt and late light
    for(int iSample = preSamples/grouping; iSample < waveformLow.size();iSample++)
      {
	if(iSample < promptSamplesEnd)
	  {
	    promptAreaMCB += (waveformLow[iSample]* ADCthreshLow +
			      waveformMid[iSample]* ADCthreshMid +
			      waveformHigh[iSample]* ADCthreshHigh);
	  }	
	else
	  {
	    lateAreaMCB += waveformLow[iSample]*ADCthreshLow;
	  }
      }
    promptAreaMCB*=grouping;
    lateAreaMCB*=grouping;
    
//    printf("prompt %f/%f  Late %f/%f\n",
//	   promptAreaRAT,promptAreaMCB,
//	   lateAreaRAT,lateAreaMCB);
    
    finalPromptPEs = promptAreaMCB/(grouping*ADCthreshLow);
    finalLatePEs = lateAreaMCB/(grouping*ADCthreshLow);;
    

    ////adjust the prompt window charge
    //int promptStart = preSamples/grouping;
    //int promptEnd = (preSamples + promptSamples)/grouping;
    //double promptUnderEstimate = promptAreaRAT - promptAreaMCB;
    //for(int iSample = promptStart; 
    //	(iSample < promptEnd);
    //	iSample++)
    //  {
    //	double sampleValue = (ADCthreshHigh*waveformHigh[iSample] +
    //			      ADCthreshMid*waveformMid[iSample] + 
    //			      ADCthreshLow*waveformLow[iSample]);
    //	for(int iCut = (levels.size()-1);iCut > 0;iCut--)
    //	  {	    
    //	    if((sampleValue < levels[iCut]) &&
    //	       ((levels[iCut] - sampleValue) < promptUnderEstimate))
    //	      {
    //		promptUnderEstimate -= (levels[iCut] - sampleValue);
    //		waveformHigh[iSample] = high[iCut];
    //		waveformMid[iSample] = mid[iCut];
    //		waveformLow[iSample] = low[iCut];
    //	      }
    //	  }
    //	finalPromptArea += (ADCthreshHigh*waveformHigh[iSample] +
    //			    ADCthreshMid*waveformMid[iSample] + 
    //			    ADCthreshLow*waveformLow[iSample]);
    //  }
  }
  const std::vector<uint8_t> & GetWaveformLow(){return waveformLow;};
  const std::vector<uint8_t> & GetWaveformMid(){return waveformMid;};
  const std::vector<uint8_t> & GetWaveformHigh(){return waveformHigh;};
  const std::vector<float> & GetRealWaveform(){return realWaveform;};
 private:


  std::vector<uint8_t> waveformLow;
  std::vector<uint8_t> waveformMid;
  std::vector<uint8_t> waveformHigh;
  std::vector<float> realWaveform;

  void copy(const LowResWaveform & rhs)
  {  
    waveformLow = rhs.waveformLow;
    waveformMid = rhs.waveformMid;
    waveformHigh = rhs.waveformHigh;
    finalPromptPEs =  rhs.finalPromptPEs ;
    finalLatePEs  =  rhs.finalLatePEs ;
    realWaveform = rhs.realWaveform;
  };
  void ZeroWaveform()
  {
    if(waveformLow.size() > 0)
      memset(&(waveformLow[0]),0,sizeof(uint8_t)*waveformLow.size());

    if(waveformMid.size() > 0)
      memset(&(waveformMid[0]),0,sizeof(uint8_t)*waveformMid.size());

    if(waveformHigh.size() > 0)
      memset(&(waveformHigh[0]),0,sizeof(uint8_t)*waveformHigh.size());
  };
};

#endif
