#include <RunControl_XML.h>

RunControl_XML::RunControl_XML(DCMySQL * conn,int runNumber,XML_Factory * fac)
  : Thread_XML(conn,runNumber)
{
  //WARNING!!!
  //The factory CAN NOT be used in this function!
  //WARNING!!!
  parentFactory = fac;
  //WARNING!!!
  //Only in the function Generate_XML and those that it calls.
  //WARNING!!!
}

bool RunControl_XML::Generate_XML(xmlNode * threadsNode,
				  std::string pcName,
				  std::string threadType,
				  std::string threadName,
				  xmlNode * responseNode)
{
  bool ret = true;
  //Set up most of the thread using the normal Thread_XML Generate_XML


  ret |= Thread_XML::Generate_XML(threadsNode,
				  pcName,
				  threadType,
				  threadName,
				  responseNode);

  //Add the "GO" node (should add this to the data tables)
  xmlNewChild(threadNode,NULL,(xmlChar *)"GO",NULL);
  //====================================================================
  //Build all the OpBlocks
  //====================================================================

  //Create the first group of messages.
  ret |= CreateMessages("Messages_Start");
  //Create DCMessageClient Connections to other DCDAQ sessions
  ret |= CreateConnections();
  //Create the second group of messages.
  ret |= CreateMessages("Messages_BeforeThreads");



  //Create remote threads

  std::stringstream query;
  query << "SELECT pc_name,IP from NetworkConnections WHERE run_number="
	<< runNumber << " ORDER BY order_id";
  std::vector<std::vector<std::string> > PCList;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  DCMySQLConnection->GetTable(PCList);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  for(size_t iPC=0; iPC < PCList.size();iPC++)
    {
      ret |= CreateRemoteXML(PCList[iPC][0],PCList[iPC][1]);
      if(!ret)
	{
	  printf("Error at %s:%s\n",
		 PCList[iPC][0].c_str(),
		 PCList[iPC][1].c_str());
	}
    }



  //Create the final group of messages.
  ret |= CreateMessages("Messages_End");

  ret |= CreateShutdown();
  return ret;
}

bool RunControl_XML::CreateShutdown()
{
  int ret = true;
  //Build sql query
  std::stringstream query;  
  enum{TYPE=0,SRC_IP,SRC_NAME,DEST_IP,DEST_NAME,DATA_TYPE,DATA}; //Enum for array ordering
  query << "SELECT type,src_ip,src_name,dest_ip,dest_name,data_type,data" 
	<< " from " << "Messages_Shutdown"
	<< " WHERE run_number=" << runNumber
	<< " ORDER BY order_id";
  std::vector<std::vector<std::string> > msgsData;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  DCMySQLConnection->GetTable(msgsData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  //Create XML for each message
  xmlNode * shutdownNode = xmlNewChild(threadNode,NULL,
				       (xmlChar*) "STOPOPBLOCKS",NULL);
  
  xmlNode * opblockNode = NULL;
  for(size_t iMsg = 0; iMsg < msgsData.size();iMsg++)
    {
      //Build this opblock
      opblockNode = xmlNewChild(shutdownNode,NULL,
				(xmlChar*) "OPBLOCK",NULL);
      xmlNode * opNode = xmlNewChild(opblockNode,NULL,
				     (xmlChar*) "OP",NULL);
      //====================================================================      
      //Add type
      //====================================================================
      xmlNewTextChild(opNode,NULL,
		      (xmlChar*) "TYPE",
		      (xmlChar*) ((msgsData[iMsg][TYPE]).c_str()));
	  
      //====================================================================      
      //Build src if IP or name is not NULL
      //====================================================================
      if( (!boost::algorithm::iequals(msgsData[iMsg][SRC_IP],"NULL")) ||
	  (!boost::algorithm::iequals(msgsData[iMsg][SRC_NAME],"NULL")) )
	{
	  //Source XML tag
	  xmlNode * srcNode = xmlNewChild(opNode,NULL,
					  (xmlChar*) "SOURCE",NULL);
	  //IP tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][SRC_IP],"NULL"))
	    {
	      xmlNewTextChild(srcNode,NULL,(xmlChar*) "ADDR",
			      (xmlChar*) ((msgsData[iMsg][SRC_IP]).c_str()));
	    }
	  //NAME tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][SRC_NAME],"NULL"))
	    {
	      xmlNewTextChild(srcNode,NULL,(xmlChar*) "NAME",
			      (xmlChar*) ((msgsData[iMsg][SRC_NAME]).c_str()));
	    }
	}
      //====================================================================
      //Build dest if IP or name is not NULL
      //====================================================================
      if( (!boost::algorithm::iequals(msgsData[iMsg][DEST_IP],"NULL")) ||
	  (!boost::algorithm::iequals(msgsData[iMsg][DEST_NAME],"NULL")) )
	{
	  //destination XML tag
	  xmlNode * destNode = xmlNewChild(opNode,NULL,
					  (xmlChar*) "DEST",NULL);
	  //IP tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][DEST_IP],"NULL"))
	    {
	      xmlNewTextChild(destNode,NULL,(xmlChar*) "ADDR",
			      (xmlChar*) ((msgsData[iMsg][DEST_IP]).c_str()));
	    }
	  //NAME tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][DEST_NAME],"NULL"))
	    {
	      xmlNewTextChild(destNode,NULL,(xmlChar*) "NAME",
			      (xmlChar*) ((msgsData[iMsg][DEST_NAME]).c_str()));
	    }
	}
      //====================================================================
      //Build data if data_type is not NULL
      //====================================================================
      if(!boost::algorithm::iequals(msgsData[iMsg][DATA_TYPE],"NULL"))
	{
	  //data XML tag
	  xmlNode * dataNode = xmlNewChild(opNode,NULL,
					   (xmlChar*) "DATA",NULL);
	  //data type tag containing data
	  xmlNewChild(dataNode,NULL,
		      (xmlChar*) msgsData[iMsg][DATA_TYPE].c_str(),
		      (xmlChar*) msgsData[iMsg][DATA].c_str());
	}      

    }
  //====================================================================
  //Build shutdown response for the last message
  //====================================================================
  if(opblockNode != NULL)
    {
      //Create the launch response...response check
      xmlNode * launchResponseNode = xmlNewChild(opblockNode,NULL,(xmlChar*) "RESPONSE",NULL);
      xmlNewTextChild(launchResponseNode,NULL,(xmlChar*) "TYPE",(xmlChar*) "STOP");
      //      xmlNewTextChild(launchResponseNode,NULL,(xmlChar*) "TYPE",(xmlChar*) "NETWORK");
    }
  
  return ret;
}


bool RunControl_XML::CreateMessages(std::string msgTable)
{
  int ret = true;
  //Build sql query
  std::stringstream query;  
  enum{TYPE=0,SRC_IP,SRC_NAME,DEST_IP,DEST_NAME,DATA_TYPE,DATA}; //Enum for array ordering
  query << "SELECT type,src_ip,src_name,dest_ip,dest_name,data_type,data" 
	<< " from " << msgTable
	<< " WHERE run_number=" << runNumber
	<< " ORDER BY order_id";
  std::vector<std::vector<std::string> > msgsData;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  DCMySQLConnection->GetTable(msgsData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  //Create XML for each message
  for(size_t iMsg = 0; iMsg < msgsData.size();iMsg++)
    {
      //Build this opblock
      xmlNode * opblockNode = xmlNewChild(threadNode,NULL,
					  (xmlChar*) "OPBLOCK",NULL);
      xmlNode * opNode = xmlNewChild(opblockNode,NULL,
				     (xmlChar*) "OP",NULL);
      //====================================================================      
      //Add type
      //====================================================================
      xmlNewTextChild(opNode,NULL,
		      (xmlChar*) "TYPE",
		      (xmlChar*) ((msgsData[iMsg][TYPE]).c_str()));
	  
      //====================================================================      
      //Build src if IP or name is not NULL
      //====================================================================
      if( (!boost::algorithm::iequals(msgsData[iMsg][SRC_IP],"NULL")) ||
	  (!boost::algorithm::iequals(msgsData[iMsg][SRC_NAME],"NULL")) )
	{
	  //Source XML tag
	  xmlNode * srcNode = xmlNewChild(opNode,NULL,
					  (xmlChar*) "SOURCE",NULL);
	  //IP tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][SRC_IP],"NULL"))
	    {
	      xmlNewTextChild(srcNode,NULL,(xmlChar*) "ADDR",
			      (xmlChar*) ((msgsData[iMsg][SRC_IP]).c_str()));
	    }
	  //NAME tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][SRC_NAME],"NULL"))
	    {
	      xmlNewTextChild(srcNode,NULL,(xmlChar*) "NAME",
			      (xmlChar*) ((msgsData[iMsg][SRC_NAME]).c_str()));
	    }
	}
      //====================================================================
      //Build dest if IP or name is not NULL
      //====================================================================
      if( (!boost::algorithm::iequals(msgsData[iMsg][DEST_IP],"NULL")) ||
	  (!boost::algorithm::iequals(msgsData[iMsg][DEST_NAME],"NULL")) )
	{
	  //destination XML tag
	  xmlNode * destNode = xmlNewChild(opNode,NULL,
					  (xmlChar*) "DEST",NULL);
	  //IP tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][DEST_IP],"NULL"))
	    {
	      xmlNewTextChild(destNode,NULL,(xmlChar*) "ADDR",
			      (xmlChar*) ((msgsData[iMsg][DEST_IP]).c_str()));
	    }
	  //NAME tag
	  if(!boost::algorithm::iequals(msgsData[iMsg][DEST_NAME],"NULL"))
	    {
	      xmlNewTextChild(destNode,NULL,(xmlChar*) "NAME",
			      (xmlChar*) ((msgsData[iMsg][DEST_NAME]).c_str()));
	    }
	}
      //====================================================================
      //Build data if data_type is not NULL
      //====================================================================
      if(!boost::algorithm::iequals(msgsData[iMsg][DATA_TYPE],"NULL"))
	{
	  //data XML tag
	  xmlNode * dataNode = xmlNewChild(opNode,NULL,
					   (xmlChar*) "DATA",NULL);
	  //data type tag containing data
	  xmlNewChild(dataNode,NULL,
		      (xmlChar*) msgsData[iMsg][DATA_TYPE].c_str(),
		      (xmlChar*) msgsData[iMsg][DATA].c_str());
	}      
    }
  return ret;
}

bool RunControl_XML::CreateConnections()
{
  bool ret = true;
  
  //build query to find out which PCs we need to connect to
  std::stringstream query;
  query << "SELECT pc_name from NetworkConnections WHERE run_number="
	<< runNumber << " ORDER BY order_id";
  std::vector<std::vector<std::string> > ConnectionList;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  DCMySQLConnection->GetTable(ConnectionList);
  DCMySQLConnection->FreeResult();

  std::string pcName("local_conn");
  std::string threadType("DCMessageClient");
  query.str(std::string());
  
  for(size_t iConnection =0; iConnection < ConnectionList.size();iConnection++)
    {
      //Build this opblock
      xmlNode * opBlockNode = xmlNewChild(threadNode,NULL,(xmlChar*) "OPBLOCK",NULL);

      //Build Op node
      xmlNode * opNode = xmlNewChild(opBlockNode,NULL,(xmlChar*) "OP",NULL);

      //Since these are all Launch messages for DCMessageClients, we can hard code a few things
      //These are all for setting up a thread launch message
      xmlNewTextChild(opNode,NULL,(xmlChar*) "TYPE",(xmlChar*) "LAUNCH");
      xmlNode * dataNode = xmlNewChild(opNode,NULL,(xmlChar*) "DATA",NULL);
      ///Must be named xml_Node because xmlNode is a type
      xmlNode * xml_Node = xmlNewChild(dataNode,NULL,(xmlChar*) "XML",NULL);
      xmlNode * setupNode = xmlNewChild(xml_Node,NULL,(xmlChar*) "SETUP",NULL);
      xmlNode * threadsNode = xmlNewChild(setupNode,NULL,(xmlChar*) "THREADS",NULL);      

      //Build the xml for this thread
      ret |= parentFactory->AddThread(threadsNode,
      				      pcName,
      				      threadType,
      				      ConnectionList[iConnection][0],
      				      opBlockNode);
      
      

      //Create the launch response...response check
      xmlNode * launchResponseNode = xmlNewChild(opBlockNode,NULL,(xmlChar*) "RESPONSE",NULL);
      xmlNewTextChild(launchResponseNode,NULL,(xmlChar*) "TYPE",(xmlChar*) "LAUNCH_RESPONSE");
      xmlNode * responseDataNode = xmlNewChild(launchResponseNode,NULL,(xmlChar*) "DATA",NULL);
      xmlNewTextChild(responseDataNode,NULL,(xmlChar*) "WORD32",(xmlChar*) "0x0");
      xmlNewTextChild(responseDataNode,NULL,(xmlChar*) "WORD32",(xmlChar*) "0x0");
    }
  return ret;
}

bool RunControl_XML::CreateRemoteXML(std::string PC,std::string IP)
{
  bool ret = true;

  //Create the OpBlock and the base XML in it
  xmlNode * opBlockNode = xmlNewChild(threadNode,NULL,(xmlChar*) "OPBLOCK",NULL);
  
  //Build Op node
  xmlNode * opNode = xmlNewChild(opBlockNode,NULL,(xmlChar*) "OP",NULL);

  //Build launch message
  xmlNewTextChild(opNode,NULL,(xmlChar*) "TYPE",(xmlChar*) "LAUNCH");
  
  //Message routing information
  xmlNode * destNode = xmlNewChild(opNode,NULL,(xmlChar*) "DEST",NULL);
  xmlNewTextChild(destNode,NULL,(xmlChar*) "ADDR",(xmlChar*) IP.c_str());

  //message data (threads/memory managers)
  xmlNode * dataNode = xmlNewChild(opNode,NULL,(xmlChar*) "DATA",NULL);
  //can't name it xmlNode because that is a type
  xmlNode * xml_Node = xmlNewChild(dataNode,NULL,(xmlChar*) "XML",NULL);
  xmlNode * setupNode = xmlNewChild(xml_Node,NULL,(xmlChar*) "SETUP",NULL);

  //Create the thread XML for this PC
  xmlNode * threadsNode = xmlNewChild(setupNode,NULL,(xmlChar*) "THREADS",NULL);      
  std::stringstream query;
  query << "select thread_type,thread_name from Thread_Threads " 
	<< "where run_number=" << runNumber 
	<< " and pc_name='" << PC
	<< "' order by order_id";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  std::vector<std::vector<std::string> > threads;
  DCMySQLConnection->GetTable(threads);
  DCMySQLConnection->FreeResult();
  query.str(std::string());
  for(size_t iThread =0; iThread < threads.size();iThread++)
    {
      ret |= parentFactory->AddThread(threadsNode,
				      PC,
				      threads[iThread][0],
				      threads[iThread][1],
				      opBlockNode);
    }
  
  //Create the memory manager XML for this PC
  xmlNode * memManagersNode = xmlNewChild(setupNode,NULL,(xmlChar*) "MEMORYMANAGERS",NULL);      
  query << "select mm_type,mm_name from Memory_MemoryManagers " 
	<< "where run_number=" << runNumber 
	<< " and pc_name='" << PC
	<< "' order by order_id";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  std::vector<std::vector<std::string> > memManagers;
  DCMySQLConnection->GetTable(memManagers);
  DCMySQLConnection->FreeResult();
  query.str(std::string());
  for(size_t iMem =0; iMem < memManagers.size();iMem++)
    {
      ret |= parentFactory->AddMemoryManager(memManagersNode,
					     PC,
					     memManagers[iMem][0],
					     memManagers[iMem][1]);
    }

  //Create the launch response...response check
  xmlNode * launchResponseNode = xmlNewChild(opBlockNode,NULL,(xmlChar*) "RESPONSE",NULL);
  xmlNewTextChild(launchResponseNode,NULL,(xmlChar*) "TYPE",(xmlChar*) "LAUNCH_RESPONSE");
  xmlNode * responseDataNode = xmlNewChild(launchResponseNode,NULL,(xmlChar*) "DATA",NULL);
  xmlNewTextChild(responseDataNode,NULL,(xmlChar*) "WORD32",(xmlChar*) "0x0");
  xmlNewTextChild(responseDataNode,NULL,(xmlChar*) "WORD32",(xmlChar*) "0x0");				       
  


  return ret;
}

