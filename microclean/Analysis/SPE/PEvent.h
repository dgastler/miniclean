#ifndef ROOT_PEvent
#define ROOT_PEvent
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Event                                                                //
//                                                                      //
// Description of the event and track parameters                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>

class PEvent : public TObject {
  
 private:
  
  Bool_t         fIsValid;  

  Bool_t         AbvTHRS;

  Int_t          iEventNumber;
  Int_t          SizefFast;
  Int_t          SizefSlow;
  Int_t          SizeSPE;
  Int_t          SizeTOF;
  Int_t          SizeShift;
  Int_t          SizeDelay;
  Int_t          SizeMaxHeight;

  Float_t*       fSPE; //[SizeSPE]
  Float_t*       ffFast; //[SizefFast]
  Float_t*       ffSlow; //[SizefSlow]
  Float_t*       fTOF; //[SizeTOF]
  Float_t*       fShift; //[SizeShift]
  Float_t*       fDelay;//[SizeDelay]
  Float_t*       fMaxHeight;//[SizeMaxHeight]


 public:
  PEvent();
  virtual        ~PEvent();


  void           Build(Float_t * Array1,Float_t* Array2, Float_t* Array3,Float_t* Array4, Float_t* Array5,Float_t* Array6,Float_t* Array7,Int_t Size);
  

  void           SPE  (Int_t i,Float_t value){if(i<SizeSPE){fSPE[i] = value;}}
  Float_t        SPE  (Int_t i) {if(i < SizeSPE){return(fSPE[i]);}return(0);}
  void           fFast(Int_t i,Float_t value){if(i<SizefFast){ffFast[i] = value;}}
  Float_t        fFast(Int_t i) {if(i < SizefFast){return(ffFast[i]);}return(0);}
  void           fSlow(Int_t i,Float_t value){if(i<SizefSlow){ffSlow[i] = value;}}
  Float_t        fSlow(Int_t i) {if(i < SizefSlow){return(ffSlow[i]);}return(0);}
  void           TOF  (Int_t i,Float_t value){if(i<SizeTOF){fTOF[i] = value;}}
  Float_t        TOF  (Int_t i) {if(i < SizeTOF){return(fTOF[i]);}return(0);}
  void           Shift(Int_t i,Float_t value){if(i<SizeShift){fShift[i] = value;}}
  Float_t        Shift(Int_t i) {if(i < SizeShift){return(fShift[i]);}return(0);}
  void           Delay(Int_t i,Float_t value){if(i<SizeShift){fDelay[i] = value;}}
  Float_t        Delay(Int_t i) {if(i < SizeShift){return(fDelay[i]);}return(0);}
  void           MaxHeight(Int_t i,Float_t value){if(i<SizeMaxHeight){fMaxHeight[i] = value;}}
  Float_t        MaxHeight(Int_t i) {if(i < SizeMaxHeight){return(fMaxHeight[i]);}return(0);}
  void           EventNumber(Int_t value){iEventNumber = value;}
  Int_t          EventNumber() {return(iEventNumber);}

  void           AboveTHRS(Bool_t value){AbvTHRS = value;};
  Bool_t         AboveTHRS() {return(AbvTHRS);}

  Int_t          Size(){return(SizeTOF);}

  
  //Int_t          test() {return(NumberOfChannels);}
  //Float_t          test2() {return(fSPE[0]);}
  void           Clear(Option_t *option);
  Bool_t         IsValid() const { return fIsValid; }

  ClassDef(PEvent,1)  //PEvent structure
};


#endif
