#include <EventMonitor.h>

EventMonitor::EventMonitor()
{
  lastTime = time(NULL);
  timeBetweenEvents = 10;
  queue = NULL;
}
EventMonitor::~EventMonitor()
{
  if(queue != NULL)
    {
      queue->Shutdown();
      queue=NULL;
    }
};
void EventMonitor::Setup(RATCopyQueue * _queue)
{
  if(_queue != NULL)
    {
      queue = _queue;
    }
}
void EventMonitor::operator() (RATEVBlock * event)
{
  if((queue != NULL)&&(event != NULL))
    {
      ProcessEvent(event->eventID,
		   event->eventTime,
		   event->eventTriggers,
		   event->reductionLevel,
		   event->ev);
    }
}
void EventMonitor::operator() (RATDSBlock * event)
{
  if((queue != NULL)&&(event != NULL))
    {
      ProcessEvent(event->eventID,
		   event->eventTime,
		   event->eventTriggers,
		   event->reductionLevel,
		   event->ds->GetEV(0));
    }
}

void EventMonitor::ProcessEvent(int64_t eventID,
				uint64_t eventTime,
				uint32_t eventTriggers,
				uint8_t reductionLevel,
				RAT::DS::EV * ev)
{
  if(ev != NULL)
    {
      /*      time_t currentTime = time(NULL);
      if(difftime(currentTime,lastTime) > timeBetweenEvents)
	{
	  RAT::DS::EV * copy = new RAT::DS::EV(*ev);
	  queue->Add(copy);
	  lastTime=currentTime;
	  }*/
    }
}
