//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//Class includes
#include "PEvent.h"
#include "TMath.h"
#include "Cut_Parameters.h"
#include "TextArray.h"

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TLine.h"
#include "TProfile.h"


//Notes
/*
April 22 
   I removed all the dependence of the weights and voltages from the Cuts file
   I need to make up for this with the new data in the run notes. 
*/





using namespace std;





int main(int argc, char ** argv)
{
  if(argc < 4)
    {
      cout << argv[0] << " inFile outfile cutfile" << endl;
      return(0);
    }
  Int_t  Window = 0;
  if(argc > 4)
    {
      Window = atoi(argv[4]);
    }
  //  cout << argv[1] << endl;
  Float_t Volts = (1.0/255.0);
  Float_t Seconds = 2E-9;
  Float_t C = Volts * Seconds * (1.0/50.0);
  Float_t EnergyUnits = 1;


  TFile *InFile = new TFile(argv[1]);
  cout << "Opening file: " << argv[1] << endl;
  TTree *ptree  = (TTree*) InFile->Get("ptree");
  TTree *TextTree = (TTree*) InFile->Get("TextTree");
  Int_t NEvents = ptree->GetEntries();
  PEvent *pevent = 0;
  TextArray *RootText = 0;

  Int_t CNEvents = 0;

  //  ptree->Print();
  ptree->SetBranchAddress("PEvent branch",&pevent);
  TextTree->SetBranchAddress("Run Notes",&RootText);
  TextTree->GetEntry(1);
  char * TempPointer = RootText->GetArray();
  //  cout << TempPointer << endl;


  TH1F * TotalCharge = (TH1F*) InFile->Get("TotalCharge");
  //  TH1F * PMT0Charge  = (TH1F*) InFile->Get("PMT0Charge");  //Not Used
  //  TH1F * PMT1Charge  = (TH1F*) InFile->Get("PMT1Charge");  //Not Used
  //  TH1F * PMTasym = InFile->Get("PMT0Charge");
  //  TH1F * PMTSPEs     = (TH1F*) InFile->Get("PMTSPEs");
  
  
  Float_t * SPEs = new Float_t[2];
//  SPEs[0] = PMTSPEs->GetBinContent(1);
//  SPEs[1] = PMTSPEs->GetBinContent(2);

  SPEs[0] = ( (TF1 *) ( (TH1F *) InFile->Get("SPE0"))->GetListOfFunctions()->First())->GetParameter(1);
  SPEs[1] = ( (TF1 *) ( (TH1F *) InFile->Get("SPE1"))->GetListOfFunctions()->First())->GetParameter(1);

  XMLNode xMainNode=XMLNode::openFileHelper(argv[3],"PMML");
  XMLNode DAQNode=XMLNode::parseString(TempPointer,"DAQ");
  Cut_Parameters * Cuts = new Cut_Parameters;
  Cuts->SetCuts(&xMainNode,Window,SPEs,2);
  //All Cuts Loaded!
  cout << "All Cuts Loaded!" << endl;


  // Check if the Energy Callibration is complete and use it if it is
  // Generate it if it isn't.
    //Open SPE.xml file and parse it.
  Bool_t Calibration = false;
  XMLResults CalFile;
  XMLNode CalNode=XMLNode::parseFile("Cal.xml","CAL",&CalFile);
  //Check if it failed and exit it on failure 
  if(CalFile.error != eXMLErrorNone)
    {
      cout << "No energy callibration file found.  All results wil be in pe" << endl;
      Calibration = false;
    }
  else
    {
      EnergyUnits = 1.0/atof(CalNode.getAttribute("keVee"));
      Calibration = true;
      cout << "Energy callibration file found.  Using " << EnergyUnits << "pe/keVee" << endl;
    }




  Float_t LightLimit = 0;//600000*Volts*Seconds;
  for(int i = TotalCharge->GetMaximumBin();i < TotalCharge->GetNbinsX();i++)
    {
      if(TotalCharge->GetBinContent(i)/TotalCharge->GetMaximum() < 0.01)
	{
	  LightLimit = TotalCharge->GetBinCenter(i)/(0.5*SPEs[0] + 0.5*SPEs[1]);
	  break;
	}
    }
  LightLimit = 2000;

  LightLimit *= EnergyUnits;
  
  
  
  //Raw Histograms
  TH2F * rHeightvsLight = new TH2F("rHeightvsLight","Raw summed height vs. light",100,0,10,500,0,LightLimit);
  TH2F * rLightvsRatio   = new TH2F("rLightvsRatio","Raw Energy vs. Fast/Slow",100,0,LightLimit,100,0,3.0);
  TH2F * rLightvsfFast   = new TH2F("rLightvsfFast","Raw Energy vs. Fast",500,0,LightLimit,100,0,1.0);
  TH2F * rLightvsTOF    = new TH2F("rLightvsTOF","Raw Energy vs. TOF",500,0,LightLimit,402,-100*Seconds,100*Seconds);
  TH2F * rLightvsPMT2    = new TH2F("rLightvsPMT2","Raw Energy vs. PMT2",500,0,LightLimit,100,0,5000*C);
  TH2F * rLightvsOsci    = new TH2F("rLightvsOsci","Raw Energy vs. OSCi",500,0,LightLimit,1000,0,20E9);
  TH1F * rRatio          = new TH1F("rRatio","Raw Fast/Slow",50,0,5);
  TH1F * rLight          = new TH1F("rLight","Raw Energy",500,0,LightLimit);
  TH1F * rfFast          = new TH1F("rfFast","Raw fFast",100,0,1);
  TH1F * rfSlow          = new TH1F("rfSlow","Raw fSlow",100,0,1);
  TH1F * rTB             = new TH1F("rTB","Raw Top bottom difference",500,-1,1);
  
  TH1F * rPMT0TIME       = new TH1F("rPMT0TIME","Raw PMT0 start time",500,0*Seconds,13000*Seconds);
  TH1F * rPMT1TIME       = new TH1F("rPMT1TIME","Raw PMT1 start time",500,0*Seconds,13000*Seconds);
  TH1F * rPMT2TIME       = new TH1F("rPMT2TIME","Raw PMT2 start time",500,0*Seconds,13000*Seconds);
  TH1F * rTOF            = new TH1F("rTOF","Raw time of flight",402,-100*Seconds,100*Seconds);
  
  TH2F * rINTvsMHPMT0    = new TH2F("rINTvsMHPMT0","Raw Integrated waveform VS. max height PMT0",1000,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * rINTvsMHPMT1    = new TH2F("rINTvsMHPMT1","Raw Integrated waveform VS. max height PMT1",1000,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * rINTvsMHPMT2    = new TH2F("rINTvsMHPMT2","Raw Integrated waveform VS. max height PMT2",1000,0,5000*C,200,0*Volts,200*Volts);
  TH1F * rOsciPSD        = new TH1F("rOsciPSD","Organic Scintillator",1000,0,20E9);
  TH2F * rPMT2IntvsTOF   = new TH2F("rPMT2IntvsTOF","PMT2 Integral versus TOF",1000,0,5000*C,402,-100*Seconds,100*Seconds);

  TH1F * rMaxHeight0     = new TH1F("rMaxHeight0","Raw MaxHeights0",100,0,1000*Volts);
  TH1F * rMaxHeight1     = new TH1F("rMaxHeight1","Raw MaxHeights1",100,0,1000*Volts);
  TH1F * rMaxHeight2     = new TH1F("rMaxHeight2","Raw MaxHeights2",100,0,255*Volts);

  TH1F * rPMT0Light      = new TH1F("rPMT0Light","Raw Light in PMT0",100,0,LightLimit);
  TH1F * rPMT1Light      = new TH1F("rPMT1Light","Raw Light in PMT1",100,0,LightLimit);
  TH1F * rPMT2Light      = new TH1F("rPMT2Light","Raw Light in PMT2",100,0,5000*C);

  char * TempBuffer = new char[100];
  sprintf(TempBuffer,"Raw Light in PMT0 in Charge. V=%s",DAQNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",0).getAttribute("PMTVoltage"));
  TH1F * rPMT0LightCharge      = new TH1F("rPMT0LightCharge",TempBuffer,100,0,LightLimit*SPEs[0]);
  sprintf(TempBuffer,"Raw Light in PMT1 in Charge. V=%s",DAQNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",2).getAttribute("PMTVoltage"));
  TH1F * rPMT1LightCharge      = new TH1F("rPMT1LightCharge",TempBuffer,100,0,LightLimit*SPEs[1]);



  // Histograms with Cuts applied. 
  TH2F * cHeightvsLight = new TH2F("cHeightvsLight","Summed height vs. light",100,0,10,500,0,LightLimit);
  TH2F * cLightvsRatio   = new TH2F("cLightvsRatio"," Energy vs. Fast/Slow",100,0,LightLimit,100,0,3.0);
  TH2F * cLightvsfFast   = new TH2F("cLightvsfFast"," Energy vs. Fast",500,0,LightLimit,100,0,1.0);
  TH2F * cLightvsTOF    = new TH2F("cLightvsTOF","Energy vs. TOF",500,0,LightLimit,402,-100*Seconds,100*Seconds);
  TH2F * cLightvsPMT2    = new TH2F("cLightvsPMT2","Energy vs. PMT2",500,0,LightLimit,100,0,5000*C);
  TH2F * cLightvsOsci    = new TH2F("cLightvsOsci","Energy vs. OSCi",500,0,LightLimit,1000,0,20E9);
  TH1F * cRatio          = new TH1F("cRatio"," Fast/Slow",50,0,5);
  TH1F * cLight          = new TH1F("cLight"," Energy",500,0,LightLimit);
  TH1F * cfFast          = new TH1F("cfFast"," fFast",100,0,1);
  TH1F * cfSlow          = new TH1F("cfSlow"," fSlow",100,0,1);
  TH1F * cTB             = new TH1F("cTB"," Top bottom difference",500,-1,1);
				     
  TH1F * cPMT0TIME       = new TH1F("cPMT0TIME"," PMT0 start time",500,0*Seconds,13000*Seconds);
  TH1F * cPMT1TIME       = new TH1F("cPMT1TIME"," PMT1 start time",500,0*Seconds,13000*Seconds);
  TH1F * cPMT2TIME       = new TH1F("cPMT2TIME"," PMT2 start time",500,0*Seconds,13000*Seconds);
  TH1F * cTOF            = new TH1F("cTOF"," time of flight",402,-100*Seconds,100*Seconds);
				     
  TH2F * cINTvsMHPMT0    = new TH2F("cINTvsMHPMT0"," Integrated waveform VS. max height PMT0",1000,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * cINTvsMHPMT1    = new TH2F("cINTvsMHPMT1"," Integrated waveform VS. max height PMT1",1000,0,LightLimit,1000,0*Volts,2000*Volts);
  TH2F * cINTvsMHPMT2    = new TH2F("cINTvsMHPMT2"," Integrated waveform VS. max height PMT2",1000,0,5000*C,200,0*Volts,200*Volts);
  TH1F * cOsciPSD        = new TH1F("cOsciPSD","Organic Scintillator",1000,0,20E9);
  TH2F * cPMT2IntvsTOF   = new TH2F("cPMT2IntvsTOF","PMT2 Integral versus TOF",1000,0,5000*C,402,-100*Seconds,100*Seconds);
				     
  TH1F * cMaxHeight0     = new TH1F("cMaxHeight0"," MaxHeights0",100,0,1000*Volts);
  TH1F * cMaxHeight1     = new TH1F("cMaxHeight1"," MaxHeights1",100,0,1000*Volts);
  TH1F * cMaxHeight2     = new TH1F("cMaxHeight2"," MaxHeights2",100,0,255*Volts);
				     
  TH1F * cPMT0Light      = new TH1F("cPMT0Light"," Light in PMT0",100,0,LightLimit);
  TH1F * cPMT1Light      = new TH1F("cPMT1Light"," Light in PMT1",100,0,LightLimit);
  TH1F * cPMT2Light      = new TH1F("cPMT2Light"," Light in PMT2",100,0,5000*C);


  
  


  // Make Ed and Jen stop bugging me about labels
  if(Calibration)
    {
      rHeightvsLight->GetXaxis()->SetTitle("Summed heights (V)");
      rHeightvsLight->GetXaxis()->SetTitle("Energy (keVee)");
      rLightvsRatio->GetXaxis()->SetTitle("Light (keVee)");
      rLightvsfFast->GetXaxis()->SetTitle("Light (keVee)");
      rRatio->GetXaxis()->SetTitle("Prompt Light/Slow Light");
      rLight->GetXaxis()->SetTitle("Light (keVee)");
      rfFast->GetXaxis()->SetTitle("Prompt Light/Total Light");
      rfSlow->GetXaxis()->SetTitle("Slow Light/Total Light");
      rTB->GetXaxis()->SetTitle("(PMT0 - PMT1)/(PMT0 + PMT1)");          
      rPMT0TIME->GetXaxis()->SetTitle("Waveform start (s)");
      rPMT1TIME->GetXaxis()->SetTitle("Waveform start (s)");
      rPMT2TIME->GetXaxis()->SetTitle("Waveform start (s)");
      rTOF->GetXaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");           
      rINTvsMHPMT0->GetXaxis()->SetTitle("Light (keVee)");
      rINTvsMHPMT1->GetXaxis()->SetTitle("Light (keVee)");
      rINTvsMHPMT2->GetXaxis()->SetTitle("Charge (C)");                     
      rMaxHeight0->GetXaxis()->SetTitle("Max Waveform Height (V)");
      rMaxHeight1->GetXaxis()->SetTitle("Max Waveform Height (V)");
      rMaxHeight2->GetXaxis()->SetTitle("Max Waveform Height (V)");
      rPMT0Light->GetXaxis()->SetTitle("Light (keVee)");
      rPMT1Light->GetXaxis()->SetTitle("Light (keVee)");
      rPMT2Light->GetXaxis()->SetTitle("Light (C)");
      rPMT0LightCharge->GetXaxis()->SetTitle("Change (C)");
      rPMT1LightCharge->GetXaxis()->SetTitle("Charge (C)"); 

      rLightvsRatio->GetYaxis()->SetTitle("Prompt Light/Slow Light");
      rLightvsfFast->GetYaxis()->SetTitle("Prompt Light/Total Light");
      rOsciPSD->GetXaxis()->SetTitle("PMT2 Max Waveform Height/Integral (V/C)");
      rPMT2IntvsTOF->GetXaxis()->SetTitle("PMT2 Integral (C)");
      rPMT2IntvsTOF->GetYaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");
      

      cHeightvsLight->GetXaxis()->SetTitle("Summed heights (V)");
      cHeightvsLight->GetXaxis()->SetTitle("Energy (keVee)");
      cLightvsRatio->GetXaxis()->SetTitle("Light (keVee)");
      cLightvsfFast->GetXaxis()->SetTitle("Light (keVee)");
      cRatio->GetXaxis()->SetTitle("Prompt Light/Slow Light");
      cLight->GetXaxis()->SetTitle("Light (keVee)");
      cfFast->GetXaxis()->SetTitle("Prompt Light/Total Light");
      cfSlow->GetXaxis()->SetTitle("Slow Light/Total Light");
      cTB->GetXaxis()->SetTitle("(PMT0 - PMT1)/(PMT0 + PMT1)");          
      cPMT0TIME->GetXaxis()->SetTitle("Waveform start (s)");
      cPMT1TIME->GetXaxis()->SetTitle("Waveform start (s)");
      cPMT2TIME->GetXaxis()->SetTitle("Waveform start (s)");
      cTOF->GetXaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");           
      cINTvsMHPMT0->GetXaxis()->SetTitle("Light (keVee)");
      cINTvsMHPMT1->GetXaxis()->SetTitle("Light (keVee)");
      cINTvsMHPMT2->GetXaxis()->SetTitle("Charge (C)");                     
      cMaxHeight0->GetXaxis()->SetTitle("Max Waveform Height (V)");
      cMaxHeight1->GetXaxis()->SetTitle("Max Waveform Height (V)");
      cMaxHeight2->GetXaxis()->SetTitle("Max Waveform Height (V)");
      cPMT0Light->GetXaxis()->SetTitle("Light (keVee)");
      cPMT1Light->GetXaxis()->SetTitle("Light (keVee)");
      cPMT2Light->GetXaxis()->SetTitle("Light (C)");
      
      cLightvsRatio->GetYaxis()->SetTitle("Prompt Light/Slow Light");
      cLightvsfFast->GetYaxis()->SetTitle("Prompt Light/Total Light");
      cOsciPSD->GetXaxis()->SetTitle("PMT2 Max Waveform Height/Integral (V/C)");
      cPMT2IntvsTOF->GetXaxis()->SetTitle("PMT2 Integral (C)");
      cPMT2IntvsTOF->GetYaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");
    }
  else
    {
      rHeightvsLight->GetXaxis()->SetTitle("Summed heights (V)");
      rHeightvsLight->GetXaxis()->SetTitle("Light (pe)");
      rLightvsRatio->GetXaxis()->SetTitle("Light (pe)");
      rLightvsfFast->GetXaxis()->SetTitle("Light (pe)");
      rRatio->GetXaxis()->SetTitle("Prompt Light/Slow Light");
      rLight->GetXaxis()->SetTitle("Light (pe)");
      rfFast->GetXaxis()->SetTitle("Prompt Light/Total Light");
      rfSlow->GetXaxis()->SetTitle("Slow Light/Total Light");
      rTB->GetXaxis()->SetTitle("(PMT0 - PMT1)/(PMT0 + PMT1)");          
      rPMT0TIME->GetXaxis()->SetTitle("Waveform start (s)");
      rPMT1TIME->GetXaxis()->SetTitle("Waveform start (s)");
      rPMT2TIME->GetXaxis()->SetTitle("Waveform start (s)");
      rTOF->GetXaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");           
      rINTvsMHPMT0->GetXaxis()->SetTitle("Light (pe)");
      rINTvsMHPMT1->GetXaxis()->SetTitle("Light (pe)");
      rINTvsMHPMT2->GetXaxis()->SetTitle("Light (pe)");                     
      rMaxHeight0->GetXaxis()->SetTitle("Max Waveform Height (V)");
      rMaxHeight1->GetXaxis()->SetTitle("Max Waveform Height (V)");
      rMaxHeight2->GetXaxis()->SetTitle("Max Waveform Height (V)");
      rPMT0Light->GetXaxis()->SetTitle("Light (pe)");
      rPMT1Light->GetXaxis()->SetTitle("Light (pe)");
      rPMT2Light->GetXaxis()->SetTitle("Light (C)");
      rPMT0LightCharge->GetXaxis()->SetTitle("Change (C)");
      rPMT1LightCharge->GetXaxis()->SetTitle("Charge (C)"); 

      rLightvsRatio->GetYaxis()->SetTitle("Prompt Light/Slow Light");
      rLightvsfFast->GetYaxis()->SetTitle("Prompt Light/Total Light");
      rOsciPSD->GetXaxis()->SetTitle("PMT2 Max Waveform Height/Integral (V/C)");
      rPMT2IntvsTOF->GetXaxis()->SetTitle("PMT2 Integral (C)");
      rPMT2IntvsTOF->GetYaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");

      cHeightvsLight->GetXaxis()->SetTitle("Summed heights (V)");
      cHeightvsLight->GetXaxis()->SetTitle("Light (pe)");
      cLightvsRatio->GetXaxis()->SetTitle("Light (pe)");
      cLightvsfFast->GetXaxis()->SetTitle("Light (pe)");
      cRatio->GetXaxis()->SetTitle("Prompt Light/Slow Light");
      cLight->GetXaxis()->SetTitle("Light (pe)");
      cfFast->GetXaxis()->SetTitle("Prompt Light/Total Light");
      cfSlow->GetXaxis()->SetTitle("Slow Light/Total Light");
      cTB->GetXaxis()->SetTitle("(PMT0 - PMT1)/(PMT0 + PMT1)");          
      cPMT0TIME->GetXaxis()->SetTitle("Waveform start (s)");
      cPMT1TIME->GetXaxis()->SetTitle("Waveform start (s)");
      cPMT2TIME->GetXaxis()->SetTitle("Waveform start (s)");
      cTOF->GetXaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");           
      cINTvsMHPMT0->GetXaxis()->SetTitle("Light (pe)");
      cINTvsMHPMT1->GetXaxis()->SetTitle("Light (pe)");
      cINTvsMHPMT2->GetXaxis()->SetTitle("Light (pe)");                     
      cMaxHeight0->GetXaxis()->SetTitle("Max Waveform Height (V)");
      cMaxHeight1->GetXaxis()->SetTitle("Max Waveform Height (V)");
      cMaxHeight2->GetXaxis()->SetTitle("Max Waveform Height (V)");
      cPMT0Light->GetXaxis()->SetTitle("Light (pe)");
      cPMT1Light->GetXaxis()->SetTitle("Light (pe)");
      cPMT2Light->GetXaxis()->SetTitle("Light (C)");
      
      cLightvsRatio->GetYaxis()->SetTitle("Prompt Light/Slow Light");
      cLightvsfFast->GetYaxis()->SetTitle("Prompt Light/Total Light");
      cOsciPSD->GetXaxis()->SetTitle("PMT2 Max Waveform Height/Integral (V/C)");
      cPMT2IntvsTOF->GetXaxis()->SetTitle("PMT2 Integral (C)");
      cPMT2IntvsTOF->GetYaxis()->SetTitle("PMT2 - (PMT0 + PMT1)/2 (s)");
    }


  for(int i = 0; i < NEvents ; i++)
    {
      ptree->GetEntry(i);
      Float_t FastLight = ((pevent->Fast(0,Window)   )/SPEs[0]  +
			   (pevent->Fast(1,Window)    )/SPEs[1]);
      Float_t SlowLight = ((pevent->Slow(0,Window))/SPEs[0] + (pevent->Slow(1,Window))/SPEs[1]);
      Float_t PMT0Int = (pevent->Fast(0,Window) + pevent->Slow(0,Window))/SPEs[0];
      Float_t PMT1Int = (pevent->Fast(1,Window) + pevent->Slow(1,Window))/SPEs[1];
      FastLight *= EnergyUnits;
      SlowLight *= EnergyUnits;
      PMT0Int   *= EnergyUnits;
      PMT1Int   *= EnergyUnits;
      Float_t PMT0Light = PMT0Int;
      Float_t PMT1Light = PMT1Int;
      Float_t TotalLight = FastLight + SlowLight;
      
      if(Cuts->PassCuts(pevent))
      //      if(TotalLight > 60 && TotalLight < 100 && Cuts->PassCuts(pevent))
	{
	  CNEvents++;

	  cLightvsRatio->Fill(TotalLight,FastLight/SlowLight);
	  cLightvsfFast->Fill(TotalLight,FastLight/TotalLight);
	  
	  cRatio->Fill(FastLight/SlowLight);
	  cLight->Fill(TotalLight);
	  cfFast->Fill(FastLight/TotalLight);
	  cfSlow->Fill(SlowLight/TotalLight);
	  cTB->Fill((PMT0Light - PMT1Light)/TotalLight);
	  
	  cPMT0TIME->Fill(pevent->Delay(0)*Seconds);
	  cPMT1TIME->Fill(pevent->Delay(1)*Seconds);
	  
	  cINTvsMHPMT0->Fill(PMT0Int,pevent->MaxHeight(0)*Volts);
	  cINTvsMHPMT1->Fill(PMT1Int,pevent->MaxHeight(1)*Volts);
	  
	  cMaxHeight0->Fill(pevent->MaxHeight(0)*Volts);
	  cMaxHeight1->Fill(pevent->MaxHeight(1)*Volts);
	  
	  cPMT0Light->Fill(PMT0Light);
	  cPMT1Light->Fill(PMT1Light);
	
	  cHeightvsLight->Fill((pevent->MaxHeight(0)+pevent->MaxHeight(1))*Volts,TotalLight);
  
	  if(DAQNode.getChildNode("Run").getAttribute("Type")[0] != 'c' &&
	     DAQNode.getChildNode("Run").getAttribute("Type")[0] != 'C'
	     )
	    {
	      cOsciPSD->Fill(pevent->MaxHeight(2)*Volts/pevent->Fast(2,0));
	      cPMT2TIME->Fill(pevent->Delay(2)*Seconds);
	      cTOF->Fill((pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	      //cINTvsMHPMT2->Fill(pevent->Fast(2,0)+pevent->Slow(2,0),pevent->MaxHeight(2)*Volts);
	      cINTvsMHPMT2->Fill(pevent->Fast(2,0),pevent->MaxHeight(2)*Volts);
	      cPMT2Light->Fill((pevent->Fast(2,0) + pevent->Slow(2,0)));
	      cMaxHeight2->Fill(pevent->MaxHeight(2)*Volts);
	      cPMT2IntvsTOF->Fill(pevent->Fast(2,0) + pevent->Slow(2,0),(pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	      cLightvsTOF->Fill(TotalLight,(pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	      cLightvsPMT2->Fill(TotalLight,(pevent->Fast(2,0) + pevent->Slow(2,0)));
	      cLightvsOsci->Fill(TotalLight,pevent->MaxHeight(2)*Volts/pevent->Fast(2,0));
	    }
	  else
	    {
	      cTOF->Fill((pevent->Delay(0) - pevent->Delay(1))*Seconds);
	    }
	}
      
      rLightvsRatio->Fill(TotalLight,FastLight/SlowLight);
      rLightvsfFast->Fill(TotalLight,FastLight/TotalLight);
      
      rRatio->Fill(FastLight/SlowLight);
      rLight->Fill(TotalLight);
      rfFast->Fill(FastLight/TotalLight);
      rfSlow->Fill(SlowLight/TotalLight);
      rTB->Fill((PMT0Light - PMT1Light)/TotalLight);
      
      rPMT0TIME->Fill(pevent->Delay(0)*Seconds);
      rPMT1TIME->Fill(pevent->Delay(1)*Seconds);
      
      rINTvsMHPMT0->Fill(PMT0Int,pevent->MaxHeight(0)*Volts);
      rINTvsMHPMT1->Fill(PMT1Int,pevent->MaxHeight(1)*Volts);
      
      rMaxHeight0->Fill(pevent->MaxHeight(0)*Volts);
      rMaxHeight1->Fill(pevent->MaxHeight(1)*Volts);
      
      rPMT0Light->Fill(PMT0Light);
      rPMT1Light->Fill(PMT1Light);

      rPMT0LightCharge->Fill(PMT0Light*SPEs[0]);
      rPMT1LightCharge->Fill(PMT1Light*SPEs[1]);

      rHeightvsLight->Fill((pevent->MaxHeight(0)+pevent->MaxHeight(1))*Volts,TotalLight);
      
      if((DAQNode.getChildNode("Run").getAttribute("Type")[0] != 'c') &&
	 DAQNode.getChildNode("Run").getAttribute("Type")[0] != 'C')
	{
	  rOsciPSD->Fill(pevent->MaxHeight(2)*Volts/pevent->Fast(2,0));
	  rPMT2TIME->Fill(pevent->Delay(2)*Seconds);
	  rTOF->Fill((pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	  //rINTvsMHPMT2->Fill(pevent->Fast(2,0)+pevent->Slow(2,0),pevent->MaxHeight(2)*Volts);
	  rINTvsMHPMT2->Fill(pevent->Fast(2,0),pevent->MaxHeight(2)*Volts);
	  rPMT2Light->Fill((pevent->Fast(2,0) + pevent->Slow(2,0)));
	  rMaxHeight2->Fill(pevent->MaxHeight(2)*Volts);
	  rPMT2IntvsTOF->Fill(pevent->Fast(2,0) + pevent->Slow(2,0),(pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	  rLightvsTOF->Fill(TotalLight,(pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0)*Seconds);
	  rLightvsPMT2->Fill(TotalLight,(pevent->Fast(2,0) + pevent->Slow(2,0)));
	  rLightvsOsci->Fill(TotalLight,pevent->MaxHeight(2)*Volts/pevent->Fast(2,0));
	}
      else
	{
	  rTOF->Fill((pevent->Delay(0) - pevent->Delay(1))*Seconds);
	}
    }


  TF1 * TBFit = new TF1("TBFit","gaus",-1,1);
  TBFit->SetParameter(0,cTB->GetMaximum());
  TBFit->SetParameter(1,cTB->GetBinCenter(cTB->GetMaximumBin()));
  TBFit->SetParLimits(2,0,1);
  TBFit->SetParameter(2,.2);
  cTB->Fit(TBFit,"BRQ");
  //  cout << "asdf " << TBFit->GetParameter(1) << " " << TBFit->GetParameter(2) << endl;
  cTB->Fit(TBFit,"BQ","",TBFit->GetParameter(1) - TBFit->GetParameter(2),TBFit->GetParameter(1) + TBFit->GetParameter(2));
  //  cout << "TBCut " << TBFit->GetParameter(1) << endl;
















  //Setup neutron specific cuts. 

  if((DAQNode.getChildNode("Run").getAttribute("Type")[0] == 'n') ||
     DAQNode.getChildNode("Run").getAttribute("Type")[0] == 'N')
    {
      TF1 * TOFfit = new TF1("TOFfit","gaus",0,100E-9);
      TOFfit->SetParameter(0,cTOF->GetMaximum());
      TOFfit->SetParameter(1,cTOF->GetBinCenter(cTOF->GetMaximumBin()));
      TOFfit->SetParLimits(1,0,100E-9);
      TOFfit->SetParameter(2,10E-9);
      TOFfit->SetParLimits(2,0,30E-9);
      cTOF->Fit(TOFfit,"BQR","",cTOF->GetBinCenter(cTOF->GetMaximumBin())*0.7,cTOF->GetBinCenter(cTOF->GetMaximumBin())*1.3);
      cTOF->Fit(TOFfit,"BQR","",TOFfit->GetParameter(1) - TOFfit->GetParameter(2),TOFfit->GetParameter(1) + TOFfit->GetParameter(2));

      TF1 * fFastFit = new TF1("fFastFit","gaus",0.5,1);
      fFastFit->SetParameter(0,cfFast->GetMaximum());
      fFastFit->SetParameter(1,cfFast->GetBinCenter(cfFast->GetMaximumBin()));
      fFastFit->SetParLimits(1,0.6,0.9);
      fFastFit->SetParLimits(2,0,0.2);
      
      cfFast->Fit(fFastFit,"RQB");
      cfFast->Fit(fFastFit,"BQ","",fFastFit->GetParameter(1) - 1.0*fFastFit->GetParameter(2),fFastFit->GetParameter(1) + 1.0*fFastFit->GetParameter(2));
      cfFast->Fit(fFastFit,"BQ","",fFastFit->GetParameter(1) - fFastFit->GetParameter(2),fFastFit->GetParameter(1) + fFastFit->GetParameter(2));
      

      TF1 * PMT2Fit = new TF1("fPMT2Fit","gaus",0,1E-9);
      PMT2Fit->SetParLimits(1,0,1E-9);
      PMT2Fit->SetParLimits(2,0,1000);
      cPMT2Light->Fit(PMT2Fit,"QR");
      cPMT2Light->Fit(PMT2Fit,"Q","",PMT2Fit->GetParameter(1) - PMT2Fit->GetParameter(2),PMT2Fit->GetParameter(1) + PMT2Fit->GetParameter(2));
      cPMT2Light->GetXaxis()->SetRangeUser(0,1E-9);

      cout << "NeutronPAR " << floor(TOFfit->GetParameter(1)/(2e-9)) << " " << floor(TOFfit->GetParameter(2)/(2e-9)) << " " << fFastFit->GetParameter(1) << " " << fFastFit->GetParameter(2) << " " << TBFit->GetParameter(1) << " " << PMT2Fit->GetParameter(1) << " " << fabs(PMT2Fit->GetParameter(2));
      cLight->GetXaxis()->SetRangeUser(0,200);
    }
  else
    {
      cout << "PMTHeightCut " ; 
    }
  


  //Get a recommendation for the PMT linearity cut
  //PMT0
  TProfile * PMT0HeightFunction = rINTvsMHPMT0->RebinY(4,"rINTvsPMT0Temp")->ProfileY("PMT0HEightFunction");
  Int_t PMT0MinPoint = PMT0HeightFunction->GetNbinsX();
  Int_t PMT0MinPoint2 = 0;
  Float_t PMT0MinPointHeight = 1e9;
  Bool_t FoundMin0 = false;
  for(Int_t i = PMT0HeightFunction->GetNbinsX() - 1; i > 0;i--)
    {
      if(PMT0HeightFunction->GetBinContent(i) != 0)
	{
	  if(FoundMin0 && PMT0HeightFunction->GetBinContent(i) < PMT0MinPointHeight)
	    {
	      PMT0MinPoint2 = i;
	      //	      cout << "Found Min2 " << PMT0MinPoint2 << endl;
	      i = -1;
	      break;
	    }
	  
	  else if(PMT0HeightFunction->GetBinContent(i) > 1.1* PMT0MinPointHeight && !FoundMin0 && i + 5 < PMT0MinPoint)
	    {
	      FoundMin0 = true;
	      //      cout << "Found Min " << PMT0MinPoint << " " << PMT0MinPointHeight << endl;
	    }
	  else if(PMT0HeightFunction->GetBinContent(i) < PMT0MinPointHeight && !FoundMin0)
	    {
	      PMT0MinPoint = i;
	      PMT0MinPointHeight = PMT0HeightFunction->GetBinContent(i);
	      //	      cout << PMT0MinPoint << " " << PMT0MinPointHeight << endl;
	    }

	}
    }
  TF1 * PMT0Fit = new TF1("PMT0Fit","gaus",PMT0HeightFunction->GetBinCenter(PMT0MinPoint2),PMT0HeightFunction->GetBinCenter(PMT0MinPoint));
  PMT0Fit->SetParameter(0,PMT0HeightFunction->GetBinContent((Int_t)((PMT0MinPoint2+PMT0MinPoint)/2.0)));
  PMT0Fit->SetParameter(1,PMT0HeightFunction->GetBinCenter((Int_t)((PMT0MinPoint2+PMT0MinPoint)/2.0)));
  PMT0Fit->SetParameter(2,(PMT0HeightFunction->GetBinCenter(PMT0MinPoint2) - PMT0HeightFunction->GetBinCenter(PMT0MinPoint))/2.0);
  PMT0HeightFunction->Fit(PMT0Fit,"QRB");
  //  cout << "PMT0HeightCut " 

  //PMT1
  TProfile * PMT1HeightFunction = rINTvsMHPMT1->RebinY(4,"rINTvsPMT1Temp")->ProfileY("PMT1HEightFunction");
  Int_t PMT1MinPoint = PMT1HeightFunction->GetNbinsX();
  Int_t PMT1MinPoint2 = 0;
  Float_t PMT1MinPointHeight = 1e9;
  Bool_t FoundMin1 = false;
  for(Int_t i = PMT1HeightFunction->GetNbinsX() - 1; i > 0;i--)
    {
      if(PMT1HeightFunction->GetBinContent(i) != 0)
	{
	  if(FoundMin1 && PMT1HeightFunction->GetBinContent(i) < PMT1MinPointHeight)
	    {
	      PMT1MinPoint2 = i;
	      //	      cout << "Found Min2 " << PMT1MinPoint2 << endl;
	      i = -1;
	      break;
	    }
	  
	  else if(PMT1HeightFunction->GetBinContent(i) > 1.1* PMT1MinPointHeight && !FoundMin1 && i + 5 < PMT1MinPoint)
	    {
	      FoundMin1 = true;
	      //	      cout << "Found Min " << PMT1MinPoint << " " << PMT1MinPointHeight << endl;
	    }
	  else if(PMT1HeightFunction->GetBinContent(i) < PMT1MinPointHeight && !FoundMin1)
	    {
	      PMT1MinPoint = i;
	      PMT1MinPointHeight = PMT1HeightFunction->GetBinContent(i);
	      //	      cout << PMT1MinPoint << " " << PMT1MinPointHeight << endl;
	    }

	}
    }
  TF1 * PMT1Fit = new TF1("PMT1Fit","gaus",PMT1HeightFunction->GetBinCenter(PMT1MinPoint2),PMT1HeightFunction->GetBinCenter(PMT1MinPoint));
  PMT1Fit->SetParameter(0,PMT1HeightFunction->GetBinContent((Int_t)((PMT0MinPoint2+PMT0MinPoint)/2.0)));
  PMT1Fit->SetParameter(1,PMT1HeightFunction->GetBinCenter((Int_t)((PMT1MinPoint2+PMT1MinPoint)/2.0)));
  PMT1Fit->SetParameter(2,(PMT1HeightFunction->GetBinCenter(PMT1MinPoint2) - PMT1HeightFunction->GetBinCenter(PMT1MinPoint))/2.0);
  PMT1HeightFunction->Fit(PMT1Fit,"QRB");
  //  cout << "PMTHeightCut " <<  (PMT0HeightFunction->GetBinCenter(PMT0MinPoint2) + PMT0HeightFunction->GetBinCenter(PMT0MinPoint))/2.0 << " " <<  (PMT1HeightFunction->GetBinCenter(PMT1MinPoint2) +PMT0HeightFunction->GetBinCenter(PMT1MinPoint))/2.0 << endl;
  cout << " " << PMT0Fit->GetParameter(1) << " " << PMT1Fit->GetParameter(1) << endl;










    //Let draw our Cuts on the cut histograms.... this will be a pain.

  //INTvsMPHPMT0Line draw cut
  //  cINTvsMHPMT0->Draw();
  TLine * INTvsMHPMT0Line = new TLine(cINTvsMHPMT0->GetXaxis()->GetXmin(),
				      atof(xMainNode.getChildNode("Cuts").getChildNode("PMTLinearity1").getChildNode("Parameter").getAttribute("P")),
				      cINTvsMHPMT0->GetXaxis()->GetXmax(),
				      atof(xMainNode.getChildNode("Cuts").getChildNode("PMTLinearity1").getChildNode("Parameter").getAttribute("P")));
  INTvsMHPMT0Line->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("PMTLinearity1").getAttribute("use")) == 1)
    {
      cINTvsMHPMT0->GetListOfFunctions()->AddLast(INTvsMHPMT0Line);
      rINTvsMHPMT0->GetListOfFunctions()->AddLast(INTvsMHPMT0Line->Clone());
    }
  
  //INTvsMPHPM1Line draw cut
  //  cINTvsMHPMT1->Draw();
  TLine * INTvsMHPMT1Line = new TLine(cINTvsMHPMT1->GetXaxis()->GetXmin(),
				      atof(xMainNode.getChildNode("Cuts").getChildNode("PMTLinearity2").getChildNode("Parameter").getAttribute("P")),
				      cINTvsMHPMT1->GetXaxis()->GetXmax(),
				      atof(xMainNode.getChildNode("Cuts").getChildNode("PMTLinearity2").getChildNode("Parameter").getAttribute("P")));
  INTvsMHPMT1Line->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("PMTLinearity2").getAttribute("use")) == 1)
    {
      cINTvsMHPMT1->GetListOfFunctions()->AddLast(INTvsMHPMT1Line);
      rINTvsMHPMT1->GetListOfFunctions()->AddLast(INTvsMHPMT1Line->Clone());
    }
  
  //cfFast draw cuts
  //  cfFast->Draw();
  TLine * fFastLine1 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("FastMin").getChildNode("Parameter").getAttribute("P")),
				 cfFast->GetYaxis()->GetXmin(),
				 atof(xMainNode.getChildNode("Cuts").getChildNode("FastMin").getChildNode("Parameter").getAttribute("P")),
				 cfFast->GetMaximum()*1.05);
  TLine * fFastLine2 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("FastMax").getChildNode("Parameter").getAttribute("P")),
				 cfFast->GetYaxis()->GetXmin(),
				 atof(xMainNode.getChildNode("Cuts").getChildNode("FastMax").getChildNode("Parameter").getAttribute("P")),
				 cfFast->GetMaximum()*1.05);
  fFastLine1->SetLineColor(kRed);
  fFastLine2->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("FastMin").getAttribute("use")) == 1)
    {
      cfFast->GetListOfFunctions()->AddLast(fFastLine1);
      rfFast->GetListOfFunctions()->AddLast(fFastLine1->Clone());
    }
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("FastMax").getAttribute("use")) == 1)
    {
      cfFast->GetListOfFunctions()->AddLast(fFastLine2);
      rfFast->GetListOfFunctions()->AddLast(fFastLine2->Clone());
    }
  
  //cLightvsfFast draw cuts
  //  cfFast->Draw();
  TLine * LightvsfFastLine1 = new TLine(cLightvsfFast->GetXaxis()->GetXmin(),
					atof(xMainNode.getChildNode("Cuts").getChildNode("FastMin").getChildNode("Parameter").getAttribute("P")),
					cLightvsfFast->GetXaxis()->GetXmax(),
					atof(xMainNode.getChildNode("Cuts").getChildNode("FastMin").getChildNode("Parameter").getAttribute("P")));
  TLine * LightvsfFastLine2 = new TLine(cLightvsfFast->GetXaxis()->GetXmin(),
					atof(xMainNode.getChildNode("Cuts").getChildNode("FastMax").getChildNode("Parameter").getAttribute("P")),
					cLightvsfFast->GetXaxis()->GetXmax(),
					atof(xMainNode.getChildNode("Cuts").getChildNode("FastMax").getChildNode("Parameter").getAttribute("P")));
  LightvsfFastLine1->SetLineColor(kRed);
  LightvsfFastLine2->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("FastMin").getAttribute("use")) == 1)
    {
      cLightvsfFast->GetListOfFunctions()->AddLast(LightvsfFastLine1);
      rLightvsfFast->GetListOfFunctions()->AddLast(LightvsfFastLine1->Clone());
    }
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("FastMax").getAttribute("use")) == 1)
    {
      cLightvsfFast->GetListOfFunctions()->AddLast(LightvsfFastLine2);
      rLightvsfFast->GetListOfFunctions()->AddLast(LightvsfFastLine2->Clone());
    }
  
  //TOF draw cuts
  //  cTOF->Draw();
  TLine * TOFLine1 = new TLine(2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("TOFmin").getChildNode("Parameter").getAttribute("P")),
			       cTOF->GetYaxis()->GetXmin(),
			       2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("TOFmin").getChildNode("Parameter").getAttribute("P")),
			       cTOF->GetMaximum()*1.05);
  TLine * TOFLine2 = new TLine(2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("TOFmax").getChildNode("Parameter").getAttribute("P")),
			       cTOF->GetYaxis()->GetXmin(),
			       2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("TOFmax").getChildNode("Parameter").getAttribute("P")),
			       cTOF->GetMaximum()*1.05);
  TOFLine1->SetLineColor(kRed);
  TOFLine2->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("TOFmin").getAttribute("use")) == 1)
    {
      cTOF->GetListOfFunctions()->AddLast(TOFLine1);
      rTOF->GetListOfFunctions()->AddLast(TOFLine1->Clone());
    }
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("TOFmax").getAttribute("use")) == 1)
    {
      cTOF->GetListOfFunctions()->AddLast(TOFLine2);
      rTOF->GetListOfFunctions()->AddLast(TOFLine2->Clone());
    }


  //PMT2psaV2 draw cuts
  //  cTOF->Draw();
  TLine * PMT2psaV21 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psaV2").getChildNode("Parameter").getAttribute("P")),
			       cOsciPSD->GetYaxis()->GetXmin(),
			       2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psaV2").getChildNode("Parameter").getAttribute("P")),
			       cOsciPSD->GetMaximum()*1.05);
  TLine * PMT2psaV22 = new TLine(2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psaV2").getChildNode("Parameter").getAttribute("P")),
			       cOsciPSD->GetYaxis()->GetXmin(),
			       2E-9*atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psaV2").getChildNode("Parameter").getAttribute("P")),
			       cOsciPSD->GetMaximum()*1.05);
  PMT2psaV21->SetLineColor(kRed);
  PMT2psaV22->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psaV2").getAttribute("use")) == 1)
    {
      cOsciPSD->GetListOfFunctions()->AddLast(PMT2psaV21);
      rOsciPSD->GetListOfFunctions()->AddLast(PMT2psaV21->Clone());
    }
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psaV2").getAttribute("use")) == 1)
    {
      cOsciPSD->GetListOfFunctions()->AddLast(PMT2psaV22);
      rOsciPSD->GetListOfFunctions()->AddLast(PMT2psaV22->Clone());
    }


  //TB draw cuts
  TLine * TBLine1 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("TBRatioMin").getChildNode("Parameter").getAttribute("P")),
			      cTB->GetYaxis()->GetXmin(),
			      atof(xMainNode.getChildNode("Cuts").getChildNode("TBRatioMin").getChildNode("Parameter").getAttribute("P")),
			      cTB->GetMaximum()*1.05);
  TLine * TBLine2 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("TBRatioMax").getChildNode("Parameter").getAttribute("P")),
			      cTB->GetYaxis()->GetXmin(),
			      atof(xMainNode.getChildNode("Cuts").getChildNode("TBRatioMax").getChildNode("Parameter").getAttribute("P")),
			      cTB->GetMaximum()*1.05);
  TBLine1->SetLineColor(kRed);
  TBLine2->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("TBRatioMin").getAttribute("use")) == 1)
    {
      cTB->GetListOfFunctions()->AddLast(TBLine1);
      rTB->GetListOfFunctions()->AddLast(TBLine1->Clone());
    }
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("TBRatioMax").getAttribute("use")) == 1)
    {
      cTB->GetListOfFunctions()->AddLast(TBLine2);
      rTB->GetListOfFunctions()->AddLast(TBLine2->Clone());
    }

  //OSC draw cuts
  //  cINTvsMHPMT2->Draw();
  TLine * PMT2Line1 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",0).getAttribute("P")),
				cINTvsMHPMT2->GetYaxis()->GetXmin(),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",0).getAttribute("P")),
				cINTvsMHPMT2->GetMaximum()*1.05);
  TLine * PMT2Line2 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",1).getAttribute("P")),
				cINTvsMHPMT2->GetYaxis()->GetXmin(),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",1).getAttribute("P")),
				cINTvsMHPMT2->GetMaximum()*1.05);
  TLine * PMT2Line3 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",0).getAttribute("P")),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",0).getAttribute("P"))*
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",2).getAttribute("P"))+ 
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",3).getAttribute("P")),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",1).getAttribute("P")),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",1).getAttribute("P"))*
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",2).getAttribute("P"))+ 
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getChildNode("Parameter",3).getAttribute("P")));
  TLine * PMT2Line4 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2").getChildNode("Parameter",0).getAttribute("P")),
				cPMT2Light->GetYaxis()->GetXmin(),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2").getChildNode("Parameter",0).getAttribute("P")),
				cPMT2Light->GetMaximum()*1.05);
  TLine * PMT2Line5 = new TLine(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2").getChildNode("Parameter",1).getAttribute("P")),
				cPMT2Light->GetYaxis()->GetXmin(),
				atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2").getChildNode("Parameter",1).getAttribute("P")),
				cPMT2Light->GetMaximum()*1.05);

  PMT2Line1->SetLineColor(kRed);
  PMT2Line2->SetLineColor(kRed);
  PMT2Line3->SetLineColor(kRed);
  PMT2Line4->SetLineColor(kRed);
  PMT2Line5->SetLineColor(kRed);
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2psa").getAttribute("use")) == 1)
    {
      cINTvsMHPMT2->GetListOfFunctions()->AddLast(PMT2Line1);
      cINTvsMHPMT2->GetListOfFunctions()->AddLast(PMT2Line2);
      cINTvsMHPMT2->GetListOfFunctions()->AddLast(PMT2Line3);
      rINTvsMHPMT2->GetListOfFunctions()->AddLast(PMT2Line1->Clone());
      rINTvsMHPMT2->GetListOfFunctions()->AddLast(PMT2Line2->Clone());
      rINTvsMHPMT2->GetListOfFunctions()->AddLast(PMT2Line3->Clone());
    }
  if(atof(xMainNode.getChildNode("Cuts").getChildNode("PMT2").getAttribute("use")) == 1)
    {
      cPMT2Light->GetListOfFunctions()->AddLast(PMT2Line4);
      cPMT2Light->GetListOfFunctions()->AddLast(PMT2Line5);
      rPMT2Light->GetListOfFunctions()->AddLast(PMT2Line4->Clone());
      rPMT2Light->GetListOfFunctions()->AddLast(PMT2Line5->Clone());
    }










  

  TObjArray Histograms(0);

  Histograms.Add(cHeightvsLight);
  Histograms.Add(rHeightvsLight);

  Histograms.Add(rLightvsPMT2);
  Histograms.Add(rLightvsOsci);

  Histograms.Add(PMT0HeightFunction);
  Histograms.Add(PMT1HeightFunction);
  
  Histograms.Add(rPMT0LightCharge)    ;
  Histograms.Add(rPMT1LightCharge)    ;
  
  Histograms.Add(rPMT2IntvsTOF);
  Histograms.Add(rOsciPSD);
  Histograms.Add(rLightvsRatio); 
  Histograms.Add(rLightvsfFast);
  Histograms.Add(rLightvsTOF);
  Histograms.Add(rRatio); 
  Histograms.Add(rLight)   ;     
  Histograms.Add(rfFast)    ;   
  Histograms.Add(rfSlow)     ;   
  Histograms.Add(rTB)         ;   
  Histograms.Add(rPMT0TIME)    ; 
  Histograms.Add(rPMT1TIME)    ;
  Histograms.Add(rPMT2TIME)     ; 
  Histograms.Add(rTOF);
  Histograms.Add(rINTvsMHPMT0)  ;
  Histograms.Add(rINTvsMHPMT1)  ;
  Histograms.Add(rINTvsMHPMT2)  ;
  Histograms.Add(rPMT0Light)    ;
  Histograms.Add(rPMT1Light)    ;
  Histograms.Add(rPMT2Light);
  Histograms.Add(rMaxHeight0);
  Histograms.Add(rMaxHeight1);
  Histograms.Add(rMaxHeight2);


  Histograms.Add(cLightvsPMT2);
  Histograms.Add(cLightvsOsci);
  Histograms.Add(cPMT2IntvsTOF);
  Histograms.Add(cOsciPSD);
  Histograms.Add(cLightvsRatio);
  Histograms.Add(cLightvsfFast);
  Histograms.Add(cLightvsTOF);
  Histograms.Add(cRatio); 
  Histograms.Add(cLight)   ;     
  Histograms.Add(cfFast)    ;   
  Histograms.Add(cfSlow)     ;   
  Histograms.Add(cTB)         ;   
  Histograms.Add(cPMT0TIME)    ; 
  Histograms.Add(cPMT1TIME)    ;
  Histograms.Add(cPMT2TIME)     ;                                
  Histograms.Add(cTOF);
  Histograms.Add(cINTvsMHPMT0)  ;
  Histograms.Add(cINTvsMHPMT1)  ;
  Histograms.Add(cINTvsMHPMT2)  ;
  Histograms.Add(cPMT0Light)    ;
  Histograms.Add(cPMT1Light)    ;
  Histograms.Add(cPMT2Light);
  Histograms.Add(cMaxHeight0);
  Histograms.Add(cMaxHeight1);
  Histograms.Add(cMaxHeight2);


  
  TFile OutFile(argv[2],"recreate");
  Histograms.Write();
  cout << "File: " << argv[1] << " " <<CNEvents << " passed out of " << NEvents << ". " << 100.0*CNEvents/NEvents << "%\n" ;
  cout << endl << endl << endl;
  return(0);
}
