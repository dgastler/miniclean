#include <runNumberHelper.h>


bool GetRunNumber(uint64_t &RunNumber)
{
  RunNumber = 0;
  bool ret = true;
  //useful string.
  int fileBufferSize = 100;
  char * fileBuffer = new char[fileBufferSize];

  FILE * RunNumberFile = fopen(".RUNNUMBER","r");
  if(!RunNumberFile)
    {
      //No RunNumber file found, so using run number 0
      RunNumber = 0;
    }
  else
    {
      //Get Run number from the file.
      fgets(fileBuffer,fileBufferSize,RunNumberFile);
      RunNumber = atoi(fileBuffer);
      fclose(RunNumberFile);
    }
  //Write out the next run number for the next run.
  sprintf(fileBuffer,"%lu\n",RunNumber + 1);
  RunNumberFile = fopen(".RUNNUMBER","w");
  if(!RunNumberFile)
    {
      //Fail if we can't update the file
      fprintf(stderr,"Can not update run number file \".RUNNUMBER\"!\n");
      ret = false;
    }
  else
    {      
      fputs(fileBuffer,RunNumberFile);
      fclose(RunNumberFile);
    }
  delete [] fileBuffer;
  return(ret);
}
