#include <DCMessageClient.h>

DCMessageClient::DCMessageClient(std::string Name)
{
  SetType("DCMessageClient");
  SetName(Name);
 
  //Initialize Client variables

  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
}

void DCMessageClient::ThreadCleanUp()
{
  DeleteConnection();  
}

void DCMessageClient::MainLoop()
{
  //Check for a new connection
  CheckForNewConnection();
 
  //Check for a new packet
  if(IsReadReady(DCDAQServer.GetFullPacketFD()))
    {      
      RouteRemoteDCMessage();
    }
  //Check for a new message
  if(IsReadReady(DCDAQServer.GetMessageFD()))
    {
      ProcessChildMessage();
    }
}

bool DCMessageClient::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Determine if this is local or not
  if(!message.GetDestination().IsDefaultAddr())
    {
      //Message is sent out of this DCDAQ session
      RouteLocalDCMessage(message);
    }
  else if(message.GetType() == DCMessage::STOP)
    {
      DeleteConnection();
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}

//Read a message from the remote host and then route it
void DCMessageClient::RouteRemoteDCMessage()
{  
  //create a new DCMessage using the data from the packets
  DCMessage::Message message = ReadRemoteMessage();
  
  //Check if the address is the local address. 
  if(DCDAQServer.RouteToLocal(message.GetDestination()))
    {
      //Drop the IP info so the message isn't routed back as a remote 
      //message by dcmessageprocessor
      message.SetDestination(message.GetDestination().GetName());      
      //The Client will forward broadcast messages to this session  
      ForwardMessageOut(message);
    }

}

//Message is coming from this process and out to the network
bool DCMessageClient::RouteLocalDCMessage(DCMessage::Message &message)
{  
  bool ret = true;

  //Get destination of this packet
  DCMessage::DCAddress destAddr = message.GetDestination();
  
  //Get the message's source
  DCMessage::DCAddress sourceAddr = message.GetSource();

  //Check for broadcast destination Send to all. 
  if(destAddr.IsBroadcastAddr())
    {      
      //Don't send it back to the source
      if(!DCDAQServer.RouteToRemote(sourceAddr))
	{
	  DCMessage::Message tempMessage = message;	
	  //If this broadcast originated here, update the source addr to here
	  if(sourceAddr.IsDefaultAddr())
	    {
	      DCMessage::DCAddress tempAddr;
	      tempAddr.Set(sourceAddr.GetName(),
			   DCDAQServer.GetLocalAddress().GetAddr(),
			   DCDAQServer.GetLocalAddress().GetAddrSize());
	      tempMessage.SetSource(tempAddr);
	    }
	  ret &= SendMessage(tempMessage);
	}
    }
  else
    {      
      //Check if this message should be sent to the remote host
      if(DCDAQServer.RouteToRemote(destAddr))
	{
	  //If this broadcast originated here, update the source addr to here
	  DCMessage::Message tempMessage = message;	
	  if(sourceAddr.IsDefaultAddr())
	    {
	      DCMessage::DCAddress tempAddr;
	      tempAddr.Set(sourceAddr.GetName(),
			   DCDAQServer.GetLocalAddress().GetAddr(),
			   DCDAQServer.GetLocalAddress().GetAddrSize());
	      tempMessage.SetSource(tempAddr);
	    }
	  ret &= SendMessage(tempMessage);
	}
    }
  return(ret);
}


void DCMessageClient::ProcessChildMessage()
{
  DCMessage::Message message = DCDAQServer.GetMessageOut();

  if(message.GetType() == DCMessage::END_OF_THREAD)
    {      
      std::stringstream ss;
      ss << "Lost connection with " << DCDAQServer.GetRemoteAddress().GetAddrStr();
      PrintWarning(ss.str().c_str());

      DeleteConnection();

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::DISCONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      SendStop(false);
      Loop = false;
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  else if(message.GetType() == DCMessage::NETWORK)
    {
      ForwardMessageOut(message);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      //SendMessageOut(message,true);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}
void DCMessageClient::PrintStatus()
{
  DCThread::PrintStatus();
  DCDAQServer.PrintStatus();
}
