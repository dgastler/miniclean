#include <DCMessageProcessor.h>

//Commands
void DCMessageProcessor::Stop(DCMessage::Message & message,
			      bool & run)
{
  run = false;
  uint64_t timeTemp = message.GetTime();

  std::string name;
  struct in_addr addr;
  message.GetSource(name,addr);

  printf("%s (%s): STOP!\n",
	 GetTime(&(timeTemp)).c_str(),
	 name.c_str());      
}

void DCMessageProcessor::Go(DCMessage::Message & /*message*/,
			    bool & /*run*/)
{
  std::vector<DCThread*> threads = Launcher->GetThreads();
  //Send all threads the GO message
  for(unsigned int i = 0; i < threads.size();i++)
    {	  
      DCMessage::Message GOmessage;
      GOmessage.SetType(DCMessage::GO);
      threads[i]->SendMessageIn(GOmessage,true);
    }
}
void DCMessageProcessor::Pause(DCMessage::Message & /*message*/,
			       bool & /*run*/)
{
  std::vector<DCThread*> threads = Launcher->GetThreads();
  //Send all threads the PAUSE message
  for(unsigned int i = 0; i < threads.size();i++)
    {
      DCMessage::Message PAUSEmessage;
      PAUSEmessage.SetType(DCMessage::PAUSE);
      threads[i]->SendMessageIn(PAUSEmessage,true);
    }
}

void DCMessageProcessor::Launch(DCMessage::Message & message,    
				bool & /*run*/)             
{  
  std::vector<uint8_t> data;
  message.GetData(data);
  if(data.size() >0)
    {
      //copy string data.
      std::string setupText((char*)&data[0],data.size());
      
      //Parse the configuration string into an XML structure
      xmlDoc * configXML = xmlReadMemory(setupText.c_str(),
				     (int) setupText.size(),
				     NULL,
				     NULL,
				     0);
      //Check that the text was correctly parsed into XML
      if(configXML != NULL)
	{
	  //Get the base node of the xml
	  xmlNode * baseNode = xmlDocGetRootElement(configXML);
	  
	  //Parse the XML in this file and check the baseNode's name
	  if(boost::iequals(std::string((char*)baseNode->name),std::string("Setup")))
	    //Local Launch message
	    {
	      Launcher->Launch(setupText);        
	    }
	  else if(boost::iequals(std::string((char*) baseNode->name),std::string("RemoteSetup")))
	    //Remote Launch message
	    {
	      unsigned int remoteSetupNodes = NumberOfNamedSubNodes(baseNode,"RemoteLaunch");
	      for(unsigned int iRemoteSetup =0 ; iRemoteSetup < remoteSetupNodes; iRemoteSetup++)
		{
		  SendRemoteSetupMessage(setupText,iRemoteSetup);
		}
	    }      
	}
    }
}

void DCMessageProcessor::RunNumber(DCMessage::Message & message,
				   bool & /*run*/)
{
  DCMessage::SingleINT64 data;
  if(message.GetDataStruct(data))
    {
      std::vector<DCThread*> threads = Launcher->GetThreads();
      //forward the RUN_NUMBER message to everyone.
      for(unsigned int i = 0; i < threads.size();i++)
	{
	  //Copy message's data into RunNumberMessage and send out.
	  DCMessage::Message RunNumberMessage = message;;      
	  threads[i]->SendMessageIn(RunNumberMessage,true);
	}      
    }
}

void DCMessageProcessor::GetStats(DCMessage::Message & /*message*/,    
				  bool & /*run*/)             
{
  std::map<std::string,MemoryManager *> mem = Launcher->GetMemoryManagers();
  std::map<std::string,MemoryManager *>::iterator itMem;
  for(itMem = mem.begin();itMem != mem.end();itMem++)
    {
      itMem->second->PrintStatus();
    }  
  
  std::vector<DCThread*> threads = Launcher->GetThreads();
  for(size_t i = 0;i < threads.size();i++)
    {
      threads[i]->PrintStatus();
    }
}


void DCMessageProcessor::RegisterAddr(DCMessage::Message & message,
				      bool & /*run*/)
{
  std::vector<uint8_t> data;
  std::string Name;
  struct in_addr Addr;
  std::string Thread;
  DCMessage::DCRoute routeInfo;
 
  //Get the data from the DCMessage
  message.GetData(data);
  //Parse the data using our DCRoute GetData function
  if(routeInfo.GetData(data,Name,Addr,Thread))
    {  
      //Add this route to the map
      messageAddrToNameMap[Addr.s_addr] = Thread;
    }
}

void DCMessageProcessor::UnRegisterAddr(DCMessage::Message & message,
					bool & /*run*/)
{
  std::vector<uint8_t> data;
  std::string Name;
  struct in_addr Addr;
  std::string Thread;
  DCMessage::DCRoute routeInfo;
 
  //Get the data from the DCMessage
  message.GetData(data);
  //Parse the data using our DCRoute GetData function
  if(routeInfo.GetData(data,Name,Addr,Thread))
    {
  
      //Find this entry in the map
      std::map<uint32_t,std::string>::iterator it;
      it = messageAddrToNameMap.find(Addr.s_addr);
      if(it != messageAddrToNameMap.end())
	{
	  messageAddrToNameMap.erase(it);
	}
    }
}


void DCMessageProcessor::RegisterType(DCMessage::Message & message,
				      bool & /*run*/)
{
  std::vector<uint8_t> data;
  uint16_t ID;
  std::string Name;

  DCMessage::DCTypeID typeInfo;
 
  //Get the data from the DCMessage
  message.GetData(data);
  //Parse the data using our DCRoute GetData function
  if(typeInfo.GetData(data,ID,Name))
    {
      
      //Add this route to the map
      messageTypeToNameMap[ID] = Name;
      printf("DCMessageProcessor: Routing %u type ID to thread %s\n",
	     ID,
	     Name.c_str());      
    }
}

void DCMessageProcessor::UnRegisterType(DCMessage::Message & message,
					bool & /*run*/)
{
  std::vector<uint8_t> data;
  uint16_t ID;
  std::string Name;

  DCMessage::DCTypeID typeInfo;
 
  //Get the data from the DCMessage
  message.GetData(data);
  //Parse the data using our DCRoute GetData function
  if(typeInfo.GetData(data,ID,Name))
    {
      //Find this entry in the map
      std::map<uint16_t,std::string>::iterator it;
      it = messageTypeToNameMap.find(ID);
      if(it != messageTypeToNameMap.end())
	{
	  messageTypeToNameMap.erase(it);
	}
    }
}





