#ifndef __CONVERT_CONTROLLER__
#define __CONVERT_CONTROLLER__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>
#include <RAT/DS/DCDAQ_DCTime.hh>
#include <sys/inotify.h>
#include <ConvertGroup.h>

class ConvertController : public DCThread
{
 public:
  ConvertController(std::string Name)
  {
    SetName(Name);
    SetType(std::string("ConvertController"));
    bufferData.resize(sizeof(inotify_event));
    socketPath.assign("/tmp/DCDAQ/");
  }
  ~ConvertController()
  {
  };
  virtual bool Setup(xmlNode * SetupNode, 
		     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  
  void ProcessNewFile(char * filename);
  //  void BuildRunGroup(int runNumber, char * firstFileName);
  void BuildRunGroup(int runNumber);
  //  void CreateNewDCDAQSession(int runNumber,char * filename);
  void CreateNewDCDAQSession(int runNumber);

  int iNotifyFD;
  int watchFD;
  uint8_t * iNotifyBuffer;
  std::vector<uint8_t> bufferData;

  std::string watchPath;
  std::string watchFileBase;
  std::string watchFileExtension;

  uint8_t portOffset;
  std::string socketPath;
  
  std::map<int,ConvertGroup> Run;
};

#endif
