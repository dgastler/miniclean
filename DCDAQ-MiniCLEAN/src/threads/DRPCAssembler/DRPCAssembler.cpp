#ifdef __RATDS__

#include <DRPCAssembler.h>

DRPCAssembler::DRPCAssembler(std::string Name)
{
  SetType("DRPCAssembler");
  SetName(Name);
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>2800</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>2800</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
}

void DRPCAssembler::ThreadCleanUp()
{
  //Close our server listening socket
  //Close all of our connections to remote DCDAQs 
  server.Clear();
}

bool DRPCAssembler::Setup(xmlNode * SetupNode,
			    std::vector<MemoryManager*> &MemManager)
{  

  Ready = false;
  //================================
  //Get the DRPC memory manager
  //================================
  if(!FindMemoryManager(DRPC_mem,SetupNode,MemManager))
    {
      return false;
    }

  //================================
  //Load a PMT list
  //================================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::PMT_COUNT);
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }



  

  //================================
  //Parse data for server config and expected clients.
  //================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     int(managerSettings.size()),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  if(server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.GetSocketFD());
      SetupSelect();
      Ready=true;
    }
  else
    {
      return false;
    }

 if(FindSubNode(SetupNode, "TRIGGER") == NULL)
    runningTrigger = 1;
  else
    GetXMLValue(SetupNode, "TRIGGER", GetName().c_str(), runningTrigger);

  lastErrorTime = 0;
  badCount = 0;
  lastChannelWord = 0;
  lastRawIntegral = 0;
  lastRawStartTime = 0;
  lastRawWidth = 0;
  debugInformation.clear();


  return true;
}


void DRPCAssembler::ProcessChildMessage(size_t iConn)
{
  DCMessage::Message message = server[iConn].GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      std::stringstream ss;
      ss << "Lost connection with " << server[iConn].GetRemoteAddress().GetAddrStr();
      PrintError(ss.str().c_str());
      
      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::DISCONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      DeleteConnection(iConn);
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}

bool DRPCAssembler::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      server.Clear();
      DRPC_mem->Shutdown();
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return ret;
}

void DRPCAssembler::ProcessTimeout()
{
}

void DRPCAssembler::PrintStatus()
{
  DCThread::PrintStatus();
  for(size_t i = 0; i <server.Size();i++)
    {
      server[i].PrintStatus();
    }
}

#endif
