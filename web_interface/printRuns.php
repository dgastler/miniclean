<?php

//Open database
include 'opendb.php';

//This function checks to make sure that the input is an integer
function checkType($num_check,$type_check) {
  if ($num_check != "0" && $type_check == 0) {
    die("That wasn't a valid integer!");
  } else {
    return $num_check;
  }
}

function runExists($run_num) {
  $exists_query = mysql_query("SELECT run_number FROM daq_interface.Thread_Threads WHERE run_number = $run_num");
  if ($exists_query) {
    return 'true';
  } else {
    die("That run does not exist! Sorry.");
    }
}

//Making an array of the tables to touch
$table_array = array();
$table_array[0] = "SThread_Data_Compressed";
$table_array[1] = "NetworkConnections";
$table_array[2] = "SThread_DecisionMaker";
$table_array[3] = "Settings_DataReduction";
$table_array[4] = "Memory_MemoryManagers";
$table_array[5] = "Messages_BeforeThreads";
$table_array[6] = "Messages_End";
$table_array[7] = "Messages_Start";
$table_array[8] = "Settings_PMTs";
$table_array[9] = "SThread_Data_Raw";
$table_array[10] = "Thread_Threads";
$table_array[11] = "Settings_WFDs";

$length = count($table_array);

$to1 = $_GET["runto"];
$to2 = 0 + $to1;
$run_num = checkType($to1,$to2);
runExists($run_num);

//Making an array to store the printing values
$print_array = array();
$print_array[0] = 0;
$print_array[1] = 0;
$print_array[2] = 0;
$print_array[3] = 0;
$print_array[4] = 0;

$print_tables = array();
$print_tables[0] = "Run number";
$print_tables[1] = "Order ID";
$print_tables[2] = "Thread name";
$print_tables[3] = "Thread type";
$print_tables[4] = "PC name";

for ($i = 0; $i < $length; $i++) {
  //Get the name of the current table
  $table = $table_array[$i];
  $table_name = "daq_interface.".$table;
  
  //This is where this file differs from the other ones: I'm just going to look at the info in the file, and print it out onto the web page
  $table_query = "SELECT * FROM $table_name WHERE run_number = $run_num";
  $print_array[0] = $run_num;
  $find_table = mysql_query($table_query);
  if ($find_table) {
    $table_length = mysql_num_fields($find_table);
    for ($j = 0; $j < $table_length; $j++) {
      if ((mysql_field_name($find_table,$j) == 'order_id') && ($print_array[1] == 0)) {
	while ($row = mysql_fetch_array($find_table)) {
	  $print_array[1] = $row['order_id'];}
      } else if ((mysql_field_name($find_table,$j) == 'thread_name') && ($print_array[2] == 0)) {
	while ($row = mysql_fetch_array($find_table)) {
	  $print_array[2] = $row['thread_name'];}
      } else if ((mysql_field_name($find_table,$j) == 'thread_type') && ($print_array[3] == 0)) {
	while ($row = mysql_fetch_array($find_table)) {
	  $print_array[3] = $row['thread_type'];}
      } else if ((mysql_field_name($find_table,$j) == 'pc_name') && ($print_array[4] == 0)) {
	while ($row = mysql_fetch_array($find_table)) {
	  $print_array[4] = $row['pc_name']; }
      }
    }
  }
}
//Now print out the contents of that print array to the html page
for ($j = 0; $j < count($print_array); $j++) {
  echo $print_tables[$j] . ": " . $print_array[$j] . "<BR>";
 }
     
   
//Close the database. I guess this happens when the script ends but just in case, you know?
include 'closedb.php';
?>
   