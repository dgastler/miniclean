#include <RATParser.h>

void RATParser::MainLoop()
{
  //Check for a new memory block event
  //Don't do anything if we already have a blockIndex
  if(IsReadReady(Block_mem->GetFullFDs()[0]) &&
     (memBlockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Block_mem->GetFull(memBlockIndex);
      if(memBlockIndex != FreeFullQueue::BADVALUE)       //We have a valid MemBlock
	{
	  //Get the memory bloc for the blockIndex
	  memBlock = &Block_mem->Memory[memBlockIndex];
	  //Zero our current point in the memory block.
	  currentDataIndex = 0;
	  
	  //We no longer need a memblock.
	  //Remove memory block manager from the select set
	  RemoveReadFD(Block_mem->GetFullFDs()[0]);
	  
	  if(evIndex ==  FreeFullQueue::BADVALUE) 
	    {
	      //We need a DS block so add the RAT manager's FD to select 
	      AddReadFD(EV_mem->GetFreeFDs()[0]);	  
	    }
	  
	  ///Apply select changes
	  SetupSelect();
	}
      else 
	{		  
	  //This means that the memory block manager
	  //has been set to shutdown.
	  //We probably have a message from DCDAQ waiting
	  //for us
	}      
    }
  
  
  
  
  //check for a new basic ev block event
  if(IsReadReady(EV_mem->GetFreeFDs()[0]) &&
     (evIndex == FreeFullQueue::BADVALUE))
    {
      //Get new ev block
      EV_mem->GetFree(evIndex);
      if(evIndex != FreeFullQueue::BADVALUE)
	{
	  //Get the EVBlock for evIndex
	  evBlock = EV_mem->GetWFDBlock(evIndex);
	  //Zero it's use
	  evBlock->usedSize = 0;
	  
	  //Now that we have a ev block we don't need to listen to it's queue
	  RemoveReadFD(EV_mem->GetFreeFDs()[0]);	     
	  
	  if(memBlockIndex == FreeFullQueue::BADVALUE)
	    {	      
	      //We need a mem block so add the mem block manager's FD to select  
	      AddReadFD(Block_mem->GetFullFDs()[0]);
	    }
	  
	  //Apply select changes
	  SetupSelect();
	}
      else
	{
	  //This means that the ev manager has been shut down
	  //We probably have a message waiting for us from DCDAQ
	}
    }
  
  //If we are in MainLoop, then something has changed with the state of 
  //either the memblock or evblock.   
  //Check that we have a valid memblock and a valid evBlock
  if((memBlockIndex != FreeFullQueue::BADVALUE)&&
     (evIndex != FreeFullQueue::BADVALUE))
    {
      //Loop over the current memblock untill it is emtpy
      while(currentDataIndex < memBlock->dataSize)
	{
	  //Parse the current CAEN V1720 style event.  
	  //It is located at memBlock->buffer + CurrentDataIndex
	  int32_t shift =  CurrentEvent.ProcessEvent( (char*) (memBlock->buffer + currentDataIndex),
						      memBlock->dataSize - currentDataIndex);
	  //	  printf("%03d: Shift: %d\n",memBlockIndex,shift);
//	  FILE * blahFile = fopen("blah.dat","a");
//	  uint32_t blahSize = 0;
//	  uint32_t * blah = CurrentEvent.GetRawData(blahSize);
//	  for(uint32_t iBlah = 0; iBlah < blahSize;iBlah++)
//	    {
//	      fprintf(blahFile,"%04u : 0x%08X\n",iBlah,blah[iBlah]);
//	    }
//	  fclose(blahFile);
	  //If shift is positive, then we read out a valid event
	  if(shift > 0)
	    {			
	      //Take the parsed CAEN event an put it in a RAT ev
	      ProcessOneEvent(CurrentEvent,evBlock->array + evBlock->usedSize);	     
//	      fprintf(stderr,"0 Parsed event into index %u: event %d:%u\n",
//		      evBlock->usedSize,
//		      (evBlock->array + evBlock->usedSize)->GetEventID(),
//		      (((uint32_t*)(memBlock->buffer + currentDataIndex))[2]) >> 27);
	      //We processed another event.  Yay!
	      TimeStep_Events++;
	      //Move CurrentDataIndex to the start of the next event. 
	      currentDataIndex += shift;  
	      //Move us forward in the evBlock
	      evBlock->usedSize++;
	      //Check if we should pass off this block. 
	      if(evBlock->usedSize >= evBlock->allocatedSize)
		{
		  //Break out of the while loop and get a new evBlock
		  break;
		}
	    }
	  else if(shift == V1720EVENT_SMALL_EVENT)
	    {
	      //No more events to parse, but there is still some data.
	      //This is strange and worthy of reporting
	      char * buffer = new char[100];
	      sprintf(buffer,"Found small event of size %d",memBlock->dataSize - currentDataIndex);
	      PrintWarning(buffer);
	      delete [] buffer;

	      currentDataIndex = memBlock->dataSize;
	      break;
	    }
	  else if(shift == V1720EVENT_CORRUPT_EVENT)
	    {
	      //we encountered a bad event
	      TimeStep_BadEvents++;//Bad data. 		 

	      char * buffer = new char[100];
	      sprintf(buffer,"Corrupt event. Lost %d bytes of data.",memBlock->dataSize - currentDataIndex);
	      PrintWarning(buffer);
	      delete [] buffer;	      

	      DCMessage::Message message;      
	      //Send a message back to the master and stop the inner run loop
	      message.SetType(DCMessage::STOP);
	      SendMessageOut(message,true);

	      currentDataIndex = memBlock->dataSize;
	      break;
	    }
	  else
	    {
	      //shouldn't happen.
	      PrintError("Unknown error (you win!)");
	      currentDataIndex = memBlock->dataSize;
	      break;
	    }	 	  
	}
      
      //Check if we should return the current memBlock
      if(currentDataIndex >= memBlock->dataSize)
	{
	  //Zero memBlock's data size
	  memBlock->dataSize = 0;
	  //Forget the address of the memBlock (that way we don't mess with it)
	  memBlock = NULL; 
	  //Recycle the memblock by passing off it's index
	  Block_mem->AddFree(memBlockIndex);
	  //We need to listen for a new full memBlock
	  AddReadFD(Block_mem->GetFullFDs()[0]);
	  SetupSelect();
	}
      
      //Check if we should pass off the current evBlock
      if(evBlock->usedSize >= evBlock->allocatedSize)
	{	  
	  //Forget the evBlock
	  evBlock = NULL;
	  //Give back the evBlock for writing.
	  EV_mem->AddFull(evIndex);
	  //Now we need to listen for a new free EVBlock
	  AddReadFD(EV_mem->GetFreeFDs()[0]);
	  SetupSelect();
	}     	
    }
}



void  RATParser::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      float TimeStep = float(difftime(currentTime,lastTime));
      DCMessage::TimedCount Rate;
      DCMessage::SingleUINT64 Count;
      
      BlocksRead += TimeStep_BlocksRead;
      Events+=TimeStep_Events;
      BadEvents+=TimeStep_BadEvents;

      //Data message
      sprintf(Count.text,"Blocks read");
      Count.i = TimeStep_BlocksRead;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);

      

      //Data message
      sprintf(Rate.text,"Events read");
      Rate.count = TimeStep_Events;
      Rate.dt = TimeStep;
      message.SetDataStruct(Rate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      

      //Data message
      sprintf(Count.text,"Bad events read");
      Count.i = TimeStep_BadEvents;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);



      TimeStep_BlocksRead = 0;
      TimeStep_Events = 0;
      TimeStep_BadEvents = 0;
      //Setup next time
      lastTime = currentTime;
    }
}

