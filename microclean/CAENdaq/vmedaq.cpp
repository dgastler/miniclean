//SYSTEM HEADERS
#include <iostream>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fstream>
#include <signal.h>
#include <unistd.h>
#include <string.h>
//#include <pthread.h>
//#include <time.h>

//ROOT HEADERS
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

//CLASSES
#include "vme_crate_lib.h"
#include "wfd_card_lib.h"
#include "Event.h"
#include "TextArray.h"
#include "xmlParser.h"

//Slow control interface
extern "C"
{
  //#include </home/clean/SC_db_interface/SC_db_interface.h>
#include <SC_db_interface.h>
}


#define FIRST_WFD_ADER0   0x100
#define FIRST_WFD_ADER1   0x0
#define SUBRUNMAXEVENTS   10000
#define STRINGSIZE        200
#define LOGSIZE           100000
#define RUNNUMBERFILENAME ".RUNNUMBER"
//#define TIMEOUTSMAX       20
//DAQ-MOnitor communication lock file
#define FILE "/dev/shm/lock" 

using namespace std;

//Globals for signals
volatile bool stoprun = false;  //
volatile bool newevent = false; // Connect signal handlers to main daq loop.
volatile bool timeout = false;  //
int timoutsMAX = 20;
int Ntimeouts = 0;
sigset_t      mask_set; // Mask out *all signals in the signal handlers
struct sigaction newEvent;
struct sigaction userStop;
struct sigaction timerStop;
struct sigaction ignore;



void signal_timeout(int sig_num)
{
  timeout = true;
}

void signal_stoprun(int sig_num) //ctrl-c and alarm handler
{
  //Run over.  Stop listening to signals and stop run loop
  sigaction(SIGINT, &ignore,NULL);
  sigaction(SIGALRM,&ignore,NULL);
  sigaction(SIGUSR1,&ignore,NULL);
  stoprun = true;
}
void signal_newevent(int sig_num)//DAQ Trigger handler
{
  //Got an event.  Start event code.
  //sigaction(SIGUSR1,&ignore,NULL);  
  alarm(0);
  newevent = true;
}



void setup_signals()  //Initial setup of signal handling. 
{
  //Mask all signals in the signal handlers
  sigfillset(&mask_set);
  sigdelset(&mask_set,SIGINT);
  sigdelset(&mask_set,SIGALRM);

  //zero structures
  memset(&newEvent,0,sizeof(newEvent));
  memset(&userStop,0,sizeof(userStop));
  memset(&timerStop,0,sizeof(timerStop));
  memset(&ignore,0,sizeof(ignore));

  //Assign signal handlers and masks
  ignore.sa_handler = SIG_IGN;
  newEvent.sa_handler = signal_newevent;
  newEvent.sa_mask = mask_set;
  userStop.sa_handler = signal_stoprun;
  userStop.sa_mask = mask_set;
  timerStop.sa_handler = signal_timeout;
  timerStop.sa_mask = mask_set;
  
  //Block all signals!
  sigaction(SIGINT, &userStop,NULL);
  sigaction(SIGALRM,&ignore,NULL);
  sigaction(SIGUSR1,&ignore,NULL);

}

//File locking for DAQ monitor connection
bool lock()
{
  Int_t fd = open(FILE,O_WRONLY | O_CREAT | O_EXCL,0);
  if(fd == -1)
    {
      return(false);
    }
  close(fd);
  return(true);
}

void unlock()
{
  unlink(FILE);
}








//MAIN Program
int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      cout << "Use: " << argv[0] << " XML-File" << endl;
      return(0);
    }

  //Setup signals and IGNORE SIGUSR.
  setup_signals();

  bool AutoMoveFiles = false;

  int RunNumber = 0;   // Current run number  set by .RUNUMBER
  int SubRunNumber = 1;  // current sub-run number
  int NumberOfEventsMax = 0;
  int TimeMax = 0;
  int eventN = 1; // Current event number
  int Timeout = 2; // seconds before we check if something went wrong with the DAQ.  Defauls = 2, set by XML
  int timeoutsMAX = 20; // Max number of timeouts where there were no DAQ errors before the daq is shutdown

  //Strings
  int OutPutFileNameSize = STRINGSIZE;
  char * OutPutFileName = new char[STRINGSIZE];
  char * MovePath = new char[STRINGSIZE];
  char * MoveCommand = new char[STRINGSIZE];
  char * LogText = new char[LOGSIZE];
  char * Message1 = new char[STRINGSIZE];
  char * Message2 = new char[STRINGSIZE];
  char * Path     = new char[STRINGSIZE];
  char * Type     = new char[STRINGSIZE];

  // stream for RunNumber reading and writing
  fstream RunNumberFile;

  //Pointers for XML text
  int    XMLPointerSize = 0;
  char * XMLPointer = 0;
  
  //Time variables
  time_t eventtime = 0; // current event time
  time_t LastMonitorEventTime = 0;  // last event time
  time_t SlowControlTime = 0; // for slow control
  time_t starttime = 0; // Run start time
  useconds_t MicroSecond = 1; 

  // I dont think these are needed.....
  sigset_t suspendset;
  sigemptyset(&suspendset);


  //DAQ-Monitor communication files
  char  *MonitorFileName   = new char[STRINGSIZE];
  TFile *MonitorFile = 0;
  TTree *MonitorTree = 0;
  sprintf(MonitorFileName,"/dev/shm/DAQtemp.root");   
  
  //Copying strings setup.
  getcwd(Path,STRINGSIZE);
  sprintf(Message1,"Sub-Run Start");
  sprintf(Message2,"Sub-Run End");

  //Open XML card file called DAQ.XML and report errors. 
  XMLResults Results;
  XMLNode xMainNode=XMLNode::parseFile(argv[1],"DAQ",&Results);
  if(Results.error != eXMLErrorNone)
    {
      if(Results.error == eXMLErrorFileNotFound)
	{
	  cerr << "Error: DAQ.xml not found" << endl;	  
	}
      else
	{
	  cerr << "Error: Error # " << Results.error << endl;
	}
      return(0);
    }
  else
    {
      cout << "Opening DAQ.xml ok" << endl;
    }


  //Get start of run XML card file
  XMLPointer = xMainNode.createXMLString(true,&XMLPointerSize);
 
  //Create event pointer and the XML Text pointer
  Event *event = new Event;
  TextArray * RootText = new TextArray();
  vector<string> *ErrorLog = new vector<string>;
  //  TextArray * ErrorLog = new TextArray();
 
  //Open RunNumber file and return on failure
  //GetRunNumber for file
  RunNumberFile.open(RUNNUMBERFILENAME,ios::in);
  if(RunNumberFile.fail())
    {
      cerr << "Error reading RunNumberFile." << endl;
      return(-1);
    }
  char * buffer = new char[STRINGSIZE];
  RunNumberFile.getline(buffer,STRINGSIZE);
  RunNumber = atoi(buffer) + 1;
  RunNumberFile.close();
  delete [] buffer;


  //Create RootFile Pointer

  //Open First File
  sprintf(OutPutFileName,"run_%8.8d_%s_%4.4d.root",
	  RunNumber,
	  xMainNode.getChildNode("Run").getAttribute("Type"),
	  SubRunNumber
	  );    
  TFile *RootFile = new TFile(OutPutFileName, "RECREATE");
  RootFile->SetCompressionLevel(atoi( xMainNode.getChildNode("Run").getChildNode("Compression").getAttribute("Level")) );
  
  //Setup a tree for events and XML Texts
  TTree *tree     = new TTree("tree","Run");
  TTree *TextTree = new TTree("TextTree","RunNotes");
  TTree *ErrorTree = new TTree("ErrorTree","ErrorLog");
  tree->SetAutoSave(10000000);
  tree->Branch("Event branch","Event",&event,64000,0);
  TextTree->Branch("Run Notes","TextArray",&RootText);
  ErrorTree->Branch("Error Log","std::vector<string>",&ErrorLog);

  //Write start of run XML Test file
  RootText->FillArray(XMLPointerSize,XMLPointer);
  TextTree->Fill();
 

  //Initialize Crate;  
  VME_Crate *crate = new VME_Crate;
  int cratefd = crate->getcratefd();
  crate->reset();  //reset crate
  sleep(10);       //and wait


  //Run Settings
  NumberOfEventsMax = atoi(xMainNode.getChildNode("Run").getChildNode("Events").getAttribute("N"));
  TimeMax = atoi(xMainNode.getChildNode("Run").getChildNode("Time").getAttribute("T"));
  timeoutsMAX = atoi(xMainNode.getChildNode("Run").getChildNode("NTimeouts").getAttribute("N"));
  Timeout = atoi(xMainNode.getChildNode("Run").getChildNode("Timeout").getAttribute("T"));

  if(atoi(xMainNode.getChildNode("Run").getChildNode("SavePath").getAttribute("Auto")) == 1)
    {
      sprintf(MovePath,"%s",xMainNode.getChildNode("Run").getChildNode("SavePath").getAttribute("Path"));
      AutoMoveFiles = true;
      cout << "Moving files to " << MovePath << endl;
    }
  else
    {
      sprintf(MovePath,"./");
      AutoMoveFiles = false;
    }
  

  //Find out the number of WFDs
  int NumberOfWFDs = xMainNode.getChildNode("Setup").nChildNode("WFD");
  WFD_Card *WFDs = new WFD_Card[NumberOfWFDs];
  WFD_Card * CurrentWFD = 0;

  //ReadOut PMTVoltages
  time_t ReadTime = 0;
  time_t CurrentTime = time(NULL);
  double PMT0_Voltage = 0;
  double PMT1_Voltage = 0;
  cout << "Asking slow control for HV" << endl;
  
  read_mysql_sensor_data("HV_V_1",&ReadTime,&PMT0_Voltage);
  read_mysql_sensor_data("HV_V_2",&ReadTime,&PMT1_Voltage);
  //int HVReadTimeouts = 20;
  //while(difftime(ReadTime,CurrentTime) < 0)
  //  {
  //    read_mysql_sensor_data("HV_V_1",&ReadTime,&PMT0_Voltage);
  //    read_mysql_sensor_data("HV_V_2",&ReadTime,&PMT1_Voltage);
  //    printf("%s %d :  %s %d\n",ctime(&CurrentTime),CurrentTime,ctime(&ReadTime),ReadTime);
  //    sleep(1);
  //    HVReadTimeouts--;
  //    if(HVReadTimeouts < 0)
  //	{
  //	  cout << "Can't Read from slow control\n";
  //	  stoprun = true;
  //	  break;
  //	}
  //  }

  // Let's sleep a while here to see if that helps things.
  sleep(5);
  if(PMT0_Voltage == 0 || PMT1_Voltage == 0)
    {
      cout << "Can't Read from slow control\n";
      stoprun = true;
    }
  
  cout << "PMT0 V = " << PMT0_Voltage <<" PMT1 V = " << PMT1_Voltage << endl;


  //Setup WFDS and their data arrays
  u_int32_t **Buffer = (u_int32_t**) new u_int32_t* [NumberOfWFDs*4];  
  
  for(int i = 0;i < NumberOfWFDs;i++)
    {
      XMLNode WFDSettings = xMainNode.getChildNode("Setup").getChildNode("WFD",i);
      CurrentWFD = WFDs + i;
      int slot = atoi(WFDSettings.getAttribute("Slot"));
      int F0ADER = FIRST_WFD_ADER0 + 256*i;
      int F1ADER = FIRST_WFD_ADER1 + 256*i;
      int Personality = atoi(WFDSettings.getAttribute("Mode"));
      int WindowSize[4] = {0,0,0,0};
      u_int8_t Offsets[4] = {0,0,0,0};

      // Check that there is actually a WFD at slot and let the user know if anything is wrong
      // Also lets take this oppertunity to update the XML with the WFD ids and their firmware versions.
      u_int8_t data = 0;
      int ret = crate->georead(slot,0x7ffff,&data);
      if(ret == 0)
	{
	  if(data == slot)
	    {
	      char smallbuffer[40];
	      memset(&smallbuffer,0,40*sizeof(char));
	      cout << "Found WFD in slot: " << slot;
	      //Get WFD ID and put it in the XML file
	      crate->georead(slot,0x3f,&data);
	      sprintf(smallbuffer,"0x%d",(u_int32_t) data);
	      WFDSettings.updateAttribute(smallbuffer,NULL,"ID");
	      cout << " with ID: " << smallbuffer ;
	      //Get WFD firmware version and convert it to date. Add to XML 
	      u_int8_t fw0 = 0;
	      u_int8_t fw1 = 0;
	      u_int8_t fw2 = 0;
	      u_int8_t fw3 = 0;
	      crate->georead(slot,0x43,&fw0);
	      crate->georead(slot,0x47,&fw1);
	      crate->georead(slot,0x4b,&fw2);
	      crate->georead(slot,0x4f,&fw3);
	      u_int8_t Fyear = fw3&0x0F;
	      u_int8_t Fmonth = fw3&0xF0 >> 4;
	      u_int8_t Fday = fw2;
	      u_int8_t Byear = fw1&0x0F;
	      u_int8_t Bmonth = fw1&0xF0 >> 4;
	      u_int8_t Bday = fw0;
	      sprintf(smallbuffer,"Board=%2.2u-%2.2u-%2.2u Firmware=%2.2u-%2.2u-%2.2u",Bday,Bmonth,Byear,Fday,Fmonth,Fyear);
	      cout << " with Version: " << smallbuffer << endl;
	      WFDSettings.updateAttribute(smallbuffer,NULL,"Version");      
	    }
	  else
	    {
	      cerr << "Wrong geographic address at slot: " << slot << " got " << data << endl;
	      return(0);
	    }
	}
      else
	{
	  cerr << "WFD not found in slot: " << slot << " Return value = " << ret << endl;
	  return(0);
	}
      // Check settings for each channel and then setup the current WFD
      if(WFDSettings.nChildNode("Channel") != 4)
	{
	  cerr << "WFD #" << i << " in DAQ.xml does not contain the correct number of channels." << endl;
	  return(0);
	}
      for(int j = 0; j < 4 ; j++)
	{
	  //This sets the number of 32bit integers to get from the WFD for each channel (500mhz * (8bit))*(1 (32bit)/4 (8bit))* time(s) = 125*time(us)
	  int Channel = atoi(WFDSettings.getChildNode("Channel",j).getAttribute("N"));
	  int ID = atoi(WFDSettings.getChildNode("Channel",j).getAttribute("PMTID"));
	  WindowSize[Channel] = 125*atoi(WFDSettings.getChildNode("Channel",j).getAttribute("Window")) ;
	  Offsets[Channel] = atoi(WFDSettings.getChildNode("Channel",j).getAttribute("Offset")) ;
	  CurrentWFD->setChanID(Channel,ID);
	  
	  //Set PMT Voltage
	  //This is hardcoded for microCLEAN and something must be done to connect what PMT is really is connnected to a HV channel and to a WFD channel

	  char smallbuffer[10];
	  memset(&smallbuffer,0,10*sizeof(char));
	  if((atof(WFDSettings.getChildNode("Channel",j).getAttribute("PMTID")) < 1) && (atof(WFDSettings.getChildNode("Channel",j).getAttribute("PMTID")) >= 0))
	    {
	      sprintf(smallbuffer,"%f",PMT0_Voltage);
	      WFDSettings.getChildNode("Channel",j).updateAttribute(smallbuffer,NULL,"PMTVoltage");
	    }
	  else if ((atof(WFDSettings.getChildNode("Channel",j).getAttribute("PMTID")) < 2) && (atof(WFDSettings.getChildNode("Channel",j).getAttribute("PMTID")) >= 1))
	    {
	      sprintf(smallbuffer,"%f",PMT1_Voltage);
	      WFDSettings.getChildNode("Channel",j).updateAttribute(smallbuffer,NULL,"PMTVoltage");
	    }
	}
      CurrentWFD->setupWFD(crate,slot,F0ADER,F1ADER,Personality,WindowSize[0],WindowSize[1],WindowSize[2],WindowSize[3],Offsets[0],Offsets[1],Offsets[2],Offsets[3]);      
      for(int j = 0;j < 4;j++)
	{
	  Buffer[4*i + j] = (u_int32_t*) new u_int32_t[WindowSize[j]];	  
	  CurrentWFD->setWindow(j,Buffer[4*i+j]);
	}
      CurrentWFD->resetfifo(5);
    }

  //Get IOREgister ready to start event
  crate->a32d32write(0x38383808,0x0);
  crate->a32d32write(0x38383804,0x00000E01);  //set irq level 
  crate->a32d32write(0x38383800,0x00110001);  //enable irq and turn LED on

  //Listen for ctrl-c, alarm and DAQ Trigger signals
  sigaction(SIGINT, &userStop ,NULL);
  sigaction(SIGALRM,&timerStop,NULL);
  sigaction(SIGUSR1,&newEvent ,NULL);

  //Setup VME->PCI interrupt.  Tell VME controller to pass IRQs to PCI card.
  struct sis1100_irq_ctl irqctl;
  struct sis1100_irq_get irqget;
  struct sis1100_irq_ack irqack;

  //  irqctl.irq_mask=0xffffffff; /* all IRQs */
  irqctl.irq_mask=SIS3100_VME_IRQS;
  irqctl.signal=SIGUSR1;
  if(ioctl(cratefd,SIS1100_IRQ_CTL,&irqctl)<0)
    {
      cerr << "SIS1100_irq_ctl error\n";
      return(0);
    }

  /*
    this IRQ_ACK is only necessary because of a driver bug
    (it will be fixed in the next release (2.05))
  */
  irqack.irq_mask=0xffffffff;
  if (ioctl(cratefd, SIS1100_IRQ_ACK, &irqack)<0) 
    {
      cerr << "irqack hack error" << endl;
      return (0);    
    }

  //turn off veto and start DAQ.
  crate->a32d32write(0x38383808,0x0);
  crate->a32d32write(0x38383808,0x1);

  starttime = time(NULL);
  char * StartTimePointer = ctime(&starttime);
  sprintf(Type,"%s",xMainNode.getChildNode("Run").getAttribute("Type"));

  cout << "Running DAQ for " << NumberOfEventsMax << " events or " << TimeMax << " minutes." <<endl;
  cout << "Starting at " << StartTimePointer << endl;

  //I'm lying here.   The file was opened a long time ago.

  cout << "Opening File: " << OutPutFileName << endl;
  insert_mysql_fast_DAQ_message(Message1,Type,OutPutFileName,Path,&SlowControlTime);




  int TimePass = TimeMax * 60;


  //Main event loop
  while(!stoprun)
    {
      // If the daq has gone one Timeout without an event.
      // This checks if there was an interrupt that was somehow missed.
      if(timeout)
	{

	  u_int32_t data = 0;
	  //Read out the VME IO Register
	  crate->a32d32read(0x38383800,&data);
	  cerr << "Timeout waiting for event #" << eventN << " Check gave 0x" << hex << data << dec << endl;
	  sprintf(LogText,"Timeout waiting for event #%d Check gave 0x%X\n",eventN,data);
	  ErrorLog->push_back(LogText);
	  if((data & 0x10000000) == 0x10000000) // Missed IRQ.  Just process the event as normal now.
	    {
	      newevent = true;
	      cerr << "Missed IRQ! Processing Event"  << endl;
	      sprintf(LogText,"Missed IRQ! Processing Event\n");
	      ErrorLog->push_back(LogText);
	    }
	  // Either something strange happened or there really hasn't been an event. 
	  // Keep track of how many of these there has been in a row and shutdown
	  // if there are too many. 
	  else
	    {
	      Ntimeouts++;
	      if(Ntimeouts > timeoutsMAX)
		{
		  cout << "Max number of timeouts reached. Shutting down DAQ" << endl;
		  sprintf(LogText,"Max number of timeouts reached. Shutting down DAQ\n");
		  ErrorLog->push_back(LogText);
		  stoprun = true;
		  insert_mysql_system_message("Max number of timeouts reached. Shutting down DAQ","Error",1);
		}
	      else
		{
		  cerr << "Timeout #" << Ntimeouts << endl;
		  sprintf(LogText,"Timeout #%d\n",Ntimeouts);
		  ErrorLog->push_back(LogText);
		  if(!(Ntimeouts & 0x1)) //If this is an even number of timeouts assume the veto is stuck and reset it. 
		    {
		      cerr << "Sending Veto reset signal.(maybe this is why we're not getting any events.)" << endl;
		      sprintf(LogText,"Sending Veto reset signal.(maybe this is why we're not getting any events.)\n");
		      ErrorLog->push_back(LogText);
		      //Reset IORegist's reset output.
		      crate->a32d32write(0x38383808,0x0);
		      //restart DAQ and turn off veto.
		      crate->a32d32write(0x38383808,0x1);
		    }
		  alarm(Timeout);
		}
	    }
	  timeout = false;
	  
	  
	}
      if(newevent)
	{
	  //Signal handler has blocked new SIGUSR1 signals.  We are safe from the DAQ, but
	  //not from ctrl-c.   We now have to finish the VME IRQ ACK
	  
	  //Get ALL IRQ levels from struck
	  irqget.irq_mask=SIS3100_VME_IRQS;
	  if(ioctl(cratefd,SIS1100_IRQ_GET,&irqget)<0)
	    {
	      cerr << "error irqget\n";
	      sprintf(LogText,"error irqget\n");
	      ErrorLog->push_back(LogText);
	      stoprun = true;
	    }


	  //Reset the IORegister IRQ.
	  u_int32_t data = 0;
	  crate->a32d32read(0x38383814,&data); //read latched data
	  crate->a32d32write(0x38383800,0x1000); //reset irq
	  crate->a32d32write(0x38383808,0x0);

	  
	  //acknowledge all the IRQ levels from struck. 
	  irqack.irq_mask=irqget.irqs;
	  if(ioctl(cratefd,SIS1100_IRQ_ACK,&irqack)<0)
	    {
	      cerr << "error irqack\n";
	      sprintf(LogText,"error irqack\n");
	      ErrorLog->push_back(LogText);
	      stoprun = true;
	    }
	  
	  //Reset IORegist's reset output.
	  crate->a32d32write(0x38383808,0x0);
	  
	  //do IRQ ACK cycle and stop the run if something bad happens.
	  irqack.irq_mask=1<<irqget.level;
	  if (ioctl(cratefd, SIS1100_IRQ_ACK, &irqack)<0) 
	    {
	      cerr << "error irqack\n";
	      sprintf(LogText,"error irqack\n");
	      ErrorLog->push_back(LogText);
	      stoprun = true;    
	    }
	  
	  //We have now acknoledged the VME IRQ and can process the event.
	  alarm(0);
	  
	  //Reset Ntimeouts counter
	  Ntimeouts = 0;

	  //Check if we need to breakup the file or end the run
	  if(eventN <= NumberOfEventsMax)
	    {
	      if(eventN > (SubRunNumber)*SUBRUNMAXEVENTS)
		{
		  char * NEventsBuffer = new char[STRINGSIZE];	      
		  sprintf(NEventsBuffer,"%d",eventN - 1 - (SubRunNumber*SUBRUNMAXEVENTS));
		  xMainNode.getChildNode("Run").updateAttribute("true",NULL,"GoodRun");
		  xMainNode.getChildNode("Run").getChildNode("Events").updateAttribute(NEventsBuffer,NULL,"N");
		  sprintf(NEventsBuffer,"%d",RunNumber);  // Reusing to set run Number
		  xMainNode.getChildNode("Run").updateAttribute(NEventsBuffer,NULL,"RunNumber");
		  delete [] NEventsBuffer;
		  
		  //Write End of file XML string
		  int XMLEndPointerSize =0;
		  char * XMLEndPointer = xMainNode.createXMLString(true,&XMLEndPointerSize);
		  RootText->FillArray(XMLEndPointerSize,XMLEndPointer);
		  //ErrorLog->FillArray(strlen(LogText),LogText);
		  TextTree->Fill();
		  ErrorTree->Fill();
		  free(XMLEndPointer);
		  
		  RootFile->Write();
		  event->Clear("C");
		  delete tree;
		  delete TextTree;
		  //delete ErrorLog;
		  RootFile->Close();
		  
		  delete RootFile;
		  
		  
		  cout << "Closing File: " << OutPutFileName << endl;
		  insert_mysql_fast_DAQ_end_time(SlowControlTime);//,Type,Message2);
		  //AutoMove File
		  if(AutoMoveFiles)
		    {
		      sprintf(MoveCommand,"nice -n %d mv ./%s %s &",18,OutPutFileName,MovePath);
		      system(MoveCommand);
		    }
		  
		  SubRunNumber++;
		  sprintf(OutPutFileName,"run_%8.8d_%s_%4.4d.root",
			  RunNumber,
			  xMainNode.getChildNode("Run").getAttribute("Type"),
			  SubRunNumber
			  );   
		  ErrorLog->clear();
		  RootFile = new TFile(OutPutFileName, "RECREATE");
		  RootFile->SetCompressionLevel(atoi( xMainNode.getChildNode("Run").getChildNode("Compression").getAttribute("Level")) );
		  tree     = new TTree("tree","Run");
		  TextTree = new TTree("TextTree","RunNotes");
		  ErrorTree = new TTree("ErrorTree","ErrorLog");
		  tree->SetAutoSave(10000000);
		  tree->Branch("Event branch","Event",&event,64000,0);
		  TextTree->Branch("Run Notes","TextArray",&RootText);
		  ErrorTree->Branch("Error Log","std::vector<string>",&ErrorLog);

		  //Write start of run XML Test file
		  RootText->FillArray(XMLPointerSize,XMLPointer);
		  TextTree->Fill();
		  cout << "Opening File: " << OutPutFileName << endl;
		  insert_mysql_fast_DAQ_message(Message1,Type,OutPutFileName,Path,&SlowControlTime);
		  
		  //Reset ErrorLog
		  //sprintf(LogText,"");

		}
	    }
	  else
	    stoprun = true;

	  //Process new event. 
	  int Current_WaveformN = 0;
	  bool CorrectWindow = true;
	  eventtime = time(NULL);
	  //If the DAQ was set to only run for a set amount of time, check if we are past that.
	  if(TimePass != 0)
	    {
	      if(difftime(eventtime,starttime)  > TimePass)
		{
		  kill(getpid(),SIGINT);
		}
	    }
	  //Set the timestamp for the event
	  char * TimePointer = ctime(&eventtime);
	  event->SetHeader(eventN,RunNumber,TimePointer,30);
	  //Read out each WFD, WFD Channel and add them to an event structure
	  for(int i = 0; i < NumberOfWFDs;i++)
	    {
	      CurrentWFD = WFDs + i;
	      //HACK!!!!! GET THE WFD FIXED SO THIS CAN BE DONE ON EACH CHANNEL!
	      //This is to check if the WFD buffers are correct.
	      //If they aren't clear out the WFD to reset it. 
	      //Currently the WFD can't check the (0-3) 3rd channels blockcount
	      if(CurrentWFD->getWindowSize(0) == 2*CurrentWFD->blockcount(0) &&
		 CurrentWFD->getWindowSize(1) == 2*CurrentWFD->blockcount(1) &&
		 CurrentWFD->getWindowSize(2) == 2*CurrentWFD->blockcount(2) && 
		 CorrectWindow)
		{
		  for(int j = 0; j < 4 ;j++)
		    {
		      if(CurrentWFD->getWindowSize(j) >0)
			{
			  CurrentWFD->read_data(j,CurrentWFD->getWindowSize(j),CurrentWFD->getWindow(j));
			  event->AddWave();
			  event->SetID(Current_WaveformN,CurrentWFD->getChanID(j));
			  event->SetTrig(Current_WaveformN,0);
			  event->SetVOffset(Current_WaveformN,0);
			  event->SetVMult(Current_WaveformN,(1.0/255.0));
			  event->SetTick(Current_WaveformN,2E-9);
			  event->SetWaveform(Current_WaveformN,CurrentWFD->getWindowSize(j),CurrentWFD->getWindow(j));
			  Current_WaveformN++;
			}
		    }
		}
	      else
		{
		  //Force readout of all channels when one is bad.
		  if(CorrectWindow)
		    {
		      i = 0;
		    }
		   
		  cerr << "Event " << eventN << " Expected window size: " << CurrentWFD->getWindowSize(0) << " Actual window size: " << 2*CurrentWFD->blockcount(0) << endl;
		  sprintf(LogText,"Expected window size: %d Actual window size: %d\n",CurrentWFD->getWindowSize(0),2*CurrentWFD->blockcount(0));
		  ErrorLog->push_back(LogText);
		  CorrectWindow = false;
		  for(int j = 0; j < 4 ;j++)
		    {
		      if(CurrentWFD->getWindowSize(j) >0)
			{
			  for(int i = 0; i < 5;i++)
			    {
			      CurrentWFD->read_data(j,CurrentWFD->getWindowSize(j),CurrentWFD->getWindow(j));
			    }
			  Current_WaveformN++;
					  
			}
		    }
		}
	    }
	  //Write out Monitor event if it has been over two seconds
	  if(difftime(eventtime,LastMonitorEventTime) > 2)
	    {
	      //Try to get a lock for the MonitorFile
	      if(lock())
		{
		  //Got lock.  Now write monitor event
		  MonitorFile = new TFile(MonitorFileName,"RECREATE");
		  MonitorFile->SetCompressionLevel(0);
		  MonitorTree = new TTree("tree","run");
		  MonitorTree->Branch("Event branch","Event",&event,64000,0);
		  MonitorTree->Fill();
		  MonitorFile->Write();
		  delete MonitorTree;
		  MonitorFile->Close();
		  delete MonitorFile;
		  // Ok.  Wrote out everything and now we unlock the file so 
		  // the monitor can see it. 
		  unlock();
		  LastMonitorEventTime = eventtime;
		}
	    }

	  

	  if(!stoprun && CorrectWindow)
	    {
	      //Add the event ot the tree
	      eventN++;
	      tree->Fill();
	    }
	  else if(!CorrectWindow)
	    {
	      cerr << "Bad Windows.  Dropping event @#" << eventN << endl;
	      sprintf(LogText,"Bad Windows.  Dropping event @#%d\n",eventN);
	      ErrorLog->push_back(LogText);
	    }
	  event->Clear("C");
	  
	  newevent = false;
	  alarm(Timeout);
	  //restart DAQ and turn off veto.
	  crate->a32d32write(0x38383808,0x1);
	}
      usleep(MicroSecond);
    }
  //Stop Alarm if still active
  alarm(0);
  //Free innitial XML file pointer
  free(XMLPointer);

  //Max End of file XML string
  char * NEventsBuffer = new char[STRINGSIZE];	      
  sprintf(NEventsBuffer,"%d",eventN - 1 - (SubRunNumber*SUBRUNMAXEVENTS));
  xMainNode.getChildNode("Run").updateAttribute("true",NULL,"GoodRun");
  xMainNode.getChildNode("Run").getChildNode("Events").updateAttribute(NEventsBuffer,NULL,"N");
  sprintf(NEventsBuffer,"%d",RunNumber);  // Reusing to set run Number
  xMainNode.getChildNode("Run").updateAttribute(NEventsBuffer,NULL,"RunNumber");
  delete [] NEventsBuffer;
  
  //Write End of file XML string
  int XMLEndPointerSize =0;
  char * XMLEndPointer = xMainNode.createXMLString(true,&XMLEndPointerSize);
  RootText->FillArray(XMLEndPointerSize,XMLEndPointer);
  TextTree->Fill();
  //  ErrorLog->FillArray(strlen(LogText),LogText);
  ErrorTree->Fill();
  free(XMLEndPointer);
  RootFile->Write();
  RootFile->Close();
  cout << "Closing File: " << OutPutFileName << endl;

  //AutoMove File
  if(AutoMoveFiles)
    {
      sprintf(MoveCommand,"nice -n %d mv ./%s %s &",18,OutPutFileName,MovePath);
      system(MoveCommand);
    }

  //Tell Slow control we are done
  insert_mysql_fast_DAQ_end_time(SlowControlTime);//,Type,Message2);
  
  //Stop all the WFDs after Run
  for(int i = 0; i < NumberOfWFDs;i++)
    {
      CurrentWFD = WFDs + i;
      CurrentWFD->stop();
    }

  //Update RunNumber file
  RunNumberFile.open(RUNNUMBERFILENAME,ios::out);
  if(RunNumberFile.fail())
    {
      cerr << "Error: updating RunNumber" << endl;
      return(0);
    }
  else
    {
      RunNumberFile << RunNumber << endl;
    }
  RunNumberFile.close();
  
  //Data collection
  for(int i = 0; i < NumberOfWFDs*4;i++)
    {
      delete [] Buffer[i];
    }
  delete Buffer;

  eventtime = time(NULL);
  char * EndTimePointer = ctime(&eventtime);
  cout << "DAQ finished with " << eventN << "events at " << EndTimePointer << " ." << endl;


  delete OutPutFileName;
  delete MovePath;
  delete MoveCommand;


  return(0);
}
