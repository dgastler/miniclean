#include "FAKE_Control.h"

FAKE_Control::FAKE_Control()
{
  board[1].IsMaster = true;
}

bool FAKE_Control::Setup_Threads(std::vector<std::string> & files,
				 std::vector<std::string> & IPs)
{
  //Setup all the boards
  bool ret = true;
  for(size_t iBoard = 0; iBoard< 3;iBoard++)
    {
      //laod the events from disk
      ret = ret && (board[iBoard].Load_Data(files[iBoard]) > 0);
      //setup the network connections
      ret = ret && board[iBoard].Setup_Network(IPs[iBoard]);
    }  
  return ret;
}

uint32_t FAKE_Control::Get_Next_Time()
{
  freq_Lock.LockObject();
  double dt;                                                                    
  do                                                                            
    {                                                                           
      dt=Compute_dt();                                                          
    }while(dt < dead_Time);       
  
  freq_Lock.UnlockObject();
  return dt;    
}
double FAKE_Control::Compute_dt()
{
  //Compute next random number for this board
  //http://en.wikipedia.org/wiki/Linear_feedback_shift_register
  uint32_t bit = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
  lfsr = (lfsr >> 1) | (bit << 15);
  
  #ifdef ASYNC
  return convert*(log_N - log(lfsr));
  #else
  return convert;
  #endif
}

bool FAKE_Control::Setup_Random(double _frequency, double dead_Time)
{
  //lock random variables
  freq_Lock.LockObject();
  frequency = _frequency;
  dead_Time = dead_Time;
  log_N = log(double(0xFFFF));
  printf("Updating frequency to %1.0f\n",_frequency);
  #ifdef ASYNC
  convert = (1E6/frequency) - dead_Time;
  #else
  convert = (1E6/frequency);
  #endif

  //unlock random variables
  freq_Lock.UnlockObject();
  return true;
}

void * FAKE_Control::Run()
{
  event_Time=1E6;
  printf("Starting run.\n");
  //start board threads
  board[0].Start();
  board[1].Start();
  board[2].Start();

  uint32_t block_RAM_Address = 0;
  uint32_t block_RAM_Offset=0x100;
  uint32_t last_Valid_Block_Address = 0xC600;

  while(running)
    {
      int32_t index[3] = {-1,-1,-1};
      //Block on getting the next packet index from each ATLYS
      board[0].Packet_Indexes.GetFree(index[0]);
      board[1].Packet_Indexes.GetFree(index[1]);
      board[2].Packet_Indexes.GetFree(index[2]);      
      if((index[0] != index[1]) || (index[0] != index[2]) || (index[0] == -1))
	{
	  running = false;	  
	}
      else
	{	  
	  //Get raw packet
	  packet[0] = board[0].GetPacket(index[0]);
	  packet[1] = board[1].GetPacket(index[1]);
	  packet[2] = board[2].GetPacket(index[2]);

	  //generate the next time
	  event_Time += Get_Next_Time();
	  packet[0]->part1.eventTimeStamp = htonl(event_Time);
	  packet[1]->part1.eventTimeStamp = htonl(event_Time);
	  packet[2]->part1.eventTimeStamp = htonl(event_Time);

	  //update FPGA block address
	  if(block_RAM_Address > last_Valid_Block_Address)
	    {
	      block_RAM_Address = 0;
	    }
	  packet[0]->part1.memBlockAddress = htonl(block_RAM_Address);
	  packet[1]->part1.memBlockAddress = htonl(block_RAM_Address);
	  packet[2]->part1.memBlockAddress = htonl(block_RAM_Address);
	  block_RAM_Address += block_RAM_Offset;
	  packet[0]->part2.memBlockAddress = htonl(block_RAM_Address);
	  packet[1]->part2.memBlockAddress = htonl(block_RAM_Address);
	  packet[2]->part2.memBlockAddress = htonl(block_RAM_Address);
	  block_RAM_Address += block_RAM_Offset;	  
	}
      //Pass the event back to the ATLYS threads
      board[0].Packet_Indexes.AddFull(index[0]);
      board[1].Packet_Indexes.AddFull(index[1]);
      board[2].Packet_Indexes.AddFull(index[2]);
    }

  //Stop the threads
  board[0].Stop();
  board[1].Stop();
  board[2].Stop();
  
  //wait for the child threads to 
  board[0].Join();
  board[1].Join();
  board[2].Join();
  return NULL;
}

void * launch_FAKE_Control(void * ptr)
{
  return ((FAKE_Control *) ptr)->Run();
}
