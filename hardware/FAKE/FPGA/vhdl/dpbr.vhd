--Wrapper module for some block RAM's so that redundancy is reduced without
--using a Xilinx IP core to do the same thing (for consistency's sake).
--Signals ending in "A" are associated with the write-side and signals ending
--in "B" are associated with the read-side of the RAM. DOUTB changes a
--clock cycle after ADDRB changes--a situation that complicates almost every
--state machine in this design (especially when combined with subsequent delays).
--Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity dpbr is Port(
	RESET : in STD_LOGIC;
	CLKA : in  STD_LOGIC;
	WEA : in  STD_LOGIC;
	ADDRA : in  STD_LOGIC_VECTOR(11 downto 0);
	DINA : in  STD_LOGIC_VECTOR(7 downto 0);
	CLKB : in  STD_LOGIC;
	CLKEN : in STD_LOGIC;
	ADDRB : in  STD_LOGIC_VECTOR (11 downto 0);
	DOUTL : out STD_LOGIC_VECTOR(7 downto 0);
	DOUTB : out  STD_LOGIC_VECTOR(31 downto 0));
end dpbr;

architecture Behavioral of dpbr is
	signal dout1 : STD_LOGIC_VECTOR(31 downto 0);
	signal dout2 : STD_LOGIC_VECTOR(31 downto 0);
	signal dout : STD_LOGIC_VECTOR(31 downto 0);
	signal din : STD_LOGIC_VECTOR(31 downto 0);
	signal we : STD_LOGIC_VECTOR(3 downto 0);
	signal addr1 : STD_LOGIC_VECTOR(2 downto 0);
begin
	dout <= dout2 when addr1(2) = '1' else dout1;
	DOUTL <= dout(7 downto 0) when addr1(1 downto 0) = "00"
		else dout(15 downto 8) when addr1(1 downto 0) = "01"
		else dout(23 downto 16) when addr1(1 downto 0) = "10"
		else dout(31 downto 24);
	DOUTB <= dout;
	din <= DINA & DINA & DINA & DINA;
	we <= "000" & WEA when ADDRA(1 downto 0) = "00" else
		"00" & WEA & '0' when ADDRA(1 downto 0) = "01" else
		'0' & WEA & "00" when ADDRA(1 downto 0) = "10" else
		WEA & "000";

	process(CLKB, CLKEN)
	begin
		if CLKB'event and CLKB = '1' and CLKEN = '1' then
			addr1 <= ADDRB(11) & ADDRB(1 downto 0);
		end if;
	end process;

	memory1 : RAMB16BWER generic map(
		DATA_WIDTH_A => 36,
		DATA_WIDTH_B => 36,
		WRITE_MODE_A => "READ_FIRST",
		WRITE_MODE_B => "READ_FIRST") port map(
		CLKA => CLKA,
		WEA => we,
		ENA => not ADDRA(11),
		REGCEA => '0',
		ADDRA(13 downto 5) => ADDRA(10 downto 2),
		ADDRA(4 downto 0) => "00000",
		DIA => din,
		DIPA => x"0",
		DOA => open,
		DOPA => open,
		RSTA => RESET,
		CLKB => CLKB,
		WEB => x"0",
		ENB => CLKEN,
		REGCEB => '0',
		ADDRB(13 downto 5) => ADDRB(10 downto 2),
		ADDRB(4 downto 0) => "00000",
		DIB => x"00000000",
		DIPB => x"0",
		DOB => dout1,
		DOPB => open,
		RSTB => RESET);

	memory2 : RAMB16BWER generic map(
		DATA_WIDTH_A => 36,
		DATA_WIDTH_B => 36,
		WRITE_MODE_A => "READ_FIRST",
		WRITE_MODE_B => "READ_FIRST") port map(
		CLKA => CLKA,
		WEA => we,
		ENA => ADDRA(11),
		REGCEA => '0',
		ADDRA(13 downto 5) => ADDRA(10 downto 2),
		ADDRA(4 downto 0) => "00000",
		DIA => din,
		DIPA => x"0",
		DOA => open,
		DOPA => open,
		RSTA => RESET,
		CLKB => CLKB,
		WEB => x"0",
		ENB => CLKEN,
		REGCEB => '0',
		ADDRB(13 downto 5) => ADDRB(10 downto 2),
		ADDRB(4 downto 0) => "00000",
		DIB => x"00000000",
		DIPB => x"0",
		DOB => dout2,
		DOPB => open,
		RSTB => RESET);
end Behavioral;

