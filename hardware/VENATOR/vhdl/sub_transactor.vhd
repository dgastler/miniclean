--This module looks at the payload of incoming UDP packets and determines whether
--it requests that one of the four possible operations (read, write, bitmask, and sum)
--be performed on the information stored in top.vhd. If so, sub_transactor.vhd performs
--the operation and informs sub_packet_resp.vhd of the payload that should be included
--in the UDP response packet. This attention to the  Control System protocol fulfills
--the main, high-level purpose of this project.
--Important caveats: the base address should point to the base address of the
--UDP payload (not the beginning of the raw packet) and be 32-bit aligned at
--that point. This code does not take care of applying the proper UDP or Ethernet
--header (given the above specification).
--Original code by Jeremy Mans.
--Translation into VHDL along with modifications to the state machine to make it 
--decently efficient and other modifications to support fragmented UDP datagrams 
--by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sub_transactor is Port(
	CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	BUS_DATA_I : in unsigned(31 downto 0);
	BUS_DATA_O : out unsigned(31 downto 0);
	BUS_ADDR : out unsigned(31 downto 0);
	BUS_WRITE : out STD_LOGIC;
	BUS_STROBE : out STD_LOGIC;
	BUS_DTACK : in STD_LOGIC;
	BUS_BERR : in STD_LOGIC;
	PACKET_DATA_I : in unsigned(31 downto 0);
	PACKET_LEN_I : in unsigned(8 downto 0);
	PACKET_ADDR_I : out unsigned(8 downto 0);
	PACKET_DATA_O : out unsigned(31 downto 0);
	PACKET_ADDR_O : out unsigned(8 downto 0);
	PACKET_LEN_O : out unsigned(8 downto 0);
	PACKET_WE_O : out STD_LOGIC;
	NEW_PACKET : in STD_LOGIC;
	FRAGMENT_COUNT : out unsigned(10 downto 0);
	DONE : out STD_LOGIC_VECTOR(1 downto 0));
end sub_transactor;

architecture Behavioral of sub_transactor is
	signal endianness : STD_LOGIC;
	signal wordsTotal : unsigned(15 downto 0);
	signal words : unsigned(8 downto 0);
	signal transactId : unsigned(3 downto 0);
	signal ptr : unsigned(8 downto 0);
	signal temp_a : unsigned(31 downto 0);
	signal temp_b : unsigned(31 downto 0);
	signal temp_c : unsigned(31 downto 0);
	signal operation : unsigned(4 downto 0);
	signal packet_out : unsigned(31 downto 0);
	signal packet_in  : unsigned(31 downto 0);
	signal addri : unsigned(8 downto 0);
	signal addro : unsigned(8 downto 0);
	signal leno : unsigned(8 downto 0);
	signal donefied : STD_LOGIC_VECTOR(1 downto 0);
	signal baddr : unsigned(31 downto 0);
	type state_type is (st_idle, st_decode, st_nexti, st_isend, st_swap,
		st_unknown, st_done, st_write_begin, st_write_pre_addr, st_write_addr,
		st_write_data, st_write_wait, st_write_done_ok, st_write_done_err,
		st_read_begin, st_read_pre_addr, st_read_addr, st_read_data, st_read_wait,
		st_read_done_ok, st_read_done_err, st_read_done_final, st_rmw_begin,
		st_rmw_pre_addr, st_rmw_addr, st_rmw_op2, st_rmw_data, st_rmw_wait, 
		st_rmw_wait_ok, st_rmw_store1, st_rmw_store2, st_rmw_done_ok, 
		st_rmw_done_err, st_rmw_done_final, st_read_fragment, st_read_fragment_wait,
		st_read_fragment_pause, st_read_fragment_done);
	signal state : state_type := st_idle;
	constant cmd_swap : unsigned(4 downto 0) := "11111";
	constant cmd_read : unsigned(4 downto 0) := "00011";
	constant cmd_write : unsigned(4 downto 0) := "00100";
	constant cmd_rmwbits : unsigned(4 downto 0) := "00101";
	constant cmd_rmwsum : unsigned(4 downto 0) := "00110";
	constant mtu : unsigned(8 downto 0) := '1' & x"6F";
begin
	packet_in <= PACKET_DATA_I when endianness = '1' else PACKET_DATA_I(7 downto 0) & 
		PACKET_DATA_I(15 downto 8) & PACKET_DATA_I(23 downto 16) & PACKET_DATA_I(31 downto 24);
	PACKET_DATA_O <= packet_out when endianness = '1' else packet_out(7 downto 0) & 
		packet_out(15 downto 8) & packet_out(23 downto 16) & packet_out(31 downto 24);
	PACKET_ADDR_I <= addri;
	PACKET_ADDR_O <= addro;
	PACKET_LEN_O <= leno;
	DONE <= donefied;
	BUS_ADDR <= baddr;
	FRAGMENT_COUNT <= (wordsTotal(8 downto 0) & "00") when wordsTotal < ("0000000" & mtu) 
		else (mtu & "00");--assumes that it takes (at least) four clock cycles to read a word from memory

	process(CLK)
	begin
		if CLK'event and CLK= '1' then
			if RESET = '1' then
				donefied <= "00";
				PACKET_WE_O <= '0';
				BUS_WRITE <= '0';
				BUS_STROBE <= '0';
				state <= st_idle;
			else
				case state is
				when st_idle =>
					words <= '0' & x"00";
					wordsTotal <= x"0000";
					transactId <= x"0";
					ptr <= '0' & x"00";
					packet_out <= x"00000000";
					addri <= '0' & x"00";
					addro <= '0' & x"00";
					endianness <= '0';
					if NEW_PACKET = '1' then
						leno <= '0' & x"00";
						state <= st_decode;
					else
						state <= st_idle;
					end if;
				when st_decode =>
					transactId <= packet_in(27 downto 24);
					operation <= packet_in(7 downto 3);
					--endian change
					if (packet_in(31 downto 24) = x"F8") and (packet_in(7 downto 0) = x"00") then
						endianness <= not endianness;
						state <= st_decode;
					else
						case packet_in(7 downto 3) is
						when cmd_swap => state <= st_swap;
						when cmd_read => state <= st_read_begin;
						when cmd_write => state <= st_write_begin;
						when cmd_rmwbits => state <= st_rmw_begin;
						when cmd_rmwsum => state <= st_rmw_begin;
						when others => state <= st_unknown;
						end case;
					end if;
				when st_nexti =>
					PACKET_WE_O <= '0';
					addri <= addri + ('0' & x"01");
					state <= st_isend;
				when st_isend =>
					addro <= addro + ('0' & x"01");
					if addri >= PACKET_LEN_I then
						state <= st_done;
					else
						state <= st_decode;
					end if;
				when st_swap =>
					packet_out <= packet_in(31 downto 3) & "100";
					PACKET_WE_O <= '1';
					leno <= leno + ('0' & x"01");
					state <= st_nexti;
				when st_unknown =>
					packet_out <= packet_in(31 downto 3) & "111";
					PACKET_WE_O <= '1';
					leno <= leno + ('0' & x"01");
					state <= st_nexti;
				when st_done =>
					PACKET_WE_O <= '0';
					if (donefied = "00") or (NEW_PACKET = '1') then
						donefied <= "01";
						state <= st_done;
					else
						donefied <= "00";
						state <= st_idle;
					end if;
				when st_write_begin =>
					wordsTotal <= packet_in(23 downto 8);
					words <= '0' & x"00";
					addri <= addri + ('0' & x"01");
					BUS_WRITE <= '1';
					state <= st_write_pre_addr;
				when st_write_pre_addr =>
					--step to next data
					addri <= addri + ('0' & x"01");
					state <= st_write_addr;
				when st_write_addr =>
					baddr <= packet_in;
					state <= st_write_wait;
				when st_write_wait =>
					if BUS_DTACK = '0' then
						if ("0000000" & words) = wordsTotal then 
							state <= st_write_done_ok;
						else
							BUS_DATA_O <= packet_in;
							state <= st_write_data;
						end if;
					else
						state <= st_write_wait;
					end if;
				when st_write_data =>
					if BUS_BERR = '1' then
						BUS_STROBE <= '0';
						state <= st_write_done_err;
					elsif BUS_DTACK = '1' then
						BUS_STROBE <= '0';
						addri <= addri + ('0' & x"01");
						words <= words + ('0' & x"01");
						baddr <= baddr + x"00000001";
						state <= st_write_wait;
					else
						BUS_STROBE <= '1';
						state <= st_write_data;
					end if;			
				when st_write_done_ok =>
					packet_out <= x"0" & transactID & "0000000" & words & cmd_write & "100";
					PACKET_WE_O <= '1';
					leno <= leno + ('0' & x"01");
					state <= st_nexti;
				when st_write_done_err =>
					--doesn't handle PACKET_ADDR_I failures properly
					packet_out <= x"0" & transactID & "0000000" & words & cmd_write & "110";
					PACKET_WE_O <= '1';
					leno <= leno + ('0' & x"01");
					state <= st_done;--force to end, for safety's sake
				when st_read_begin =>
					BUS_WRITE <= '0';
					wordsTotal <= packet_in(23 downto 8);
					words <= '0' & x"00";
					ptr <= addro;--pointer to beginning of request
					--advance to the base address
					addri <= addri + ('0' & x"01");
					state <= st_read_pre_addr;
				when st_read_pre_addr =>
					state <= st_read_addr;
				when st_read_addr =>
					leno <= leno + ('0' & x"01");
					baddr <= packet_in;
					state <= st_read_wait;
				when st_read_wait =>
					PACKET_WE_O <= '0';
					if BUS_DTACK = '0' then
						if ("0000000" & words) = wordsTotal then
							BUS_STROBE <= '0';
							state <= st_read_done_ok;
						elsif words = mtu then--could not fit datagram in one Ethernet packet so it must
							wordsTotal <= wordsTotal - ("0000000" & words);--be held in multiple containers
							BUS_STROBE <= '0';
							state <= st_read_fragment;
						else
							BUS_STROBE <= '1';
							addro <= addro + ('0' & x"01");
							state <= st_read_data;
						end if;
					else
						BUS_STROBE <= '0';
						state <= st_read_wait;
					end if;
				when st_read_data =>
					if BUS_BERR = '1' then
						BUS_STROBE <= '0';
						PACKET_WE_O <= '0';
						state <= st_read_done_err;
					elsif BUS_DTACK = '1' then
						BUS_STROBE <= '0';
						packet_out <= BUS_DATA_I;
						PACKET_WE_O <= '1';
						words <= words + ('0' & x"01");
						baddr <= baddr + x"00000001";
						leno <= leno + ('0' & x"01");
						state <= st_read_wait;
					else
						BUS_STROBE <= '1';
						PACKET_WE_O <= '0';
						state <= st_read_data;
					end if;
				when st_read_fragment =>
					packet_out <= x"0" & transactId & "0000000" & words & cmd_read & "101";
					addro <= ptr;--switch writing address back to beginning of request
					PACKET_WE_O <= '1';
					state <= st_read_fragment_pause;
				when st_read_fragment_pause =>
					PACKET_WE_O <= '0';
					state <= st_read_fragment_done;
				when st_read_fragment_done =>
					if (donefied = "00") or (NEW_PACKET = '1') then
						donefied <= "11";
						state <= st_read_fragment_done;
					else
						donefied <= "10";
						state <= st_read_fragment_wait;
					end if;
				when st_read_fragment_wait =>
					words <= '0' & x"00";--zero everything out for next fragment
					ptr <= '0' & x"00";
					addro <= '0' & x"00";
					if NEW_PACKET = '1' then
						leno <= '0' & x"01";
						state <= st_read_wait;
					else
						state <= st_read_fragment_wait;
					end if;
				when st_read_done_ok =>--send out operation ID -after- operation completed
					packet_out <= x"0" & transactId & "0000000" & words & cmd_read & "100";
					addro <= ptr;--switch writing address back to beginning of request
					ptr <= addro;--and store the end address in a register
					PACKET_WE_O <= '1';
					state <= st_read_done_final;
				when st_read_done_err =>
					packet_out <= x"0" & transactId & "0000000" & words & cmd_read & "110";
					addro <= ptr;
					ptr <= addro;
					PACKET_WE_O <= '1';
					state <= st_read_done_final;
				when st_read_done_final =>
					PACKET_WE_O <= '0';
					addro <= ptr - ('0' & x"01");
					state <= st_nexti;
				when st_rmw_begin =>
					BUS_WRITE <= '0';
					--advance to the base address
					addri <= addri + ('0' & x"01");
					ptr <= addro;
					addro <= addro + ('0' & x"01");
					state <= st_rmw_pre_addr;
				when st_rmw_pre_addr =>
					--advance to the first op
					addri <= addri + ('0' & x"01");
					state <= st_rmw_addr;
				when st_rmw_addr =>
					baddr <= packet_in;
					if operation = cmd_rmwbits then
						--advance to the second op
						addri <= addri + ('0' & x"01");
						state <= st_rmw_op2;
					else
						state <= st_rmw_wait;
					end if;
				when st_rmw_op2 =>
					temp_b <= packet_in;
					state <= st_rmw_wait;
				when st_rmw_wait =>
					temp_a <= packet_in;--the first/second operand
					if BUS_BERR = '1' then
						BUS_STROBE <= '0';
						state <= st_rmw_done_err;
					elsif BUS_DTACK = '1' then
						BUS_STROBE <= '1';
						state <= st_rmw_data;
					else
						BUS_STROBE <= '1';
						state <= st_rmw_wait;
					end if;
				when st_rmw_data =>
					BUS_STROBE <= '0';
					if operation = cmd_rmwbits then
						temp_c <= (BUS_DATA_I and temp_b) or temp_a;
					else
						temp_c <= BUS_DATA_I + temp_a;
					end if;
					state <= st_rmw_store1;
				when st_rmw_store1 =>
					packet_out <= temp_c;
					BUS_DATA_O <= temp_c;
					BUS_WRITE <= '1';
					if BUS_DTACK = '0' then
						PACKET_WE_O <= '1';
						state <= st_rmw_store2;
					else
						PACKET_WE_O <= '0';
						state <= st_rmw_store1;
					end if;
				when st_rmw_store2 =>
					PACKET_WE_O <= '0';
					if BUS_BERR = '1' then 
						BUS_STROBE <= '0';
						state <= st_rmw_done_err;
					elsif BUS_DTACK = '1' then
						BUS_STROBE <= '0';
						state <= st_rmw_wait_ok;
					else
						BUS_STROBE <= '1';
						state <= st_rmw_store2;
					end if;
				when st_rmw_wait_ok =>
					BUS_WRITE <= '0';
					if BUS_DTACK = '0' then
						state <= st_rmw_done_ok;
					else
						state <= st_rmw_wait_ok;
					end if;
				when st_rmw_done_ok =>
					packet_out <= x"0" & transactId & x"0001" & operation & "100";
					addro <= ptr;
					ptr <= addro;
					PACKET_WE_O <= '1';
					state <= st_rmw_done_final;
				when st_rmw_done_err =>
					packet_out <= x"0" & transactId & x"0001" & operation & "110";
					addro <= ptr;
					ptr <= addro;
					PACKET_WE_O <= '1';
					state <= st_rmw_done_final;
				when st_rmw_done_final =>
					PACKET_WE_O <= '0';
					addro <= ptr - ('0' & x"01");
					leno <= leno  + ('0' & x"02");
					state <= st_nexti;
				end case;
			end if;
		end if;
	end process;
end Behavioral;
