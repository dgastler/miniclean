#ifndef __DCMESSAGEPROCESSOR__
#define __DCMESSAGEPROCESSOR__


#include <DCLauncher.h>
#include <DCThread.h>
#include <DCMessage.h>

#include <timeHelper.h>
#include <netHelper.h> //BuildSockAddrVector

#include <boost/algorithm/string.hpp> //For iequal

#include <sstream> //stringstrem

class DCMessageProcessor
{
 public:
  DCMessageProcessor(DCLauncher * _Launcher);
  ~DCMessageProcessor(){};
  //Find and process all the DCMessages from the threads.
  //Return will update the run status of the DAQ
  bool ProcessMessage(int selectReturn,
		      fd_set ReadSet,
		      fd_set WriteSet,
		      fd_set ExceptionSet);

  bool ProcessMessage(DCMessage::Message message);
  const std::string & GetName(){return(Name);};
 private:
  DCLauncher * Launcher;
  
  //Recursive error block
  uint8_t recursiveCount;
  uint8_t maxRecursiveCount;

  //Message processing functions
  void Launch(DCMessage::Message & message,bool & run);
  void RegisterAddr(DCMessage::Message & message,bool & run);
  void UnRegisterAddr(DCMessage::Message & message,bool & run);
  void RegisterType(DCMessage::Message & message,bool & run);
  void UnRegisterType(DCMessage::Message & message,bool & run);
  void Stop(DCMessage::Message & message,bool & run);
  void Go(DCMessage::Message & message,bool & run);
  void Pause(DCMessage::Message & message,bool & run);
  void RunNumber(DCMessage::Message & message,bool & run);
  void GetStats(DCMessage::Message & message,bool & run);

  DCThread * FindThread(std::string);

  //Map of remote addresses to local thread names
  //the index is a pair of AF type and address vectors
  std::map<uint32_t, std::string > messageAddrToNameMap;
  bool RouteRemoteMessage(DCMessage::Message message);
  bool RouteLocalMessage(DCMessage::Message message);

  //Map of remote addresses to registered functions
  //the index is a pair of AF type and address vectors
  std::map< uint16_t, std::string> messageTypeToNameMap;
  bool RouteMessageType(DCMessage::Message message);
  
  //Map of message types to registered functions
  //the index is a DCMessageType
  std::map<uint16_t, void (DCMessageProcessor::*)(DCMessage::Message &,bool &) > messageTypeToFunctionMap ;
  //  bool FindMessageFunction(DCMessage::Message message);


  void InstallPrintErrorThread();

  void SendRemoteSetupMessage(std::string setupText, unsigned int remoteNodeNumber);


  std::string Name;
};

#endif
