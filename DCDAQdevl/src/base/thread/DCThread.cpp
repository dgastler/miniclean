#include <DCThread.h>

DCThread::DCThread()
{
  //Thread/run loop control
  Ready = false;
  Running = false;
  Loop = false;
  
  //Book keeping values
  RunNumber = -1;
  ThreadID = 0;      
  ThreadResults = NULL;      
  ThreadName = "unassigned";
  ThreadType = "unassigned";
  UpdateTime = 60;              //Seconds
    
  //Select parameters
  FD_ZERO(&ReadSet);            //Empty
  FD_ZERO(&WriteSet);           //Empty
  FD_ZERO(&ExceptionSet);       //Empty
  DefaultSelectTimeout.tv_sec = UpdateTime;   //seconds
  DefaultSelectTimeout.tv_usec = 0;
  SelectTimeout = DefaultSelectTimeout;

  //Set the bad message type in the IN/OUT queues
  badDCDAQMessage.SetType(DCMessage::BLANKMESSAGE);
  MessagesIn.Setup(badDCDAQMessage);
  MessagesOut.Setup(badDCDAQMessage);

  //Setup select
  SetupSelect();

}

/*
  This is the function that is the primary loop of the DCThread thread.
  It can run in two different ways.   
    1. SelectRunningMode(true)
       This is how almost all threads should run, so if you don't know what
       this means, ONLY USE THIS MODE!
       In this mode you write MainLoop() that is called when a non-DCDAQ message queue
       file descriptor has been opened for reading or writing. 
       After MainLoop() finishes, this code goes back to blocking on select.

    2. SelectRunningMode(false)
       You better know what you are doing and why you are using this mode. 
       This is for when you want total control of all thread flow in MainLoop.
       YOU have to manage the incomming DCDAQ message queue yourself when
       Loop is true.   
       This is needed when you are effectively polling some resouce. 
       (ie reading out the WFDs over the VME bus.)
       Again, YOU must manage the incomming DCDAQ message queue and respond
       to it's messages appropriately. 

  Running loop handles basic DCDAQ messaging for start/pause/stop and calls the
  MainLoop() function to perform the inherited class's job.
 */
void *DCThread::Run(void * /*Arg*/)
{
  while(Running)
    {
      //===================================================================
      //===================DCThread running loop===========================
      //===================Just listening for messages from DCDAQ==========
      //===================No thread specific code may be running!=========
      //===================================================================

      //If this is a thread that isn't running by default (Loop = true), then we should
      //clear any select FDs it may want to listen to. 
      if(!Loop)
	{
	  ClearSelect();   
	}
      
      //Block on select for something to happen.
      SelectTimeout = DefaultSelectTimeout;  //select will modify SelectTimeout.
      //reset read/write/exception sets
      retReadSet = ReadSet;                  
      retWriteSet = WriteSet;                
      retExceptionSet = ExceptionSet;        
      //block in select
      int selectReturn = select(MaxFDPlusOne,
				&retReadSet,
				&retWriteSet,
				&retExceptionSet,
				&SelectTimeout);            
      if(selectReturn > 0) //We can use one of our FDs
	{
	  //Check for an incomming message from DCDAQ
	  if(IsReadReady(GetMessageInFDs()[0]))	    
	    {
	      DCMessage::Message message =  GetMessageIn(true);
	      //Pass the message to the derived class's ProcessMessage
	      //if it returns false, then it didn't understand the message
	      //and we should call the DCThread version of ProcessMessage
	      if(!ProcessMessage(message))
		{
		  DCThread::ProcessMessage(message);
		}
	    }
	}
      else if(selectReturn == 0)
	{
	  ProcessTimeout();
	}
      else
	{
	  if(!selectErrorIgnore->IgnoreSelectError(errno))
	    {
	      printf("Warning(Running):  %s:%s received select errror(%s).\n",
		     ThreadType.c_str(),
		     ThreadName.c_str(),
		     strerror(errno));	  
	    }
	}

      while(Loop) 
	{
	  //===================================================================
	  //===================Specific thread main loop=======================
	  //===================================================================
	  //reset read/write/exception sets and reset select's timeout
	  SelectTimeout = DefaultSelectTimeout;  
	  retReadSet = ReadSet;                  
	  retWriteSet = WriteSet;                
	  retExceptionSet = ExceptionSet;        
	  //Block on select for something to happen.
	  int selectReturn = select(MaxFDPlusOne,
				    &retReadSet,
				    &retWriteSet,
				    &retExceptionSet,
				    &SelectTimeout);            
	  if(selectReturn > 0) //We can use one of our FDs
	    {
	      //Check for an incomming message from DCDAQ
	      if(IsReadReady(GetMessageInFDs()[0]))
		{
		  DCMessage::Message message =  GetMessageIn(true);
		  //Pass the message to the derived class's ProcessMessage
		  //if it returns false, then it didn't understand the message
		  //and we should call the DCThread version of ProcessMessage
		  if(!ProcessMessage(message))
		    {
		      DCThread::ProcessMessage(message);
		    }
		  
		  //Check if we should be updating DCDAQ
		  ProcessTimeout();
		}
	      //For anything else, call MainLoop.
	      else
		{
		  //Run the thread's main loop
		  MainLoop();
		  //Check if we should be updating DCDAQ
		  ProcessTimeout();
		}
	    }
	  else if(selectReturn == 0)
	    {
	      //Check if we should be updating DCDAQ
	      ProcessTimeout();
	    }	  
	  else
	    {	      
	      if(!selectErrorIgnore->IgnoreSelectError(errno))
		{
		  printf("Warning(MainLoop):  %s:%s received select errror(%s).\n",
			 ThreadType.c_str(),
			 ThreadName.c_str(),
			 strerror(errno));
		}	      
	      if(errno == EBADF)
		{
		  for(int i = 0;i < MaxFDPlusOne;i++)
		    {
		      if(FD_ISSET(i,&ReadSet))
			{
			  printf("   Selecting on FD: %u:%u\n",i,MaxFDPlusOne);
			}
		    }
		  //Don't do this!
		  Loop = false;
		  Running = false;
		}
	    }
	  //===================================================================
	  //===================Specific thread main loop end===================
	  //===================================================================	  
	} //MainLoop      
      //===================================================================
      //===================DCThread running loop end=======================
      //===================================================================
    }
  //Function called at the end of the thread execution.
  //Use this to clean up something special you made
  //(like child threads)
  ThreadCleanUp();
  return(NULL);
}
bool DCThread::Start(void * /*Arg*/)
{
  //If things aren't setup don't make the thread.
  if(!Ready)
    {
      printf("Warning: Can not create thread %s(%s) because thread wasn't setup yet.\n",GetName().c_str(),GetType().c_str());
      return(false);
    }
  if(Running)
    {
      printf("Warning: Can not creat thread because thread is already running.\n");
      return(false);
    }
  //Need Running to be true before pthread_create
  Running = true;
  if(pthread_create(&ThreadID,NULL,start_thread,this))
    {
      fprintf(stderr,"Error creating consumer thread!\n");
      Running = false;
    }

  return(true);
}

void * DCThread::End(bool Wait)
{
  ThreadResults = NULL;
  //Request that the thread end
  if(IsRunning())
    {
      //Shutdown thread
      //      pthread_cancel(ThreadID);
      if(Wait)
	{
	  pthread_join(ThreadID,&ThreadResults);
	}
    }
  return(ThreadResults);
}

DCMessage::Message DCThread::GetMessageOut(bool Wait)
{
  DCMessage::Message ret;
  MessagesOut.Get(ret,Wait);
  return(ret);
}
DCMessage::Message DCThread::GetMessageIn(bool Wait)
{
  DCMessage::Message ret; 
  MessagesIn.Get(ret,Wait);
  return(ret);
}
bool DCThread::SendMessageIn(DCMessage::Message &message,bool Wait) 
{
  //Add a message to the queue.
  return(MessagesIn.Add(message,Wait));
}

bool DCThread::SendMessageOut(DCMessage::Message &message,bool Wait)
{
  //Adding source information (default remote address is set by DCMessage)
  message.SetSource(GetName());
  //Add a message to the queue.
  return(MessagesOut.Add(message,Wait));
}

/*
  Function used to add a file descriptor to the fd vector.
  This has the added benefit that it won't duplicate an existing entry.  
 */
void DCThread::AddToVectorWODup(int value,std::vector<int> &vec)
{
  if(value >= 0)
    {
      bool found = false;
      //Search vec for value
      for(unsigned int i = 0;((!found)&&(i < vec.size()));i++)
	{if(vec[i] == value) {found = true;}}
      if(!found) //if value was not found add it to vec
	{vec.push_back(value);}
    }
}
/*
  Function used to remove an FD from our fd vectors.
 */
void DCThread::RemoveFromVector(int value,std::vector<int> &vec)
{
  std::vector<int>::iterator it = vec.begin();
  //iterate through the vector until we reach the end or find "value"
  for(;it != vec.end();it++)
    {if((*it) == value){break;}}
  //If "it" isn't vec.end(), then we found val and we should remove it. 
  if(it != vec.end())
    {vec.erase(it);}
}

/*
  ClearSelect(): This functions removes all fds from the list select 
  listen's to.  It then calls SetupSelect to make these changes take
  effect.   SetupSelect will add back in the InMessageQueue's fd.
 */
void DCThread::ClearSelect()
{
  //Clear all fds from our select fd lists
  ReadFDs.clear();       
  WriteFDs.clear();
  ExceptionFDs.clear();
  //Call setup select to have these changes take effect.
  //InMessageQueue's fd will be added in SetupSelect()
  SetupSelect();
}

/*
  SetupSelect(): This function sets up all of the values needed for select 
  to properly work. It also makes sure that the read message queue's 
  fd is ALWAYS listened to. 
 */
void DCThread::SetupSelect()
{
  //Clear all FDs from Read/Write/Exception sets.
  FD_ZERO(&ReadSet);
  FD_ZERO(&WriteSet);
  FD_ZERO(&ExceptionSet);

  //Add MessageInPipeFD (we ALWAYS listen to this!)
  AddReadFD(GetMessageInFDs()[0]);

  //Zero out so that we can find the new maximum FD
  MaxFDPlusOne = 0;

  //Add all FDs in ReadFDs to ReadSet
  for(unsigned int i = 0; i < ReadFDs.size();i++)
    {
      FD_SET(ReadFDs[i],&ReadSet);
      if(ReadFDs[i] > MaxFDPlusOne)
	{MaxFDPlusOne = ReadFDs[i];}
    }
  //Add all FDs in WriteFDs to WriteSet
  for(unsigned int i = 0; i < WriteFDs.size();i++)
    {
      FD_SET(WriteFDs[i],&WriteSet);
      if(WriteFDs[i] > MaxFDPlusOne)
	{MaxFDPlusOne = WriteFDs[i];}
    }
  //Add all FDs in ExceptionFDs to ExceptionSet
  for(unsigned int i = 0; i < ExceptionFDs.size();i++)
    {
      FD_SET(ExceptionFDs[i],&ExceptionSet);
      if(ExceptionFDs[i] > MaxFDPlusOne)
	{MaxFDPlusOne = ExceptionFDs[i];}
    }
  
  //Add one to the max file descriptor
  MaxFDPlusOne++;
}


void DCThread::SetSelectTimeout(long seconds,long useconds)
{
  DefaultSelectTimeout.tv_sec = seconds;
  DefaultSelectTimeout.tv_usec = useconds;
  SelectTimeout = DefaultSelectTimeout;
}

void DCThread::PrintError(const char * text)
{  
//  fprintf(stderr,"Error(%s:%s): %s\n",
//	  GetType().c_str(),
//	  GetName().c_str(),
//	  text);
  DCMessage::Message errorMessage;
  errorMessage.SetType(DCMessage::ERROR);
  errorMessage.SetDestination();
  
  DCMessage::SingleUINT64 errorData;
  sprintf(errorData.text,"%s\n",
	  text);
  errorMessage.SetDataStruct(errorData);
  SendMessageOut(errorMessage,true);
}
void DCThread::PrintWarning(const char * text)
{
  fprintf(stderr,"Warning(%s:%s): %s\n",
	  GetType().c_str(),
	  GetName().c_str(),
	  text);
}
void DCThread::SendStop(bool PrintStackTrace)
{
  if(PrintStackTrace == true)
    {
      //Print the current stack trace
      print_trace();
    }
  //Shutdown DCDAQ
  PrintError("STOP!\n");
  DCMessage::Message message;
  message.SetType(DCMessage::STOP);
  SendMessageOut(message,true);
  //Clear the select set
  ClearSelect();
  //Resetup select
  SetupSelect();
  Loop = false;
}

//Basic message processor
bool DCThread::ProcessMessage(DCMessage::Message &message)
{
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      if(Ready)
	Loop = true;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      Loop = false;
    }
  else if(message.GetType() == DCMessage::RUN_NUMBER)
    {
      DCMessage::SingleINT64 data;
      if(message.GetDataStruct(data))
	{
	  SetRunNumber(data.i);
	}
    }
  else
    {
      printf("Warning:    %s unknown DCDAQ message type %u.\n",
	     GetName().c_str(),
	     message.GetType());
      return(false);
    }
  return(true);
}

void DCThread::PrintStatus()
{
  printf("Thread: %s:%s is %sready, %srunning and %slooping\n",
	 GetType().c_str(),
	 GetName().c_str(),
	 (Ready)?"":"not ",
	 (Running)?"":"not ",
	 (Loop)?"":"not ");
}

void DCThread::SetDefaultSelectTimeout(long int tv_sec, long int tv_usec)
{
  DefaultSelectTimeout.tv_sec  = tv_sec;
  DefaultSelectTimeout.tv_usec = tv_usec;
}
