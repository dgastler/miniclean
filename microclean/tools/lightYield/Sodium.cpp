//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TObjArray.h"
#include "TSpline.h"

using namespace std;

TSpline * SplinePointer;


TH1D * SmearedSodiumSpectrum(TH1D * MCSpectrum)
{
  TH1D * SmearedNaSpectrum = new TH1D("SmearedNaSpectrum",
				      "Smeared Na spectrum",
				      MCSpectrum->GetNbinsX(),
				      MCSpectrum->GetXaxis()->GetXmin(),
				      MCSpectrum->GetXaxis()->GetXmax()*1.2);
  
   
  TF1 * SmearingFunction = new TF1("SmearingFunction","gaus",-100,2000);
  //Vector to contain the new energy of each even after smearing. 
  vector<double> EntryEnergies;
  //loop over all bins in this histogram
  for(int bin = 1;bin <= MCSpectrum->GetNbinsX();bin++) 
    {
      //Set smearing function.
      //A gaussian centered around the current energy bin with width of the sqrt of the enrgy bin.
      SmearingFunction->SetParameters(1,
				      MCSpectrum->GetBinCenter(bin),
				      sqrt(5*MCSpectrum->GetBinCenter(bin)));
      for(int entry = 0;entry < MCSpectrum->GetBinContent(bin);entry++)
	{
	  EntryEnergies.push_back(SmearingFunction->GetRandom());
	}
    }
  delete SmearingFunction;
  //Fill smeared histogram with smeared events.
  for(unsigned int entry = 0; entry < EntryEnergies.size();entry++)
    {
      SmearedNaSpectrum->Fill(EntryEnergies[entry]);
    }
  return(SmearedNaSpectrum);
}






TSpline3 * MakeSpline(TH1D * Spectrum)
{
  //Build a graph to make the spline
  TGraph * gr = new TGraph(Spectrum->GetNbinsX());
  for(int bin = 1; bin <= Spectrum->GetNbinsX();bin++)
    {
      gr->SetPoint(bin,Spectrum->GetBinCenter(bin),Spectrum->GetBinContent(bin));
    }
  //Make the spline
  TSpline3 * Spline = new TSpline3("SplineFit",gr);
  delete gr;
  return(Spline);
}





Double_t FitFunction(Double_t *x,Double_t *par) 
{
  if(SplinePointer)
    {
      if(x[0]/par[1] > SplinePointer->GetXmin() && x[0]/par[1] < SplinePointer->GetXmax())
	{
	  Double_t ret = par[0]*SplinePointer->Eval(x[0]/par[1]);
	  return(ret);
	}
      else
	return(0);
    }
  return(0);
}








int main(int argc, char ** argv)
{
  SplinePointer = 0;
  if(argc < 3)
    {
      cout << argv[0] << " SimFile inFiles" << endl;
      return(0);
    }

  TFile * InFile = 0;

  TFile * SimFile = TFile::Open(argv[1]);
  if(!SimFile)
    {
      fprintf(stderr,"Bad input file.\n");
      return(0);
    }
  //Get the MC sodium spectrum
  TH1D * NaSpectrumMC = (TH1D*) SimFile->Get("h_Edep");
  if(!NaSpectrumMC)
    {
      fprintf(stderr,"Bad Na MC histogram\n");
      return(0);
    }
  
  //Apply a gaussian smearing to the MC spectrum
  TH1D * SmearedNaSpectrum = SmearedSodiumSpectrum(NaSpectrumMC);
  //Smooth histogram
  SmearedNaSpectrum->Rebin(16);
  //normalize histogram.
  SmearedNaSpectrum->Scale(1.0/SmearedNaSpectrum->Integral());

  //Make Spline
  SplinePointer = MakeSpline(SmearedNaSpectrum);

  //Make fitting function
  //Par[0] = Height scale
  //Par[1] = light yield
  TF1 * fit = new TF1("fit",FitFunction,-100,20000,2);

  vector<TH1F*> Fits;
  vector<float> LightYields;
  vector<float> LightYieldErrors;
  vector<int> Run;

  for(int InFileNumber = 2; InFileNumber < argc;InFileNumber++)
    {
      InFile = TFile::Open(argv[InFileNumber]);
      if(!InFile)
	{
	  fprintf(stderr,"Bad infile: %s",argv[InFileNumber]);
	  return(0);
	}
      
      TH1F * InHist = (TH1F *) InFile->Get("cLight");
      if(!InHist)
	{
	  fprintf(stderr,"Bad Histogram: %s",argv[InFileNumber]);
	  return(0);
	}
      //Reset title
      std::string RunNumber(argv[InFileNumber]);
      RunNumber = RunNumber.substr(RunNumber.find("run_")+4,8);
      std::string Title = std::string("Run ") + RunNumber; 
      InHist->SetTitle(Title.c_str());
      InHist->SetName(Title.c_str());
      //Smooth histogram
      //      InHist->Rebin(2);
      //Guess 2pe/kev
      //      printf("%f\n",			 InHist->GetBinCenter(InHist->GetMaximumBin())/SmearedNaSpectrum->GetBinCenter(SmearedNaSpectrum->GetMaximumBin()));
      fit->SetParameters(InHist->Integral()/SmearedNaSpectrum->Integral(),
			 InHist->GetBinCenter(InHist->GetMaximumBin())/SmearedNaSpectrum->GetBinCenter(SmearedNaSpectrum->GetMaximumBin())
			 //2.0
			 );
      //InHist->GetListOfFunctions()->AddLast(fit);
      InHist->Fit(fit,"Q","",
		  InHist->GetBinCenter(InHist->GetMaximumBin())/2.0,
		  //      		  InHist->GetBinCenter(InHist->GetNbinsX()));
		  //		  600);
		  InHist->GetBinCenter(InHist->GetMaximumBin())*1.5);
      Fits.push_back(InHist);

      Run.push_back(atoi(RunNumber.c_str()));
      LightYields.push_back(fit->GetParameter(1));
      LightYieldErrors.push_back(fit->GetParError(1));
      printf("Run: %d  Yield: %f\n",Run[Run.size()-1],LightYields[LightYields.size()-1]);
    }


  TFile * OutFile = new TFile("Cal.root","CREATE");
  if(!OutFile)
    {
      fprintf(stderr,"Output file already exists!\n");
      return(0);
    }
  NaSpectrumMC->Write();
  SmearedNaSpectrum->Write();
  TGraph * gr = new TGraph(LightYields.size());
  float LightYield = 0;
  float Error = 0;
  for(int i = 0; i < LightYields.size();i++)
    {
      (Fits[i])->Write();
      gr->SetPoint(i,Run[i],LightYields[i]);
      LightYield += LightYields[i];
      Error += LightYields[i]*LightYields[i];
    }
  Error = (Error - LightYield*LightYield/LightYields.size())/(LightYields.size()-1);
  Error = sqrt(Error);
  LightYield /= LightYields.size();
  gr->Write();
  OutFile->Close();
  delete OutFile;
  
  //Generate XML file
  fstream CalFile;
  CalFile.open("Cal.xml",fstream::trunc | fstream::out);
  CalFile << "<CAL keVee=\"" 
	  << LightYield 
	  << "error=\""
	  << Error
	  << "\"/>" 
	  << endl;
  CalFile.close();

  printf("Light yield = %f +/- %fpe/kev\n",LightYield,Error);

  return(0);
}
