#ifndef RUNCONTROL_XML_H
#define RUNCONTROL_XML_H

//#include <libxml/tree.h>
//#include <libxml/parser.h>
#include <vector>
#include <string>

#include <DCMySQL.h>

#include <XML_Factory.h>

#include <Thread_XML.h>
#include <V1720_XML.h>
#include <DecisionMaker_XML.h>
#include <NetThread_XML.h>

#include <boost/algorithm/string.hpp>

class XML_Factory;


//=============================================
//Class RunControl_XML inherits from Thread_XML
//This way it can call functions 
//like Get_Thread_XML from Thread_XML class.
//=============================================

class RunControl_XML: public Thread_XML
{
 public:
  //Constructor initializes mysql connection for RunControl!
  RunControl_XML(DCMySQL *conn,int _runNumber,XML_Factory * fac);

  //=================================
  //RunControl Version of Get_XML
  //==================================
  virtual bool Generate_XML(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responseNode);
  
private:
  //=========================================
  //Set up DCMessageClient Connections to all
  //DCDAQ Machines dcdaq_connections table
  //=========================================
  bool CreateConnections();
  //====================================
  //Can create "start", "before_threads"
  //and "end" messages
  //====================================
  bool CreateMessages(std::string msgType);
  bool CreateShutdown();
  //=======================
  //Sets up remote threads & memorymanagers
  //=======================
  bool CreateRemoteXML(std::string PC,std::string Addr);

  XML_Factory * parentFactory;
};

#endif
