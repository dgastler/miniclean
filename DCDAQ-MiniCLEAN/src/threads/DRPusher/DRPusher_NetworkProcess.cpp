#ifdef __RATDS__
#include <DRPusher.h>

 
 void DRPusher::AddToPacket(RAT::DS::Board * board,
			    uint8_t * &currentPacketDataPtr,
			    uint16_t &packetSizeLeft,
			    int16_t PMTID)
{
  //Add board header
  DRPacket::Board * drBoard = (DRPacket::Board *) currentPacketDataPtr;
  //Move forward in the packet
  currentPacketDataPtr += sizeof(DRPacket::Board);
  packetSizeLeft -= sizeof(DRPacket::Board);

  //Fill header
  std::vector<uint32_t> header = board->GetHeader();
  drBoard->LVDSPattern = uint16_t ((header[1] >> 8)&0xFFFF);
  drBoard->Time = header[3];
  drBoard->BoardID = uint8_t( (header[1]>>27) & 0x1F );

  drBoard->ChannelPattern = 0;

  int wfdChannelsProcessed = 0;
  while(wfdChannelsProcessed < board->GetChannelCount())
    {
      RAT::DS::Channel * channel = board->GetChannel(wfdChannelsProcessed);

      if(!promptTotalMode)
	//We are using ZLE integrals for DR decisions
	{
	  //Add the channel header
	  DRPacket::Channel * drChannel = (DRPacket::Channel*) currentPacketDataPtr;
	  currentPacketDataPtr += sizeof(DRPacket::Channel); 
	  packetSizeLeft -= sizeof(DRPacket::Channel); 
	  
	  //Add the raw blocks
	  int rawBlockCount = channel->GetRawBlockCount();
	  drChannel->RawIntegralCount = 0;
	  //Add this channel to the channel pattern mask 
	  //(remember that the channels are in order from FEPCAssembler)
	  drBoard->ChannelPattern |= uint8_t(0x1 << channel->GetInput());   
	  
	  //Add the raw integrals
	  for(int iRawBlock = 0;
	      iRawBlock < rawBlockCount;
	      iRawBlock++)
	    {
	      //Get the the current RAT rawIntegral
	      RAT::DS::RawBlock * rawBlock= channel->GetRawBlock(iRawBlock);
	      //Get a pointer to a DRPacket RawIntegral type that points to the next
	      //available spot in the packet array
	      DRPacket::RawIntegral * drRawIntegral = 
		(DRPacket::RawIntegral *) currentPacketDataPtr;
	      currentPacketDataPtr += sizeof(DRPacket::RawIntegral); 
	      packetSizeLeft -= sizeof(DRPacket::RawIntegral); 
	      
	      //Copy data
	      //	  drRawIntegral->integral    = uint16_t(rawBlock->GetIntegral());
	      drRawIntegral->integral    = rawBlock->GetIntegral();
	      drRawIntegral->startTime   = rawBlock->GetOffset();
	      drRawIntegral->width       = rawBlock->GetWidth();      
	      
	      drChannel->RawIntegralCount++;
	    }                
	}
      else
	//We are using prompt/total information for DR decisions.
	{
	  float promptIntegral = 0;
	  double totalIntegral = 0;

	  DRPacket::Channel * drChannel = (DRPacket::Channel *) currentPacketDataPtr;
	  currentPacketDataPtr += sizeof(DRPacket::Channel);
	  packetSizeLeft -= sizeof(DRPacket::Channel);
	  drChannel->RawIntegralCount = 1; //prompt total only has one block

	  //Add this channel to the ChannelPattern mask
	  drBoard->ChannelPattern |= uint8_t(0x1 << channel->GetInput());

	  DRPacket::PromptTotalBlock * drPromptTotal = (DRPacket::PromptTotalBlock *) currentPacketDataPtr;
	  currentPacketDataPtr += sizeof(DRPacket::PromptTotalBlock);
	  packetSizeLeft -= sizeof(DRPacket::PromptTotalBlock);      
	  
	  uint32_t eventStartTime = detectorInfo.GetEventStartTime();
	  uint32_t promptIntegrationEndTime = detectorInfo.GetPromptIntegrationEndTime();
	  double channelOffset = detectorInfo.GetPMTOffset(PMTID);
	  
	  for(int iRawBlock = 0; iRawBlock < channel->GetRawBlockCount();iRawBlock++)
	    {
	      RAT::DS::RawBlock * rawBlock = channel->GetRawBlock(iRawBlock);
	      std::vector<uint16_t> waveform = rawBlock->GetSamples();
	      uint32_t sampleTime = rawBlock->GetOffset();
	      
	      for(size_t iSample = 0; iSample < waveform.size();iSample++)
		{
		  //If we are in the valid event time of the waveform
		  if(sampleTime > eventStartTime)	    
		    {	      
		      float sample = channelOffset - waveform[iSample];
		      //Add charge to prompt integral if in prompt time
		      if(sampleTime < promptIntegrationEndTime)
			{
			  promptIntegral+=sample;
			}
		      //Add charge to total integral
		      totalIntegral+=sample;
		      
		    }
		  sampleTime++;
		}
	    }
	  
	  drPromptTotal->prompt  = promptIntegral;
	  drPromptTotal->total   = totalIntegral; 
	}
      wfdChannelsProcessed++;
    }
}

#endif
