#include <v1720FakeReadout.h>

void  v1720FakeReadout::MainLoop()
{
  //  if(IsReadReady(Mem->GetFreeFDs()[0]) &&
  //     (blockIndex == FreeFullQueue::BADVALUE))
  if(blockIndex == FreeFullQueue::BADVALUE)
    {
      //Get new memory block
      Mem->GetFree(blockIndex,true);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = &(Mem->Memory[blockIndex]);
	  block->dataSize = 0;
	}
    }
  
  if(blockIndex != FreeFullQueue::BADVALUE)
    {
      uint32_t eventCount = 0;      
      while(eventCount < EventsPerBlock)
	{
	  uint32_t * event = FakeEvent[iEvent];//get event
	  uint32_t eventSize = event[0]&0x0FFFFFFF;//get this event's size	      
	  if(((block->dataSize>>2) + eventSize) <= (block->allocatedSize>>2))
	    {
	      //Get a pointer to part of the Block buffer we are goign to write to.
	      uint32_t * bufferPointer = (uint32_t * )(block->buffer + block->dataSize);
	      //Copy from event to bufferPointer eventsize*4 bytes.
	      memcpy(bufferPointer,event,eventSize<<2);
	      //Increse the event number
	      bufferPointer[2] += EventCountOffset;
	      //Set block's data size to include this event
	      block->dataSize += (eventSize <<2);		  
	      //Move to the next event.
	      eventCount++;
	      iEvent++;
	      //If we are at the end of our loaded events go back to zero.
	      if(iEvent >= FakeEvent.size())
		{
		  EventCountOffset+=FakeEventCount;
		  if(MemoryLoop)
		    {
		      iEvent = 0;
		    }
		  else
		    {
		      printf("v1720FakeReadout:   Sent all events (%d)\n",iEvent);
		      Loop = false;
		      
		      RemoveReadFD(Mem->GetFreeFDs()[0]);
		      SetupSelect();
		      eventCount = EventsPerBlock+1;
		    }
		}
	    } //Add event
	  else
	    {
	      break;
	    }	      
	}//Fill block
      //Pass off this full block.
      Mem->AddFull(blockIndex);
      //      fprintf(stderr,"v1720FakeReadout: wrote out a block\n");
      TimeStep_BlocksRead++; 
    }
}

void  v1720FakeReadout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      BlocksRead += TimeStep_BlocksRead;
      DCMessage::SingleUINT64 Count;

      //Data message
      sprintf(Count.text,"Blocks read");
      Count.i=TimeStep_BlocksRead;
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);

      TimeStep_BlocksRead = 0;

      //Setup next time
      lastTime = currentTime;
    }
  if(Period!=0)
    {
      MainLoop();
    }
}
