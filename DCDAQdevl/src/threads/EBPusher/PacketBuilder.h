#ifndef __PACKETBUILDER__
#define __PACKETBUILDER__

#include <stdint.h>
#include <ReductionLevel.h>
#include <FEPCBlock.h>
#include <DCNetPacketManager.h>
#include <EBDataPacket.h>

#include <math.h>

class PacketBuilder
{
 public:
  PacketBuilder()
    {
      packetTypeFull = ReductionLevel::UNKNOWN;
      packetTypePartial = ReductionLevel::UNKNOWN;
      ebEvent=NULL;
      packetManager=NULL;
    };
  virtual ~PacketBuilder(){};
  virtual bool Send(FEPCBlock * event);
  virtual void SetPacketManager(DCNetPacketManager* _packetManager){packetManager=_packetManager;}

 protected:
  int packetTypeFull;
  int packetTypePartial;
  EBPacket::Event * ebEvent;
  EBPacket::ChannelData * ebChannel;
  std::vector<EBPacket::WFDHeader> header;

  DCNetPacketManager * packetManager;
  
 
  //====================================
  //packet interface 
  //====================================
  DCNetPacket *packet;            //Pointer to packet object
  uint8_t     *dataPtr;            //Ptr to the begining of the packet data
  uint8_t     *currentPtr;         //Ptr to current position in the packet
  uint16_t     dataSizeLeft;       //remaining packet data size
  uint8_t      packetNumber;       //Packet number for this EV
  int32_t      iDCPacket;          //Index to packet in MM
  //Resets all packet members except the packetNumber 
  //(used if breaking up packets)
  void ResetPacket();
  //Clears all packet meembers
  //(used when you are done sending packets)
  void ClearPacket();
  //====================================
  
  virtual void     SendPacket(int packetType);
  virtual int      CheckPacket(uint32_t size,FEPCBlock * event);
  virtual int      AddEventHeader(FEPCBlock * event);
  virtual int      AddChannelHeader(RAT::DS::PMT * pmt);
  virtual int      CheckSizeLeftPMT(RAT::DS::Raw * raw,FEPCBlock * event);
  virtual int      CheckSizeLeftWFD(FEPCBlock * event);
  virtual uint32_t CalculatePMTSize(RAT::DS::Raw * raw) ;
  virtual int      AddData(RAT::DS::Raw * raw);
};

#endif
