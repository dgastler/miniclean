#include <DCLauncher.h>

//Memory managers
#include <MemoryBlockManager.h>
#include <BasicRATDSManager.h>
#include <FEPCRatManager.h>
#include <DRPCRatManager.h>
#include <EBPCRatManager.h>

uint32_t DCLauncher::SetupMemoryManagers(xmlNode * MMNode,
				      std::map<std::string,MemoryManager*> & tempMM)
{
  //Check how many managers we are suppose to set up.
  uint32_t MMCount = NumberOfNamedSubNodes(MMNode,"MANAGER");  

  //Fail if it is over 16.
  if(MMCount > 16)
    return(MM_TOO_MANY);


  uint32_t ret  = 0;
  std::string errName("SetupMemoryManagers");
  //Loop over all MANAGER nodes in the XML stream 
  for(uint32_t node = 0; node < MMCount;node++)
    {
      //Load the node th element
      xmlNode* memoryManagerNode = FindIthSubNode(MMNode,"MANAGER",node);
      if(memoryManagerNode != NULL) // If this pointer isn't NULL
	{	  
	  //Get type of this manager
	  std::string memoryManagerType("");
	  if(FindSubNode(memoryManagerNode,"TYPE") == NULL)
	    {
	      ret = MM_XML_TYPE;
	      ret |= 0x1<<node;
	      break;
	    }
	  else
	    GetXMLValue(memoryManagerNode,"TYPE",errName.c_str(),memoryManagerType);	  
	  
	  //Get name of this manager
	  std::string memoryManagerName("");
	  if(FindSubNode(memoryManagerNode,"NAME") == NULL)
	    {
	      ret = MM_XML_NAME;
	      ret |= 0x1<<node;
	      break;
	    }
	  else
	    GetXMLValue(memoryManagerNode,"NAME",errName.c_str(),memoryManagerName);


	  //Allocate the correct memorymanager
	  bool KnownThread = true;	  
	  if(memoryManagerType == "MemoryBlockManager")
	    {
	      tempMM[memoryManagerName]= new MemoryBlockManager(memoryManagerName);
	    }
	  else if(memoryManagerType == "BasicRATDSManager")
	    {
	      tempMM[memoryManagerName]= new BasicRATDSManager(memoryManagerName);
	    }
	  else if(memoryManagerType == "FEPCRatManager")
	    {
	      tempMM[memoryManagerName]= new FEPCRatManager(memoryManagerName);
	    }
	  else if(memoryManagerType == "DRPCRatManager")
	    {
	      tempMM[memoryManagerName]= new DRPCRatManager(memoryManagerName);
	    }
	  else if(memoryManagerType == "EBPCRatManager")
	    {
	      tempMM[memoryManagerName]= new EBPCRatManager(memoryManagerName);
	    }
	  else
	    {
	      KnownThread = false;
	    }

	  //Set up the thread.
	  if(KnownThread)
	    {
	      //Check that the allocation went well
	      if(tempMM[memoryManagerName] == NULL)
		{
		  ret =  MM_NEW_FAIL;
		  ret |= (0x1<<node);
		  break;
		}
		
	      if(!tempMM[memoryManagerName]->AllocateMemory(memoryManagerNode))
		{
		  ret = MM_ALLOC_FAIL;
		  ret = ret|(0x1 << node); //Set the nodeth bit of ret to 1
		  break;
		}
	      else
		{
		  printf("Setup memory manager %s(%s)\n",
			 memoryManagerName.c_str(),
			 memoryManagerType.c_str());
		  
		}
	    }
	  else
	    {	      
	      printf("Memory manager %d unknown name/type: %s/%s\n",
		     node,
		     memoryManagerName.c_str(),
		     memoryManagerType.c_str());
	      ret = MM_UNKNOWN;
	      ret |= (0x1 << node); //Set the nodeth bit of ret to 1
	      break;
	    }
	}
      else
	{
	  fprintf(stderr,"Bad memory manager node # %d\n",node);	 
	  ret = MM_NOT_FOUND;
	  ret |= (0x1<<node);
	  break;
	}
    }
  return(ret);
}
