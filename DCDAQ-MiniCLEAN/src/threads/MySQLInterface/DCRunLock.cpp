#include <DCRunLock.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[DCRunLock::COUNT] = 
  {
    "RUNNING",
    "FREE",
    "ERROR",
    "NEW_RUN",
    "STOP"
  };  

const char * DCRunLock::GetTypeName(DCRunLock::Type type)
{
  if(type < DCRunLock::COUNT)
    return TypeName[type];
  return NULL;
} 

DCRunLock::Type DCRunLock::GetType(const char * name)
{
  std::string searchString(name);  
  DCRunLock::Type ret = DCRunLock::Type(0);
  for(DCRunLock::Type type = DCRunLock::Type(0);
      type < DCRunLock::COUNT;
      type = DCRunLock::Type(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
