PCBNEW-LibModule-V1  Wed 06 Feb 2013 05:42:54 PM EST
# encoding utf-8
$INDEX
SHV_Cables_Mount
SHV_Strain_Relief
$EndINDEX
$MODULE SHV_Strain_Relief
Po 0 0 0 15 50EDC91F 00000000 ~~
Li SHV_Strain_Relief
Sc 00000000
AR SHV_Strain_Relief
Op 0 0 0
T0 0 -1650 600 600 0 120 N V 21 N "SHV Strain Relief"
T1 0 1800 600 600 0 120 N V 21 N ""
$PAD
Sh "" C 1281 1281 0 0 0
Dr 1281 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2700 0
.LocalClearance 500
$EndPAD
$PAD
Sh "" C 1281 1281 0 0 0
Dr 1281 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2700 0
.LocalClearance 500
$EndPAD
$PAD
Sh "" C 1281 1281 0 0 0
Dr 1281 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
.LocalClearance 500
$EndPAD
$EndMODULE  SHV_Strain_Relief
$MODULE SHV_Cables_Mount
Po 0 0 0 15 5112DC69 00000000 ~~
Li SHV_Cables_Mount
Sc 00000000
AR SHV_Cables_Mount
Op 0 0 0
T0 0 0 600 600 0 120 N V 21 N ""
T1 0 0 600 600 0 120 N V 21 N ""
$PAD
Sh "" C 1281 1281 0 0 0
Dr 1281 0 0
At STD N 00E00001
Ne 0 ""
Po -5200 0
.LocalClearance 1500
$EndPAD
$PAD
Sh "" C 1281 1281 0 0 0
Dr 1281 0 0
At STD N 00E00001
Ne 0 ""
Po 5200 0
.LocalClearance 1500
$EndPAD
$EndMODULE  SHV_Cables_Mount
$EndLIBRARY
