#ifndef __THREAD_TYPE__
#define __THREAD_TYPE__
/*
  Thread description
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>


class ThreadType : public DCThread 
{
 public:
  ThreadType(std::string Name);
  ~ThreadType(){};

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  
  virtual void MainLoop();
 private:

  //==============================================================
  //Basic DCThread things to implement
  //==============================================================
  //This is where you process messages from DCDAQ.
  //if the message was processed return true
  //if you don't repond to a message return false.
  //Remember to "shutdown" memory managers if you get a stop.
  virtual bool ProcessMessage(DCMessage::Message &message);
  
  virtual void ProcessTimeout();

  virtual void ThreadCleanUp(); //Probably don't need this.

  //==============================================================
  //Memory manager interface
  //==============================================================
  //Put all your memorymanager pointers here

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  //I usually use these for sending updates, but you don't have to. 

};
#endif
