#include <Control.h>
#include <string>
#include <cl_helper.h>

bool Control::Setup(xmlNode * /*SetupNode*/,
		    std::vector<MemoryManager*> &/*MemManager*/)
{
  Ready = true;

  //Add Commands.
  //==========================================
  //List all commands
  //==========================================
  //ECHO
  Commands.push_back(std::string("echo"));
  CommandFunctions.push_back(&Control::Echo);

  //SENDOUT
  Commands.push_back(std::string("sendout"));
  CommandFunctions.push_back(&Control::SendOut);

  //SEND
  Commands.push_back(std::string("send"));
  CommandFunctions.push_back(&Control::Send);
		     
  //HELP
  Commands.push_back(std::string("help"));
  CommandFunctions.push_back(&Control::Help);
  //==========================================
  //Tell DCDAQ to shut down all threads and clean up
  //==========================================
  //QUIT
  Commands.push_back(std::string("quit"));
  CommandFunctions.push_back(&Control::Quit);
  //STOP
  Commands.push_back(std::string("stop"));
  CommandFunctions.push_back(&Control::Quit);
  //EXIT
  Commands.push_back(std::string("exit"));
  CommandFunctions.push_back(&Control::Quit);

  //==========================================
  //Tell DCDAQ to startup all threads
  //==========================================
  //START
  Commands.push_back(std::string("start"));
  CommandFunctions.push_back(&Control::Start);
  //GO
  Commands.push_back(std::string("go"));
  CommandFunctions.push_back(&Control::Start);
  //BEGIN
  Commands.push_back(std::string("begin"));
  CommandFunctions.push_back(&Control::Start);

  //==========================================
  //Tell DCDAQ to pause all threads
  //==========================================
  //PAUSE
  Commands.push_back(std::string("pause"));
  CommandFunctions.push_back(&Control::Pause);
  //HOLD
  Commands.push_back(std::string("hold"));
  CommandFunctions.push_back(&Control::Pause);

  //==========================================
  //Set Run number
  //==========================================
  //SetRun
  Commands.push_back(std::string("setrun"));
  CommandFunctions.push_back(&Control::RunNumber);
  //newrun
  Commands.push_back(std::string("newrun"));
  CommandFunctions.push_back(&Control::RunNumber);

  //==========================================
  //Print DCDAQ status
  //==========================================
  //STATUS
  Commands.push_back(std::string("status"));
  CommandFunctions.push_back(&Control::Status);

  //==========================================
  //Print DCDAQ routing tables
  //==========================================
  //ROUTE
  Commands.push_back(std::string("route"));
  CommandFunctions.push_back(&Control::Route);

  //==========================================
  //Load a to be launched XML file
  //==========================================
  //LOAD
  Commands.push_back(std::string("load"));
  CommandFunctions.push_back(&Control::Load);

  //==========================================
  //Send a message to a thread
  //==========================================
  //sendmsg
  Commands.push_back(std::string("sendmesg"));
  CommandFunctions.push_back(&Control::SendMesg);
  
  //==========================================
  //Setup a loop
  //==========================================
  Commands.push_back(std::string("loop"));
  CommandFunctions.push_back(&Control::CmdLoop);

  //==========================================
  //Send a command
  //==========================================
  //command
  Commands.push_back(std::string("command"));
  CommandFunctions.push_back(&Control::Command);

  //==========================================
  //Send a command to a thread
  //==========================================
  //command
  Commands.push_back(std::string("sendcommand"));
  CommandFunctions.push_back(&Control::SendCommand);

  //make sure all commands are lower case
  for(std::vector<std::string>::iterator it = Commands.begin();
      it != Commands.end();++it)
    {
      LowerCase(*it);
    }

  Loop= true;
  //Register the STDIN_FILENO fd with DCThread
  AddReadFD(STDIN_FILENO);
  SetupSelect();
  //Define Readline prompt and install callback handler
  const char * prompt="DCDAQ>";
  rl_callback_handler_install(prompt, (rl_vcpfunc_t*) &Callback);
  return(true);
}

void Control::Start(std::string arg, 
		    DCMessage::DCAddress destAddress)
{  
  DCMessage::Message message;        
  if(!arg.empty() && (destAddress.IsBlank()))
    destAddress.Set(arg);
  message.SetDestination(destAddress);
  message.SetType(DCMessage::GO);
  SendMessageOut(message);
}
void Control::Pause(std::string arg, 
		    DCMessage::DCAddress destAddress)
{
  DCMessage::Message message;      
  if(!arg.empty() && (destAddress.IsBlank()))
    destAddress.Set(arg);
  message.SetDestination(destAddress);
  message.SetType(DCMessage::PAUSE);
  SendMessageOut(message);
}
void Control::Quit(std::string arg, 
		   DCMessage::DCAddress destAddress)
{
  DCMessage::Message message;      
  if(!arg.empty() && (destAddress.IsBlank()))
    destAddress.Set(arg);
  message.SetDestination(destAddress);
  message.SetType(DCMessage::STOP);
  SendMessageOut(message);
}
void Control::Help(std::string /*arg*/, 
		   DCMessage::DCAddress /*destAddress*/)
{
  std::vector<std::string>::iterator it = Commands.begin();
  printf("\n Commands:\n");
  for(;it != Commands.end();it++)
    {
      printf("\t%s\n",it->c_str());
    }
}
void Control::Echo(std::string arg, 
		   DCMessage::DCAddress destAddress)
{
  DCMessage::Message message;
  message.SetType(DCMessage::TEXT);
  message.SetDestination(destAddress);
  DCMessage::DCString string;
  std::vector<uint8_t> data = string.SetData(arg);
  message.SetData(&(data[0]),data.size());
  SendMessageOut(message);
}

void Control::SendOut(std::string arg, 
		      DCMessage::DCAddress destAddress)
{  
  destAddress.SetBroadcast();
  ProcessCommand(arg,destAddress);  
}

void Control::Send(std::string arg, 
		      DCMessage::DCAddress destAddress)
{  
  if(!arg.empty())
    {
      //Find the destination of the message
      size_t destEnd = arg.find(' ');
      std::string dest = arg.substr(0,destEnd);
      destAddress.Set(dest,"BROADCAST");
      //Find the arguments if any.
      std::string mesg;
      if((destEnd == arg.size()) ||
	 (destEnd == std::string::npos))
	{
	  mesg = "";
	}
      else
	{
	  mesg = arg.substr(destEnd+1,
			    arg.size() - destEnd);
	}
      ProcessCommand(mesg,destAddress);  
    }
}

void Control::Status(std::string arg, 
		     DCMessage::DCAddress destAddress)
{
  DCMessage::Message message;
  message.SetDestination(destAddress);
  message.SetType(DCMessage::GET_STATS);
  message.SetData(arg.c_str(),arg.size());
  SendMessageOut(message);    
}

void Control::Route(std::string arg, 
		     DCMessage::DCAddress destAddress)
{
  DCMessage::Message message;
  message.SetDestination(destAddress);
  message.SetType(DCMessage::GET_ROUTE);
  message.SetData(arg.c_str(),arg.size());
  SendMessageOut(message);    
}

void Control::Load(std::string arg, 
		   DCMessage::DCAddress destAddress)
{
  std::string scriptString;
  //Open   
  if((arg.size() > 0) && LoadFile(arg,scriptString))
    {
      DCMessage::Message message;
      message.SetDestination(destAddress);
      message.SetData(scriptString.c_str(),scriptString.size());
      message.SetType(DCMessage::LAUNCH);
      SendMessageOut(message);
      return;
    }
  else
    {
      std::string errorText("Bad XML file: ");
      char * buffer = new char[errorText.size() + arg.size()+1];
      if(buffer != NULL)
	{
	  if(arg.size() > 0)	    
	    sprintf(buffer,"%s%s",errorText.c_str(),arg.c_str());      
	  else
	    sprintf(buffer,"%s blank",errorText.c_str());      
	  PrintWarning(buffer);
	  delete [] buffer;
	  return;
	}
      return;
    }  
}

void Control::RunNumber(std::string arg, 
			DCMessage::DCAddress destAddress)
{
  DCMessage::Message message;    
  message.SetDestination(destAddress);
  int32_t data;
  if(arg.empty())
    {
      data = -1;
    }
  else
    {
      data = strtol(arg.c_str(),NULL,0);
    }
  message.SetData(&data,sizeof(data));
  message.SetType(DCMessage::RUN_NUMBER);
  SendMessageOut(message);  
}

void Control::CmdLoop(std::string arg, 
		      DCMessage::DCAddress destAddress)
{
  //Use a string streamer to pull out the first argument if it exists
  std::istringstream iss(arg);
  std::string firstArgument("");
  iss >> firstArgument;
  
  //if the first argument is break, stop any existing loop
  if(boost::iequals(firstArgument,"break"))
    {
      loopTime = 0;
    }
  //Parse second argument as time (should return 0 if not a number)
  //only setup command loop if there is a positive time
  else if(atoi(firstArgument.c_str()) > 0)
    {
      //Hold the loop time/command until we see a correct command
      int loopTimeTemp = atoi(firstArgument.c_str());
      std::string loopCommandTemp("");
      
      //Remote the first argument (timeout) and look for 
      size_t firstArgumentPosition = firstArgument.size();      
      loopCommandTemp = arg.substr(firstArgumentPosition,
				   arg.size()-firstArgumentPosition);      
      //Remove leading spaces
      while(loopCommandTemp.size() &&
	    loopCommandTemp[0] == ' ')
	{
	  loopCommandTemp.erase(0,1);      
	}
      //remove '\n's
      while(loopCommandTemp.size() && 
	    loopCommandTemp[loopCommandTemp.size()-1] == '\n')
	{
	  loopCommandTemp.erase(loopCommandTemp.size()-1,1);
	}
      
      //If the loop command doesn't exist, stop the loop timer
      if(loopCommandTemp.empty())
	{
	  fprintf(stderr,"Empty loop!\n");
	}
      else
	{
	  //Everythign seems ok, set the loop in motion
	  loopTime = loopTimeTemp;
	  loopCommand = loopCommandTemp;
	  loopAddress = destAddress;
	  nextLoopTime = time(NULL) + loopTime;
	  ProcessTimeout();
	}
    }
  else
    {
      Print("Bad loop command!\n");
    }
}

void Control::SendMesg(std::string arg, 
		       DCMessage::DCAddress destAddress)
{
  if(!arg.empty())
    {
      //Find the destination of the message
      size_t destEnd = arg.find(' ');
      std::string dest = arg.substr(0,destEnd);
      destAddress.Set(dest,"BROADCAST");
      //Find the arguments if any.
      std::string mesg;
      if((destEnd == arg.size()) ||
	 (destEnd == std::string::npos))
	{
	  mesg = "";
	}
      else
	{
	  mesg = arg.substr(destEnd+1,
			    arg.size() - destEnd);
	}
      DCMessage::Message message;
      message.SetType(DCMessage::TEXT);
      message.SetDestination(destAddress);
      DCMessage::DCString string;
      std::vector<uint8_t> data = string.SetData(mesg);
      message.SetData(&(data[0]),data.size());
      SendMessageOut(message);
    }
  else
    {
      Print("no destination!\n");
    }
}

void Control::SendCommand(std::string arg, 
			  DCMessage::DCAddress destAddress)
{
  if(!arg.empty())
    {
      //Find the destination of the message
      size_t destEnd = arg.find(' ');
      std::string dest = arg.substr(0,destEnd);
      destAddress.Set(dest,"BROADCAST");
      //Find the arguments if any.
      std::string mesg;
      if((destEnd == arg.size()) ||
	 (destEnd == std::string::npos))
	{
	  mesg = "";
	}
      else
	{
	  mesg = arg.substr(destEnd+1,
			    arg.size() - destEnd);
	}
      Command(mesg,destAddress);
    }
  else
    {
      Print("no destination!\n");
    }
}

void Control::Command(std::string arg, 
		       DCMessage::DCAddress destAddress)
{
  if(!arg.empty())
    {
      DCMessage::Message message;
      message.SetType(DCMessage::COMMAND);
      message.SetDestination(destAddress);
      DCMessage::DCString string;
      std::vector<uint8_t> data = string.SetData(arg);
      message.SetData(&(data[0]),data.size());
      SendMessageOut(message);
    }
  else
    {
      Print("Missing command!\n");
    }
}


