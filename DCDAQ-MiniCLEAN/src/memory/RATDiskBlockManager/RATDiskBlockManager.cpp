#ifdef __RATDS__

#include <RATDiskBlockManager.h>

RATDiskBlockManager::RATDiskBlockManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATDiskBlockManager"));
  //Make sure our memory vector is empty
  Memory.clear();
  ShutdownStatus = false;
}

RATDiskBlockManager::~RATDiskBlockManager()
{
  Deallocate();
}

void RATDiskBlockManager::Deallocate()
{
  if(!Lock.LockObject())
    {
      fprintf(stderr,"Lock error on deallocation\n");
      abort();
    }
  //Clear our containers
  Free.Clear(true);
  EventStore.Clear(true);
  ToDisk.Clear(true);
  BadEvent.Clear(true); 
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //      delete EBPC event (check if it's not NULL)
      if(Memory.back().ds)
	{
	  delete Memory.back().ds;
	  Memory.back().ds = NULL;
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
  Lock.UnlockObject();
}


int64_t RATDiskBlockManager::GetEvent(int64_t Event,
				 int32_t &index,
				 uint64_t &password)
{ 
  int64_t ret = returnDCVector::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      //The object is now mutex locked

      //Get the index at Event
      ret = EventStore.GetEntry(Event,index,password);
      if(ret == returnDCVector::EMPTY_KEY)
	//The index was empty
	{      
	  //Get a new index from the free queue
	  int32_t freeIndex;
	  bool gotFree = false;
	  while(!(gotFree = Free.Get(freeIndex)) && 
		!Lock.GetShutdown())
	    {
	    }
	  if(gotFree)
	    {
	      index = freeIndex;
	      ret = EventStore.UpdateEntry(Event,freeIndex,password); 	  
	    }
	  else
	    {
	      index = EventStore.GetBadValue();
	      //Must block to make sure everything is consistent
	      ret = EventStore.ClearEntry(Event,password);
	      if(ret == returnDCVector::OK)
		{
		  ret = returnDCVector::NO_FREE;
		}
	    }	
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

int64_t RATDiskBlockManager::SetEvent(int64_t Event,
				 int32_t &index,
				 uint64_t password)
{
  int64_t ret = returnDCVector::OK;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = EventStore.UpdateEntry(Event,index,password);
      if(ret == returnDCVector::OK)
	{
	  ret = EventStore.ClearPassword(Event,password);
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);  
}

int64_t RATDiskBlockManager::ClearEvent(int64_t key, uint64_t password)
{
  int64_t ret = returnDCVector::OK;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = EventStore.ClearEntry(key,password);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);  
}

bool RATDiskBlockManager::AllocateMemory(xmlNode * setupNode)
{
  if(!Lock.LockObject())
    {
      fprintf(stderr,"Lock error on allocation\n");
      abort();
    }
  bool ret = true;
  
  int NumBlocksERR = GetXMLValue(setupNode,"NUMBLOCKS","MANAGER",NumBlocks);
  if(NumBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMBLOCKS in %s.\n",GetType().c_str());
      ret = false;
    }
  int VectorHashSizeERR = GetXMLValue(setupNode,"VECTORHASHSIZE","MANAGER",VectorHashSize);
  if(VectorHashSizeERR != 0)
    {
      fprintf(stderr,"Error reading VECTORHASHSIZE in %s.\n",GetType().c_str());
      ret = false;
    }

  //Allocate the needed RAT RATDS blocks
  if(ret)
    {
      for(uint32_t iBlock = 0; iBlock < NumBlocks;iBlock++)
	{
	  RATDSBlock evBlock;	  
	  Memory.push_back(evBlock);
	  Memory.back().ds = new RAT::DS::Root;	  
	  if(Memory.back().ds == NULL)
	    {
	      ret = false;
	      break;
	    }
	  Memory.back().ds->AddNewEV();
	  if(Memory.back().ds->GetEVCount() != 1)
	    {
	      ret = false;
	      break;
	    }
	  int32_t index = int32_t(Memory.size())-1;
	  Free.Add(index);

	}     
    }
  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash (-1 emtpy value, -2 bad value)
      EventStore.Setup(VectorHashSize,-1,-2);
    }
  Lock.UnlockObject();
  return(ret);
}

bool RATDiskBlockManager::AddBadEventNOFD(int64_t eventKey, int32_t &index, Level::Type errorCode)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      //Read off one from the FD to not cause select to fire on this event.
      BadEvent::sBadEvent badEvent;
      badEvent.eventKey = eventKey;
      badEvent.index = index;
      badEvent.errorCode = errorCode;
      ret = BadEvent.Add(badEvent);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);  
}

int64_t RATDiskBlockManager::MoveEventToBadQueue(int64_t eventKey,
						 int32_t &index,
						 uint64_t password,
						 Level::Type errorCode)
{  
  int64_t ret = returnDCVector::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      BadEvent::sBadEvent badEvent;
      badEvent.errorCode = errorCode;
      badEvent.index = index;
      badEvent.eventKey = eventKey;
      bool moveWorked = BadEvent.Add(badEvent);
      if(moveWorked)
	//We moved this event, so now we need to clear it. 
	{
	  //We will always wait on this!
	  ret = EventStore.ClearEntry(eventKey,
				      password,
				      true);        
	  if(ret == returnDCVector::OK)
	    //The caller no longer should know this
	    {
	      index = EventStore.GetBadValue();
	    }
	}
      else
	//The bad queue was locked and we aren't waiting
	{
	  ret = returnDCVector::EXTERNAL;
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

int64_t RATDiskBlockManager::MoveEventToDiskQueue(int64_t Event,
					     int32_t &index,
					     uint64_t password)
{  
  int64_t ret = returnDCVector::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      int32_t indexToSend = index;
      bool moveWorked = ToDisk.Add(indexToSend);      
      if(moveWorked)
	//We moved this event, so now we need to clear it. 
	{
	  //We will always wait on this!
	  ret = EventStore.ClearEntry(Event,
				      password,
				      true);        
	  if(ret == returnDCVector::OK)
	    //The caller no longer should know this
	    {
	      index = EventStore.GetBadValue();
	    }
	}
      else
	//The Send queue was locked and we aren't waiting
	{
	  ret = returnDCVector::EXTERNAL;
	}
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

void RATDiskBlockManager::PrintStatus(int indent, int /*verbosity*/)
{
  StatMessage::StatRATDiskMM sStat;
  sStat.sBase.type = StatMessage::RAT_DISK;
  sStat.sInfo.time = time(NULL);
  sStat.sInfo.subID = SubID::BLANK;
  sStat.sInfo.level = Level::BLANK;
  sStat.allocatedSize = Memory.size();
  sStat.eventStore.size   = EventStore.GetSize();
  sStat.eventStore.active = EventStore.GetActiveEntryCount();
  sStat.free.size     = Free.GetSize();
  sStat.free.addCount = Free.GetAddCount();
  sStat.toDisk.size     = ToDisk.GetSize();
  sStat.toDisk.addCount = ToDisk.GetAddCount();
  sStat.badEvent.size     = BadEvent.GetSize();
  sStat.badEvent.addCount = BadEvent.GetAddCount();

  DCMessage::Message message;
  message.SetType(DCMessage::STATISTIC);
  message.SetData(sStat);
  SendMessageOut(message);

#ifdef VERBOSE_MEMORY
  std::stringstream ss;
  ss << GetType().c_str() << std::endl;
  if(indent > 0){for(int i = 0; i < indent;i++){ss << " ";}}
  ss << "Size: " <<  sStat.allocatedSize << std::endl;
  if(indent > 0){for(int i = 0; i < indent;i++){ss << " ";}}
  ss << "Free: " <<  sStat.free.size   
     << "     (" << sStat.free.addCount << ")" << std::endl;
  if(indent > 0){for(int i = 0; i < indent;i++){ss << " ";}}
  ss << "Active Stored: " <<  sStat.eventStore.active   
     << "     (" << sStat.eventStore.size << ")" << std::endl;
  if(indent > 0){for(int i = 0; i < indent;i++){ss << " ";}}
  ss << "To Disk: " <<  sStat.toDisk.size   
     << "     (" << sStat.toDisk.addCount << ")" << std::endl;
  if(indent > 0){for(int i = 0; i < indent;i++){ss << " ";}}
  ss << "BadEvent: " <<  sStat.badEvent.size   
     << "     (" << sStat.badEvent.addCount << ")" << std::endl;
  Print(ss.str());
#endif
}

bool RATDiskBlockManager::AddFree(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      //Clear the event
      RATDSBlock * ptr = GetRATDiskBlock(index);
      if(ptr)
	ptr->Clear();

      ret = Free.Add(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}
bool RATDiskBlockManager::GetFree(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = Free.Get(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

bool RATDiskBlockManager::GetToDisk(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = ToDisk.Get(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}
bool RATDiskBlockManager::AddToDisk(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = ToDisk.Add(index);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

bool RATDiskBlockManager::GetBadEvent(BadEvent::sBadEvent &badEvent)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = BadEvent.Get(badEvent);
#ifdef LOCK_EVERYTHING
      //Unlock the mutex for this object
      Lock.UnlockObject();
    }
#endif
  return(ret);
}

#endif
