#include <DCNetPacket.h>

void DCNetPacket::PrintHeader(FILE * fd)
{  
  fprintf(fd,"\n");
  //          123456789ab123456789ab123456789ab123456789ab123456789ab123456789ab
  fprintf(fd,"Ctrl-Wrd   Type       size[0]    size[1]    PacketID\n");
  for(int i = 0; ((i < DCNETPACKET_HEADER_SIZE)&&(i < MaxPacketSize));i++)
    {
      fprintf(fd,"0x%02x       ",PacketData[i]);
    }
  fprintf(fd,"\n");
  //Flush data
  fflush(fd);
}

int DCNetPacket::SetData(uint8_t * data,uint32_t size,uint8_t packetNumber)
{    
  int ret = 0;
  *PacketNumber = packetNumber;
  if(size > MaxDataSize) 
    {
      ret = -1;
    }
  else if(!Data)
    {
      ret = -2;
    }
  else
    {
      //Copy data into packet
      memcpy(Data,data,size);
//      for(uint32_t i = 0; i < size;i++)
//	{
//	  Data[i] = data[i];
//	}
      DataSize = size;
      //Set the header word
      *HeaderWord = DCNETPACKET_HEADER_WORD;
      //Determine the packet size and set it in the header.
      uint16_t packetSize = DataSize + DCNETPACKET_HEADER_SIZE;
      PacketSize[0] = uint8_t (packetSize >> 8);  //Get upper 8 bits.
      PacketSize[1] = uint8_t (0x00FF & packetSize); //Get lower 8 bits
    }
  return(ret);
}

int DCNetPacket::Deallocate()
{
  DataSize = 0;
  MaxDataSize = 0;
  PacketSize = 0;    
  //Clear convenience pointers
  HeaderWord = NULL;
  PacketSize = NULL;
  Type = NULL;
  PacketNumber = NULL;
  Data = NULL;

  if(PacketData)
    {
      delete [] PacketData;
    }
  PacketData = NULL;
  return(true);
}

int DCNetPacket::Allocate(uint32_t packetDataSize)
{
  int ret = 0;  
  Deallocate();
  DataSize = 0;
  MaxDataSize = packetDataSize;
  MaxPacketSize = MaxDataSize + DCNETPACKET_HEADER_SIZE;
  //Allocate data
  PacketData = new uint8_t[MaxPacketSize];
  if(!PacketData)
    {
      
      ret = -1;
    }
  else
    {
      //Zero header data.
      for(int i =0; i < DCNETPACKET_HEADER_SIZE;i++)
	{
	  PacketData[i] = 0;
	}
      //Set internal pointers.
      HeaderWord = PacketData + DCNETPACKET_HEADER_WORD_POS;
      Type = PacketData + DCNETPACKET_HEADER_TYPE_POS;
      *Type = BAD_PACKET;
      PacketSize = PacketData + DCNETPACKET_HEADER_SIZE_POS1;
      PacketNumber = PacketData + DCNETPACKET_HEADER_PACKETNUMBER_POS;
      *PacketNumber = 0;
      Data = PacketData + DCNETPACKET_HEADER_SIZE;
    }
  return(ret);
}


//int DCNetPacket::SendPacket(int socketFD,
//			    time_t seconds = 0,
//			    suseconds_t useconds = SELECT_TIMEOUT)
int DCNetPacket::SendPacket(int socketFD,time_t seconds,suseconds_t useconds)
{
  //Setup timeout structure
  struct timeval timeout;
  timeout.tv_sec = seconds;
  timeout.tv_usec = useconds;

  int ret = 0;
  if(!Data)
    {
      ret = BAD_ALLOCATION;
    }
  else if(*Type == BAD_PACKET)
    {
      ret = BAD_PACKET_TYPE;
    }
  else //Everything seems ok, so send the packet.  
    {   
      uint16_t packetSize = DataSize + DCNETPACKET_HEADER_SIZE;   
      int32_t writeReturn = writeN(socketFD,PacketData,packetSize,timeout);
      if(writeReturn == 0)
	{
	  ret = 0;
	}
      else if(writeReturn == -1)
	{
	  ret = -1; 
	}
      else if(writeReturn == -2)
	{
	  ret = -2;
	}
      else
	{
	  ret = writeReturn;
	  *Type = BAD_PACKET;
	  DataSize = 0;
	}
    }
  return (ret);
}

int DCNetPacket::ListenForPacket(int socketFD,time_t seconds,suseconds_t useconds)
{
  //Setup timeout structure
  struct timeval timeout;
  timeout.tv_sec = seconds;
  timeout.tv_usec = useconds;

  int ret = 0;
  if(!Data)
    {
      ret = BAD_ALLOCATION;
    }
  else
    {            
      //=======================================
      //Read the header.
      //=======================================
      uint16_t packetSize = DCNETPACKET_HEADER_SIZE;
      int32_t readReturn = readN(socketFD,PacketData,packetSize,timeout);
      //=======================================
      //Valid header size.
      //=======================================
      if(readReturn == DCNETPACKET_HEADER_SIZE)
	//Read valid message header
	{
	  //=======================================
	  //check if the packet has the correct header.
	  //=======================================
	  if(*HeaderWord == DCNETPACKET_HEADER_WORD)
	    {	      
	      //Determine data size
	      //	      uint16_t size = ((0xFF00&( uint16_t(PacketSize[0]) << 8)) + PacketSize[1]) - DCNETPACKET_HEADER_SIZE;
	      uint16_t size = ((0xFF00&( uint16_t(PacketSize[0]) << 8)) | (0xFF&(PacketSize[1]))) - DCNETPACKET_HEADER_SIZE;
	      if(size > MaxDataSize)
		{
		  fprintf(stderr,"Error ListenForPacket:   Packet too large!\n");
		  *Type = BAD_PACKET;
		  ret = -1;
		}
	      else
		{
		  uint8_t * ptr = Data;
		  int16_t sizeLeft = size;
		  uint16_t readSize = 0;
		  while(readSize  < sizeLeft)
		    {
		      readReturn = readN(socketFD,ptr,sizeLeft,timeout);
		      sizeLeft-=readReturn;
		      ptr+=readReturn;
		      readSize+=readReturn;
		    }
		  //=======================================
		  //Check data size versus read size.
		  //=======================================
		  if(readSize)
		    readReturn = readSize;
		  if(((uint16_t) readReturn) == size)
		    {
		      ret =  readReturn + DCNETPACKET_HEADER_SIZE;
		      DataSize = readReturn;
		    }
		  else
		    {
		      *Type = BAD_PACKET;
		      fprintf(stderr,"ListenForPacket: Bad data read size. %d != %d\n",size, readReturn);
		      ret = -1;
		    }		  
		}
	    }//Header word check
	  else
	    {
	      fprintf(stderr,"ListenForPacket: Bad header word\n");
	      fprintf(stderr,"%02X",*HeaderWord);
	      fprintf(stderr,"%02X",*Type);
	      fprintf(stderr,"%02X",PacketSize[0]);
	      fprintf(stderr,"%02X",PacketSize[1]);
	      fprintf(stderr,"%02X\n",*PacketNumber);
	      //bad header
	      *Type = BAD_PACKET;
	      ret = -1;
	    }	  
	}
      //=======================================
      //Time out
      //=======================================
      else if(readReturn == 0)
	{
	  *Type = BAD_PACKET;
	  ret = 0;
	}
      //=======================================
      //Other errors.
      //=======================================
      else
	{
	  fprintf(stderr,"ERROR ListenForPacket: Packet header read too small!\n");
	  *Type = BAD_PACKET;
	  ret = readReturn;
	}
    }
  return(ret);
}
