#include "xmlHelper.h"

char * GetNodeText(xmlNode * baseNode)
{
  xmlNode * curNode = NULL;
  char * xmlText = NULL;
  for(curNode = baseNode->children;curNode;curNode = curNode->next)
    {
      //If this node is a text node and it contains a valid string
      //and that string isn't just an EOL. 
      if((curNode->type == XML_TEXT_NODE) &&
	 (curNode->content != NULL) &&
	 (curNode->content[0] != '\n'))
	//      if(curNode->type == XML_TEXT_NODE)
	{
	  xmlText = (char*) curNode->content;
	  break;
	}
    }
  return(xmlText);
}

xmlNode * FindIthSubNode(xmlNode * baseNode, const char * Name,int i)
{
  //Takes in an xmlNode and a Node name (char array) and searches
  //for a sub-node with that node name.
  //If found returns a pointer to that node.
  //else returns NULL;
  int foundNodeNumber = 0;
  xmlNode * curNode = NULL;
  xmlNode * retNode = NULL;
  //Loop over all sub-nodes.
  for(curNode = baseNode->children; curNode; curNode = curNode->next)
    {
      //If this is an element node then check if it has the correct name.
      if((curNode->type == XML_ELEMENT_NODE) && (xmlStrcasecmp(curNode->name,(xmlChar *) Name) == 0))
	{
	  //If this is the ith found Node with name Name stop
	  if(foundNodeNumber == i)
	    {
	      //set return node ot current node and break from loop.
	      retNode = curNode;
	      break;
	    }
	  else
	    {
	      //Look for the next Node with name Name
	      foundNodeNumber++;
	    }
	}
    }
  return(retNode);
}
xmlNode * FindSubNode(xmlNode * baseNode, const char * Name)
{
  //Call FindIthSubNode with index 0
  xmlNode * retNode = FindIthSubNode(baseNode,Name,0);

  //if we don't find the node, then we should check if there is a file version.
  //The file version of a node is the node's name with "FILE" added to it.
  //e.g. Name = "PMTLIST"   would then search for a node called "PMTLISTFILE"
  //WARNING!  This probably leaks memory and should be fixed if you are not
  //ok with that.
  if(retNode == NULL)
    {
      printf("Can't find %s: looking for %sFILE\n",Name,Name);
      std::string nameFile = std::string(Name) + std::string("FILE");      
      xmlNode * fileNode = FindIthSubNode(baseNode,nameFile.c_str(),0);
      if(fileNode != NULL)
	{
	  //Get the filename
	  std::string filename;
	  GetXMLValue(baseNode,nameFile.c_str(),Name,filename);
	  //Open the filename and copy it into the std::string data
	  std::string data;
	  if(LoadFile(filename.c_str(),data) == false)
	    //Failed to load filename
	    {
	      printf("\tFound %sFILE tag, but can't open the file %s.\n",Name,filename.c_str());
	      //Do nothing. (we'll be returning NULL anyways)
	    }
	  else
	    //Opened and loaded file into memory
	    {
	      fprintf(stderr,"\tWARNING!!!!!!!  YOU ARE CAUSING A MEMORY LEAK BY USING FindSubNode WITH THE FILE SEARCH OPTION!\n");
	      //Load the file data into a xmlDocument
	      xmlDoc * XMLDoc = xmlReadMemory(data.c_str(),
					      data.size(),
					      NULL,
					      NULL,
					      0);
	      //Get a pointer to the base node of the xml document
	      retNode = xmlDocGetRootElement(XMLDoc);
	      //This  XMLDOC not being delete is going to be the cause
	      //of the warning about memory leaks.   If you want this
	      // to work please turn this into a C++ class that can
	      // clean up the allocated memory with it's deconstructor.
	    }
	}
      else
	{
	  printf("\tCan not find %sFILE\n",Name);
	}
    }
  return(retNode);
  
}

unsigned int       NumberOfSubNodes(xmlNode * baseNode)
{
  unsigned int numberOfSubNodes = 0;
  xmlNode * curNode = NULL;
  //loop over all sub-nodes
  for(curNode = baseNode->children; curNode; curNode = curNode->next)
    {
      //If this is an element sub-node add one to numberOfSubNodes.
      if(curNode->type == XML_ELEMENT_NODE)
	{
	  numberOfSubNodes++;
	}
    }
  return(numberOfSubNodes);
}
unsigned int       NumberOfNamedSubNodes(xmlNode * baseNode,const char * Name)
{
  unsigned int numberOfSubNodes = 0;
  xmlNode * curNode = NULL;
  //loop over all sub-nodes
  for(curNode = baseNode->children; curNode; curNode = curNode->next)
    {
      //If this is an element sub-node add one to numberOfSubNodes.
      if((curNode->type == XML_ELEMENT_NODE) && !xmlStrcasecmp(curNode->name,(xmlChar * )Name))
	{
	  numberOfSubNodes++;
	}
    }
  return(numberOfSubNodes);
}

xmlNode * AddNewSubNode(xmlNode * baseNode,const char * Name)
{
  xmlNode * newNode = NULL;
  newNode = xmlNewChild(baseNode,NULL,(xmlChar*)Name,NULL);
  return(newNode);
}

bool LoadFile(const std::string &filename,std::string &text)
{
  std::ifstream InFile;
  InFile.open(filename.c_str());
  if(InFile.fail())
    {
      return(false);
    }
  unsigned int bufferSize = 1000;
  char * buffer = new char[bufferSize];
  while(!InFile.eof())
    {
      InFile.getline(buffer,bufferSize);
      text.append(buffer);
    }
  delete [] buffer;
  return(true);
}

int GetIthXMLValue(xmlNode * baseNode,int i,  const char * Name,  const char * ErrorBase,uint32_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtoul(GetNodeText(xmlNodePointer),NULL,0);
  return(ret);
}
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,int32_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtol(GetNodeText(xmlNodePointer),NULL,0);
  return(ret);
}
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,int16_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtol(GetNodeText(xmlNodePointer),NULL,0);
  return(ret);
}
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,uint16_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtoul(GetNodeText(xmlNodePointer),NULL,0);
  return(ret);
}
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,int8_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtol(GetNodeText(xmlNodePointer),NULL,0);
  return(ret);
}
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,uint8_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtoul(GetNodeText(xmlNodePointer),NULL,0);
  return(ret);
}





int GetIthXMLValue(xmlNode * baseNode,int i,const char * Name,const char * ErrorBase,std::string &reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg.assign(GetNodeText(xmlNodePointer));
  return(ret);
}

int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,double & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindIthSubNode(baseNode,Name,i);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = atof(GetNodeText(xmlNodePointer));
  return(ret);
}

int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,uint32_t & reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,int32_t & reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,int16_t & reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,uint16_t & reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,int8_t & reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,uint8_t & reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,std::string &reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
int GetXMLValue(xmlNode * baseNode,const char * Name,const char * ErrorBase,double &reg)
{
  return(GetIthXMLValue(baseNode,0,Name,ErrorBase,reg));
}
