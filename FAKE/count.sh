#!/bin/bash
#to use this set the frequency range and time step
#then run:
#count.sh | ./FAKE file1 file2 file3

sleep 10
freq="300"
while [ $freq -lt 4000 ]
do
    echo $freq
    freq=$[$freq+10]
    sleep 10
done
echo "stop"