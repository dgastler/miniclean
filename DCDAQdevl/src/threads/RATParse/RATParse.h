#ifndef __RATPARSE__
#define __RATPARSE__
//Ratparse processer takes in a raw CAEN event,
//parses the raw data into a DS structure and then
//passes that ds structure on to another processor.
//This is where one might add the event reconstruction
//for mini CLEAN to reconstruct one DS event from 
//multiple CAEN events.

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <BasicRATDSManager.h>
#include <xmlHelper.h>

#include <DSBlock.h>
#include "RAT/DS/Root.hh"

#include <v1720Event.h>



class RATParse : public DCThread 
{
 public:
  RATParse(std::string Name);
  ~RATParse(){};

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  //Memory Block manager interface
  MemoryBlockManager * Block_mem; //Input manager
  MemoryBlock * memBlock;//Local data structure that holds a pointer to data
  int32_t memBlockIndex;//Index to find our block from the block manager
  uint32_t currentDataIndex;

  //RATDS manager
  BasicRATDSManager * DS_mem; //Output manager
  DSBlock * dsBlock;
  int32_t dsIndex;

  //Statisticas
  time_t lastTime;
  time_t currentTime;
  //Input blocks
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;
  //Events
  uint32_t Events;
  uint64_t TimeStep_Events;
  uint64_t BadEvents;
  uint64_t TimeStep_BadEvents;

  //Put one event into a 
  void ProcessOneEvent(v1720Event &Event,RAT::DS::Root * DS);
  //Event parsing vectors
  std::vector<uint32_t> WindowHeaderVector;
  std::vector<uint16_t> WindowDataVector;

  //v1720Event parser
  v1720Event CurrentEvent;
};


#endif
