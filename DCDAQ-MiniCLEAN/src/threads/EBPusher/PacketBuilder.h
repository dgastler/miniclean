#ifdef __RATDS__

#ifndef __PACKETBUILDER__
#define __PACKETBUILDER__

#include <stdint.h>
#include <RAT/DS/DCDAQ_RATEVBlock.hh>
#include <DCNetPacketManager.h>
#include <EBDataPacket.h>
#include <DetectorInfo.h>

#include <math.h>
#include <sstream>

#include <DCThread.h>

class PacketBuilder
{
 public:
  PacketBuilder(DetectorInfo & _detectorInfo) : detectorInfo(_detectorInfo)
    {
      packetTypeFull = ReductionLevel::UNKNOWN;
      packetTypePartial = ReductionLevel::UNKNOWN;
      ebEvent=NULL;
      packetManager=NULL;
      packet=NULL;
      for(int i = 0; i < ReductionLevel::COUNT;i++)
	{
	  std::stringstream ss;
	  ss << "redLevel_" << i << "_TotalQ";
	  TotalQHist[i] = new TH1F(ss.str().c_str(),
				   ss.str().c_str(),
				   1000,0,1E6);
	  fprintf(stdout,"Created histogram %s\n",ss.str().c_str());
	}
    };
  virtual ~PacketBuilder(){};
  virtual bool Send(RATEVBlock * event);
  virtual void SetPacketManager(DCNetPacketManager* _packetManager){packetManager=_packetManager;}

  std::map<int,TH1F*> SendHistograms(){return TotalQHist;};

  double EventTotalQ;
  std::map<int,TH1F*> TotalQHist;

 protected:
  EBPacket::Event * ebEvent;

  DetectorInfo const & detectorInfo;

  bool SendSummaryEvent(RATEVBlock * event);
  bool SendChanEvent(RATEVBlock * event);


  int packetTypeFull;
  int packetTypePartial;
  
  DCNetPacketManager * packetManager;
  
 
  //====================================
  //packet interface 
  //====================================
  DCNetPacket *packet;            //Pointer to packet object
  uint8_t     *dataPtr;            //Ptr to the begining of the packet data
  uint8_t     *currentPtr;         //Ptr to current position in the packet
  uint32_t     dataSizeLeft;       //remaining packet data size
  uint8_t      packetNumber;       //Packet number for this EV
  int32_t      iDCPacket;          //Index to packet in MM
  //Resets all packet members except the packetNumber 
  //(used if breaking up packets)
  void ResetPacket();
  //Clears all packet meembers
  //(used when you are done sending packets)
  void ClearPacket();
  //====================================
  
  virtual void     SendPacket(int packetType);
  virtual bool     CheckPacket(uint32_t size,RATEVBlock * event);
  virtual int      AddEventHeader(RATEVBlock * event);

  bool CheckBoardSize_CHAN_ZLE_INTEGRAL(RATEVBlock * event,RAT::DS::Board * board);
  void AddChannel_CHAN_ZLE_INTEGRAL(RAT::DS::Channel * channel,int16_t PMTID);

  bool CheckBoardSize_CHAN_ZLE_WAVEFORM(RATEVBlock * event,RAT::DS::Board * board);
  void AddChannel_CHAN_ZLE_WAVEFORM(RAT::DS::Channel * channel,int16_t PMTID);

  bool CheckBoardSize_CHAN_PROMPT_TOTAL(RATEVBlock * event,RAT::DS::Board * board);
  void AddChannel_CHAN_PROMPT_TOTAL(RAT::DS::Channel * channel,int16_t PMTID);

  PacketBuilder();

};

#endif

#endif
