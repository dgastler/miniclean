--Module that coordinates the write and read accesses to a block ram.
--Boston University CLEAN project--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mem_flag is Port(
	RESET : in STD_LOGIC;
	CLK : in STD_LOGIC;
	INDEX : in unsigned(6 downto 0);
	WADDR : in unsigned(15 downto 0);
	RADDR : in unsigned(6 downto 0);
	EN : out STD_LOGIC;
	WERR : out STD_LOGIC;
	WR_EN : in STD_LOGIC;
	RD_ACK : in STD_LOGIC;
	RD_CONF : out STD_LOGIC);
end mem_flag;

architecture Behavioral of mem_flag is
	signal count : unsigned(3 downto 0) := x"1";
	signal inc_count : unsigned(3 downto 0) := x"2";
	signal last_ack : STD_LOGIC_VECTOR(2 downto 0) := "000";
	signal just_wrote : STD_LOGIC := '0';
	signal just_read : STD_LOGIC := '0';
begin
	inc_count <= count + x"1";
	just_wrote <= '1' when (WR_EN = '1') and (WADDR(15 downto 9) = INDEX)
						and (WADDR(8 downto 0) = '1' & x"FF") else '0';
	just_read <= '1' when (count /= x"F") and (RADDR = INDEX) 
						and (last_ack(2 downto 1) = "01") else '0';
	
	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			last_ack <= last_ack(1 downto 0) & RD_ACK;
			if RESET = '1' then
				WERR <= '0';
				RD_CONF <= '0';
				EN <= '0';
				count <= x"1";
			else
				if count = x"0" then
					WERR <= '1';--cannot overwrite values when not yet read
					RD_CONF <= '1';--okay to read out
				else
					WERR <= '0';
					RD_CONF <= '0';--change to '1' to allow unlimited reading
				end if;
				
				if WADDR(15 downto 9) = INDEX then
					EN <= '1';--write pointer currently at this memory block
				else
					EN <= '0';
				end if;
				if just_wrote = '1' then
					count <= x"0";--reset count when new data transfer finished
				elsif just_read = '1' then
					count <= inc_count;--increment count on every read
				else
					count <= count;
				end if;
			end if;
		end if;
	end process;
end Behavioral;

