#ifdef __RATDS__
 #include <EBPCAssembler.h> 
bool EBPCAssembler::ProcessCHAN_PROMPT_TOTAL(uint8_t *data, uint32_t size,RATDSBlock * event)
{
  //parsed channels (-1 for an error)
  bool ret = 0; 

  //Parsing pointer and size left.
  uint8_t * dataPtr = data;
  uint32_t  dataSizeLeft = size;

  //Check to see if we have enough space in the data for a EBPacket::Event
  if(sizeof(EBPacket::Event) > dataSizeLeft)
    {
      PrintError("Packet too small for EBPacket::Event (CHAN_PROMPT_TOTAL packet).\n");
      ret = false;
    }
  else
    //We have enough data to have a Event header packet.
    {   
      
      EBPacket::Event * eventHeader = (EBPacket::Event * ) dataPtr;
      dataPtr += sizeof(EBPacket::Event);        //move forward in dataPtr
      dataSizeLeft -= sizeof(EBPacket::Event);   //decrease remaining size

      //Boards in this event packet
      uint16_t boardCount = eventHeader->boardCount;
      //      fprintf(stdout,"boardCount: %u\n",boardCount);

      RAT::DS::EV * ev = event->ds->GetEV(0);

      //Set trigger values
      ev->SetTriggerPattern(eventHeader->eventTriggerPattern,
			    eventHeader->eventTriggerCounts);
      ev->SetEventID(eventHeader->eventID);
      ev->SetUTC(TriggerTimeToUTC(eventHeader->eventTime));
      event->eventTime = eventHeader->eventTime;
      ev->SetReductionLevel(eventHeader->dataType);      

      EventTotalQ = 0;

      for(uint16_t iBoard = 0; iBoard < boardCount; iBoard++)
	{
	  //Check if there is room for a new board structure
	  if(sizeof(EBPacket::Board) <= dataSizeLeft)
	    {
	      //Load the board structure from memory
	      EBPacket::Board * ebBoard = (EBPacket::Board *) dataPtr;
	      dataPtr += sizeof(EBPacket::Board);
	      dataSizeLeft -= sizeof(EBPacket::Board);

	      RAT::DS::Board * board = ev->AddNewBoard();
	      board->SetID(((ebBoard->header[1]) >> 27)&0x1F);
//	      fprintf(stderr,"Board header 0: 0x%08X\n",ebBoard->header[0]);
//	      fprintf(stderr,"Board header 1: 0x%08X\n",ebBoard->header[1]);
//	      fprintf(stderr,"Board header 2: 0x%08X\n",ebBoard->header[2]);
//	      fprintf(stderr,"Board header 3: 0x%08X\n",ebBoard->header[3]);
	      //	      fprintf(stdout,"Board ID: %u\n",board->GetID());
	      board->SetHeader(std::vector<uint32_t>((ebBoard->header),(ebBoard->header+4)));
	      board->SetFormat(RAT::DS::Board::CAENWFD_ZLE_FORMAT_2PACK);
	      
	      //Parse the assosiated channels (board->channelPattern bit n is 1 if there is data)
	      for(int8_t channelShift = 0;channelShift < 8;channelShift++)
		{
		  //Check if we have a channel for this bitshift
		  if(ebBoard->channelPattern & (uint8_t(0x1) << channelShift))
		    {
		      if(sizeof(EBPacket::Channel) <= dataSizeLeft)
			{
			  EBPacket::Channel * ebChannel = (EBPacket::Channel*) dataPtr;
			  dataPtr += sizeof(EBPacket::Channel);
			  dataSizeLeft -= sizeof(EBPacket::Channel);
			  
			  RAT::DS::Channel * channel = board->AddNewChannel();
			  channel->SetInput(channelShift);
			  channel->SetID(detectorInfo.GetChannelMap(board->GetID(),channelShift));

			  //			  fprintf(stdout,"Channel block count: %u\n",ebChannel->dataBlockCount);

			  if(ebChannel->dataBlockCount == 1)
			    {
			      if(sizeof(EBPacket::PromptTotalBlock) > dataSizeLeft)
				{
				  PrintError("Packet too small for EBPacket::PromptTotalBlock (CHAN_PROMPT_TOTAL packet).\n");
				  ret = false;
				  break;				  
				}
			      else
				{
				  EBPacket::PromptTotalBlock * promptTotalBlock = (EBPacket::PromptTotalBlock * ) dataPtr;
				  dataPtr += sizeof(EBPacket::PromptTotalBlock);      //move forward in dataPtr
				  dataSizeLeft -= sizeof(EBPacket::PromptTotalBlock); //decrease remaining size
				  
				  channel->SetPromptQ(promptTotalBlock->prompt);
				  channel->SetTotalQ(promptTotalBlock->total);
				  EventTotalQ += promptTotalBlock->total;
				  

				}
			    }
			  else
			    {
			      PrintError("Bad number of data blocks for Prompt/Total packet");
			    }
			}
		      else
			{
			  PrintError("Packet too small for EBPacket::Channel (CHAN_PROMPT_TOTAL packet).\n");
			  break;
			  ret = false;
			}
		    }
		  else
		    {
		      //Get PMT ID for this channel
		      int PMTID = detectorInfo.GetChannelMap(board->GetID(),channelShift);
		      //Check if detector info things this is a valid channel
		      if(PMTID>=0)
			{
			  //There is not data for this valid channel, add a blank channel
			  RAT::DS::Channel * channel = board->AddNewChannel();
			  channel->SetInput(channelShift);
			  channel->SetID(PMTID);		      
			  channel->SetPromptQ(0);
			  channel->SetTotalQ(0);
			}
		    }
		}
	      event->WFD.push_back(board->GetID());
	    }
	  else
	    {
	      PrintError("Packet too small for EBPacket::Channel (CHAN_PROMPT_TOTAL packet).\n");
	      break;
	      ret = false;
	    }
	}
      TotalQHist[ReductionLevel::CHAN_PROMPT_TOTAL]->Fill(EventTotalQ);
    }			      
  return ret;
}
#endif
