-------------------------------------------------------------------------------
-- Generic synchronizer per Mr Wu
-- S is rising-edge sensitive input
-- Y is one-clock wide synch'd output
--   output occurs 2-3 clocks after input edge
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity synch is Port(
	CLK : in STD_LOGIC;
	RST : in STD_LOGIC;
	S : in STD_LOGIC;
	Y : out STD_LOGIC);
end synch;

architecture synch_a of synch is
	signal st, s1, s2, s3 : STD_LOGIC;
begin
	-- toggle st on rising edge of S
	process (S, RST)
	begin
		if RST = '1' then
			st <= '0';
		elsif rising_edge(S) then
			st <= not st;
		end if;
	end process;
  
	-- synchronize st through s1, st, s3
	-- form output as (s2 xor s3) and resynchronize
	process (CLK, RST)
	begin
		if rising_edge(CLK) then
			s1 <= st;
			s2 <= s1;
			s3 <= s2;
			Y <= s2 xor s3;
		end if;
	end process;
end synch_a;