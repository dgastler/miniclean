#ifdef __RATDS__
#include <EBPusher.h>

void EBPusher::ProcessEvent()
{
  //Get event ID to send.
  int32_t index;
  if(FEPC_mem->GetSend(index))
    //We got an event from the Send queue
    {
      if(index != FEPC_mem->GetBadValue())
	{
	  //Get the DRPCBlock for this event
	  RATEVBlock * event = FEPC_mem->GetFEPCBlock(index);
	  //fprintf(stderr,"Got the FEPCBlock for this event\n");
	  if(SendData(event))
	    {
	      //Add the event to the free queue
	      //Event cleared in AddFree
	      if(!FEPC_mem->AddFree(index))
		{
		  //We are probably shutting down in this case
		  PrintError("Can't add to free queue. DCDAQ shutdown?\n");
		}
	    }
	  else
	    {
	      PrintError("Failed in SendData\n");
	    }
	}
      else
	{
	  PrintError("Bad event index\n");
	  //We are probably shutting down in this case
	}
    }
  else
    //We couldn't get a Send.       
    {
      //We are probably shutting down in this case
      PrintError("Can't get a Send event.  DCDAQ shutdown?\n");
      //      SendStop();
    }

}

bool EBPusher::SendData(RATEVBlock * event)
{
  bool ret = true;

  ret = packetBuilder.Send(event);

  return ret ;
}

#endif
