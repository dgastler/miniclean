--This module is designed to test the CRC of an ethernet frame.
--OUT_OK_STB and OUT_BAD_STB are strobes asserted after OUT_ETH_STREAM(8) drops, 
--to indicate if the CRC was correct or not.
--Original code by Xilinx.
--Translation into VHDL by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity eth_rx_crc is Port(
	CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	IN_ETH_STREAM : in STD_LOGIC_VECTOR(9 downto 0);--Input
	--Output data stream - delayed to match CRC calculation latency
	OUT_ETH_STREAM : out STD_LOGIC_VECTOR(9 downto 0);
	OUT_OK_STB : out STD_LOGIC;--CRC was correct
	OUT_BAD_STB : out STD_LOGIC);--CRC was incorrect
end eth_rx_crc;

architecture Behavioral of eth_rx_crc is
	signal init : STD_LOGIC;
	signal crc_reg : STD_LOGIC_VECTOR(31 downto 0);
	signal in_frm_prev : STD_LOGIC;
	signal crc_ok : STD_LOGIC;
	constant magic_number : STD_LOGIC_VECTOR(31 downto 0) := x"C704DD7B";

	component crc32_8 Port(
		CRC_REG : out STD_LOGIC_VECTOR(31 downto 0);
		CRC : out STD_LOGIC_VECTOR(7 downto 0);
		D : in STD_LOGIC_VECTOR(7 downto 0);
		CALC : in STD_LOGIC;
		INIT : in STD_LOGIC;
		D_VALID : in STD_LOGIC;
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC);
	end component;
begin
	crc_checksum : crc32_8 port map(
		CLK => CLK, 
		D => IN_ETH_STREAM(7 downto 0),--data input
		INIT => init,
		CALC => '1',
		D_VALID => IN_ETH_STREAM(9),--strobe bit
		CRC_REG => crc_reg, 
		CRC => open, 
		RESET => RESET);--global reset used as redundant signal

	init <= not IN_ETH_STREAM(8) and not in_frm_prev;
	crc_ok <= '1' when crc_reg = magic_number else '0';

	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			if RESET = '1' then
				OUT_ETH_STREAM <= "00" & x"00";
				OUT_OK_STB <= '0';
				OUT_BAD_STB <= '0';
				in_frm_prev <= '0';
			else
				--register output
				OUT_ETH_STREAM <= IN_ETH_STREAM;
				--generate special strobes
				OUT_OK_STB <= IN_ETH_STREAM(9) and not IN_ETH_STREAM(8) and in_frm_prev and crc_ok;
				OUT_BAD_STB <= IN_ETH_STREAM(9) and not IN_ETH_STREAM(8) and in_frm_prev and not crc_ok;
				if IN_ETH_STREAM(9) = '1' then
					in_frm_prev <= IN_ETH_STREAM(8);
				end if;
			end if;
		end if;
	end process;
end Behavioral;

