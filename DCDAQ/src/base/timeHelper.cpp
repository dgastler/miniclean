#include <timeHelper.h>

std::string GetTime(const time_t *now)
{
  std::string timeString;
  //Get timestamp
  if(ctime(now))
    {
      timeString.assign(ctime(now));  
    }
  else
    {
      timeString.assign("Bad time");
    }
  //only keep things before the first '\n'
  if(timeString.find('\n') != std::string::npos)
    {
      timeString = timeString.substr(0,timeString.find('\n'));
    }
  return(timeString);
}

std::string GetTime(const DCMessage::DCtime_t *now)
{
  //if we are on a 64 bit system
  if(sizeof(DCMessage::DCtime_t) == sizeof(time_t))
    {
      return(GetTime((time_t*) now));
    }
  //32bit machine
  else if(sizeof(DCMessage::DCtime_t) == 2*sizeof(time_t))
    {
      time_t * temp = (time_t *) now;
      return(GetTime(temp));
    }
  else
    {
      return("Bad DCtime_t!");
    }
}
