--Wrapper module for some block RAM's so that redundancy is reduced without
--using a Xilinx IP core to do the same thing (for consistency's sake).
--Signals ending in "A" are associated with the write-side and signals ending
--in "B" are associated with the read-side of the RAM. DOUTB changes a
--clock cycle after ADDRB changes--a situation that complicates almost every
--state machine in this design (especially when combined with subsequent delays).
--Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity dpbr2 is Port(
	RESET : in STD_LOGIC;
	CLKA : in  STD_LOGIC;
	WEA : in  STD_LOGIC;
	ADDRA : in  STD_LOGIC_VECTOR (9 downto 0);
	DINA : in  STD_LOGIC_VECTOR (31 downto 0);
	CLKB : in  STD_LOGIC;
	CLKEN : in STD_LOGIC;
	ADDRB : in  STD_LOGIC_VECTOR (11 downto 0);
	DOUTB : out  STD_LOGIC_VECTOR (7 downto 0));
end dpbr2;

architecture Behavioral of dpbr2 is
	signal wea1 : STD_LOGIC;
	signal wea2 : STD_LOGIC;
	signal doutb1 : STD_LOGIC_VECTOR(31 downto 0);
	signal doutb2 : STD_LOGIC_VECTOR(31 downto 0);
	signal dob : STD_LOGIC_VECTOR(31 downto 0);
	signal addr2 : STD_LOGIC_VECTOR(2 downto 0);
begin
	wea1 <= WEA and not ADDRA(9); 
	wea2 <= WEA and ADDRA(9);
	DOUTB <= dob(7 downto 0) when addr2(1 downto 0) = "00"
		else dob(15 downto 8) when addr2(1 downto 0) = "01"
		else dob(23 downto 16) when addr2(1 downto 0) = "10"
		else dob(31 downto 24);
	dob <= doutb2 when addr2(2) = '1' else doutb1;

	process(CLKB, CLKEN)
	begin
		if CLKB'event and CLKB = '1' and CLKEN = '1' then
			addr2 <= ADDRB(11) & ADDRB(1 downto 0);
		end if;
	end process;

	memory1 : RAMB16BWER generic map(
		DATA_WIDTH_A => 36,
		DATA_WIDTH_B => 36,
		WRITE_MODE_A => "READ_FIRST",
		WRITE_MODE_B => "READ_FIRST") port map(
		CLKA => CLKA,
		WEA(0) => wea1,
		WEA(1) => wea1,
		WEA(2) => wea1,
		WEA(3) => wea1,
		ENA => '1',
		REGCEA => '0',
		ADDRA(13 downto 5) => ADDRA(8 downto 0),
		ADDRA(4 downto 0) => "00000",
		DIA => DINA,
		DIPA => x"0",
		DOA => open,
		DOPA => open,
		RSTA => RESET,
		CLKB => CLKB,
		WEB => x"0",
		ENB => CLKEN,
		REGCEB => '0',
		ADDRB(13 downto 5) => ADDRB(10 downto 2),
		ADDRB(4 downto 0) => "00000",
		DIB => x"00000000",
		DIPB => x"0",
		DOB => doutb1,
		DOPB => open,
		RSTB => RESET);

	memory2 : RAMB16BWER generic map(
		DATA_WIDTH_A => 36,
		DATA_WIDTH_B => 36,
		WRITE_MODE_A => "READ_FIRST",
		WRITE_MODE_B => "READ_FIRST") port map(
		CLKA => CLKA,
		WEA(0) => wea2,
		WEA(1) => wea2,
		WEA(2) => wea2,
		WEA(3) => wea2,
		ENA => '1',
		REGCEA => '0',
		ADDRA(13 downto 5) => ADDRA(8 downto 0),
		ADDRA(4 downto 0) => "00000",
		DIA => DINA,
		DIPA => x"0",
		DOA => open,
		DOPA => open,
		RSTA => RESET,
		CLKB => CLKB,
		WEB => x"0",
		ENB => CLKEN,
		REGCEB => '0',
		ADDRB(13 downto 5) => ADDRB(10 downto 2),
		ADDRB(4 downto 0) => "00000",
		DIB => x"00000000",
		DIPB => x"0",
		DOB => doutb2,
		DOPB => open,
		RSTB => RESET);
end Behavioral;

