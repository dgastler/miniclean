//-----------------------------//
//---Header file for DCMYSQL---//

/*Check DCMYSQL.cpp for more 
  specific function descriptions*/


#ifndef __DCMYSQL__
#define __DCMYSQL__

#include <iostream>
#include <cstdio>
#include <vector>
#include <mysql/mysql.h>
#include <mysql/errmsg.h>
#include <xmlHelper.h>

class DCMySQL
{
 public:
  // Constructor and Destructor functions
  DCMySQL();
  ~DCMySQL();

  // Connection functions
  void SetServer(std::string mysql_server);
  void SetUser(std::string mysql_user);
  void SetPassword(std::string mysql_password);
  void SetDatabase(std::string mysql_database);
  void SetConnect(std::string mysql_server, std::string mysql_user, 
		  std::string mysql_password, std::string mysql_database);
  void SetConnect(xmlNode * baseNode);
  std::string GetError();

  std::string OpenConnect();
  bool IsConnected(){return isConnected;};
  void CloseConnect();
  void CheckConnectionError();

  // Query functions
  bool RunQuery(std::string mysql_query);
  void FreeResult();
  unsigned GetNumFields();
  unsigned GetNumRows();
  MYSQL_ROW GetRow();

  //======================================
  //These parse 2d data from a previous Query
  //They do not release the query result
  //======================================
  bool GetTable(std::vector<std::vector<std::string> > &table);
  bool GetFieldNames(std::vector<std::string> &table_columns);

  //================================================
  //Similar to getData, but useful for queries which 
  //expect a single value to be returned
  //================================================
  bool GetSingleValue(std::string mysql_query,std::string &value);

 private:
  // MySQL pointers
  MYSQL *ConnectionCheck, conn;
  MYSQL_RES *result;

  // database connection variables
  std::string server;
  std::string user;
  std::string password;
  std::string database;

  // query state variables
  std::string mysql_query;
  unsigned long int query_length;
  int query_state;
  bool isConnected;
};
 
#endif
