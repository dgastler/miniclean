#include <Thread_XML.h>

Thread_XML::Thread_XML(DCMySQL * conn,int _runNumber)
{
  DCMySQLConnection = conn;
  runNumber = _runNumber;
  threadNode = NULL;
}

//Queries the threads table to get the data necessary to build the individual threads
bool Thread_XML::Generate_XML(xmlNode * threadsNode,
			      std::string pcName,
			      std::string threadType,
			      std::string threadName,
			      xmlNode * responseNode)
{
  //Create a THREAD XML node in THREADS
  threadNode=xmlNewChild(threadsNode,NULL,
			 (xmlChar *)"THREAD",NULL);
  //Add thread TYPE
  xmlNewTextChild(threadNode,NULL,
		  BAD_CAST "TYPE",BAD_CAST threadType.c_str());
  //Add thread NAME
  xmlNewTextChild(threadNode,NULL,BAD_CAST "NAME",
		  BAD_CAST threadName.c_str());

  //Get all needed thread data from DB
  std::vector<std::vector<std::string> > threadTemplate;
  std::vector<std::vector<std::string> > threadData; //Note that this thread will be one larger
  std::vector<std::vector<std::string> > threadDefaults;
  std::vector<std::vector<std::string> > threadRequired;
  if(!FillData(pcName,
	       threadType,
	       threadName,
	       threadTemplate,
	       threadData,    
	       threadDefaults,
	       threadRequired))
    {
      fprintf(stderr,"Thread_XML failed to load data\n");
      return false;
    }
  
  //Build XML
  size_t fieldCount = threadTemplate[0].size();
  for(size_t iField = 0; iField < fieldCount;iField++)
    {
      //Check if there is an entry for this in threadData
      if(!boost::algorithm::iequals(threadData[0][iField],"NULL"))
	{	 
	  xmlNewTextChild(threadNode,NULL,
			  (xmlChar*) threadTemplate[0][iField].c_str(),
			  (xmlChar*) threadData[0][iField].c_str());
	}
      //if there isn't, check if there is a default
      else if(!boost::algorithm::iequals(threadDefaults[0][iField],"NULL"))
	{
	  xmlNewTextChild(threadNode,NULL,
			  (xmlChar*) threadTemplate[0][iField].c_str(),
			  (xmlChar*) threadDefaults[0][iField].c_str());
	}
      //if there isn't, check if this is required and fail if it is
      else if(boost::algorithm::iequals(threadRequired[0][iField],"YES"))
	{
	  //Failed
	  fprintf(stderr,"Thread_XML::Error required field %s not set\n",threadTemplate[0][iField].c_str());
	  return false;
	}
    }
  
  //Check to see if this thread needs the detector info XML
  if(boost::algorithm::iequals(threadData[0].back(),"YES"))
    {
      if(!GetDetectorInfo(pcName,threadType,threadName))
	{
	  return false;
	}
    }
  return true;
}

bool Thread_XML::FillData(const std::string &pcName,
			  const std::string &threadType,
			  const std::string &threadName,
			  std::vector<std::vector<std::string> > &threadTemplate,
			  std::vector<std::vector<std::string> > &threadData,
			  std::vector<std::vector<std::string> > &threadDefaults,
			  std::vector<std::vector<std::string> > &threadRequired)
{
  std::stringstream query;
  std::string fieldNames("data_01,data_02,data_03,data_04,data_05,data_06,data_07,data_08,data_09,data_10,data_11,data_12,data_13,data_14,data_15,data_16,data_17,data_18,data_19,data_20,data_21,data_22,data_23,data_24,data_25,data_26,data_27,data_28,data_29,data_30,data_31,data_32");
  
  //=====================================================
  //Gets meaning of data entries for specific thread type
  //=====================================================
  query << "SELECT " << fieldNames  
	<< " FROM Thread_Templates WHERE thread_type='" 
	<< threadType << "'";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      fprintf(stderr,"Thread_XML: Thread templates failed\n");
      return false;
    }
  DCMySQLConnection->GetTable(threadTemplate);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query

  //=====================================================
  //Get data for this thread
  //=====================================================
  query << "SELECT " << fieldNames << ",channel_map" << " FROM Thread_Threads"
	<< " where thread_type='" << threadType 
	<< "' AND thread_name='" << threadName 
	<< "' AND pc_name='" << pcName 
	<< "' AND run_number=" << runNumber 
	<< " ORDER BY order_id";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      fprintf(stderr,"Thread_XML: Thread data failed\n");
      return false;
    }
  DCMySQLConnection->GetTable(threadData);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query


  //=====================================================
  //Get the default values for datavalues
  //=====================================================
  query << "SELECT " << fieldNames 
	<<" FROM Thread_Defaults WHERE thread_type='"
	<< threadType << "'";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      fprintf(stderr,"Thread_XML: Thread defaults failed\n");
      return false;
    }
  DCMySQLConnection->GetTable(threadDefaults);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query

  //=====================================================
  //Get required data for this thread
  //=====================================================
  query << "SELECT " << fieldNames 
	<<" FROM Thread_Necessary WHERE thread_type='"
	<< threadType << "'";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      fprintf(stderr,"Thread_XML: Thread requirements failed\n");
      return false;
    }
  DCMySQLConnection->GetTable(threadRequired);
  DCMySQLConnection->FreeResult();
  query.str(std::string()); //Clear query

  


  //Check for bad data sizes
  if((threadData.size() == 1) &&
     (threadDefaults.size() == 1) &&
     (threadTemplate.size() == 1) &&
     (threadRequired.size() == 1))
    {
      //The +1 here is to account for threadData also containing the channel_map field
      if( !( (threadData[0].size() == (threadDefaults[0].size()+1) ) &&
	     (threadData[0].size() == (threadRequired[0].size()+1) ) &&
	     (threadData[0].size() == (threadTemplate[0].size()+1) ) ))
	{
	  return false;
	}
    }
  else
    {
      fprintf(stderr,"Thread_XML: Failed to have unique thread(%s:%s)\n",threadType.c_str(),threadName.c_str());
      fprintf(stderr,"    threadData:       %zu\n",threadData.size());
      fprintf(stderr,"    threadDefaults:   %zu\n",threadDefaults.size());
      fprintf(stderr,"    threadTemplate:   %zu\n",threadTemplate.size());
      fprintf(stderr,"    threadRequired:   %zu\n",threadRequired.size());
      return false;
    }
  return true;
}
  
