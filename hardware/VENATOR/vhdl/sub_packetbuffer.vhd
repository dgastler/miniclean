--This module helps respond to UDP packets by dealing with all of the header information:
--leaving sub_transactor to focus on the Control System protocol embedded within the packet.
--Original code by Jeremy Mans.
--Completely redesigned by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sub_packetbuffer is Port(
	CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	SEND_CKE : in STD_LOGIC;
	LINK_SPEED : in STD_LOGIC;
	INCOMING_READY : in STD_LOGIC;
	DONE_WITH_INCOMING : out STD_LOGIC;
	RXA : out unsigned(10 downto 0);
	RXW : in unsigned(31 downto 0);
	TXD : out unsigned(7 downto 0);
	TX_REQ : out STD_LOGIC;
	TX_SEND : out STD_LOGIC;
	TX_OK : in STD_LOGIC;
	FRAGMENT_COUNT : in unsigned(10 downto 0);
	UDP_SIZE : in unsigned(15 downto 0);
	REQ_ADDR : in unsigned(8 downto 0);
	REQ_LEN : out unsigned(8 downto 0);
	RESP_ADDR : in unsigned(8 downto 0);
	RESP_DATA : in unsigned(31 downto 0);
	RESP_LEN : in unsigned(8 downto 0);
	RESP_WE : in STD_LOGIC;
	REQ_AVAIL : out STD_LOGIC;
	RESP_DONE : in STD_LOGIC_VECTOR(1 downto 0));
end sub_packetbuffer;

architecture Behavioral of sub_packetbuffer is
	signal ravail : STD_LOGIC;
	signal respData : unsigned(31 downto 0);
	signal respAddr : unsigned(8 downto 0);
	signal respWe : STD_LOGIC;
	signal send_resp : STD_LOGIC;
	signal addra : unsigned(8 downto 0);
	signal addra1 : unsigned(8 downto 0);
	signal addra2 : unsigned(8 downto 0);
	signal addrb : unsigned(8 downto 0);
	signal we : STD_LOGIC;
	signal we1 : STD_LOGIC;
	signal we2 : STD_LOGIC;
	signal transmitting : STD_LOGIC;
	signal ip_len : unsigned(9 downto 0);
	signal udp_len : unsigned(9 downto 0);
	signal total_len : unsigned(10 downto 0);
	signal checksum_ok : STD_LOGIC_VECTOR(4 downto 0);
	signal checksum_ok1 : STD_LOGIC_VECTOR(4 downto 0);
	signal checksum_ok2 : STD_LOGIC_VECTOR(4 downto 0);
	signal transwe : STD_LOGIC;
	signal transword : unsigned(31 downto 0);
	signal transdata : unsigned(31 downto 0);
	signal transaddr : unsigned(8 downto 0);
	signal countdown : unsigned(13 downto 0);
	type state_type is (st_idle, st_pause, st_copyheader, st_wait_for_engine, 
		st_preidle1, st_preidle2, st_wait_for_engine2, st_wait_enough_time,
		st_tweakheader1, st_tweakheader2, st_tweakheader3);
	signal state : state_type := st_idle;

	component sub_packetresp port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		SEND_CKE : in STD_LOGIC;
		TX_REQ : out STD_LOGIC;
		TX_SEND : out STD_LOGIC;
		DONE : in STD_LOGIC_VECTOR(1 downto 0);
		TX_OK : in STD_LOGIC;
		CHECKSUM_OK : in STD_LOGIC_VECTOR(4 downto 0);
		TXD : out unsigned(7 downto 0);
		LEN : in unsigned(10 downto 0);
		RESPDATA : in unsigned(31 downto 0);
		RESPADDR : in unsigned(8 downto 0);
		RESPWE : in STD_LOGIC);
	end component;
begin
	outbuf : sub_packetresp port map(
		CLK => CLK,
		RESET => RESET,
		SEND_CKE => SEND_CKE,
		TX_REQ => transmitting,
		TX_SEND => TX_SEND,
		DONE(1) => RESP_DONE(1),
		DONE(0) => send_resp,
		TX_OK => TX_OK,
		CHECKSUM_OK => checksum_ok2,
		TXD => TXD,
		LEN => total_len,
		RESPDATA => respData,
		RESPADDR => respAddr,
		RESPWE => respWe);

	TX_REQ <= transmitting;
	addrb <= '0' & x"08" when addra = '0' & x"07" else
		'0' & x"07" when addra = '0' & x"08" else addra;--swap IP addresses
	respAddr <= transaddr + ('0' & x"0B") when ravail = '1' else addra2;
	RXA <= (REQ_ADDR & "00") + ("000" & x"2C") when ravail = '1' else (addrb & "00");
	respData <= transdata when ravail = '1' else ip_len(5 downto 0) & "000000" & 
		ip_len(9 downto 6) & transword(15 downto 0) when addra2 = '0' & x"04" else--insert IP length
		x"1100" & transword(15 downto 0) when addra2 = '0' & x"06" else--insert placeholder for IP checksum
		transword(15 downto 0) & transword(31 downto 16) when addra2 = '0' & x"09" else--swap UDP ports
		udp_len(5 downto 0) & "000000" & udp_len(9 downto 6) & udp_len(5 downto 0) & "000000" 
		& udp_len(9 downto 6) when addra2 = '0' & x"0A" else transword;--insert UDP length twice: one as checksum placeholder
	respWe <= transwe when ravail = '1' else we2;
	REQ_AVAIL <= ravail;
	REQ_LEN <= UDP_SIZE(10 downto 2) - "000000010";

	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			total_len <= (RESP_LEN & "00") + ("000" & x"2A");--ENET + IP + UDP
			ip_len <= ('0' & RESP_LEN) + ("00" & x"07");--IP + UDP (in 4 byte words)
			udp_len <= ('0' & RESP_LEN) + ("00" & x"02");--just UDP (in 4 byte words)
			addra2 <= addra1;
			addra1 <= addra;
			we2 <= we1;
			we1 <= we;
			checksum_ok2 <= checksum_ok1;
			checksum_ok1 <= checksum_ok;
			transdata <= RESP_DATA;
			transword <= RXW;
			transwe <= RESP_WE;
			transaddr <= RESP_ADDR;
			if RESET = '1' then
				we <= '0';
				checksum_ok <= "00000";
				send_resp <= '0';
				ravail <= '0';
				DONE_WITH_INCOMING <= '0';
				state <= st_idle;
			else
				case state is
				when st_idle =>
					we <= '0';
					checksum_ok <= "00000";
					send_resp <= '0';
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					if INCOMING_READY = '1' then
						state <= st_wait_for_engine;
					else
						state <= st_idle;
					end if;
				when st_wait_for_engine =>
					we <= '0';
					if RESP_WE = '1' then
						checksum_ok <= "00110";--payload data contributes to UDP checksum
					else
						checksum_ok <= "00000";
					end if;
					send_resp <= '0';
					ravail <= '1';
					DONE_WITH_INCOMING <= '0';
					if RESP_DONE(0) = '1' then
						state <= st_pause;
						addra <= '0' & x"00";
					else
						state <= st_wait_for_engine;
					end if;
				when st_pause =>--delay to allow respData to update
					we <= '1';
					checksum_ok <= "00000";
					send_resp <= '0';
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					state <= st_copyheader;
				when st_copyheader =>
					ravail <= '0';
					send_resp <= '0';
					if addra = '0' & x"0A" then
						we <= '0';
						checksum_ok <= "00000";
						--only throw away received packet when no more fragments intended
						DONE_WITH_INCOMING <= not RESP_DONE(1);
						state <= st_preidle1;
					else
						we <= '1';
						addra <= addra + ('0' & x"01");
						--compute IP and UDP checksums only from relevant data
						case addra is
						when '0' & x"03" => checksum_ok <= "11000";
						when '0' & x"04" => checksum_ok <= "11001";
						when '0' & x"05" => checksum_ok <= "10011";
						when '0' & x"06" => checksum_ok <= "11111";
						when '0' & x"07" => checksum_ok <= "11111";
						when '0' & x"08" => checksum_ok <= "00111";
						when '0' & x"09" => checksum_ok <= "00110";
						when others => checksum_ok <= "00000";
						end case;
						DONE_WITH_INCOMING <= '0';
						state <= st_copyheader;
					end if;
				when st_preidle1 =>--extra states to ensure that INCOMING_READY will have updated by the next cycle...
					we <= '0';
					checksum_ok <= "00000";
					send_resp <= '0';
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					state <= st_preidle2;
				when st_preidle2 =>
					we <= '0';
					checksum_ok <= "00000";
					send_resp <= '1';--...and provide time for checksum calculation
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					if RESP_DONE(1) = '1' then
						--Adding twelve to the countdown produces the interframe gap time (96 ns or 960 ns).
						--Since all other packets are generated in a one request - one response model,
						--this position in the code marks the only place capable of violating the gap time.
						if LINK_SPEED = '0' then
							countdown <= (total_len & "000") + ("00" & total_len & '0') + 
								('0' & x"0E0") - ("000" & FRAGMENT_COUNT);
						else
							countdown <= ("000" & total_len) + ('0' & x"00C") - ("000" & FRAGMENT_COUNT);
						end if;
						state <= st_wait_enough_time;
					else
						state <= st_idle;
					end if;
				when st_wait_enough_time =>--wait a carefully predetermined amount of
					we <= '0';--time such that the next fragment will be sent out
					checksum_ok <= "00000";--exactly when current fragment finished
					send_resp <= '0';
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					if countdown = '0' & x"000" then
						state <= st_wait_for_engine2;
					else
						countdown <= countdown - ('0' & x"001");
						state <= st_wait_enough_time;
					end if;
				when st_wait_for_engine2 =>
					we <= '0';
					if RESP_WE = '1' then
						checksum_ok <= "00110";--payload data contributes to UDP checksum
					else
						checksum_ok <= "00000";
					end if;
					send_resp <= '0';
					ravail <= '1';
					DONE_WITH_INCOMING <= '0';
					if RESP_DONE(0) = '1' then
						state <= st_tweakheader1;
					else
						state <= st_wait_for_engine2;
					end if;
				when st_tweakheader1 =>
					we <= '1';
					checksum_ok <= "11000";
					addra <= '0' & x"04";--alter IP length
					send_resp <= '0';
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					state <= st_tweakheader2;
				when st_tweakheader2 =>
					we <= '1';
					checksum_ok <= "00110";
					addra <= '0' & x"0A";--alter UDP length
					send_resp <= '0';
					ravail <= '0';
					DONE_WITH_INCOMING <= '0';
					state <= st_tweakheader3;
				when st_tweakheader3 =>
					we <= '0';
					checksum_ok <= "00000";
					send_resp <= '0';
					ravail <= '0';
					--only throw away received packet when no more fragments intended
					DONE_WITH_INCOMING <= not RESP_DONE(1);
					state <= st_preidle1;
				end case;
			end if;
		end if;
	end process;
end Behavioral;
