#ifdef __CAENLIBS__
#include <v1720Readout.h>

void v1720Readout::MainLoop()
{ 
  //Get a new block if we don't already have one
  if(IsReadReady(Mem->GetFreeFDs()[0]) &&
     (blockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Mem->GetFree(blockIndex);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = Mem->GetBlock(blockIndex);
	  block->dataSize = 0;
	  //	  usleep(1000);
	}
    }
  
  //Fill the current block with data
  if(blockIndex != FreeFullQueue::BADVALUE)
    {      
      //Readout loop
      uint32_t nWFDs = WFD.size();  //Won't change in this loop      
      //Loop over all the wfds a minimum of this many times
      //before passing off the current block.
      for(uint32_t iBlockReadout = 0; 
	  iBlockReadout < MaxReadoutsPerBlock;
	  iBlockReadout++)
	{
	  //Check if we have the minimum amount of free space available 
	  //in the current block. 
	  if((block->allocatedSize - block->dataSize) > MinimumFreeBufferSpace)
	    {	      
	      for(uint32_t iWFD = 0; iWFD < nWFDs;iWFD++)
		{
		  int32_t readoutReturn = WFD[iWFD]->Readout(*block,3);
		  if((readoutReturn == OK)||(readoutReturn == BUFFERFULL))
		    {
		      
		      TimeStep_EventCount[iWFD]   += WFD[iWFD]->GetNumberOfEvents();
		      TimeStep_ReadoutNumber[iWFD]++;
		      TimeStep_ReadoutSize[iWFD]  += WFD[iWFD]->GetTransferredBytes();
		      TimeStep_Occupancy[iWFD]    += WFD[iWFD]->GetOccupancy();
		      
		      //Return block for processing.
		      if(readoutReturn == BUFFERFULL)
			{			     
			  iBlockReadout = MaxReadoutsPerBlock; 
			  iWFD = nWFDs;
			}
		    }
		  else
		    {
		      //Print error and stop DAQ
		      char * buffer = new char[100];
		      sprintf(buffer,
			      "WFD %d readout error %d",
			      iWFD,readoutReturn);			  
		      //		      PrintWarning(buffer);
		      
		      //Exit loop
		      iBlockReadout = MaxReadoutsPerBlock; 
		      iWFD = nWFDs;
		      //		      if((readoutReturn == NOTREADY) ||
		      //			 (readoutReturn == CAEN_DGTZ_CommError))
		      //{
		      //This is a bad error.  Stop the DAQ
		      PrintError(buffer);
		      Loop = false;
		      Mem->AddFree(blockIndex);
		      RemoveReadFD(IsReadReady(Mem->GetFreeFDs()[0]));			  
		      SetupSelect();
		      return;
		      //			  break;			  
		      //			}
		      delete [] buffer;
		    }// readoutReturn
		}
	    }//Block size left check
	  else if(iBlockReadout == 0)
	    {
	      //This is a bad enough error to shut down the DAQ
	      PrintError("Not enough memory allocated!");
	    }
	}
      //Pass off memory block
      if(block->dataSize != 0)
	Mem->AddFull(blockIndex);
    }
}

void  v1720Readout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  DCMessage::Message message;
  message.SetType(DCMessage::STATISTIC);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t TimeStep = currentTime-lastTime;
      StatMessage::StatRate ssRate;
      StatMessage::StatFloat ssFloat;
                  
      size_t nWFDs = WFD.size();
      for(size_t iWFD= 0; iWFD < nWFDs;iWFD++)
	{
	  //Generate text for enum lookup
	  char WFDID[] = "WFD_00";	  
	  if((WFD[iWFD]->GetBoardID() < 99) &&
	     (WFD[iWFD]->GetBoardID() > 0))
	    sprintf(WFDID,"WFD_%02u",(unsigned)WFD[iWFD]->GetBoardID());


	  //Send Event numbers
	  ssFloat.sBase.type = StatMessage::OCCUPANCY;	  
	  if(TimeStep_ReadoutNumber[iWFD] != 0)
	    ssFloat.val = double(TimeStep_Occupancy[iWFD])/double(TimeStep_ReadoutNumber[iWFD]);
	  else
	    ssFloat.val = 0;
	  ssFloat.sInfo.time = currentTime;
	  ssFloat.sInfo.interval = TimeStep;
	  ssFloat.sInfo.subID = SubID::GetType(WFDID);
	  ssFloat.sInfo.level = Level::BLANK;
	  message.SetData(ssFloat);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);

	  
	  //Send Bytes transfered.

	  //Save value in the first 32 bits
	  ssRate.sBase.type = StatMessage::DATA_RATE;
	  ssRate.count = TimeStep_ReadoutSize[iWFD];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::GetType(WFDID);
	  ssRate.sInfo.level = Level::BLANK;
	  ssRate.units = Units::BYTES;	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);


	  
	  //Send Readout numbers
	  ssRate.sBase.type = StatMessage::READOUT_RATE;
	  ssRate.count = TimeStep_ReadoutNumber[iWFD];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::GetType(WFDID);
	  ssRate.sInfo.level = Level::BLANK;
	  ssRate.units = Units::NUMBER;	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);
	  
	  
	  //Send Event numbers
	  ssRate.sBase.type = StatMessage::EVENT_RATE;
	  ssRate.count = TimeStep_EventCount[iWFD];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::GetType(WFDID);
	  ssRate.sInfo.level = Level::BLANK;
	  ssRate.units = Units::NUMBER;	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);

	  TimeStep_Occupancy[iWFD] = 0;
	  TimeStep_ReadoutSize[iWFD] = 0;
	  TimeStep_EventCount[iWFD] = 0;
	  TimeStep_ReadoutNumber[iWFD] = 0;		  
	}
      
               
      //Setup next time
      lastTime = currentTime;
    }
}

#endif
