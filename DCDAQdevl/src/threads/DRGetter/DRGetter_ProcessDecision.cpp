#include <DRGetter.h>
void DRGetter::ProcessDecision()
{  
  EventDecision decision;
  //Get the next decision in the queue
  if(DecisionQueue.Get(decision,true))
    {
    }
  else
    {
      PrintError("Can not read from DecisionQueue!");
      SendStop();
      return;
    }

  //Get this event from the Event hash
  int32_t evIndex = FEPC_mem->GetBadValue();      
  uint64_t password = FEPC_mem->GetStorageBlankPassword();
  FEPCBlock * evBlock = NULL;
  int64_t retFEPCmem = FEPC_mem->GetStorageEvent(decision.ID,
						 evIndex,
						 password);
  if(retFEPCmem == returnDCVectorHash::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    {      
      //Get our evBlock
      evBlock = FEPC_mem->GetFEPCBlock(evIndex);
      //Update the reduction decision.
      evBlock->reductionLevel = decision.reductionLevel;
      //Remove the entry from the storage VH
      if(FEPC_mem->ClearStorageEvent(decision.ID,password) !=
	 returnDCVectorHash::OK)
	{	  
	  PrintError("ClearStorageEvent failed!");
	  SendStop();
	}
      else
	{
	  //Add the index to the Send queue
	  if(FEPC_mem->AddSend(evIndex))
	    {
	    }
	  else
	    {
	      PrintError("Could not add index to send queue!");
	      SendStop();
	    }
	}
    }
  else if(retFEPCmem >= returnDCVectorHash::COLLISION)    
    //We have a collision
    {
      MoveToBad(decision.ID,BadEvent::STORAGE_COLLISION);
    }
  else if(retFEPCmem == returnDCVectorHash::EMPTY_KEY)
    //We have an empty value... this is very strange
    {
      MoveToBad(decision.ID,BadEvent::STORAGE_EMPTY);
    }
  else if(retFEPCmem == returnDCVectorHash::BAD_PASSWORD ||
	  (retFEPCmem == returnDCVectorHash::BAD_MUTEX))
    {
      //Return the event to the DecisionQueue
      DecisionQueue.Add(decision,true);
    }
  else
    //Some other error that should never happen
    {
      char * buffer = new char[1000];
# if __WORDSIZE == 64
      sprintf(buffer,"Error in FEPC GetEvent: %ld(%ld)\n",
	      retFEPCmem,
	      decision.ID);
# else 
      sprintf(buffer,"Error in FEPC GetEvent: %lld(%lld)\n",
	      retFEPCmem,
	      decision.ID);
# endif
      PrintError(buffer);
      delete [] buffer;
      SendStop();	  
    }
}

void DRGetter::MoveToBad(int64_t eventID,int32_t errorCode)
{
  int32_t badIndex = FEPC_mem->GetBadValue();
  if(FEPC_mem->AddBadEvent(eventID,
			   badIndex,
			   errorCode))
    {	  
    }
  else
    {
      PrintError("Could not add event to bad queue");
      SendStop();
    }
}
