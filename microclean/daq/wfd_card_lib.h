#if !defined(WFD_CARD_LIB)
#define WFD_CARD_LIB


#include "vme_crate_lib.h"


class WFD_Card
{
 public:
  WFD_Card();
  ~WFD_Card();

  int setupWFD(VME_Crate*, int, u_int32_t, u_int32_t, int, int, int, int, int,u_int8_t,u_int8_t,u_int8_t,u_int8_t);
  int getslot();
  u_int32_t getf0ADER();
  u_int32_t getf1ADER();
  int getcardID();
  int getpersonality();
  int  getWindowSize(int i);
  void setWindow(int i , u_int32_t * pointer);
  u_int32_t * getWindow(int i);
  void setChanID(int i,int id);
  int  getChanID(int i);


  int start();
  int stop();
  int read_data(int,u_int32_t,u_int32_t*);
  

  int resetfifo(int channel);
  int blockcount(int channel);



 private:
  
  VME_Crate *Crate;
  void getbytes(u_int32_t word, u_int8_t bytes[4]);
  u_int32_t address;
  int slot;
  u_int32_t f0ADER;
  u_int32_t f1ADER;
  u_int32_t Windows;
  int cardID;
  u_int32_t scale;
  int personality;
  int WindowSize[4];
  u_int32_t * Window[4];
  int ChanID[4];
  
};
#endif
