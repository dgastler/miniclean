--Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity trigger_counter is Port(
	CLOCK : in STD_LOGIC;
	STIMULUS : in STD_LOGIC_VECTOR(1 downto 0);
	TRIGGER : in STD_LOGIC;
	COUNT : out unsigned(3 downto 0));
end trigger_counter;

architecture Behavioral of trigger_counter is
	signal trigger_count : unsigned(3 downto 0) := x"0";
	signal state : STD_LOGIC;
begin
	COUNT <= trigger_count;
	process(CLOCK)
	begin
		if CLOCK'event and CLOCK = '1' then
			state <= TRIGGER;
			if STIMULUS = "01" then
				trigger_count <= "000" & TRIGGER;
			elsif STIMULUS(1) = '1' and state = '0' and TRIGGER = '1' then
				trigger_count <= trigger_count + x"1";
			end if;
		end if;
	end process;
end Behavioral;

