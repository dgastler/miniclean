#ifdef __RATDS__
#include <EBPCAssembler.h> 
bool EBPCAssembler::ProcessCHAN_ZLE_WAVEFORM(uint8_t *data, uint32_t size,RATDSBlock * event)
{
  //parsed channels (-1 for an error)
  bool ret = true; 

  //Parsing pointer and size left.
  uint8_t * dataPtr = data;
  uint32_t  dataSizeLeft = size;

  //Check to see if we have enough space in the data for a EBPacket::Event
  if(sizeof(EBPacket::Event) > dataSizeLeft)
    {
      PrintError("Packet too small for EBPacket::Event (CHAN_ZLE_WAVEFORM packet). /n");
      ret = false;
    }
  else
    //We have enough data to have a Event header packet.
    { 
      EBPacket::Event * eventHeader = (EBPacket::Event * ) dataPtr;
      dataPtr += sizeof(EBPacket::Event);        //move forward in dataPtr
      dataSizeLeft -= sizeof(EBPacket::Event);   //decrease remaining size

      //Boards in this event packet
      uint16_t boardCount = eventHeader->boardCount;
      
      RAT::DS::EV * ev = event->ds->GetEV(0);

      //Set trigger values
      ev->SetTriggerPattern(eventHeader->eventTriggerPattern,
			    eventHeader->eventTriggerCounts);
      ev->SetEventID(eventHeader->eventID);
      ev->SetUTC(TriggerTimeToUTC(eventHeader->eventTime));
//      fprintf(stderr,"EBPCAssembler time: 0x%016zX %e\n",eventHeader->eventTime,eventHeader->eventTime*8e-9);
//      fprintf(stderr,"EBPCAssembler time: %ld %ld\n",ev->GetUTC().GetTimeSpec().tv_sec,ev->GetUTC().GetTimeSpec().tv_nsec);
      event->eventTime=eventHeader->eventTime;
      ev->SetReductionLevel(eventHeader->dataType);      

      EventTotalQ = 0;

      for(uint16_t iBoard = 0; iBoard < boardCount; iBoard++)
	{
	  //Check if there is room for a new board structure
	  if(sizeof(EBPacket::Board) <= dataSizeLeft)
	    {
	      //Load the board structure from memory
	      EBPacket::Board * ebBoard = (EBPacket::Board *) dataPtr;
	      dataPtr += sizeof(EBPacket::Board);
	      dataSizeLeft -= sizeof(EBPacket::Board);

	      RAT::DS::Board * board = ev->AddNewBoard();
	      board->SetID(((ebBoard->header[1]) >> 27)&0x1F);
	      board->SetHeader(std::vector<uint32_t>((ebBoard->header),(ebBoard->header+4)));
	      board->SetFormat(RAT::DS::Board::CAENWFD_ZLE_FORMAT_2PACK);
	      
	      //Parse the assosiated channels (board->channelPattern bit n is 1 if there is data)
	      for(int8_t channelShift = 0;channelShift < 8;channelShift++)
		{
		  //Check if we have a channel for this bitshift
		  if(ebBoard->channelPattern & (uint8_t(0x1) << channelShift))
		    {
		      if(sizeof(EBPacket::Channel) <= dataSizeLeft)
			{
			  EBPacket::Channel * ebChannel = (EBPacket::Channel*) dataPtr;
			  dataPtr += sizeof(EBPacket::Channel);
			  dataSizeLeft -= sizeof(EBPacket::Channel);
			  
			  RAT::DS::Channel * channel = board->AddNewChannel();
			  channel->SetInput(channelShift);
			  channel->SetID(detectorInfo.GetChannelMap(board->GetID(),channelShift));
			  //Make sure that prompt and total Q are set to zero for this reduction level
			  channel->SetPromptQ(0);
			  channel->SetTotalQ(0);
			  uint8_t dataBlockCount = ebChannel->dataBlockCount;
			  for(uint8_t iBlock = 0;iBlock < dataBlockCount;iBlock++)
			    {
			      if(sizeof(EBPacket::WaveformBlock) > dataSizeLeft)
				{
				  PrintError("Packet too small for EBPacket::WaveformBlock (CHAN_ZLE_WAVEFORM packet).\n");
				  ret = false;
				  break;
				}
			      else
				{
				  EBPacket::WaveformBlock * waveformBlock = (EBPacket::WaveformBlock *) dataPtr;
				  dataPtr += sizeof(EBPacket::WaveformBlock);
				  dataSizeLeft -= sizeof(EBPacket::WaveformBlock);
				  
				  if(waveformBlock->dataBytes > dataSizeLeft)
				    {
				    
				      char * buffer = new char[100];
				      sprintf(buffer,"Packet too small for EBPacket::WaveformBlock.dataBytes (CHAN_ZLE_WAVEFORM packet).  Data size left: %u; Waveform block size: %u\n",dataSizeLeft,waveformBlock->dataBytes);
				      PrintError(buffer);
				      //PrintError("Packet too small for EBPacket::WaveformBlock.dataBytes (CHAN_ZLE_WAVEFORM packet).\n");
				      delete [] buffer;
				      ret = false;
				      break;
				    }
				  else			    
				    {
				      //Fill the waveform data into the Raw structure      	
				      RAT::DS::RawBlock * rawBlock = channel->AddNewRawBlock();
				      
				      //set the start time of the RawWaveform	
				      rawBlock->SetOffset(waveformBlock->startTime);
				      std::vector<uint16_t> samples( (uint16_t *)dataPtr,
								     ((uint16_t *)dataPtr) + 
								     waveformBlock->dataBytes/sizeof(uint16_t));
				      rawBlock->SetSamples(samples);
				      dataPtr      +=waveformBlock->dataBytes;
				      dataSizeLeft -=waveformBlock->dataBytes;			  

				      double channelOffset = double(detectorInfo.GetPMTOffset(channel->GetID()));
				      for(uint iSample=0;iSample<samples.size();iSample++)
					{
					  EventTotalQ += (channelOffset - samples[iSample]);
					}
				    }
				}
			    }
			}
		      else
			{
			  PrintError("Packet too small for EBPacket::Channel (CHAN_ZLE_WAVEFORM packet).\n");
			  break;
			  ret = false;
			}
		    }
		  else
		    {
		      //Get PMT ID for this channel
		      int PMTID = detectorInfo.GetChannelMap(board->GetID(),channelShift);
		      //Check if detector info things this is a valid channel
		      if(PMTID>=0)
			{
			  //There is not data for this valid channel, add a blank channel
			  RAT::DS::Channel * channel = board->AddNewChannel();
			  channel->SetInput(channelShift);
			  channel->SetID(PMTID);		      
			  channel->SetPromptQ(0);
			  channel->SetTotalQ(0);
			}
		    }
		}	      
	      event->WFD.push_back(board->GetID());
	    }
	  else
	    {
	      PrintError("Packet too small for EBPacket::Channel (CHAN_ZLE_WAVEFORM packet).\n");
	      break;
	      ret = false;
	    }
	}
      TotalQHist[ReductionLevel::CHAN_ZLE_WAVEFORM]->Fill(EventTotalQ);
    }
  return ret;

}
#endif
