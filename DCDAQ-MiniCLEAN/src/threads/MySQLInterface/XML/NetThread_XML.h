#ifndef NETTHREAD_XML_H
#define NETTHREAD_XML_H

#include <Thread_XML.h>

class NetThread_XML: public Thread_XML
{
 public:
  NetThread_XML(DCMySQL *conn,int runNumber);
  virtual bool Generate_XML(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responsesNode);
};

#endif
