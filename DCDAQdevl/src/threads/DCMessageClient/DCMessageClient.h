#ifndef __DCMESSAGECLIENT__
#define __DCMESSAGECLIENT__

#include <DCThread.h>
#include <DCNetThread.h>

#include <Connection.h>
#include <netHelper.h>

class DCMessageClient : public DCThread 
{
 public:
  DCMessageClient(std::string Name);
  ~DCMessageClient();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);  
  virtual void MainLoop();
 private:

  void ProcessChildMessage();

  //Process messages in/out
  void RouteRemoteDCMessage();
  bool RouteLocalDCMessage(DCMessage::Message &message);
  bool SendMessage(DCMessage::Message &message);

  //Handle connections
  bool StartNewConnection();
  bool ProcessNewConnection();
  void DeleteConnection();
  //Vector of connections to server DCDAQ session
  Connection DCDAQServer;
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
};


#endif
