#include <DetectorInfo.h>

uint32_t DetectorInfo::ProcessWFDNode(xmlNode * wfdNode)
{
  uint32_t typesFound = InfoTypes::NONE;
  //===============================================
  //Read the Board ID
  //===============================================
  uint16_t boardID = 0;
  if(FindSubNode(wfdNode,"BOARDID") == NULL)		    
    {return typesFound;}//Board id not found
  //Get the Board ID.
  GetXMLValue(wfdNode,"BOARDID","DetectorInfo",boardID);

  //===============================================
  //Load pmt info from the XML 
  //===============================================
  std::vector<PMTInfo> pmtInfo;
  size_t PMTNodeCount = NumberOfNamedSubNodes(wfdNode,"PMT");
  for(size_t iPMT =0; iPMT < PMTNodeCount;iPMT++)
    {
      xmlNode * pmtNode = FindIthSubNode(wfdNode,"PMT",iPMT);
      typesFound |= ProcessPMTNode(pmtNode,pmtInfo,boardID);
    }

  //===============================================
  //Add PMT info to the vectors used in this thread
  //===============================================
  //Resize ChannelMap to hold boardID
  if(ChannelMap.size() <= boardID)
    {
      ChannelMap.resize(boardID+1);
    }
  //Increase the count of WFDs
  WFDCount++;
  typesFound |= InfoTypes::WFD_COUNT;
  //load pmt info into ChannelMap,PMTOffset,PMTPreSampleCount
  for(std::vector<PMTInfo>::iterator it = pmtInfo.begin(); 
      it != pmtInfo.end();
      it++)
    {
      //Add board channel to PMT info
      if(ChannelMap[boardID].size() <= it->WFDPos)
	{
	  ChannelMap[boardID].resize(it->WFDPos + 1,BAD_CHANNEL_ID);
	}
      ChannelMap[boardID][it->WFDPos] = it->PMTID;
      
      //Add in pmt to ADC offset
      if(PMTOffset.size() <= it->PMTID)
	{
	  PMTOffset.resize(it->PMTID + 1,0);
	}
      PMTOffset[it->PMTID] = it->ADCOffset;

      //Add in pmt presamples 
      if(PMTPreSampleCount.size() <= it->PMTID)
	{
	  PMTPreSampleCount.resize(it->PMTID + 1,0);	  
	}
      PMTPreSampleCount[it->PMTID] = it->PreSamples;
    }
  return typesFound;
}

uint32_t DetectorInfo::ProcessPMTNode(xmlNode * pmtNode,std::vector<PMTInfo> & pmtInfo,uint16_t boardID)
{
  PMTInfo pmt;
  uint32_t typesFound = InfoTypes::NONE;
  if(GetPMTValue(pmtNode,"WFDPOS",pmt.WFDPos))
    typesFound |= InfoTypes::CHANNEL_MAP;
  if(GetPMTValue(pmtNode,"PMTID",pmt.PMTID))
    typesFound |= InfoTypes::PMT_ID;
  if(GetPMTValue(pmtNode,"ADC_OFFSET",pmt.ADCOffset))
    typesFound |= InfoTypes::PMT_OFFSET;
  if(GetPMTValue(pmtNode,"PRESAMPLES",pmt.PreSamples))
    typesFound |= InfoTypes::PMT_PRESAMPLES;
  printf("  Mapping WFD board %d.%d to PMT %d\n",
	 boardID,
	 pmt.WFDPos,
	 pmt.PMTID);
  printf("    Pre-samples: %u\n",pmt.PreSamples);
  printf("    ADC offset:  %f\n",pmt.ADCOffset);
  pmtInfo.push_back(pmt);
  return typesFound;
}

template<class T>
bool DetectorInfo::GetPMTValue(xmlNode *node,std::string name,T & value)
{
  if(FindSubNode(node,name.c_str()) ==NULL)		    
    {
      //Value not found
      return false;
    } 
  GetXMLValue(node,name.c_str(),"DetectorInfo",value);
  return(true);
}

uint32_t DetectorInfo::Setup(xmlNode * SetupNode)
{
  infoTypesFound = 0x0; //Nothign found yet

  //Look for ChannelMap information
  xmlNode * channelMapNode = FindSubNode(SetupNode,"CHANNELMAP");
  //Parse the pmtlist data
  if(channelMapNode != NULL)
    {
      //Load the WFD structures and fill the three helper data structures
      //1) ChannelMap
      //2) PMTOffset
      //3) PMTPreSampleCount
      size_t WFDCount= NumberOfNamedSubNodes(channelMapNode,"WFD");
      if(WFDCount >0)
	infoTypesFound |= InfoTypes::WFD_COUNT;
      //Clear data structures
      ChannelMap.clear();
      PMTOffset.clear();
      PMTPreSampleCount.clear();
      for(size_t iWFD = 0; iWFD < WFDCount; iWFD++)
	{
	  //Process this WFD node	  
	  xmlNode * wfdNode = FindIthSubNode(channelMapNode,"WFD",iWFD);
	  infoTypesFound |= ProcessWFDNode(wfdNode);	  
	}
    }

  //Look for Event property info
  infoTypesFound |= ProcessEventData(SetupNode);

  return infoTypesFound;
}

uint32_t DetectorInfo::ProcessEventData(xmlNode * SetupNode)
{
  uint32_t ret = InfoTypes::NONE;
  //Get event integration start time
  if(FindSubNode(SetupNode,"EventIntegrationStartTime") != NULL)
    {
      GetXMLValue(SetupNode,
		  "EventIntegrationStartTime",
		  "DetectorInfo",
		  eventStartTime);
      ret |= InfoTypes::EVENT_START;
    }
  //Get event integration end time
  if(FindSubNode(SetupNode,"EventIntegrationEndTime") != NULL)
    {      
      GetXMLValue(SetupNode,
		  "EventIntegrationEndTime",
		  "DetectorInfo",
		  eventEndTime);
      ret |= InfoTypes::EVENT_END;
    }
  //Get prompt integration end time
  if(FindSubNode(SetupNode,"PromptIntegrationEndTime") != NULL)
    {
      GetXMLValue(SetupNode,
		  "PromptIntegrationEndTime",
		  "DetectorInfo",
		  promptIntegrationEndTime);
      ret |= InfoTypes::EVENT_PROMPT;
    }
  return ret;
}
