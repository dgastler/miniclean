#ifdef __RATDS__
#include <FEPusher.h>

bool FEPusher::ProcessNewConnection()
{
  //create a DCNetThread for this new connection
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().GetFullPacketFD());
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().GetMessageFD());      

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::CONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      printf("FEPC::DRGetter connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().GetRemoteAddress().GetAddrStr().c_str());
      SetupSelect();
    }
 else
   {
     //Connection creation failed
     return(false);
   }
  return(true);
}

void FEPusher::DeleteConnection(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
 

  //Remove the incoming packet manager fds from list
  RemoveReadFD(server[iConn].GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(server[iConn].GetMessageFD());          
  
  SetupSelect();
  server.DeleteConnection(iConn);
}

void FEPusher::ProcessChildMessage(size_t iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
  DCMessage::Message message = server[iConn].GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      std::stringstream ss;
      ss << "Lost connection with " << server[iConn].GetRemoteAddress().GetAddrStr();
      PrintError(ss.str().c_str());

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::DISCONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      DeleteConnection(iConn);

    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}
#endif
