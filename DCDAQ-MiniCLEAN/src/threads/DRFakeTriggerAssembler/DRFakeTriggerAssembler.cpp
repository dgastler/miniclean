#include <DRFakeTriggerAssembler.h>

DRFakeTriggerAssembler::DRFakeTriggerAssembler(std::string Name)
{
  SetType("DRFakeTriggerAssembler");
  SetName(Name);
  Loop = true;
  triggerEventID = 0;
}

bool DRFakeTriggerAssembler::Setup(xmlNode * SetupNode, 
				   std::vector<MemoryManager*> &MemManager)
{
  Ready = false;
  //================================
  //Get the DRPC memory manager
  //================================
  if(!FindMemoryManager(DRPC_mem,SetupNode,MemManager))
    {
      return false;
    }

  //===============================
  //Load detector info
  //===============================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::WFD_COUNT);
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }

  //Get the wait time between triggers.
   if(FindSubNode(SetupNode,"TRIGGERPERIOD") == NULL)
    {
      Period = 0;
    }
  else
    GetXMLValue(SetupNode,"TRIGGERPERIOD",GetName().c_str(),Period);
   Ready = true;
   return true;
}

void DRFakeTriggerAssembler::SetPeriod(useconds_t _Period)
{
  Period = _Period;
  SetDefaultSelectTimeout(Period/1000000,Period%1000000);
}

bool DRFakeTriggerAssembler::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      DRPC_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      SetPeriod(Period);
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}
