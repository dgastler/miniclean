#ifndef __EVNT_FULL_WAVEFORM__
#define __EVNT_FULL_WAVEFORM__

#include <PacketBuilder.h>

class EVNT_FULL_WAVEFORM : public PacketBuilder
{
 public:
  EVNT_FULL_WAVEFORM()
  {
    ClearPacket();
  };
  virtual ~EVNT_FULL_WAVEFORM(){ClearPacket();};
  virtual bool Send(FEPCBlock * event)
  {
    printf("EVNT_FULL_WAVEFORM not written.  FAIL!\n");
    return(false);
  }

};

#endif
