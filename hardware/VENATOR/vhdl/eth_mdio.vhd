--This module reads the PHY status using the MDIO interface.  Specifically, 
--it discovers the current LINK speed of the ethernet connection which is 
--used by the external ethernet routing logic. For a diagram of MDIO timing 
--see http://upload.wikimedia.org/wikipedia/commons/9/9d/MDIO_READ_WRITE.jpg
--Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity eth_mdio is Port(
	CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	E_RST_L : out STD_LOGIC;
	E_MDC : out STD_LOGIC;
	E_MDIO : inout STD_LOGIC;
	--speeds: 0 = Off, 1 = 100Mbit, 2 = 1Gbit, 3 = Reserved
	E_LINK_SPEED : out STD_LOGIC_VECTOR(1 downto 0));
end eth_mdio;

architecture Behavioral of eth_mdio is
	signal link_speed : STD_LOGIC_VECTOR(2 downto 0) := "000";
	signal state : unsigned(27 downto 0) := x"0000000";
	signal next_state : unsigned(27 downto 0) := x"0000000";
	--request consists of read op-code "0110", phy_addr "00111", and reg_addr "10001"
	constant request : STD_LOGIC_VECTOR(0 to 13) := "01100011110001";
begin
	E_LINK_SPEED <= "10" when link_speed(2) = '1' and link_speed(0) = '1'
		else "01" when link_speed(1) = '1' and link_speed(0) = '1' else "00";
	next_state <= x"0000000" when RESET = '1' else x"8000000" when
		state = x"8000FFF" else state + x"0000001";--link speed updates every 32 us

	process(CLK)
	begin
		if CLK'event and CLK = '1' then
			state <= next_state;
			if next_state(27) = '0' then--wait for 1 second
			   if next_state(26 downto 21) = "000000" then
					E_RST_L <= '0';--hold reset condition for 17 ms
				else
					E_RST_L <= '1';
				end if;
				E_MDC <= '1';
				E_MDIO <= 'Z';
			else
				E_RST_L <= '1';
				if next_state(11) = '1' or next_state(4) = '1' then
					E_MDC <= '1';
				else--maximum frequency of E_MDC is 8.3 MHz
					E_MDC <= '0';--I drive it at 3.9 MHz
				end if;
				if next_state(11 downto 5) < "0100000" then
					E_MDIO <= '1';--send preamble of 32 '1's
				elsif next_state(11 downto 5) < "0101110" then
					E_MDIO <= request(to_integer(next_state(8 downto 5)));
				elsif next_state(11 downto 0) = x"600" then--bit 15
					link_speed(2) <= E_MDIO;--after request asking for link speed,
				elsif next_state(11 downto 0) = x"620" then--bit 14
					link_speed(1) <= E_MDIO;--read in the data at the appropriate times
				elsif next_state(11 downto 0) = x"680" then--bit 11
					link_speed(0) <= E_MDIO;
				else
					E_MDIO <= 'Z';
				end if;
			end if;
		end if;
	end process;
end Behavioral;

