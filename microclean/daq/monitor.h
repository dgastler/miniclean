// @(#)root/test:$Name:  $:$Id: guiviewer.h,v 1.3 2002/11/12 10:27:06 brun Exp $
// Author: Brett Viren   04/15/2001




#include "Event.h"
#include "SPEFit.h"
#include "Integrate.h"
#include "WaveformProcessing.h"

#include "TGFrame.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TGClient.h"
#include "TGButton.h"
#include "TCanvas.h"
#include "Riostream.h"
#include "TGraph.h"
#include "TFile.h"
#include "TBranch.h"
#include "TTree.h"
#include "TRootEmbeddedCanvas.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TGLabel.h"



class Viewer : public TGMainFrame 
{
 private:
  
  //  TGMainFrame *fMainFrame;
  TGCompositeFrame *fCompositeFrame;
  
  TRootEmbeddedCanvas *fRootEmbeddedCanvas01;
  Int_t wfRootEmbeddedCanvas01;
  TCanvas *c01;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas02;
  Int_t wfRootEmbeddedCanvas02;
  TCanvas *c02;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas03;
  Int_t wfRootEmbeddedCanvas03;
  TCanvas *c03;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas04;
  Int_t wfRootEmbeddedCanvas04;
  TCanvas *c04;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas05;
  Int_t wfRootEmbeddedCanvas05;
  TCanvas *c05;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas06;
  Int_t wfRootEmbeddedCanvas06;
  TCanvas *c06;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas07;
  Int_t wfRootEmbeddedCanvas07;
  TCanvas *c07;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas08;
  Int_t wfRootEmbeddedCanvas08;
  TCanvas *c08;
  TRootEmbeddedCanvas *fRootEmbeddedCanvas09;
  Int_t wfRootEmbeddedCanvas09;
  TCanvas *c09;
  
  //Button space and text outputs
  TGHorizontalFrame *fHorizontalFrame01;
  TGLabel *fLabel01;
  TGLabel *fLabel02;
  TGLabel *fLabel03;

  //Timer
  TTimer  *timer;

  //GUI visual parameters
  Int_t w;                
  Int_t h;                
  Int_t border;           
  Int_t buffer;           
  Float_t PercentData;    
  Int_t WaveformWidth;    
  Int_t WaveformHeight;   
  Int_t WaveformHeightSpecial;
  Int_t WaveformX;        
  Int_t WaveformY0;       
  Int_t WaveformY1;       
  Int_t WaveformY2;       
  Int_t WaveformY3;       
  Int_t WaveformY4;
  Int_t HistWidth;        
  Int_t HistHeight;       
  Int_t HistX0;           
  Int_t HistX1;           
  Int_t HistY0;           
  Int_t HistY1;           
  Int_t HorzWidth;        
  Int_t HorzHeight;       
  Int_t HorzX;            
  Int_t HorzY;            
  Int_t TextWidth;        
  Int_t TextHeight;       
  Int_t TextX;            
  Int_t TextY0;           
  Int_t TextY1;           
  Int_t TextY2;

  //Text Strings;
  Char_t * EventText;
  Char_t * HistText;
  Char_t * TimeText;
  
  //Plots
  Int_t               HistNumber;
  Bool_t              TogglePlots;
  TH1F                *TB;
  TH1F                *PMT0;
  TH1F                *PMT1;
  TH1F                *Total;
  TH1F                *fPrompt;

  TH2F                *ChargeVSfPrompt;

  TH2F                *SPE_IvsH0;
  TH2F                *SPE_IvsH1;
  TH1D                *SPE0;
  TH1D                *SPE1;
  TF1                 *f0;
  TF1                 *f1;

  TGraph              *Waveform0;
  TGraph              *Waveform1;
  TGraph              *Waveform2;
  TGraph              *Waveform3;
  TGraph              *Waveform4;

  //Arrays
  Bool_t              ArraysSet;
  Int_t               Size;
  Float_t             *Chan0;
  Float_t             *Chan1;
  Float_t             *Chan2;
  Float_t             *Chan3;
  Float_t             *Chan4;
  Float_t             *ChanX;
  

  //File Variables
  TFile               *DataFile;
  TTree               *tree;
  Event               *event;
  Event               *TempEvent;
  Char_t              *FileName;
  Int_t               FileNumber;
  Int_t               ReadAttempts;


  //Other
  Float_t voltsLow;
  Float_t voltsHigh;
  Bool_t  EverythingIsOk;

  //Not Used and should remove
  Char_t              *options;

  
  
 public:
  Viewer(const TGWindow *p,Char_t * filename,Char_t *optionsi);
  virtual ~Viewer();
  void Reset();
  void Exit();
  void Plot();
  void Start();
  void Stop();
  void Toggle();
  void CloseWindow();
  Int_t FindPlotMaxBin(TH1F *h);
  Int_t FindPlotMaxBinX(TH2F *h);
  Int_t FindPlotMaxBinY(TH2F *h);
  Int_t FindPlotMaxBin(TH1D *h);
  Int_t FindPlotMaxBinX(TH2D *h);
  Int_t FindPlotMaxBinY(TH2D *h);
  ClassDef(Viewer,0) //GUI example
};
    
