#include <DCMessageClient.h>


bool DCMessageClient::Setup(xmlNode * SetupNode,
			    std::vector<MemoryManager*> &/*MemManager*/)
{  
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  //================================
  //Parse data for Client config and expected clients.
  //================================
  if(!DCDAQServer.SetupRemoteAddr(SetupNode))
    //Connection setup failed
    {
      return false;
    }


  Ready = true;
  return StartNewConnection();
}

//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DCMessageClient::StartNewConnection()
{
  if(DCDAQServer.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return true;
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //	  SendStop(false);
	  return false;
	}
    }
  else 
    {
      if(DCDAQServer.GetSocketFD() >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD(DCDAQServer.GetSocketFD());
	  AddWriteFD(DCDAQServer.GetSocketFD());
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  SendStop();
	  return false;
	}
    }
  return true;
}

void DCMessageClient::CheckForNewConnection()
{
  if(IsReadReady(DCDAQServer.GetSocketFD()) || 
     IsWriteReady(DCDAQServer.GetSocketFD()))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(DCDAQServer.GetSocketFD());
      RemoveWriteFD(DCDAQServer.GetSocketFD());      
      SetupSelect();

      //If the connect check fails
      if(!DCDAQServer.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %lds\n",
		  DCDAQServer.GetRemoteAddress().GetAddrStr().c_str(),
		  GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  DCDAQServer.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      SendStop();
	    }	  
	}
    }
}

void DCMessageClient::ProcessTimeout()
{
  //Catch a timeout if we don't have a DCNetThread
  if((!DCDAQServer.IsRunning()) &&
     (DCDAQServer.GetSocketFD() != DCDAQServer.badFD))
    {      
      DCDAQServer.Shutdown();
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(DCDAQServer.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }
}

bool DCMessageClient::ProcessNewConnection()
{  
  bool ret = true;
  RemoveReadFD(DCDAQServer.GetSocketFD());
  RemoveWriteFD(DCDAQServer.GetSocketFD());  
  SetupSelect();
  if(DCDAQServer.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(DCDAQServer.GetFullPacketFD());
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(DCDAQServer.GetMessageFD());      
      
      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::CONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);
      Print("Connected");
      SetupSelect();

      
    

      //================================================
      //Register this connection with DCMessageProcessor
      //================================================

      //Data structure that holds the routing information
      DCMessage::DCRoute routeData;
      DCMessage::DCAddress dcAddr = DCDAQServer.GetRemoteAddress();            
      //note: check here if routing is wrong, what is the DCDAQServer remote address's name?
      std::vector<uint8_t> data = routeData.SetData(dcAddr,
						    GetName());	
      //message of REGISTER_ADDR type to tell the DCMessageProcessor to route
      //messages with this address to this thread
      DCMessage::Message message;
      message.SetType(DCMessage::REGISTER_ADDR);
      message.SetDestination();
      message.SetData(&data[0],data.size());
      SendMessageOut(message);
    }
  else
    {
      ret = false;
    }
  return ret;
}

bool DCMessageClient::SendMessage(DCMessage::Message &message)
{
  //Get the data for this packet
  std::vector<uint8_t> stream;
  //get a vector of data for this message to send.
  uint streamSize = message.StreamMessage(stream);
  if(streamSize <= 0)
    return false;
  uint8_t * dataPtr  = &(stream[0]);	  
  int packetNumber = 0;
  //Send loop over the data until all of it is sent
  while(streamSize >0)
    {
      //Get a free packet index
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      if(DCDAQServer.GetOutPacketManager() == NULL)
	return false;      
      DCDAQServer.GetOutPacketManager()->GetFree(iDCPacket,true);
      if(iDCPacket == FreeFullQueue::BADVALUE)
	return false;
      //Get the DCNetPacket for that index
      DCNetPacket * packet = DCDAQServer.GetOutPacketManager()->GetPacket(iDCPacket);
      //Fail if our packet is bad
      if(packet == NULL)
	{
	  return false;
	}
      //Fill packet
      
      if(streamSize > packet->GetMaxDataSize())
	{
	  packet->SetType(DCNetPacket::DCMESSAGE_PARTIAL);
	  packet->SetData(dataPtr,packet->GetMaxDataSize(),packetNumber);
	  dataPtr+=packet->GetMaxDataSize();
	  streamSize-=packet->GetMaxDataSize();
	  packetNumber++;
	}
      else
	{
	  packet->SetType(DCNetPacket::DCMESSAGE);
	  packet->SetData(dataPtr,streamSize,packetNumber);
	  dataPtr+=streamSize;
	  streamSize-=streamSize;
	  packetNumber++;
	}
      //Send off packet
      DCDAQServer.GetOutPacketManager()->AddFull(iDCPacket);
      packet = NULL;
    }
  return true;
}

void DCMessageClient::DeleteConnection()
{
  //Remove the incoming packet manager fds from list
  RemoveReadFD(DCDAQServer.GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(DCDAQServer.GetMessageFD());
  
  //No longer wait for connections
  RemoveReadFD(DCDAQServer.GetSocketFD());
  RemoveWriteFD(DCDAQServer.GetSocketFD());

  SetupSelect();  
  DCDAQServer.Shutdown();
}


DCMessage::Message DCMessageClient::ReadRemoteMessage()
{
  DCMessage::Message blankMessage;
  std::vector<uint8_t> messageData;
  int packetCount = 1;
  //Loop over all the packets that make up this DCMessage
  for(int iPacket = 0; iPacket < packetCount; iPacket++)
    {
      int32_t iDCPacket = FreeFullQueue::BADVALUE;    
      //Try to get a full in packet index
      DCDAQServer.GetInPacketManager()->GetFull(iDCPacket,true);
      //Get the associated packet
      DCNetPacket * packet = DCDAQServer.GetInPacketManager()->GetPacket(iDCPacket);
      //Fail if the packet is bad
      if(packet == NULL)
	{
	  PrintWarning("Bad net packet.\n");
	  return blankMessage;
	}
      
      //Now we have a valid packet
      
      //Check it's type (if it is a partial packet increase packetCount;
      if(packet->GetType() == DCNetPacket::DCMESSAGE_PARTIAL)
	{
	  packetCount++;
	}
      else if(packet->GetType() != DCNetPacket::DCMESSAGE)
	{	  
	  PrintWarning("Bad incoming packet\n");
	  //return packet as free and return
	  DCDAQServer.GetInPacketManager()->AddFree(iDCPacket);
	  return blankMessage;
	}
      
      //Now get the data from the packet and append it to the message data vector
      unsigned int existingMessageDataSize = messageData.size();
      //Resize our vector to hold the new data
      messageData.resize(existingMessageDataSize + packet->GetSize());
      //Check that the packet has a valid data pointer
      if(packet->GetData() == NULL)
	{
	  PrintWarning("Bad packet data\n");
	  //return packet as free and return
	  DCDAQServer.GetInPacketManager()->AddFree(iDCPacket);
	  return blankMessage;
	}
      //Copy in the new data
      memcpy(&(messageData[existingMessageDataSize]),packet->GetData(),packet->GetSize());
      //return our packet
      DCDAQServer.GetInPacketManager()->AddFree(iDCPacket);
    }
  //create a new DCMessage using the data from the packets
  DCMessage::Message  message;
  message.SetMessage(&messageData[0],messageData.size());
  return message;
}
