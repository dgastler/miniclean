#ifdef __CAENLIBS__
#include <v1720FakeTrigReadout.h>

void v1720FakeTrigReadout::MainLoop()
{ 
  //Get a new block if we don't already have one
  if(IsReadReady(Mem->GetFreeFDs()[0]) &&
     (blockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Mem->GetFree(blockIndex);
      if(blockIndex != FreeFullQueue::BADVALUE)
	{
	  block = Mem->GetBlock(blockIndex);
	  block->dataSize = 0;
	  //	  usleep(1000);
	}
    }
  
  //Fill the current block with data
  if(blockIndex != FreeFullQueue::BADVALUE)
    {      
      //Readout loop
      uint32_t nWFDs = WFD.size();  //Won't change in this loop      
      //Loop over all the wfds a minimum of this many times
      //before passing off the current block.
      for(uint32_t iBlockReadout = 0; 
	  iBlockReadout < MaxReadoutsPerBlock;
	  iBlockReadout++)
	{
	  //Check if we have the minimum amount of free space available 
	  //in the current block. 
	  if((block->allocatedSize - block->dataSize) > MinimumFreeBufferSpace)
	    {
	      for(uint32_t iWFD = 0; iWFD < nWFDs;iWFD++)
		{
		  int32_t readoutReturn = WFD[iWFD]->Readout(tempBlock);
		  if((readoutReturn == OK)||(readoutReturn == BUFFERFULL))
		    {		      
		      TimeStep_Occupancy[iWFD] += WFD[iWFD]->GetOccupancy();
		      TimeStep_ReadoutNumber[iWFD]++;
		      
		      if((tempBlock.dataSize > 0) &&
			 (!SendPhysicsEvent()))
			{
			  iBlockReadout = MaxReadoutsPerBlock; 
			  iWFD = nWFDs;
			}
		      tempBlock.dataSize =0;
		    }
		  else
		    {
		      //Print error and stop DAQ
		      char * buffer = new char[100];
		      sprintf(buffer,
			      "WFD %d readout error %d",
			      iWFD,readoutReturn);			  
		      PrintWarning(buffer);
		      
		      //Exit loop
		      iBlockReadout = MaxReadoutsPerBlock; 
		      iWFD = nWFDs;
		      if((readoutReturn == NOTREADY) ||
			 (readoutReturn == CAEN_DGTZ_CommError))
			{
			  //This is a bad error.  Stop the DAQ
			  PrintError(buffer);
			  SendStop(false);
			  break;
			}
		      delete [] buffer;
		    }// readoutReturn
		}
	    }//Block size left check
	  else if(iBlockReadout == 0)
	    {
	      //This is a bad enough error to shut down the DAQ
	      PrintError("Not enough memory allocated!");
	      DCMessage::Message message;
	      
	      //Shut down the DAQ
	      message.SetType(DCMessage::STOP);
	      SendMessageOut(message);		  
	    }
	}
      //Pass off memory block
      if(block->dataSize != 0)
	Mem->AddFull(blockIndex);
    }
}

bool v1720FakeTrigReadout::SendPhysicsEvent()
{
  bool ret =true;
  
  //Calculate the size of the next WFDsPerEvent
  uint32_t physicsSize = 0;
  for(size_t i = 0 ; i < WFDsPerEvent;i++)
    {
      uint32_t index = (iWFDEvent + i)%FakeEventCount;
      uint32_t * event = FakeWFDEvent[index];//get event
      physicsSize += event[0]&0x0FFFFFFF;//get this event's size	      
    }

  if((block->allocatedSize - block->dataSize) > physicsSize)
    {
      for(uint32_t iWFD = 0; iWFD < WFDsPerEvent;iWFD++)
	{
	  //Get a pointer to the part of buffer we are going to write to.
	  uint32_t * bufferPointer = (uint32_t * )(block->buffer + 
						   block->dataSize);

	  uint32_t * event = FakeWFDEvent[iWFDEvent];//get event
	  uint32_t eventSize = event[0]&0x0FFFFFFF;//get this event's size

	  //Copy from event to bufferPointer eventsize*4 bytes.
	  memcpy(bufferPointer,event,eventSize<<2);
	  
	  //Calculate the real event number
	  //	  uint32_t RealEventID = (event[2]&0xFFFFFF) + EventCountOffset;
	  //Write lowest 8 bits of real event number to header
	  bufferPointer[1] = event[1] & 0xFFFF00FF;
	  bufferPointer[1] = ((tempBlock.buffer[1] & 0x0000FF00)) | bufferPointer[1];
	  //Set the WFD event number and increment counter
	  uint32_t WFDID = (event[1]&0xF8000000)>>27;
	  //bufferPointer[2] = 0xFFFFFF & WFDEventID[WFDID];
	  bufferPointer[2] = tempBlock.buffer[2];
	  bufferPointer[3] = tempBlock.buffer[3];
	  WFDEventID[WFDID] +=1;

	  bufferPointer[3] = tempBlock.buffer[3];

	  //Set block's data size to include this event
	  block->dataSize += eventSize;		  
	  //Move to the next event.
	  iWFDEvent++;
	  //If we are at the end of our loaded events go back to zero.
	  if(iWFDEvent >= FakeWFDEvent.size())
	    {		
	      EventCountOffset+=FakeEventCount;
	      iWFDEvent = 0;
	    }
	  TimeStep_EventCount[iWFD] ++;
	  TimeStep_ReadoutSize[iWFD] += eventSize;
	}
    }
  else
    {
      ret = false;
    }
  return ret;
}


void  v1720FakeTrigReadout::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  DCMessage::Message message;
  message.SetType(DCMessage::STATISTIC);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t TimeStep = currentTime-lastTime;
      StatMessage::StatRate ssRate;
      StatMessage::StatFloat ssFloat;
                  
      size_t nWFDs = WFD.size();
      for(size_t iWFD= 0; iWFD < nWFDs;iWFD++)
	{
	  //Generate text for enum lookup
	  char WFDID[] = "WFD_00";	  
	  if((WFD[iWFD]->GetBoardID() < 99) &&
	     (WFD[iWFD]->GetBoardID() > 0))
	    sprintf(WFDID,"WFD_%02u",(unsigned)WFD[iWFD]->GetBoardID());


	  //Send Event numbers
	  ssFloat.sBase.type = StatMessage::OCCUPANCY;	  
	  if(TimeStep_ReadoutNumber[iWFD] != 0)
	    ssFloat.val = double(TimeStep_Occupancy[iWFD])/double(TimeStep_ReadoutNumber[iWFD]);
	  else
	    ssFloat.val = 0;
	  ssFloat.sInfo.time = currentTime;
	  ssFloat.sInfo.interval = TimeStep;
	  ssFloat.sInfo.subID = SubID::GetType(WFDID);
	  ssFloat.sInfo.level = Level::BLANK;
	  message.SetData(ssFloat);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);

	  
	  //Send Bytes transfered.

	  //Save value in the first 32 bits
	  ssRate.sBase.type = StatMessage::DATA_RATE;
	  ssRate.count = TimeStep_ReadoutSize[iWFD];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::GetType(WFDID);
	  ssRate.sInfo.level = Level::BLANK;
	  ssRate.units = Units::BYTES;	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);


	  
	  //Send Readout numbers
	  ssRate.sBase.type = StatMessage::READOUT_RATE;
	  ssRate.count = TimeStep_ReadoutNumber[iWFD];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::GetType(WFDID);
	  ssRate.sInfo.level = Level::BLANK;
	  ssRate.units = Units::NUMBER;	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);
	  
	  
	  //Send Event numbers
	  ssRate.sBase.type = StatMessage::EVENT_RATE;
	  ssRate.count = TimeStep_EventCount[iWFD];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::GetType(WFDID);
	  ssRate.sInfo.level = Level::BLANK;
	  ssRate.units = Units::NUMBER;	  
	  message.SetData(ssRate);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message);

	  TimeStep_Occupancy[iWFD] = 0;
	  TimeStep_ReadoutSize[iWFD] = 0;
	  TimeStep_EventCount[iWFD] = 0;
	  TimeStep_ReadoutNumber[iWFD] = 0;		  
	}
      
               
      //Setup next time
      lastTime = currentTime;
    }
}

#endif
