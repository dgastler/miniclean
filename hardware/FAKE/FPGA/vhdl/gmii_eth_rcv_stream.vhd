--This module receives ethernet packets via the MII or GMII interface to the 
--PHY. It then outputs the ethernet packets in the following streaming format:
-- Bit 9: CKE: Clock enable sets data rate.  Lower bits are only valid if CKE.
-- Bit 8: FRM: Frame signal, asserted for entire ethernet frame
-- Bits 7-0: DAT: Frame data, ignored if not FRM.
--NOTES: The streaming ethernet frame includes all bytes from the Destination 
--MAC through the CRC bytes. This module is intended to move the received 
--packets from the RXCLK domain into a 125Mhz internal clock domain.  
--Because the 125Mhz internal clock can be slightly faster than the RXCLK, 
--it is possible for CKE to deassert for a cycle during the ETH_RX_STREAM
--frame.  This is not a problem if ETH_RX_STREAM is later stored in a buffer 
--and eventually re-timed.  However, if ETH_RX_STREAM was to drive 
--a RGMII_ETH_TX module directly, the cke sequence would likely not be correct
--(i.e. cke=1 for duration of frame (1000Mbps)).
--Original code by Xilinx.
--Translation into VHDL by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity gmii_eth_rcv_stream is Port(
	CLK125 : in STD_LOGIC;
	RESET : in STD_LOGIC;
	RXCLK : in  STD_LOGIC;--receive clock
	RXD : in  STD_LOGIC_VECTOR(7 downto 0);
	RXCTRL : in STD_LOGIC;--receiver ctrl signal
	SPEED : in STD_LOGIC_VECTOR(1 downto 0);
	ETH_RX_STREAM : out STD_LOGIC_VECTOR(9 downto 0));
end gmii_eth_rcv_stream;

architecture Behavioral of gmii_eth_rcv_stream is
	signal rx_en_inff : STD_LOGIC := '0';
	signal rxdat_inff : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	--"keep" rx_en_inff and rxdat_inff?
	signal ge_rxd_byte_en : STD_LOGIC := '0';--ge received byte strobe (skip preamble)
	signal ge_rxd_byte : STD_LOGIC_VECTOR(7 downto 0) := x"00";--ge received byte
	signal ge_frame_low : STD_LOGIC := '0';--after packet done, write extra byte with '0' frame
	signal fe_rxd_byte_en : STD_LOGIC := '0';--fe received byte strobe
	signal fe_rxd_byte : STD_LOGIC_VECTOR(7 downto 0) := x"00";--fe received byte
	signal fe_tgl : STD_LOGIC := '0';--used to choose nybble of fe_rxd_byte
	signal fe_frame_low : STD_LOGIC := '0';--after packet done, write extra byte with '0' frame
	signal wr_stb : STD_LOGIC;
	signal wr_rx_byte : STD_LOGIC_VECTOR(7 downto 0);
	signal wr_rx_frm : STD_LOGIC;
	signal out_cnt : unsigned(3 downto 0) := x"0";
	signal cke : STD_LOGIC := '0';
	signal fifo_empty : STD_LOGIC;
	signal fifo_rd_en : STD_LOGIC;
	signal fifo_dout : STD_LOGIC_VECTOR(8 downto 0);
	signal out_cke : STD_LOGIC := '0';
	signal out_frm : STD_LOGIC := '0';--set when sfd found
	signal out_dat : STD_LOGIC_VECTOR(7 downto 0) := x"00";--rxd formatted into a byte
	signal gigabit : STD_LOGIC;

	component bfifo port(
		RESET : in STD_LOGIC;
		WR_CLK : in STD_LOGIC;
		DIN : in STD_LOGIC_VECTOR(8 downto 0);
		WR_EN : in STD_LOGIC;
		RD_CLK : in STD_LOGIC;
		RD_EN : in STD_LOGIC;
		DOUT : out STD_LOGIC_VECTOR(8 downto 0);
		EMPTY : out STD_LOGIC;
		FULL : out STD_LOGIC);
	end component;
begin
	--this fifo is a first word fall through type
	clkchng_fifo : bfifo port map(
		RESET => RESET,
		WR_CLK => RXCLK,
		DIN(8) => wr_rx_frm,
		DIN(7 downto 0) => wr_rx_byte,
		WR_EN => wr_stb,
		RD_CLK => CLK125,
		RD_EN => fifo_rd_en,
		DOUT => fifo_dout,
		EMPTY => fifo_empty,
		FULL => open);

	--choose signals based on speed
	gigabit <= '1' when SPEED = "10" else '0';
	wr_stb <= ((ge_rxd_byte_en or ge_frame_low) and gigabit) or 
		(((fe_rxd_byte_en and fe_tgl) or fe_frame_low) and not gigabit);--strobe for writing to fifo
	wr_rx_byte <= ge_rxd_byte when gigabit = '1' else fe_rxd_byte;--received byte
	wr_rx_frm <= '0' when ge_frame_low = '1' or fe_frame_low = '1' else '1';
	--make our new ethernet format
	ETH_RX_STREAM <= out_cke & out_frm & out_dat;
	--use asynchronous fifo to go from RXCLK to 125MHz
	fifo_rd_en <= cke and not fifo_empty;

	process(RXCLK, RESET)
	begin
		if RESET = '1' then
			rx_en_inff <= '0';
			ge_rxd_byte_en <= '0';
			fe_tgl <= '0';
			fe_rxd_byte_en <= '0';
			rxdat_inff <= x"00";
			ge_rxd_byte <= x"00";
			fe_rxd_byte <= x"00";
		elsif RXCLK'event and RXCLK = '1' then
			--1000Mbps mode sigs
			rxdat_inff <= RXD;
			rx_en_inff <= RXCTRL;
			ge_rxd_byte <= rxdat_inff;
			if ge_rxd_byte = x"D5" and rx_en_inff = '1' then--preamble ends in x"D5"
				ge_rxd_byte_en <= '1';
			elsif rx_en_inff = '0' then
				ge_rxd_byte_en <= '0';
			else
				ge_rxd_byte_en <= ge_rxd_byte_en;
			end if;
			ge_frame_low <= ge_rxd_byte_en and not rx_en_inff;
			--100Mbps mode sigs
			if fe_rxd_byte = x"D5" and fe_rxd_byte_en = '0' then
				fe_tgl <= '0';
			else
				fe_tgl <= not fe_tgl;
			end if;
			--form two consecutive nybbles into a byte
			fe_rxd_byte <= rxdat_inff(3 downto 0) & fe_rxd_byte(7 downto 4);
			if fe_rxd_byte = x"D5" and rx_en_inff = '1' then--preamble ends in x"D5"
				fe_rxd_byte_en <= '1';
			elsif rx_en_inff = '0' then
				fe_rxd_byte_en <= '0';
			else
				fe_rxd_byte_en <= fe_rxd_byte_en;
			end if;
			fe_frame_low <= fe_rxd_byte_en and not rx_en_inff;
		end if;
	end process;

	process(CLK125)
	begin
		if CLK125'event and CLK125 = '1' then
			--generate tick for readout of fifo
			if out_cnt = x"9" or gigabit = '1' then
				out_cnt <= x"0";
			else
				out_cnt <= out_cnt + x"1";
			end if;
			if out_cnt = x"0" then
				cke <= '1';
			else
				cke <= '0';
			end if;
			if fifo_empty = '1' and out_frm = '1' then
				--during a frame, want to lower cke when fifo empty
				out_cke <= '0';
			else
				out_cke <= cke;
			end if;
			if fifo_rd_en = '1' then
				out_frm <= fifo_dout(8);
				out_dat <= fifo_dout(7 downto 0);
			end if;
		end if;
	end process;
end Behavioral;

