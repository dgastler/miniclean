#ifdef __RATDS__
#include <RATThrowAway.h>

void RATThrowAway::MainLoop()
{
  if(IsReadReady(RATBlock_mem->GetToDiskFDs()[0]))
    { 
      //Get the complete event's index
      int32_t index;
      if(RATBlock_mem->GetToDisk(index))
	//We got an index from the 
	{
	  if(index != RATBlock_mem->GetBadValue())
	    //A valid event
	    {	      
	      RATDSBlock * event = RATBlock_mem->GetRATDiskBlock(index);
	
	      //Now we need to clear the existing EBPCBLock
	      event->Clear();

	      timeStepProcessedEvents++;  //Incriment the time step event count

	      //add index to the free queue
	      if(!RATBlock_mem->AddFree(index))
		{
		  PrintError("Lost a EVBlock\n");
		}
	      	      	      
	      //Delete the Branch DS object.
	      //	      delete BranchDS;
	    }
	  else
	    {
	      //We are probably shutting down now. 
	      PrintError("Bad event index\n");
	      SendStop();
	    }
	}
      else
	//We couldn't get a new ToDisk
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a ToDisk event.  DCDAQ shutdown?\n");
	  SendStop();
	}
    }
}


bool RATThrowAway::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(RATBlock_mem->GetToDiskFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the to disk queue with select
      AddReadFD(RATBlock_mem->GetToDiskFDs()[0]);      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}


void RATThrowAway::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t timeStep = difftime(currentTime,lastTime);      
      DCMessage::Message message;
      StatMessage::StatRate ssRate;

      ssRate.sBase.type = StatMessage::EVENT_RATE;
      ssRate.count = timeStepProcessedEvents;
      ssRate.sInfo.time = currentTime;
      ssRate.sInfo.interval = timeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.units = Units::NUMBER;   
      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);
      
      timeStepProcessedEvents = 0;

      //Setup next time
      lastTime = currentTime;
    }
}

#endif
