#ifdef __RATDS__
#ifndef __DRPCRATMANAGER__
#define __DRPCRATMANAGER__

#include <math.h>
#include <MemoryManager.h>
#include <xmlHelper.h>

#include <DCVector.h>
#include <DCQueue.h>
#include <BadEventStruct.h>
#include <StatMessage.h>
#include <DRDecisionPacket.h>
#include <VENATOREvent.h>
#include <DRDataPacket.h>

#include <RAT/DS/DCDAQ_RATEVBlock.hh>

#include <TFile.h>
#include <TH1F.h>

#include <iostream>
#include <sstream>
#include <string>

//#define USE_EVENT_TIME_FOR_KEY 
class DRPCRATManager : public MemoryManager

{
 public:
  DRPCRATManager(std::string Name);
  ~DRPCRATManager();
  bool AllocateMemory(xmlNode * setupNode);

#ifndef USE_EVENT_TIME_FOR_KEY
  int64_t GetKey(sEvent & event)
  {
    return event.eventID;
  }
  int64_t GetKey(RATEVBlock * block)
  {
    return block->wfdEventID;
  };
  int64_t GetKey(DRDecisionPacket::Event & decision)
  {
    return decision.wfdEventID;
  }
  int64_t GetKey(DRPacket::Event * event)
  {
    return event->ID;
  }
#else
  int64_t GetKey(sEvent & event)
  {
    return event.timeStamp;
  }
  int64_t GetKey(DRPacket::Event * event)
  {
    return event->EventTime;
  }
  int64_t GetKey(RATEVBlock * block)
  {
    return block->eventTime;
  };
  int64_t GetKey(DRDecisionPacket::Event & decision)
  {
    return decision.eventTime;
  }

#endif

 
  //Block access
  RATEVBlock * GetDRPCBlock(uint32_t iDRPC) 
    {
      if(iDRPC < Memory.size())
	{
	  return(&(Memory[iDRPC]));
	}
    return(NULL);
    };  
  //FD access
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetUnProcessedFDs(){return(UnProcessed.GetFDs());};
  const int * GetRespondFDs(){return(Respond.GetFDs());};
  const int * GetBadEventFDs(){return(BadEvent.GetFDs());};
  const int * GetEBPCFDs(){return(EBPC.GetFDs());};

  //Shutdown
  virtual void Shutdown()
  {
    //Child objects
    Free.ShutdownWait();
    UnProcessed.ShutdownWait();
    EventStore.ShutdownWait();
    Respond.ShutdownWait();
    BadEvent.ShutdownWait();

    EBPC.ShutdownWait();

    ShutdownStatus = true;

    WakeUp();
  };

  virtual bool IsShutdown(){return(ShutdownStatus);};

  //Get the defined bad value
  int32_t GetBadValue(){return(EventStore.GetBadValue());};

  //Un-sent queue
  bool AddFree(int32_t &index);

  //Un-sent queue
  bool GetUnProcessed(int32_t &index);

  //To-sent queue
  bool GetRespond(int32_t &index);
  bool AddRespond(int32_t &index);

  //EBPC queue
  bool GetEBPC(int32_t &index);
  bool AddEBPC(int32_t &index);

  //BadEvent queue
  bool GetBadEvent(BadEvent::sBadEvent &badEvent);
  bool AddBadEventNOFD(int64_t eventKey, int32_t &index, Level::Type errorCode);
  bool AddBadEvent(int64_t eventKey, int32_t &index, Level::Type errorCode);

  //Vector hash access
  int64_t GetEvent(int64_t eventKey,int32_t &index,uint64_t &password);
  int64_t SetEvent(int64_t eventKey,int32_t &index,uint64_t password);
  int64_t ClearEvent(int64_t eventKey, uint64_t password);
     
  //Autopassing functions
  int64_t MoveEventToUnProcessedQueue(int64_t eventKey,uint64_t password);
  int64_t MoveEventToRespondQueue(int64_t eventKey,int32_t &index,uint64_t password);
  int64_t MoveEventToBadQueue(int64_t eventKey,uint64_t password, Level::Type errorCode);
 
  virtual void PrintStatus(int indent = 0, int verbosity = 0);

  virtual void WakeUp()
  {
    //child objects
    Free.WakeUp();
    UnProcessed.WakeUp();
    EventStore.WakeUp();
    Respond.WakeUp();
    BadEvent.WakeUp();
    EBPC.WakeUp();
  }

  void PrintVectorHash();

  uint64_t GetBlankPassword(){return EventStore.GetBlankPassword();};

 private: 
  uint32_t NumBlocks;
  uint32_t VectorHashSize;
  std::vector<RATEVBlock> Memory;

  //Shutdown status
  volatile bool ShutdownStatus;

  //================================================================
  //Statistics
  //================================================================
  std::map<enum TimeStampCode::Code,TH1F*> hTimeStamp;
  void AddTimeStampHistogram(enum TimeStampCode::Code code);
  void PrintTimeStampHistograms(std::string filename);
  
  //Index management 
  DCQueue<int32_t,PipeSelect,LockedObject> Free;  //queue of indicies of free memory blocks
  DCQueue<int32_t,PipeSelect,LockedObject> UnProcessed;  //queue of indicies of un-processed memory blocks
 
  #ifndef USE_EVENT_TIME_FOR_KEY
  DCVector<int32_t,LockedObject,EventNumberHash<int32_t> > EventStore; //vector-hash of stored events.
  #else
  DCVector<int32_t,LockedObject,EventTimeGroup<int32_t> > EventStore; //vector-hash of stored events.
  #endif
  DCQueue<int32_t,PipeSelect,LockedObject> Respond; //queue of indicies to send back with the decision.
  DCQueue<BadEvent::sBadEvent,PipeSelect,LockedObject> BadEvent; //queue of indices that have collided. 
  
  DCQueue<int32_t,PipeSelect,LockedObject> EBPC; //queue of indices to directly send to the local EBPC threads

  void Deallocate();
};

#endif
#endif

