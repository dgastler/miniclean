
//pport parts of the code come from this source with this license.
/*
 * snowmakingcontrolppdev v0.1 9/25/01
 * www.embeddedlinuxinterfacing.com
 *
 * The original location of this code is
 * http://www.embeddedlinuxinterfacing.com/chapters/07/
 * snowmakingcontrolppdev.c
 *
 * Copyright (C) 2001 by Craig Hollabaugh
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* snowmakingcontrolppdev
 * snowmakingcontrolppdev uses /dev/parport0 to control an interface
 * circuit connected the PC parallel printer port. The port's
 * control port drives the interface circuit's output latch signals.
 */

/*
 * For more information, see The Linux 2.4 Parallel Port Subsystem, T. Waugh
 * http://people.redhat.com/twaugh/parport/html/parportguide.html
 */





#include <SpeedTest.h>

//For parallel port access.
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/parport.h>
#include <linux/ppdev.h>
#include <fcntl.h>

SpeedTest::SpeedTest(std::string Name)
{
  Fd=-1;
  Mode=0;
  DCDAQ_time=0;
  iRun=-1;    
  DCDAQ_time_done = false;
  SetName(Name);
  SetType(std::string("SpeedTest"));
 
  SetUpdateTime(1);
  currentTime = time(NULL);
  lastTime=time(NULL)-10;
 
  SetSelectTimeout(1,0);
 
  Loop = true;
};

SpeedTest::~SpeedTest()
{
  if(Fd != -1)
    {
      /*release the port */
      ioctl (Fd, PPRELEASE);
      /*close the device file */
      close(Fd);
    }
}

bool SpeedTest::Setup(xmlNode * SetupNode,
		      std::map<std::string,MemoryManager*> &MemManager)
{
  std::string filename;
  if(FindSubNode(SetupNode,"FILENAME") == NULL)
    {
      PrintError("Can not find XML tag FILENAME");
      return(false);
    }
  GetXMLValue(SetupNode,"FILENAME","THREAD",filename);
  
  if(LoadFile(filename,Settings) == false)
    {
      char * buffer = new char[100];
      sprintf(buffer,"Bad XML settings file %s.",filename.c_str());
      PrintError(buffer);
      delete [] buffer;
      return(false);
    }
  Ready = true;
  //Open XML document
  XMLDoc = xmlReadMemory(Settings.c_str(),
			 Settings.size(),
			 NULL,
			 NULL,
			 0);
  if(XMLDoc)
    {
      BaseNode = xmlDocGetRootElement(XMLDoc);
    }
  else
    {
      fprintf(stderr,"Error opening XMLDOC.\n");
      Ready = false;
    }
  

  //Get device
  if(!BaseNode || GetXMLValue(BaseNode,"DEVICE","SpeedTest",Dev))
    {
      fprintf(stderr,"Can not find DEVICE XMLnode.\n");
      Ready = false;
    }

  //Get DCDAQ_time
  if(!BaseNode || GetXMLValue(BaseNode,"STARTUP_TIME","SpeedTest",DCDAQ_time))
    {
      fprintf(stderr,"Can not find STARTUP_TIME XMLnode.\n");
      Ready = false;
    }
    
  //Get output pattern (number of pulses)
  if(!BaseNode || (NumberOfNamedSubNodes(BaseNode,"SETTING") < 1) )
    {
      fprintf(stderr,"Can not find NPULSE XMLnode.\n");
      Ready = false;
    }
  else
    {
      uint32_t numberOfSettings = NumberOfNamedSubNodes(BaseNode, "SETTING");
      for(uint32_t i = 0; i < numberOfSettings; i++)
	{
	  SettingNode = FindIthSubNode(BaseNode,"SETTING",i);
	  uint32_t Pulse_count_temp = 0;
	  uint32_t Freq_temp = 0;
	  uint32_t Wait_time_temp = 0;
	  Setting set_temp;
	  GetXMLValue(SettingNode,"WAIT_TIME","SpeedTest",Wait_time_temp);
	  GetXMLValue(SettingNode,"FREQ","SpeedTest",Freq_temp);
	  GetXMLValue(SettingNode,"NPULSE","SpeedTest",Pulse_count_temp);
	  set_temp.Wait_time = Wait_time_temp;
	  set_temp.Pulse_count = Pulse_count_temp;
	  set_temp.Frequency = Freq_temp;
	  SettingVector.push_back(set_temp);
	}
    }

  if(Ready)
    {
      Ready = Setup_port();
    }
  return(Ready);  
}

unsigned char SpeedTest::PPControl(unsigned char TempCount)
{
  unsigned char TempControl;
  
  TempControl = 0xFF;
  TempControl = TempControl & TempCount;
  TempControl = TempControl >> 1;
  TempControl = TempControl & 0x0F;
  
  return(TempControl);
}

unsigned char SpeedTest::PPData(unsigned char TempFreq, unsigned char TempCount)
{   
  unsigned char Data0Bit, TempData;
  //printf("0x%02X 0x%02X\n",TempCount,TempFreq);  


  TempData = 0x7F;
  //printf("0x%02X\n",TempData);
  TempData = TempData & TempFreq;
  //printf("0x%02X\n",TempData);
  TempData = TempData << 1;
  //printf("0x%02X\n",TempData);
  TempData = TempData & 0xFE;
  //printf("0x%02X\n",TempData);

  Data0Bit = TempCount & 0x01;
  //printf("0x%02X\n",TempData);
  TempData = TempData | Data0Bit;
  //printf("0x%02X\n",TempData);

  return(TempData);
}

bool SpeedTest::Setup_port()
{
  DCMessage::Message message;
  DCMessage::SingleINT MessData;
  /*get the file descriptor for the parallel port */
  Fd = open(Dev.c_str(),O_RDWR);
  if (Fd == BadPipeFD)
  {
    perror("open");


    //Send STOP message to DAQ
    MessData.i = Fd;
    message.SetData(&MessData,sizeof(MessData));
    message.SetType(DCMessage::STOP);
    SendMessageOut(message,true);

    return(false);
  }

  /*set to exclusive control*/
  ioctl(Fd,PPEXCL);

/*request access to the port */
  if (ioctl(Fd,PPCLAIM))
  {
    perror("PPCLAIM");
    close(Fd);
    
    //Send STOP message to DAQ
    MessData.i = PPCLAIM;
    message.SetData(&MessData,sizeof(MessData));
    message.SetType(DCMessage::STOP);
    SendMessageOut(message,true);

    return(false);
  }

  /*configure the port for SPP mode */
  Mode = IEEE1284_MODE_COMPAT;
  if (ioctl(Fd, PPNEGOT, &Mode))
  {
    perror ("PPNEGOT");
    close (Fd);

    //Send STOP message to DAQ
    MessData.i = PPNEGOT;
    message.SetData(&MessData,sizeof(MessData));
    message.SetType(DCMessage::STOP);
    SendMessageOut(message,true);

    return(false);
  }

  return(true);
}

void SpeedTest::MainLoop()
{  
  //This function should never be called.   
  //This is not the way we should be using DCDAQ, but it's
  //useful for this occasion. 

  //Everything is done in ProcessTimeout()
}


bool SpeedTest::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}



void  SpeedTest::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //if we would be normally in running in the MainLoop sense.
  if(Loop)
    {
      if(DCDAQ_time_done)
	{
	  if((currentTime - lastTime) > GetUpdateTime())
	    {
	      //Move to next pulse count value;
	      iRun++;
	      if(iRun >= (int32_t)SettingVector.size())
		{
		  Loop = false;
		  DCMessage::Message message;
		  message.SetType(DCMessage::STOP);
		  SendMessageOut(message,true);
		}   

	      unsigned char TempFreq = SettingVector[iRun].Frequency;
	      unsigned char TempCount = SettingVector[iRun].Pulse_count;
	      
	      unsigned char DATACHAR = PPData(TempFreq, TempCount);
	      unsigned char CONTROLCHAR = PPControl(TempCount);
	      
	      //Write Pulse_count[iPulse_count] to the pport	  
	      int errorData = ioctl(Fd,
				    PPWDATA,
				    &DATACHAR
				    );
	      
	      int errorControl = ioctl(Fd,
				       PPWCONTROL,
				       &CONTROLCHAR
				       );
	      
	      //sleep at the current value of seconds
	      SetSelectTimeout(SettingVector[iRun].Wait_time,0);	      
	      
	      DCMessage::Message message;
	      DCMessage::PulseFreq PulserData;

	      PulserData.n = SettingVector[iRun].Pulse_count;
	      PulserData.f = SettingVector[iRun].Frequency;
	      message.SetData(&PulserData,sizeof(PulserData));
	      message.SetType(DCMessage::PULSEHITS);
	      SendMessageOut(message,true);
	      
	      //Process errors
	      if(errorData | errorControl)
		{
		  message.SetType(DCMessage::STOP);
		  SendMessageOut(message,true);
		}
	      
   
	      //Setup next time
	      lastTime = currentTime;
	    }
	  else
	    {
	      //We need to fix the wait time because we exited
	      SetSelectTimeout(SettingVector[iRun].Wait_time - 
			       (currentTime - lastTime),0);
	    }
	}
      else
	{
	  sleep(DCDAQ_time); 	  
	  DCMessage::Message message;
	  message.SetType(DCMessage::GO);
	  SendMessageOut(message,true);	  
	  DCDAQ_time_done = true;
	}       
    }
}

