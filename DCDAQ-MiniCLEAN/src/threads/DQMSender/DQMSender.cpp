#include <DQMSender.h>
#include <TH1.h>

DQMSender::DQMSender(std::string Name)  
{
  SetName(Name);
  SetType(std::string("DQMSender"));
 
}

bool DQMSender::Setup(xmlNode * SetupNode,
		      std::vector<MemoryManager*> &MemManager)
{
  
  //Find the memoryManager we need
 
  
    
  if(!FindSubNode(SetupNode,"SERVERADDRREQ"))
    {
      return false;
    }
  string serverAddrReq;
  GetXMLValue(SetupNode,"SERVERADDRREQ",GetName().c_str(),serverAddrReq);
 
  if(!FindSubNode(SetupNode,"SERVERADDRPULL"))
    {
      return false;
    }
  string serverAddrPull;
  GetXMLValue(SetupNode,"SERVERADDRPULL",GetName().c_str(),serverAddrPull);
 
  context=new zmq::context_t(1);
  daqServerReq=new zmq::socket_t(*context,ZMQ_REQ);
  zmqfdReq=-1;
  size_t zmqfdReq_size=sizeof(zmqfdReq);
  daqServerReq->getsockopt(ZMQ_FD,&zmqfdReq,&zmqfdReq_size);
  char * endpoint=new char[100];
  sprintf(endpoint,"tcp://%s",serverAddrReq.c_str());
  daqServerReq->connect(endpoint);
  delete endpoint;

  daqServerPull=new zmq::socket_t(*context,ZMQ_PULL);
  zmqfdPull=-1;
  size_t zmqfdPull_size=sizeof(zmqfdPull);
  daqServerPull->getsockopt(ZMQ_FD,&zmqfdPull,&zmqfdPull_size);
  endpoint=new char[100];
  sprintf(endpoint,"tcp://%s",serverAddrPull.c_str());
  daqServerPull->connect(endpoint);
  delete endpoint;

  
  //Get Start port for zmq socket
  if(FindSubNode(SetupNode,"ZMQPORT")==NULL)
    portStart=20000;

  else
    {
      string port;
      GetXMLValue(SetupNode,"ZMQPORT",GetName().c_str(),port);
      portStart=atoi(port.c_str());
  
    }
  
  char * mssg=new char[100];
  sprintf(mssg,"Setting up zmqport for CLEANViewer Communication: %i \n",portStart);
  Print(mssg);
  delete [] mssg;
  
  for(int i=0;i<50;i++)
    {
      availPorts.push_back(portStart+1+i);

    }
  //  portCurrentInc=portStart;

  daqSocketDistro=new zmq::socket_t(*context,ZMQ_REP);
  zmqfd=-1;
  size_t zmqfd_size=sizeof(zmqfd);
  daqSocketDistro->getsockopt(ZMQ_FD,&zmqfd,&zmqfd_size);
 
  endpoint=new char[100];
  sprintf(endpoint,"tcp://*:%i",portStart);
  
  daqSocketDistro->bind(endpoint);
 
  delete endpoint;
  Ready = true;
 
  return(true);  
}

bool DQMSender::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
     
      RemoveReadFD(zmqfdPull);
      RemoveReadFD(zmqfd);
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;

      RemoveReadFD(zmqfdPull);
      RemoveReadFD(zmqfd);
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      AddReadFD(zmqfdPull);
      AddReadFD(zmqfd);
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DQMSender::MainLoop()
{ 
  
  if(IsReadReady(zmqfdPull))
    {
     
      int zmq_events;
      size_t zmq_events_size=sizeof(zmq_events);
     
      daqServerPull->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
     
      while(zmq_events & ZMQ_POLLIN)
	{
	 
	  //We've really got something!
	  zmq::message_t message;
	  daqServerPull->recv(&message);
	  
	  
	  
	  ZMQ::MessageHead MssgHead;
	  memcpy(&MssgHead,message.data(),sizeof(ZMQ::MessageHead));

	  //Copy Event and send to correct port
	  if(MssgHead.mssg==ZMQ::MSGEVENT)
	    {

	      zmq::message_t messageSend(message.size());
	      memcpy(messageSend.data(),message.data(),message.size());
	 
	      int port=MssgHead.port;
	      int errCntr=0;
	      bool clientRemove=false;

	      if(writezmqMap.find(port)==writezmqMap.end())
		{
		  clientRemove=true;
		}
	      else
		{
		 
		  while(!(writezmqMap[port]->send(messageSend,ZMQ_NOBLOCK)))
		    {
		      if((errno=EAGAIN)&&(errCntr<5))
			{
			  errCntr+=1;
			  //wait 10 microseconds
			  usleep(10);
			}
		      else
			{ 
			  //Timeout,close down this socket
			  clientRemove=true;
			  break;
			}
		    }
		}

	      //remove closed sockets

	      if(clientRemove)
		{
		 
		  ZMQ::Request * zmqReq=new ZMQ::Request();
		  zmqReq->req=ZMQ::REQREMOVESOCK;
		  zmqReq->port=port;
		  zmq::message_t messg(sizeof(ZMQ::Request));

		  memcpy(messg.data(),zmqReq,sizeof(ZMQ::Request));


		  daqServerReq->send(messg);

		  zmq::message_t response;


		  daqServerReq->recv(&response);
		  availPorts.insert(availPorts.begin(),port);
		  writezmqMap[port]->close();
		  delete writezmqMap[port];
		  writezmqMap.erase(port);
		 
		}
	    }
	  //Shutdown in Message is to stop
	  if(MssgHead.mssg==ZMQ::MSGSTOP)
	    {
	     
	      SendStop();
	     
	    }
	  daqServerPull->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
	}
    }
 
  if(IsReadReady(zmqfd))
    {
     
      //check to see if we can really read anything
     
      int zmq_events;
      size_t zmq_events_size=sizeof(zmq_events);
     
      daqSocketDistro->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
     
      while(zmq_events & ZMQ_POLLIN)
	{
	  

	  //We've really got something!
	  zmq::message_t message;
	  daqSocketDistro->recv(&message);
	  
	  
	  
	  ZMQ::Request * zmq_req=new ZMQ::Request();
	 
	  memcpy(zmq_req,message.data(),sizeof(ZMQ::Request));
	  
	 
	 

	  if(zmq_req->GetHead().magic != 0xAB)
	    {
	      printf("magic wrong \n");
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      ZMQ::MessageHead MssgRepHead;//=ZMQ::MSGBADMAGIC;
	      MssgRepHead.mssg=ZMQ::MSGBADMAGIC;
	      memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
	      daqSocketDistro->send(reply,ZMQ_NOBLOCK);
	      //some sort of error
	      return;
	    }

	 
	  if(zmq_req->GetHead().version != ZMQVERSION)
	    {
	      //need to tell cleanviewer has wrong version,to update and recompile
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      ZMQ::MessageHead MssgRepHead;//=ZMQ::MSGWRNGVERSION;
	      memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
	      daqSocketDistro->send(reply,ZMQ_NOBLOCK);
	      printf("version wrong %i, %i\n",zmq_req->GetHead().version,ZMQVERSION);
	      return;

	    }
	 

	  //otherwise, check to see what kind of request
	  if(zmq_req->req==ZMQ::REQPORT)
	    {
	 
	      //client requesting port to connect to
	      //Get next available port
	      if(availPorts.size()==0) return;
	      
	      portCurrentInc=availPorts[0];
	      availPorts.erase(availPorts.begin());
	      //	      portCurrentInc +=1;
	 
	      zmqInfo * socketInfo=new zmqInfo();
	      zmq::socket_t * sock=new zmq::socket_t(*context,ZMQ_PUSH);
	      socketInfo->filter=new StreamFilter();
	     
	      char * endpoint=new char[100];
	      sprintf(endpoint,"tcp://*:%i",portCurrentInc);
	      //char * clientendpoint=new char[100];
	      //sprintf(clientendpoint,"tcp://192.168.0.1:%i",portCurrentInc);
	      //Send new Port request to Server
	      ZMQ::Request * zmq_reqServer=new ZMQ::Request();
	      zmq_reqServer->req=ZMQ::REQPORT;
	      zmq_reqServer->port=portCurrentInc;

	      zmq::message_t messageForward(sizeof(ZMQ::Request));
	      memcpy(messageForward.data(),zmq_reqServer,sizeof(ZMQ::Request));
	      daqServerReq->send(messageForward);
	     
	      zmq::message_t response;
	      daqServerReq->recv(&response);
	      delete zmq_reqServer;
	      //check to see that good message                                                                                                                                                 
	      ZMQ::MessageHead MssgHead;
	      memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
	      uint8_t msg=MssgHead.mssg;
	      if(msg!=ZMQ::MSGRECVDGOOD)
		{
		  if(msg==ZMQ::MSGWRNGVERSION)
		    {
		      PrintError("ZMQ version difference between here and Server\n");
		      return;
		    }
		  else if(msg==ZMQ::MSGBADMAGIC)
		    {
		      PrintError("BAD Magic word sent to Server \n");
		      return;
		    }
		  else
		    {

		      PrintError("ZMQ Messaging error \n");
		      return;
		    }
		}

	      //send the endpoint to CLEANViewer client
	      zmq::message_t reply(sizeof(ZMQ::MessageHead)+sizeof(int32_t));
	      ZMQ::MessageHead MssgRepHead;//=ZMQ::MSGRECVDGOOD;
	      MssgRepHead.mssg=ZMQ::MSGRECVDGOOD;
	      MssgRepHead.port=portCurrentInc;
	      memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
	      //memcpy((uint8_t*)reply.data()+sizeof(ZMQ::MessageHead),(int32_t *)&portCurrentInc,sizeof(int32_t));
	      daqSocketDistro->send(reply,ZMQ_NOBLOCK);
	      sock->bind(endpoint);
	      writezmqMap[portCurrentInc]=sock;

	      

	    }
	  else if(zmq_req->req==ZMQ::REQFILTER)
	    {
	      
	      if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgRepHead;
		  MssgRepHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
		  daqSocketDistro->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	      StreamFilter * filter=new StreamFilter();
	     
	      memcpy(filter,(uint8_t*)message.data()+sizeof(ZMQ::Request),sizeof(StreamFilter));
	      //Check to see if correct veriosn
	      if(filter->GetVersion() != FILTERVERSION)
		{	 
		 
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgRepHead;
		  MssgRepHead.mssg=ZMQ::MSGWRNGVERSION;
		  memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
		  daqSocketDistro->send(reply,ZMQ_NOBLOCK);
		  delete filter;
		  filter = NULL;
		}
	      else
		{
		  //forward request to server
		  
		  zmq::message_t messageForward(message.size());
		  memcpy(messageForward.data(),message.data(),message.size());
		  daqServerReq->send(messageForward);
		  
		  zmq::message_t response;
		  daqServerReq->recv(&response);
		 
		  //check to see that good message                                                                                                                                                  
		  ZMQ::MessageHead MssgHead;
		  memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
		  uint8_t msg=MssgHead.mssg;
		  if(msg!=ZMQ::MSGRECVDGOOD)
		    {
		      if(msg==ZMQ::MSGWRNGVERSION)
			{
			  PrintError("ZMQ version difference between here and Server\n");
			  return;
			}
		      else if(msg==ZMQ::MSGBADMAGIC)
			{
			  PrintError("BAD Magic word sent to Server \n");
			  return;
			}
		      else
			{

			  PrintError("ZMQ Messaging error \n");
			  return;
			}
		    }
	      
		  //respond to client
	
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgRepHead;
		  MssgRepHead.mssg=ZMQ::MSGRECVDGOOD;
		  memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
		  daqSocketDistro->send(reply,ZMQ_NOBLOCK);
		  
		}

	    }
	 
	  else
	    {
	      //all other messages forwarded straight through to DQMServer

	      
	      if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgRepHead;//  msg=ZMQ::MSGBADPORT;
		  MssgRepHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
		  daqSocketDistro->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	 
	      //forward message on to Server

	      zmq::message_t messageForward(message.size());
	      memcpy(messageForward.data(),message.data(),message.size());
	      daqServerReq->send(messageForward);
	     
	      zmq::message_t response;
	      daqServerReq->recv(&response);
	     
	      //check to see that good message                                                                                                                                                 
	      ZMQ::MessageHead MssgHead;
	      memcpy(&MssgHead,response.data(),sizeof(ZMQ::MessageHead));
	      uint8_t msg=MssgHead.mssg;
	      if(msg!=ZMQ::MSGRECVDGOOD)
		{
		  if(msg==ZMQ::MSGWRNGVERSION)
		    {
		      PrintError("ZMQ version difference between here and Server\n");
		      return;
		    }
		  else if(msg==ZMQ::MSGBADMAGIC)
		    {
		      PrintError("BAD Magic word sent to Server \n");
		      return;
		    }
		  else
		    {
 
		      PrintError("ZMQ Messaging error \n");
		      return;
		    }
		}
	      //respond to client
	
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      ZMQ::MessageHead MssgRepHead;
	      MssgRepHead.mssg=ZMQ::MSGRECVDGOOD;
	      memcpy(reply.data(),&MssgRepHead,sizeof(ZMQ::MessageHead));
	      daqSocketDistro->send(reply,ZMQ_NOBLOCK);
	
	    }
	  delete zmq_req;
	  daqSocketDistro->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
	 
	}
       
    }
}        
    
 
 
    


  void DQMSender::ProcessTimeout()
  {

  }
