#ifndef __NOSELECT__
#define __NOSELECT__

#include <SelectValues.h>

class NoSelect
{
 public:
  NoSelect(){};
  virtual ~NoSelect(){};

  uint8_t Read(){return SelectValues::OK;};  
  uint8_t Write(){return SelectValues::OK;};

//  const int * GetFDs() 
//  {
//    //This doesn't make sense for this select model so fail
//    fprintf(stderr,"GetFDs called on the NoSelect model\n");
//    abort();
//    //Return a constant NULL pointer
//    return(NULL);
//  }; 
};

#endif
