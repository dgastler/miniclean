#ifndef ROOT_PEvent
#define ROOT_PEvent
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Event                                                                //
//                                                                      //
// Description of the event and track parameters                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>
#include <vector>



class PEvent : public TObject {
  
 private:
  
  Bool_t         fIsValid;  

  Bool_t         AbvTHRS;

  Int_t          iEventNumber;

  Bool_t         bSubEvent;
  
  Int_t          iSubType;

  //Holds primary event
  std::vector<Float_t> vFast0;
  std::vector<Float_t> vFast1;
  std::vector<Float_t> vFast2;
  std::vector<Float_t> vFast3;
  std::vector<Float_t> vFast4;
  std::vector<Float_t> vSlow0;
  std::vector<Float_t> vSlow1;
  std::vector<Float_t> vSlow2;
  std::vector<Float_t> vSlow3;
  std::vector<Float_t> vSlow4;

  std::vector<Float_t> vTOF; 
  std::vector<Float_t> vShift;
  std::vector<Float_t> vDelay;
  std::vector<Float_t> vMaxHeight;
  std::vector<Float_t> vOffset;
  std::vector<Float_t> vOffsetSigma;

  //Holds sub-event if it exists.  Should be zero if otherwise. 
  std::vector<Float_t> vSubFast0;
  std::vector<Float_t> vSubFast1;
  std::vector<Float_t> vSubFast2;
  std::vector<Float_t> vSubFast3;
  std::vector<Float_t> vSubFast4;
  std::vector<Float_t> vSubSlow0;
  std::vector<Float_t> vSubSlow1;
  std::vector<Float_t> vSubSlow2;
  std::vector<Float_t> vSubSlow3;
  std::vector<Float_t> vSubSlow4;

  std::vector<Float_t> vSubTOF; 
  std::vector<Float_t> vSubShift;
  std::vector<Float_t> vSubDelay;
  std::vector<Float_t> vSubMaxHeight;



 public:
  PEvent();
  virtual        ~PEvent();

  void           SetSize(UInt_t i);
  Float_t        GetSize();

  void           EventNumber(Int_t value){iEventNumber = value;}
  Int_t          EventNumber() {return(iEventNumber);}

  void           SubEvent(Bool_t value){bSubEvent = value;}
  Bool_t         SubEvent() {return(bSubEvent);}

  void           SubType(Int_t value){iSubType = value;}
  Int_t          SubType() {return(iSubType);}

  void           AboveTHRS(Bool_t value){AbvTHRS = value;};
  Bool_t         AboveTHRS() {return(AbvTHRS);}


  void           Fast(UInt_t i,UInt_t n,Float_t value);
  Float_t        Fast(UInt_t i,UInt_t n);
  void           Slow (UInt_t i,UInt_t n,Float_t value);
  Float_t        Slow (UInt_t i,UInt_t n);

  void           TOF   (UInt_t i,Float_t value){if(i<vTOF.size()){vTOF[i] = value;}}
  Float_t        TOF   (UInt_t i) {if(i < vTOF.size()){return(vTOF[i]);}return(0);}
  void           Shift (UInt_t i,Float_t value){if(i<vShift.size()){vShift[i] = value;}}
  Float_t        Shift (UInt_t i) {if(i < vShift.size()){return(vShift[i]);}return(0);}
  void           Delay (UInt_t i,Float_t value){if(i<vDelay.size()){vDelay[i] = value;}}
  Float_t        Delay (UInt_t i) {if(i < vDelay.size()){return(vDelay[i]);}return(0);}
  void           MaxHeight(UInt_t i,Float_t value){if(i<vMaxHeight.size()){vMaxHeight[i] = value;}}
  Float_t        MaxHeight(UInt_t i) {if(i < vMaxHeight.size()){return(vMaxHeight[i]);}return(0);}
  void           Offset(UInt_t i,Float_t value){if(i<vOffset.size()){vOffset[i] = value;}}
  Float_t        Offset(UInt_t i) {if(i < vOffset.size()){return(vOffset[i]);}return(0);}
  void           OffsetSigma(UInt_t i,Float_t value){if(i<vOffsetSigma.size()){vOffsetSigma[i] = value;}}
  Float_t        OffsetSigma(UInt_t i) {if(i < vOffsetSigma.size()){return(vOffsetSigma[i]);}return(0);}

  //Sub Event versions.
  
  void           SubFast(UInt_t i,UInt_t n,Float_t value);
  Float_t        SubFast(UInt_t i,UInt_t n);
  void           SubSlow (UInt_t i,UInt_t n,Float_t value);
  Float_t        SubSlow (UInt_t i,UInt_t n);

  void           SubTOF   (UInt_t i,Float_t value){if(i<vSubTOF.size()){vSubTOF[i] = value;}}
  Float_t        SubTOF   (UInt_t i) {if(i < vSubTOF.size()){return(vSubTOF[i]);}return(0);}
  void           SubShift (UInt_t i,Float_t value){if(i<vSubShift.size()){vSubShift[i] = value;}}
  Float_t        SubShift (UInt_t i) {if(i < vSubShift.size()){return(vSubShift[i]);}return(0);}
  void           SubDelay (UInt_t i,Float_t value){if(i<vSubDelay.size()){vSubDelay[i] = value;}}
  Float_t        SubDelay (UInt_t i) {if(i < vSubDelay.size()){return(vSubDelay[i]);}return(0);}
  void           SubMaxHeight(UInt_t i,Float_t value){if(i<vSubMaxHeight.size()){vSubMaxHeight[i] = value;}}
  Float_t        SubMaxHeight(UInt_t i) {if(i < vSubMaxHeight.size()){return(vSubMaxHeight[i]);}return(0);}






  
  void           Clear(Option_t *option);
  Bool_t         IsValid() const { return fIsValid; }

  ClassDef(PEvent,3)  //PEvent structure
};


#endif
