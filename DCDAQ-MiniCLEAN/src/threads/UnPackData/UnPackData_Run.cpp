#ifdef __RATDS__
#include <UnPackData.h>

void UnPackData::MainLoop()
{
  //Check if we need a new Memblock
  if(IsReadReady(Mem->GetFullFDs()[0]))
    {

      //Get new memory block
      Mem->GetFull(blockIndex);
      if(blockIndex != FreeFullQueue::BADVALUE)
	//We have a valid MemBlock.
	{
	  //Get the memory block for our blockIndex
	  block = Mem->GetBlock(blockIndex);
	  //	  currentDataPointer = (uint32_t *)(block->buffer);
	  currentDataPointer = block->buffer;
//	  sizeAdded = 0;
	  //Remove this fd from events
	  RemoveReadFD(Mem->GetFullFDs()[0]);
	  SetupSelect();

	  //=================================================================
	  //Check the new MemBlock to see if it tells us that there is an end of run (this can be an emtpy block)
	  //=================================================================
	  
	  if(block->dataSize==0)
	    {

	      //If the block has size 0, we are done with all events.
	      //We need a blank DS block for this
	      int32_t dsIndexShutdown;
	      RATDSBlock * dsBlockShutdown;
	      DS_mem->GetFree(dsIndexShutdown);
	      if(dsIndexShutdown != FreeFullQueue::BADVALUE)
		{
		  dsBlockShutdown = DS_mem->GetRATDiskBlock(dsIndexShutdown);
		}
	      else
		{
		  PrintError("Problem getting a RATDSBlock\n");
		  return;
		}
	      //Add an empty RATDSBlock to the ToDisk queue.
	      dsBlockShutdown->ds = NULL;
	      DS_mem->AddToDisk(dsIndexShutdown);
	      SetupSelect();
	      return;
	    }
	  else if (block->dataSize > block->allocatedSize)
	    {
	      PrintError("MemBlock's dataSize is larger than its allocatedSize.");
	      return;
	    }
	  else //This checks the memblock for the run data words
	    {

	      //We have a new memblock, it should have a run word at the beginning
	      DCDAQEvent::Run *run=(DCDAQEvent::Run *) currentDataPointer;
	      
	      if(run->version!=DCDAQEventVersion)
		{
		  char mssg[100];
		  sprintf(mssg,"Wrong DCDAQEventVersion, I am %i, this is %i",DCDAQEventVersion,run->version);
		  PrintError(mssg);
		}
  
	      if(run->magic == DCDAQEvent::RUNSTART)
		{
		  
		  currentDataPointer = ParseRunStart(currentDataPointer);
		  //add a read wakeup for DS blocks
		  AddReadFD(DS_mem->GetFreeFDs()[0]);
		}
	      else if(run->magic == DCDAQEvent::RUNCONTINUE)
		{

		  DCDAQEvent::RunContinue * runContinue=(DCDAQEvent::RunContinue*) currentDataPointer;
		  runNumber=runContinue->runNumber;
		  currentDataPointer += sizeof(DCDAQEvent::RunContinue)>>2;
		  //add a read wakeup for DS blocks
		  AddReadFD(DS_mem->GetFreeFDs()[0]);
		}
	      else if(run->magic == DCDAQEvent::RUNEND)
		{
		  
		  //We are at the end of the run.
		  //We need a blank DS block for this
		  int32_t dsIndexShutdown;
		  RATDSBlock * dsBlockShutdown;
		  DS_mem->GetFree(dsIndexShutdown);
		  if(dsIndexShutdown != FreeFullQueue::BADVALUE)
		    {
		      dsBlockShutdown = DS_mem->GetRATDiskBlock(dsIndexShutdown);
		    }
		  else
		    {
		      PrintError("Problem getting a RATDSBlock\n");
		      return;
		    }
		  //Add an empty RATDSBlock to the ToDisk queue.
		  dsBlockShutdown->ds = NULL;
		  DS_mem->AddToDisk(dsIndexShutdown);
		  SetupSelect();
		  return;
		}
	      else
		{	     
		  PrintError("Could not read run header.");
		  return;
		}
	    }
	  //remove the read on Mem MemoryManager
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed to get MemBlock\n");
	  block = NULL;
	  currentDataPointer = NULL;
	}
    }
  
  if(IsReadReady(DS_mem->GetFreeFDs()[0]))
    {
      //Get a new DS block for this event
      DS_mem->GetFree(dsIndex);
      if(dsIndex != FreeFullQueue::BADVALUE)
	{
	  dsBlock = DS_mem->GetRATDiskBlock(dsIndex);
	}
      else
	{
	  PrintError("Problem getting a RATDSBlock\n");
	  dsBlock = NULL;
	  RemoveReadFD(DS_mem->GetFreeFDs()[0]);
	  SetupSelect();
	  return;
	}

      //Check if we have good data to read from
      if(block != NULL)
	{
	  if((currentDataPointer != NULL) &&
	     (currentDataPointer < (uint32_t *)(block->buffer + block->dataSize)))
	    {
	      //Parse the next event
	      currentDataPointer = ProcessEvent(currentDataPointer);	      	
	      
	      //Put the dsBlock in the ToDisk queue.
	      DS_mem->AddToDisk(dsIndex);
	      dsIndex = FreeFullQueue::BADVALUE;
	    }
	  else
	    {
	      //We are done with this memblock
	      block->dataSize = 0;
	      block = NULL;
	      Mem->AddFree(blockIndex);
	      currentDataPointer = NULL;
	      //Stop listening for new DS blocks
	      RemoveReadFD(DS_mem->GetFreeFDs()[0]);
	      //start listening to memblocks again
	      AddReadFD(Mem->GetFullFDs()[0]);
	      SetupSelect();
	    }	     
	}
      else
	{
	  PrintError("Processing DS with NULL mem block");
	  return;
	}
    }
}


uint32_t * UnPackData::ProcessEvent(uint32_t * data)
{
  //Get the EV 
  RAT::DS::EV * ev = dsBlock->ds->GetEV(0);

  //Get the run number
  dsBlock->ds->SetRunID(runNumber);

  DCDAQEvent::Event * dcEvent = (DCDAQEvent::Event *) data;
  
  if(dcEvent->magic!=DCDAQEvent::EVENT)
    {           
      std::stringstream ss;
      ss << "Incorrect magic word for DCDAQEvent::Event: "
      	 << std::hex << uint32_t(dcEvent->magic) << std::endl;
      PrintError(ss.str().c_str());
      Loop=false;
      //print out the file to disk for viewing
      FILE * errorFile =fopen("UnPackData_BadEvent_block.dat","w");
      for(size_t iWord = 0;iWord<block->dataSize;iWord++)
	{
	  fprintf(errorFile,"%08zu:     0x%08X\n",iWord,block->buffer[iWord]);
	}
    }
  
  uint32_t sizeAdded= StreamBufftoEV((uint8_t*)data,ev);
  //  fprintf(stderr,"TimeCode test: 0x%016zX\n",((DCDAQEvent::Event *) data)->eventTime);
  
  data+=sizeAdded >> 2;
  eventMonitor(dsBlock);
  return data;
}

uint32_t * UnPackData::ParseRunStart(uint32_t * data)
{
    DCDAQEvent::RunStart * runStart = (DCDAQEvent::RunStart *) data;
    runNumber = runStart->runNumber;
    runStartTime = runStart->startTime;
    size_t channelMapSize = runStart->channelMapSize; //32 bit words
    data += int(ceil(sizeof(DCDAQEvent::RunStart)/sizeof(uint32_t)));
    
    //Read in the channel map xml
    
    std::string channelMapString((char*) data);
    xmlDoc * channelMapXML = xmlReadMemory(channelMapString.c_str(), 
					   (int) channelMapString.size(), 
					   NULL, NULL, 0);
    if(channelMapXML == NULL)
      {
	PrintError("Bad XML\n");
	Loop = false;
	SendStop(false);
	//print out the file to disk for viewing
	FILE * errorFile =fopen("UnPackData_BadXML_block.dat","w");
	for(size_t iWord = 0;iWord<block->dataSize;iWord++)
	  {
	    fprintf(errorFile,"%08zu:     0x%08X\n",iWord,block->buffer[iWord]);
	  }
      }
    xmlNode * baseNode = xmlDocGetRootElement(channelMapXML);
    detectorInfo.Setup(baseNode);

    data += channelMapSize; //(move 32bit pointer)

    return data;
}

#endif
