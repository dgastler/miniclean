#ifndef __DCRUNLOCK__
#define __DCRUNLOCK__
namespace DCRunLock
{
  enum Type
    {             //  DAQ STATUS                            who controls the DB in this state 
      RUNNING=0,  //DCDAQ is running:                       NO ONE HAS CONTROL
      FREE,       //currently no run:                       WEB/CMD HAS CONTROL
      ERROR,      //currently no run with failed last run:  WEB/CMD HAS CONTROL
      NEW_RUN,    //DCDAQ stops current run and loads run 0:DCDAQ HAS CONTROL
      STOP,       //This tells DCDAQ to stop a run:         DCDAQ HAS CONTROL
      COUNT       //ALWAYS LAST
    };
  const char * GetTypeName(DCRunLock::Type type);
  DCRunLock::Type GetType(const char * name);
};
#endif
