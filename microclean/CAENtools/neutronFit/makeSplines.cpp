#include <iostream>
#include <math.h>
#include <vector>

#include "TFile.h"
#include "TH1F.h"
#include "THStack.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TTree.h"
#include "TF1.h"
#include "TColor.h"
#include "TLegend.h"

//Take a float from 0 to 1 and return a color on the rainbow.
Color_t GetColor(double flevel)
{
  if(flevel < 0)
    flevel = 0;
  unsigned int MaxLevel = 256*6 - 1;
  unsigned int level = flevel*MaxLevel;
  //  fprintf(stderr,"HI! %e %u : %u\n",flevel,level,MaxLevel);
  if(level > MaxLevel)
    level = MaxLevel;
  unsigned int Leg = 0;
  unsigned int PositionOnLeg = 0;
  if(level)
    {
      Leg = floor(level/256);
      PositionOnLeg = level%256;
    }
  

  
  Color_t color;
  switch (Leg)
    {
    case 0:
      color = TColor::GetColor(255,PositionOnLeg,0);
      break;
    case 1:
      color = TColor::GetColor(255-PositionOnLeg,255,0);
      break;
    case 2:
      color = TColor::GetColor(0,255,PositionOnLeg);
      break;
    case 3:
      color = TColor::GetColor(0,255-PositionOnLeg,255);
      break;
    case 4:
      color = TColor::GetColor(PositionOnLeg,0,255);
      break;
    case 5:
      color = TColor::GetColor(255,0,255-PositionOnLeg);
      break;
    default:
      color = TColor::GetColor(255,255,255);
    }
  //  printf("Leg: %u P: %u Color: %d\n",Leg,PositionOnLeg,color);
  return(color);
}




TH1F * SmearSpectrum(TH1F * InSpectrum,double SmearingFactor)
{
  std::string Name = InSpectrum->GetName();
  std::string Title = InSpectrum->GetTitle();

  Name = std::string("Smeared") + Name;
  Title = std::string("Smeared ") + Title;

  
  TH1F * SmearedSpectrum = new TH1F(Name.c_str(),
				    Title.c_str(),
				    InSpectrum->GetNbinsX(),
				    InSpectrum->GetXaxis()->GetXmin(),
				    InSpectrum->GetXaxis()->GetXmax()*1.2);
  
   
  TF1 * SmearingFunction = new TF1("SmearingFunction","gaus",-1.0*InSpectrum->GetXaxis()->GetXmax(),InSpectrum->GetXaxis()->GetXmax());
  //Vector to contain the new energy of each even after smearing. 
  std::vector<double> EntryEnergies;
  //loop over all bins in this histogram
  for(int bin = 1;bin <= InSpectrum->GetNbinsX();bin++) 
    {
      //Set smearing function.
      //A gaussian centered around the current energy bin with width of the sqrt of the enrgy bin.
      SmearingFunction->SetParameters(1,
				      InSpectrum->GetBinCenter(bin),
				      sqrt(SmearingFactor*InSpectrum->GetBinCenter(bin)));
      for(int entry = 0;entry < InSpectrum->GetBinContent(bin);entry++)
	{
	  EntryEnergies.push_back(SmearingFunction->GetRandom());
	}
    }
  delete SmearingFunction;
  //Fill smeared histogram with smeared events.
  for(unsigned int entry = 0; entry < EntryEnergies.size();entry++)
    {
      SmearedSpectrum->Fill(EntryEnergies[entry]);
    }
  return(SmearedSpectrum);
}




int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      printf("%s RATfile.root\n",argv[0]);
      return(0);
    }
  TFile * InFile = TFile::Open(argv[1],"READ");
  if(!InFile)
    {
      fprintf(stderr,"Bad input file.\n");
      return(0);
    }
  
  TTree * T = (TTree *) InFile->Get("T");
  if(!T)
    {
      fprintf(stderr,"Bad tree.\n");
      return(0);
    }
  if(!T->GetBranch("LSc_volID"))
    {
      fprintf(stderr,"Bad branch LSc_VolID.\n");
      return(0);
    }  
  if(!T->GetBranch("TOF"))
    {
      fprintf(stderr,"Bad branch TOF.\n");
      return(0);
    }
  if(!T->GetBranch("numPe"))
    {
      fprintf(stderr,"Bad branch numPE.\n");
      return(0);
    }
  if(!T->GetBranch("asym"))
    {
      fprintf(stderr,"Bad branch asym.\n");
      return(0);
    }
  if(!T->GetBranch("int_proc"))
    {
      fprintf(stderr,"Bad branch int_proc.\n");
      return(0);
    }
  if(!T->GetBranch("int_scat"))
    {
      fprintf(stderr,"Bad branch int_scat.\n");
      return(0);
    }
  if(!T->GetBranch("ext_scat"))
    {
      fprintf(stderr,"Bad branch ext_scat.\n");
      return(0);
    }
  
      

 
  Int_t ID;
  T->SetBranchAddress("LSc_volID",&ID);
  Double_t TOF;
  T->SetBranchAddress("TOF",&TOF);
  Int_t NumPE;
  T->SetBranchAddress("numPe",&NumPE);
  Double_t Asym;
  T->SetBranchAddress("asym",&Asym);
  Int_t IntScat;
  T->SetBranchAddress("int_scat",&IntScat);
  Int_t ExtScat;
  T->SetBranchAddress("ext_scat",&ExtScat);
  Char_t  IntProc[20];
  T->SetBranchAddress("int_proc",&IntProc);
  


  //  std::vector<double> Angle(20,0);
  //  std::vector<double> Energy(20,0);
  std::vector<int> IDs(20,0);
  IDs[0] = 0;
  IDs[1] = 10;
  IDs[2] = 1;
  IDs[3] = 11;
  IDs[4] = 2;
  IDs[5] = 12;
  IDs[6] = 3;
  IDs[7] = 13;
  IDs[8] = 4;
  IDs[9] = 14;
  IDs[10] = 5;
  IDs[11] = 15;
  IDs[12] = 6;
  IDs[13] = 16;
  IDs[14] = 7;
  IDs[15] = 17;
  IDs[16] = 8;
  IDs[17] = 18;
  IDs[18] = 9;
  IDs[19] = 19;
    
  Double_t TOFMax = 83;
  Double_t TOFMin = 61;
  Double_t AsymMax = .4;
  Double_t AsymMin = -.4;
 
  int BufferSize = 1000;
  char * Buffer = new char[BufferSize];
  TObjArray * Splines = new TObjArray();
  for(int position = 0; position < IDs.size();position++)
    {
      sprintf(Buffer,"Pos_%02d",position);
      TH1F * EnergySpectrum = new TH1F(Buffer,Buffer,1600,0,800);
      EnergySpectrum->GetXaxis()->SetTitle("Recoil Energy (keVee)");


      //      TH1F * hTOF = new TH1F("TOF","Time of flight",200,0,200);
      sprintf(Buffer,"TOF_1_0_%02d",position);
      TH1F * hTOF10 = new TH1F(Buffer,Buffer,200,0,200e-9);
      sprintf(Buffer,"TOF_n_n_%02d",position);
      TH1F * hTOFnn = new TH1F(Buffer,Buffer,200,0,200e-9);
      sprintf(Buffer,"TOF_1_n_%02d",position);
      TH1F * hTOF1n = new TH1F(Buffer,Buffer,200,0,200e-9);
      sprintf(Buffer,"TOF_O_%02d",position);
      TH1F * hTOFO = new TH1F(Buffer,Buffer,200,0,200e-9);
      hTOF10->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      hTOFnn->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      hTOF1n->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      hTOFO->GetXaxis()->SetTitle("Neutron time of flight (ns)");

      hTOF10->SetFillColor(kBlue);
      hTOF1n->SetFillColor(kRed);
      hTOF1n->SetFillStyle(3001);
      hTOFnn->SetFillColor(kRed);
      hTOFnn->SetFillStyle(3005);
      hTOFO->SetFillColor(kGreen);


      TF1 * Gaus = new TF1("gaus","gaus",0,500);
      for(int Event = 0; Event < T->GetEntries();Event++)
	{
	  T->GetEntry(Event);
	  if(ID == IDs[position])
	    {
	      if((Asym < AsymMax) &&
		 (Asym > AsymMin) &&
		 // (IntScat == 1) &&
		 //		 (ExtScat == 0) &&
		 (IntProc[0] == 'H'))
		{
		  if(IntScat == 1)
		    {
		      if(ExtScat == 0)
			{
			  hTOF10->Fill(TOF*1e-9);
			}
		      else
			{
			  hTOF1n->Fill(TOF*1e-9);
			}
		    }
		  else
		    {
		      hTOFnn->Fill(TOF*1e-9);
		    }
		}
	      else
		{
		  hTOFO->Fill(TOF*1e-9);
		}
	    }
	  
	}
      Gaus->SetParameters(hTOF10->GetMaximum(),hTOF10->GetBinCenter(hTOF10->GetMaximumBin()),1);
      hTOF10->Fit(Gaus,"Q");
      TOFMax = (Gaus->GetParameter(1) + 2*Gaus->GetParameter(2))*1e9;
      TOFMin = (Gaus->GetParameter(1) - 2*Gaus->GetParameter(2))*1e9;

      sprintf(Buffer,"Pos %02d TOF: mean %5e sigma %5e",position,Gaus->GetParameter(1),Gaus->GetParameter(2));
      printf("%s\n",Buffer);

      TOFMax = 100;
      TOFMin = 0;

      for(int Event = 0; Event < T->GetEntries();Event++)
	{
	  T->GetEntry(Event);
	  if((ID == IDs[position]) &&
	     (TOF < TOFMax)   &&
	     (TOF > TOFMin)   &&
	     (Asym < AsymMax) &&
	     (Asym > AsymMin) &&
	     (IntProc[0] == 'H'))
	    {
	      EnergySpectrum->Fill(NumPE/5.0);
	    }
	}

      //new way of smearing the data
      TH1F * SmearedEnergySpectrum = SmearSpectrum(EnergySpectrum,0.5);

      
      sprintf(Buffer,"%02d_NeutronSpectrum",position);
  
      //TH1F * SmearedEnergySpectrum = (TH1F*)  EnergySpectrum->Clone(Buffer);
      //      SmearedEnergySpectrum->Smooth(4);

      //           Old way of smearing the data
      //           TH1F * htemp = (TH1F*) EnergySpectrum->Rebin(2,Buffer);
      //           //TH1F * htemp = (TH1F*) (EnergySpectrum->Clone(Buffer));
      //           //htemp->Smooth(20);
      //           if(position > 6)
      //           	htemp->Rebin(2);
      //           htemp->Rebin(2);
      //           htemp->Smooth(2);



      TH1F * htemp = (TH1F*) SmearedEnergySpectrum->Rebin(2,Buffer);
      htemp->Rebin(2);
      if(position > 3)
	{

	  htemp->Rebin(2);
	}
      htemp->Smooth(2);



      htemp->Scale(1.0/htemp->Integral());


      sprintf(Buffer,"%02d_Neutrons.root",position);
      TFile * OutFile = TFile::Open(Buffer,"CREATE");
      if(!OutFile)
	{
	  fprintf(stderr,"File already exists");
	  return(0);
	}
      EnergySpectrum->Write();
      SmearedEnergySpectrum->Write();
      //Make and add histos to a stack histogram.
      sprintf(Buffer,"TOF_pos%02d",position);
      THStack * TOFStack = new THStack(Buffer,Buffer);
      TOFStack->Add(hTOF10);
      TOFStack->Add(hTOF1n);
      TOFStack->Add(hTOFnn);
      TOFStack->Add(hTOFO);
      //make a legend
      TLegend * TOFLegend = new TLegend(0.5,0.5,0.9,0.9);
      TOFLegend->SetBorderSize(1);
      TOFLegend->SetFillColor(0);
      TOFLegend->AddEntry(hTOF10,"(1 int./ 0 ext.) scatters","f");
      TOFLegend->AddEntry(hTOF1n,"(1 int./ N ext.) scatters","f");
      TOFLegend->AddEntry(hTOFnn,"(M int./ N ext.) scatters","f");
      TOFLegend->AddEntry(hTOFO ,"Other interactions","f");
      hTOF10->GetListOfFunctions()->AddLast(TOFLegend);
      //write stack stuff
      TOFStack->Write();
      hTOF10->Write();
      hTOF1n->Write();
      hTOFnn->Write();
      hTOFO->Write();
      
      htemp->Write();
      
      Splines->Add(htemp);
      Splines->Add(hTOF10);
      Splines->Add(hTOF1n);
      Splines->Add(hTOFnn);
      Splines->Add(hTOFO);
      Splines->Add(TOFStack);
      
      OutFile->Close();
      delete OutFile;
      delete EnergySpectrum;
      delete SmearedEnergySpectrum;
//      delete hTOF10;
//      delete hTOF1n;
//      delete hTOFnn;
//      delete hTOFO;
    }
  delete Buffer;
  TFile * OutFile = TFile::Open("NeutronSplines.root","CREATE");
  if(!OutFile)
    {
      fprintf(stderr,"File already exists");
      return(0);
    }
  Splines->Write();
  OutFile->Close();
  delete OutFile;
  return(0);
}
