#ifndef __DRTRIGGERASSEMBLER__
#define __DRTRIGGERASSEMBLER__

#include <DCThread.h>
#include <xmlHelper.h>
#include <DCQueue.h>
#include <DRPCRatManager.h>
#include <DetectorInfo.h>
#include <PID.h>

#include <VENATOREvent.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <queue>
#include <sstream>
#include <time.h>

#define WRITE_COMMAND 0x20
#define READ_COMMAND 0x18
#define OPTIMUM_COUNT 0x800
#define INITIAL_PERIOD 10000
#define MAXIMUM_PERIOD 2000000
#define MINIMUM_PERIOD 50



class DRTriggerAssembler : public DCThread
{
   public:
   DRTriggerAssembler(std::string Name);
   ~DRTriggerAssembler()
     {
       if(sockfd >= 0)
	 {
	   SetNonBlocking(sockfd,false);
	   close(sockfd);
	 }
     };

   bool Setup(xmlNode * SetupNode, std::vector<MemoryManager*> &MemManager);
   virtual void MainLoop();

   private:
   int write_registers(uint16_t words, uint32_t address);
   int read_registers(uint16_t words, uint32_t address, uint8_t hidden);
   void HandleEvent(sEvent);
   int InitializeTriggerBox();

   //Network Communication
   char packetBytes[1500];  //DO NOT CHANGE SIZE!
   uint32_t * packet;
   int sockfd;
   uint8_t packetID;
   uint8_t parsing_position;
   struct sockaddr_in servaddr;

   //TriggerBox setup
   uint32_t clock_low, clock_high, next_event;
   uint16_t gate_duration, trigger_offset;
   uint8_t trigger_mask, trigger_active_time,
     use_external_clock, enable_triggers;
   int badRunningChecks;
   //TriggerBox readout
   DCQueue<sEvent,PipeSelect,NotLockedObject> event_queue;

   //Readout timing control
   time_t currentTime;
   PID * period_perturber;

   //Data reduction PC memory manager
   DRPCRATManager * DRPC_mem;
   DetectorInfo detectorInfo;

   //Trigger box rate control variables
   uint32_t Period;
   void SetPeriod(uint32_t _Period);

   //Handle incoming messages
   virtual bool ProcessMessage(DCMessage::Message &message);
   //Handle select timeout
   virtual void ProcessTimeout();
};

#endif

