#include "v1720EventWindow.h"
#include <math.h>

v1720EventWindow::v1720EventWindow()
{
  Clear();
}
v1720EventWindow::~v1720EventWindow()
{
  Clear();
}

void v1720EventWindow::Clear()
{
  rawData = 0;
  size = 0;
  timeOffset = 0;
  twoPack = false;
}

void v1720EventWindow::Setup(uint32_t * _rawData,uint32_t _size,bool _twoPack,uint32_t _timeOffset)
{
  rawData = _rawData;
  size = _size;
  twoPack = _twoPack;
  timeOffset = _timeOffset;
}

uint16_t v1720EventWindow::GetSample(uint32_t i)
{
  uint16_t ret = 0;
  //If we are using two packing use this function.
  if(twoPack)
    {
      ret = GetSample20pk(i);
    }
  //If we are using 2.5 packing use this function.
  else
    {
      ret = GetSample25pk(i);
    }
  return(ret);
}

uint32_t v1720EventWindow::GetSampleCount()
{
  uint32_t ret = 0;
  if(twoPack)
    {
      ret = size << 1;
    }
  else
    {
      ret = ( (size << 1) + (size >> 1));
    }
  return(ret);
}

uint16_t v1720EventWindow::GetSample20pk(uint32_t i)
{
  uint16_t ret = 0xFFFF;
  if((i < (size<<1)) )
    {
      ret = ((uint16_t *) rawData)[i];
      if(ret&0xF000)
	{
	  fprintf(stderr,"Bad 2.0 packed word\n");
	}
    }
  return(ret);
}

uint16_t v1720EventWindow::GetSample25pk(uint32_t i)
{
  uint16_t ret = 0xFFFF;
  //Parse everything in 64bit chuncks so we have an integer
  //number of samples in our array elements.
  uint64_t * rawData64 = (uint64_t *) rawData;
  uint64_t blocktemp = 0;
  uint64_t block = 0;
  //The next three lines are a hack to build a mask
  //used to get the correct bits from the shifted
  //upper 32 bits from the 64bit word
  uint64_t bigMask = 0xFFFFFFF;
  bigMask = bigMask << 32;
  bigMask |= 0xC0000000;
  if((i < ((size << 1)+(size >> 1))))
    {
      //Find the correct 64bit word.
      blocktemp = rawData64[i/5];
      //Set block to have the correct lower 30 bits.
      block = blocktemp & 0x3FFFFFFF;
      //Shift block temp by two bits, to cover for the
      //00 padding on the 32 bit words.
      blocktemp = (blocktemp >> 2 );
      //Or block with the masked bits(60-30) of the blocktemp
      block = block | ( blocktemp & bigMask);
      //Shift the bits till the correct 12bit sample is at the bottom.
      block = block >> (i%5)*12;
      //mask off those 12 bits and return them.
      ret = block & 0xFFF;
    }
  return(ret);
}



