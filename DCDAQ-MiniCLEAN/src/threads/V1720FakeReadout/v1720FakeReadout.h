#ifndef __V1720FAKEREADOUT__
#define __V1720FAKEREADOUT__

#include <map>
#include <string>

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <cstdlib> //for getenv()
#include <cstring> //for memcpy()
#include <sys/stat.h> //for file stats

#include <cmath> //ceil
#include <boost/algorithm/string.hpp> //for split

class v1720FakeReadout : public DCThread 
{
 public:
  v1720FakeReadout(std::string Name);
  ~v1720FakeReadout();
  virtual bool Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:
  //Input fake data file info and file decriptor
  std::string InFileName;
  FILE * InFile;

  //Event rate control variables.  (passed on to DCThread
  useconds_t InternalPeriod; //Period for DCThread timeout 
  useconds_t DesiredEventPeriod; //Period the user wants.
  time_t lastTime;
  time_t currentTime;
  time_t TimePeriod;
  uint32_t TimePeriodEventCount;

  void LoadEventRate();
  void SetEventPeriod(useconds_t _Period);
  void SetEventFrequency(double freq);


  //Loaded fake data
  uint32_t FakeWFDDataSize;
  uint32_t * FakeWFDData;  //all fake data pointer
  std::vector<uint32_t *> FakeWFDEvent;   //Pointer to individual fake events in FAKEWFD
  unsigned int FakeEventCount;
  uint32_t EventCountOffset;
  uint32_t WFDsPerEvent;


  //Process incoming text message
  void ProcessTextMessage(std::string command);

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message &message);
  //Handle select timeout
  virtual void ProcessTimeout();

  //Map of WFD event IDs 
  std::map<int,uint32_t> WFDEventID;

  //Map of Fake times for WFD header
  std::map<int,uint32_t> WFDFakeTime;

  //Current event index
  uint32_t iWFDEvent;
  //Single loop tranfer bool
  bool MemoryLoop; // Controls if we keep sending all the events or we 

  //Memory manager access and opperations
  uint32_t LoopOffset;
  MemoryBlock * block;
  int32_t blockIndex;
  MemoryBlockManager * Mem; // Memory access

  //Event ID
};
#endif
