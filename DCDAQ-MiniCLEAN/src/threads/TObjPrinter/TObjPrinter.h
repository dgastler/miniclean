#ifndef __TOBJPRINTER__
#define __TOBJPRINTER__
/*

 */

#include <TObject.h>
#include <TFile.h>

#include <DCThread.h>
#include <xmlHelper.h>
#include <StatMessage.h>

#include <math.h> //fabs()

class TObjPrinter : public DCThread 
{
 public:
  TObjPrinter(std::string Name);
  ~TObjPrinter();

  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  
  virtual void MainLoop(){};  //none yo!
 private:
  virtual bool ProcessMessage(DCMessage::Message &message);  

  void PrintTObj(DCMessage::Message &message);

  struct sRoute
  {
    enum DCMessage::DCMessageType type;
    DCMessage::DCAddress route;
  };
  std::vector<sRoute> routes;

  TCanvas * c1;

};
#endif
