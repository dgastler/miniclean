#include <WFDBlockManager.h>

WFDBlockManager::WFDBlockManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("WFDBlockManager"));
  //Make sure our memory vector is empty
  Memory.clear();
}

WFDBlockManager::~WFDBlockManager()
{
  Deallocate();
}

void WFDBlockManager::Deallocate()
{
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //delete DS event (check if it's not NULL)
      if(Memory.back().array)
	{
	  delete [] Memory.back().array; 
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
}

bool WFDBlockManager::AllocateMemory(xmlNode * setupNode)
{
  bool ret = true;
  uint32_t NumWFD;
  int NumDSERR = GetXMLValue(setupNode,"NUMWFD","MANAGER",NumWFD);
  if(NumDSERR != 0)
    {
      fprintf(stderr,"Error reading NUMWFD in WFDBlockManager.\n");
      ret = false;
    }

  uint32_t NumBlocks;
  int NumBlocksERR = GetXMLValue(setupNode,"NUMBLOCKS","MANAGER",NumBlocks);
  if(NumBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMBLOCKS in WFDBlockManager.\n");
      ret = false;
    }

  if(ret)
    {
      for(uint32_t iBlock = 0; iBlock < NumBlocks;iBlock++)
	{
	  WFDBlock wfdBlock;
	  //Allocate a new DS
	  wfdBlock.array = new RAT::DS::EV[NumWFD];
	  if(wfdBlock.array == NULL)
	    {
	      ret = false;
	      wfdBlock.allocatedSize = 0;	      
	    }
	  else
	    {
	      wfdBlock.allocatedSize = NumWFD;
	      wfdBlock.usedSize = 0;
	      //Add it's pointer to the Memory vector.
	      Memory.push_back(wfdBlock);
	      //Add it's index in the Memory vector to the free DS vector.
	      int32_t index = Memory.size()-1;
	      AddFree(index);
	    }
	}
    }
  return(ret);
}

void WFDBlockManager::PrintStatus(int indent)
{
  printf("\n");
  if(indent > 0)
    {
      for(int i = 0; i < indent;i++)
	{
	  printf(" ");
	}
    }
# if __WORDSIZE == 64
  printf("WFDBlockManager\n     Size: %04lu\n     Full: %04lu\n     Free: %04lu\n",
	 Memory.size(),
	 FullSize(),
	 FreeSize());
# else
  printf("WFDBlockManager\n     Size: %04u\n     Full: %04u\n     Free: %04u\n",
	 Memory.size(),
	 FullSize(),
	 FreeSize());
# endif
}
