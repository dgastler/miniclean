#include <EBPusher.h>

EBPusher::EBPusher(std::string Name)
{  
  SetName(Name);
  SetType(std::string("EBPusher"));

  //FEPC memory manager
  FEPC_mem = NULL;

  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  timeStepProcessedEvents = 0;

  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>60000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>60000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
  //Needed for network connetion
  Loop = true;
}

bool EBPusher::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager)
{
  //Set size of the packetBUilder vector
  packetBuilder.resize(ReductionLevel::COUNT);


  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  bool ret = true;
  //==============================================================
  //Load the FEPC memory manager
  //==============================================================
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  //==============================================================
  //Get DetectorINfo data
  //==============================================================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::PMT_OFFSET | 
			 InfoTypes::EVENT_START|
			 InfoTypes::EVENT_END |
			 InfoTypes::EVENT_PROMPT
			 );
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }

  //==============================================================
  //Set up packet parser vector
  //==============================================================
  packetBuilder[ReductionLevel::UNKNOWN           ] = new UNKNOWN();
  packetBuilder[ReductionLevel::CHAN_FULL_WAVEFORM] = new CHAN_FULL_WAVEFORM();
  packetBuilder[ReductionLevel::CHAN_ZLE_WAVEFORM ] = new CHAN_ZLE_WAVEFORM();
  packetBuilder[ReductionLevel::CHAN_ZLE_INTEGRAL ] = new CHAN_ZLE_INTEGRAL();
  packetBuilder[ReductionLevel::CHAN_PROMPT_TOTAL ] = new CHAN_PROMPT_TOTAL(detectorInfo.GetEventStartTime(),
									    detectorInfo.GetEventEndTime(),
									    detectorInfo.GetPromptIntegrationEndTime(),
									    detectorInfo.GetPMTOffset()
);
  packetBuilder[ReductionLevel::EVNT_FULL_WAVEFORM] = new EVNT_FULL_WAVEFORM();
  packetBuilder[ReductionLevel::EVNT_ZLE_WAVEFORM ] = new EVNT_ZLE_WAVEFORM();
  packetBuilder[ReductionLevel::EVNT_ZLE_INTEGRAL ] = new EVNT_ZLE_INTEGRAL();
  packetBuilder[ReductionLevel::EVNT_PROMPT_TOTAL ] = new EVNT_PROMPT_TOTAL();


  //================================
  //Parse data for Client config and expected clients.
  //================================
  //Use port 9997 for EBAssebler
  if(!EBAssembler.Setup(SetupNode))    
    ret = false; //Connection setup failed
  
  if(ret)
    Ready = true;
  else
    return false;
  return StartNewConnection();
}

void EBPusher::PrintStatus()
{
  DCThread::PrintStatus();
  if(EBAssembler.thread != NULL)
    EBAssembler.thread->PrintStatus();
}
