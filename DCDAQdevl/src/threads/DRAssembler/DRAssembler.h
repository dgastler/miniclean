#ifndef __DRASSEMBLER__
#define __DRASSEMBLER__
//DRAssembler 

#include <DCThread.h>

#include <xmlHelper.h>

#include <DRDataPacket.h>
#include "RAT/DS/EV.hh"

#include <DRPCRatManager.h>

#include <ServerConnection.h>
#include <Connection.h>

class DRAssembler : public DCThread 
{
 public:
  DRAssembler(std::string Name);
  ~DRAssembler();

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  enum RATAssemberErrorCodes
  {
    OK = 0,
    LOCK_BLOCKED    = -1,
    WFD_NOT_PROCESSED = -2,
    FATAL_ERROR       = -3,
    BAD_WFD_HEADER    = -4
  };

  
  //======================================
  //DRAssembler.cpp
  //======================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ProcessChildMessage(size_t iConn);
  virtual void PrintStatus();
  //======================================

  //======================================
  //DRAssembler_Run.cpp
  //======================================
  void ProcessDRPusher(DCNetThread * thread);
  int32_t ProcessDRPusherEvent(uint8_t * & data,
			       uint16_t &maxSize);			    
  void ProcessCollision(int64_t newEventID,
			int64_t oldEventID,
			int32_t evIndex,
			uint64_t password);
  int AddDataToEV(DRPCBlock * event,
		  uint8_t * &data,
		  int32_t &dataSizeMax,
		  int pmtCount);
  //  void ProcessBadPacketLostPMT(DCNetPacket *packet);
  //======================================

  //======================================
  //DRAssembler_Net.cpp
  //======================================
  bool ProcessNewConnection();
  void DeleteConnection(size_t iConn);
  //======================================

  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  ServerConnection server;
  
  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;


  //PMT database
  struct PMTInfo
  {
    uint32_t ID;
    double ADC_Offset;
    uint16_t FEPC;
    uint16_t WFD;
  };

  std::vector<PMTInfo> pmtList;
  template<class T>
  bool ReadPMTInfo(xmlNode * pmtNode,const char * entry, T & value);
};


#endif
