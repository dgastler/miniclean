#ifdef __RATDS__
#include <DRGetter.h>

void DRGetter::CheckForNewConnection()
{
 if(IsReadReady(DRSender.GetSocketFD()) || 
    IsWriteReady(DRSender.GetSocketFD()))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(DRSender.GetSocketFD());
      RemoveWriteFD(DRSender.GetSocketFD());      
      SetupSelect();
      
      //If the connect check fails
      if(!DRSender.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %lds\n",
		  DRSender.GetRemoteAddress().GetAddrStr().c_str(),
		  GetSelectTimeoutSeconds());
	  PrintWarning(buffer);
	  DRSender.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      SendStop();
	    }	  
	}
    }
}

bool DRGetter::ProcessNewConnection()
{  
  bool ret = true;
  //  RemoveReadFD(DRSender.GetSocketFD());
  //  RemoveWriteFD(DRSender.GetSocketFD());  
  //  SetupSelect();
  if(DRSender.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(DRSender.GetFullPacketFD());
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(DRSender.GetMessageFD());      

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::CONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);


      std::string connMessage("Connected to DRSender at: ");
      connMessage += DRSender.GetRemoteAddress().GetAddrStr();
      Print(connMessage.c_str());
      //Update the select set so we pay attention to our new connection.
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DRGetter::DeleteConnection()
{
  //Making sure that the thread actually exists

  //Remove the incoming packet manager fds from list
  RemoveReadFD(DRSender.GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(DRSender.GetMessageFD());

  //remove any selecting on GetSocketFD()
  RemoveReadFD(DRSender.GetSocketFD());
  RemoveWriteFD(DRSender.GetSocketFD());

  SetupSelect();

  //Shutdown the thread
  DRSender.Shutdown();

}

void DRGetter::ProcessChildMessage()
{
  DCMessage::Message message = DRSender.GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      DeleteConnection();
      PrintError("DCNetThread Ended!");
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,
	      "Unknown message (%s) from DCNetThread\n",
	      DCMessage::GetTypeName(message.GetType()));
      PrintWarning(buffer);
      delete [] buffer;
    }
}

//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DRGetter::StartNewConnection()
{
  if(DRSender.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return true;
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  SendStop();
	  return(false);
	}

    }
  else 
    {
      if(DRSender.GetSocketFD() >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (DRSender.GetSocketFD());
	  AddWriteFD(DRSender.GetSocketFD());
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  SendStop();
	  return false;
	}
    }
  return true;
}


#endif
