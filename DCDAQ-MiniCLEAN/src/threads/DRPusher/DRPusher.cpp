#ifdef __RATDS__

#include <DRPusher.h>

DRPusher::DRPusher(std::string Name)
{  
  SetName(Name);
  SetType(std::string("DRPusher"));

  //FEPC memory manager
  FEPC_mem = NULL;

  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);
  timeStepProcessedEvents = 0;

  //Mode
  promptTotalMode = false;


  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>2800</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>2800</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);
  //Needed for network connetion
  Loop = true;
  
  #ifdef DEBUG_ROLLOVER
  rollover = false;
  #endif
}

bool DRPusher::Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager)
{
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  bool ret = true;
  
  if(FindSubNode(SetupNode,"PROMPTTOTAL") == NULL)
    {
      promptTotalMode = 0;
    }
  else
    GetXMLValue(SetupNode,"PROMPTTOTAL",GetName().c_str(),promptTotalMode);

  //================================
  //Load the DetectorInfo
  //================================
  //pattern of data needed for this thread

  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);

  if((parsedInfo&InfoTypes::WFD_COUNT) != InfoTypes::WFD_COUNT)
    {
      PrintError("DetectorInfo not complete");
    }


  // Find and load the FEPC memory interface
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!FEPC_mem)
    {
      char * buffer = new char[100];	  
      sprintf(buffer,"Error %s: Missing FEPC RAT memory manager\n.",GetName().c_str());      
      PrintError(buffer);
      delete [] buffer;
      ret = false;
    }


  //================================
  //Parse data for Client config and expected clients.
  //================================
  //Use port 9999 for DCAssembler
  if(!DRAssembler.SetupRemoteAddr(SetupNode))
    //Connection setup failed
    {
      ret = false;
    }
  if(ret)
    {
      Ready = true;
    }
  else
    {
      return(false);
    }
  return(StartNewConnection());
}

void DRPusher::PrintStatus()
{
  DCThread::PrintStatus();
  if(DRAssembler.IsRunning())
    {
      DRAssembler.PrintStatus();
    }
}
#endif
