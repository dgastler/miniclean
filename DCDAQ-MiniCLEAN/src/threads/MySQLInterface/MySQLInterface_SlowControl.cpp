#include <MySQLInterface.h>

void MySQLInterface::SendStats(DCMessage::Message &message)
{
  //Get message source
  DCMessage::DCAddress address = message.GetSource();
  
  std::string query = statParser.ProcessStat(message);
  if(!query.empty())
    {
      if(!SCDB_Connection.RunQuery(query.c_str()))	
	{
	  //	  Print(SCDB_Connection.GetError().c_str());
	}
    }
}

void MySQLInterface::SendError(DCMessage::Message &message)
{
  //Get message time
  DCMessage::SingleUINT64 singleUINT64;
  if(message.GetDataStruct(singleUINT64))
    {
      std::stringstream ss;
      
      ss << "INSERT INTO msg_log (time,ip_address,msgs,type) VALUES (\'"
	 << message.GetTime() << "\',\'"
	 << message.GetSource().GetAddrStr() << "\',\'"
	 << message.GetSource().GetStr() << std::endl
	 << singleUINT64.text << "\',"
	 << "\'Error\');";
      if(!SCDB_Connection.RunQuery(ss.str().c_str()))
	{
	  PrintError(SCDB_Connection.GetError().c_str());
	  SendStop();
	}
    }
}

void MySQLInterface::SendText(DCMessage::Message &message)
{
}

void MySQLInterface::ProcessRunNumber(DCMessage::Message &message)
{
  uint32_t runNumber = 0;
  if(message.GetData(runNumber))
    {
      SetRunNumber(runNumber);      
    }
  else
    {
      GetRunNumberFromSC(message.GetSource());
    }
}

void MySQLInterface::GetRunNumberFromSC(DCMessage::DCAddress sourceAddr)
{
  uint32_t runNumber = 0;
  if(runNumberControl)
    EndRun(); 
  runNumberControl = true;
  //Here we are asked to get the run number, and forward it out to 
  //everyone as if the message really came from message.GetSourceAddress()  
  //Get run number;
  std::string query("select max(num) from runs");
  std::string runNumberString;// = SCDB_Connection.RunQuery(query);
  //  if(!runNumberString.empty())
  if(SCDB_Connection.GetSingleValue(query,runNumberString))
    {
      runNumber = strtol(runNumberString.c_str(),NULL,0);
      if(runNumber < 0)
	runNumber = 0;
      runNumber++;	      
      std::stringstream ss;
      ss << "INSERT INTO runs (start_t) VALUE ("
	 << time(NULL) << ")";
      SCDB_Connection.RunQuery(ss.str().c_str());
      DCMessage::Message runNumberMessage;
      DCMessage::DCAddress dcAddress;
      dcAddress.SetBroadcast();
      runNumberMessage.SetType(DCMessage::RUN_NUMBER);
      runNumberMessage.SetDestination(dcAddress);
      runNumberMessage.SetSource(sourceAddr);
      runNumberMessage.SetData(runNumber);
      ForwardMessageOut(runNumberMessage);	  
      //New run running, update state machine
      UpdateRunLockState(DCRunLock::RUNNING);
    }
}

void MySQLInterface::EndRun()
{
  //Get max run number
  std::string query("select max(num) from runs");
  uint32_t currentRunNumber = 0;
  std::string runNumberString;// = SCDB_Connection.RunQuery(query);
  if(SCDB_Connection.GetSingleValue(query,runNumberString))
    {      
      currentRunNumber = atoi(runNumberString.c_str());
      //	  SCDB_Connection.FreeResult();
      //Check that the run end time is NULL
      std::stringstream ss;	  
      ss << "update runs set end_t="
	 << time(NULL) << " where num=" << currentRunNumber;
      SCDB_Connection.RunQuery(ss.str().c_str());
    }
  SCDB_Connection.FreeResult();
}

void MySQLInterface::SendWarning(DCMessage::Message &message)
{
  if(IncomingMessageCount() < 100)
    {
      DCMessage::DCString dcString;
      std::string text;
      std::vector<uint8_t> data = message.GetData();
      if(dcString.GetData(data,text))
	{
	  //      boost::replace_all(text,"\n"," ");
	  std::stringstream ss;      
	  ss << "INSERT INTO msg_log (time,ip_address,msgs,type) VALUES ('"
	     << message.GetTime() << "','"	
	     << message.GetSource().GetAddrStr() << "',\""
	     << message.GetSource().GetStr() << " "
	     << text << "\","
	     << "'Alert')";
	  if(!SCDB_Connection.RunQuery(ss.str().c_str()))
	    {
	      PrintError(SCDB_Connection.GetError().c_str());
	      //	  SendStop();
	    }
	}
    }
}
