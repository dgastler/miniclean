#ifndef __BADEVENTSTRUCT__
#define __BADEVENTSTRUCT__

namespace BadEvent
{

enum
{
  NO_ERROR = 0,
  STORAGE_COLLISION,
  STORAGE_EXISTING_FULL_EVENT,
  RATASSEMBLER_TOO_MANY_WFDS,
  RATASSEMBLER_COLLISION,
  STORAGE_EMPTY,
  ERROR_COUNT //count of error codes
};

struct sBadEvent
{
  int64_t eventID;
  int32_t index;
  int32_t errorCode;
  sBadEvent()
  {
    errorCode = NO_ERROR;
  }
};

}
#endif
