#ifdef __RATDS__

#include <RATThrowAway.h>

RATThrowAway::RATThrowAway(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATThrowAway"));

  //EBPC memorymanager
  RATBlock_mem = NULL;
  
  //statistics
  lastTime = time(NULL);
  currentTime = time(NULL);
  
  timeStepProcessedEvents = 0;
}

bool RATThrowAway::Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager)
{
 
  //================================
  //Get the EBPC memory manager
  //================================
  if(!FindMemoryManager(RATBlock_mem,SetupNode,MemManager))
    {
      return(false);
    }

  Ready = true;
  return(true);
}
#endif
