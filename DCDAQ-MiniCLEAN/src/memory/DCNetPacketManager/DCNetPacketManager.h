#ifndef __DCNETPACKETMANAGER__
#define __DCNETPACKETMANAGER__

#include <DCNetPacket.h>
#include <MemoryManager.h>
#include <FreeFullQueue.h>
#include <xmlHelper.h>
#include <StatMessage.h>

//for errstring
#include <string.h>

class DCNetPacketManager : public MemoryManager , public FreeFullQueue
{
 public:
  DCNetPacketManager(std::string Name);
  ~DCNetPacketManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  DCNetPacket * GetPacket(uint32_t iPacket)
  {
    if(iPacket < Packets.size())
      {
	return((Packets[iPacket]));
      }
    return(NULL);
  };
  //Overload the FreeFull queue to clear a disarded packet.
  bool AddFree(int32_t & Index)
  {
    //Make sure the freed packet is reset to zero data size.
    Packets[Index]->ClearPacket();
    return(this->FreeFullQueue::AddFree(Index));
  };

  virtual void Shutdown(){ShutdownQueues(); ShutdownStatus = 0;};
  virtual void WakeUp(){WakeUpQueues();};
  virtual bool IsShutdown(){return(ShutdownStatus);};
  
  virtual void PrintStatus(int indent = 0, int verbosity = 0);
  
 private:
  //Vector of Packet pointers.
  std::vector<DCNetPacket*> Packets;  
  void Deallocate();
  volatile bool ShutdownStatus;
};

#endif
