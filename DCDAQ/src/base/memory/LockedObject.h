#ifndef __LOCKEDOBJECT__
#define __LOCKEDOBJECT__

#include <stdint.h>

class LockedObject
{
 public:
  LockedObject(){Shutdown = false;};
  virtual          ~LockedObject(){Clear();};
  void             ShutdownWait(){Shutdown = true;};
  virtual bool     Clear(){return(false);};
  virtual void     WakeUp(){};
  virtual uint16_t GetSize(){return(0);};
 protected:
  virtual void     SetSize(uint16_t _size){};
  bool             GetShutdown(){return(Shutdown);};
 private:
  volatile bool Shutdown;  
  //================================
  //Disallow coping of LockedObjects
  //================================
  LockedObject(const LockedObject&);              //Never implement these functions
  LockedObject& operator=(const LockedObject&);   //Never implement these functions
};

#endif
