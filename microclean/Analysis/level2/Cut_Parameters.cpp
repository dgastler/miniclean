#include "Cut_Parameters.h"

using namespace std;
Cut_Parameters::Cut_Parameters()
{
  Type = 0;

  V0 = 0;
  V1 = 0;
  W0 = 0;
  W1 = 0;
  
  PMTLinearity1 = 0;
  PMTLinearity2 = 0;
  
  PMTTiming = 0;
  TOFmin = 0;
  TOFmax = 0;
  
  PMT2psa0 = 0;
  PMT2psa1 = 0;
  PMT2psa2 = 0;
  PMT2psa3 = 0;
  
  TBRatio = 0;
  Fast = 0;

  bPMTLinearity1 = false;
  bPMTLinearity2 = false;

  bPMTTiming = false;
  bTOFmin = false;
  bTOFmax = false;
  
  bPMT2psa = false;

  bTBRatio = false;
  bFast = false;
}
bool Cut_Parameters::PassCuts(PEvent * pevent)
{
  bool Pass = true;
  if(bPMTLinearity1)
    {
      if(pevent->MaxHeight(0) < PMTLinearity1)
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bPMTLinearity2)
    {
      if(pevent->MaxHeight(1) < PMTLinearity2)
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bPMTTiming)
    {
      if(fabs(pevent->Delay(0) - pevent->Delay(1)) < PMTTiming)
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bTOFmax)
    {
      if( pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0  < TOFmax)
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bTOFmin)
    {
      if( pevent->Delay(2) - (pevent->Delay(0) + pevent->Delay(1))/2.0  > TOFmin)
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bPMT2psa)
    {
      if( (pevent->fFast(2) > PMT2psa0)
	  &&(pevent->fFast(2) < PMT2psa1)
	  &&(pevent->MaxHeight(2) < PMT2psa2*pevent->fFast(2) + PMT2psa3)
	  )
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bTBRatio)
    {
      if( fabs(((pevent->fFast(0) + pevent->fSlow(0))*W0- (pevent->fFast(1) + pevent->fSlow(1))*W1)/((pevent->fFast(0) + pevent->fSlow(0))*W0+ (pevent->fFast(1) + pevent->fSlow(1))*W1)) < TBRatio  )
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }
  if(bFast)
    {
      if( (pevent->fFast(0) + pevent->fFast(1))/(pevent->fFast(0) + pevent->fFast(1) + pevent->fSlow(0) + pevent->fSlow(1)) > Fast)
	Pass = Pass&&true;
      else
	Pass = Pass&&false;
    }

  return(Pass);
  
}
int Cut_Parameters:: SetCuts(XMLNode *xMainNode)
{
  XMLNode xNode=xMainNode->getChildNode("Header");
  cout << "==========================================================" << endl;
  cout << "Run Type is: " << xNode.getAttribute("Run") ;//<< endl;
  char TypeChar = xNode.getAttribute("Run")[0];
  if(TypeChar == 'c' || TypeChar == 'C')
    Type = 1;
  else if(TypeChar == 's' || TypeChar == 'S')
    Type = 2;
  else if(TypeChar == 'n' || TypeChar == 'N')
    Type = 3;
  else
    Type =0;
  cout << " " << Type << endl;
  int n = xNode.nChildNode("Voltage");
  if(n >2)
    cerr << "#pmts > 2?  wtf? miniCLEAN data alreadY?" << endl;

  V0 = atof(xNode.getChildNode("Voltage",0).getAttribute("V"));
  V1 = atof(xNode.getChildNode("Voltage",1).getAttribute("V"));
  W0 = atof(xNode.getChildNode("Weight",0).getAttribute("W"));
  W1 = atof(xNode.getChildNode("Weight",1).getAttribute("W"));
  cout << endl;
  cout << "PMT0 @ " << V0 << "Volts.     Channel Weight = " << W0 << endl
       << "PMT1 @ " << V1 << "Volts      Channel Weight = " << W1 << endl;
  xNode = xMainNode->getChildNode("Cuts");
  //PMT Linearity Cuts
  PMTLinearity1 = atof(xNode.getChildNode("PMTLinearity1").getChildNode("Parameter").getAttribute("P"));
  bPMTLinearity1 = true&&atoi(xNode.getChildNode("PMTLinearity1").getAttribute("use"));  
  if(bPMTLinearity1)
      cout << "    ";
  else
      cout << "not ";
  cout << "using PMT0 Linearity cut.     ";
  if(bPMTLinearity1)
    cout << " Cut @ " << PMTLinearity1 <<endl;
  else
    cout << endl;
  PMTLinearity2 = atof(xNode.getChildNode("PMTLinearity2").getChildNode("Parameter").getAttribute("P"));
  bPMTLinearity2 = true&&atoi(xNode.getChildNode("PMTLinearity2").getAttribute("use"));
  if(bPMTLinearity2)
      cout << "    "; 
  else
      cout << "not ";
  cout << "using PMT1 Linearity cut.     ";
  if(bPMTLinearity2)
    cout << " Cut @ " << PMTLinearity2 <<endl;
  else
    cout << endl;
  //PMT Timing
  PMTTiming = atoi(xNode.getChildNode("PMTTiming").getChildNode("Parameter").getAttribute("P"));
  bPMTTiming = true&&atoi(xNode.getChildNode("PMTTiming").getAttribute("use"));
  if(bPMTTiming)
      cout << "    ";
  else
      cout << "not ";
  cout << "using PMT Timing cut.         ";
  if(bPMTTiming)
    cout << " Cut @ " << PMTTiming <<endl;
  else
    cout << endl;
  //TOF max
  TOFmax = atoi(xNode.getChildNode("TOFmax").getChildNode("Parameter").getAttribute("P"));
  bTOFmax = true&&atoi(xNode.getChildNode("TOFmax").getAttribute("use"));
  if(bTOFmax)
      cout << "    ";
  else
      cout << "not ";
  cout << "using TOFmax cut.             ";
  if(bTOFmax)
    cout << " Cut @ " << TOFmax << endl;
  else
    cout << endl;
  //TOF min
  TOFmin = atoi(xNode.getChildNode("TOFmin").getChildNode("Parameter").getAttribute("P"));
  bTOFmin = true&&atoi(xNode.getChildNode("TOFmin").getAttribute("use"));
  if(bTOFmin)
      cout << "    ";
  else
      cout << "not ";
  cout << "using TOFmin cut.             ";
  if(bTOFmin)
    cout << " Cut @ " << TOFmin << endl ;
  else
    cout << endl;
  //PMT2psa
  PMT2psa0 = atof(xNode.getChildNode("PMT2psa").getChildNode("Parameter",0).getAttribute("P"));
  PMT2psa1 = atof(xNode.getChildNode("PMT2psa").getChildNode("Parameter",1).getAttribute("P"));
  PMT2psa2 = atof(xNode.getChildNode("PMT2psa").getChildNode("Parameter",2).getAttribute("P"));
  PMT2psa3 = atof(xNode.getChildNode("PMT2psa").getChildNode("Parameter",3).getAttribute("P"));
  bPMT2psa = true&&atoi(xNode.getChildNode("PMT2psa").getAttribute("use"));
  if(bPMT2psa)
    cout << "    ";
  else
    cout << "not ";
  cout << "using PMT2 pulse shape cuts.  ";
  if(bPMT2psa)
    cout << " Cuts @ " << PMT2psa0 << " : " << PMT2psa1 << " : " << PMT2psa2 << " : " << PMT2psa3 << endl  ;
  else
    cout << endl;
  //TBRatio
  TBRatio = atof(xNode.getChildNode("TBRatio").getChildNode("Parameter").getAttribute("P"));
  bTBRatio = true&&atoi(xNode.getChildNode("TBRatio").getAttribute("use"));
  if(bTBRatio)
    cout << "    ";
  else
    cout << "not ";
  cout << "using top bottom cut.         ";
  if(bTBRatio)
    cout << " Cut @ " << TBRatio << endl;
  else
    cout << endl;
  //Fast
  Fast = atof(xNode.getChildNode("Fast").getChildNode("Parameter").getAttribute("P"));
  bFast = true&&atoi(xNode.getChildNode("Fast").getAttribute("use"));
  if(bFast)
    cout << "    ";
  else
    cout << "not ";
  cout << "using Fast fraction cut.      ";
  if(bFast)
    cout << " Cut @ " << Fast << endl;
  else
    cout << endl;

  cout << "==========================================================" << endl;

  cout << endl;
  return(0);
}
