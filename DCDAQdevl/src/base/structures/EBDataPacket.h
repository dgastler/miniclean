#ifndef __EBDATAPACKET__
#define __EBDATAPACKET__
#include <ReductionLevel.h>

namespace EBPacket
{
  struct Event
  {
    uint32_t ID;
    uint8_t channelCount; //If this is zero, then we should 
                          //look for an EventData word
    uint8_t  dataType;    //Reductionlevel definitions
    uint8_t  WFDHeaderCount;  //Number of WFD headers to send
  };

  struct WFDHeader
  {
    uint32_t word[4];
  };

//  struct EventData
//  {
//    uint8_t waveformBlockCount;
//  };
  struct ChannelData
  {
    uint8_t ID;
    uint8_t dataBlockCount;
  }; 

  struct PromptTotalBlock
  {
    //Prompt integral (ADC sum)
    uint32_t prompt;
    //total integral (ADC sum)
    uint32_t total;
  };

  struct IntegralBlock
  {
    //    double integral;     //ADC sum
    uint16_t integral;
    //    uint32_t startTime;  //clock ticks
    uint16_t startTime;  //clock ticks
    //    uint32_t width;      //clock ticks
    uint8_t width;      //clock ticks
    //    double baseline;     //ADC
    //    double baselineRMS;  //ADC
  };  
  struct WaveformBlock
  {
    uint32_t startTime; //clock ticks
    uint32_t dataBytes; //bytes of data after this struct that contains waveform
                        //data
  };
}
#endif
