#include <RATWrite.h>

void RATWrite::MainLoop()
{
  if(IsReadReady(Mem->GetFullFDs()[0]) &&
     dsIndex == FreeFullQueue::BADVALUE)
    {
      //Get new ds event
      Mem->GetFull(dsIndex);
      if(dsIndex != -1)
	{
	  dsBlock = Mem->GetDSBlock(dsIndex);
	  iDS = 0;
	}
    }

  if(dsIndex != FreeFullQueue::BADVALUE)
    {
      for(; iDS < dsBlock->usedSize;iDS++)
	{
	  BranchDS = dsBlock->array + iDS;
	  //Check if we have a file opened.  If we don't open one.
	  if(!OutFile)
	    {
	      fileReady = OpenFile();
	    }
	  if(fileReady)
	    {	      
	      BranchDS->GetEV(0)->SetEventID(EventNumber);
	      Tree->Fill(); 
	      BranchDS->PruneEV();
	      EventNumber++;  //Incriment the event number
	      TimeStep_Events++;

	      //Check if we have gone over the maximum number of events
	      if(EventNumber > MaxRunEvents)
		{			  
		  break;
		}
	      else
		{
		  SubEventNumber++;
		  //Handle subrun changes
		  if(SubEventNumber > SubRunMaxEvents)
		    {
		      CloseFile();
		      SubRunNumber++;
		    }		  
		}
	    }//file is open
	  else
	    {
	      iDS--; //make up for the iDS++ that's going to happen.
	      //Something happened when we tried to open the file.
	      //We need to fail if this happens a lot
	      failedFileOpens++;
	      if(failedFileOpens > maxFailedFileOpens)
		{
		  PrintError("Max file opening errors reached");
		  DCMessage::Message message;      
		  message.SetType(DCMessage::STOP);
		  SendMessageOut(message,true);
		}
	    }
	}

      if(iDS >= dsBlock->usedSize)
	{
	  //Pass off free memory block
	  Mem->AddFree(dsIndex);	  
	  iDS = 0;
	}
      if(EventNumber > MaxRunEvents)
	{			  
	  DCMessage::Message message;      
	  //Send a message back to the master and stop the inner run loop
	  printf("Max sub runs reached. Shutting down DAQ\n");
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	}
    }
}

void  RATWrite::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if(difftime(currentTime, lastTime) > GetUpdateTime())
    {
      //Send any status messages
      DCMessage::Message message;
      DCMessage::TimedCount Count;
      
      //Data message
      sprintf(Count.text,"Events read");
      Count.count = TimeStep_Events;
      Count.dt = difftime(currentTime,lastTime);
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);

      TimeStep_Events = 0;

      
      //Check for run timeout
      if((RunEndTime != 0) &&
	 (currentTime > RunEndTime))
	{
	  //Send a message back to the master and stop the inner run loop
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  Loop = false;
	  Running =false;
	}
     
      //Setup next time
      lastTime = currentTime;
    }
}
