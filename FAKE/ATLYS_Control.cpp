#include "ATLYS_Control.h"

ATLYS_Control::ATLYS_Control()
{
  sockFD = -1;
  packets = NULL;
  number_Of_Events = -1;
  IsMaster = false;
}

//Load events from the file
uint32_t ATLYS_Control::Load_Data(const std::string & file)
{  
  struct stat file_Stats;
  if(stat(file.c_str(),&file_Stats) != 0)
    {
      printf("ERROR: %s(file=%s)\n",strerror(errno),file.c_str());
      return -1;
    }

  int fileFD;
  fileFD = open(file.c_str(),O_RDONLY);
  if(fileFD < 0)
    {
      printf("ERROR: failed to open file %s\n",file.c_str());
      return -1;
    }

  //Allocate the needed memory
  number_Of_Events = file_Stats.st_size/(sizeof(ATLYS_Packet));
  packets = (ATLYS_Packet*) malloc(number_Of_Events*sizeof(ATLYS_Packet));
  if(packets == NULL)
    {
      printf("Error: Failed to allocate packet structures\n");
      return -1;
    }

  //Load events from file
  for(uint32_t iEvent = 0; iEvent < number_Of_Events; iEvent++)
    {
      ssize_t readRet;
      //Read packet 1
      readRet = read(fileFD,
		     &(packets[iEvent].part1),
		     sizeof(packets[iEvent].part1));
      if(readRet != sizeof(packets[iEvent].part1))
	{
	  printf("Error: Bad read\n");
	  return -1;
	}

      //Read packet 2
      readRet = read(fileFD,
		     &(packets[iEvent].part2),
		     sizeof(packets[iEvent].part2));
      if(readRet != sizeof(packets[iEvent].part2))
	{
	  printf("Error: Bad read\n");
	  return -1;
	}
      //Add the index to this packet to the queue
      int32_t event_Index = iEvent;
      Packet_Indexes.AddFree(event_Index);
    }
  printf("Loaded %d events\n",number_Of_Events);
  
  source_File = file;
  return number_Of_Events;
}

bool ATLYS_Control::Setup_Network(const std::string & IP)
{
  //Set up sockets for communication
  sockFD = socket(AF_INET,SOCK_DGRAM,0);
  if(sockFD < 0)
    {
      printf("Error allocating socket.\n");
      return false;
    }

  //  struct sockaddr_in sAddr;  
  bzero(&sAddr,sizeof(sAddr));
  sAddr.sin_family = AF_INET;
  sAddr.sin_addr.s_addr=inet_addr(IP.c_str());
  sAddr.sin_port = htons(791);

  Set_Event_Timeout();

  if(IsMaster)
    {
      Send_Start();
      Send_Stop();
    }

  return true;
}

void * ATLYS_Control::main(void * args)
{
  printf("Starting board\n");
  //Set event timeout


  uint32_t event_Count=0;
  uint32_t start_Event = number_Of_Events/2;
  if(start_Event >= 100)
    start_Event = 50;
  bool start_Wait = true;
 
  int32_t packet_Index = FreeFullQueue::BADVALUE;
  while(Running && Packet_Indexes.GetFull(packet_Index))
    {
      //Check for a stop on BADVALUE
      if(packet_Index == FreeFullQueue::BADVALUE)
	break;
      
      if(start_Wait && (event_Count == start_Event) && IsMaster)
	{
	  Send_Start();
	  start_Wait = false;
	}

      //Send packet1
      if(!Send_Part1(packet_Index)) 		      
	{
	  printf("Error sending packet part 1 to board\n");
	  break;
	}
      //Send packet2
      if(!Send_Part2(packet_Index))
	{
	  printf("Error sending packet part 2 to board\n");
	  break;
	}

      //Return packet_Index
      Packet_Indexes.AddFree(packet_Index);

      event_Count++;
    }  
  if(IsMaster)
    Send_Stop();

  return NULL;
}

bool ATLYS_Control::Send_Part1(int32_t index)
{
  bool ret = true;
  //Get our packet
  ATLYS_Packet * packet = GetPacket(index);

  Response_Packet response;
  bzero(&response,sizeof(response));

  while(true)
    {
      if(Is_Running())
	{	  
	  //Send packet
	  sendto(sockFD,&(packet->part1),sizeof(packet->part1),
		 0, //flags: zero
		 (struct sockaddr *)&sAddr,sizeof(sAddr));      
	  recvfrom(sockFD,&response,sizeof(response),
		   0, //flags: zero
		   NULL,NULL); //Don't save the source address
	}
      else
	{	  
	  //The run has ended and we should return
	  ret = false;
	  Running = false;
	  break;
	}      
      
      //Convert response to host byte order
      for(unsigned iWord = 0; iWord < (sizeof(response)/sizeof(uint32_t));iWord++)
	{
	  ((uint32_t *)&response)[iWord] = ntohl(((uint32_t *)&response)[iWord]);
	} 

      //Break out of the loop upon successful sending of the event data
      // a non-zero result code is an error
      if(response.header.result_Code == 0x0)
	{
	  break;
	}
    }
  return ret;
}

bool ATLYS_Control::Send_Part2(int32_t index)
{
  bool ret = true;
  //Get our packet
  ATLYS_Packet * packet = GetPacket(index);
  
  Response_Packet response;
  bzero(&response,sizeof(response));
  do
    {
      if(Is_Running())
	{	  
	  //Send packet
	  sendto(sockFD,&(packet->part2),sizeof(packet->part2),
		 0, //flags: zero
		 (struct sockaddr *) &sAddr,sizeof(sAddr));      
	  recvfrom(sockFD,&response,sizeof(response),
		   0, //flags: zero
		   NULL,NULL); //Don't save the source address
	}
      else
	{
	  //The run has ended and we should return
	  ret = false;
	  break;
	}      
      //Convert response to host byte order
      for(unsigned iWord = 0; iWord < (sizeof(response)/sizeof(uint32_t));iWord++)
	{
	  ((uint32_t *)&response)[iWord] = ntohl(((uint32_t *)&response)[iWord]);
	}
    }while(response.header.result_Code != 0x0);//response.status != 0x09);
  return ret;
}


void ATLYS_Control::Send_Stop()
{
  Control_Packet command;
  Response_Packet response;

  bzero(&command,sizeof(command));
  bzero(&response,sizeof(response));

  command.header.type=0x4;
  command.header.words = 0x1;
  command.address = 0xFFFFFFFF;//start run register??
  command.data = 0x00000000;//zero value turns output off

  for(unsigned iWord = 0; iWord < (sizeof(command)/sizeof(uint32_t));iWord++)
    {
      ((uint32_t *)&command)[iWord] = htonl(((uint32_t *)&command)[iWord]);
    }

  //Send command
  sendto(sockFD,&command,sizeof(command),0,
	 (struct sockaddr *)&sAddr,sizeof(sAddr));
  //Wait for response.
  recvfrom(sockFD,&response,sizeof(response),0,NULL,NULL);
}

void ATLYS_Control::Send_Start()
{
  Control_Packet command;
  Response_Packet response;

  bzero(&command,sizeof(command));
  bzero(&response,sizeof(response));

  command.header.type=0x4;
  command.header.words = 0x1;
  command.address = 0xFFFFFFFF;//start run register??
  command.data = 0x00000539;//non-zero value turns output on

  for(unsigned iWord = 0; iWord < (sizeof(command)/sizeof(uint32_t));iWord++)
    {
      ((uint32_t *)&command)[iWord] = htonl(((uint32_t *)&command)[iWord]);
    }

  //Send command
  sendto(sockFD,&command,sizeof(command),0,
	 (struct sockaddr *)&sAddr,sizeof(sAddr));
  //Wait for response.
  recvfrom(sockFD,&response,sizeof(response),0,NULL,NULL);
}

void ATLYS_Control::Set_Event_Timeout()
{
  Control_Packet command;
  Response_Packet response;

  bzero(&command,sizeof(command));
  bzero(&response,sizeof(response));

  command.header.type=0x4;
  command.header.words = 0x1;
  command.address = 0x7FFFFFFF;//event timeout register
  command.data = 0xF0000000;//time that vhdl is willing to wait 
      //to send an event. 0 is complete patience, 0xFC000000 is the default
      //value of 1 minute, 0xFFFFFFFF is complete impatience

   //Send command
   sendto(sockFD,&command,sizeof(command),0,
	 (struct sockaddr *)&sAddr,sizeof(sAddr));
   //Wait for response.
   recvfrom(sockFD,&response,sizeof(response),0,NULL,NULL);
}

void * launch_ATLYS_Control(void * ptr)
{
  return ((ATLYS_Control *) ptr)->main(NULL);
}
