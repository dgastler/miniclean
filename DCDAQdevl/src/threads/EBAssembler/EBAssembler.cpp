#include <EBAssembler.h>

EBAssembler::EBAssembler(std::string Name)
{
  SetType("EBAssembler");
  SetName(Name);
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>60000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>60000</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
}

EBAssembler::~EBAssembler()
{
  //Close our server listening socket
  //Close all of our connections to remote DCDAQs 
  server.Clear();
}

bool EBAssembler::Setup(xmlNode * SetupNode,
			    std::map<std::string,MemoryManager*> &MemManager)
{  
  Ready = false;
  

  //================================
  //Get the EBPC memory manager
  //================================
  if(!FindMemoryManager(EBPC_mem,SetupNode,MemManager))
    {
      return(false);
    }

  //================================
  //Load Data processing values
  //================================
  if(!FindSubNode(SetupNode,"PromptStartTime"))
    GetXMLValue(SetupNode,"PromptStartTime",GetName().c_str(),promptStartTime);
  else
    {
      PrintError("No PromptStartTime!\n");
      return(false);
    }
  if(!FindSubNode(SetupNode,"LateStartTime"))
    GetXMLValue(SetupNode,"LateStartTime",GetName().c_str(),lateStartTime);
  else
    {
      PrintError("No lateStartTime!\n");
      return(false);
    }

  //================================
  //Load a PMT list
  //================================
  xmlNode * pmtListNode = FindSubNode(SetupNode,"PMTLIST");
  //Parse the pmtlist data
  if(pmtListNode == NULL)
    {
      PrintError("No PMT info!\n");
      return(false);
    }
  else
    {
      //Load the PMT data
      uint32_t PMTCount = NumberOfNamedSubNodes(pmtListNode,"PMT");
      pmtList.resize(PMTCount);
      for(size_t iPMT = 0; iPMT < pmtList.size();iPMT++)
	{
	  //Get this PMT
	  xmlNode * pmtNode = FindIthSubNode(pmtListNode,"PMT",iPMT);

	  //Get the PMTID
	  if(!ReadPMTInfo(pmtNode,"ID",pmtList[iPMT].ID))
	      return(false);
	  //Get the FEPC
	  if(!ReadPMTInfo(pmtNode,"FEPC",pmtList[iPMT].FEPC))
	      return(false);
	  //Get the WFD
	  if(!ReadPMTInfo(pmtNode,"WFD",pmtList[iPMT].WFD))
	    return(false);
	  
	  printf("FEPC: %u  WFD: %u  ID:  %u\n",
		 pmtList[iPMT].FEPC,
		 pmtList[iPMT].WFD,
		 pmtList[iPMT].ID
		 );
	}
    }

  //Get number of copies to make of each PMT 
  if(FindSubNode(SetupNode,"PMTCOPIES") == NULL)
    {
      nCopies = 1; //Default max number of events.
    }
  else
    GetXMLValue(SetupNode,"PMTCOPIES",GetName().c_str(),nCopies);

  pmtCopies.resize(nCopies);
  rawCopies.resize(nCopies);
  rawIntegralCopies.resize(nCopies);
  rawWaveformCopies.resize(nCopies);

  //================================
  //Parse data for server config and expected clients.
  //================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  if(server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.serverSocketFD);
      SetupSelect();
      Ready=true;
    }
  else
    {
      return(false);
    }
  return(true);
}

void EBAssembler::ProcessChildMessage(size_t iConn)
{
  DCMessage::Message message = server[iConn].thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::ERROR)
    {
      DeleteConnection(iConn);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      //      SendMessageOut(message,true);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,"Unknown message (%d) from EBAssembler's DCNetThread\n",message.GetType());
      PrintWarning(buffer);
      delete [] buffer;
    }
}

bool EBAssembler::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void EBAssembler::ProcessTimeout()
{
}

void EBAssembler::PrintStatus()
{
  DCThread::PrintStatus();
  for(size_t i = 0; i <server.Size();i++)
    {
      if(server[i].thread != NULL)
	{
	  server[i].thread->PrintStatus();
	}
    }
}



template<class T>
bool EBAssembler::ReadPMTInfo(xmlNode * pmtNode,const char * entry, T & value)
{
  //Find entry
  if(FindSubNode(pmtNode,entry) ==NULL)		
    //entry not found
    {
      char * buffer = new char[100];
      sprintf(buffer,"Missing %s\n",entry);
      PrintError(buffer);
      delete [] buffer;
      return(false);
    }
  else
    {
      //Get the entry and put it in value.
      GetXMLValue(pmtNode,entry,GetName().c_str(),value);
    } 
  return(true);
}
