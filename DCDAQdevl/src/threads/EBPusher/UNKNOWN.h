#ifndef __UNKNOWN__
#define __UNKNOWN__

#include <PacketBuilder.h>

class UNKNOWN : public PacketBuilder
{
 public:
  UNKNOWN()
  {
    ClearPacket();
  };
  virtual ~UNKNOWN(){ClearPacket();};
  virtual bool Send(FEPCBlock * event)
  {
    printf("UNKNOWN not written.  FAIL!\n");
    return(false);
  }

};

#endif
