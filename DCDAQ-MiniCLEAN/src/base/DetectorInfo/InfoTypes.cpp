#include <InfoTypes.h>

#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>
#include <cmath>

static const char * const TypeName[InfoTypes::COUNT] =
  {
    "NONE",
    "WFD_COUNT",
    "PMT_COUNT",
    "BOARD_NODE",
    "CHANNEL_MAP",
    "PMT_OFFSET",
    "PMT_PRESAMPLES",
    "PMT_ID",
    "PMT_TYPE",
    "EVENT_START",
    "EVENT_END",
    "EVENT_PROMPT"
  };

const char * InfoTypes::GetTypeName(InfoTypes::Type type)
{
  //map from value to index by taking the log2
  int typeMap = (type>0) ? size_t(log2(type)) + 1 : 0;
  if(typeMap < InfoTypes::COUNT)
    return TypeName[typeMap];
  return NULL;
} 

InfoTypes::Type InfoTypes::GetType(const char * name)
{
//  std::string searchString(name);  
//  InfoTypes::Type ret = InfoTypes::Type(0);
//  for(InfoTypes::Type type = InfoTypes::Type(0);
//      type < InfoTypes::COUNT;
//      type = InfoTypes::Type(((int)type) + 1))
//    {
//      if(boost::algorithm::iequals(searchString,
//				   std::string(GetTypeName(type))))
//	{
//	  ret = type;
//	  break;
//	}
//    }
  fprintf(stderr,"I need to go home now\n Please kick me if I don't fix this (dgastler@gmail.com)\n");
  return Type(0);
}
