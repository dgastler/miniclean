#ifdef __RATDS__

#ifndef __RATTHROWAWAY__
#define __RATTHROWAWAY__
/*
This thread reads out a RAT disk block manager and throws away the data inside.
 */

#include <DCThread.h>
#include <xmlHelper.h>

#include <RAT/DS/Root.hh>

#include <RATDiskBlockManager.h>

#include <StatMessage.h>

class RATThrowAway : public DCThread 
{
 public:
  RATThrowAway(std::string Name);
  ~RATThrowAway(){};

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  
  virtual void MainLoop();
 private:

  //==============================================================
  //Basic DCThread things to implement
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  
  virtual void ProcessTimeout();


  //==============================================================
  //Memory manager interface
  //==============================================================
  RATDiskBlockManager * RATBlock_mem;  


  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;
  uint32_t timeStepProcessedEvents;



  //Config file and variables
  std::string Settings;
  uint32_t EventNumber;
  uint32_t SubEventNumber;
  uint16_t SubRunNumber;
  uint16_t SubRunNumberMax;
  uint32_t SubRunMaxEvents;
  uint32_t MaxRunEvents;
  time_t RunEndTime; //seconds



  //The ROOT tree pointer and the pointer to the current DS::Root data structure
  RAT::DS::Root * BranchDS;

};

#endif

#endif
