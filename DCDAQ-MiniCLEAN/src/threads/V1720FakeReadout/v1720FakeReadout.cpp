#include <v1720FakeReadout.h>

v1720FakeReadout::v1720FakeReadout(std::string Name)
{
  SetName(Name);
  SetType(std::string("v1720FakeReadout"));
  blockIndex = FreeFullQueue::BADVALUE;
  block = NULL;
  Mem = NULL;  
  FakeWFDData = NULL;
  TimePeriod = 20;
  TimePeriodEventCount = 0;  
}

v1720FakeReadout::~v1720FakeReadout()
{
  //clear pointer vector
  FakeWFDEvent.clear();
  if(FakeWFDData)
    {
      delete [] FakeWFDData;
    }
  Mem->Shutdown();
}

void v1720FakeReadout::LoadEventRate()
{
  SetDefaultSelectTimeout(InternalPeriod/1000000, 
			  InternalPeriod%1000000);
  TimePeriodEventCount = 0;  
  lastTime = time(NULL);
}
void v1720FakeReadout::SetEventPeriod(useconds_t _Period)
{
  DesiredEventPeriod = _Period;

  //Make a guess as to the correct internal period.
  //Set the internal period to the desired period 
  InternalPeriod = DesiredEventPeriod; 
  //Account for size of mem-blocks
  InternalPeriod *= double(Mem->GetBlockSize())*(double(FakeWFDEvent.size())/double(FakeWFDDataSize));
  //Account for the number of WFDEvents per Event.
  InternalPeriod /= WFDsPerEvent;  
}
void v1720FakeReadout::SetEventFrequency(double freq)
{
  if(freq > 0)
    {
      SetEventPeriod(useconds_t(1E6/freq));  //Convert freq to period in micro seconds
    }
  else
    {
      std::stringstream ss;
      ss << "Invalid frequency: " << freq << std::endl;
      PrintError(ss.str().c_str());
    }
}


bool v1720FakeReadout::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
      RemoveReadFD(Mem->GetFreeFDs()[0]);
      SetupSelect();
      if(InternalPeriod!=0)
	{
	  SetDefaultSelectTimeout(60,0);
	}
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the free queue's FD so we know when there are
      iWFDEvent = 0;      
      if(InternalPeriod == 0)
	{
	  AddReadFD(Mem->GetFreeFDs()[0]);
	}
      else
	{
	  LoadEventRate();
	}
      SetupSelect();
      Loop = true;
    }
  else if(message.GetType() == DCMessage::TEXT)
    {
      //Yank the text data from the DCMessage and process it
      DCMessage::DCString dcString;
      std::string text;
      std::vector<uint8_t> data = message.GetData();
      if(dcString.GetData(data,
			  text))
	{
	  ProcessTextMessage(text);
	}
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void v1720FakeReadout::ProcessTextMessage(std::string command)
{
  //Break up the command by spaces
  std::vector<std::string> splitCommand;
  boost::split(splitCommand,command,boost::is_any_of(" "));
  //Should be freq # or per #
  if(splitCommand.size() >= 2)
    {
      if(boost::iequals(splitCommand[0],"freq"))
	{
	  if(boost::iequals(splitCommand[1],"+") && (splitCommand.size() >= 3))
	    {
	      double oldFreq = 1.0/(double(DesiredEventPeriod)*1E-6);
	      oldFreq += atof(splitCommand[2].c_str());
	      SetEventFrequency(oldFreq);
	    }
	  else
	    {
	      SetEventFrequency(atof(splitCommand[1].c_str()));
	    }
	  if(Loop)
	    {
	      LoadEventRate();
	    }
	}
      else if(boost::iequals(splitCommand[0],"per"))
	{
	  SetEventPeriod(atoi(splitCommand[1].c_str()));
	  if(Loop)
	    {
	      LoadEventRate();
	    }
	}
      //Wrong command
      else
	{
	  std::stringstream ss;
	  ss << "Unknown command " << command;
	  PrintWarning(ss.str());
	}
    }
}
