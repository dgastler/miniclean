#ifdef __CAENLIBS__

#include "v1720WFD.h"
#include "xmlHelper.h"

//int v1720::a32d32r(uint32_t address,uint32_t &value)
//{
//  int CAENret = CAEN_DGTZ_Success;
//  CAENret = CAEN_DGTZ_Read32(Handle,address,&value);
//  if(CAENret != 0)
//    {
//      size_t CAENErrorSize = 100;
//      char * CAENError = new char[CAENErrorSize+1];
//      CAENError[CAENErrorSize] = '\0';
//      CAEN_DGTZ_DecodeError( CAENret,CAENError);
//      fprintf(stderr,"a32d32r: %s\n", CAENError);
//      delete [] CAENError;
//    }
//  return CAENret;
//}

//int v1720::a32d32w(uint32_t address,uint32_t value)
//{
//  int CAENret = CAEN_DGTZ_Success;
//  CAENret = CAEN_DGTZ_Write32(Handle,address,value);
//  if(CAENret != 0)
//    {
//      size_t CAENErrorSize = 100;
//      char * CAENError = new char[CAENErrorSize+1];
//      CAENError[CAENErrorSize] = '\0';
//      CAEN_DGTZ_DecodeError( (int) CAENret,CAENError);
//      fprintf(stderr,"a32d32w: %s\n",CAENError);
//      delete [] CAENError;
//    }
//  return CAENret;
//}

//int v1720::a32MBLTr(uint32_t address,uint32_t * buffer,uint32_t bufferSize,uint32_t &readSize)
//{
//  int CAENret = CAEN_DGTZ_Success;
//  int CAENreadSize = 0;
//
//  CAENret = CAEN_DGTZ_MBLTRead(Handle,
//			      address,
//			      buffer,
//			      bufferSize,
//			      &CAENreadSize);
////  if((CAENret != CAEN_DGTZ_Success) &&
////     !((CAENret == CAEN_DGTZ_Terminated) && CAENreadSize > 0))
//  if((CAENret != CAEN_DGTZ_Success) && (CAENret != CAEN_DGTZ_Terminated))
//    {
//      size_t CAENErrorSize = 100;
//      char * CAENError = new char[CAENErrorSize+1];
//      CAENError[CAENErrorSize] = '\0';
//      CAEN_DGTZ_DecodeError( CAENret,CAENError) ;
//      fprintf(stderr,"CAEN: %s\n",CAENError);
//      delete [] CAENError;
//      readSize = 0;
//    }
//  else
//    {
//      CAENret = CAEN_DGTZ_Success;
//      readSize = uint32_t(CAENreadSize);
//    }
//  return CAENret;
//}

int v1720::ReadRegister(uint32_t address,uint32_t & value)
{
  int ret = CAEN_DGTZ_Success;
  //Read it back
  ret += CAEN_DGTZ_ReadRegister(Handle,address,&value);
  return ret;
}

int v1720::SetRegister(uint32_t address,uint32_t value,const char * Name)
{
  int ret = CAEN_DGTZ_Success;
  uint32_t RegisterReturn = 0;
  //Write value
  //  fprintf(stderr,"Wrote: 0x%08X to 0x%08X\n",value,address);
  ret = CAEN_DGTZ_WriteRegister(Handle,address,value);
  //Read it back
  ret += CAEN_DGTZ_ReadRegister(Handle,address,&RegisterReturn);
  //Make sure everythign is correct.
  if(RegisterReturn != value)
    {
      fprintf(stderr,"%s(0x%08X) write/readback error 0x%08X vs 0x%08X\n",Name,address,RegisterReturn,value);
      ret++;
    }
  return ret;
}

#endif
