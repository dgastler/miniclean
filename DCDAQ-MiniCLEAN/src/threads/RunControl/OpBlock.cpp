#include <OpBlock.h>

bool OpBlock::Load(xmlNode * blockNode)
{
  Clear();
  xmlNode * currentNode = NULL;

  //Load the operation for this block
  if( (currentNode = FindSubNode(blockNode,"OP")) != NULL)
    {
      if(!blockOps.Setup(currentNode))
	{
	  blockOps.Clear();
	  valid = false;
	  return false;
	}
	
    }
  else
    {
      valid = false;
      return false;
    }

  size_t numberOfResponses = NumberOfNamedSubNodes(blockNode,"RESPONSE");
  for(size_t i = 0; i < numberOfResponses; i++)
    {
      if( (currentNode = FindIthSubNode(blockNode,"RESPONSE",i)) != NULL)
	{
	  blockResponse tempBlockResponse;
	  tempBlockResponse.nameSet = false;
	  tempBlockResponse.addrSet = false;
	  tempBlockResponse.typeSet = false;
	  tempBlockResponse.dataSet = false;
	  tempBlockResponse.mesg.Clear();
	  if(tempBlockResponse.mesg.Setup(currentNode))
	    {	      
	      //See what parts of the message we need to check
	      if(FindSubNode(currentNode,"NAME") != NULL)
		tempBlockResponse.nameSet=true;
	      if(FindSubNode(currentNode,"ADDR") != NULL)
		tempBlockResponse.addrSet=true;
	      if(FindSubNode(currentNode,"TYPE") != NULL)
		tempBlockResponse.typeSet=true;
	      if(FindSubNode(currentNode,"DATA") != NULL)
		tempBlockResponse.dataSet=true;
	      blockResponses.push_back(tempBlockResponse);	      
	    }
	  else 
	    {
	      Clear(); 
	      return false;
	    }
	}
      else
	{
	  Clear();
	  return false;
	}
    }
  
  failed = false;
  valid = true;
  return valid; 
}

void OpBlock::CopyObj(const OpBlock & rhs)
{
  Clear();
  valid = rhs.valid;
  blockOps = rhs.blockOps;
  blockResponses = rhs.blockResponses;
  failed = rhs.failed;
  responsesLeft = rhs.responsesLeft;
}

void OpBlock::Clear()
{
  valid=false;
  blockOps.Clear();
  blockResponses.clear();
  Reset();
}

void OpBlock::Reset()
{
  failed = false;
  responsesLeft.clear();
  for(size_t i = 0; i < blockResponses.size();i++)
    {
      responsesLeft.push_back(blockResponses[i]);
    }
}

bool OpBlock::GetOpsMessage(DCMessage::Message & message)
{  
  if(valid)
    {
      message.Clear();
      message = blockOps;
    }
  return valid;
}

bool OpBlock::IsMessageOfInterest(DCMessage::Message & message)
{
  if(!valid)
    {
      return false;
    }
  bool ret = false;
  std::list<blockResponse>::iterator it;
  for(it = responsesLeft.begin();
      it != responsesLeft.end();
      it++)
    {
      //Check NAME
      if(it->nameSet && 
	 !boost::algorithm::iequals(message.GetSource().GetName(),
				    it->mesg.GetSource().GetName()))
	continue;
      //Check Addr
      if(it->addrSet && 
	 (message.GetSource() != 
	  it->mesg.GetSource()))
	continue;
      //Check type
      if(it->typeSet &&
	 (message.GetType() != it->mesg.GetType()))
	continue;
      //check data
      if(it->dataSet)
	{
	  std::vector<uint8_t> itData,mesgData;
	  itData = it->mesg.GetData();
	  mesgData = message.GetData();
	  if(itData.size() != mesgData.size())
	    continue;      
	  std::vector<uint8_t>::iterator itRespData  = itData.begin();
	  std::vector<uint8_t>::iterator respDataEnd = itData.end();
	  std::vector<uint8_t>::iterator itMesgData  = mesgData.begin();
	  if(it->dataSet && 
	     !std::equal(itRespData,respDataEnd,itMesgData))
	    {
	      failed = true;
	    }
	}
      //ok, things are equal now
      ret = true;
      responsesLeft.erase(it);
      it = responsesLeft.begin();
      break;
    }
  return ret;
}

std::string OpBlock::Print()
{
  std::stringstream ss;
  ss << "OpBlock:\n";
  ss << blockOps.PrintPretty();
  ss << "Responses:\n";
  for(size_t iResponse = 0;iResponse < blockResponses.size();iResponse++)
    {
      ss << blockResponses[iResponse].mesg.PrintPretty();
    }  
  return ss.str();
}

