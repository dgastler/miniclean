#include <string>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <sstream>

#include <mysql.h>

#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TStyle.h>

#include "Helper.h"

void MakeDiskRatePlot(MYSQL * conn,int runNumber,time_t start,time_t end);

