#include <LockedObject.h>

LockedObject::LockedObject()
{
  //Allocate this queue's mutex lock.
  if(pthread_mutex_init(&mutex,NULL))
    {
      fprintf(stderr,"Error creating mutex!\n");
      abort();
    }
  //Allocate this queue's condition variable
  if(pthread_cond_init(&cond,NULL))
    {
      fprintf(stderr,"Error creating condition variable!\n");
      abort();
    }
  Shutdown = false;
}
LockedObject::~LockedObject()
{ 
  //Destroy the condition variable
  pthread_cond_destroy(&cond);
  //Destroy the mutex
  pthread_mutex_destroy(&mutex);
}

bool LockedObject::LockObject(bool wait)
{
  //Try to get the lock
  int status = pthread_mutex_trylock(&mutex);
  if((status == EBUSY)  &&   //the only acceptible error from trylock
     wait               &&   //we are suppose to wait
     (!Shutdown) )           //we aren't shutting down
    {
      //Get the mutex. (keep trying if we get an EBUSY)
      while((status = pthread_mutex_lock(&mutex)) == EBUSY)
	{
	  if(Shutdown)
	    break;
	}
    }

  if(status != 0)
    {
      return false; 
    }      
  //Return we have the mutex lock. 
  return true;
}

bool LockedObject::ConditionWait()
{
  //If we need to stop waiting, break here.
  if(Shutdown){return(false);} 
  //condition wait
  pthread_cond_wait(&cond,&mutex);
  return(true);
}
