#include <EBPusher.h>

void EBPusher::ProcessEvent()
{
  //Get event ID to send.
  int32_t index;      
  if(FEPC_mem->GetSend(index))
    //We got an event from the Send queue
    {
      if(index != FEPC_mem->GetBadValue())
	{
	  //Get the DRPCBlock for this event
	  FEPCBlock * event = FEPC_mem->GetFEPCBlock(index);
	  if(SendData(event))
	    {
	      //Clear the event
	      event->Clear();
	      //Add the event to the free queue
	      if(!FEPC_mem->AddFree(index))
		{
		  //We are probably shutting down in this case
		  PrintError("Can't add to free queue. DCDAQ shutdown?\n");
		}
	    }
	  else
	    {
	      PrintError("Failed in SendData\n");
	    }
	}
      else
	{
	  PrintError("Bad event index\n");
	  //We are probably shutting down in this case
	}
    }
  else
    //We couldn't get a Send.       
    {
      //We are probably shutting down in this case
      PrintError("Can't get a Send event.  DCDAQ shutdown?\n");
      SendStop();
    }

}

bool EBPusher::SendData(FEPCBlock * event)
{
  bool ret = true;
  //====================================
  //Event type switch. 
  //====================================
  if((int)packetBuilder.size() > event->reductionLevel)
    {
      ret = packetBuilder[event->reductionLevel]->Send(event);
    }
  else if(event->reductionLevel == ReductionLevel::UNKNOWN)
    {
      PrintError("UNKNOWN reduction level!\n");
      ret = false;
    }
  else
    {
      PrintError("Corrupted reduction level!\n");
      ret = false;
    }	

//  if(event->reductionLevel == ReductionLevel::CHAN_ZLE_INTEGRAL)
//    {
//      ret = SendCHAN_ZLE_INTEGRAL(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::CHAN_ZLE_WAVEFORM)
//    {
//      ret = SendCHAN_ZLE_WAVEFORM(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::CHAN_PROMPT_TOTAL)
//    {
//      ret = SendCHAN_PROMPT_TOTAL(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::EVNT_ZLE_WAVEFORM)
//    {
//      ret = SendEVNT_ZLE_WAVEFORM(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::EVNT_ZLE_INTEGRAL)
//    {
//      ret = SendEVNT_ZLE_INTEGRAL(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::EVNT_PROMPT_TOTAL)
//    {
//      ret = SendEVNT_PROMPT_TOTAL(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::EVNT_FULL_WAVEFORM)
//    {
//      ret = SendEVNT_FULL_WAVEFORM(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::CHAN_FULL_WAVEFORM)
//    {
//      ret = SendCHAN_FULL_WAVEFORM(event);
//    }
//  else if(event->reductionLevel == ReductionLevel::UNKNOWN)
//    {
//      PrintError("UNKNOWN reduction level!\n");
//      ret = false;
//    }
//  else
//    {
//      PrintError("Corrupted reduction level!\n");
//      ret = false;
//    }	
  return(ret);
}

