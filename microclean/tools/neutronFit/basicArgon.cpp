#include <iostream>
#include <math.h>
#include <fstream>

#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TGaxis.h"
#include "TSpline.h"
#include "TObject.h"
#include "TCanvas.h"


int main(int argc, char ** argv)
{
  if(argc < 3)
    {
      fprintf(stderr,"%s EnergyFile outfile SEfiles\n",argv[0]);
      return(0);
    }


  //Parse Energy File
  ifstream EnergyFile(argv[1]);
  if(EnergyFile.fail())
    {
      fprintf(stderr,"Bad Energy file %s.\n",argv[1]);
      return(0);
    }
  std::vector<std::string> vEnergyFile;
  char * DataBuffer = new char[1000];
  while(!EnergyFile.eof())
    {
      EnergyFile.getline(DataBuffer,1000);
      if(DataBuffer[0] != '\n')
	{
	  vEnergyFile.push_back(std::string(DataBuffer));
	  //	  printf("%s\n",vEnergyFile[vEnergyFile.size()-1].c_str());
	}
    }
  delete DataBuffer;

  std::vector<int> ID;
  std::vector<float> Angle;
  std::vector<float> TrueEnergy;
  std::vector<float> TrueEnergyErrorp;
  std::vector<float> TrueEnergyErrorn;
  for(int i = 0;i < vEnergyFile.size();i++)
    {
      std::string::size_type pos = 0;
      ID.push_back(atoi((vEnergyFile[i].substr(pos,vEnergyFile[i].find("     ",pos))).c_str()));
      pos = vEnergyFile[i].find("     ",pos) + 5;
      Angle.push_back(atof((vEnergyFile[i].substr(pos,vEnergyFile[i].find("     ",pos))).c_str()));
      pos = vEnergyFile[i].find("     ",pos) + 5;
      TrueEnergy.push_back(atof((vEnergyFile[i].substr(pos,vEnergyFile[i].find("     ",pos))).c_str()));
      pos = vEnergyFile[i].find("     ",pos) + 5;
      TrueEnergyErrorp.push_back(atof((vEnergyFile[i].substr(pos,vEnergyFile[i].find("     ",pos))).c_str()));
      pos = vEnergyFile[i].find("     ",pos) + 5;
      TrueEnergyErrorn.push_back(atof((vEnergyFile[i].substr(pos,vEnergyFile[i].find("     ",pos))).c_str()));
      //      printf("%d %f %f %f\n",ID[i],Angle[i],TrueEnergy[i],TrueEnergyError[i]);
    }

  std::vector<TH1F*> Histograms;
  std::vector<float> FitEnergy;
  std::vector<float> FitError;
  std::vector<int>   HistID;
  std::vector<float> NumberOfEvents;

  for(int file = 3; file < argc;file++)
    {
      //Figure out energy
      std::string Title(argv[file]);
      std::string::size_type start = Title.find("SE_")+3;
      Title = Title.substr(start,Title.size());
      std::string::size_type end = Title.find("_");
      Title = Title.substr(0,end);
      printf("%s\n",Title.c_str());
      if((start == std::string::npos)||(end == std::string::npos))
	{
	  fprintf(stderr,"Bad input file name\n");
	  return(0);
	}
      int VectorIndex = atoi(Title.c_str()) - 1;
      if((VectorIndex < 0) || (VectorIndex > (ID.size() -1)))
	{
	  fprintf(stderr,"Bad input file name ID\n");
	  return(0);
	}
      //Open File;
      TFile * InFile = TFile::Open(argv[file],"READ");
      if(!InFile)
	{
	  fprintf(stderr,"Bad input file\n");
	  return(0);
	}
      //Fitting functions
      TF1 * gaus1 = new TF1("gaus1","gaus",0,220);
      TF1 * gaus2 = new TF1("gaus2","gaus",0,220);
      //Fit Colors
      gaus1->SetLineColor(kRed);
      gaus2->SetLineColor(kBlue);
      
      //Open histogram and check that it's there. 
      char * NameBuffer = new char[100];
      sprintf(NameBuffer,"%f",TrueEnergy[VectorIndex]);
      TH1F * Light = (TH1F*) ((TH1F*)InFile->Get("cLight"))->Rebin(1,NameBuffer);
      delete NameBuffer;
      if(!Light)
	{
	  fprintf(stderr,"Bad histogram\n");
	  return(0);
	}

      printf("MaxHeightLocation/Mean:  %f\n",Light->GetBinCenter(Light->GetMaximumBin())/Light->GetMean());

      if(Light->GetBinCenter(Light->GetMaximumBin()) > 10)
	//if(Light->GetMean() > 10)
	{
	  Light->Rebin(2);
	}

      //Initial guess at fit parameters.

      gaus1->SetParLimits(1,0,100);
      gaus1->SetParLimits(2,0,40);
      gaus1->SetParameters(Light->GetMaximum(),
			   Light->GetBinCenter(Light->GetMaximumBin()),
			   //			   Light->GetMean(),
			   sqrt(15 + Light->GetBinCenter(Light->GetMaximumBin())));
      if(Light->GetBinCenter(Light->GetMaximumBin()) < 10)
	{
	  gaus1->SetParameter(2,gaus1->GetParameter(1));
	}

      if(Light->GetBinCenter(Light->GetMaximumBin())/Light->GetMean() > 0.2)
	{
	  Light->Fit(gaus1,"Q+","",
		     gaus1->GetParameter(1) - 1*gaus1->GetParameter(2),
		     gaus1->GetParameter(1) + 1*gaus1->GetParameter(2));
	}
      else
	{
	  gaus1->SetParameter(2,20);
	  gaus1->SetParLimits(2,0,30);
	  Light->Fit(gaus1,"Q+","",
		     30,
		     100);
	}
      //Use the last fit for the next fit's parameters.
      gaus2->SetParLimits(1,0,100);
      gaus2->SetParLimits(2,0,40);
      gaus2->SetParameters(gaus1->GetParameter(0),
			   gaus1->GetParameter(1),
			   gaus1->GetParameter(2));
      //fit the second parameter and use old fit to set range. 
      float FitMin = gaus1->GetParameter(1) - 2*gaus1->GetParameter(2);
      float FitMax = gaus1->GetParameter(1) + 2*gaus1->GetParameter(2);
      //Light->Fit(gaus2,"Q+","",
      //		 FitMin,
      //		 FitMax);
      Light->GetXaxis()->SetRangeUser(0,250);
      //Fill vectors with things determined from the fits. 
      Histograms.push_back(Light);
      FitEnergy.push_back(gaus2->GetParameter(1));
      FitError.push_back(gaus2->GetParError(1)); 
      printf("Fit: %f +/- %f keVee\n",FitEnergy[FitEnergy.size()-1],FitError[FitEnergy.size()-1]);
      HistID.push_back(VectorIndex);
      NumberOfEvents.push_back(Light->Integral(
					       Light->GetXaxis()->FindBin(FitMin),
					       Light->GetXaxis()->FindBin(FitMax)));
      //Commented out cause ROOT fucking sucks!
      //      InFile->Close();  
      //      delete InFile;
    }
  TFile * OutFile = TFile::Open(argv[2],"CREATE");
  if(!OutFile)
    {
      fprintf(stderr,"OutFile already exists\n");
      return(0);
    }
  TGraphAsymmErrors * SE = new TGraphAsymmErrors(FitEnergy.size());
  SE->SetName("SE");
  SE->GetXaxis()->SetTitle("Recoil energy (keVr)");
  SE->GetYaxis()->SetTitle("Scintillation efficiency");
  //  SE->SetTitle("Scintillation efficiency");
  for(int Point = 0; Point < Histograms.size();Point++)
    {
      int Index = HistID[Point];
      float dEp = TrueEnergyErrorp[Index];
      float dEn = TrueEnergyErrorn[Index];
      (Histograms[Point])->Write();
      SE->SetPoint(Point,TrueEnergy[Index],FitEnergy[Point]/TrueEnergy[Index]);
      //      float yErrorp = pow(FitError[Point]/(TrueEnergy[Index]*sqrt(NumberOfEvents[Point])),2);
      //      yErrorp      += pow(dEn*FitEnergy[Point]/(TrueEnergy[Index]*TrueEnergy[Index]),2);   // Use dEn for yErrorp because a lower true energy leads to a higher SE
      //      yErrorp = sqrt(yErrorp);
      //      float yErrorn = pow(FitError[Point]/(TrueEnergy[Index]*sqrt(NumberOfEvents[Point])),2);
      //      yErrorn      += pow(dEp*FitEnergy[Point]/(TrueEnergy[Index]*TrueEnergy[Index]),2); // same as above but with n/p swapped. 
      //      yErrorn = sqrt(yErrorn);
      //      SE->SetPointError(Point,dEn,dEp,yErrorn,yErrorp);
      SE->SetPointError(Point,dEn,dEp,FitError[Point]/TrueEnergy[Index],FitError[Point]/TrueEnergy[Index]);
    }
  
  SE->Draw("AP");
  SE->SetDrawOption("AP");
  SE->Write();
  OutFile->Close();
  delete OutFile;
  return(0);
}
