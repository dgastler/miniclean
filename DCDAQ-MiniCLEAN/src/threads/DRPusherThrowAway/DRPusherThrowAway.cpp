#ifdef __RATDS__
#include <DRPusherThrowAway.h>

DRPusherThrowAway::DRPusherThrowAway(std::string Name)
{
  SetType("DRPusherThrowAway");
  SetName(Name);
  //Set initial value for statistics updates
  lastTime = time(NULL);
  currentTime = lastTime;
  Ready = false;
  mem = NULL;
}

bool DRPusherThrowAway::Setup(xmlNode * SetupNode,
			 std::vector<MemoryManager*> &MemManager)
{  
  bool ret = true;
  // Find and load the FEPC memory interface
  if(!FindMemoryManager(mem,SetupNode,MemManager))
    {
      ret = false;
    }
  if(!mem)
    {
      PrintError("Missing FEPC RAT memory manager\n.");
      ret = false;
    }
  if(ret)
    {
      Ready = true;
    }
  return ret;
}



void DRPusherThrowAway::MainLoop()
{
  if(IsReadReady(mem->GetUnSentFDs()[0]))  
    {
      int32_t index;      
      if(mem->GetUnSent(index)) 
	{
	  //Pointer to RATEVBlock
	  RATEVBlock * block = NULL;
	  //Check and get the RATEVBlock
	  if(index != mem->GetBadValue() &&
	     ((block = mem->GetFEPCBlock(index)) != NULL))
	    {
	      //clear the event
	      block->Clear();	
	      timeStepEventCount++;
	      //Add the event to the free queue
	      if(!mem->AddFree(index))
		{
		  //We are probably shutting down in this case
		  PrintError("Can't add to free queue.\n");
		  SendStop();
		}	           
	    }
	  else
	    {
	      PrintError("Bad index/block value\n"); 
	    }
	}
      else
	{
	  PrintError("Failed to get unsent index\n");
	}    
    }
}

void DRPusherThrowAway::ProcessTimeout()
{
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      timeStepEventCount = 0;

      //record time
      lastTime = currentTime;
    }
}

bool DRPusherThrowAway::ProcessMessage(DCMessage::Message & message)
{
  if(message.GetType() == DCMessage::GO)
    {
      if(Ready)
	{
	  AddReadFD(mem->GetUnSentFDs()[0]);
	  SetupSelect();
	  Loop = true;
	  timeStepEventCount = 0;
	}
      return true;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {      
      RemoveReadFD(mem->GetUnSentFDs()[0]);
      SetupSelect();
      Loop = false;
      return true;
    }
  return false;
}
#endif
