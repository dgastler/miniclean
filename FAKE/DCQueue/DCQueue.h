#ifndef __DCQUEUE__
#define __DCQUEUE__

#include <iostream>
#include <cstdio>
#include <cstring> //for strerr

#include <queue>

//ObjectModels
#include <LockedObject.h>
#include <NotLockedObject.h>

//SelectModels
#include <PipeSelect.h>
#include <NoSelect.h>



//==========================================================================
//Functions you care about:
//
//Get( Type, wait):  Gets the next Type.  Waits if specified 
//                   returns true if a message was returned in Type
//                   returns false if no message to return.  returns bad Type
//
//Add( Type, wait):  Adds the next Type.  Waits if specified
//
//SingleReadFD(wait):Call this to do one read on the queue's pipe.
//                   This is helpful for when you call Get, and then
//                   want to Add the result back to the end of the queue. 
//
//GetSize():            Returns queue size
//
//Clear():           Clears all data from queue
//
//ShutdownWait():    Removes the wait option on queues (used when shutting down DCDAQ)
//
//WakeUp():          Uses condition variable to wake anyone stuck waiting on data.
//                   Used after ShutdownWait() when shutting down DCDAQ.
//Setup(Type):       Sets the value returned by Get if there is not data to be returned.
//

//==========================================================================

template< 
  class T , 
  class SelectModel = PipeSelect,
  class ObjectModel = LockedObject> 
  //class DCQueue: public ObjectModel, public SelectModel
class DCQueue: public SelectModel
{
public:
  DCQueue():SelectModel()
  {
    //Setup the Queue
    badValueSet = false;
    addCount = 0;
  };
  
  virtual ~DCQueue()
    {
      Clear(true);
    };

  void ShutdownWait()
  {
    Lock.ShutdownLockWait();
  }

  void WakeUp()
  {
    Lock.WakeUpSignal();
  }

  void Setup(T _badValue)
  {
    badValue = _badValue;
    badValueSet = true;
  };

  size_t GetSize(){return(Queue.size());};

  uint64_t GetAddCount(){return addCount;};

  bool Get( T &Obj,bool Wait = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCQUEUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCQueue badValue not set!\n");
      }
#endif

    //Needed for later, but due to some MACRO scope issues
    //this needs to be declaired here.
    bool notEmpty;
    
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	Obj = badValue;
	return(false);
      }

    //We now have the mutex!

    //if we are waiting for a new block wait on a condition variable.
    if(Wait)
      {
	while(Queue.empty() && Lock.ConditionWait())
	  {
	    //We are in condition wait with the understanding
	    //that a ShutdownLockWait() and a wakeup will
	    //cause this loop to terminate
	  }
      }
    notEmpty = !Queue.empty();    
    if(notEmpty)
      {
	//Set obj to the element at the front of the queue
	Obj = Queue.front();
	//Remove the front object.
	Queue.pop();
	//Read from select model
	SelectModel::Read();	
      }
    else
      {
	//Set the object to badValue;
	Obj = badValue;
      }
    //Unlock 
    Lock.UnlockObject();
    return(notEmpty);
  };

  bool Add(T &Obj,bool Wait = true)
  {
    //Debug mode for checking of someone forgot to set a bad value
#ifdef DCQUEUE_DEBUG_MODE
    if(!badValueSet)
      {
	fprintf(stderr,"DCQueue badValue not set!\n");
      }
#endif

    //Lock
    if(!Lock.LockObject(Wait))
      {
	//Not waiting for unlock
	return(false);
      }
    
    //We now have the mutex!

    //Add obj to the end of the queue
    T PushObj = Obj;   //Make a copy so the Adder can't
                       //change it later.
    Queue.push(PushObj);
    //Write to our select model (write a zero)
    SelectModel::Write(0);

    //Add 1 to the add count
    addCount++;

    //Now that the Object has been added to the queue, return back to the user the badValue.
    Obj = badValue;
    //signal so that if anyone is in cond_wait in Get() they wake up
    Lock.WakeUpSignal();
    //release the mutex.
    Lock.UnlockObject();
    return(true);
  };



  bool Clear(bool wait = true)
  {
    //Lock
    if(!Lock.LockObject(wait))
      {
	//Not waiting for unlock
	return(false);
      }
    
    //We now have the mutex!
    
    //clear all objects in the queue
    while(Queue.size())
      {
	//Remove an entry
	Queue.pop();
	//Read from our Select model
	SelectModel::Read();
      }
	  
    //no nead to signal the condition variable since no one wants to know the
    //queue is empty. 
    
    //unlock mutex
    Lock.UnlockObject();
    return(true);
  };

  const T GetBadValue(){return(badValue);};
  
 private:  
  ObjectModel Lock;
  std::queue< T > Queue;   //The free queue
  T badValue;
  volatile bool badValueSet;
  uint64_t addCount;
};

#endif
