#ifdef __RATDS__
#include <DRGetter.h>

DRGetter::DRGetter(std::string Name)
{
  Ready = false;
  SetType("DRGetter");
  SetName(Name);
  Loop = true; //This thread should start asap
               //This is so the network connections can be made

  //FEPC memory manager
  FEPC_mem = NULL;

  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  //Default packet manager string.
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>64</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  SetSelectTimeout(2,0);

  maxDecisionAttempts = 20;

  outfile = NULL;
}

DRGetter::~DRGetter()
{
  //Shutdown the net thread and close the network socket
  DeleteConnection();
  //  DRSender.Shutdown();
}

void DRGetter::ThreadCleanUp()
{
  DRSender.Shutdown();
}

bool DRGetter::Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;
  //==============================================================
  //Load the DRPC memory manager
  //==============================================================
  if(!FindMemoryManager(FEPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  //==============================================================
  //Parse data for the client packet MM
  //==============================================================
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     int(managerSettings.size()),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);
  //==============================================================
  //Setup connection class 
  //==============================================================
  if(!DRSender.SetupRemoteAddr(SetupNode))
    //Connection setup failed
    {
      ret = false;
    }
  //==============================================================
  //Listen to the local DCQueue
  //==============================================================
  AddReadFD(DecisionQueue.GetFDs()[0]);
  SetupSelect();

  //If everything in setup worked, then set Ready to true;
  if(ret == true)
    {
      Ready = true;
    }
  else
    {
      return false;
    }

  //This starts the connection class
  //it will keep trying to connect, but allow the thread
  //to run.   
  return(StartNewConnection());
}

void DRGetter::PrintStatus()
{
  DCThread::PrintStatus();
  char decisionQueueStatus[] = " DecisionQueue   Size: %04zu\n";
  printf(decisionQueueStatus,DecisionQueue.GetSize());
  if(DRSender.IsRunning())
    {
      DRSender.PrintStatus();
    }  
}
#endif
