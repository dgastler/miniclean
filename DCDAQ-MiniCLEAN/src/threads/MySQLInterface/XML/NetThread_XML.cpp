#include <NetThread_XML.h>


NetThread_XML::NetThread_XML(DCMySQL * conn,int runNumber)
  : Thread_XML(conn,runNumber)
{
}

bool NetThread_XML::Generate_XML(xmlNode * threadsNode,
				 std::string pcName,
				 std::string threadType,
				 std::string threadName,
				 xmlNode * responsesNode)
{
  bool ret = true;
  ret |= Thread_XML::Generate_XML(threadsNode,pcName,threadType,threadName,responsesNode);


  //============================
  //Generates a NETWORK response
  //============================
 
  xmlNode * responseNode=xmlNewChild(responsesNode,NULL,(xmlChar *)"RESPONSE",NULL);
  xmlNewTextChild(responseNode,NULL,BAD_CAST "TYPE",BAD_CAST "NETWORK");
  xmlNode * srcNode=xmlNewChild(responseNode,NULL,(xmlChar *)"SOURCE",NULL);
  xmlNewTextChild(srcNode,NULL,
		  (xmlChar*) "NAME",(xmlChar*) threadName.c_str());

  //if the PCName is not local, then we need to add the Addr
  if(!boost::algorithm::iequals(pcName,"local_conn") &&
     !boost::algorithm::iequals(pcName,"local"))
    {
      std::stringstream query;
      query << "Select ip from NetworkConnections where " 
	    << "pc_name='" <<  pcName <<  "' "
	    << "AND run_number=" << runNumber
	    << " order by order_id";
      std::string Addr;
      DCMySQLConnection->GetSingleValue(query.str(),Addr);
      xmlNewTextChild(srcNode,NULL,
		      (xmlChar*) "ADDR",
		      (xmlChar*) Addr.c_str());		      
    }
  
  //Sucessful network connection response data
  xmlNode * dataNode = xmlNewChild(responseNode,NULL,(xmlChar *)"DATA",NULL);
  xmlNewTextChild(dataNode,NULL,BAD_CAST "WORD32",BAD_CAST "0x1");

  return ret;
}
