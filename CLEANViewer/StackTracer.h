#ifndef __STACKTRACER__
#define __STACKTRACER__
#ifdef __gnu_linux__
//#include <execinfo.h>
//#include <stdio.h>
//#include <stdlib.h>



extern "C" {
#include <backtracesymbols.h>
}
#include <cxxabi.h>
#include <string>

/* Obtain a backtrace and print it to stdout. */
void print_trace();
#endif
#endif
