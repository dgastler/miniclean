#include <RATThrowAway.h>

void RATThrowAway::MainLoop()
{
  //Get a full block
  if(IsReadReady(BasicRATDSManagerFullFDs[0]) &&
     (dsIndex == FreeFullQueue::BADVALUE))
    {
      //Get new ds block
      Mem->GetFull(dsIndex);
      if(dsIndex != FreeFullQueue::BADVALUE)
	{
	  dsBlock = Mem->GetDSBlock(dsIndex);
	}
    }
  
  if(dsIndex != FreeFullQueue::BADVALUE)
    {      
      EventNumber+=dsBlock->usedSize;  //Incriment the event number
      TimeStep_Events+=dsBlock->usedSize;
      //Pass off free memory block
      dsBlock->usedSize = 0;
      Mem->AddFree(dsIndex);
    }
}


void  RATThrowAway::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      DCMessage::TimedCount Count;
      //Data message
      Count.count = TimeStep_Events;
      Count.dt = difftime(currentTime,lastTime);
      message.SetData(&Count,sizeof(Count));
      message.SetType(DCMessage::EVENTSREAD);
      SendMessageOut(message,false);
      TimeStep_Events = 0;
      //Setup next time
      lastTime = currentTime;
    }
}
