#include "v1720Event.h"

v1720Event::v1720Event()
{
  Clear();
}

v1720Event::~v1720Event()
{
  Clear();
}

void v1720Event::Clear()
{
  //clear each channel
  for(size_t i = 0; i < V1720_CHANNEL_COUNT;i++)
    channel[i].Clear();

  rawData = 0;
  size = 0;
  memset(header,0,V1720_HEADER_SIZE);
  ZLE = false;
  twoPack = false;    
}

int32_t v1720Event::ProcessEvent(uint32_t * _rawData,uint32_t _rawSizeMax, bool BasicParseOnly)
{
  int32_t ret = 0;
  //Recast the input char data to uint32_t  
  rawData = (uint32_t *) _rawData;
  if(rawData == NULL)
    {
      return V1720EVENT_NULL_POINTER;
    }
  rawSizeMax = _rawSizeMax ; 
  size = _rawSizeMax;
  uint32_t position = 0;

  int32_t retProcessHeader = ProcessHeader();
  if(retProcessHeader == V1720EVENT_OK)
    //process the event header and proceed if it returns 0
    //Skip full parsing of channels if BasicParseOnly.
    {
      if(BasicParseOnly)
	{
	  //Valid event that we didn't parse the sub channels.
	  //Useful when you just want to get the size of the event and a pointer to it.	  
	  ret = size; // 32bit words

	  //clear each channel (just to be safe since we didnt' fill them)
	  for(size_t i = 0; i < V1720_CHANNEL_COUNT;i++)
	    channel[i].Clear();
	}
      else
	{
	  //move past the header
	  position+=4;
	  //loop over all of the possible eight channels and
	  //add a v1720EventChannel if that channel was digitized.
	  for(uint8_t channelID = 0; ((channelID < V1720_CHANNEL_COUNT) && (position < size)) ; channelID++)
	    {
	      channel[channelID].Clear();
	      //Check if this channel has data
	      if(0x1 & ( channelPattern >> channelID))
		{
		  //There is data and we pass call Process on the channel
		  if(ZLE)
		    {
		      //Process ZLE event
		      ProcessZLE(channelID,position);
		    }
		  else
		    {
		      //Process a full waveform event.
		      ProcessFULL(channelID,position);
		    }			       
		}
	      else
		{
		  //No data in this channel
		  //Process channel with zero size
		  if(channel[channelID].ProcessChannel(rawData + position + 1,
						       //Zero size will fill v1720Channel data,
						       //but no v1720EventWindows
						       0,  
						       ZLE,
						       twoPack,
						       channelID) < 0)
		    {
		      return V1720EVENT_CORRUPT_EVENT;
		    }
		}
	    }
	  //return the index of the next event in memory
	  ret = (position);
	}
    }
  else if (retProcessHeader == V1720EVENT_SMALL_EVENT)
    {
      //return that there are no more valid events.
      ret = V1720EVENT_SMALL_EVENT;  
    }  
  else if(retProcessHeader == V1720EVENT_CORRUPT_EVENT)
    {
      //corrupt event.  Write out bad event and return error.
      //      WriteCorruptEvent();
      ret = V1720EVENT_CORRUPT_EVENT;
    }
  else if(retProcessHeader == V1720EVENT_JUNK_WORD)
    {
      ret = retProcessHeader;
    }
  else
    {
      //This shouldn't happen
      ret = V1720EVENT_OTHER;
    }
  //Finished processing channels If there is more data to process return the next
  //pointer.  If there isn't data return NULL
  return(ret);
}

int32_t v1720Event::ProcessHeader()
{
  //Reset local copy of the event header
  memset(header,0,V1720_HEADER_SIZE);
  //ret values
  //OK            OK!
  //SMALL_EVENT   Not enough data for valid event
  //CORRUPT_EVENT Corrupt event header
  int ret = V1720EVENT_OK;
  //check if the size is big enought for there to be a header(4xuint32_t = 16bytes)
  if(rawSizeMax >= 4)
    {
      //Size big enough for a header.
      //copy the head to the local header copy
      memcpy(header,rawData,sizeof(uint32_t) * V1720_HEADER_SIZE);
      //Check if the header is formatted correctly by looking 
      //for 0xA******* in the header.
      if((header[0]&0xF0000000) == 0xA0000000)
	{
	  //The format of this header can be found in the v1720's manual
	  //Look up the bit offsets/masks there.

	  //set this event's size
	  size = header[0]&0xFFFFFFF;
	  
	  //set board ID (bits 31-27) of header[1];
	  boardID = header[1] >> 27;
	  
	  //Set ZLE bool
	  ZLE = 0x1 & (header[1] >> 24);

	  //Set twopack bool
	  twoPack = !(0x1 & (header[1] >> 25));
	  
	  //Get the pattern on the V1720's front panel
	  IOPattern = 0xFFFF&(header[1] >> 8);
	  
	  //Get the channel mask
	  channelPattern = 0xFF&(header[1]);
	  //Count the bits of the channel mask to find the number of channels.
	  numberOfActiveChannels = BitSum(channelPattern);
	  
	  //get the CAEN event number for this event
	  eventNumber = (header[2])&0xFFFFFFF;

	  //get the CAEN time for this event
	  baseTime = header[3];
	}
      else
	{
	  //Check for a junk word
	  if((header[1]&0xF0000000) == 0xA0000000)
	    {
	      fprintf(stderr,"WARNING: Junk word found\n");
	      ret=V1720EVENT_JUNK_WORD;
	    }
	  else
	    {
	      fprintf(stderr,"ERROR: Incorrect header value:\n");
	      for(uint32_t iHeader = 0; iHeader < 4; iHeader++)
		{
		  fprintf(stderr,"\t\t\t0x%08X\n",header[iHeader]);
		}
	      //	      uint32_t ErrorOutput = 100;
	      //	      fprintf(stderr,"Next %d words:\n",std::min(ErrorOutput,rawSizeMax-4));
//	      for(uint32_t iOutput = 4;(iOutput < (ErrorOutput+4) && iOutput < rawSizeMax);iOutput++)
//		{
//		  fprintf(stderr,"\t\t\t0x%08X\n",rawData[iOutput]);
//		}
	      ret=V1720EVENT_CORRUPT_EVENT;
	    }
	}
    }
  else
    {
      fprintf(stderr,"ERROR:rawData is too small to be an event! %d bytes.\n",size);
      ret=V1720EVENT_SMALL_EVENT;
    }
  return(ret);
}

int v1720Event::ProcessZLE(uint8_t channelID, uint32_t &position)
{
  int ret = 0;
  //Find out how big this channel is in 32bit words
  //subtract one for the size header.
  uint32_t channelSize = rawData[position]-1;

  //check for corrupt data
  if( ((channelSize+position) > (0xFFFFFF&header[0])) ||
      ((channelSize+position) > size))
    {
      ret = -1;
      WriteCorruptEvent();
    }
  else
    {
      //Process this channel.  The data is of size channel size
      //and it starts at rawData + position +1.
      if(channel[channelID].ProcessChannel((rawData + position + 1),
					   channelSize,
					   ZLE,
					   twoPack,
					   channelID) < 0)
	{
	  return V1720EVENT_CORRUPT_EVENT;
	}
      //Move to the next channel position.
      position+= channelSize+1;
    }
  return ret;
}
int v1720Event::ProcessFULL(uint8_t channelID, uint32_t &position)
{
  int ret = 0;
  //Find out how big this channel is in 32bit words
  uint32_t channelSize = (size-4)/(numberOfActiveChannels);

  //extra checks for corrupt data.
  //THis check looks for the amount of data not being aligned to 32bits
  if(((channelSize*numberOfActiveChannels) + 4) != size)
    {
      ret = -1;
      WriteCorruptEvent();
    }
  else
    {
      //Process this channel.  The data is of size channel size
      //and it starts at rawData32 + position32.
      if(channel[channelID].ProcessChannel((rawData + position),
					   channelSize,
					   ZLE,
					   twoPack,
					   channelID) < 0)
	{
	  return V1720EVENT_CORRUPT_EVENT;
	}
      //Move to the next channel position.
      position+= channelSize;
    }
  return ret;
}

uint8_t v1720Event::BitSum(uint32_t data)
{
  uint8_t ret = 0;
  for(int bit = 0; bit < 32;bit++)
    {
      ret+= 0x1 & (data >> bit);
    }
  return(ret);
}

void v1720Event::WriteCorruptEvent()
{
  //Open file to write to.
  char * textBuffer = new char[100];
  
  //Get current time
  time_t errorTime = time(NULL);
  
  //Create file name and re-create file
  sprintf(textBuffer,"v1720Event_CORRUPT_%010ld_%p.dat",errorTime,(void *) rawData);
  FILE * errorFile = fopen(textBuffer,"w");

  if(errorFile)
    {
      //write out the offset and hex value of ecah 32bit word.
      for(uint32_t i =0;i < rawSizeMax;i++)
	{
	  fprintf(errorFile,"%012d 0x%08X\n",i,rawData[i]);
	}
      fclose(errorFile);
    }
  if(textBuffer)
    {
      delete [] textBuffer;
    }
}

uint32_t v1720Event::GetHeaderWord(size_t i)
{
  if(i < V1720_HEADER_SIZE)
    {
      return(header[i]);
    }
  return(0);
}

const v1720EventChannel & v1720Event::GetChannel(size_t i)
{
  //no checking of bounds on i
  return(channel[i]);
}



