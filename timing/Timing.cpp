#include <RAT/DSReader.hh>
#include <TFile.h>
#include <math.h>
#include <iostream>
#include <sstream>

int main(int argc, char ** argv)
{

  int nTotalEvents = 100; //Default value
  int arg_file = 1;
  int arg1 = atoi(argv[1]);
  if(arg1 > 0)
    {
      nTotalEvents = arg1;
      arg_file = 2;
    }
  else
    {
      RAT::DSReader reader(argv[1]);
    }

  RAT::DSReader reader(argv[arg_file]);
  RAT::DS::Root * ds = reader.GetEvent(0);
  RAT::DS::EV * ev = ds->GetEV(0);
  int nBoards = ev->GetBoardCount();

  int first_waveform = 0;
  double event_average[13] = {0};
  double event_offset[13] = {0};
  double offset[13] = {0};
  int iEvent = 0;
  int nEvents = 0;

  //  for(int iEvent = 0; iEvent < 1; iEvent++)
  while(nEvents < nTotalEvents && iEvent < reader.GetTotal())
    {
      ds = reader.GetEvent(iEvent);
      ev = ds->GetEV(0);
      
      if(ev->GetReductionLevel()==2)
	{
	  if(first_waveform==0)
	    first_waveform = iEvent;
	  //Set time zero and determine which is the last WFD
	  uint32_t timeZero = 0xFFFFFFFF;
	  int lastWFD_ID = 0;
	  int lastWFD = -1;
	  for(int iBoard = 0; iBoard < nBoards; iBoard++)
	    {
	      RAT::DS::Board * board = ev->GetBoard(iBoard);
	      uint32_t boardTimeZero = ((board->GetHeader()[3])&0x7FFFFFFF) << 1;
	      if(boardTimeZero < timeZero)
		timeZero = boardTimeZero;  
	      if(board->GetID() > lastWFD_ID)
		{
		  lastWFD_ID = board->GetID();
		  lastWFD = iBoard;
		}
	    }
	  
	  for(int iBoard = 0; iBoard < nBoards; iBoard++)
	    {
	      RAT::DS::Board * board = ev->GetBoard(iBoard);
	      uint32_t boardOffset = ((board->GetHeader()[3])&0x7FFFFFFF) << 1;
	      boardOffset -= timeZero;
	      int channels = 0;
	      int channel_count = board->GetChannelCount();
	      if(board->GetID()==12 || board->GetID()==13){channel_count = 4;}
	      //for(int iChannel = 0; iChannel < board->GetChannelCount(); iChannel++)
	      for(int iChannel = 0; iChannel < channel_count; iChannel++)
		{
		  //std::cout << "Channel " << iChannel << std::endl;
		  RAT::DS::Channel * channel = board->GetChannel(iChannel);
		  if(channel->GetRawBlockCount() > 0)
		    {
		      int max_ADC = 4096;
		      int max_sample = 0;
		      for(int iRaw = 0; iRaw < channel->GetRawBlockCount(); iRaw++)
			{
			  RAT::DS::RawBlock * raw = channel->GetRawBlock(iRaw);
			  for(uint iSample = 0; iSample < raw->GetSamples().size(); iSample++)
			    {
			      if(raw->GetSamples()[iSample] < max_ADC)
				{
				  max_ADC = raw->GetSamples()[iSample];
				  max_sample = iSample + raw->GetOffset() + boardOffset;
				}
			    }
			}
		      event_average[board->GetID()] += max_sample;
		      channels++;
		    }
		}
	      event_average[board->GetID()] = event_average[board->GetID()]/channels;
	    }
	  for(int iBoard = 0; iBoard < nBoards; iBoard++)
	    {
	      RAT::DS::Board *board = ev->GetBoard(iBoard);
	      event_offset[board->GetID()] = event_average[board->GetID()] - event_average[lastWFD_ID];
	      offset[board->GetID()] += event_offset[board->GetID()];
	    }
	  nEvents++;
	}
      iEvent++;
    }

  std::cout << "First waveform event: " << first_waveform << std::endl;

  for(int boardID = 1; boardID <= nBoards; boardID++)
    {
      offset[boardID] = offset[boardID]/nEvents;
      std::cout << std::dec << boardID << "  " << event_average[boardID] << "  " << offset[boardID] << "    " << int(offset[boardID]) << std::endl;
    }
  
  
}
