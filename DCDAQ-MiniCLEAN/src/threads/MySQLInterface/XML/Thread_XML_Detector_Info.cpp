#include <Thread_XML.h>

bool Thread_XML::GetDetectorInfo(const std::string & pcName,
				 const std::string & threadType,
				 const std::string & threadName)
{
 
  std::vector<std::vector<std::string> > pmtData;
  std::vector<std::vector<std::string> > wfdData;

  std::stringstream query;

  xmlNode *channelNode;
  channelNode=xmlNewChild(threadNode,NULL,(xmlChar *)"CHANNELMAP",NULL);

  std::string snWFDs;
  std::stringstream singleQuery;
  singleQuery <<"select count(BoardID) from Settings_WFDs where run_number="<<runNumber<<" and pc_name='"<<pcName<<"'";
  DCMySQLConnection->GetSingleValue(singleQuery.str(),snWFDs);

  //Get list of wfds
  query << "select BoardID, InternalDelay from Settings_WFDs"
	<<" where run_number=" <<runNumber;
  int nWFDs=atoi(snWFDs.c_str());
  if(nWFDs>0)
    {
      query<<" and pc_name='" <<pcName <<"'";
	  
    }
  query<<" order by BoardID";
  enum{BOARD_ID=0,INTERNAL_DELAY};
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }

  DCMySQLConnection->GetTable(wfdData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  //Get list of pmts for each wfd

  query << "select wfd_pos, pmt_id, baseline, presamples, wfd_id, channel_type from Settings_PMTs"
	<<" where ";
  //Loop over boards
 
 
  for(size_t iWFD=0;iWFD<wfdData.size();iWFD++)
    {
      if(iWFD==0)
	{
	  query<<"(";
	}
      query<<"wfd_id="<<wfdData[iWFD][BOARD_ID];
      if(iWFD<wfdData.size()-1)
	{
	  query<<" or ";
	}
      else
	{
	  query<<") and";
	}
    }
  query<<" run_number=" <<runNumber;
  if(nWFDs>0)
    {
      query<<" and pc_name='" <<pcName<<"'";
    }
     query<<" order by wfd_id";
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      return false;
    }

  DCMySQLConnection->GetTable(pmtData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());
  
  enum{WFD_POS=0,PMT_ID,BASELINE,PRESAMPLES,WFD_ID,CHANNEL_TYPE};
 
  //pmt counter for making pmt loop slightly more efficient
  size_t PMTcounter=0;
  //loop through wfds creating nodes
  for(size_t iWFD=0;iWFD<wfdData.size();iWFD++)
    {
      //create WFD node
      xmlNode * wfdNode;
      wfdNode=xmlNewChild(channelNode,NULL,(xmlChar *)"WFD",NULL);

      xmlNewTextChild(wfdNode,NULL,(xmlChar *)"BOARDID",(xmlChar *) wfdData[iWFD][BOARD_ID].c_str());
      xmlNewTextChild(wfdNode,NULL,(xmlChar *) "BOARDDELAY",(xmlChar *) wfdData[iWFD][INTERNAL_DELAY].c_str());

      //now loop over pmts, find ones on this board
      for(size_t iPMT=PMTcounter;iPMT<pmtData.size();iPMT++)
	{ 
	  if(boost::algorithm::iequals(pmtData[iPMT][WFD_ID],wfdData[iWFD][BOARD_ID]))
	    {
	      xmlNode * pmtNode=xmlNewChild(wfdNode,NULL,(xmlChar *)"PMT",NULL);
 	      
	      xmlNewTextChild(pmtNode,NULL,(xmlChar *)"WFDPOS",(xmlChar *) pmtData[iPMT][WFD_POS].c_str());
	      xmlNewTextChild(pmtNode,NULL,(xmlChar *)"PMTID",(xmlChar *) pmtData[iPMT][PMT_ID].c_str());
	      xmlNewTextChild(pmtNode,NULL,(xmlChar *)"ADC_OFFSET",(xmlChar *) pmtData[iPMT][BASELINE].c_str());
	      xmlNewTextChild(pmtNode,NULL,(xmlChar *)"PRESAMPLES",(xmlChar *) pmtData[iPMT][PRESAMPLES].c_str());
	      xmlNewTextChild(pmtNode,NULL,(xmlChar *)"CHANNELTYPE",(xmlChar *) pmtData[iPMT][CHANNEL_TYPE].c_str());
	  
	    }
	  else
	    {
	      PMTcounter=iPMT;
	      break;
	    }
	    
	}
    }


  return true;
}

