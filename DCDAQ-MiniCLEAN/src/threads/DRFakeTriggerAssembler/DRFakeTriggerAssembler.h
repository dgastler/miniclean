#ifndef __DRFAKETRIGGERASSEMBLER__
#define __DRFAKETRIGGERASSEMBLER__

#include <DCThread.h>
#include <xmlHelper.h>
#include <DCQueue.h>
#include <DRPCRatManager.h>

#include <DetectorInfo.h>

class DRFakeTriggerAssembler : public DCThread
{
 public:
  DRFakeTriggerAssembler(std::string Name);
  ~DRFakeTriggerAssembler(){};

  bool Setup(xmlNode * SetupNode, std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:

  //Trigger box event number
  int triggerEventID;

  //Statistics
  time_t currentTime;

  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;
  //RATEVBlock * evBlock;
  //int32_t blockIndex;
  DetectorInfo detectorInfo;

  //Trigger box rate control variables
  useconds_t Period;
  void SetPeriod(useconds_t _Period);

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message &message);
  //Handle select timeout
  virtual void ProcessTimeout();
  //Process collisions
  virtual void ProcessCollision(int64_t newEventID, 
				int64_t oldEventID, 
				int32_t evIndex, 
				uint64_t password);

  //Queue for blocked events
  std::deque<int> triggerQueue;

};

#endif
