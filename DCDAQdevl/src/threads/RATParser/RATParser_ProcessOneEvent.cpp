#include <RATParser.h>

void RATParser::ProcessOneEvent(v1720Event & V1720Event,RAT::DS::EV * RATEvent)
{
  //Set IDs    
  RATEvent->SetEventID(0x00FFFFFF & V1720Event.GetHeaderWord(2));
  
  //Loop over the WFD channels.
  size_t numberOfChannels = V1720Event.GetChannelCount();
  for(size_t iChannel = 0; iChannel < numberOfChannels;iChannel++)
    {
      //Allocate a new PMT if there aren't enough
      if(iChannel >= (unsigned int) RATEvent->GetPMTCount())
	{
	  RATEvent->AddNewPMT();
	}
      //RAT::DS::PMT.
      RAT::DS::PMT * RATPMT = RATEvent->GetPMT(int(iChannel));
      //Get the parsed channel structure.
      v1720EventChannel currentParsedChannel = V1720Event.GetChannel(iChannel);
      RATPMT->SetID(currentParsedChannel.GetChannelID());
      RATPMT->SetPMTType(1);
      //Get this PMT's raw event.
      //RAT::DS::Raw * RATRaw = RATPMT->AddRaw();
      RAT::DS::Raw * RATRaw = RATPMT->GetRaw();

      //Set the window header.
      WindowHeaderVector.clear(); //clears, but does not deallocate
      //Set the header for this PMT
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(0));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(1));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(2));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(3));
      RATRaw->SetHeader(WindowHeaderVector);


      //Clear the raw waveforms already in this PMT.
      //      RATRaw->ClearRaw();      
      //Loop over the zero suppressed windows.
      size_t numberOfWindows = currentParsedChannel.GetWindowCount();
      for(size_t iWindow = 0; iWindow < numberOfWindows;iWindow++)
	{
	  RAT::DS::RawWaveform * RATRawWaveform = NULL;
	  if(iWindow >= ((unsigned int) RATRaw->GetRawWaveformCount()))
	    RATRawWaveform = RATRaw->AddNewRawWaveform();
	  else
	    RATRawWaveform = RATRaw->GetRawWaveform(int(iWindow));
	  
	  //Get the current parsed window in this channel
	  v1720EventWindow currentParsedWindow = currentParsedChannel.GetWindow(iWindow);

	  //For each window show the window's time offset to the event time stamp
	  RATRawWaveform->SetTimeOffset(currentParsedWindow.GetTimeOffset());
	  //Set the data samples for this window
	  WindowDataVector.clear();//clears, but does not deallocate
	  uint32_t windowSize = currentParsedWindow.GetSampleCount();
	  //Unpack the samples from this window and enter them in to the RAT window
	  for(uint32_t iSample = 0;iSample < windowSize;iSample++)
	    {
	      WindowDataVector.push_back(currentParsedWindow.GetSample(iSample));
	    }
	  RATRawWaveform->SetSamples(WindowDataVector);
	}//Window/RAW loop
    }//Channel loop
}
