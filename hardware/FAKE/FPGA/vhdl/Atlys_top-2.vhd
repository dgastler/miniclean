--Boston University Electronics Design Facility--Conor DuBois.
--Set device to xc6slx45 3csg324
--Project derived from work of Xilinx and Jeremy Mans.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity Atlys_top_2 is Port(
	DEBUG : out STD_LOGIC_VECTOR(7 downto 0);
	SYSCLK : in STD_LOGIC;
	CLOCK_P : out STD_LOGIC_VECTOR(3 downto 0);
	CLOCK_N : out STD_LOGIC_VECTOR(3 downto 0);
	DATA_P : out STD_LOGIC_VECTOR(15 downto 0);
	DATA_N : out STD_LOGIC_VECTOR(15 downto 0);
	SYNC_P : in STD_LOGIC_VECTOR(1 downto 0);
	SYNC_N : in STD_LOGIC_VECTOR(1 downto 0);
	PHY_RXCLK : in STD_LOGIC;
	PHY_RESET_L : out STD_LOGIC;
	PHY_MDC : out STD_LOGIC;
	PHY_MDIO : inout STD_LOGIC;
	PHY_RXCTRL_RXDV : in STD_LOGIC;
	PHY_RXD : in STD_LOGIC_VECTOR(7 downto 0);
	PHY_TXC_GTXCLK : out STD_LOGIC;
	PHY_TXCTL_TXEN : out STD_LOGIC;
	PHY_TXD : out STD_LOGIC_VECTOR(7 downto 0);
	PHY_TXCLK : in STD_LOGIC);
end Atlys_top_2;

architecture Behavioral of Atlys_top_2 is
	signal sysclk1 : STD_LOGIC;
	signal sysclk2 : STD_LOGIC;
	signal preclk125 : STD_LOGIC;
	signal clk125 : STD_LOGIC;
	signal prenclk125 : STD_LOGIC;
	signal nclk125 : STD_LOGIC;
	signal clk125_rx : STD_LOGIC;
	signal preclk50 : STD_LOGIC;
	signal clk50 : STD_LOGIC;
	signal preclk350 : STD_LOGIC;
	signal clk350 : STD_LOGIC;
	signal asymmetric : STD_LOGIC_VECTOR(3 downto 0);
	signal feedback : STD_LOGIC;
	signal feedback2 : STD_LOGIC;
	signal data_inbound : unsigned(31 downto 0);
	signal data_outbound : unsigned(31 downto 0);
	signal outbound : STD_LOGIC_VECTOR(111 downto 0);
	signal outbound2 : STD_LOGIC_VECTOR(111 downto 0);
	signal outbound3 : STD_LOGIC_VECTOR(15 downto 0);
	signal addr : unsigned(31 downto 0);
	signal strobe : STD_LOGIC;
	signal writing : STD_LOGIC;
	signal dtack : STD_LOGIC;
	constant myMAC : STD_LOGIC_VECTOR(47 downto 0) := x"00183E00F6CE";
	constant myIP : STD_LOGIC_VECTOR(31 downto 0) := x"C0A80302";
	constant udpPort : STD_LOGIC_VECTOR(15 downto 0) := x"0317";
	signal wr_en : STD_LOGIC;
	signal rd_en : STD_LOGIC := '0';
	signal ran : STD_LOGIC := '0';
	signal run : STD_LOGIC := '0';
	signal prun : STD_LOGIC;
	signal clks : STD_LOGIC;
	signal clkr : STD_LOGIC;
	signal clke : STD_LOGIC;
	signal sync : STD_LOGIC_VECTOR(3 downto 0);
	signal werr :STD_LOGIC_VECTOR(99 downto 0);
	signal berr : STD_LOGIC;
	signal q_addr : unsigned(6 downto 0);
	signal rd_ack : STD_LOGIC;
	signal rd_conf : STD_LOGIC_VECTOR(99 downto 0);
	signal en : STD_LOGIC_VECTOR(99 downto 0);
	signal error_info : unsigned(31 downto 0);
	signal impatience : unsigned(32 downto 0) := '0' & x"FC000000";
	signal fail_count : unsigned (31 downto 0) := x"00000000";
	signal failed : STD_LOGIC;

	component sub_top port(
		CLK125 : in STD_LOGIC;
		CLK125_RX : in STD_LOGIC;
		PHY_RESET_L : out STD_LOGIC;
		PHY_MDC : out STD_LOGIC;
		PHY_MDIO : inout STD_LOGIC;
		PHY_RXCTRL_RXDV : in STD_LOGIC;
		PHY_RXD : in STD_LOGIC_VECTOR(7 downto 0);
		PHY_TXCTL_TXEN : out STD_LOGIC;
		PHY_TXD : out STD_LOGIC_VECTOR(7 downto 0);
		PHY_TXCLK : in STD_LOGIC;
		myMAC : STD_LOGIC_VECTOR(47 downto 0);
		myIP : STD_LOGIC_VECTOR(31 downto 0);
		udpPort : STD_LOGIC_VECTOR(15 downto 0);
		DATA_I : in unsigned(31 downto 0);
		DATA_O : out unsigned(31 downto 0);
		STROBE : out STD_LOGIC;
		WRITING : out STD_LOGIC;
		DTACK : in STD_LOGIC;
		BERR : in STD_LOGIC;
		ERROR_INFO : in unsigned(31 downto 0);
		ADDR : out unsigned(31 downto 0);
		RESET : in STD_LOGIC);
	end component;
	
	component event_queue port(
		CLKS : in STD_LOGIC;
		CLKR : in STD_LOGIC;
		DEBUG : out STD_LOGIC_VECTOR(7 downto 0);
		RESET : in STD_LOGIC;
		CLKA : in STD_LOGIC;
		CLKB : in STD_LOGIC;
		DIN : in STD_LOGIC_VECTOR(31 downto 0);
		WR_EN : in STD_LOGIC;
		WADDR : in STD_LOGIC_VECTOR(8 downto 0);
		EN : in STD_LOGIC_VECTOR(99 downto 0);
		RD_EN : in STD_LOGIC;
		RD_ADDR : out unsigned(6 downto 0);
		RD_CONF : in STD_LOGIC;
		RD_ACK : out STD_LOGIC;
		ERROR_INFO : out unsigned(31 downto 0);
		FAILED : out STD_LOGIC;
		IMPATIENCE : in unsigned(32 downto 0);
		DOUT2 : out STD_LOGIC_VECTOR(15 downto 0);
		DOUT : out STD_LOGIC_VECTOR(95 downto 0));
	end component;
	
	component output_map port(
		DIN : in STD_LOGIC_VECTOR(111 downto 0);
		DOUT : out STD_LOGIC_VECTOR(111 downto 0));
	end component;
	
	component serializer port(
		CLK_IN : in STD_LOGIC;
		CLK_OUT : out STD_LOGIC_VECTOR(3 downto 0);
		DATA_IN : in STD_LOGIC_VECTOR(111 downto 0);
		DATA_OUT : out STD_LOGIC_VECTOR(15 downto 0));
	end component;
	
	component mem_flag port(
		RESET : in STD_LOGIC;
		CLK : in STD_LOGIC;
		INDEX : in unsigned(6 downto 0);
		WADDR : in unsigned(15 downto 0);
		RADDR : in unsigned(6 downto 0);
		EN : out STD_LOGIC;
		WERR : out STD_LOGIC;
		WR_EN : in STD_LOGIC;
		RD_ACK : in STD_LOGIC;
		RD_CONF : out STD_LOGIC);
	end component;
begin
	controller: sub_top port map(
		CLK125 => clk125,
		CLK125_RX => clk125_rx,
		PHY_RESET_L => PHY_RESET_L,
		PHY_MDC => PHY_MDC,
		PHY_MDIO => PHY_MDIO,
		PHY_RXCTRL_RXDV => PHY_RXCTRL_RXDV,
		PHY_RXD => PHY_RXD,
		PHY_TXCTL_TXEN => PHY_TXCTL_TXEN,
		PHY_TXD => PHY_TXD,
		PHY_TXCLK => PHY_TXCLK,
		myMAC => myMAC,
		myIP => myIP,
		udpPort => udpPort,
		DATA_I => data_outbound,
		DATA_O => data_inbound,
		STROBE => strobe,
		WRITING => writing,
		DTACK => dtack,
		BERR => berr,
		ERROR_INFO => error_info,
		ADDR => addr,
		RESET => '0');

	DCM_SP_clk125tx : DCM_SP generic map(
		CLKDV_DIVIDE => 4.0,
		CLKFX_DIVIDE => 4,
		CLKFX_MULTIPLY => 5,
		CLKIN_DIVIDE_BY_2 => FALSE, 
		CLKIN_PERIOD => 10.0,
		CLKOUT_PHASE_SHIFT => "NONE",
		CLK_FEEDBACK => "1X", 
		DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
		DLL_FREQUENCY_MODE => "HIGH",
		DUTY_CYCLE_CORRECTION => TRUE,
		PHASE_SHIFT => 0,
		STARTUP_WAIT => FALSE) port map(
		CLK0 => feedback,
		CLK180 => open,
		CLK270 => open,
		CLK2X => open,
		CLK2X180 => open,
		CLK90 => open,
		CLKDV => open,
		CLKFX => preclk125,
		CLKFX180 => prenclk125,
		LOCKED => open,
		PSDONE => open,
		STATUS => open,
		CLKFB => feedback,
		CLKIN => sysclk2,
		PSCLK => '0',
		PSEN => '0',
		PSINCDEC => '0',
		RST => '0');

	DCM_SP_clk350tx : DCM_SP generic map(
		CLKDV_DIVIDE => 2.0,
		CLKFX_DIVIDE => 2,
		CLKFX_MULTIPLY => 7,
		CLKIN_DIVIDE_BY_2 => FALSE, 
		CLKIN_PERIOD => 10.0,
		CLKOUT_PHASE_SHIFT => "NONE",
		CLK_FEEDBACK => "1X", 
		DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
		DLL_FREQUENCY_MODE => "HIGH",
		DUTY_CYCLE_CORRECTION => TRUE,
		PHASE_SHIFT => 0,
		STARTUP_WAIT => FALSE) port map(
		CLK0 => feedback2,
		CLK180 => open,
		CLK270 => open,
		CLK2X => open,
		CLK2X180 => open,
		CLK90 => open,
		CLKDV => preclk50,
		CLKFX => preclk350,
		CLKFX180 => open,
		LOCKED => open,
		PSDONE => open,
		STATUS => open,
		CLKFB => feedback2,
		CLKIN => sysclk2,
		PSCLK => '0',
		PSEN => '0',
		PSINCDEC => '0',
		RST => '0');

	bufgsys : IBUFG port map(
		O => sysclk1,
		I => SYSCLK);
	
	bufgsys2 : BUFG port map(
		O => sysclk2,
		I => sysclk1);

	bufg125_tx : BUFG port map(
		O => clk125,
		I => preclk125);
	
	bufg125_n : BUFG port map(
		O => nclk125,
		I => prenclk125);

	ibufg125rx : IBUFG port map(
		O => clk125_rx,
		I => PHY_RXCLK);
	
	bufg50_tx : BUFG port map(
		O => clk50,
		I => preclk50);

	bufg350_tx : BUFG port map(
		O => clk350,
		I => preclk350);
	
	--Even though TXC should be an exact duplicate of CLK125, a primitive
	--needs to be inserted between the two in this fashion so that excess
	--skew does not emerge on CLK125 as a result of driving external pins.
	txc_ddr : ODDR2 generic map(
		DDR_ALIGNMENT => "NONE",
		INIT => '0',
		SRTYPE => "SYNC") port map(
		Q => PHY_TXC_GTXCLK,--replica of clk125
		C0 => clk125,
		C1 => nclk125,
		CE => '1',
		D0 => '1',
		D1 => '0',
		R => '0',
		S => '0');
	
	synched: for i in 0 to 1 generate
		lvds0to1: IBUFDS generic map(
			IOSTANDARD => "LVDS_25") port map(
			I => SYNC_P(i),
			IB => SYNC_N(i),
			O => sync(i));
	end generate;
	
	clocks: for i in 0 to 3 generate
		lvds0to3: OBUFDS generic map(
			IOSTANDARD => "LVDS_25") port map(
			O => CLOCK_P(i),
			OB => CLOCK_N(i),
			I => asymmetric(i));
	end generate;

	all_the_data: for i in 0 to 15 generate
		lvds0to15: OBUFDS generic map(
			IOSTANDARD => "LVDS_25") port map(
			O => DATA_P(i),
			OB => DATA_N(i),
			I => outbound3(i));
	end generate;
	
	observant_ophanim: serializer port map(
		CLK_IN => clk350,
		CLK_OUT => asymmetric,
		DATA_IN => outbound2,
		DATA_OUT => outbound3);
	
	shuffler: output_map port map(
		DIN => outbound,
		DOUT => outbound2);

	retentive_register0to99: event_queue port map(
		CLKS => clks,
		CLKR => clkr,
		DEBUG => DEBUG,
		RESET => '0',
		CLKA => clk125,
		CLKB => clk50,
		DIN => STD_LOGIC_VECTOR(data_inbound),
		WR_EN => wr_en,
		EN => en,
		WADDR => STD_LOGIC_VECTOR(addr(8 downto 0)),
		RD_EN => rd_en,
		RD_ADDR => q_addr,
		RD_CONF => rd_conf(to_integer(q_addr)),
		RD_ACK => rd_ack,
		ERROR_INFO => error_info,
		FAILED => failed,
		IMPATIENCE => impatience,
		DOUT2 => outbound(111 downto 96),
		DOUT => outbound(95 downto 0));
	
	mem_flags: for i in 0 to 99 generate
		flag0to99: mem_flag port map(
			RESET => clke,
			CLK => clk125,
			INDEX => to_unsigned(i, 7),
			WADDR => addr(15 downto 0),
			RADDR => q_addr,
			EN => en(i),
			WERR => werr(i),
			WR_EN => wr_en,
			RD_ACK => rd_ack,
			RD_CONF => rd_conf(i));
	end generate;

	data_outbound <= fail_count;--x"00000000";
	prun <= sync(0);
	clks <= sync(1);

	process(clk125)
	begin
		if clk125'event and clk125 = '1' then
			run <= prun;
			ran <= run;
			if run = '0' then
				rd_en <= '0';
				clkr <= '0';
				if ran = '1' then
					clke <= '1';--reset mem flags
				else
					clke <= '0';
				end if;
				fail_count <= x"00000000";
			else
				rd_en <= '1';--turn output on
				clke <= '0';
				if ran = '0' then
					clkr <= '1';--set global clock to 0
				else
					clkr <= '0';
				end if;
				if failed = '1' and fail_count /= x"FFFFFFFF" then
					fail_count <= fail_count + x"00000001";
				else
					fail_count <= fail_count;
				end if;
			end if;
			
			if writing = '1' then
				if (werr(to_integer(addr(15 downto 9))) = '1')
						and (addr(8 downto 0) /= '1' & x"FF") then
					--send error response when trying to overwrite unread packet
					berr <= '1';
					wr_en <= '0';
					dtack <= '0';
				else
					berr <= '0';
					wr_en <= strobe and not dtack;
					dtack <= strobe;
					if strobe = '1' and addr = x"7FFFFFFF" then
						impatience(32) <= not impatience(32);--safety check
						impatience(31 downto 0) <= data_inbound;
					end if;
				end if;
			else
				berr <= '0';
				wr_en <= '0';
				dtack <= strobe;
			end if;
		end if;
	end process;
end Behavioral;
