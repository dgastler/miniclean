#include "FakeWFD.h"


int FakeWFD::FillTriggerTimeTag(time_t triggerTime)
{
  //Check that data vector exists and has allocated enough memory
  int dataSize = data.size();
  if (dataSize < 3)
    {
      cerr << "Memory not allocated for triggerTime" << endl;
      return -1;
    }

  //Clear memory in data vector for the triggerTime
  data[3] = data[3] & 0x0;
  //Fill memory in data vector with triggerTime
  data[3] = data[3] | triggerTime;
  return 0;
}

int FakeWFD::FillEventCounter(uint32_t eventCounter)
{
  //Check that data vector exists and has allocated enough memory
  int dataSize = data.size();
  if (dataSize < 2)
    {
      cerr << "Memory not allocated for eventCounter" << endl;
      return -1;
    }

  //Clear memory in data vector for the eventCounter
  data[2] = data[2] & 0xff000000;
  //Clear unused bits in eventCounter
  eventCounter = eventCounter & 0x00ffffff;
  //Fill memory in data vector with eventCounter
  data[2] = data[2] | eventCounter;
  return 0;
}

int FakeWFD::FillChannelMask()
{
  //Check that data vector exists and has allocated enough memory
  int dataSize = data.size();
  if (dataSize < 1)
    {
      cerr << "Memory not allocated for channelMask" << endl;
      return -1;
    }

  //channelMask starts out all 1s
  uint8_t channelMask = 0xff;
  //Get the number of PMTs from PMTID
  int numberOfPMTs = PMTID.size();

  /*The difference between the total 8 bits and the number of PMTs   |
  | will be the number of bitshifts that will leave channelMask with |
  | the right number of bits set to 1.                              */
  int bitShift = 8 - numberOfPMTs;
  channelMask >> bitShift;

  //Clear memory in data vector for channelMask
  data[1] = data[1] & 0xffffff00;
  //Clear unused bits in channelMask
  channelMask = ChannelMask & 0x000000ff;
  //Fill memory in data vector with channelMask
  data[1] = data[1] | ChannelMask;
  return 0;
}

int FakeWFD::BoardID(uint32_t boardID)
{
  //Check that data vector exists and has allocated enough memory
  int dataSize = data.size();
  if (dataSize < 1)
    {
      cerr << "Memory not allocated for boardID" << endl;
      return -1;
    }

  //Clear memory in data vector for boardID
  data[1] = data[1] & 0x07ffffff;
  //Clear unused bits in boardID
  boardID = boardID & 0x0000001f;
  //Bitshift boardID 27 bits moving used bits to bits 27-31
  boardID << 27;
  //Reclear unused bits to make sure they are 0
  boardID = boardID & 0xf8000000;
  //Fill memory in data vector with boardID
  data[1] = data[1] | boardID; 
  return 0;
}

int FakeWFD::FillEventSize()
{
  //Check that data vector exists and has allocated enough memory
  int dataSize = data.size();
  if (dataSize < 0)
    {
      cerr << "Memory not allocated for eventSize" << endl;
      return -1;
    }

  //Get evenSize
  uint32_t eventSize = data.size();

  //Clear memory in data vector for eventSize
  data[0] = data[0] & 0xf0000000;
  //Clear unused bits in eventSize
  eventSize = eventSize & 0x0fffffff;
  //Fill memory in data vector with eventSize
  data[0] = data[0] | eventSize;  
  return 0;
}

int FakeWFD::FillHeaderWord()
{
  //Check that data vector exists and has allocated enough memory
  int dataSize = data.size();
  if (dataSize < 0)
    {
      cerr << "Memory not allocated for headerWord" << endl;
      return -1;
    }

  //Create header Word starting with 1010
  uint32_t headerWord = 0xa0000000;
  //Clear memory in data vector for headerWord
  data[0] = data[0] & 0x0fffffff;
  //Fill memory in data vector with headerWord
  data[0] = data[0] | headerWord;
  return 0;
}

int FakeWFD::Process_25_Packing(RAT::DS::RawWaveform * rawWave)
{
  uint64_t dataBlock = 0;
  int wordsAdded = 0;
  const std::vector<uint16_t> sample = rawWave->GetSamples();  

  //2.5 packed data should always have multiples of five samples.
  if((sample.size() % 5) != 0)
    return(-1);
  else if(sample.size() < 5)
    return(-1);
  
  for(unsigned int iBlock = 5; iBlock <= sample.size();Block+=5)
    {
      dataBlock = 0;
      
      //Add this blocks 5th sample
      dataBlock |= uint64_t(sample[iBlock-1])   & 0x0FFF;
      //Shift it up 12 bits (now the last 12 bits of dataBlock are 0)
      dataBlock = dataBlock << 12;
      
      //Add the 4th sample
      dataBlock |= uint64_t(sample[iBlock-2]) & 0xFFF; 
      //Shift it up 6 bits (now the last 6 bits of dataBlock are 0)
      dataBlock = dataBlock << 6;
      
      //Add the 6 MSBs of the 3rd sample 
      dataBlock |= uint64_t( sample[iBlock-3] >> 6)&0x3F;
      //Shift it up 8 bits (now the last 8 bits are 0, the MS 2 are for a word header)
      dataBlock = dataBlock << 8;
      //Add the 6 LSBs of the 3rd sample
      dataBlock |= uint64_t( sample[iBlock-3] ) &0x3F;
      //Shift it up by 12 bits. (now 12 bits are zero)
      dataBlock = dataBlock << 12;
      
      //Add the 2nd sample
      dataBlock |= uint64_t ( sample[iBlock-4] )&0xFFF;
      //Shift it up by 12 bits.
      dataBlock = dataBlock << 12;
      
      //Add the 1st sample
      dataBlock |= uint64_t (sample[iBlock-5]) & 0xFFF;
      
      //Put the block into the 32bit data vector by breaking the 64bit into 2 32bits
      data.push_back( ((uint32_t *) &dataBlock)[0] );
      data.push_back( ((uint32_t *) &dataBlock)[1] );
      
      wordsAdded += 2;
    }
  return(wordsAdded);
}

int FakeWFD::Process_20_Packing(RAT::DS::RawWaveform * rawWave)
{
  uint32_t dataBlock = 0;
  int wordsAdded = 0;
  const std::vector<uint16_t> sample = rawWave->GetSamples();  

  //2 packed data should always have multiples of two samples.
  if((sample.size() % 2) != 0)
    return(-1);
  else if(sample.size() < 2)
    return(-1);
  
  for(unsigned int iBlock = 2; iBlock <= sample.size();Block+=2)
    {
      dataBlock = 0;
      
      //Add the second sample in this block to dataBlock.
      dataBlock |= uint32_t(sample[iBlock-1])&0xFFF;
      //Shift it by 16 bits
      dataBlock = dataBlock << 16;

      //Add the first samle in this block to dataBlock.
      dataBlock |= uint32_(sample[iBlock-2])&0xFFF;
      
      //Add the block to the data vector.
      data.push_back(dataBlock);

      wordsAdded++;
    }
  return(wordsAdded);
}

int FakeWFD::ProcessWaveforms(RAT::DS::Root *ds)
{
  dataFormat = 0; //Set data format to be invalid
  for(unsigned int iPMTID = 0; iPMTID < PMTID.size();iPMTID++)
    {
      RAT::DS::PMT * pmt = NULL;

      //Find current PMT
      pmt = FindPMT(ds,PMTID[iPMT]);
      if(pmt == NULL)
	return(-2);
      
      if(iPMTID == 0)
	{
	  //Set dataFormat;
	  switch (pmt->GetRaw()->GetFormat())
	    {
	    case RAT::DS::RAW::CAENWFD_FULL_FORMAT_2PACK :
	      dataFormat = 0x4;
	      break;
	    case RAT::DS::RAW::CAENWFD_FULL_FORMAT_25PACK :
	      dataFormat = 0x6;
	      break;
	    case RAT::DS::RAW::CAENWFD_ZLE_FORMAT_2PACK :
	      dataFormat = 0x5;
	      break;
	    case RAT::DS::RAW::CAENWFD_ZLE_FORMAT_25PACK :
	      dataFormat = 0x7;
	      break;
	    default:
	      dataFormat = 0;
	      break;
	    }
	  
	}

      switch (pmt->GetRaw()->GetFormat())
	{
	case RAT::DS::RAW::CAENWFD_FULL_FORMAT_2PACK :
	  //Only one RawWaveform per PMT
	  if(pmt->GetRaw()->GetRawCount() == 1)
	    Process_20_Packing(pmt->GetRaw()->GetRawWaveform(0));
	  else
	    return(-1);
	  break;
	case RAT::DS::RAW::CAENWFD_FULL_FORMAT_25PACK :
	  //Only one RawWaveform per PMT
	  if(pmt->GetRaw()->GetRawCount() == 1)
	    Process_25_Packing(pmt->GetRaw()->GetRawWaveform(0));
	  else
	    return(-1);
	  break;
	case RAT::DS::RAW::CAENWFD_ZLE_FORMAT_2PACK :
	  ProcessZLEWindows(pmt,2);
	  break;		   
	case RAT::DS::RAW::CAENWFD_ZLE_FORMAT_25PACK :
	  ProcessZLEWindows(pmt,5);
	  break;
	default:
	  return(-1);
	  break;
	}
    }
}

int ProcessZLEWindows(RAT::DS::PMT * pmt,int samplesPer64BitWord)
{
  if((samplesPer64BitWord != 2)&&(samplesPer64BitWord != 5))
    {      
      return(-2);
    }
  unsigned int channelSizeWordPosition = data.size(); //We will fill this channels size here.
  data.push_back(0); //Size;
  uint32_t timeOffset = 0;
  //Loop over all the ZLE windows in this waveform.
  for(int iRawWaveform = 0; iRawWaveform < pmt->GetRaw()->GetRawCount();iRawWaveform++)
    {
      //Get the next raw window.
      RAT::DS::RawWaveform * rawWaveform = pmt->GetRaw()->GetRawWaveform(iRawWaveform);
      
      //Calculate the number of skipped samples since the last window end.
      timeOffset = rawWaveform->GetTimeOffset()*(2.0/simplesPer64BitWord) - timeOffset;	      
      //Add in the skipped words word.
      data.push_back( (timeOffset&0x3FFFFFFF) | 0x80000000 );   // Add skip header to word
      
      //Add in a window size word and save it's position.   Save for after we fill the data.
      unsigned int windowWordPosition = data.size();
      data.push_back(0);

      unsigned int windowSize = -1;
      switch (samplesPer64BitWord)
	{
	case 2:
	  windowSize = Process_20_Packing(rawWaveform);
	  break;
	case 5:
	  windowSize = Process_25_Packing(rawWaveform);
	  break;
	default:
	  return(-3);
	  break;
	}

      if(windowSize < 0)
	return(-1);
      //Set this window's size word
      data[windowWordPosition] = windowSize;
      //Move the timeOffset variable to be the end of this window
      timeOffset += windowSize;
    }
  //Set the channel's size word
  data[channelSizeWordPosition] = data.size() - channelSizeWordPosition;
}


RAT::DS::PMT * FindPMT(RAT::DS::Root * ds,int id)
{
  //DO SOMETHING SMARTER!!!!!!!!!!
  RAT::DS::EV * ev = ds->GetEV(0);
  for(int iPMT = 0; iPMT < ev->GetPMTCount();iPMT++)
    {
      if(ev->GetPMT(iPMT)->GetID() == id)
	return(ev->GetPMT(iPMT));
    }
  return(NULL);
}

int FillDataFormat(uint32_t dataFormat)
{
}

int FakeWFD::Process(RAT::DS::Root * ds)
{
  validData = false;
  data.clear();

  //Pushback data vector fourtimes for memory for header
  for (int iheader = 0; iheader < 4; iheader++)
    data.push_back(0x0);
  int dataSize() = data.size();
  if (dataSize < 4)
    {
      cerr << "Yo dawg, there's something wrong." << endl;
      return -1;
    }

  //Fill data vector with event data
  int WaveformsOK = ProcessWaveforms(RAT::DS::Root * ds);
  if (WaveformsOK < 0)
    {
      cerr << "Waveforms not Filled to data correctly" << endl;
      return -1;
    }
  
  //Get Header information to fill to data vector
  RAT::DS::EV * ev = ds->GetEV(0);
  time_t triggerTime = ev->GetUTC();
  uint32_t eventCounter = ev->GetEventID();

  //Fill data vector with header information
  FillHeaderWord();
  FillChannelMask();
  FillEventSize();
  FillBoardID();
  FillTriggeTime(triggerTime);
  FillEventCounter(eventCounter);

  validData = true;
  return 0;
}

  
