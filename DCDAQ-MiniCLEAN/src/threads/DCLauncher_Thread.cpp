#include <DCLauncher.h>


//Threads

//Control processor
#include <Control.h>
//DCMessageServer
#include <DCMessageServer.h>
//DCMessageClient
#include <DCMessageClient.h>
//MemBlock Throw Away processor
#include <MemBlockThrowAway.h>
//MemBlock Writer
#include <MemBlockWriter.h>
//MemBlock Reader
#include <MemBlockReader.h>
//MemBlock Compressor
#include <MemBlockCompressor.h>
//MemBlock DeCompressor
#include <MemBlockDeCompressor.h>
//V1720 readout processor
#include <v1720Readout.h>
//V;1720 Fake trig processor
#include <v1720FakeTrigReadout.h>
//V1720 readout processor
#include <v1720FakeReadout.h>
//PrintServer
#include <PrintServer.h>
//MySQLInterface
#include <MySQLInterface.h>
//RunControl
#include <RunControl.h>

//Threads that require RAT
#ifdef __RATDS__
//RAT write processor
#include <RATWriter.h>
//FEPC Assembler
#include <FEPCAssembler.h>
//Test bench Assembler
#include <BenchAssembler.h>
//DRPusher
#include <DRPusher.h>
//DRPCAssembler
#include <DRPCAssembler.h>
//DRFakeTriggerAssembler
#include <DRFakeTriggerAssembler.h>
//RAT Throw away
#include <RATThrowAway.h>
//DecisionMaker
#include <DecisionMaker.h>
//FEPusher
#include <FEPusher.h>
//DRGetter
#include <DRGetter.h>
//EBPusher
#include <EBPusher.h>
//EBPCAssembler
#include <EBPCAssembler.h>
//EBPCAssemblerFEPCmult
//#include <EBPCAssemblerFEPCMult.h>
//FEPCBadEvent
#include <FEPCBadEvent.h>
//DRPusherThrowAway
#include <DRPusherThrowAway.h>
//DRPCBadEvent
#include <DRPCBadEvent.h>
//EBPCBadEvent
#include <EBPCBadEvent.h>
//PackData
#include <PackData.h>
//UnPackData
#include <UnPackData.h>
//DRTriggerAssembler
#include <DRTriggerAssembler.h>
//TObjPrinter
#include <TObjPrinter.h>
//Run Convert data
#include <ConvertController.h>
//DQMSender
#include <DQMSender.h>
//DQMServer
#include <DQMServer.h>
#endif


//=============================================================================
//=============================================================================
uint64_t DCLauncher::SetupThreads(xmlNode * baseNode,
				  std::vector<DCThread*> &tempThread,
				  std::vector<MemoryManager*> &tempMM
				  )
{
  //Check how many mangers we are suppose to set up.
  uint32_t ThreadCount = NumberOfNamedSubNodes(baseNode,"THREAD");
  //Fail if it os over 48.
  if(ThreadCount > 48)
    {
      return(THREAD_TOO_MANY);
    }

  uint64_t ret = 0;
  std::string errName("SetupThreads");
  //Loop over all THREAD nodes
  for(uint32_t node = 0; node < ThreadCount;node++)
    {
      xmlNode* threadNode = FindIthSubNode(baseNode,"THREAD",node);
      if(threadNode) // IF the pointer isn't NULL.
	{	  
	  //Check if the all the requested memory blocks have been created
	  bool memoryIsAllocated = true;
	  for(unsigned int iMMName = 0; iMMName < NumberOfNamedSubNodes(threadNode,"MEMORYMANAGER");iMMName++)
	    {
	      std::string tempString("");
	      //Get the name of the current MM
	      GetIthXMLValue(threadNode,iMMName,"MEMORYMANAGER",errName.c_str(),tempString);
	      //Search the two memory manager maps for this name
	      bool found = false;
	      //Searth the new tempMM vector
	      for(size_t i = 0;((i < tempMM.size())&&!found);i++)
		{
		  if(boost::algorithm::iequals(tempMM[i]->GetName(),tempString))
		    found = true;
		}
	      //search the existing MM vector
	      for(size_t i = 0;((i < memoryManager.size())&&!found);i++)
		{
		  if(boost::algorithm::iequals(memoryManager[i]->GetName(),tempString))
		    found = true;
		}
	      //Check that one of the maps had the MM we were looking for.
	      if(!found)
		{
		  ret = THREAD_MISSING_MM;
		  ret |= 0x1<<node;
		  memoryIsAllocated = false;
		  fprintf(stderr,"Error: Memory manager %s not found\n",tempString.c_str());
		  break;
		}
	    }
	  //If the memory managers are loaded for this thread
	  if(memoryIsAllocated)
	    {

	      //Get the name and type for this thread
	      std::string threadName("");
	      std::string threadType("");
	      if(FindSubNode(threadNode,"NAME") == NULL)
		{
		  ret = THREAD_MISSING_NAME;
		  ret |= 0x1<<node;
		  break;
		}
	      else
		GetXMLValue(threadNode,"NAME",errName.c_str(),threadName);
	      if(FindSubNode(threadNode,"TYPE") == NULL)
		{
		  ret = THREAD_MISSING_TYPE;
		  ret |= 0x1<<node;
		  break;
		}
	      else
		GetXMLValue(threadNode,"TYPE",errName.c_str(),threadType);


	      bool KnownThread = true;
	      //Using "TYPE", allocate a new thread
	      //KnownThread will be false if we get to the end of this if-else struct
	      if(boost::iequals(threadType,"MemBlockThrowAway"))
		{
		  tempThread.push_back(new MemBlockThrowAway(threadName));
		}
	      else if(boost::iequals(threadType,"MemBlockWriter"))
		{
		  tempThread.push_back(new MemBlockWriter(threadName));
		}
	      else if(boost::iequals(threadType,"MemBlockReader"))
		{
		  tempThread.push_back(new MemBlockReader(threadName));
		}
	      else if(boost::iequals(threadType,"MemBlockCompressor"))
		{
		  tempThread.push_back(new MemBlockCompressor(threadName));
		}
	      else if(boost::iequals(threadType,"MemBlockDeCompressor"))
		{
		  tempThread.push_back(new MemBlockDeCompressor(threadName));
		}
	      else if(boost::iequals(threadType,"Control"))
		{
		  tempThread.push_back(new Control(threadName));
		}
	      else if(boost::iequals(threadType,"DCMessageServer"))
		{
		  tempThread.push_back(new DCMessageServer(threadName));
		}
	      else if(boost::iequals(threadType,"DCMessageClient"))
		{
		  tempThread.push_back(new DCMessageClient(threadName));
		}
	      else if(boost::iequals(threadType,"V1720Reader"))
		{
		  tempThread.push_back(new v1720Readout(threadName));
		}
	      else if(boost::iequals(threadType,"V1720FakeReader"))
		{
		  tempThread.push_back(new v1720FakeReadout(threadName));
		}
	      else if(boost::iequals(threadType,"V1720FakeTrigReader"))
		{
		  tempThread.push_back(new v1720FakeTrigReadout(threadName));
		}
	      else if(boost::iequals(threadType,"PrintServer"))
		{
		  tempThread.push_back(new PrintServer(threadName));
		}		
	      else if(boost::iequals(threadType,"MySQLInterface"))
		{
		  tempThread.push_back(new MySQLInterface(threadName));
		}	
	      else if(boost::iequals(threadType,"RunControl"))
		{
		  tempThread.push_back(new RunControl(threadName));
		}
	      //Threads that require RATDS
#ifdef __RATDS__
	      else if(boost::iequals(threadType,"TObjPrinter"))
		{
		  tempThread.push_back(new TObjPrinter(threadName));
		}
	      else if(boost::iequals(threadType,"DRPusherThrowAway"))
		{
		  tempThread.push_back(new DRPusherThrowAway(threadName));
		}
	      else if(boost::iequals(threadType,"EBPCAssembler"))
		{
		  tempThread.push_back(new EBPCAssembler(threadName));
		}
//	      else if(boost::iequals(threadType,"EBPCAssemblerFEPCMult"))
//		{
//		  tempThread.push_back(new EBPCAssemblerFEPCMult(threadName));
//		}
	      else if(boost::iequals(threadType,"RATWriter"))
		{
		  tempThread.push_back(new RATWriter(threadName));
		}
	      else if(boost::iequals(threadType,"FEPCBadEvent"))
		{
		  tempThread.push_back(new FEPCBadEvent(threadName));
		}	
	      else if(boost::iequals(threadType,"EBPusher"))
		{
		  tempThread.push_back(new EBPusher(threadName));
		}	      
	      else if(boost::iequals(threadType,"DecisionMaker"))
		{
		  tempThread.push_back(new DecisionMaker(threadName));
		}
	      else if(boost::iequals(threadType,"DRGetter"))
		{
		  tempThread.push_back(new DRGetter(threadName));
		}	  
	      else if(boost::iequals(threadType,"FEPusher"))
		{
		  tempThread.push_back(new FEPusher(threadName));
		}	         
	      else if(boost::iequals(threadType,"RATThrowAway"))
		{
		  tempThread.push_back(new RATThrowAway(threadName));
		}
	      else if(boost::iequals(threadType,"FEPCAssembler"))
		{
		  tempThread.push_back(new FEPCAssembler(threadName));
		}
	      else if(boost::iequals(threadType,"BenchAssembler"))
		{
		  tempThread.push_back(new BenchAssembler(threadName));
		}
	      else if(boost::iequals(threadType,"DRPCAssembler"))
		{
		  tempThread.push_back(new DRPCAssembler(threadName));
		}
	      else if(boost::iequals(threadType, "DRFakeTriggerAssembler"))
		{
		  tempThread.push_back(new DRFakeTriggerAssembler(threadName));
		}
	      else if(boost::iequals(threadType,"DRPusher"))
		{
		  tempThread.push_back(new DRPusher(threadName));
		}
	      else if(boost::iequals(threadType,"DRPCBadEvent"))
		{
		  tempThread.push_back(new DRPCBadEvent(threadName));
		}
	      else if(boost::iequals(threadType,"EBPCBadEvent"))
		{
		  tempThread.push_back(new EBPCBadEvent(threadName));
		}
	      else if(boost::iequals(threadType,"DRTriggerAssembler"))
		{
		  tempThread.push_back(new DRTriggerAssembler(threadName));
		}
	      else if(boost::iequals(threadType,"PackData"))
		{
		  tempThread.push_back(new PackData(threadName));
		}
	       else if(boost::iequals(threadType,"UnPackData"))
		{
		  tempThread.push_back(new UnPackData(threadName));
		}
	       else if(boost::iequals(threadType,"ConvertController"))
		{
		  tempThread.push_back(new ConvertController(threadName));
		}
	       else if(boost::iequals(threadType,"DQMSender"))
		 {
		   tempThread.push_back(new DQMSender(threadName));
		 }
	       else if(boost::iequals(threadType,"DQMServer"))
		 {
		   tempThread.push_back(new DQMServer(threadName));
		 }
#endif
	      else
		{
		  KnownThread = false;
		}
	      
	      //Setup the new thread if it exists
	      if(!KnownThread) //thread type unknown
		{
		  printf("Thread node %d unknown: %s\n",node,threadType.c_str());
		  ret = THREAD_UNKNOWN;
		  ret |= 0x1<<node;
		  break;
		}
	      else //Thread type known
		{
		  //To set up a thread we need to send it a memory manger map.
		  //Since we are in the process of setting things up, the 
		  //MMs are in two vectors, ones that have been set up fine
		  //and another with the MMs we have set up in this setup call.
		  //We need to make a temporary map that merges them for
		  //the thread setup function.
		  std::vector<MemoryManager*> tempMemoryMap;
		  //Add the existing memoryManager entries
		  tempMemoryMap.insert(tempMemoryMap.end(),
				       memoryManager.begin(),
				       memoryManager.end());		  
		  //Add the tempMM entries
		  tempMemoryMap.insert(tempMemoryMap.end(),
				       tempMM.begin(),
				       tempMM.end());
		  //Run the thread setup function and check for fail
		  printf("Setting up node %d of %d: %s(%s): \n",
			 node + 1,
			 ThreadCount,
			 tempThread.back()->GetName().c_str(),
			 tempThread.back()->GetType().c_str());
		  if(!(tempThread.back())->Setup(threadNode,
					     tempMemoryMap))
		    {
		      printf(" Failed\n");
		      ret = THREAD_SETUP;
		      ret |= 0x1<<node;
		      break;

		    }
		  else
		    {
		      printf(" Succeeded\n");
		      //setup the select error handler
		      (tempThread.back())->SetupSelectError(&selectErrorIgnore);
		      tempThread.back()->Start(NULL);
		    }
		}
	    }
	}
      else
	{
	  fprintf(stderr,"Bad thread node # %d\n",node);	 
	  ret = THREAD_BAD_NODE;
	  ret |= 0x1<<node;
	  break;
	}
    }  
  return(ret);
}
