#include <XML_Factory.h>

XML_Factory::XML_Factory(DCMySQL * conn)
{
  DCMySQLConnection = conn;
}

XML_Factory::~XML_Factory()
{
  CleanUp();
}

void XML_Factory::CleanUp()
{
  while(threads.size()>0)
    {
      delete threads.back();
      threads.pop_back();
    }
}

std::string XML_Factory::LoadRun(int _runNumber)
{  
  bool error = false;
  runNumber= _runNumber;
  
  //Initialize xml documents
  doc=xmlNewDoc(BAD_CAST "1.0");
  xmlNode * setupNode=xmlNewDocNode(doc,NULL,BAD_CAST "SETUP",NULL);
  xmlDocSetRootElement(doc,setupNode);

  std::stringstream query;

  //Add Memory managers
  xmlNode * memManagersNode = xmlNewChild(setupNode,NULL,(xmlChar *)"MEMORYMANAGERS",NULL);
  query << "SELECT pc_name,mm_type,mm_name FROM Memory_MemoryManagers WHERE pc_name='local' AND run_number=" 
	<< runNumber << " ORDER BY order_id";
  std::vector<std::vector<std::string> > mmList;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      error = true;
      return "MM ERROR";
    }
  DCMySQLConnection->GetTable(mmList);
  DCMySQLConnection->FreeResult();
  query.str(std::string());
  for(size_t iMM = 0; iMM < mmList.size();iMM++)
    {
      error |= !AddMemoryManager(memManagersNode,
				 mmList[iMM][0],
				 mmList[iMM][1],
				 mmList[iMM][2]);
      if(error)
	{
	  printf("XML_Factory Error at %s:%s:%s\n",
		 mmList[iMM][0].c_str(),
		 mmList[iMM][1].c_str(),
		 mmList[iMM][2].c_str());
	  break;
	}
    }

  //Add threads
  xmlNode * threadsNode = xmlNewChild(setupNode,NULL,(xmlChar *)"THREADS",NULL);
  query << "SELECT pc_name,thread_type,thread_name FROM Thread_Threads WHERE pc_name='local' AND run_number=" 
	<< runNumber << " ORDER BY order_id";
  std::vector<std::vector<std::string> > threadList;
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      error = true;
      return "THREAD ERROR";
    }
  DCMySQLConnection->GetTable(threadList);
  DCMySQLConnection->FreeResult();
  query.str(std::string());
  for(size_t iThread = 0; iThread < threadList.size();iThread++)
    {
      error |= !AddThread(threadsNode,
			  threadList[iThread][0],
			  threadList[iThread][1],
			  threadList[iThread][2]);
      if(error)
	{
	  printf("XML_Factory Error at %s:%s:%s\n",
		 threadList[iThread][0].c_str(),
		 threadList[iThread][1].c_str(),
		 threadList[iThread][2].c_str());
	  break;
	}
    }
  
  if(error)
    {
      return "ERROR";
    }
  //lots of copying
  char * string; 
  int size;
  xmlDocDumpFormatMemory(doc,(xmlChar**)&string,&size,1);
  std::string ret(string);

  return ret;
}

  
bool XML_Factory::AddThread(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responsesNode)
{
  Thread_XML * threadPtr = NULL;
  //Create the appropriate class for this thread type
  if(boost::algorithm::iequals(threadType,"RunControl"))
    {
      threadPtr=(Thread_XML *) new RunControl_XML(DCMySQLConnection,runNumber,this);
    }
  else if(boost::algorithm::iequals(threadType,"DecisionMaker"))
    {
      threadPtr=(Thread_XML *) new DecisionMaker_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"V1720READER"))
    {
      threadPtr=(Thread_XML *) new V1720_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"PackData"))
    {
      threadPtr=(Thread_XML *) new CompressData_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"MemBlockCompressor"))
    {
      threadPtr=(Thread_XML *) new CompressData_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"DRGetter"))
    {
      threadPtr=(Thread_XML *) new NetThread_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"DRPusher"))
    {
      threadPtr=(Thread_XML *) new NetThread_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"EBPusher"))
    {
      threadPtr=(Thread_XML *) new NetThread_XML(DCMySQLConnection,runNumber);
    }
  else if(boost::algorithm::iequals(threadType,"DCMessageClient"))
    {
      threadPtr=(Thread_XML *) new NetThread_XML(DCMySQLConnection,runNumber);
    }
  else
    {
      threadPtr=(Thread_XML *) new Thread_XML(DCMySQLConnection,runNumber);
    }

  
  bool ret;
  if(threadPtr != NULL)
    {
      printf("Building XML for: %s:%s:%s\n",
	     pcName.c_str(),
	     threadType.c_str(),
	     threadName.c_str());

      threads.push_back(threadPtr);
      ret = threads.back()->Generate_XML(threadsNode,
					 pcName,
					 threadType,
					 threadName,
					 responsesNode);
      if(!ret)
	{
	  delete threadPtr;
	  threads.pop_back();
	}
    }
  else
    {
      ret = false;
    }
  return ret;
}


bool XML_Factory::AddMemoryManager(xmlNode * mmNode,
				   std::string pcName,
				   std::string mmType,
				   std::string mmName)
{
  MemoryManager_XML * mmPtr = NULL;
  mmPtr =(MemoryManager_XML *) new MemoryManager_XML(DCMySQLConnection,
						     runNumber);
  bool ret;
  if(mmPtr != NULL)
    {
      memoryManagers.push_back(mmPtr);
      ret = memoryManagers.back()->Generate_XML(mmNode,
						pcName,
						mmType,
						mmName);
    }
  else
    {
      ret = false;
    }
  return ret;
}


