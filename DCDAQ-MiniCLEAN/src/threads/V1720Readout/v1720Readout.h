#ifndef __V1720READOUT__
#define __V1720READOUT__

#include <map>
#include <string>

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <v1720WFD.h>
#include <xmlHelper.h>
#include <StatMessage.h>
#include <iostream>
#include <iomanip>

#ifdef __CAENLIBS__

class v1720Readout : public DCThread 
{
 public:
  

  v1720Readout(std::string Name);
  ~v1720Readout();
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  uint32_t MinimumFreeBufferSpace;
  uint32_t MaxReadoutsPerBlock;

  //Statistics
  std::vector<uint32_t> TimeStep_ReadoutNumber;

  std::vector<uint32_t> TimeStep_ReadoutSize;  

  std::vector<uint32_t> TimeStep_Occupancy;  

  std::vector<uint32_t> TimeStep_EventCount;

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message & message);
  //Handle select timeout
  virtual void ProcessTimeout();
  
  //Memory block manager
  MemoryBlockManager * Mem; 
  MemoryBlock * block;
  int32_t blockIndex;
  
  //Setup data
  std::string WFDSetup; //Strings containing the WFD XML config strings
  xmlDoc * XMLDoc; //xml documents created from the XML strings
  bool StartWFDs();
  bool StopWFDs();


  //Time values
  time_t currentTime;
  time_t lastTime;

  //WFD vectors.
  std::vector<xmlNode*> BaseXMLNodes; //base XML node of the XML Doc
  std::vector<v1720*> WFD; //Vector of v1720WFD classes
};









#else  //CAENLIBS
//Stand in for V1720Readout class for when the caen libraries are not present. 
class v1720Readout : public DCThread 
{
 public:
  v1720Readout(std::string Name){};
  ~v1720Readout(){};
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager)
  {
    fprintf(stderr,"CAENLIBS not included. FATAL ERROR!\n");
    return false;
  };
};

#endif
#endif
