#ifndef __CONNECTION__
#define __CONNECTION__

#include <string>
#include <DCNetThread.h>

//Structure to hold connection info
typedef struct Connection
{
  std::string    LocalIP;
  std::string    RemoteIP;
  int32_t        SocketFamily;
  uint32_t       Port;
  //Cast as what you need ie struct sockaddr_in, struct sockaddr_in6, struct sockaddr_un based on SocketFamily
  std::vector<uint8_t> addrLocal;
  //struct sockaddr_in addrLocal;
  std::vector<uint8_t> addrRemote;
  //  struct sockaddr_in addrRemote;
  int fdSocket;
  DCNetThread * Thread;
  int OutPacketManagerFDs[2];
  int InPacketManagerFDs[2];
  int retryCountDown;
  bool retry;
  Connection()
  {
    Clear();
  };
  void Clear()
  { 
    LocalIP  = "";
    RemoteIP = "";
    SocketFamily = -1;
    Port = 0;
    addrLocal.clear();
    addrRemote.clear();
    fdSocket = -1;
    Thread = NULL;
    OutPacketManagerFDs[0] = BadPipeFD;
    OutPacketManagerFDs[1] = BadPipeFD;
    InPacketManagerFDs[0]  = BadPipeFD;
    InPacketManagerFDs[1]  = BadPipeFD;
    retryCountDown=2;
    retry = false;
  };
  //SetRetry() and Retry() are used to wait a DCThread timeout
  //before trying another connection.  The countdown is 2 to 
  //account for the checking of ProcessTimeout() after calling
  //MainLoop() in DCThread
  void SetRetry(int countdown = 2)
  {
    retryCountDown = countdown;
    retry = true;
  }
  bool Retry()
  {
    if(retry)
      {
	//If our retry countdown has gotten to zero return true
	if(retryCountDown <= 0)
	  {
	    retry = false;
	    return(true);
	  }
	//If we are above zero decrimate countdown and return false;
	else
	  retryCountDown--;
      }    
    return(false);
  };
} Connection;

#endif
