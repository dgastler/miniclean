#include <iostream>
#include <TDirectory.h>
#include <TProcessID.h>

#include "PEvent.h"


using namespace std;


ClassImp(PEvent)


//______________________________________________________________________________
  PEvent::PEvent() : fIsValid(kFALSE)
{
  fIsValid = kTRUE;

  AbvTHRS = false;

  iEventNumber = 0;
  
  fSPE = 0;
  ffFast = 0;
  ffSlow = 0;
  fTOF = 0;
  fShift = 0;
  fDelay = 0;
  fMaxHeight = 0;

  SizeSPE = 0;
  SizefFast = 0;
  SizefSlow = 0;
  SizeTOF   = 0;
  SizeShift = 0;
  SizeDelay = 0;
  SizeMaxHeight = 0;
}
//______________________________________________________________________________
  
PEvent::~PEvent()
{
  Clear("C");
}

//______________________________________________________________________________

void PEvent::Clear(Option_t *option)
{
  fIsValid = kFALSE;
 
  AbvTHRS = false;

  iEventNumber = 0;

  fSPE = 0;
  ffFast = 0;
  ffSlow = 0;
  fTOF = 0;
  fShift = 0;
  fDelay = 0;
  fMaxHeight = 0;
  
  SizeSPE = 0;
  SizefFast = 0;
  SizefSlow = 0;
  SizeTOF = 0;
  SizeShift = 0;
  SizeDelay = 0;
  SizeMaxHeight = 0;
}

//______________________________________________________________________________


void PEvent::Build(Float_t * Array1,Float_t* Array2, Float_t* Array3,Float_t* Array4, Float_t* Array5, Float_t* Array6,Float_t* Array7,Int_t Size)
{
  fIsValid = true;

  fSPE = Array1;
  ffFast = Array2;
  ffSlow = Array3;
  fTOF = Array4;
  fShift = Array5;
  fDelay = Array6;
  fMaxHeight = Array7;

  SizeSPE = Size;
  SizefFast = Size;
  SizefSlow = Size;
  SizeTOF = Size;
  SizeShift = Size;  
  SizeDelay = Size;
  SizeMaxHeight = Size;
}
//______________________________________________________________________________
