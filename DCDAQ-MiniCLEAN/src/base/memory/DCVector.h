#ifndef __DCVECTOR__
#define __DCVECTOR__

#include <pthread.h>

#include <DCVectorEntry.h>

#include <LockedObject.h>
#include <NotLockedObject.h>

#include <EventNumberHash.h>
#include <EventTimeGroup.h>

#include <vector>

#include <StackTracer.h>

#include <iostream>
#include <sstream>
#include <string>

namespace returnDCVector
{
  //Return value
  enum 
    {
      COLLISION       = 0LL,
      OK              = -1LL,
      EMPTY_KEY       = -2LL,
      BAD_PASSWORD    = -3LL,
      BAD_MUTEX       = -4LL,
      EXTERNAL        = -5LL,
      NO_FREE         = -6LL,
      OTHER           = -7LL
    };
}

//==========================================================================
//==========================================================================
//DCVector
//==========================================================================
//==========================================================================

template<class T ,
	 class ObjectModel = LockedObject,
	 class EventBuilderType = EventNumberHash<T> >
class DCVector : public EventBuilderType
{
public:
  //================================================================
  //Setup
  //================================================================
  //Setup(size,emptyValue,badValue):
  //  Input:
  //    size:       sets the size of the entry vector
  //    emptyValue: what will be returned for an empty slot 
  //    badValue:   or what will be returned if there is an error (badValue)
  //
  //================================================================
  DCVector()
  {
    blank_password = 0;
    //Setup our event builder with a pointer to the entry conatiner
    SetupEventBuilder(&entry);
  }
  ~DCVector()
  {
    Clear();
  };
  void ShutdownWait()
  {
    Lock.ShutdownLockWait();
  }

  void WakeUp()
  {
    Lock.WakeUpSignal();
  }
  void Setup(uint32_t size, T _emptyValue, T _badValue) 
  {
    if(!Lock.LockObject())
      {
	return;
      }

    //Set return value for no data.
    badValue = _badValue;
    //Set return value for locked data.
    emptyValue = _emptyValue;
    //Allocate a vector of entries
    DCVectorEntry<T> _entry(emptyValue,	  //empty value
			    returnDCVector::EMPTY_KEY,     //empty last key
			    blank_password);    //blank password value
    //Clear the entry vector before we fill it. 
    entry.clear();
    for(uint32_t i = 0; i < size;i++)
      {	
	entry.push_back(_entry);
      }
    
    //Zero the active entries
    ActiveEntries = 0;
    
    //Unlock
    Lock.UnlockObject();
  };

  //================================================================
  //Special value functions
  //================================================================
  //Size():            Returns vector size
  //
  //================================================================
  size_t GetSize(){return(entry.size());};
  const T GetEmptyValue(){return(emptyValue);};
  const T GetBadValue(){return(badValue);};
  size_t GetActiveEntryCount() {return ActiveEntries;};
  uint64_t GetBlankPassword(){return blank_password;};
  int64_t GetPasswordBlocks(int64_t key)
  {
    //Since this is just a read, we won't use a mutex.
    //Always assume that this value could change while you get it. 
    //Get the index value for our key
    uint32_t index = this->GetKeyIndex(key);    
    int64_t ret = entry[index].passwordBlocks;    
    return(ret);   
  }


  //================================================================
  //Public Interfaces
  //================================================================
  //GetEntry(key, &Entry, &password, wait):  
  //  Input:
  //    key(int64_t): The value that is put through the index function 
  //                   to determine which vector entry to use.
  //                  This is compared to the current stored key to
  //                   catch index-collisions. 
  //  Output:
  //    return(int64_t): OK        (-1): everything returned well
  //                     EMPTY_KEY (-2): if the Entry at the key was empty   
  //                     BAD_PASSWORD (-3): if the Entry was locked and you 
  //                                         didn't supply the correct password
  //                     BAD_MUTEX (-4): if the mutex didn't lock 
  //                                      (only when wait = false) 
  //                     >=0           : if this key's entry already has data, 
  //                                     it will return the previously used key
  //                                     This allows us to understand what 
  //                                     collisions may have happened. 
  //           
  //    Entry(Type):   If return is EMPTY_KEY, Type will be "emptyValue" 
  //                    (entry is now locked! password set)
  //                   If return is BAD_PASSWORD, Type will be "badValue"
  //                   If return is BAD_MUTEX, Type will be "badValue"
  //                   If return is >=0, this will return the Entry associated 
  //                    with the (old) key. (entry is now locked! password set)
  //                   If return is OK, this will return the Entry associated 
  //                    with key. (entry is now locked! password set)
  //                    
  //  In/Out:
  //    Password(unsigned int): As an input this sets the password you need to 
  //                             access the entry if you are not unlocking 
  //                             something the input isn't used. 
  //                            As an output it is set to the lock password if 
  //                             you checked out an unlocked Entry.  
  //
  //UpdateEntry(key, &Entry, &password, wait):  
  //  Input:
  //    key(int64_t): The value that is put through the index function
  //                   to determine which vector entry to use.
  //                  This is compared to the current stored key to
  //                   catch index-collisions. 
  //    Password(unsigned int): This is the password you need to access 
  //                             the entry if you are not unlocking something 
  //                             the input isn't used. 
  //
  //  Output:
  //    return(int64_t): OK           (-1): everything returned well
  //                     BAD_PASSWORD (-3): if the Entry was locked and you 
  //                                        didn't supply the correct password
  //                     BAD_MUTEX    (-4): if the mutex didn't lock 
  //                                        (only when wait = false) 
  //           
  //  In/Out:
  //    Entry(Type): As an input this is a reference to the Type you want to 
  //                 put into Entry
  //                 As an output it's set to badValue on return if Put worked.
  //                 It is unchanged if unlock failed.   
  // 
  //ClearEntry(key, password, wait):  
  //  Input: 
  //    key(uint64_t) Key to clear entry for
  //    password      Password to clear, if entry is locked. 
  //  Return:
  //    return(int64_t): OK        (-1): everything returned well
  //                     BAD_PASSWORD    (-3): if Entry was locked + you didn't
  //                                           supply the correct password
  //                     BAD_MUTEX (-4): if the mutex didn't lock  
  //                                     (only when wait = false) 
  //             
  //
  //================================================================
  int64_t GetEntry(int64_t key, T & _entry,uint64_t &password, bool wait = true)
  {    
#ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"DCVector::GetEntry(key=%zd,_entry=%d,password=%zX)\n",key,_entry,password);
#endif 
    if(!Lock.LockObject(wait))
      {
	_entry = badValue;
	return(returnDCVector::BAD_MUTEX);
      }    
    //We now have the mutex!    
    int64_t ret = returnDCVector::OK;

    //Get the entry at key if password is correct
    ret = _Get(key,_entry,password); 
    Lock.UnlockObject();     
    return(ret);
  };
  
  int64_t UpdateEntry(uint64_t key, T & _entry, uint64_t password, bool wait=true)
  {
#ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"DCVector::UpdateEntry(key=%zd,_entry=%d,password=%zX)\n",key,_entry,password);
#endif 
    if(!Lock.LockObject(wait))
      {
	return(returnDCVector::BAD_MUTEX);
      }

    //Return value
    int64_t ret = returnDCVector::OK;
    ret = _Update(key,_entry,password);
    Lock.UnlockObject();
    return(ret);
  }

  int64_t ClearEntry(int64_t key, uint64_t password,bool wait=true)
  {
#ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"DCVector::ClearEntry(key=%zd,password=%zX)\n",key,password);
#endif 
    if(!Lock.LockObject(wait))
      {
	return(returnDCVector::BAD_MUTEX);
      }

    //Return value
    int64_t ret = returnDCVector::OK;

    ret = _Update(key,emptyValue,password);
    
    Lock.UnlockObject();

    return(ret);
  };

void PrintDCVector(char * text)
{
  if(Lock.LockObject(true))
    {
      text += sprintf(text,"Printing DCVector\n");
      for(uint iEntry=0;iEntry<entry.size();iEntry++)
	{
	  text += sprintf(text,"Entry: %u\n",iEntry);
	  text += entry[iEntry].PrintEntry(text);
	}
      Lock.UnlockObject();
    }

}

  //================================================================
  //Public Interfaces (special)
  //================================================================
  //ClearPassword(key, password, wait)
  //  Input:
  //    key(int64_t): The value that is put through the index function             
  //                   to determine which vector entry to use.
  //                  This is compared to the current stored key to
  //                   catch index-collisions. 
  //    Password(unsigned int): This is the password you want need to access the entry
  //                             if you are not unlocking something the input isn't used. 
  //  Output:
  //    return(int64_t):  OK if unlock worked (key now unlocked)
  //                      BAD_PASSWORD if we gave the wrong password
  //                      BAD_MUTEX if we couldn't get the mutex lock
  //                      >= 0 if the lock is from another key.  (no unlock!)
  //
  //================================================================
  int64_t ForceGet(int64_t key, T & _entry,uint64_t &password,bool wait = true)  
  {
    if(!Lock.LockObject(wait))
      {
	_entry = badValue;
	return(returnDCVector::BAD_MUTEX);
      }    
    //We now have the mutex!    
    int64_t ret = returnDCVector::OK;

    password = _GetPassword(key);
    ret = _Get(key,_entry,password);    
    Lock.UnlockObject();    
    return(ret);
  }
    
  int64_t ClearPassword(int64_t key, uint64_t password,bool wait=true)
  {
#ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"DCVector::ClearPassword(key=%zd,password=%zX)\n",key,password);
#endif 
    if(!Lock.LockObject(wait))
      {
	return(returnDCVector::BAD_MUTEX);
      }

    int64_t ret = returnDCVector::OK;

    //We now have the mutex!
    ret = _ClearPassword(key,password);
    Lock.UnlockObject();
    return(ret);
  }

  //================================================================
  //Public Interfaces (special)
  //================================================================
  //Clear():           Clears all data from vector and removes all locks.
  //
  //================================================================
  bool Clear(bool wait = true)
  {
    if(!Lock.LockObject(wait))
      {
	return(false);
      }

    //We have the mutex

    
    for(uint32_t i = 0; i < entry.size();i++)
      {
	if(entry[i].key != returnDCVector::EMPTY_KEY)
	  ActiveEntries--;
	//Reset entry to empty
	entry[i].Clear();

      }
    
    Lock.UnlockObject();
    return(true);
  };
  

private:

  //================================================================
  //Private Interfaces to passwords
  //================================================================
  uint64_t _GetPassword(int64_t key)
  {
    //Get the index value for our key
    uint32_t index = this->GetKeyIndex(key);
    //Return the password
    return (entry[index].password);    
  }
  int64_t _ClearPassword(int64_t key, uint64_t password)
  {
    //Get the index value for our key
    uint32_t index = this->GetKeyIndex(key);
    //Check if the object is password protected
    int64_t ret = _CheckPassword(index,password);
    if(ret != returnDCVector::BAD_PASSWORD)
      {
	if(key == entry[index].key)
	  {
	    //Clear the password the entry
	    entry[index].ClearPassword();
	    //reset the password block count
	    entry[index].passwordBlocks = 0;
	    ret = returnDCVector::OK;
	  }
	else
	  {
	    ret = entry[index].key;
	  }	
      }
    return ret;
  }
  //Standard way to check if there is a password on the entry corresponding to key.
  //You MUST have the mutex locked when you are doing this. 
  int64_t _CheckPassword(int32_t index,uint64_t password)
  {
    int64_t ret = returnDCVector::OK;
    //Check if the object is password locked
    if(entry[index].ExistPassword() && !Lock.GetShutdown())
      {
	//Check if password unlocks
	if(password != entry[index].password)
	  {
	    //password didn't check out, so we should return BAD_PASSWORD
	    ret = returnDCVector::BAD_PASSWORD;
	//fprintf(stderr,"We got a password block: %lu %u %ld\n",
	//	    entry[index].key,
	//	    uint32_t(entry[index].Obj),
	//	    entry[index].passwordBlocks);
	    //Increase the number of times the lock blocked use. 
	    entry[index].passwordBlocks++;
	  }
      }  
    return(ret);
  };
  //================================================================
  //Private Interfaces to entries
  //================================================================
  int64_t _Update(uint64_t key, T & _entry, uint64_t password)
  {
    //Get the index value for our key
    uint32_t index = this->GetKeyIndex(key);
    //Check if the object is locked
    int64_t ret = _CheckPassword(index,password);
    
    //Check if things are still ok (will be BAD_PASSWORD if we had a password problem)
    if(ret == returnDCVector::OK)
      {
	if(entry[index].Obj == emptyValue && 
	   _entry != emptyValue)
	  {
	    ActiveEntries++;	    
	  }
	//Copy in new entry
	entry[index].Obj = _entry;
	if(_entry == emptyValue)
	  {
	    if(ActiveEntries > 0)
	      ActiveEntries--;
	    else
	      fprintf(stderr,"ERROR: Somehow we were going to have negative active entries?\n");
	    entry[index].Clear();
	  }
	else if(_entry != badValue)
	  {
	    entry[index].key = key;
	  }	
	else
	  {
	    fprintf(stderr,"badValue in DCVector\n");
	    entry[index].key = key;
	  }
      }
    return ret;
  }

  int64_t _Get(int64_t key, T & _entry,uint64_t &password)
  {
    //Get the index value for our key
    uint32_t index = this->GetKeyIndex(key);
    //Check if the object is locked
    int64_t ret = _CheckPassword(index,password);
    if(ret == returnDCVector::BAD_PASSWORD)
      {
	_entry = badValue;
      }
    //    fprintf(stderr,"   DCVector::_Get: Key %zd with index %d used password 0x%zX\n",key,_entry,password);
    //Check if things are still ok (will be BAD_PASSWORD if we had a password problem)
    if(ret == returnDCVector::OK)
      {
	//There are three possiblities now 
	//EMPTY_KEY, returnDCVector::OK, and a collision

	//return the entry
	_entry = entry[index].Obj;
	//set the entries password
	entry[index].password = PasswordFunction();
	password=entry[index].password;	  
	
	//set our return value
	if(_entry == emptyValue)
	  {
	    //We have an empty element
	    //Set the key to the current key
	    entry[index].key = key;
	    //set return value
	    ret = returnDCVector::EMPTY_KEY;
	  }
	//check for a collision
	else if(!this->EquivalentKeys(key,entry[index].key))	  //	else if(key != entry[index].key)
	  {
	    //set return value to previous key
	    ret = entry[index].key;
	  }
	//else
	//  ret already OK
	//  key already correct
      }     
    return(ret);
  }


  //================================================================
  //Special number functions
  //================================================================  
  //Password function (simple, just for safety)
  uint64_t PasswordFunction() 
  {
    //Check that the new pw isn't our notLocked value
    if((++lastPW) == blank_password)
      {
	++lastPW;
      }
    return(lastPW);
  };

  //================================================================
  //Data members
  //================================================================  
  //Last password used.
  uint64_t lastPW;  
  //blank password value
  uint64_t blank_password;
  
  //Current count of active entries
  uint32_t ActiveEntries;

  ObjectModel Lock;
  //vector of entries
  std::vector< DCVectorEntry<T> > entry;   //Vector of data
  T                 emptyValue; //Value to return if there is no data for our key's index.
  T                 badValue;//Value for bad data

};



#endif
