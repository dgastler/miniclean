--This module takes incoming ARP requests and creates a ARP reply packet.
--An ARP packet is very small and has an inflexible format so this task is
--not really difficult. The ARP packet consists mostly of addresses--destination 
--MAC, source MAC, sender MAC, sender IP, target MAC, and target IP. Half of 
--these addresses are copied from the incoming packet--the other half must be 
--produced. This module must also synchronize its efforts with the packet buffers.
--Original code by Jeremy Mans.
--Translation into VHDL along with modifications to the state machine to make it 
--decently efficient by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity arp is Port(
	MAC_CLK : in STD_LOGIC;
	RESET : in STD_LOGIC;
	SEND_CKE : in STD_LOGIC;
	PACKET_READY : in STD_LOGIC;
	DONE_WITH_PACKET : out STD_LOGIC;
	PACKET_DATA : in STD_LOGIC_VECTOR(7 downto 0);
	PACKET_READ_ADDR : out unsigned(5 downto 0);
	MYMAC : in STD_LOGIC_VECTOR(47 downto 0);
	MYIP : in STD_LOGIC_VECTOR(31 downto 0);
	PACKET_OUT : out STD_LOGIC_VECTOR(7 downto 0);
	EARLY_WARNING : out STD_LOGIC;
	PACKET_XMIT : out STD_LOGIC);
end arp;

architecture Behavioral of arp is
	signal compareConst : STD_LOGIC_VECTOR(7 downto 0);
	signal compareIP : STD_LOGIC_VECTOR(7 downto 0);
	signal resp_read_addr : unsigned(5 downto 0);
	signal resp_data : STD_LOGIC_VECTOR(7 downto 0);
	signal packet_ra : unsigned(5 downto 0);
	signal packet_oa : unsigned(5 downto 0);
	signal packet_oa1 : unsigned(5 downto 0);
	signal packet_oa2 : unsigned(5 downto 0);
	signal we : STD_LOGIC_VECTOR(3 downto 0);
	type state_type is (st_idle, st_checkconstwait, st_checkconst, 
		st_checkip_wait, st_checkip, st_resp_we, st_done);
	signal state : state_type := st_idle;
-- worth responding to: HARDWARE = 0x0 0x1, PROTOCOL = 0x8 0x0,
--                      HLEN = 0x6, PLEN=0x4, OPERATION = 0x0 0x1
--                      TARGET IA = my IP 
begin
	PACKET_READ_ADDR <= packet_ra;
	PACKET_XMIT <= we(3) and we(0);
	EARLY_WARNING <= we(0);
	compareConst <= x"00" when packet_ra = "00" & x"E" else
		x"01" when packet_ra = "00" & x"F" else
		x"08" when packet_ra = "01" & x"0" else
		x"00" when packet_ra = "01" & x"1" else
		x"06" when packet_ra = "01" & x"2" else
		x"04" when packet_ra = "01" & x"3" else
		x"00" when packet_ra = "01" & x"4" else
		x"01" when packet_ra = "01" & x"5" else x"00";
	compareIP <= MYIP(31 downto 24) when packet_ra = "10" & x"6" else
		MYIP(23 downto 16) when packet_ra = "10" & x"7" else
		MYIP(15 downto 8) when packet_ra = "10" & x"8" else
		MYIP(7 downto 0) when packet_ra = "10" & x"9" else x"00";
	resp_read_addr <= "00" & x"6" when packet_oa = "00" & x"0" else
		--bytes 6 through B of the incoming packet contain the source MAC
		"00" & x"7" when packet_oa = "00" & x"1" else
		"00" & x"8" when packet_oa = "00" & x"2" else
		"00" & x"9" when packet_oa = "00" & x"3" else
		"00" & x"A" when packet_oa = "00" & x"4" else
		"00" & x"B" when packet_oa = "00" & x"5" else
		--incoming bytes 16 through 1B contain the original sender MAC
		"01" & x"6" when packet_oa = "10" & x"0" else
		"01" & x"7" when packet_oa = "10" & x"1" else
		"01" & x"8" when packet_oa = "10" & x"2" else
		"01" & x"9" when packet_oa = "10" & x"3" else
		"01" & x"A" when packet_oa = "10" & x"4" else
		"01" & x"B" when packet_oa = "10" & x"5" else
		--incoming bytes 1C through 1F contain the sender IP address
		"01" & x"C" when packet_oa = "10" & x"6" else
		"01" & x"D" when packet_oa = "10" & x"7" else
		"01" & x"E" when packet_oa = "10" & x"8" else
		"01" & x"F" when packet_oa = "10" & x"9" else packet_oa;
	--outgoing data includes personal MAC and IP, protocol data,
	--and data salvaged from the incoming packet
	resp_data <= PACKET_DATA when packet_oa2 < "00" & x"6" else
		myMAC(47 downto 40) when packet_oa2 = "00" & x"6" else
		myMAC(39 downto 32) when packet_oa2 = "00" & x"7" else
		myMAC(31 downto 24) when packet_oa2 = "00" & x"8" else
		myMAC(23 downto 16) when packet_oa2 = "00" & x"9" else
		myMAC(15 downto 8) when packet_oa2 = "00" & x"A" else
		myMAC(7 downto 0) when packet_oa2 = "00" & x"B" else
		x"08" when packet_oa2 = "00" & x"C" else
		x"06" when packet_oa2 = "00" & x"D" else
		x"00" when packet_oa2 = "00" & x"E" else
		x"01" when packet_oa2 = "00" & x"F" else
		x"08" when packet_oa2 = "01" & x"0" else
		x"00" when packet_oa2 = "01" & x"1" else
		x"06" when packet_oa2 = "01" & x"2" else
		x"04" when packet_oa2 = "01" & x"3" else
		x"00" when packet_oa2 = "01" & x"4" else
		x"02" when packet_oa2 = "01" & x"5" else
		myMAC(47 downto 40) when packet_oa2 = "01" & x"6" else
		myMAC(39 downto 32) when packet_oa2 = "01" & x"7" else
		myMAC(31 downto 24) when packet_oa2 = "01" & x"8" else
		myMAC(23 downto 16) when packet_oa2 = "01" & x"9" else
		myMAC(15 downto 8) when packet_oa2 = "01" & x"A" else
		myMAC(7 downto 0) when packet_oa2 = "01" & x"B" else
		MYIP(31 downto 24) when packet_oa2 = "01" & x"C" else
		MYIP(23 downto 16) when packet_oa2 = "01" & x"D" else
		MYIP(15 downto 8) when packet_oa2 = "01" & x"E" else
		MYIP(7 downto 0) when packet_oa2 = "01" & x"F" else 
		PACKET_DATA;

	process(MAC_CLK)
	begin
		if MAC_CLK'event and MAC_CLK = '1' then
			if SEND_CKE = '1' then
				packet_oa1 <= packet_oa;
				packet_oa2 <= packet_oa1;
				we(3 downto 1) <= we(2 downto 0);
			end if;
			if RESET = '1' then
				we(0) <= '0';
				state <= st_idle;
				DONE_WITH_PACKET <= '0';
			else
				case state is
				when st_idle =>
					we(0) <= '0';
					DONE_WITH_PACKET <= '0';
					if PACKET_READY = '1' then
						packet_ra <= "00" & x"E";--just after the header
						packet_oa <= "00" & x"0";
						state <= st_checkconstwait;
					else
						state <= st_idle;
					end if;
				when st_checkconstwait =>
					we(0) <= '0';
					state <= st_checkconst;--one wait state
					DONE_WITH_PACKET <= '0';
				when st_checkconst =>
					we(0) <= '0';
					if PACKET_DATA /= compareConst then
						state <= st_done;
						DONE_WITH_PACKET <= '1';--failure
					elsif packet_ra = "01" & x"5" then
						packet_ra <= "10" & x"6";
						state <= st_checkip_wait;
						DONE_WITH_PACKET <= '0';
					else
						packet_ra <= packet_ra + "000001";
						state <= st_checkconstwait;
						DONE_WITH_PACKET <= '0';
					end if;
				when st_checkip_wait =>
					we(0) <= '0';
					state <= st_checkip;
					DONE_WITH_PACKET <= '0';
				when st_checkip =>
					if PACKET_DATA /= compareIP then
						we(0) <= '0';
						state <= st_done;
						DONE_WITH_PACKET <= '1';--failure
					elsif packet_ra = "10" & x"9" then
						if SEND_CKE = '1' then
							we(0) <= '1';
							state <= st_resp_we;
							DONE_WITH_PACKET <= '0';
						else
							we(0) <= '0';
							state <= st_checkip;
							DONE_WITH_PACKET <= '0';
						end if;
					else
						we(0) <= '0';
						packet_ra <= packet_ra + "000001";
						state <= st_checkip_wait;
						DONE_WITH_PACKET <= '0';
					end if;
				when st_resp_we =>
					if SEND_CKE = '0' then
						we(0) <= '1';
						state <= st_resp_we;
						DONE_WITH_PACKET <= '0';
					else
						PACKET_OUT <= resp_data;
						packet_ra <= resp_read_addr;
						if packet_oa2 = "10" & x"A" then
							packet_oa <= "000000";
							we(0) <= '0';
							state <= st_done;
							DONE_WITH_PACKET <= '1';--packet sent successfully
						else
							packet_oa <= packet_oa + "000001";
							we(0) <= '1';
							state <= st_resp_we;
							DONE_WITH_PACKET <= '0';
						end if;
					end if;
				when st_done=>
					state <= st_idle;--one wait state
					we(0) <= '0';
					DONE_WITH_PACKET <= '1';
				end case;
			end if;
		end if;
	end process;
end Behavioral;
