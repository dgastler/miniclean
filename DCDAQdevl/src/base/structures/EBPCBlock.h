#ifndef __EBPCBLOCK__
#define __EBPCBLOCK__

#include <vector>
#include "RAT/DS/Root.hh"
#include <ReductionLevel.h>
#include <EventState.h>

struct EBPCBlock
{   
  RAT::DS::Root * ds;
  int8_t reductionLevel;
  std::vector<uint8_t> WFD;
  std::vector<uint8_t> PMT;
  int8_t state;
  int64_t eventID;

  EBPCBlock(){ds = NULL;Clear();};
  void Clear()
  {
    state = EventState::UNFINISHED;
    eventID = -1;
    reductionLevel = ReductionLevel::UNKNOWN;
    PMT.clear();
    WFD.clear();
    if(ds != NULL)      
      ds->PruneEV();
  };
  ~EBPCBlock()
  {
  }
};
#endif
