#include <DS/Root.hh>
#include <LowResWaveform.h>
#include <DSReader.hh>
#include <iostream>
#include <sys/stat.h> 
#include <fcntl.h>
#include <arpa/inet.h>

#include <fstream>

#include <sstream>

#include <map>

#include <TH2F.h>
#include <TH1F.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TStyle.h>

TH2F * RAT_EvsPrompt;
TH2F * MCB_EvsPrompt;
TH1F * RAT_Energy;
TH1F * MCB_Energy;
TH1F * RAT_Prompt;
TH1F * MCB_Prompt;
TH1F * diffEnergy;
TH1F * diffPrompt;

struct conorPacket1
{
  uint32_t writeCommand;   //lowest 8 bits should be 0x20 for write
                        //bits 8 and above are write size
                        //i.e. 0x100 (256 time samples)
  uint32_t memBlockAddress;//location in the FPGA memory where this 
                           //event will be saved. 
  uint32_t eventTimeStamp; //based on a 1Mhz clock
  uint32_t promptBitTwoPattern[5]; //The MSB patterns for the prompt region
  uint32_t promptBitOnePattern[5];   //The MidSB patterns for the prompt region
  uint32_t bitZeroPattern[245];      //the lsb pattern for 5+240 clock ticks
};
struct conorPacket2
{
  uint32_t writeCommand;   //lowest 8 bits should be 0x20 for write
                           //bits 8 and above are write size
                           //i.e. 0x100 (256 time samples)
  uint32_t memBlockAddress;//location in the FPGA memory where this 
                           //event will be sent. 
  uint32_t bitZeroPattern[256];  //the last 256 lsb patterns for this board
};

struct fakeBoard
{
  uint32_t address;   //current address on FPGA
                      //ranges from 0x0 to 0xC600

  uint32_t addressOffset; //offset between packet1 and packet2 addresses.
                          //packet1 address = address
                          //packet2 address = address + addressOffset
  uint32_t size;
  uint32_t command; 
  conorPacket1 packet1;
  conorPacket2 packet2;
};

void FillPacket(struct fakeBoard & fBoard,		 
	        std::map< int,std::vector<LowResWaveform> > & board,
		size_t boardStart,
		size_t boardEnd,
		int preSamples = 50) //presamples in low-res waveform. 
{    
  fBoard.packet1.writeCommand = htonl((fBoard.size << 8) | fBoard.command);
  fBoard.packet2.writeCommand = htonl((fBoard.size << 8) | fBoard.command);
  fBoard.packet1.memBlockAddress = htonl(fBoard.address);
  fBoard.packet2.memBlockAddress = htonl(fBoard.address + fBoard.addressOffset);
  //fill packet1's high bit
  std::map<int, std::vector<LowResWaveform > >::iterator it;
  std::map<int, std::vector<LowResWaveform > >::iterator itEnd = board.begin();
  std::map<int, std::vector<LowResWaveform > >::iterator itStart = board.begin();
  for(int i =0 ; i < boardEnd;i++)
    itEnd++;  
  for(int i =0 ; i < boardStart;i++)
    itStart++;  
  itStart--;
  
  for(size_t i = 0; i < 5;i++)
    {     
      //zero this time bin      
      fBoard.packet1.promptBitTwoPattern[i] = 0;
      for(it= (itEnd); 
	  it != (itStart);it--)
	{
	  for(int iBoardWave = 7; iBoardWave >= 0 ; iBoardWave--)
	    {
	      //shift bits to the left
	      fBoard.packet1.promptBitTwoPattern[i] = (fBoard.packet1.promptBitTwoPattern[i] << 1);
	      //set rightmost bit
	      if(it->second.size() > iBoardWave)
		{
		  fBoard.packet1.promptBitTwoPattern[i] |= 0x1&
		    (it->second[iBoardWave].GetWaveformHigh()[preSamples+i]);
		}
	    } 
	}
      //Convert to network byte order
      fBoard.packet1.promptBitTwoPattern[i] = htonl(fBoard.packet1.promptBitTwoPattern[i]);
    }

  //fill packet1's mid bit
  for(size_t i = 0; i < 5;i++)
    {     
      //zero this time bin      
      fBoard.packet1.promptBitOnePattern[i] = 0;
      for(it= itEnd; 
	  it != (itStart);it--)
	{
	  for(int iBoardWave = 7; iBoardWave >= 0 ; iBoardWave--)
	    {
	      //shift bits to the left
	      fBoard.packet1.promptBitOnePattern[i] = (fBoard.packet1.promptBitOnePattern[i] << 1);
	      //set rightmost bit
	      if(it->second.size() > iBoardWave)
		{
		  fBoard.packet1.promptBitOnePattern[i] |= 0x1&
		    (it->second[iBoardWave].GetWaveformMid()[preSamples+i]);
		}

	    } 
	}
      //convert to network byte order
      fBoard.packet1.promptBitOnePattern[i] = htonl(fBoard.packet1.promptBitOnePattern[i]);
    }
  //fill packet1's low bit
  for(size_t i = 0; i < 245;i++)
    {     
      //zero this time bin      
      fBoard.packet1.bitZeroPattern[i] = 0;
      for(it= itEnd; 
	  it != (itStart);it--)
	{
	  for(int iBoardWave = 7; iBoardWave >= 0 ; iBoardWave--)
	    {
	      //shift bits to the left
	      fBoard.packet1.bitZeroPattern[i] = (fBoard.packet1.bitZeroPattern[i] << 1);
	      //set rightmost bit
	      if(it->second.size() > iBoardWave)
		{
		  fBoard.packet1.bitZeroPattern[i] |= 0x1&
		    (it->second[iBoardWave].GetWaveformLow()[preSamples+i]);
		}
	    } 
	}
      //      convert to network byte order
      fBoard.packet1.bitZeroPattern[i] = htonl(fBoard.packet1.bitZeroPattern[i]);
    }

  //fill packet2's low bit
  for(size_t i = 0; i < 256;i++)
    {     
      //zero this time bin      
      fBoard.packet2.bitZeroPattern[i] = 0;
      for(it= itEnd; 
	  it != (itStart);it--)
	{
	  for(int iBoardWave = 7; iBoardWave >= 0 ; iBoardWave--)
	    {
	      //set rightmost bit
	      if(it->second.size() > iBoardWave)
		{
		  fBoard.packet2.bitZeroPattern[i] |= 0x1&
		    (it->second[iBoardWave].GetWaveformLow()[preSamples+245+i]);
		}
	      //shift bits to the left
	      fBoard.packet2.bitZeroPattern[i] = (fBoard.packet2.bitZeroPattern[i] << 1);
	    } 
	}
      //convert to network byte order
      fBoard.packet2.bitZeroPattern[i] = htonl(fBoard.packet2.bitZeroPattern[i]);
    }

}


void BuildEvent(RAT::DS::Root * ds,
		std::map< int,std::vector<LowResWaveform> > &boardWaveforms,
		float threshLow,float threshMid,float threshHigh,
		int waveformSize,int grouping,
		int presamples, int promptSamples,
		float baseline, bool save = false)
{
  //Clear all the previous waveforms
  boardWaveforms.clear();
  LowResWaveform waveform(waveformSize);
  
  RAT::DS::EV * ev = ds->GetEV(0);
  int boardCount = ev->GetBoardCount();

  double promptPEs = 0;
  double totalPEs = 0;

  for(int iBoard = 0; iBoard < boardCount;iBoard++)
    {
      RAT::DS::Board * board = ev->GetBoard(iBoard);
      int channelCount = board->GetChannelCount();
      boardWaveforms[board->GetID()].resize(channelCount);      
      for(int iChan = 0; iChan < channelCount;iChan++)
	{
	  RAT::DS::Channel * chan = board->GetChannel(iChan);
	  //If this is an IV channel
	  if(chan->GetID() < 100)
	    {
	      //Build the lowres waveform
	      waveform.Process(chan,
			       threshLow,threshMid,threshHigh,
			       grouping,waveformSize,
			       presamples,promptSamples,
			       baseline,save);
	      //copy it ot the board structure
	      boardWaveforms[board->GetID()][chan->GetInput()] = waveform;
	      promptPEs += waveform.finalPromptPEs;
	      totalPEs += waveform.finalLatePEs + waveform.finalPromptPEs;
	    }		  
	}
    }		  
	      
  double MCB_QPE = totalPEs;
  double MCB_fP = promptPEs/totalPEs;
  RAT_EvsPrompt->Fill(ev->GetQPE(),ev->GetFprompt());
  MCB_EvsPrompt->Fill(MCB_QPE,MCB_fP);
  RAT_Energy->Fill(ev->GetQPE());
  MCB_Energy->Fill(MCB_QPE);
  RAT_Prompt->Fill(ev->GetFprompt());
  MCB_Prompt->Fill(MCB_fP);
  diffEnergy->Fill((ev->GetQPE()-MCB_QPE)/ev->GetQPE());
  diffPrompt->Fill((ev->GetFprompt()-MCB_fP)/ev->GetFprompt());
}

void setupPlots()
{
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);  
  RAT_EvsPrompt = new TH2F("RAT_EvsPrompt","RAT Energy vs. fPrompt",
			   100,0,4000,100,0,1);
  MCB_EvsPrompt = new TH2F("MCB_EvsPrompt","MC-#beta Energy vs. fPrompt",
			   100,0,4000,100,0,1);
  RAT_EvsPrompt->GetXaxis()->SetTitle("PE");
  RAT_EvsPrompt->GetYaxis()->SetTitle("fPrompt");
  MCB_EvsPrompt->GetXaxis()->SetTitle("PE (LSB sample charge)");
  MCB_EvsPrompt->GetYaxis()->SetTitle("fPrompt");

  RAT_Energy = new TH1F("RAT_Energy","RAT Energy",100,0,4000); 
  MCB_Energy = new TH1F("MCB_Energy","MC-#beta Energy",100,0,4000); 
  RAT_Energy->GetXaxis()->SetTitle("PE");
  MCB_Energy->GetXaxis()->SetTitle("PE (LSB sample charge)");

  RAT_Prompt = new TH1F("RAT_Prompt","RAT fPrompt",100,0,1);
  MCB_Prompt = new TH1F("MCB_Prompt","MC-#beta fPrompt",100,0,1);
  RAT_Prompt->GetXaxis()->SetTitle("f_{prompt}");
  MCB_Prompt->GetXaxis()->SetTitle("f_{prompt}");

  diffEnergy = new TH1F("diffEnergy","Energy difference",100,-1,1);
  diffPrompt = new TH1F("diffPrompt","Prompt difference",100,-1,1);
  diffEnergy->GetXaxis()->SetTitle("Energy (RAT-MC-#beta)/RAT");
  diffPrompt->GetXaxis()->SetTitle("Prompt (RAT-MC-#beta)/RAT");
}

void PrintPlots(float threshLow,float threshMid,float threshHigh)
{
  std::stringstream ss;
  std::string name_end;
  ss << "_";
  ss << threshLow;
  ss << "_";
  ss << threshMid;
  ss << "_";
  ss << threshHigh;
  ss << ".png";
  name_end = ss.str();

  std::string name;
  TCanvas * c1 = new TCanvas("c1");
  RAT_EvsPrompt->Draw("");
  MCB_EvsPrompt->SetMarkerColor(kBlue);
  MCB_EvsPrompt->Draw("SAME");
  TLegend * leg = new TLegend(0.6,0.7,0.9,0.9);
  leg->AddEntry(RAT_EvsPrompt,"RAT","p");
  leg->AddEntry(MCB_EvsPrompt,"MC-#beta","p");
  leg->Draw("");  
  name.clear();
  name.assign(RAT_EvsPrompt->GetName());
  name += name_end;
  c1->Print(name.c_str());

  c1->Clear();
  RAT_Energy->Draw("");
  MCB_Energy->SetLineColor(kBlue);
  MCB_Energy->Draw("SAME");
  leg->Clear();
  leg->AddEntry(RAT_Energy,"RAT","l");
  leg->AddEntry(MCB_Energy,"MC-#beta","l");
  leg->Draw("");  
  name.clear();
  name.assign(RAT_Energy->GetName());
  name += name_end;
  c1->Print(name.c_str());

  c1->Clear();
  RAT_Prompt->Draw("");
  MCB_Prompt->SetLineColor(kBlue);
  MCB_Prompt->Draw("SAME");
  leg->Clear();
  leg->AddEntry(RAT_Prompt,"RAT","l");
  leg->AddEntry(MCB_Prompt,"MC-#beta","l");
  leg->Draw("");  
  name.clear();
  name.assign(RAT_Prompt->GetName());
  name += name_end;
  c1->Print(name.c_str());

  c1->Clear();
  diffEnergy->Draw("");
  name.clear();
  name.assign(diffEnergy->GetName());
  name += name_end;
  c1->Print(name.c_str());  

  c1->Clear();
  diffPrompt->Draw("");
  name.clear();
  name.assign(diffPrompt->GetName());
  name += name_end;
  c1->Print(name.c_str());  
}

void PrintWaveforms(std::map<int, std::vector<LowResWaveform> > & waveforms,
		    float chargeLow,float chargeMid,float chargeHigh,
		    int waveformSize, int grouping)
{
  std::stringstream ss;
  std::string name_end;
  ss << chargeLow;
  ss << "_";
  ss << chargeMid;
  ss << "_";
  ss << chargeHigh;
  ss << ".dat";
  name_end = ss.str();

    //Print the saved low-res waveform
  std::string rawFileName("MCB_");
  rawFileName += name_end;
  std::ofstream rawOutFile(rawFileName.c_str());
  for(int i = 0; i < waveformSize; i++)
    {
      rawOutFile << i << " ";
      for(std::map<int,std::vector<LowResWaveform> >::iterator it = waveforms.begin(); it != waveforms.end();it++)
	{
	  for(std::vector<LowResWaveform>::iterator subIT = it->second.begin();
	      subIT < it->second.end();
	      subIT++)
	    {
	      if(subIT->GetWaveformHigh().size() > i)
		{
		  rawOutFile << (subIT->GetWaveformHigh()[i]*chargeHigh + 
				 subIT->GetWaveformMid()[i]*chargeMid + 
				 subIT->GetWaveformLow()[i]*chargeLow) 
			     << " ";
		}
	    }
	}
      rawOutFile << std::endl;
    }

  //Print the saved real waveform
  std::string realFileName("RAT_");
  realFileName += name_end;
  std::ofstream realOutFile(realFileName.c_str());
  for(int i = 0; i < waveformSize*grouping; i++)
    {
      realOutFile << float(i)/float(grouping) << " ";
      for(std::map<int,std::vector<LowResWaveform> >::iterator it = waveforms.begin(); it != waveforms.end();it++)
	{
	  for(std::vector<LowResWaveform>::iterator subIT = it->second.begin();
	      subIT < it->second.end();
	      subIT++)
	    {	      
	      if(subIT->GetRealWaveform().size() > i)
		{
		  realOutFile << subIT->GetRealWaveform()[i]<< " ";
		}
	    }      
	}
      realOutFile << std::endl;
    }
}


int main(int argc, char ** argv)
{
  setupPlots();
  //MonteCarlo access

  

  //Get waveform thresholds and set SPE ratios
  if(argc < 2)
    {
      printf("Usage: %s files\n",argv[0]);
      return 0;
    }  

  RAT::DSReader reader(argv[1]);
  for(int iFile = 2;iFile < argc;iFile++)
    {    
      reader.Add(argv[iFile]);
    }

  RAT::DS::Root * ds = reader.NextEvent();

  float ADCThreshLow  = 30;
  float ADCThreshMid  = 60;
  float ADCThreshHigh = 120;
  if(ADCThreshHigh < ADCThreshLow)
    {
      float ADCThreshTemp = ADCThreshLow;
      ADCThreshLow = ADCThreshHigh;
      ADCThreshHigh = ADCThreshTemp;
    }
  if((ADCThreshLow == 0) || 
     (ADCThreshHigh == 0))
    {
      printf("ADCThresh must be positive!\n");
      return 0;
    }
  float Scale1 = float(ADCThreshHigh)/ADCThreshLow;
  float Scale2 = float(ADCThreshMid)/ADCThreshLow;

  //Constants for Waveform conversion
  const int sampleTime = 4;//ns
  int grouping = 5;  //samples
  int promptTime = 100;  //ns
  int presamples = 250; //samples
  int promptSamples = promptTime/sampleTime;  //samples
  int lowresPresamples = presamples/grouping; //lr samples
  int lowresPromptSamples = promptTime/(grouping*sampleTime); //lr samples  
  const int waveformSize = 600;
  float baseline = 4076;


  //Lowres waveform storage in board groups
  std::map<int, std::vector<LowResWaveform> > boardsOfWaveforms;

  std::map<int, std::vector<LowResWaveform> > waveformsForPrinting;
  int printEvent = -1;
  bool saveEvent = true;//false;

  struct fakeBoard fakeDataPackets[3];
  fakeDataPackets[0].address = 0;
  fakeDataPackets[0].addressOffset = 0x100;
  fakeDataPackets[0].size = 0x100;
  fakeDataPackets[0].command = 0x20;
  fakeDataPackets[2] = fakeDataPackets[1] = fakeDataPackets[0];
  

  int file0_fd = open("file0.dat",O_WRONLY | O_CREAT | O_TRUNC,
		      S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  int file1_fd = open("file1.dat",O_WRONLY | O_CREAT | O_TRUNC,
		      S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  int file2_fd = open("file2.dat",O_WRONLY | O_CREAT | O_TRUNC,
		      S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

  uint32_t time = 0; 
  uint32_t timeSpacing = 1000*1000;

  while((ds = reader.NextEvent()))
    {
      if(ds->GetEVCount() > 0)
	{
	  //save an event for printing
//	  if((ds->GetEV(0)->GetQPE() > 2700)&&
//	     (printEvent == -1))
//	    {
//	      printEvent = ds->GetEV(0)->GetEventID();
//	      saveEvent = true;
//	    }
	  
	  BuildEvent(ds,boardsOfWaveforms,
		     ADCThreshLow,ADCThreshMid,ADCThreshHigh,
		     waveformSize,grouping,
		     presamples,promptSamples,
		     baseline,saveEvent);	  
	  if(saveEvent)
	    {
	      waveformsForPrinting = boardsOfWaveforms;
	      saveEvent = false;
	    }
	  

	  //write event	  
	  FillPacket(fakeDataPackets[0],boardsOfWaveforms,0,3 ,lowresPresamples);
	  FillPacket(fakeDataPackets[1],boardsOfWaveforms,4,7 ,lowresPresamples);
	  FillPacket(fakeDataPackets[2],boardsOfWaveforms,8,11,lowresPresamples);

	  fakeDataPackets[0].packet1.eventTimeStamp = htonl(time);
	  fakeDataPackets[1].packet1.eventTimeStamp = htonl(time);
	  fakeDataPackets[2].packet1.eventTimeStamp = htonl(time);

	 
	  write(file0_fd,&(fakeDataPackets[0].packet1),sizeof(conorPacket1));
	  write(file0_fd,&(fakeDataPackets[0].packet2),sizeof(conorPacket2));

	  write(file1_fd,&(fakeDataPackets[1].packet1),sizeof(conorPacket1));
	  write(file1_fd,&(fakeDataPackets[1].packet2),sizeof(conorPacket2));

	  write(file2_fd,&(fakeDataPackets[2].packet1),sizeof(conorPacket1));
	  write(file2_fd,&(fakeDataPackets[2].packet2),sizeof(conorPacket2));

	  fakeDataPackets[0].address += 0x200;
	  fakeDataPackets[1].address += 0x200;
	  fakeDataPackets[2].address += 0x200;

	  if(fakeDataPackets[0].address > 0xC600)
	    {
	      fakeDataPackets[0].address = 0x0;
	      fakeDataPackets[1].address = 0x0;
	      fakeDataPackets[2].address = 0x0;
	    }
	  
	
	  time+=timeSpacing;
	  timeSpacing/=1.1;
	}      
    }
  close(file0_fd);
  close(file1_fd);
  close(file2_fd);
  PrintPlots(ADCThreshLow,ADCThreshMid,ADCThreshHigh);
  PrintWaveforms(waveformsForPrinting,
		 ADCThreshLow,ADCThreshMid,ADCThreshHigh,
		 waveformSize,grouping);
  return 0;
}
