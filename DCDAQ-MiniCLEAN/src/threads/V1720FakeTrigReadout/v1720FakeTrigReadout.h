#ifndef __V1720FAKETRIGREADOUT__
#define __V1720FAKETRIGREADOUT__

#include <map>
#include <string>

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <v1720WFD.h>
#include <xmlHelper.h>
#include <StatMessage.h>
#include <iostream>
#include <iomanip>

#include <cstdlib> //for getenv()
#include <cstring> //for memcpy()
#include <sys/stat.h> //for file stats

#ifdef __CAENLIBS__

class v1720FakeTrigReadout : public DCThread 
{
 public:
  v1720FakeTrigReadout(std::string Name);
  ~v1720FakeTrigReadout();
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  uint32_t MinimumFreeBufferSpace;
  uint32_t MaxReadoutsPerBlock;

  //Statistics
  std::vector<uint32_t> TimeStep_ReadoutNumber;

  std::vector<uint32_t> TimeStep_ReadoutSize;  

  std::vector<uint32_t> TimeStep_Occupancy;  

  std::vector<uint32_t> TimeStep_EventCount;

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message & message);
  //Handle select timeout
  virtual void ProcessTimeout();

  //Handle sending out a full physics event.
  bool SendPhysicsEvent();

  //Input fake data file info and file decriptor
  std::string InFileName;
  FILE * InFile;

  //Memory block manager
  MemoryBlock tempBlock;
  MemoryBlockManager * Mem; 
  MemoryBlock * block;
  int32_t blockIndex;
  
  //Setup data
  std::string WFDSetup; //Strings containing the WFD XML config strings
  xmlDoc * XMLDoc; //xml documents created from the XML strings
  bool StartWFDs();
  bool StopWFDs();

  //Loaded fake data
  uint32_t FakeWFDDataSize;
  uint32_t * FakeWFDData;  //all fake data pointer
  std::vector<uint32_t *> FakeWFDEvent;   //Pointer to individual fake events in FAKEWFD
  unsigned int FakeEventCount;
  uint32_t EventCountOffset;
  uint32_t WFDsPerEvent;

  //Time values
  time_t currentTime;
  time_t lastTime;

  //WFD vectors.
  std::vector<xmlNode*> BaseXMLNodes; //base XML node of the XML Doc
  std::vector<v1720*> WFD; //Vector of v1720WFD classes

  //Vector of WFD event IDs
  std::map<int,uint32_t> WFDEventID;
  //Current event index
  uint32_t iWFDEvent;

  //Memory manager access and opperations
  uint32_t LoopOffset;

};









#else  //CAENLIBS
//Stand in for V1720Readout class for when the caen libraries are not present. 
class v1720FakeTrigReadout : public DCThread 
{
 public:
  v1720FakeTrigReadout(std::string Name){};
  ~v1720FakeTrigReadout(){};
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager)
  {
    fprintf(stderr,"CAENLIBS not included. FATAL ERROR!\n");
    return false;
  };
};

#endif
#endif
