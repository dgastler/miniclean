#include <DQMServer.h>
#include <TH1.h>

DQMServer::DQMServer(std::string Name)  
{
  SetName(Name);
  SetType(std::string("DQMServer"));
 
 }

bool DQMServer::Setup(xmlNode * SetupNode,
		      std::vector<MemoryManager*> &MemManager)
{
  
  //Find the memoryManager we need
    
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return false;
    }
    
  if(!FindSubNode(SetupNode,"PORTPUSH"))
    {
      portPush=10000;
    }

  else
    {
      string port;
      GetXMLValue(SetupNode,"PORTPUSH",GetName().c_str(),port);
      portPush=atoi(port.c_str());
  
    }
  portRep=portPush+1;
  
  char * mssg=new char[100];
  sprintf(mssg,"Setting up zmqports for CLEANViewer Communication: %i \n",portPush);
  Print(mssg);
  delete [] mssg;
  
  
  context=new zmq::context_t(1);
  daqServerRep=new zmq::socket_t(*context,ZMQ_REP);
  zmqfdRep=-1;
  size_t zmqfdRep_size=sizeof(zmqfdRep);
  daqServerRep->getsockopt(ZMQ_FD,&zmqfdRep,&zmqfdRep_size);
 
  char * endpoint=new char[100];
  sprintf(endpoint,"tcp://*:%i",portRep);
 
  daqServerRep->bind(endpoint);
 
  daqServerPush=new zmq::socket_t(*context,ZMQ_PUSH);
  zmqfdPush=-1;
  size_t zmqfdPush_size=sizeof(zmqfdPush);
  daqServerPush->getsockopt(ZMQ_FD,&zmqfdPush,&zmqfdPush_size);
 
  delete endpoint;
  endpoint=new char[100];
  sprintf(endpoint,"tcp://*:%i",portPush);
 
  daqServerPush->bind(endpoint);
  delete endpoint;
  
  Ready = true;
 
  return(true);  
}

bool DQMServer::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      RemoveReadFD(Mem->GetFDs()[0]);
 
      RemoveReadFD(zmqfdRep);
   }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
      RemoveReadFD(Mem->GetFDs()[0]);
 
      RemoveReadFD(zmqfdRep);
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the Full queue's FD so we know when we have a new 
      //memory block to process
      AddReadFD(Mem->GetFDs()[0]);
 
      AddReadFD(zmqfdRep);
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

void DQMServer::MainLoop()
{  

  if(IsReadReady(Mem->GetFDs()[0]))
    {

      //Get new memory block
      Mem->Get(eventPorts);
      Event=eventPorts->ev;
     
     
      uint32_t sizeBuffer=CalculateEventSize(Event);

      int64_t sizeBufferWritten=0;
      uint8_t * bufferPtr=new uint8_t[sizeBuffer*sizeof(uint32_t)];

      sizeBufferWritten=StreamEVtoBuff(Event,bufferPtr,sizeBuffer);
     
     
      //send to zmq sockets
      
     

      vector<int> clientRemoveVector;

      int port;
      for(size_t i=0;i<eventPorts->ports.size();i++)
	{
	  port=eventPorts->ports[i];
 	  //put together message
	  zmq::message_t message((size_t)sizeBufferWritten+sizeof(ZMQ::MessageHead));
	  ZMQ::MessageHead MssgHead;
	  MssgHead.mssg=ZMQ::MSGEVENT;
	  MssgHead.runNumber=GetRunNumber();
	  MssgHead.port=port;
	  //ZMQ::MSG msg=ZMQ::MSGEVENT;
	  memcpy(message.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	  memcpy((uint8_t *)message.data()+sizeof(ZMQ::MessageHead),bufferPtr,sizeBufferWritten);
 
	 
	  int errCntr=0;
	      
	  while(!(daqServerPush->send(message,ZMQ_NOBLOCK)))
	    {
	      if((errno=EAGAIN)&&(errCntr<5))
		{
		  errCntr+=1;
		  //wait 10 microseconds
		  usleep(10);
		}
	      else
		{ 
		  //Timeout, lost connection to surface
		  PrintError("DQMServer has lost connection to surface");
		  break;
		}
	    }
	}
    
     
      //Cleanup memory
  
     
      delete [] bufferPtr;
     
      bufferPtr=NULL;
  
      delete eventPorts;

      eventPorts = NULL;
    }

    if(IsReadReady(zmqfdRep))
    {
    
      //check to see if we can really read anything
     
      int zmq_events;
      size_t zmq_events_size=sizeof(zmq_events);
     
      daqServerRep->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
     
      while(zmq_events & ZMQ_POLLIN)
	{

	  //We've really got something!
	  zmq::message_t message;
	  daqServerRep->recv(&message);
	  
	  
	  
	  ZMQ::Request * zmq_req=new ZMQ::Request();
	 
	  memcpy(zmq_req,message.data(),sizeof(ZMQ::Request));
	  
	 
	 

	  if(zmq_req->GetHead().magic != 0xAB)
	    {
	      printf("magic wrong \n");
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      ZMQ::MessageHead MssgHead;//=ZMQ::MSGBADMAGIC;
	      MssgHead.mssg=ZMQ::MSGBADMAGIC;
	      memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	      daqServerRep->send(reply,ZMQ_NOBLOCK);
	      //some sort of error
	      return;
	    }

	 
	  if(zmq_req->GetHead().version != ZMQVERSION)
	    {
	      //need to tell cleanviewer has wrong version,to update and recompile
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      ZMQ::MessageHead MssgHead;//=ZMQ::MSGWRNGVERSION;
	      MssgHead.mssg=ZMQ::MSGWRNGVERSION;
	      memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	      daqServerRep->send(reply,ZMQ_NOBLOCK);
	      printf("version wrong %i, %i\n",zmq_req->GetHead().version,ZMQVERSION);
	      return;

	    }
	 

	  //otherwise, check to see what kind of request
	  if(zmq_req->req==ZMQ::REQPORT)
	    {
	 
	      //client requesting port to connect to
	     
	      //Make sure port set correctly
	      if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;//  msg=ZMQ::MSGBADPORT;
		  MssgHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	     
	     
	      int port=zmq_req->port;
	      zmqInfo * socketInfo=new zmqInfo();
	      socketInfo->filter=new StreamFilter();
	      
	      zmq::message_t reply(sizeof(ZMQ::MessageHead)+sizeof(int32_t));
	      ZMQ::MessageHead MssgHead;//=ZMQ::MSGRECVDGOOD;
	      MssgHead.mssg=ZMQ::MSGRECVDGOOD;
	      memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	      daqServerRep->send(reply,ZMQ_NOBLOCK);
	      Mem->AddEntrySocket(port,socketInfo);
	      
	    }

	  if(zmq_req->req==ZMQ::REQPAUSE)
	    {
	      //Make sure port set correctly
	      if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;//  msg=ZMQ::MSGBADPORT;
		  MssgHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	      Mem->PauseEntrySocket(zmq_req->port);
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      //send CLEANViewer goodmessage response
	      ZMQ::MessageHead MssgHead;
	      MssgHead.mssg=ZMQ::MSGRECVDGOOD;
	      memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	      daqServerRep->send(reply,ZMQ_NOBLOCK);
	      
	    }

	  if(zmq_req->req==ZMQ::REQGO)
	    {
	      if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	      Mem->GoEntrySocket(zmq_req->port);
	      zmq::message_t reply(sizeof(ZMQ::MessageHead));
	      ZMQ::MessageHead MssgHead;
	      MssgHead.mssg=ZMQ::MSGRECVDGOOD;
	      memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	      daqServerRep->send(reply,ZMQ_NOBLOCK);
	    }
	  
	  if(zmq_req->req==ZMQ::REQREMOVESOCK)
	    {
	     
	       if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	       Mem->RemoveEntrySocket(zmq_req->port,true);
	       zmq::message_t reply(sizeof(ZMQ::MessageHead));
	       ZMQ::MessageHead MssgHead;
	       MssgHead.mssg=ZMQ::MSGRECVDGOOD;
	       memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	       daqServerRep->send(reply,ZMQ_NOBLOCK);

	    }
	  
	  if(zmq_req->req==ZMQ::REQFILTER)
	    {

	       if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	       if(zmq_req->filterNum<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGBADFILTERNUM;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	       
	      StreamFilter * filter=new StreamFilter();
	     
	      memcpy(filter,(uint8_t*)message.data()+sizeof(ZMQ::Request),sizeof(StreamFilter));
	      //Check to see if correct veriosn
	      if(filter->GetVersion() != FILTERVERSION)
		{	 
		 
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGWRNGVERSION;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  delete filter;
		  filter = NULL;
		}
	      else
		{
		  fprintf(stderr,"EditingFilter\n");
		  Mem->EditEntrySocket(zmq_req->port,(size_t)zmq_req->filterNum,filter);
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGRECVDGOOD;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  
		}

	    }
	  if(zmq_req->req==ZMQ::REQREMOVEFILTER)
	    {
	        if(zmq_req->port<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGBADPORT;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	       if(zmq_req->filterNum<0)
		{//port not set
		  zmq::message_t reply(sizeof(ZMQ::MessageHead));
		  ZMQ::MessageHead MssgHead;
		  MssgHead.mssg=ZMQ::MSGBADFILTERNUM;
		  memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
		  daqServerRep->send(reply,ZMQ_NOBLOCK);
		  return;
		}
	
	       	 
	       Mem->RemoveEntryFilter(zmq_req->port,(size_t)zmq_req->filterNum);
	       zmq::message_t reply(sizeof(ZMQ::MessageHead));
	       ZMQ::MessageHead MssgHead;
	       MssgHead.mssg=ZMQ::MSGRECVDGOOD;
	       memcpy(reply.data(),&MssgHead,sizeof(ZMQ::MessageHead));
	       daqServerRep->send(reply,ZMQ_NOBLOCK);
	

	    }
	  
	  
	  delete zmq_req;
	  daqServerRep->getsockopt(ZMQ_EVENTS,&zmq_events,&zmq_events_size);
	}
    }
 
 
 
}


void DQMServer::ProcessTimeout()
{

}
