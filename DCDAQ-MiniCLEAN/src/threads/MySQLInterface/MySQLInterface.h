#ifndef __MYSQLINTERFACE__
#define __MYSQLINTERFACE__
/*
This thread performs two duties:
  1) Interface with the DAQ RunControl database
     Poll DB RunLock table for action
     Build run xml and start DAQ
     Create a new run in the slow control
     Stop run when directed through RunLock table
  2) Inject DAQ monitoring data into the slow control
     Process incoming STAT messages into sql queries
     Inject into SC DB
*/

#include <DCThread.h>
#include <xmlHelper.h>
#include <DCMySQL.h>
#include <StatParser.h>
#include <DCRunLock.h>

#include <math.h> //fabs()
#include <XML_Factory.h>

class MySQLInterface : public DCThread 
{
public:
  MySQLInterface(std::string Name);
  ~MySQLInterface(){};
  
  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  
  virtual void MainLoop(){};
private:
  virtual void ProcessTimeout();
  virtual bool ProcessMessage(DCMessage::Message & message);
  bool ParseMySQLXML(xmlNode * baseNode,
		     std::string & server,
		     std::string & user,
		     std::string & password,
		     std::string & DB);

  //DAQDB
  DCMySQL DAQDB_Connection;
  time_t next_Time;
  time_t polling_Interval;
  bool UpdateRunLockState(DCRunLock::Type type);

  bool StoppingRun;
  bool SettingUpNewRun;

  bool StopCurrentRun();
  void CheckShutdown();
  bool StartNewRun();

  void ProcessCommandMessage(DCMessage::Message &message);

  //Slow Control
  DCMySQL SCDB_Connection;
  struct sRoute
  {
    enum DCMessage::DCMessageType type;
    DCMessage::DCAddress route;
  };
  std::vector<sRoute> routes;
  
  std::map<DCMessage::DCMessageType,void (MySQLInterface::*)(DCMessage::Message & message)> typeMap;
  void SendError(DCMessage::Message &message);
  void SendWarning(DCMessage::Message &message);
  void SendStats(DCMessage::Message &message);
  StatParser statParser;
  void SendText (DCMessage::Message &message);
  void ProcessRunNumber(DCMessage::Message &message);
  void GetRunNumberFromSC(DCMessage::DCAddress sourceAddr);
  bool runNumberControl;
  void EndRun();
  void BackupRun();
};
#endif
