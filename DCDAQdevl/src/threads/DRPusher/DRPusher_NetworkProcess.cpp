#include <DRPusher.h>

bool DRPusher::CheckParseStatus()
{
  bool ret = false;
  if(drEvent == NULL)
   {
     if(packet != NULL)
       //We have a packet, but no active drEvent structure
       {
	 //check if there is room for a DRPacket::EVent structre
	 if(packetSizeLeft >= sizeof(DRPacket::Event))
	   {
	     //Set up a new DRPacket::Event object
	     SetupDRPacketEvent();
	     ret = true;
	   }
	 else
	   {
	     //We have no room left and we must send a packet
	     ret = false;
	   }
       }
     else
       //No packet, so we need a new one
       {
	 //Get a free packet
	 DRAssembler.thread->GetOutPacketManager()->GetFree(iDCPacket,true);
	 packet = DRAssembler.thread->GetOutPacketManager()->GetPacket(iDCPacket);
	 //Set up our arrays
	 packetDataPtr = packet->GetData();
	 currentPacketDataPtr = packetDataPtr; 
	 packetSizeLeft = packet->GetMaxDataSize();         	  
	 SetupDRPacketEvent();
	 ret = true;
       }
   } 
  return ret;
}

void DRPusher::SetupDRPacketEvent()
{
  //Set up a new DRPacket::Event object
  //  drEvent = (DRPacket::Event *) packetDataPtr;
  drEvent = (DRPacket::Event *) currentPacketDataPtr;
  sLocal currentLocalEvent;
  LocalDeque.LockDeque();            //=============GOT LOCK===============
  currentLocalEvent = LocalDeque.Back();      
  if(currentLocalEvent != LocalDeque.GetBadValue())
    {
      drEvent->ID = currentLocalEvent.block->ev->GetEventID();
      //      printf("%d\n",drEvent->ID);
      drEvent->ChannelCount = 0;
      //move forward in our array
      currentPacketDataPtr += sizeof(DRPacket::Event); 
      //update the free space available. 	      
      packetSizeLeft -= sizeof(DRPacket::Event);
    }
  else
    {
      drEvent = NULL;
    }
  LocalDeque.UnlockDeque();          //============RELEASED LOCK===========
  currentLocalEvent = LocalDeque.GetBadValue();
}



void DRPusher::AddToPacket(RAT::DS::Raw * raw,uint16_t pmtID)
{
  //Add the channel header
  DRPacket::Channel * drChannel = (DRPacket::Channel*) currentPacketDataPtr;
  drChannel->ID = pmtID;   
  //  drChannel->RawIntegralCount = raw->GetRawIntegralCount();      
  int rawIntegralCount = raw->GetRawIntegralCount();
  drChannel->RawIntegralCount = 0;
  currentPacketDataPtr += sizeof(DRPacket::Channel); //move forward in our array
  packetSizeLeft -= sizeof(DRPacket::Channel); //update the free space available. 

  //Add the raw integrals
  for(int iRawIntegral = 0;
      iRawIntegral < rawIntegralCount;
      iRawIntegral++)
    {
      //Get the the current RAT rawIntegral
      RAT::DS::RawIntegral * rawIntegral = raw->GetRawIntegral(iRawIntegral);
      //Get a pointer to a DRPacket RawIntegral type that points to the next
      //available spot in the packet array
      DRPacket::RawIntegral * drRawIntegral = 
	(DRPacket::RawIntegral *) currentPacketDataPtr;
      
      //Copy data
      drRawIntegral->integral    = uint16_t(rawIntegral->GetIntegral());
      drRawIntegral->startTime   = rawIntegral->GetStartTime();
      drRawIntegral->width       = rawIntegral->GetWidth();      
      //move forward in our array and update the free space available. 
      currentPacketDataPtr += sizeof(DRPacket::RawIntegral); 
      packetSizeLeft -= sizeof(DRPacket::RawIntegral); 
      drChannel->RawIntegralCount++;
    }
  
  //update the drEvent's channel count
  drEvent->ChannelCount++;	    
  //  fprintf(stderr,"  Channel %u has %u raw integrals\n",
  //  drChannel->ID,
  //  drChannel->RawIntegralCount);
}

bool DRPusher::SendPacket()
{
  bool ret;
  packet->SetType(DCNetPacket::INTEGRATED_WINDOWS_FULL_PACKET);
  packet->SetData(packetDataPtr,
		  uint32_t(currentPacketDataPtr-packetDataPtr));	      
  if(DRAssembler.thread->GetOutPacketManager()->AddFull(iDCPacket))
    {
      //Clear our local packet info
      packet = NULL;
      packetDataPtr = NULL;
      currentPacketDataPtr = NULL; 
      drEvent = NULL;
      packetSizeLeft = 0;         
      iDCPacket = FreeFullQueue::BADVALUE;
      ret = true;
    }
  else
    ret= false;
  return ret ;
}

//void DRPusher::Print_sLocal(sLocal local)
//{
//  fprintf(stderr,"block: %p\n",local.block);
//  fprintf(stderr,"index: %d\n",local.index);
//  fprintf(stderr,"password: %lu\n",local.password);
//  fprintf(stderr,"time: %lu\n",local.time);
//  fprintf(stderr,"channelsProcessed: %u\n",local.channelsProcessed);
//}

void DRPusher::SendData()
{
  //Turn the data into DCPackets and send them off
  if(SendPacket())
    {
      //Clear drEvent
      drEvent = NULL;      
      //Loop over the events in the LocealDeque
      //      LocalDeque.Lock();            //=============GOT LOCK====================
      //      size_t sentCount = 0;
      size_t eventCount = LocalDeque.GetSize();
      for(size_t iDeque = 0; iDeque < eventCount;iDeque++)      
	{
	  sLocal currentEvent;	  
	  if(!LocalDeque.Get(currentEvent))
	    {
	      PrintError("Error reading out local DCDeque");
	      SendStop();
	    }
	  else
	    {
	      if(int(currentEvent.channelsProcessed) ==
		 currentEvent.block->ev->GetPMTCount())
		{
//		  printf("Event %d sent.  %d:%d\n",
//			 currentEvent.block->ev->GetEventID(),
//			 int(currentEvent.channelsProcessed),
//			 currentEvent.block->ev->GetPMTCount());
		  //Send worked, update the event's status
		  currentEvent.block->isSent = true;
		  
		  //release the event in the event store
		  int64_t retFEPC_mem;
		  retFEPC_mem = FEPC_mem->ReleaseStorageEvent(currentEvent.index,
							      currentEvent.password);
		  if(retFEPC_mem == returnDCVectorHash::OK)
		    {		      
		    }
		  else
		    {
		      //		      Print_sLocal(currentEvent);
		      //The only way for there to be an error here is that someone
		      //violated the lock and because of that we must stop. 
		      char * buffer = new char[100];
# if __WORDSIZE==64
		      sprintf(buffer,"Failed to release storage event: %ld\n",retFEPC_mem);
# else 
		      sprintf(buffer,"Failed to release storage event: %lld\n",retFEPC_mem);
# endif
		      PrintError(buffer);
		      SendStop();
		    }
		}
	      else
		{
//		  printf("Event %d not sent.  %d:%d\n",
//			 currentEvent.block->ev->GetEventID(),
//			 int(currentEvent.channelsProcessed),
//			 currentEvent.block->ev->GetPMTCount());
		  if(!LocalDeque.Add(currentEvent))
		    {
		      PrintError("Failed to add to local deque\n");
		      SendStop();
		    }
		}
	    }
	}
      //      LocalDeque.Unlock();         //============RELEASED LOCK===============
      //Now that everythign was sent we need to remove these elements. 
//      while(sentCount > 0)
//	{
//	  sLocal throwAwayEvent;
//	  LocalDeque.Get(throwAwayEvent);
//	  sentCount--;
//	}
      //Go back to Parse event we might have left half finished.
      ParseEvent(FEPC_mem->GetBadValue(),FEPC_mem->GetStorageBlankPassword());
    }
  else
    {
      //Loop over the events in the LocalDeque
      LocalDeque.LockDeque();       //=============GOT LOCK====================
      for(size_t iDeque = 0; iDeque < LocalDeque.GetSize();iDeque++)
	{
	  sLocal currentEvent = LocalDeque.At(iDeque);
	  if(currentEvent != LocalDeque.GetBadValue())
	    {
	      PrintError("Error reading out local DCDeque");
	      SendStop();
	    }
	  else
	    {
	      int64_t FEPCret;
	      FEPCret =FEPC_mem->ClearStorageEvent(currentEvent.block->eventID,
						   currentEvent.password);
	      
	      if(FEPCret != returnDCVectorHash::OK)
		{
		  //The only way for there to be an error here is that someone
		  //violated the lock and because of that we must stop. 
		  PrintError("Failed to clear failed storage event.");
		  SendStop();
		}
	      else if(!FEPC_mem->AddUnSent(currentEvent.index))
		{
		  //The only waf for a failure here is that the mutex lock has been
		  //turned off and we should shutdown.
		  char * buffer = new char[100];
# if __WORDSIZE==64
		  sprintf(buffer,"Unable to return event %ld to the UnSent DCQueue",currentEvent.block->eventID);
# else
		  sprintf(buffer,"Unable to return event %lld to the UnSent DCQueue",currentEvent.block->eventID);
# endif
		  
		  PrintError(buffer);
		  delete [] buffer;
		}      
	    }
	}
      LocalDeque.UnlockDeque();       //=============GOT LOCK====================
    }	 
}
