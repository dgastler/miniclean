#ifndef V1720_XML_H
#define V1720_XML_H

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <DCMySQL.h>
#include <Thread_XML.h>


class V1720_XML: public Thread_XML
{
 public:
  V1720_XML(DCMySQL * conn,int runNumber);
  virtual bool Generate_XML(xmlNode * threadsNode,
			    std::string pcName,
			    std::string threadType,
			    std::string threadName,
			    xmlNode * responseNode);
 private:
//  std::vector<std::vector<std::string> > V1720CommData;
//  std::vector<std::string> V1720CommFields;
//  std::vector<std::vector<std::string> > V1720BoardData;
//  std::vector<std::string> V1720BoardFields;
//  std::vector<std::vector<std::string> > V1720ChannelData;
//  std::vector<std::string> V1720ChannelFields;
//  int numRows;
//  int numFields;

};

#endif
