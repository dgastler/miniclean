#include <DCMessage.h>

void DCMessage::Message::SetSource(std::string name = "",
			  uint8_t * addr = NULL,
			  int32_t AFin = -1)
{       
  SourceName = name;
  SourceAF = AFin;
  if((AFin == -1) || (AFin == -2))
    {
      SourceAddr.resize(0);
    }
  else if(AFin == AF_INET)
    {
      Set(addr,sizeof(struct in_addr),SourceAddr);
    }
  else if(AFin == AF_INET6)
    {
      Set(addr,sizeof(struct in6_addr),SourceAddr);
    }
}


void DCMessage::Message::GetSource(std::string & name,
			  std::vector<uint8_t> & addr, 
			  int32_t &AFin)
{
  name = SourceName;
  addr = SourceAddr;
  AFin = SourceAF;
}

void DCMessage::Message::SetDestination(std::string name = "",
			       uint8_t * addr = NULL,
			       int32_t AFin = -1)
{       
  DestinationName = name;
  DestinationAF = AFin;
  if((AFin == -1) || (AFin == -2))
    {
      DestinationAddr.resize(0);
    }
  else if(AFin == AF_INET)
    {
      Set(addr,sizeof(struct in_addr),DestinationAddr);
    }
  else if(AFin == AF_INET6)
    {
      Set(addr,sizeof(struct in6_addr),DestinationAddr);
    }
}


void DCMessage::Message::GetDestination(std::string & name,
			       std::vector<uint8_t> & addr, 
			       int32_t &AFin)
{
  name = DestinationName;
  addr = DestinationAddr;
  AFin = DestinationAF;
}


void DCMessage::Message::Clear()
{
  type = BLANKMESSAGE;
  timeStamp = 0;
  SourceName.assign("");
  SourceAddr.clear();
  SourceAF = -1;
  DestinationName.assign("");
  DestinationAddr.clear();
  DestinationAF = -1;
  Data.clear();
}


void DCMessage::Message::CopyObj(const DCMessage::Message &rhs)
{
  type = rhs.type;
  timeStamp = rhs.timeStamp;
  Data = rhs.Data;
  //Source information
  SourceName = rhs.SourceName;
  SourceAddr = rhs.SourceAddr;
  SourceAF   = rhs.SourceAF;
  //Destination data
  DestinationName = rhs.DestinationName;
  DestinationAddr = rhs.DestinationAddr;
  DestinationAF   = rhs.DestinationAF;
}

void DCMessage::Message::Set(const void * in,size_t bytes,std::vector<uint8_t> & store)
{
  //Make our vector large enough;
  store.resize(bytes);
  if(bytes >0)
    {
      //Copy the data from inData to Data
      memcpy((void *) &(store[0]),in,bytes);
    }
}

void DCMessage::Message::Add(const void * in, size_t bytes,std::vector<uint8_t> & store)
{
  //Make room for copy
  store.resize(store.size() + bytes);
  if(bytes > 0)
    {
      memcpy((uint8_t *)&(store[0]) + (store.size() - bytes),in,bytes);
    }
}
