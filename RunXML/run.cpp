#include <XML_Factory.h>

#include <DCMySQL.h>

#include <iostream>
#include <fstream>
#include <vector>


int main()
{
  //===================
  //Set the run number
  //===================
  int runNumber=0;

  DCMySQL mysqlConn;
  mysqlConn.SetConnect("cleanpc6.bu.edu","control_user","27K2boil","daq_interface");
  mysqlConn.OpenConnect();

  XML_Factory xmlFactory(&mysqlConn);

  std::string xml = xmlFactory.LoadRun(runNumber);
  
  std::ofstream outFile("RunControl.xml");
  
  //std::cout << xml << std::endl;
  outFile << xml;
  outFile.close();

  return 0;
}
