#ifndef __LEVEL__
#define __LEVEL__
namespace Level
{
  enum Type
  {
    BLANK = 0,
    //Bad event levels
    STORAGE_COLLISION,
    STORAGE_EMPTY,
    TOO_MANY_WFDS,
    LVDS_MISMATCH,
    SEND_FAILED,
    MISSING_EVENT,
    //Reduction levels
    UNKNOWN,
    CHAN_FULL_WAVEFORM ,
    CHAN_ZLE_WAVEFORM,
    CHAN_ZLE_INTEGRAL,
    CHAN_PROMPT_TOTAL,
    EVNT_FULL_WAVEFORM,
    EVNT_ZLE_WAVEFORM,
    EVNT_ZLE_INTEGRAL,
    EVNT_PROMPT_TOTAL,
    SAVE_NOTHING,
    IN,
    OUT,
    //Hardware triggers
    VETO,
    EVNT,
    COMP,
    BUTN,
    CAL0,
    CAL1,
    CAL2,
    PERI,
    //Raw event rate
    RAW_EVENT,
    COUNT
  };
  const char * GetTypeName(Level::Type type);
  Level::Type GetType(const char * name);
};
#endif
