#!/bin/bash
echo
echo
echo
echo
echo
echo
echo ====================================
echo Generating progress plot
echo ====================================
pages=$(pdfinfo -meta MiniCLEAN.pdf | grep ages | awk '{print $2}')
time=$(date +%s)
echo $time $pages >> progress.dat
gnuplot 'makeProgress.gnu'
