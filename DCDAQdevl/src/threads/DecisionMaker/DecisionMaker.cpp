#include <DecisionMaker.h>

DecisionMaker::DecisionMaker(std::string Name)
{
  Ready = false;
  SetType("DecisionMaker");
  SetName(Name);
  reductionLevelCount.resize(ReductionLevel::COUNT,0);
  currentTime = time(NULL);
  lastTime = currentTime;
  statfprompt = 0;
  statpromptQ = 0;
  statearliestWindowTime = 0;
  statlatestWindowTime = 0;
  stattotalQ =0;
  rootfile = TFile::Open("test.root","RECREATE");
  for(int i = 0 ; i< ReductionLevel::COUNT;i++)
    {
      TH2F * temp = new TH2F("NAME","NAME",100,0,300000,100,0,1);
      hist.push_back(temp);
    }
}

DecisionMaker::~DecisionMaker()
{
  for(int i = 0 ; i< ReductionLevel::COUNT;i++)
    {
      hist[i]->Write();
    }
  rootfile->Close();
}

bool DecisionMaker::Setup(xmlNode * SetupNode,
			  std::map<std::string,MemoryManager*> &MemManager)
{
  bool ret = true;
  //==============================================================
  //Load the DRPC memory manager
  //==============================================================
  if(!FindMemoryManager(DRPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
 
  //If everything in setup worked, then set Ready to true;
  if(ret == true)
    {
      Ready = true;
    }
  return(ret);
}

void DecisionMaker::ProcessTimeout()
{
  //Get current Time
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      float TimeStep = difftime(currentTime,lastTime);
      DCMessage::TimedCount  timedCount;      
      DCMessage::Message message;
      for(unsigned int iLevel = 0; 
	  iLevel < reductionLevelCount.size();
	  iLevel++)
	{
	  sprintf(timedCount.text,"Reduction level %d count",iLevel);
	  timedCount.ID = iLevel;
	  timedCount.count = reductionLevelCount[iLevel];
	  timedCount.dt = TimeStep;
	  message.SetDataStruct(timedCount);
	  message.SetType(DCMessage::STATISTIC);
	  SendMessageOut(message,false);
	  reductionLevelCount[iLevel] = 0;
	}
      DCMessage::SingleDouble  singleDouble;
      sprintf(singleDouble.text,"Average fprompt: ");
      singleDouble.value = statfprompt/fpromptCount;
      message.SetDataStruct(singleDouble);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      statfprompt = 0;

      sprintf(singleDouble.text,"Average Earliest Window Time: ");
      singleDouble.value = statearliestWindowTime/fpromptCount;
      message.SetDataStruct(singleDouble);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      statearliestWindowTime = 0;
	
     sprintf(singleDouble.text,"Average Latest Window Time: ");
     singleDouble.value = statlatestWindowTime/fpromptCount;
     message.SetDataStruct(singleDouble);
     message.SetType(DCMessage::STATISTIC);
     SendMessageOut(message,false);
     statlatestWindowTime = 0;
      
     sprintf(singleDouble.text,"Average Total Charge: ");
     singleDouble.value = stattotalQ/fpromptCount;
     message.SetDataStruct(singleDouble);
     message.SetType(DCMessage::STATISTIC);
     SendMessageOut(message,false);
     stattotalQ = 0;
      
     sprintf(singleDouble.text,"Average Prompt Charge: ");
     singleDouble.value = statpromptQ/fpromptCount;
     message.SetDataStruct(singleDouble);
     message.SetType(DCMessage::STATISTIC);
     SendMessageOut(message,false);
     statpromptQ = 0;
     
     fpromptCount=0;
     //record time
     lastTime = currentTime;
    }
}
