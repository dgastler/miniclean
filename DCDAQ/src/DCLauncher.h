#ifndef __DCLAUNCHER__
#define __DCLAUNCHER__

//System includes
#include <iostream>
#include <vector>
#include <map>
#include <string>


//DCDAQ includes
#include <DCThread.h>
#include <MemoryManager.h>
#include <xmlHelper.h>

class DCLauncher
{
 public:
  DCLauncher(){};
  ~DCLauncher(){Shutdown();};
  uint32_t Launch(std::string config);
  void Shutdown();

  const std::map<int,int> & GetFDMap();
  void GetFDSets(fd_set & retReadSet,
		 fd_set & retWriteSet,
		 fd_set & retExceptionSet,
		 int    & retMaxFDPlusOne);
  
  const std::vector<DCThread *> & GetThreads() {return(thread);}
  
  enum 
  {
    SETUP_OK = 0x0,
    BAD_XML = 0x1,
    FAIL = 0x2,

    //Memory manager errors
    MM_TOO_MANY   = 0x10000,  //too many mm to set up in one command
    MM_XML_TYPE   = 0x20000,  //Bad type xml
    MM_XML_NAME   = 0x30000,  //Bad name xml
    MM_UNKNOWN    = 0x40000,  //Unknown memory manager type
    MM_NEW_FAIL   = 0x50000,  //MM creation failed
    MM_ALLOC_FAIL = 0x60000,  //MM memory allocation failed
    MM_DUP        = 0x70000,      //name collision
    MM_NOT_FOUND  = 0x80000,  //Can't find the MM in the XML (WTF?)

    THREAD_TOO_MANY     = 0x100000,   //too many threads to set up in one command
    THREAD_MISSING_MM   = 0x200000,   //One of the requested memory managers is missing
    THREAD_MISSING_NAME = 0x300000,   //No name for this thread
    THREAD_MISSING_TYPE = 0x400000,   //No name for this thread
    THREAD_SETUP        = 0x500000,   //Error in thread setup
    THREAD_UNKNOWN      = 0x600000,   //unknown thread type
    THREAD_BAD_NODE     = 0x700000,   //THis shouldn't happen@!
  };

 private:
  //Copy constructors (not allowed)(not implimented)
  DCLauncher(const DCLauncher & rhs);
  DCLauncher & operator=(const DCLauncher &rhs);
  
  //Setup functions
  uint32_t SetupMemoryManagers(xmlNode * MMNode,
			       //This is the map of newly created memory 
			       //managers that will be merged into 
			       //memoryManager if the thread setup works. 
			       std::map<std::string,MemoryManager *> &tempMemoryManager);
  
  uint32_t SetupThreads(xmlNode * ThreadNode,
			//vector of to be made threads
			//will be merged into threads if everything
			//works. 
			std::vector<DCThread *> &tempThread,
			//This is the map of newly created memory 
			//managers that will be merged into 
			//memoryManager if the thread setup works. 
			std::map<std::string,MemoryManager *> &tempMemoryManager);

  bool AddMemoryManager(std::map<std::string,MemoryManager *> &MM);
  bool AddThread(std::vector<DCThread *> & T);

  void DeleteMemoryManagerMap(std::map<std::string,MemoryManager *> &MM);
  void DeleteThreadVector(std::vector<DCThread *> & T,bool verbose = false);
  
  //Select information from threads
  int MaxFDPlusOne;                    //Current FDmax plus one for select
  std::map<int,int> FDtoThreadMap;     //Current mapping between FD and thread
  fd_set ReadSet;                      //Current fd sets
  fd_set WriteSet;
  fd_set ExceptionSet;

  void UpdateFD();


  std::map<std::string,MemoryManager *> memoryManager;
  std::vector<DCThread*> thread;
};

#endif
