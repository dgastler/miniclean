#ifndef __EVBLOCK__
#define __EVBLOCK__

#include <vector>
#include "RAT/DS/EV.hh"

struct EVBlock
{
  RAT::DS::EV * ev;
  int8_t reductionLevel;
  std::vector<uint16_t> WFD;
};
#endif
