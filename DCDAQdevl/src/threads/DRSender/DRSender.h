#ifndef __DRSENDER__
#define __DRSENDER__
/*
This thread takes full events with reduced information and makes a decision
about what data will be saved. 
 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <DRPCRatManager.h>

#include <ServerConnection.h>
#include <Connection.h>
#include <DRDecisionPacket.h>

namespace SendDecisionCode
{
  enum
  {
    SENT  = 0,
    //Failed connection errors should always be last
    //this is so we can check for error > FAILED_CONNECTION_0
    FAILED_CONNECTION_0 = 1,
    FAILED_CONNECTION_1 = FAILED_CONNECTION_0 + 1,
    FAILED_CONNECTION_2 = FAILED_CONNECTION_1 + 1,
    FAILED_CONNECTION_3 = FAILED_CONNECTION_2 + 1,
    FAILED_CONNECTION_4 = FAILED_CONNECTION_3 + 1,
    FAILED_CONNECTION_5 = FAILED_CONNECTION_4 + 1,
    FAILED_CONNECTION_6 = FAILED_CONNECTION_5 + 1,
    FAILED_CONNECTION_7 = FAILED_CONNECTION_6 + 1    
  };
}

class DRSender : public DCThread 
{
 public:
  //==============================================================
  //DRSender.cpp
  //==============================================================
  DRSender(std::string Name);
  ~DRSender();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  void PrintStatus();
  //==============================================================
  //DRSender_Run.cpp
  //==============================================================
  virtual void MainLoop();
 private:

  //==============================================================
  //DRSender_Run.cpp
  //==============================================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout(){};
  virtual void ThreadCleanUp();

  //==============================================================
  //DRSender_Processing.cpp
  //==============================================================
  int SendDecision(DRPCBlock * event);

  //==============================================================
  //DRSender_Net.cpp
  //==============================================================
  bool ProcessNewConnection();
  void DeleteConnection(unsigned int iConn);
  virtual void ProcessChildMessage(size_t iConn);

  //==============================================================
  //Network connection data members
  //==============================================================
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  ServerConnection server;

  //==============================================================
  //Memory manager interface
  //==============================================================
  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;

  //==============================================================
  //Statistics
  //==============================================================
  time_t lastTime;
  time_t currentTime;

};
#endif
