#include "HistViewer.h"
#include <TApplication.h>

HistViewer::HistViewer()
{}

HistViewer::~HistViewer()
{

  delete windowFrame;
  delete histECan;
  delete controlBar;
  delete oneHourButton;
  delete fiveHourButton;
  delete tenHourButton;
  delete oneDayButton;
  delete combineButton;
  delete layoutHint;
  delete layoutHintBar;
  delete oneHourHist;
  delete fiveHourHist;
  delete tenHourHist;
  delete oneDayHist;
  delete customHist;
  if(CustomWindow)
    {
      delete CustomWindow;
    }
  mysql_close(mysqlConn);
}

HistViewer::HistViewer(const TGWindow *p,UInt_t w, UInt_t h) : TGMainFrame(p,w,h)
{
  
  width=w;
  height=h;
  CustomWindow=NULL;
  layoutHint = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY);
  layoutHintBar=new TGLayoutHints(kLHintsExpandY);
  ActivePlot=combine;
  SetupGUI();
  SetupHists();
  SetupMySQL();
  CheckMySQL();
 
  

}

void HistViewer::SetupGUI()
{

  SetWindowName("MiniCLEAN Run Histograms\n");

  windowFrame=new TGHorizontalFrame(this,width, height);

  AddFrame(windowFrame,layoutHint);

  UInt_t controlwidth=300;
 
  controlBar=new TGVerticalFrame(windowFrame,controlwidth,height,kFixedWidth);
  windowFrame->AddFrame(controlBar,layoutHintBar);
  oneHourButton=new TGTextButton(controlBar,"1 Hour");
  oneHourButton->Connect("Clicked()","HistViewer",this,"oneHourActive()");
  controlBar->AddFrame(oneHourButton,layoutHint);
  fiveHourButton=new TGTextButton(controlBar,"5 Hours");
  fiveHourButton->Connect("Clicked()","HistViewer",this,"fiveHourActive()");
  controlBar->AddFrame(fiveHourButton,layoutHint);
  tenHourButton=new TGTextButton(controlBar,"10 Hours");
  tenHourButton->Connect("Clicked()","HistViewer",this,"tenHourActive()");
  controlBar->AddFrame(tenHourButton,layoutHint);
  oneDayButton=new TGTextButton(controlBar,"1 Day");
  oneDayButton->Connect("Clicked()","HistViewer",this,"oneDayActive()");
  controlBar->AddFrame(oneDayButton,layoutHint);
  combineButton=new TGTextButton(controlBar,"All Four");
  combineButton->Connect("Clicked()","HistViewer",this,"combineActive()");
  controlBar->AddFrame(combineButton,layoutHint);
  customButton=new TGTextButton(controlBar,"Custom Date/Time");
  customButton->Connect("Clicked()","HistViewer",this,"customTime()");
  controlBar->AddFrame(customButton,layoutHint);
  ResetAxesButton=new TGTextButton(controlBar,"Reset Axes");
  ResetAxesButton->Connect("Clicked()","HistViewer",this,"ResetAxes()");
  controlBar->AddFrame(ResetAxesButton,layoutHint);
  histECan=new TRootEmbeddedCanvas("Histograms",windowFrame,width-controlwidth,height);
  windowFrame->AddFrame(histECan,layoutHint);
  histTCanvas=histECan->GetCanvas();

  Layout();
  MapSubwindows();
  MapWindow();
}

void HistViewer::SetupHists()
{

  oneHourHist=new TH1F("One Hour","One Hour",92,-0.5,91.5);
  oneHourHist->SetStats(kFALSE);
  oneHourHist->GetXaxis()->SetTitle("Channel ID");
  oneHourHist->GetYaxis()->SetTitle("ZLE Count/1000 Events");
  oneHourHist->GetYaxis()->SetTitleOffset(1.4);

  fiveHourHist=new TH1F("Five Hour","Five Hour",92,-0.5,91.5);
  fiveHourHist->SetStats(kFALSE);
  fiveHourHist->GetXaxis()->SetTitle("Channel ID");
  fiveHourHist->GetYaxis()->SetTitle("ZLE Count/1000 Events");
  fiveHourHist->GetYaxis()->SetTitleOffset(1.4);

  oneDayHist=new TH1F("One Day","One Day",92,-0.5,91.5);
  oneDayHist->SetStats(kFALSE);
  oneDayHist->GetXaxis()->SetTitle("Channel ID");
  oneDayHist->GetYaxis()->SetTitle("ZLE Count/1000 Events");
  oneDayHist->GetYaxis()->SetTitleOffset(1.4);

  tenHourHist=new TH1F("Ten Hour","Ten Hour",92,-0.5,91.5);
  tenHourHist->SetStats(kFALSE);
  tenHourHist->GetXaxis()->SetTitle("Channel ID");
  tenHourHist->GetYaxis()->SetTitle("ZLE Count/1000 Events");
  tenHourHist->GetYaxis()->SetTitleOffset(1.4);
 
  customHist=new TH1F("Custom Time","CustomTime",92,-0.5,91.5);
  customHist->SetStats(kFALSE);
  customHist->GetXaxis()->SetTitle("Channel ID");
  customHist->GetYaxis()->SetTitle("ZLE Count/1000 Events");
  customHist->GetYaxis()->SetTitleOffset(1.4);

}

void HistViewer::SetupMySQL()
{

  mysqlConn=mysql_init(NULL);
  mysqlConn=mysql_real_connect(mysqlConn,"127.0.0.1","control_user","27K2boil","astro_ctrl",9999,NULL,0);
  mysqlTimer=new TTimer(0,kFALSE);
  mysqlTimer->Connect("Timeout()","HistViewer",this,"CheckMySQL()");
  checktime=300;
  mysqlTimer->Start(checktime*1000);
 

}

void HistViewer::CheckMySQL()
{
  CheckMySQL(-1,-1);
}


void HistViewer::CheckMySQL(int startTime=-1,int endTime=-1)
{

   if(mysqlConn)
    {
      
      if(startTime<0)
	{
	  std::stringstream query;
	  MYSQL_RES *result=NULL;
	  query<<"select unix_timestamp();";
	  if(mysql_real_query(mysqlConn,query.str().c_str(),query.str().size())!=0)
	    {
	      fprintf(stderr,query.str().c_str());
	      fprintf(stderr,"Error getting ZLE Counts from MySQL\n");
	      return;
	    }
	  result=mysql_store_result(mysqlConn);
	  MYSQL_ROW row=mysql_fetch_row(result);
	  int time_now=atoi(row[0]);

	  //time_now=1394982220; //for testing when not running

	  mysql_free_result(result);
	  result=NULL;
	  for(size_t i=0;i<92;i++)
	    {
	      if(oneHourInfo.size()<i+1)
		{
		  std::vector<CountInfo> tmpInfo;
		  oneHourInfo.push_back(tmpInfo);
		}
	      if(fiveHourInfo.size()<i+1)
		{
		  std::vector<CountInfo> tmpInfo;
		  fiveHourInfo.push_back(tmpInfo);
		}
	      if(tenHourInfo.size()<i+1)
		{
		  std::vector<CountInfo> tmpInfo;
		  tenHourInfo.push_back(tmpInfo);
		}
	      if(oneDayInfo.size()<i+1)
		{
		  std::vector<CountInfo> tmpInfo;
		  oneDayInfo.push_back(tmpInfo);
		}
	      while(true)
		{
		  int count=0;
		  if(oneHourInfo[i].size()>0 && oneHourInfo[i].front().time<time_now-3600)
		    {
		      oneHourInfo[i].erase(oneHourInfo[i].begin());
		    }
		  else
		    {
		      count++;
		    }
		  if(fiveHourInfo[i].size()>0 && fiveHourInfo[i].front().time<time_now-3600*5)
		    {
		      fiveHourInfo[i].erase(fiveHourInfo[i].begin());
		    }
		  else
		    {
		      count++;
		    }
		  if(tenHourInfo[i].size()>0 && tenHourInfo[i].front().time<time_now-3600*10)
		    {
		      tenHourInfo[i].erase(tenHourInfo[i].begin());
		    }
		  else
		    {
		      count++;
		    }
		  if(oneDayInfo[i].size()>0 && oneDayInfo[i].front().time<time_now-3600*24)
		    {
		      oneDayInfo[i].erase(oneDayInfo[i].begin());
		    }
		  else
		    {
		      count++;
		    }
	      
		  if(count==4) break;
	      
		}
	      int timeLast;
	      if(oneDayInfo[i].size()>0)
		{
		  timeLast=oneDayInfo[i].back().time;
	      
		}
	      else
		{
		  timeLast=time_now-24*3600;
		}
	      query.str(std::string());
	      if(i<10)
		{
		  query<<"select * from sc_sens_ZLECOUNT_PMT_0"<<i<<" where time>"<<timeLast<<" limit 5000;";
		}
	      else
		{
		  query<<"select * from sc_sens_ZLECOUNT_PMT_"<<i<<" where time>"<<timeLast<<" limit 5000;";
		}
	      if(mysql_real_query(mysqlConn,query.str().c_str(),query.str().size())!=0)
		{
		  fprintf(stderr,query.str().c_str());
		  fprintf(stderr,"Error getting ZLE Counts from MySQL\n");
		  return;
		}
	      result=mysql_store_result(mysqlConn);
	      int numRows=mysql_num_rows(result);
	      double sumHour=0;
	      double sumFiveHour=0;
	      double sumTenHour=0;
	      double sumDay=0;
	  
	      for(int j=0;j<numRows;j++)
		{
		  MYSQL_ROW row=mysql_fetch_row(result);
	      
		  if(atoi(row[0])>time_now-3600)
		    {
		      CountInfo tmpInfo;
		      oneHourInfo[i].push_back(tmpInfo);
		      oneHourInfo[i].back().time=atoi(row[0]);
		      oneHourInfo[i].back().count=atof(row[1])*1000;
		    }
		  if(atoi(row[0])>time_now-3600*5)
		    {
		      CountInfo tmpInfo;
		      fiveHourInfo[i].push_back(tmpInfo);
		      fiveHourInfo[i].back().time=atoi(row[0]);
		      fiveHourInfo[i].back().count=atof(row[1])*1000;

		    }
		  if(atoi(row[0])>time_now-3600*10)
		    {
		      CountInfo tmpInfo;
		      tenHourInfo[i].push_back(tmpInfo);
		      tenHourInfo[i].back().time=atoi(row[0]);
		      tenHourInfo[i].back().count=atof(row[1])*1000;

		    }
		  if(atoi(row[0])>time_now-3600*24)
		    {
		      CountInfo tmpInfo;
		      oneDayInfo[i].push_back(tmpInfo);
		      oneDayInfo[i].back().time=atoi(row[0]);
		      oneDayInfo[i].back().count=atof(row[1])*1000;

		    }
	      
	      
	      
	      
		}
	      for(size_t k=0;k<oneDayInfo[i].size();k++)
		{
		  if(k<oneHourInfo[i].size())
		    {
		      sumHour+=oneHourInfo[i][k].count;
		    }
		  if(k<fiveHourInfo[i].size())
		    {
		      sumFiveHour+=fiveHourInfo[i][k].count;
		    }
		  if(k<tenHourInfo[i].size())
		    {
		      sumTenHour+=tenHourInfo[i][k].count;
		    }
		  sumDay+=oneDayInfo[i][k].count;
		}
	      if(oneHourInfo[i].size()>0)
		{
		  oneHourHist->SetBinContent(i+1,sumHour/(float)oneHourInfo[i].size());
		}
	      if(fiveHourInfo[i].size()>0)
		{
		  fiveHourHist->SetBinContent(i+1,sumFiveHour/(float)fiveHourInfo[i].size());
		}
	      if(tenHourInfo[i].size()>0)
		{
		  tenHourHist->SetBinContent(i+1,sumTenHour/(float)tenHourInfo[i].size());
		}
	      if(oneDayInfo[i].size()>0)
		{
		  oneDayHist->SetBinContent(i+1,sumDay/(float)oneDayInfo[i].size());
		}
	      
	      mysql_free_result(result);
	      result=NULL;
	    }
 


	  DrawHists();
	}
      else
	{
	  std::stringstream query;
	  MYSQL_RES *result=NULL;
	  for(size_t i=0;i<92;i++)
	    {
	      query.str(std::string());
	      if(i<10)
		{
		  query<<"select avg(value) from sc_sens_ZLECOUNT_PMT_0"<<i<<" where time>"<<startTime<<" and time<"<<endTime<<";";
		}
	      else
		{
		  query<<"select avg(value) from sc_sens_ZLECOUNT_PMT_"<<i<<" where time>"<<startTime<<" and time<"<<endTime<<";";
		}
	      if(mysql_real_query(mysqlConn,query.str().c_str(),query.str().size())!=0)
		{
		  fprintf(stderr,query.str().c_str());
		  fprintf(stderr,"Error getting ZLE Counts from MySQL\n");
		  return;
		}
	      result=mysql_store_result(mysqlConn);
	      int numRows=mysql_num_rows(result);
	      if(numRows==0) continue;
	      MYSQL_ROW row=mysql_fetch_row(result);

	      customHist->SetBinContent(i+1,atof(row[0])*1000);

	      mysql_free_result(result);
	      result=NULL;
	    }

	}
    }  
}

void HistViewer::DrawHists()
{
  

  histTCanvas->Clear();
  if(ActivePlot==combine)
    {
      histTCanvas->Divide(2,2);
      histTCanvas->cd(1);
      oneHourHist->Draw();
      histTCanvas->cd(2);
      fiveHourHist->Draw();
      histTCanvas->cd(3);
      tenHourHist->Draw();
      histTCanvas->cd(4);
      oneDayHist->Draw();
    }
  else if (ActivePlot==fiveHour)
    {
      histTCanvas->cd();
      fiveHourHist->Draw();
    }
  else if (ActivePlot==tenHour)
    {
      histTCanvas->cd();
      tenHourHist->Draw();
    }
  else if (ActivePlot==oneDay)
    {
      histTCanvas->cd();
      oneDayHist->Draw();
    }
  else if (ActivePlot==oneHour)
    {
      histTCanvas->cd();
      oneHourHist->Draw();
    }
  histTCanvas->Update();
  histTCanvas->Modified();
  histTCanvas->SetEditable(kTRUE);
}

void HistViewer::oneHourActive()
{
  ActivePlot=oneHour;
  DrawHists();
}

void HistViewer::fiveHourActive()
{
  ActivePlot=fiveHour;
  DrawHists();
}

void HistViewer::oneDayActive()
{
  ActivePlot=oneDay;
  DrawHists();
}

void HistViewer::tenHourActive()
{
  ActivePlot=tenHour;
  DrawHists();
}

void HistViewer::combineActive()
{
  ActivePlot=combine;
  DrawHists();
}

void HistViewer::customTime()
{
  if(!CustomWindow)
    {
      mysqlTimer->Stop();
      CustomWindow=new TGMainFrame(gClient->GetRoot(),width,height);
      customFrame =new TGVerticalFrame(CustomWindow,width,height);
      CustomWindow->AddFrame(customFrame,layoutHint);

      DateFrame=new TGHorizontalFrame(customFrame,width,100);
      customFrame->AddFrame(DateFrame,new TGLayoutHints(kLHintsExpandX));
      startLabel=new TGLabel(DateFrame,"Start");
      DateFrame->AddFrame(startLabel,new TGLayoutHints(kLHintsExpandX));
      DateStart=new TGNumberEntry(DateFrame,0.,-1,5,TGNumberFormat::kNESMDayYear);
      DateFrame->AddFrame(DateStart,new TGLayoutHints(kLHintsExpandX));
      TimeStart=new TGNumberEntry(DateFrame,0.,-1,5,TGNumberFormat::kNESHourMin);
      DateFrame->AddFrame(TimeStart,new TGLayoutHints(kLHintsExpandX));

      spaceLabel=new TGLabel(DateFrame,"      ");
      DateFrame->AddFrame(spaceLabel,new TGLayoutHints(kLHintsExpandX));
  
      endLabel=new TGLabel(DateFrame,"End");
      DateFrame->AddFrame(endLabel,new TGLayoutHints(kLHintsExpandX));
      DateEnd=new TGNumberEntry(DateFrame,0.,-1,5,TGNumberFormat::kNESMDayYear);
      DateFrame->AddFrame(DateEnd,new TGLayoutHints(kLHintsExpandX));
      TimeEnd=new TGNumberEntry(DateFrame,0.,-1,5,TGNumberFormat::kNESHourMin);
      DateFrame->AddFrame(TimeEnd,new TGLayoutHints(kLHintsExpandX));
      DrawButton=new TGTextButton(DateFrame,"Draw");
      DateFrame->AddFrame(DrawButton,new TGLayoutHints(kLHintsExpandX));
      DrawButton->Connect("Clicked()","HistViewer",this,"DrawCustomHist()");
      customECan=new TRootEmbeddedCanvas("custom Hist",customFrame,width,height-100);
      customFrame->AddFrame(customECan,layoutHint);
      customCanvas=customECan->GetCanvas();

      CustomWindow->Connect("CloseWindow()","HistViewer",this,"CustomWindowClosed()");
      CustomWindow->DontCallClose();

      CustomWindow->Layout();
      CustomWindow->MapSubwindows();
      CustomWindow->MapWindow();
    }
}

void HistViewer::DrawCustomHist()
{
  int Syear,Smonth,Sday,Shour,Smin,Ssec;
  int Eyear,Emonth,Eday,Ehour,Emin,Esec;
  DateStart->GetDate(Syear,Smonth,Sday);
  TimeStart->GetTime(Shour,Smin,Ssec);
  DateEnd->GetDate(Eyear,Emonth,Eday);
  TimeEnd->GetTime(Ehour,Emin,Esec);
  TTimeStamp start(Syear,Smonth,Sday,Shour,Smin,Ssec);
  int startTime=start.GetSec();
  TTimeStamp end(Eyear,Emonth,Eday,Ehour,Emin,Esec);
  int endTime=end.GetSec();
  
  CheckMySQL(startTime,endTime);
  customCanvas->cd();
  customCanvas->Clear();
  customHist->Draw();
  customCanvas->SetEditable();
  customCanvas->Update();
  customCanvas->Modified();
}


void HistViewer::ResetAxes()
{
  oneHourHist->GetXaxis()->SetRangeUser(-0.5,91.5);  
  oneHourHist->SetMaximum();
  oneHourHist->SetMinimum();
  fiveHourHist->GetXaxis()->SetRangeUser(-0.5,91.5);  
  fiveHourHist->SetMaximum();
  fiveHourHist->SetMinimum();
  tenHourHist->GetXaxis()->SetRangeUser(-0.5,91.5);  
  tenHourHist->SetMaximum();
  tenHourHist->SetMinimum();
  oneDayHist->GetXaxis()->SetRangeUser(-0.5,91.5);  
  oneDayHist->SetMaximum();
  oneDayHist->SetMinimum();
  customHist->GetXaxis()->SetRangeUser(-0.5,91.5);  
  customHist->SetMaximum();
  customHist->SetMinimum();
  DrawHists();

}
void HistViewer::CustomWindowClosed()
{
  // CustomWindow->Cleanup();
  delete CustomWindow;
  CustomWindow=NULL;
  delete customFrame;
  delete DateFrame;
  delete startLabel;
  delete DateStart;
  delete TimeStart;
  delete DateEnd;
  delete TimeEnd;
  delete spaceLabel;
  delete endLabel;
  delete DrawButton;
  delete customECan;

  mysqlTimer->Start(checktime*1000);
}
void HistViewer::CloseWindow()
{
  
  gApplication->Terminate();

}
