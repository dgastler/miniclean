#ifndef SPEEXPO
#define SPEEXPO

#include <iostream>
#include <math.h>

#include "xmlParser.h"


class SPEExpo
{
 private:

 public:
  // exp(m * x + b)
  float m;
  float b;

  SPEExpo();
  SPEExpo(char *);
  virtual ~SPE() {};
  int Set(char *);
  float SPE(float V) {return(exp(m * V + b));};
};

#endif
