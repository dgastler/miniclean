#ifdef __RATDS__
#include <DRPusher.h>

void DRPusher::CheckForNewConnection()
{
  if(IsReadReady(DRAssembler.GetSocketFD()) || 
     IsWriteReady(DRAssembler.GetSocketFD()))
    {
      //Remove the socketFDs from select
      //No matter what happens, we won't want to listen to them anymore.
      
      RemoveReadFD(DRAssembler.GetSocketFD());
      RemoveWriteFD(DRAssembler.GetSocketFD());      
      SetupSelect();

      //If the connect check fails
      if(!DRAssembler.AsyncConnectCheck())
	{
	  //Wait a timeout period and then try the connection again.
	  char buffer[100];
	  sprintf(buffer,"Can not connect to server %s: retry in %lds\n",
		  DRAssembler.GetRemoteAddress().GetAddrStr().c_str(),
		  GetSelectTimeoutSeconds());

	  PrintWarning(buffer);
	  DRAssembler.SetRetry();
	}
      //Our connection worked
      else
	{
	  //Set up the DCNetThread
	  if(!ProcessNewConnection())
	    {
	      PrintError("Failed creating DCNetThread!");
	      //We should fail!
	      SendStop();
	    }	  
	}
    }
}

bool DRPusher::ProcessNewConnection()
{  
  bool ret = true;
  if(DRAssembler.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(DRAssembler.GetFullPacketFD());
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(DRAssembler.GetMessageFD());      

      DCMessage::Message connectMessage;      
      connectMessage.SetType(DCMessage::NETWORK);
      connectMessage.SetDestination();
      NetworkStatus::Type status = NetworkStatus::CONNECTED;
      connectMessage.SetData(&(status),sizeof(status));
      SendMessageOut(connectMessage);

      std::string connMessage("Connected to DRAssembler at: ");
      connMessage += DRAssembler.GetRemoteAddress().GetAddrStr();
      Print(connMessage.c_str());
      //If Loop is true then we need to add the FEPCRatMemory manager
      //to the select set. 
      //It wouldn't have been allowed to be added in the absence of a 
      //net thread connection
      if(Loop)
	{
	  AddReadFD(FEPC_mem->GetUnSentFDs()[0]);      
	}
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return ret;
}
void DRPusher::DeleteConnection()
{

  //Remove the incoming packet manager fds from list
  RemoveReadFD(DRAssembler.GetFullPacketFD());
  //Remove the outgoing message queue from the DCThread
  RemoveReadFD(DRAssembler.GetMessageFD());

  RemoveReadFD(DRAssembler.GetSocketFD());
  RemoveWriteFD(DRAssembler.GetSocketFD());

  //stop listening to the FEPC memory manager, since we can't process anymore events
  RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
  SetupSelect();

  //Shutdown the thread
  DRAssembler.Shutdown();

}

void DRPusher::ProcessChildMessage()
{
  DCMessage::Message message = DRAssembler.GetMessageOut();
  if(message.GetType() == DCMessage::END_OF_THREAD)
    {
      DeleteConnection();
      SendStop();
    }
  else if(message.GetType() == DCMessage::ERROR)
    {
      ForwardMessageOut(message);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message);
    }
  else
    {
      PrintWarning("Unknown message from DCMessageClient's DCNetThread\n");
    }
}


//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool DRPusher::StartNewConnection()
{
  if(DRAssembler.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return true;
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  SendStop();
	  return false;
	}

    }
  else 
    {
      if(DRAssembler.GetSocketFD() >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (DRAssembler.GetSocketFD());
	  AddWriteFD(DRAssembler.GetSocketFD());
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  SendStop();
	  return false;
	}
    }
  return true;
}
#endif
