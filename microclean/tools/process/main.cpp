#include <iostream>
#include <sys/time.h>

#include "PEvent.h"
#include "Event.h"

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TextArray.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TF1.h"

#include "xmlParser.h"

#include "SPEFit.h"
#include "Integrate.h"
#include "WaveformProcessing.h"

#include <math.h>
#include <fstream>

using namespace std;

Bool_t XMLTag(TTree *inTextTree,TextArray* inRootText,TTree * TextTree, TextArray* RootText)
{
  //If this is the first file take it's DAQ xml files and put them into the out file
  inTextTree->GetEntry(0);
  char * TextTemp = inRootText->GetArray();
  int TextTempSize = 0;
  while(true)
    {
      if(TextTemp[TextTempSize] == '<' &&
	 TextTemp[TextTempSize+1] == '/' &&
	 TextTemp[TextTempSize+2] == 'D' &&
	 TextTemp[TextTempSize+3] == 'A' &&
	 TextTemp[TextTempSize+4] == 'Q' &&
	 TextTemp[TextTempSize+5] == '>')
	{
	  TextTempSize +=7;
	  break;
	}
      else
	TextTempSize++;
      if(TextTempSize > 10000)
	{
	  cerr << "stuck in loop" << endl;
	  return(false);
	}
      
    }
  char * TextTemp2 = new char[TextTempSize];
  for(int k = 0; k < TextTempSize -1;k++)
    {
      TextTemp2[k] = TextTemp[k];
    }
  TextTemp2[TextTempSize-1] = '\0';
  
  
  RootText->FillArray(TextTempSize,TextTemp2);
  TextTree->Fill();
  delete [] TextTemp2;
  
  
  inTextTree->GetEntry(1);
  TextTemp = inRootText->GetArray();
  TextTempSize = 0;
  while(true)
    {
      if(TextTemp[TextTempSize] == '<' &&
	 TextTemp[TextTempSize+1] == '/' &&
	 TextTemp[TextTempSize+2] == 'D' &&
	 TextTemp[TextTempSize+3] == 'A' &&
	 TextTemp[TextTempSize+4] == 'Q' &&
	 TextTemp[TextTempSize+5] == '>')
	{
	  TextTempSize +=7;
	  break;
	}
      else
	TextTempSize++;
      if(TextTempSize > 10000)
	{
	  cerr << "stuck in loop" << endl;
	  return(false);
	}
    }
  TextTemp2 = new char[TextTempSize];
  for(int k = 0; k < TextTempSize -1;k++)
    {
      TextTemp2[k] = TextTemp[k];
    }
  TextTemp2[TextTempSize-1] = '\0';
  
  RootText->FillArray(TextTempSize,TextTemp2);
  TextTree->Fill();
  delete [] TextTemp2;  
  return(true);
}

































int main(int argc, char **argv)
{
  Float_t Volts;
  Float_t seconds;
  Float_t C;

  Volts = (1.0/255.0);
  seconds = 2e-9;
  C = Volts * seconds * (1.0/50.0);

  struct timezone TimeZone;
  struct timeval StartTime;
  struct timeval EndTime;

  
  memset(&StartTime,0,sizeof(struct timeval));
  memset(&EndTime,0,sizeof(struct timeval));
  TimeZone.tz_minuteswest = 0;
  TimeZone.tz_dsttime = DST_NONE;


  if(argc < 3)
    {
      cout << argv[0] << "OutFile InFiles" << endl;
      return(0);
    }
  TFile   *    InFile = 0;
  TTree   *    tree = 0;
  Event   *    event = 0;
  TTree   *    inTextTree = 0;
  TextArray *  inRootText = 0;


  //  TFile   *    OutFile = new TFile(argv[1],"recreate");
  Bool_t Run1 = true;
  TFile * OutFile = new TFile(argv[1],"create");
  if(OutFile->IsZombie())
    {
      Run1 = false;
      OutFile->Open(argv[1],"update");
      printf("Run 2\n");
    }
  else
    {
      printf("Run 1\n");
      OutFile->SetCompressionLevel(9);
    }


  PEvent  *    pevent = new PEvent;
  TTree   *    ptree = new TTree("ptree","Run");
  TTree   *    TextTree = new TTree("TextTree","RunNotes");
  TextArray *  RootText = new TextArray;//new TextArray;
  ptree->SetAutoSave(100000);
  ptree->Branch("PEvent branch","PEvent",&pevent);//,16000,4);//,32000,0);
  TextTree->Branch("Run Notes","TextArray",&RootText);

  //  Int_t        Size  = 3;
  Int_t TotalEvents = 0;
  Int_t TotalEventNumber = 0;
  Int_t Skipped = 0;


  //Array with timing windows
  Int_t WindowEnds[8] = {-10,38,45,52,59,66,2500,7500};
  //check to see if we have new window ending parameters
  ifstream WindowEndsFile("./WindowEnds.dat");
  if(!WindowEndsFile.fail())
    {
      for(int i = 0; i < 8;i++)
	{
	  WindowEndsFile >> WindowEnds[i];
	}
    }
  printf("Using Windows: %d,%d,%d,%d,%d,%d,%d,%d\n",
	 WindowEnds[0],
	 WindowEnds[1],
	 WindowEnds[2],
	 WindowEnds[3],
	 WindowEnds[4],
	 WindowEnds[5],
	 WindowEnds[6],
	 WindowEnds[7]);
  Bool_t SubEvent = false;
  XMLNode DAQNode;

  vector<Float_t> PMTVoltageTriggers(100,1);

  //Raw level Histograms

  TH1F * PMT0Charge = 0;
  TH1F * PMT1Charge = 0;
  TH1F * TotalCharge= 0;
  TH1F * PMTasym    = 0;
  TH2F * PMT0SPE    = 0;
  TH2F * PMT1SPE    = 0;

  if(Run1)
    {
      PMT0Charge  = new TH1F("PMT0Charge","PMT0 Charge",100,0,1e-9);
      PMT0Charge->GetXaxis()->SetTitle("Charge(C)");
      
      PMT1Charge  = new TH1F("PMT1Charge","PMT1 Charge",100,0,1e-9);
      PMT0Charge->GetXaxis()->SetTitle("Charge(C)");
      
      TotalCharge = new TH1F("TotalCharge","Total Charge",100,0,2e-9);
      TotalCharge->GetXaxis()->SetTitle("Charge(C)");
      
      PMTasym     = new TH1F("PMTasym","PMT asymmetry",100,-1,1);
      
      PMT0SPE     = new TH2F("PMT0SPE","PMT SPE",300,0,300*C,300,0,300*Volts);
      PMT0SPE->GetXaxis()->SetTitle("Charge(C)");
      PMT0SPE->GetYaxis()->SetTitle("Volts");
      
      PMT1SPE     = new TH2F("PMT1SPE","PMT SPE",300,0,300*C,300,0,300*Volts);
      PMT1SPE->GetXaxis()->SetTitle("Charge(C)");
      PMT1SPE->GetYaxis()->SetTitle("Volts");
    }
  else
    {
      int NameBufferSize = 1000;
      char * NameBuffer = new char[NameBufferSize];
      InFile = new TFile(argv[2],"READ","",9);
      for(int Channel = 0;Channel < 100;Channel++)
	{
	  //	  PMTVoltageTriggers.push_back(2);
	  sprintf(NameBuffer,"PMT%dSPE",Channel);
	  TH2F * Pointer = (TH2F*) InFile->Get(NameBuffer);
	  if(Pointer)
	    {
	      PMTVoltageTriggers[Channel] = ((TLine*)((TList*) Pointer->GetListOfFunctions())->At(0))->GetY1()/Volts;
	      printf("Channel %d tigger voltage = %f\n",Channel,PMTVoltageTriggers[Channel]);
	    }
	}
      InFile->Close();
      delete InFile;
      delete [] NameBuffer;
    }
  
  //File loop
  for(int filenumber = 2; filenumber < argc;filenumber++)
    {
      gettimeofday(&StartTime,&TimeZone);
      //Open file in of type "base"filenumber.root
      cout << "Opening file: " << argv[filenumber] << endl;
      InFile                 = new TFile(argv[filenumber],"READ","",9);
      tree                   = (TTree*) InFile->Get("tree");
      inTextTree             = (TTree*) InFile->Get("TextTree");
      event                  = 0;
      inRootText             = 0;
      Int_t NEvents          = tree->GetEntries();
      tree->SetBranchAddress("Event branch",&event);
      inTextTree->SetBranchAddress("Run Notes",&inRootText);
      tree->GetEntry(0);



      
      

      Int_t NumberOfChannels               = event->NRaw();
      Int_t *ChannelMap                    = new Int_t[NumberOfChannels];
      Int_t *PMTMap                        = new Int_t[NumberOfChannels];
      Float_t * ChannelIDs                 = new Float_t[NumberOfChannels];
      Int_t *ChannelSizes                  = new Int_t[NumberOfChannels];
      Float_t *Offsets                     = new Float_t[NumberOfChannels];
      Float_t *OffsetSigmas                = new Float_t[NumberOfChannels];
      Int_t NumberOfPMTs                   = 0;
      Bool_t *FixGain                      = new Bool_t[NumberOfChannels];
      Bool_t Chan3                         =false;

      //Copy XML config files
      //Find out channel ordering
      if(filenumber == 2)
	{
	  OutFile->cd();
	  if(!XMLTag(inTextTree,inRootText,TextTree,RootText))
	    {
	      return(0);
	    }
	  InFile->cd();
	  //Read out the XML setup string and figure out how the channels are setup;
	  DAQNode=XMLNode::parseString(RootText->GetArray(),"DAQ");
	}

      Int_t i = 0;
      //fprintf(stderr,"%d\n",DAQNode.getChildNode("Setup").nChildNode());
      for(Int_t WFD =0; WFD < DAQNode.getChildNode("Setup").nChildNode();WFD++)
	{
	  for(Int_t Channel = 0; Channel < DAQNode.getChildNode("Setup").getChildNode("WFD",WFD).nChildNode();Channel++)
	    {
	      std::string Read(DAQNode.getChildNode("Setup").getChildNode("WFD",WFD).getChildNode("Channel",Channel).getAttribute("PMTID"));
	      std::string Nothing("");
	      if(Read != Nothing)
		{
		  ChannelMap[i] = i;
		  ChannelIDs[i] = atof(DAQNode.getChildNode("Setup").getChildNode("WFD",WFD).getChildNode("Channel",Channel).getAttribute("PMTID"));
		  if(ChannelIDs[i] == floor(ChannelIDs[i]))
		    {
		      NumberOfPMTs++;
		    }
		  i++;
		  if(i == NumberOfChannels)
		    {
		      Channel = DAQNode.getChildNode("Setup").getChildNode("WFD",WFD).nChildNode();
		      WFD = DAQNode.getChildNode("Setup").nChildNode();
		    }
		}
	    }
	}
      //Now we have filled ChannelMap and ChannelIDs out of order
      //Now do a quick(slow) bubble sort to put them both in order
      for(int i = 0; i < NumberOfChannels;i++)
	{
	  for(int j = i+1;j < NumberOfChannels;j++)
	    {
	      if(ChannelIDs[j] < ChannelIDs[i])
		{
		  Int_t Temp = ChannelMap[j];
		  ChannelMap[j] = ChannelMap[i];
		  ChannelMap[i] = Temp;
		  Float_t fTemp = ChannelIDs[j];
		  ChannelIDs[j] = ChannelIDs[i];
		  ChannelIDs[i] = fTemp;
		}
	    }
	}
      
  
      if(NumberOfPMTs == 3)
	{
	  Chan3 = true;
	}
      
      Float_t ** Channels                  = (Float_t **) new Float_t*[NumberOfChannels];
      for(Int_t Channel = 0; Channel < NumberOfChannels; Channel++)
	{
	  ChannelSizes[Channel] = event->GetSize(ChannelMap[Channel]);
	  Channels[Channel] = new Float_t[ChannelSizes[Channel]];
	}
      
      bool ValidEvent = true;

      //Swap channels if needed
      ifstream Swapfile("./SwapFile.dat");
      if(!Swapfile.fail())
	{
	  char * SwapBuffer = new char[1000];
	  while(!Swapfile.eof())
	    {
	      Swapfile.getline(SwapBuffer,1000);
	      std::string SwapString(SwapBuffer);
	      int Swap1 = atoi((SwapString.substr(0,SwapString.find(":"))).c_str());
	      int Swap2 = atoi((SwapString.substr(SwapString.find(":") + 1,SwapString.size()-1)).c_str());
	      printf("%s      %s\n",SwapBuffer,SwapString.c_str());
	      printf("Swapping channels %d and %d.\n",Swap1,Swap2);
	      int saveMap = ChannelMap[Swap1];
	      float saveID = ChannelIDs[Swap1];
	      ChannelMap[Swap1] = ChannelMap[Swap2];
	      ChannelIDs[Swap1] = ChannelIDs[Swap2];
	      ChannelMap[Swap2] = saveMap;
	      ChannelIDs[Swap2] = saveID;
	    }
	  delete SwapBuffer;
	}
      Swapfile.close();
      //Loop over events in file
      for(int EventN = 0; EventN < NEvents; EventN++)
      	{
	  SubEvent = false;
	  pevent->SetSize(NumberOfPMTs);
	  pevent->EventNumber(TotalEventNumber);
	  TotalEventNumber++;

	  tree->GetEntry(EventN);
	  


	  
	  	  
	  //Read in Waveform and check if it needs to be merged with the second
	  // gain scale.

	  for(Int_t Channel = 0; Channel < NumberOfChannels;Channel++)
	    {
	      Offsets[Channel] = Offset(event,ChannelMap[Channel],1000,OffsetSigmas + Channel);
	      FixGain[Channel] = CopyWaveform(event,ChannelMap[Channel],Channels[Channel],ChannelSizes[Channel],Offsets[Channel]);
	      //Record the offset and offset sigma
	      pevent->Offset(Channel,Offsets[Channel]);
	      pevent->OffsetSigma(Channel,OffsetsSigma[Channel]);
	    }

	  //If waveform is large enough merge the high and low gains.  Otherwise just use high gain. 	  
	  for(int PMT =0,Channel = 0; (PMT < NumberOfPMTs) && (Channel < NumberOfChannels);PMT++)
	    {
	      if(Channel < (NumberOfChannels - 1))
		{
		  //If Channel is an integer AND Channel +1 is not an integer AND Channel + 1 rounded down is Channel and Channel has a gain that should be fixed.
		  // merge waveforms Channel and Channel+1
		  if( (ChannelIDs[Channel] == floor(ChannelIDs[Channel])) && (ChannelIDs[Channel+1] != floor(ChannelIDs[Channel+1])) && (ChannelIDs[Channel] == floor(ChannelIDs[Channel+1])) && FixGain[Channel]  )
		    {
		      pevent->Shift(PMT,MergeWaveforms(ChannelSizes[Channel],Channels[ChannelMap[Channel]],Channels[ChannelMap[Channel + 1]],Offsets[Channel],10.0));
		      //We just addressed two channels on the last line so Channel++ here and then again at the end of the loop
		      Channel++;
		    }
		  //Fill shift with zero
		  else
		    {
		      pevent->Shift(PMT,0);
		    }
		}
	      //Fill shift with zero
	      else
		{
		  pevent->Shift(PMT,0);
		}
	      //Go the the next channel
	      Channel++;
	    }



	  for(int PMT = 0,Channel = 0;(PMT < NumberOfPMTs) && (PMT < NumberOfChannels);Channel++)
	    {
	      //	      fprintf(stderr,"Channel: %d MAP: %d PMT: %d",Channel,ChannelMap[Channel],PMT);
	      if(ChannelIDs[Channel] == floor(ChannelIDs[Channel]))
		{

		  PMTMap[PMT] = ChannelMap[Channel]; // Channel
		  //		  fprintf(stderr," PMTMap:%d\n",PMTMap[PMT]);
		  PMT++;
		}
	    }
	  
	  
	  ValidEvent = true;
	  
	  
	  if(NumberOfPMTs == 3)
	    {
	      //Delay Time   TOF!
	      pevent->TOF(0,(2e-6)*(Find_Waveform_Start(ChannelSizes[PMTMap[0]],Channels[PMTMap[0]]) - Find_Waveform_Start(ChannelSizes[PMTMap[0]],Channels[PMTMap[2]])));
	      pevent->TOF(1,(2e-6)*(Find_Waveform_Start(ChannelSizes[PMTMap[1]],Channels[PMTMap[1]]) - Find_Waveform_Start(ChannelSizes[PMTMap[1]],Channels[PMTMap[2]])));
	    }
	  else
	    {
	      pevent->TOF(0,0);
	      pevent->TOF(1,0);
	      //pevent->TOFAve = 0;
	    }
	  
	  Int_t Peak[3] = {0,0,0};
	  Int_t SubPeak[3] ={0,0,0};
	  //Find waveform starts assuming they are in the first 6us of the waveform
	  Peak[0] = Find_Waveform_Start(3000,Channels[PMTMap[0]]);//ChannelSize);
	  Peak[1] = Find_Waveform_Start(3000,Channels[PMTMap[1]]);//ChannelSize);
	  if(Chan3)
	    {
	      Peak[2] = Find_Waveform_Start(3000,Channels[PMTMap[2]]);
	    }
	  pevent->Delay(0,Peak[0]);
	  pevent->Delay(1,Peak[1]);
	  
	  Float_t MaxHeights[2];

	  //Find the maximum wave form heights in assuming it happens in the first 6us.
	  MaxHeights[0] = Channels[PMTMap[0]][Find_Waveform_Maximum(3000,Channels[PMTMap[0]])];
	  MaxHeights[1] = Channels[PMTMap[1]][Find_Waveform_Maximum(3000,Channels[PMTMap[1]])];
	  pevent->MaxHeight(0,MaxHeights[0]);
	  pevent->MaxHeight(1,MaxHeights[1]);
	  if(Chan3)
	    {
	      pevent->MaxHeight(2,Channels[PMTMap[2]][Find_Waveform_Maximum(ChannelSizes[PMTMap[2]],Channels[PMTMap[2]])]);
	      pevent->Delay(2,Peak[2]);
	    }
	  
	  //Lets see if there is a second event in the waveform.
	  if((Channels[PMTMap[0]][Find_Waveform_Maximum(ChannelSizes[PMTMap[0]]-3000,Channels[PMTMap[0]]+3000)+3000] > 0.4*MaxHeights[0]) &&
	     (Channels[PMTMap[1]][Find_Waveform_Maximum(ChannelSizes[PMTMap[1]]-3000,Channels[PMTMap[1]]+3000)+3000] > 0.4*MaxHeights[1]))
	    {
	      if (fabs((Find_Waveform_Start(ChannelSizes[PMTMap[0]]-3000,Channels[PMTMap[0]]+3000)+3000) - (Find_Waveform_Start(ChannelSizes[PMTMap[1]]-3000,Channels[PMTMap[1]]+3000)+3000)) < 10)
		{
		  SubEvent = true;
		  SubPeak[0] = (Find_Waveform_Start(ChannelSizes[PMTMap[0]]-3000,Channels[PMTMap[0]]+3000)+3000);
		  SubPeak[1] = (Find_Waveform_Start(ChannelSizes[PMTMap[1]]-3000,Channels[PMTMap[1]]+3000)+3000);
		  pevent->SubDelay(0,SubPeak[0]);
		  pevent->SubDelay(1,SubPeak[1]);
		}
	    }


	  //                 Integration windows
	  //                 -20-76ns  76-90  90-104 104-118 118-132 132-5us 5us-15us
	  Float_t PMT0[7] = {0        ,0     ,0     ,0      ,0      ,0       ,0}; 
	  Float_t PMT1[7] = {0,0,0,0,0,0,0};
	  
	  Float_t FastPMT2 = 0;
	  Float_t SlowPMT2 = 0;
	  
	  //	  if((Peak[0] + 7500 > ChannelSizes[PMTMap[0]]) || (Peak[1] + 7500 > ChannelSizes[PMTMap[1]]) || 
	  if((Peak[0] + WindowEnds[7] > ChannelSizes[PMTMap[0]]) || (Peak[1] + WindowEnds[7] > ChannelSizes[PMTMap[1]]) || 
	     (Peak[0] + WindowEnds[0] < 0 ) || (Peak[1] + WindowEnds[0] < 0 ))
	    {
	      ValidEvent = false;
	    }
	  else if(!SubEvent)
	    {
	      //If there is no Sub event integrate as normal and use the last 10 us for SPE study
	      pevent->SubEvent(false);
	      pevent->SubType(0);  /// No Sub Event Type;

	      //	      cout << "Event #" << TotalEventNumber -1 << " " ;
	      PMT0[0] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[0]   ,Peak[0] +WindowEnds[1]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[1] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[1]   ,Peak[0] +WindowEnds[2]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[2] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[2]   ,Peak[0] +WindowEnds[3]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[3] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[3]   ,Peak[0] +WindowEnds[4]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[4] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[4]   ,Peak[0] +WindowEnds[5]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[5] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[5]   ,Peak[0] +WindowEnds[6]    ,0,PMTVoltageTriggers[0],25,5);
	      if(MaxHeights[0] > 300)
		PMT0[6] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[6]   ,Peak[0] +WindowEnds[7]    ,0,PMTVoltageTriggers[0],25,5);
	      else 
		{
		  PMT0[6] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[6]   ,Peak[0] +WindowEnds[7]    ,PMT0SPE,PMTVoltageTriggers[0],25,5);
		}
	      //	      cout << endl;
	      PMT1[0] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[0]   ,Peak[1] +WindowEnds[1]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[1] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[1]   ,Peak[1] +WindowEnds[2]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[2] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[2]   ,Peak[1] +WindowEnds[3]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[3] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[3]   ,Peak[1] +WindowEnds[4]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[4] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[4]   ,Peak[1] +WindowEnds[5]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[5] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[5]   ,Peak[1] +WindowEnds[6]    ,0,PMTVoltageTriggers[1],25,5);
	      if(MaxHeights[1] > 300)
		PMT1[6] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[6]   ,Peak[1] +WindowEnds[7]    ,0,PMTVoltageTriggers[1],25,5);
	      else 
		{
		  PMT1[6] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[6]   ,Peak[1] +WindowEnds[7]    ,PMT1SPE,PMTVoltageTriggers[1],25,5);
		}

	      Float_t IntegralTemp0 = 0;
	      Float_t IntegralTemp1 = 0;
	      for(int i = 0; i < 5;i++)
		{
		  IntegralTemp0 = 0;
		  IntegralTemp1 = 0;
		  for(int j = 0;j<=i;j++)
		    {
		      IntegralTemp0 += PMT0[j];
		      IntegralTemp1 += PMT1[j];
		    }
		  pevent->Fast(0,i,IntegralTemp0);
		  pevent->Fast(1,i,IntegralTemp1);
		  
		  IntegralTemp0 = 0;
		  IntegralTemp1 = 0;
		  for(int j = i + 1;j<6;j++)
		    {
		      IntegralTemp0 += PMT0[j];
		      IntegralTemp1 += PMT1[j];
		    }
		  pevent->Slow(0,i,IntegralTemp0);
		  pevent->Slow(1,i,IntegralTemp1);
		}
	      
	    }
	  else if(((SubPeak[0] + SubPeak[1] - Peak[0] - Peak[1]) >> 1) > 2510 )
	    // if sub event is 5us after the first save it as normal, but also
	    // save the sub event.   Don't add to SPE.
	    // if you can contain both events save both in full.
	    // if you can't then save the first in full and then everything from the
	    // second in the first fprompt window. 
	    // these two states are set by the SubType option.
	    {
	      pevent->SubEvent(true);

	      PMT0[0] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[0]   ,Peak[0] +WindowEnds[1]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[1] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[1]   ,Peak[0] +WindowEnds[2]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[2] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[2]   ,Peak[0] +WindowEnds[3]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[3] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[3]   ,Peak[0] +WindowEnds[4]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[4] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[4]   ,Peak[0] +WindowEnds[5]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[5] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[5]   ,Peak[0] +WindowEnds[6]    ,0,PMTVoltageTriggers[0],25,5);
	      
	      PMT1[0] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[0]   ,Peak[1] +WindowEnds[1]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[1] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[1]   ,Peak[1] +WindowEnds[2]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[2] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[2]   ,Peak[1] +WindowEnds[3]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[3] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[3]   ,Peak[1] +WindowEnds[4]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[4] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[4]   ,Peak[1] +WindowEnds[5]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[5] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[5]   ,Peak[1] +WindowEnds[6]    ,0,PMTVoltageTriggers[1],25,5);

	      Float_t IntegralTemp0 = 0;
	      Float_t IntegralTemp1 = 0;
	      for(int i = 0; i < 5;i++)
		{
		  IntegralTemp0 = 0;
		  IntegralTemp1 = 0;
		  for(int j = 0;j<=i;j++)
		    {
		      IntegralTemp0 += PMT0[j];
		      IntegralTemp1 += PMT1[j];
		    }
		  pevent->Fast(0,i,IntegralTemp0);
		  pevent->Fast(1,i,IntegralTemp1);

		  IntegralTemp0 = 0;
		  IntegralTemp1 = 0;
		  for(int j = i + 1;j<6;j++)
		    {
		      IntegralTemp0 += PMT0[j];
		      IntegralTemp1 += PMT1[j];
		    }
		  pevent->Slow(0,i,IntegralTemp0);
		  pevent->Slow(1,i,IntegralTemp1);
		}
	      
	      if(SubPeak[0] + WindowEnds[6] < ChannelSizes[PMTMap[0]] && SubPeak[1] + WindowEnds[6] < ChannelSizes[PMTMap[1]])
		//If the full 5 us of sub event can be captured do it. 
		{
		  pevent->SubType(1);  // 1 for fulling contained two events
		  PMT0[0] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[0]   ,SubPeak[0] +WindowEnds[1]    ,0,PMTVoltageTriggers[0],25,5);
		  PMT0[1] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[1]   ,SubPeak[0] +WindowEnds[2]    ,0,PMTVoltageTriggers[0],25,5);
		  PMT0[2] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[2]   ,SubPeak[0] +WindowEnds[3]    ,0,PMTVoltageTriggers[0],25,5);
		  PMT0[3] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[3]   ,SubPeak[0] +WindowEnds[4]    ,0,PMTVoltageTriggers[0],25,5);
		  PMT0[4] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[4]   ,SubPeak[0] +WindowEnds[5]    ,0,PMTVoltageTriggers[0],25,5);
		  PMT0[5] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[5]   ,SubPeak[0] +WindowEnds[6]    ,0,PMTVoltageTriggers[0],25,5);
		  
		  PMT1[0] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[0]   ,SubPeak[1] +WindowEnds[1]    ,0,PMTVoltageTriggers[1],25,5);
		  PMT1[1] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[1]   ,SubPeak[1] +WindowEnds[2]    ,0,PMTVoltageTriggers[1],25,5);
		  PMT1[2] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[2]   ,SubPeak[1] +WindowEnds[3]    ,0,PMTVoltageTriggers[1],25,5);
		  PMT1[3] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[3]   ,SubPeak[1] +WindowEnds[4]    ,0,PMTVoltageTriggers[1],25,5);
		  PMT1[4] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[4]   ,SubPeak[1] +WindowEnds[5]    ,0,PMTVoltageTriggers[1],25,5);
		  PMT1[5] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[5]   ,SubPeak[1] +WindowEnds[6]    ,0,PMTVoltageTriggers[1],25,5);
		  
		  for(int i = 0; i < 5;i++)
		    {
		      IntegralTemp0 = 0;
		      IntegralTemp1 = 0;
		      for(int j = 0;j<=i;j++)
			{
			  IntegralTemp0 += PMT0[j];
			  IntegralTemp1 += PMT1[j];
			}
		      pevent->SubFast(0,i,IntegralTemp0);
		      pevent->SubFast(1,i,IntegralTemp1);
		      
		      IntegralTemp0 = 0;
		      IntegralTemp1 = 0;
		      for(int j = i + 1;j<6;j++)
			{
			  IntegralTemp0 += PMT0[j];
			  IntegralTemp1 += PMT1[j];
			}
		      pevent->SubSlow(0,i,IntegralTemp0);
		      pevent->SubSlow(1,i,IntegralTemp1);
		    }
		  
		}
	      else
		//Grab what you can and save it in the zero fprompt bin.
		{
		  pevent->SubType(2);  //2 for partially contained two events
		  PMT0[0] = Integrate(Channels[PMTMap[0]],SubPeak[0] +WindowEnds[0]   ,ChannelSizes[PMTMap[0]],0,PMTVoltageTriggers[0],25,5);
		  PMT0[1] = 0;
		  PMT0[2] = 0;
		  PMT0[3] = 0;
		  PMT0[4] = 0;
		  PMT0[5] = 0;
		  
		  PMT1[0] = Integrate(Channels[PMTMap[1]],SubPeak[1] +WindowEnds[0]   ,ChannelSizes[PMTMap[1]],0,PMTVoltageTriggers[1],25,5);
		  PMT1[1] = 0;
		  PMT1[2] = 0;
		  PMT1[3] = 0;
		  PMT1[4] = 0;
		  PMT1[5] = 0;
		  
		  for(int i = 0; i < 5;i++)
		    {
		      IntegralTemp0 = 0;
		      IntegralTemp1 = 0;
		      for(int j = 0;j<=i;j++)
			{
			  IntegralTemp0 += PMT0[j];
			  IntegralTemp1 += PMT1[j];
			}
		      pevent->SubFast(0,i,IntegralTemp0);
		      pevent->SubFast(1,i,IntegralTemp1);
		      
		      IntegralTemp0 = 0;
		      IntegralTemp1 = 0;
		      for(int j = i + 1;j<6;j++)
			{
			  IntegralTemp0 += PMT0[j];
			  IntegralTemp1 += PMT1[j];
			}
		      pevent->SubSlow(0,i,IntegralTemp0);
		      pevent->SubSlow(1,i,IntegralTemp1);
		    }
		}	      
	    }
	  else
	    {
	      pevent->SubEvent(true);
	      pevent->SubType(3);  // 3 for overlapping events.


	      PMT0[0] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[0]   ,Peak[0] +WindowEnds[1]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[1] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[1]   ,Peak[0] +WindowEnds[2]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[2] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[2]   ,Peak[0] +WindowEnds[3]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[3] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[3]   ,Peak[0] +WindowEnds[4]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[4] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[4]   ,Peak[0] +WindowEnds[5]    ,0,PMTVoltageTriggers[0],25,5);
	      PMT0[5] = Integrate(Channels[PMTMap[0]],Peak[0] +WindowEnds[5]   ,Peak[0] +WindowEnds[6]    ,0,PMTVoltageTriggers[0],25,5);
	      
	      PMT1[0] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[0]   ,Peak[1] +WindowEnds[1]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[1] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[1]   ,Peak[1] +WindowEnds[2]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[2] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[2]   ,Peak[1] +WindowEnds[3]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[3] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[3]   ,Peak[1] +WindowEnds[4]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[4] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[4]   ,Peak[1] +WindowEnds[5]    ,0,PMTVoltageTriggers[1],25,5);
	      PMT1[5] = Integrate(Channels[PMTMap[1]],Peak[1] +WindowEnds[5]   ,Peak[1] +WindowEnds[6]    ,0,PMTVoltageTriggers[1],25,5);

	      Float_t IntegralTemp0 = 0;
	      Float_t IntegralTemp1 = 0;
	      for(int i = 0; i < 5;i++)
		{
		  IntegralTemp0 = 0;
		  IntegralTemp1 = 0;
		  for(int j = 0;j<=i;j++)
		    {
		      IntegralTemp0 += PMT0[j];
		      IntegralTemp1 += PMT1[j];
		    }
		  pevent->Fast(0,i,IntegralTemp0);
		  pevent->Fast(1,i,IntegralTemp1);
		  

		  IntegralTemp0 = 0;
		  IntegralTemp1 = 0;
		  for(int j = i + 1;j<6;j++)
		    {
		      IntegralTemp0 += PMT0[j];
		      IntegralTemp1 += PMT1[j];
		    }
		  pevent->Slow(0,i,IntegralTemp0);
		  pevent->Slow(1,i,IntegralTemp1);
		}
	      
	    }

	  //	  cout << "Event #" << pevent->EventNumber() << " SubType = " << pevent->SubType() << endl;
	  
	  FastPMT2 = 0;
	  SlowPMT2 = 0;
	  
	  if(Chan3)
	    {
	      if(Peak[2] + WindowEnds[7] > ChannelSizes[PMTMap[2]] || (Peak[2] - 10 < 0))
		ValidEvent = false;
	      else
		{
		  FastPMT2 = Integrate(Channels[PMTMap[2]],Peak[2] - 10,Peak[2] + 50,0,2,25,5);
		  SlowPMT2 = Integrate(Channels[PMTMap[2]],Peak[2] + 50,ChannelSizes[PMTMap[2]], 0,2,25,5); 
		}
	    }
	  
	  pevent->AboveTHRS(false);
	  
	  if(Chan3)
	    {
	      pevent->Fast(2,0,FastPMT2);
	      pevent->Slow(2,0,SlowPMT2);
	    }
	  

	  if(ValidEvent&&Run1)
	    {
	      PMT0Charge->Fill(pevent->Fast(0,0) + pevent->Slow(0,0));
	      PMT1Charge->Fill(pevent->Fast(1,0) + pevent->Slow(1,0));
	      TotalCharge->Fill((pevent->Fast(1,0) + pevent->Slow(1,0)) + (pevent->Fast(1,0) + pevent->Slow(1,0)) );
	      PMTasym->Fill( ((pevent->Fast(0,0) + pevent->Slow(0,0)) - (pevent->Fast(1,0) + pevent->Slow(1,0)))/
			     ((pevent->Fast(0,0) + pevent->Slow(0,0)) + (pevent->Fast(1,0) + pevent->Slow(1,0))) );
	    }
	  
	  //	  pevent->TOFAve = 5;
	  if(ValidEvent&&!Run1)
	    {
	      ptree->Fill();
	      TotalEvents++;
	    }
	  else
	    Skipped++;

	  pevent->Clear("C");
	}

      delete [] ChannelMap;   
      delete [] PMTMap;       
      delete [] ChannelIDs;   
      delete [] ChannelSizes; 
      delete [] Offsets;      
      delete [] OffsetSigmas; 
      delete [] FixGain;      
      for(int Channel = 0; Channel < NumberOfChannels;Channel++)
	{
	  delete [] Channels[Channel];
	}
      delete [] Channels;
      
      gettimeofday(&EndTime,&TimeZone);
      printf("File Processed in %ld seconds.\n",EndTime.tv_sec - StartTime.tv_sec);

      cout << "Closing file : " << argv[filenumber] << endl;

      InFile->Close("R");
      delete InFile;
    }

  if(Run1)
    {
      OutFile->cd();
      PMT0Charge->Write(); 
      PMT1Charge->Write(); 
      TotalCharge->Write();
      PMTasym->Write();    
      PMT0SPE->Write();    
      PMT1SPE->Write();    
      TextTree->Write();
      //      OutFile->Write("TextTree");
    }
  else
    {
      ptree->Write();
    }


  OutFile->Close("R");
  printf("Skipped %d of %d events.\n",Skipped,TotalEvents+Skipped);
  //  cout << "Skipped events = " << Skipped << endl;
  return(0);
}
