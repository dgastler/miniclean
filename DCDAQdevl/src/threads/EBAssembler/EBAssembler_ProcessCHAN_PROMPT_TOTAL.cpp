 #include <EBAssembler.h> 
int EBAssembler::ProcessCHAN_PROMPT_TOTAL(uint8_t *data, uint16_t size,EBPCBlock * event)
{
  //parsed channels (-1 for an error)
  int ret = 0; 

  //Parsing pointer and size left.
  uint8_t * dataPtr = data;
  uint16_t  dataSizeLeft = size;

  //Check to see if we have enough space in the data for a EBPacket::Event
  if(sizeof(EBPacket::Event) > dataSizeLeft)
    {
      PrintError("Packet too small for EBPacket::Event (CHAN_PROMPT_TOTAL packet).\n");
      ret = -1;
    }
  else
    //We have enough data to have a Event header packet.
    {   
      
      EBPacket::Event * eventHeader = (EBPacket::Event * ) dataPtr;
      dataPtr += sizeof(EBPacket::Event);        //move forward in dataPtr
      dataSizeLeft -= sizeof(EBPacket::Event);   //decrease remaining size
      uint16_t channelCount = eventHeader->channelCount;
      uint16_t channelsParsed = 0;

      //RAT::DS::EV * ev = event->ds->AddNewEV();

      for(uint16_t iChannel = 0; iChannel < channelCount;iChannel++)
	//Parse each channel
	{

	  //check that we have enough space left for a EBPacket::ChannelData block
	  if(sizeof(EBPacket::ChannelData) > dataSizeLeft)
	    {
	      PrintError("Packet too small for EBPacket::ChannelData (CHAN_PROMPT_TOTAL) packet.\n");
	      break;
	    }
	  else
	    //We have enough data to have a channel data header packet;
	    {
	      EBPacket::ChannelData * channelHeader = (EBPacket::ChannelData *) dataPtr;
	      dataPtr += sizeof(EBPacket::ChannelData);      //move forward in dataPtr
	      dataSizeLeft -= sizeof(EBPacket::ChannelData); //decrease remaining size
	      
	      //Add a new PMT to this EBBlock's EV data structure
	      for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
		{pmtCopies[iCopies] = ev->AddNewPMT();}

	      if(pmtCopies[0] == NULL) {PrintError("Failed to get a new PMT from EV\n");SendStop();break;}
	      else
		{
		  //Set the PMT ID
		  for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
		    {
		      pmtCopies[iCopies]->SetID(4*channelHeader->ID+iCopies);
		      rawCopies[iCopies] = pmtCopies[iCopies]->GetRaw();
		      if(rawCopies[iCopies] == NULL)
			{PrintError("Failed to get a new RAW from PMT\n");SendStop();break;}
		    }
		  if(channelHeader->dataBlockCount == 1)
		    {
		      if(sizeof(EBPacket::PromptTotalBlock) > dataSizeLeft)
			{
			  PrintError("Packet too small for EBPacket::IntegralBlock (CHAN_ZLE_INTEGRAL packet).\n");
			  iChannel = channelCount; //Stop outer for loop
			  break;
			}
		      else
			{
			  EBPacket::PromptTotalBlock * promptTotalBlock = (EBPacket::PromptTotalBlock * ) dataPtr;
			  dataPtr += sizeof(EBPacket::PromptTotalBlock);      //move forward in dataPtr
			  dataSizeLeft -= sizeof(EBPacket::PromptTotalBlock); //decrease remaining size

			  //Prompt light
			  for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
			    {rawIntegralCopies[iCopies] = rawCopies[iCopies]->AddNewRawIntegral();}
			  if(rawIntegralCopies[0] == NULL){PrintError("Failed to get a new RawIntegral from RAW\n");SendStop();break;}
			  for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
			    {
			      rawIntegralCopies[iCopies]->SetIntegral(promptTotalBlock->prompt);
			      rawIntegralCopies[iCopies]->SetStartTime(promptStartTime);
			    }
			  
			  //Late light
			  for(uint8_t iCopies=0; iCopies < nCopies; iCopies++)
			    {
			      rawIntegralCopies[iCopies] = rawCopies[0]->AddNewRawIntegral();
			      if(rawIntegralCopies[iCopies] == NULL){PrintError("Failed to get a new RawIntegral from RAW\n");SendStop();break;}
			      rawIntegralCopies[iCopies]->SetIntegral(promptTotalBlock->total - promptTotalBlock->prompt);
			      rawIntegralCopies[iCopies]->SetStartTime(lateStartTime);
			    }
			}
		    }
		  //Now that we have successfully parsed this channel we need to note that in two places
		  channelsParsed++;
		  event->PMT.push_back(channelHeader->ID);
		}
	    }
	}
      ret = channelsParsed;


      //Deal with the WFD headers
      uint16_t wfdHeaderCount = eventHeader->WFDHeaderCount;
      EBPacket::WFDHeader tempHeader;
      for(uint16_t i = 0; i < wfdHeaderCount;i++)
	{
	  memcpy(&tempHeader,dataPtr,sizeof(EBPacket::WFDHeader));	  
//	  printf("%08X\n%08X\n%08X\n%08X\n\n",
//		 tempHeader.word[0],
//		 tempHeader.word[1],
//		 tempHeader.word[2],
//		 tempHeader.word[3]);
	  dataPtr+=sizeof(EBPacket::WFDHeader);
	  dataSizeLeft -=sizeof(EBPacket::WFDHeader);
	  //fill the first PMT with valid header data
	  //Skipping the others to make sure people notice this code
	  //hasn't been updated to RAT-newdaq yet. 
	  if(i == 0)
	    {
	      if(ev->GetPMTCount() > 0)
		{
		  ev->GetPMT(0)->GetRaw()->SetHeader(tempHeader.word,4*sizeof(uint32_t));
		}
	    }
	}
    }
  return(ret);
}
