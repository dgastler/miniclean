#ifndef __SERVERCONNECTION__
#define __SERVERCONNECTION__

#include <vector>
#include <string>
#include <DCNetThread.h>
#include <Connection.h>

class ServerConnection
{
 public:
  ServerConnection(){serverSocketFD=badFD;Clear();}
  ~ServerConnection();
  void Clear();

  bool Setup(xmlNode * SetupNode);
  bool ProcessConnection(xmlNode * managerNode);
  void DeleteConnection(size_t i);

  int GetSocketFD(){return serverSocketFD;}
  size_t Size(){return(client.size());};
  bool RouteToLocal(DCMessage::DCAddress addr);

  Connection & operator[](size_t i){return(*(client[i]));};
  Connection & Back(){return(*(client.back()));};
  DCMessage::DCAddress GetListenAddr(){return listenAddr;};

 private:
  //Variables for the server
  uint16_t             serverPort;
  DCMessage::DCAddress listenAddr;
  int                  serverSocketFD;

  //List of cleared remote addresses
  std::vector<DCMessage::DCAddress>  acceptList;
  //List of connected objects
  std::vector<Connection*>      client;  
  //Bad file descriptor value
  static const int badFD;

  void PrintError(const char * str){fprintf(stderr,"Server Connection error: %s\n",str);};
  void PrintWarning(const char * str){printf("Server Connection warning: %s\n",str);};

};

#endif
