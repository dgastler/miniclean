#include <RATParse.h>

void RATParse::MainLoop()
{
  //Check for a new memory block event
  //Don't do anything if we already have a blockIndex
  if(IsReadReady(MemoryBlockManagerFullFDs[0]) &&
     (memBlockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Block_mem->GetFull(memBlockIndex);
      if(memBlockIndex != FreeFullQueue::BADVALUE)       //We have a valid MemBlock
	{
	  //Get the memory bloc for the blockIndex
	  memBlock = &Block_mem->Memory[memBlockIndex];
	  //Zero our current point in the memory block.
	  currentDataIndex = 0;
	  
	  //We no longer need a memblock.
	  //Remove memory block manager from the select set
	  RemoveReadFD(MemoryBlockManagerFullFDs[0]);
	  
	  if(dsIndex ==  FreeFullQueue::BADVALUE) 
	    {
	      //We need a DS block so add the RAT manager's FD to select 
	      AddReadFD(BasicRATDSManagerFreeFDs[0]);	  
	    }
	  
	  ///Apply select changes
	  SetupSelect();
	}
      else 
	{		  
	  //This means that the memory block manager
	  //has been set to shutdown.
	  //We probably have a message from DCDAQ waiting
	  //for us
	}      
    }
  
  
  
  
  //check for a new basic ds block event
  if(IsReadReady(BasicRATDSManagerFreeFDs[0]) &&
     (dsIndex == FreeFullQueue::BADVALUE))
    {
      //Get new ds block
      DS_mem->GetFree(dsIndex);
      if(dsIndex != FreeFullQueue::BADVALUE)
	{
	  //Get the DSBlock for dsIndex
	  dsBlock = DS_mem->GetDSBlock(dsIndex);
	  //Zero it's use
	  dsBlock->usedSize = 0;
	  
	  //Now that we have a ds block we don't need to listen to it's queue
	  RemoveReadFD(BasicRATDSManagerFreeFDs[0]);	     
	  
	  if(memBlockIndex == FreeFullQueue::BADVALUE)
	    {	      
	      //We need a mem block so add the mem block manager's FD to select  
	      AddReadFD(MemoryBlockManagerFullFDs[0]);
	    }
	  
	  //Apply select changes
	  SetupSelect();
	}
      else
	{
	  //This means that the ds manager has been shut down
	  //We probably have a message waiting for us from DCDAQ
	}
    }
  
  //If we are in MainLoop, then something has changed with the state of 
  //either the memblock or dsblock.   
  //Check that we have a valid memblock and a valid dsBlock
  if((memBlockIndex != FreeFullQueue::BADVALUE)&&
     (dsIndex != FreeFullQueue::BADVALUE))
    {
      //Loop over the current memblock untill it is emtpy
      while(currentDataIndex < memBlock->dataSize)
	{
	  //Parse the current CAEN V1720 style event.  
	  //It is located at memBlock->buffer + CurrentDataIndex
	  int32_t shift =  CurrentEvent.ProcessEvent( (char*) (memBlock->buffer + currentDataIndex),
						      memBlock->dataSize - currentDataIndex);
	  //If shift is positive, then we read out a valid event
	  if(shift > 0)
	    {			
	      //Take the parsed CAEN event an put it in a RAT ds
	      ProcessOneEvent(CurrentEvent,dsBlock->array + dsBlock->usedSize);	     
	      //We processed another event.  Yay!
	      TimeStep_Events++;
	      //Move CurrentDataIndex to the start of the next event. 
	      currentDataIndex += shift;  
	      //Move us forward in the dsBlock
	      dsBlock->usedSize++;
	      //Check if we should pass off this block. 
	      if(dsBlock->usedSize >= dsBlock->allocatedSize)
		{
		  //Break out of the while loop and get a new dsBlock
		  break;
		}
	    }
	  else if(shift == V1720EVENT_SMALL_EVENT)
	    {
	      //No more events to parse, but there is still some data.
	      //This is strange and worthy of reporting
	      char * buffer = new char[100];
	      sprintf(buffer,"Found small event of size %d",memBlock->dataSize - currentDataIndex);
	      PrintWarning(buffer);
	      delete [] buffer;

	      currentDataIndex = memBlock->dataSize;
	      break;
	    }
	  else if(shift == V1720EVENT_CORRUPT_EVENT)
	    {
	      //we encountered a bad event
	      TimeStep_BadEvents++;//Bad data. 		 

	      char * buffer = new char[100];
	      sprintf(buffer,"Corrupt event. Lost %d bytes of data.",memBlock->dataSize - currentDataIndex);
	      PrintWarning(buffer);
	      delete [] buffer;	      

	      DCMessage::Message message;      
	      //Send a message back to the master and stop the inner run loop
	      message.SetType(DCMessage::STOP);
	      SendMessageOut(message,true);

	      currentDataIndex = memBlock->dataSize;
	      break;
	    }
	  else
	    {
	      //shouldn't happen.
	      PrintError("Unknown error (you win!)");
	      currentDataIndex = memBlock->dataSize;
	      break;
	    }	 	  
	}
      
      //Check if we should return the current memBlock
      if(currentDataIndex >= memBlock->dataSize)
	{
	  //Zero memBlock's data size
	  memBlock->dataSize = 0;
	  //Forget the address of the memBlock (that way we don't mess with it)
	  memBlock = NULL; 
	  //Recycle the memblock by passing off it's index
	  Block_mem->AddFree(memBlockIndex);
	  //We need to listen for a new full memBlock
	  AddReadFD(MemoryBlockManagerFullFDs[0]);
	  SetupSelect();
	}
      
      //Check if we should pass off the current dsBlock
      if(dsBlock->usedSize >= dsBlock->allocatedSize)
	{	  
	  //Forget the dsBlock
	  dsBlock = NULL;
	  //Give back the dsBlock for writing.
	  DS_mem->AddFull(dsIndex);
	  //Now we need to listen for a new free DSBlock
	  AddReadFD(BasicRATDSManagerFreeFDs[0]);
	  SetupSelect();
	}     	
    }
}



void  RATParse::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      float TimeStep = difftime(currentTime,lastTime);
      DCMessage::TimedCount Count;
      
      BlocksRead += TimeStep_BlocksRead;
      Events+=TimeStep_Events;
      BadEvents+=TimeStep_BadEvents;

      //Data message
      Count.count = TimeStep_BlocksRead;
      Count.dt = TimeStep;
      message.SetData(&Count,sizeof(Count));
      message.SetType(DCMessage::BLOCKSREAD);
      SendMessageOut(message,false);

      

      //Data message
      Count.count = TimeStep_Events;
      Count.dt = TimeStep;
      message.SetData(&Count,sizeof(Count));
      message.SetType(DCMessage::EVENTSREAD);
      SendMessageOut(message,false);
      

      //Data message
      Count.count = TimeStep_BadEvents;
      Count.dt = TimeStep;
      message.SetData(&Count,sizeof(Count));
      message.SetType(DCMessage::BADEVENTSREAD);
      SendMessageOut(message,false);



      TimeStep_BlocksRead = 0;
      TimeStep_Events = 0;
      TimeStep_BadEvents = 0;
      //Setup next time
      lastTime = currentTime;
    }
}

