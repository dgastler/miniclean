#include "v1720Event.h"

v1720EventChannel::v1720EventChannel()
{  
  Clear();
}

v1720EventChannel::~v1720EventChannel()
{
  Clear();
}

void v1720EventChannel::Clear()
{
  window.clear();
  rawData = 0;
  size = 0;
  ZLE = false;
  twoPack = false;
  channelID = 0xFF;  
}

int v1720EventChannel::ProcessChannel(uint32_t * _rawData,uint32_t _size,bool _ZLE,bool _twoPack,uint8_t _channelID)
{
  int ret = 0;
  rawData = _rawData;
  size = _size;
  ZLE = _ZLE;
  twoPack = _twoPack;
  window.clear();
  channelID = _channelID;
  if(ZLE)
    {
      ret += ProcessZLE();
    }
  else
    {
      ret += ProcessFULL();
    }
  return(ret);
}

int v1720EventChannel::ProcessZLE()
{
  int ret = 0;
  //Current position in the 32bit array.
  uint32_t position = 0;
  //This is the begining time of the current window in terms of samples
  //after the event trigger time.
  uint32_t timeOffset = 0;
  //This holds two possible datas
  //  1) if the control word is good then it's the size in 32bit words of
  //  that window's data
  //  2) if the control word is skip then it's the size in 32bit words of
  //  skipped data.
  uint32_t windowDataSize;

  //Main loop for finding ZLE windows and parsing them.
  while(position < size)
    {
      //Check if this is a valid control word
      //      if(rawData[position]&0x80000000 == 0x80000000)
      if(0x1&(rawData[position] >> 31))
	{
	  //Good control word
	  v1720EventWindow newWindow;
	  
	  //determin how many 32bit words are in this window.
	  windowDataSize= rawData[position]&0x1FFFFF;	
	  if((position+windowDataSize) > size)
	    {
	      return -1;
	    }
	  newWindow.Setup(//take the rawData pointer to the control 
			  //word and incriment it by 1.
			  rawData + position + 1, 
			  //This is the number of 32bit words of data.
			  windowDataSize,
			  //Bool for if this is two packed or not.
			  twoPack,
			  //Set this windows timing offset
			  timeOffset
			  );
	  //Add a new window for this channel
	  window.push_back(newWindow);
	  
	  //Advance the timeOffset by the number of samples.
	  if(twoPack)
	    {
	      timeOffset += (windowDataSize << 1);
	    }
	  else
	    {
	      timeOffset += (windowDataSize << 1) + (windowDataSize >> 1);
	    }
	  
	  //advance the position by the number of samples plus one for the control word.	      
	  position += windowDataSize + 1;
	}
      else
	{
	  //Get the number of 32 bit words skipped.
	  windowDataSize = rawData[position]&0x1FFFFF;
	  //Advance the timeOffset by the number of samples.
	  if(twoPack)
	    {
	      timeOffset += (windowDataSize << 1);
	    }
	  else
	    {
	      timeOffset += (windowDataSize << 1) + (windowDataSize >> 1);
	    }
	  //Move to the next control word
	  position++;
	}
    }
  return(ret);  
}
int v1720EventChannel::ProcessFULL()
{
  int ret = 0;
  v1720EventWindow newWindow;
  newWindow.Setup(rawData,size,twoPack,
	       //Zero time offset
	       0);
  window.push_back(newWindow);
  return(ret);
}

const v1720EventWindow & v1720EventChannel::GetWindow(size_t i)
{
  //No check done on bounds for i
  return(window[i]);
}

