#include <inttypes.h>
#include <RAT/DSReader.hh>
#include <TFile.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <tclap/CmdLine.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TF1.h>
#include <mysql/mysql.h>
#include <mysql/errmsg.h>
#include <boost/algorithm/string.hpp>

#include "RAT/DS/DCDAQ_DQMStructs.hh"
#include "RAT/DS/DCDAQ_MemBlocks.hh"
#include "RAT/DS/DCDAQ_EventStreamHelper.hh"
#include "RAT/DS/DCDAQ_DCDAQEvent.hh"
#include "RAT/DS/DCDAQ_RAWDataStructs.hh"

#include "RAT/DS/Root.hh"
#include "RAT/DSReader.hh"
#include "RAT/DS/EV.hh"
#include "RAT/DS/Board.hh"
#include "RAT/DS/Channel.hh"
#include "RAT/DS/RawBlock.hh"
#include "RAT/DS/ReductionLevel.hh"
#include "RAT/DB.hh"

#include <zlib.h>
using std::cout;
using std::endl;
using std::setw;


FILE * InFile;
		      

int32_t ReadMemBlock(MemoryBlock * mBlock)
{
  MemBlockMessage::header blockHeader;
 
  int freadret = fread(&blockHeader,sizeof(blockHeader),1,InFile);
  if(freadret==1)
    {
     
      if(!feof(InFile)) //check that not at end of file
	{
	  uint32_t blockSize;
	  //Make sure header word is valid
	  if(blockHeader.sizeMagic!= 0xAA)
	    {
	      fprintf(stderr,"Bad Header word to start DCDAQ .dat file %x \n",blockHeader.sizeMagic);
	      return -1;
	    }
	  blockSize=blockHeader.size;
	  
	  //Find if memBlock compressed;
	  

	  if(blockHeader.compressionMagic == 0xBB)
	    {
	      
	      mBlock->uncompressedSize=blockHeader.compressionSize;     
	    }
	  else
	    {
	      
	      if(blockHeader.compressionMagic != 0xCC)
		{
		  fprintf(stderr,"Bad uc Header word %x \n",blockHeader.compressionMagic);
		  return -1;
		}

	    }
	  //read blockSize words, this will give us first memblock.  note we only read one memblock at a time
	  //blockSize=int(ceil(blockSize/sizeof(uint32_t)));
	  if(blockSize>mBlock->allocatedSize)
	    {
	      delete (mBlock->buffer);
	      mBlock->buffer=new uint32_t[blockSize];
	      mBlock->allocatedSize=blockSize;
	    }
	  mBlock->dataSize=0;
	  uint32_t * bufferPointer= (uint32_t *)(mBlock->buffer);
	  freadret=fread(bufferPointer,sizeof(uint32_t),blockSize,InFile);
	  if(uint32_t(freadret)== blockSize)
	    {
	      mBlock->dataSize=blockSize;
	     
	    }
	  else
	    {
	      fprintf(stderr,"Bad read of DCDAQ memblock");
	      return -1;
	    }
	
	}
      else
	{
	  fprintf(stderr,"Size of file Bad, ended after header");
	  return -1;
	}
    }

  else
    {
     
      if(ferror(InFile))
	{	
	  fprintf(stderr,"Problem reading header word");
	  return -1;
	}
      else
	{
	  fprintf(stderr,"End of file, no header word");
	  return 0;
	}
    }
  return 1;

}

bool DecompressMemBlock(MemoryBlock * cBlock,MemoryBlock * dBlock)
{
 
  if(dBlock->allocatedSize<(uint32_t)cBlock->uncompressedSize)
    {
      delete(dBlock->buffer);
      dBlock->buffer = new uint32_t[cBlock->uncompressedSize];
      dBlock->allocatedSize=cBlock->uncompressedSize;
    }
  z_stream stream;
  stream.zalloc = Z_NULL;
  stream.zfree = Z_NULL;
  stream.opaque = Z_NULL;
  if(Z_OK == inflateInit(&stream))
    {
      stream.next_in=(Bytef*) cBlock->buffer;
      stream.avail_in=(cBlock->dataSize)<<2;
      //stream.avail_in=(cBlock->dataSize);
      stream.next_out=(Bytef*) dBlock->buffer;
      stream.avail_out=(dBlock->allocatedSize)<<2;
      //stream.avail_out=(dBlock->allocatedSize);
      int ret = inflate(&stream,Z_FINISH);
      if(ret == Z_STREAM_END)
	{
	  inflateEnd(&stream);
	  dBlock->dataSize=dBlock->allocatedSize-(stream.avail_out>>2);
	  // dBlock->dataSize=dBlock->allocatedSize-(stream.avail_out);
	  if(dBlock->dataSize != (uint32_t)cBlock->uncompressedSize)
	    {
	      fprintf(stderr,"uc size was incorrect \n");
	      return false;
	    }
	}
      else
	{
	  fprintf(stderr,"Error with decompression \n");
	  return false;
	}
    }
  return true;
}


uint32_t* ReadPastRunHeader(uint32_t * bufferPtr)
{
  uint32_t * returnAdr=bufferPtr;
  DCDAQEvent::Run *run = (DCDAQEvent::Run *) bufferPtr;
  if(run->version!=DCDAQEventVersion)
    {
      fprintf(stderr,"Wrong DCDAQEventVersion, data is version %i, I am version %i\n",run->version,DCDAQEventVersion);
      return NULL;
    }
  if(run->magic == DCDAQEvent::RUNSTART)
    {
      DCDAQEvent::RunStart* runStart=(DCDAQEvent::RunStart*) bufferPtr;
     
      returnAdr += int(ceil(sizeof(DCDAQEvent::RunStart)/sizeof(uint32_t)));
      returnAdr += runStart->channelMapSize;

    }
  else if(run->magic ==DCDAQEvent::RUNCONTINUE)
    {

      returnAdr += int(ceil(sizeof(DCDAQEvent::RunContinue)/sizeof(uint32_t)));


    }
  else
    {
      fprintf(stderr,"Error: MemBlock did not start with Run Header \n");
      return NULL;
    }

  return returnAdr;

}




//
//Variance Function: Calculates the variance and mean of the set of samples entered.

//
//Compute Half Time Function: This computes the sample number at which the waveform reaches the halfway point between its baseline ADC and its lowest ADC. There are also several flags that will be noted throughout the function.

double ComputeHalfTime(std::vector<uint16_t> sample, int sample_parameter, double baseline ,uint32_t offset)
{

 
  uint16_t lowADC = 10000; //set this extraordinarily high so that all points will be below it
  unsigned int lowTime = 0; //sample number of the lowest point
  double halfTime = 0; //Desired time of pulse
  double halfVoltage = 0; //Voltage corresponding to desired time of pulse
  uint16_t abovehV = 0; //Voltage point above the halfVoltage (if halfVoltage is not exactly on a data point)
  uint16_t  belowhV = 0; //Voltage point below the halfVoltage (if halfVoltage is not exactly on a data point)
  unsigned int abovetime = 0;//sample number of the point immediately before the halfTime
  //  int errorcounter = 0;
  double slope = 0;
  //std::vector<uint16_t> &sample = raw->GetSamples(); //sample is a reference to a vector of unsigned integers. Why use the & symbol?
  //double baseline = 0;
  int sample_skip=sample_parameter;
  /*if(zle_mode)
    sample_skip=0;

      baseline = ComputeBaseline( raw , false , sample_skip );
  if( baseline == -1 )
    {
      return -3;
    }
  if( baseline == -5 )
    {
      return -5;
      }*/
 
 
  for( size_t i = sample_skip ; i < sample.size() ; i++ )
    {
      if( sample[i] < lowADC )
	{
	  lowADC = sample[i];
	  lowTime = i;
	}
    }
 
 
  if( baseline - lowADC >= 5 )
    {
      halfVoltage = ( ( double(lowADC) + baseline) / 2 ); //This sets the midpoint voltage
     
      for( size_t i = sample_skip; i < sample.size(); i++ )
	{
	  if( (double(sample[i]) > halfVoltage) && (double(sample[i+1]) < halfVoltage) && (i < lowTime) ) 
	    {
	      abovehV = sample[i];
	      belowhV = sample[i+1];
	      abovetime = i;
	     
	      break;
	    }
	  else if( double(sample[i]) == halfVoltage && i < lowTime )
	    {
	      return double(i+offset);
	    }
	}
      /*
      for( size_t i = sample_skip; i < sample.size() - 1 ; i++ )
	{
	  if( double(sample[i]) > halfVoltage && double(sample[i+1]) < halfVoltage)
	    {
	      errorcounter++;
	    }
	  else
	    {
	      if( double(sample[i]) == halfVoltage && double(sample[i+1]) < halfVoltage )
		{
		  errorcounter++;
		}
	    }
	    }*/
    }
  else
    {
      //No pulse in rawblock.
      return -1;
    }
  //fprintf(stderr,"belowhV %i, abovehV %i \n",belowhV,abovehV);
  slope = ( belowhV - abovehV )/1;
  halfTime = double(abovetime) + ( halfVoltage - double(abovehV) ) / slope; 
  /*if( errorcounter > 1 ) //if more than one pulse in the data...
    {
      return -2;
      }*/
  
  return halfTime + double(offset);
}

struct ChannelBaselineInfo{
  ChannelBaselineInfo(char * name)
  {
    BaseLineHist=NULL;
    TH1F * tempHist=new TH1F(name,name,8192,-0.5,4095.5);
    tempHist->SetBit(TH1F::kCanRebin);
    if(tempHist)
      BaseLineHist=tempHist;
  }

  ~ChannelBaselineInfo()
  {
    fprintf(stderr,"Destructor???????????***************");
    delete BaseLineHist;
  }

  int ChannelID;
  int Input;
  int BoardID;
  double BaseLine;
  TH1F * BaseLineHist;
   
};

struct BoardTimingInfo{

  BoardTimingInfo(char * name)
  {
    TimingHist=NULL;
    TH1F * tempHist=new TH1F(name,name,1000,-5,5);
    tempHist->SetBit(TH1F::kCanRebin);
    if(tempHist)
      TimingHist=tempHist;
  }

  ~BoardTimingInfo()
  {
   
    delete TimingHist;
  }



  int BoardID;
  double timingOffset;
  TH1F * TimingHist;
};



int main( int argc , char ** argv )
{

  

  std::vector<std::string> _Input;
  std::vector<int> _pulseInputs;
  bool _load_mode=false;
  bool _baseline_mode=false;
  int _nhitOffset = -1;
  int _zleOffset = -1;
  try{
    TCLAP::CmdLine cmd("Timing and Baseline",
		       ' ',
		       "Version 2"
		       );
    TCLAP::UnlabeledMultiArg<std::string> Var1("Files_to_be_read_for_Baseline/Timing",
					       "GIVE SHORT EXPLANATION",
					       true,
					       "Input files",
					       cmd
					       ); 

    TCLAP::SwitchArg LoadSwitch("L",
				"load_baselines/offsets",
				"load baselines or offsets to mysql",
				cmd,
				false
				);

    TCLAP::SwitchArg BaselineSwitch("B",
				    "Baseline_Mode",
				    "Loads baselines instead of offsets to mysql",
				    cmd,
				    false
				    );

    TCLAP::ValueArg<int> NHitOffset("H",
				    "NHit_Offset",
				    "If in baseline mode, loads new hitthresh values at this offset",
				    false,
				    -1,
				    "integer"
				    );

    cmd.add(NHitOffset);

    TCLAP::ValueArg<int> ZLEOffset("Z",                                                                  
				   "ZLE_Offset", 
				    "If in baseline mode, loads new zs_thresh_val values at this offset", 
				   false,                                                                                          
				   -1,                                                                                                  
				   "integer"                                                                                     
				   );
    
    cmd.add(ZLEOffset);

    TCLAP::MultiArg<int> InputPulse("I",
				    "Input",
				    "Choose Input to use for pulse finding",
				    false,
				    "Input pulse",
				    cmd
				    );


    cmd.parse( argc , argv );
    _Input = Var1.getValue();
    _load_mode=LoadSwitch.getValue();
    _baseline_mode=BaselineSwitch.getValue();
    _nhitOffset=NHitOffset.getValue();
    _zleOffset=ZLEOffset.getValue();
    _pulseInputs=InputPulse.getValue();
  }

  catch(TCLAP::ArgException &e)
    {
      fprintf(stderr, "Error %s for arg %s\n",
	      e.error().c_str(), e.argId().c_str());
      return 0;
    }

  fprintf(stderr,"NHit offset: %i\n",_nhitOffset);
  
  bool DAQFile;

  std::string fileType;
  size_t dotPos=_Input[0].find('.');
  fileType=_Input[0].substr(dotPos);

  if(boost::iequals(fileType,"dat"))
    {
      DAQFile=true;
    }
  fpos_t StartFilePos;
  RAT::DS::EV * ev=NULL;
  uint32_t sizeBlockRead=0;
  uint32_t * DAQBufferPtr=NULL;
  int TotalEventNumber=1;
  MemoryBlock * cBlock=NULL;
  MemoryBlock * dBlock=NULL;
  if(DAQFile)
    {
      fprintf(stderr,"DAQ file\n");
      InFile=fopen(_Input[0].c_str(),"rb");
 
 
      fgetpos(InFile,&StartFilePos);
      
       cBlock=new MemoryBlock();  //compressed memBlock
       cBlock->buffer=new uint32_t[100];
       cBlock->allocatedSize=100;
       dBlock=new MemoryBlock();  //decompressed memBlock
       dBlock->buffer=new uint32_t[100];
       dBlock->allocatedSize=100;
 
       ReadMemBlock(cBlock);
       //Now decompress memblock if necessary
       if(cBlock->uncompressedSize>0)
	 {
  
	   DecompressMemBlock(cBlock,dBlock);
	 }
       else
	 {
	   if(dBlock->allocatedSize<cBlock->dataSize)
	     {
	       delete (dBlock->buffer);
	       dBlock->buffer=new uint32_t[cBlock->dataSize];
	     }
	   memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
	   dBlock->dataSize=cBlock->dataSize;
	 }

       DAQBufferPtr=dBlock->buffer;
       uint32_t * initPtr = DAQBufferPtr;
       if(!(DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr)))
	 {
	   return 0;
	 }
       sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	
      
    }

  
  else
    {
      RAT::DSReader reader(_Input[0].c_str());		
      for( size_t m = 1 ; m < _Input.size() ; m++ )
	{
	  reader.Add(_Input[m].c_str());
	}
      TotalEventNumber = reader.GetTotal();
    }
  if(_pulseInputs.size()==0)
    _pulseInputs.push_back(7);
    
  // size_t BoardID = 0;
  /*  uint32_t rawOffset = 0;
  uint32_t boardTimeZero = 0;
  std::vector<double> DeltaTime;*/
 
  /*size_t Skip_Parameter = _parameterValue;
   char Hname[50], Htitle[50];
  char Gname[50];
  char Gtitle[50];
  TH1F * h = NULL;
  TGraph * g = NULL;
  TObjArray dtHistList(0);
  TObjArray dtEventSeriesList(0);
  TObjArray dtTimeSeriesList(0);
  TObjArray BaselineArrayObj(0);
  TObjArray SigmaHistogramObj(0);
  std::vector<TH1F*> BaselineArrays;
  TH1F * Bh = NULL;
  int HistogramWidth = 5;
  int HistogramBinNumber = 400;
  //   double mean_baseline = 0;
  // double baseline_variance = 0;
  //  size_t sampleNum1 = 20; //RENAME
  double multi_pulse_limit = 0;
  double baseline_problem_limit = 0;
  double absent_pulse_limit = 0;
  double whole_events_skipped = 0;
  int ChannelID = 0;
  TH1F * Sigmas = new TH1F("SigmaHist", "Histogram_of_Sigmas", 100, 0, 8);
  std::vector<double_t> flaggedSigmaValues;
  std::vector<double> BaselineValues;
  std::vector<int> goodEventCounters;
  int channelCount = 0;
  int TotalBoardCount = 0;
  std::vector<double> FinalTimingValues;
  std::vector<int> goodEventCounters2;
  */
  std::vector<ChannelBaselineInfo*> ChannelBaselineVector;
  

  if( TotalEventNumber <= 0 )
    {
      //printf("Warning: Total Event Number is zero or negative.\n");
      return -1;
    }

  for( int iEvent = 0 ; iEvent <TotalEventNumber; iEvent ++ )
    {
      if(DAQFile)
	{
	 
	  TotalEventNumber++;
	 
	  if(sizeBlockRead==dBlock->dataSize)
	    {
	      //End of memBlock, need to load new one
	 
	      int readRet=ReadMemBlock(cBlock);
	      if(readRet<0)
		{
		  return 0;
		}
	      else if(readRet==0);
	      {
		//fprintf(stderr,"Finished reading file \n");
		break;
	      }
      
	     
	     
	      if(cBlock->uncompressedSize>0)
		{
		  DecompressMemBlock(cBlock,dBlock);
		}
	      else
		{
		  if(dBlock->allocatedSize<cBlock->dataSize)
		    {
		      delete (dBlock->buffer);
		      dBlock->buffer=new uint32_t[cBlock->dataSize];
		    }
		  memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
		  dBlock->dataSize=cBlock->dataSize;
		}


	      DAQBufferPtr=dBlock->buffer;
	      uint32_t * initPtr = DAQBufferPtr;
	      if(!(DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr)))
		{
		  return 0;
		}
	      
	      sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	    }
	  
	  if(ev)
	    {
	      delete ev;
	      ev=NULL;
	    }
	  ev=new RAT::DS::EV();
	  int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ev);
	  if(size<0)
	    {
	      fprintf(stderr,"Error streaming ev \n");
	      return 0;
	    }
      
	  DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	  sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));

	  

	  
	}
      else
	{
	  RAT::DSReader reader(_Input[0].c_str());		
	  RAT::DS::Root * ds = reader.GetEvent(iEvent);
	  ev = ds->GetEV(0);
	}
      if(ev->GetReductionLevel()!=ReductionLevel::CHAN_ZLE_WAVEFORM and ev->GetReductionLevel()!=ReductionLevel::CHAN_FULL_WAVEFORM ) continue; //Skip event if not CHAN_ZLE_WAVEFORM
      int TotalBoardCount = ev->GetBoardCount();
      
      for( int iBoard = 0 ; iBoard < TotalBoardCount; iBoard++ )
	{
	  //fprintf(stderr,"iBoard %i\n",iBoard);
	  RAT::DS::Board * board = ev->GetBoard( iBoard );
	  int BoardID = board->GetID();
	  
	
	  int channelCount = board->GetChannelCount();
	   
	  for( int iChannel = 0; iChannel < channelCount ; iChannel++ )
	    {
	      RAT::DS::Channel * channel = board->GetChannel(iChannel);
	      for(int iRawBlock=0;iRawBlock<channel->GetRawBlockCount();iRawBlock++)
		{
		  RAT::DS::RawBlock * raw = channel->GetRawBlock(iRawBlock);
		  std::vector<uint16_t> samples = raw->GetSamples();
		  int ChannelID = channel->GetID();
		 
		 
		  bool foundChannel=false;
		  for(size_t i=0;i<ChannelBaselineVector.size();i++)
		    {
		      if(ChannelBaselineVector[i]->ChannelID==ChannelID)
			{
			  foundChannel=true;
			  if(ChannelBaselineVector[i]->BoardID!=BoardID || ChannelBaselineVector[i]->Input!=channel->GetInput())
			    {
			      fprintf(stderr,"Channel Labeling inconsitency for Channel %i \n",ChannelID);
			      return 0;

			    }
			  for(size_t j=0;j<samples.size();j++)
			    {
			      if(ev->GetReductionLevel()==ReductionLevel::CHAN_ZLE_WAVEFORM and j>1 and (samples.size()-j)>2) continue;
			      ChannelBaselineVector[i]->BaseLineHist->Fill(samples[j]);
			    }
			}
		    }
		  if(!foundChannel)
		    {
		      /*ChannelBaselineVector.push_back(ChannelBaselineInfo());
			ChannelBaselineVector.back().ChannelID=ChannelID;
			ChannelBaselineVector.back().BoardID=BoardID;
		      */
		      char name[100];
		      sprintf(name,"h%i",ChannelID);
		      ChannelBaselineInfo * tempChannelInfo=new ChannelBaselineInfo(name);
		      tempChannelInfo->ChannelID=ChannelID;
		      tempChannelInfo->BoardID=BoardID;
		      tempChannelInfo->Input=channel->GetInput();
		 
		      for(size_t j=0;j<samples.size();j++)
			{
			  tempChannelInfo->BaseLineHist->Fill(samples[j]);
			}
		      ChannelBaselineVector.push_back(tempChannelInfo);
		  
		    }     
		}
		  
	    }	      
	      
	}
    }
  //TCanvas * c=new TCanvas("c","c",600,600);
  
  
  int goodCounter=0;
  int badCounter=0;
  float maxOff=0;
  int boardOff=0;
  int inputOff=0;
  
  for(size_t i=0;i<ChannelBaselineVector.size();i++)
    {
      TF1 * tempFitFunction=new TF1("tff","gaus(0)",0,4096);
      TH1F * tempHist=ChannelBaselineVector[i]->BaseLineHist;
      int histMax=0;
      int maxBin=0;
      for(int j=0;j<tempHist->GetNbinsX();j++)
	{
	  if(tempHist->GetBinContent(j)>histMax)
	    {
	      histMax=tempHist->GetBinContent(j);
	      maxBin=j;
	    }
	}
      tempFitFunction->SetParameter(0,histMax);
      tempFitFunction->SetParameter(1,tempHist->GetBinCenter(maxBin));
      tempFitFunction->SetParameter(2,1);
      tempFitFunction->SetParLimits(0,0,histMax*10);
      tempFitFunction->SetParLimits(1,tempHist->GetBinCenter(maxBin)-100,tempHist->GetBinCenter(maxBin)+100);
      tempFitFunction->SetParLimits(2,.2,10);
      tempHist->Fit(tempFitFunction,"QB");
      
      
     
      //tempHist->Draw();
      fprintf(stderr,"Channel %i, %i.%i, Baseline is  %f \n",ChannelBaselineVector[i]->ChannelID,ChannelBaselineVector[i]->BoardID,ChannelBaselineVector[i]->Input,tempFitFunction->GetParameter(1));
      ChannelBaselineVector[i]->BaseLine=tempFitFunction->GetParameter(1);
      if(fabs(ChannelBaselineVector[i]->BaseLine-4000)>maxOff)
	{
	  maxOff=fabs(ChannelBaselineVector[i]->BaseLine-4000);
	  boardOff=ChannelBaselineVector[i]->BoardID;
	  inputOff=ChannelBaselineVector[i]->Input;
	}
      if(fabs(ChannelBaselineVector[i]->BaseLine-4000)<1)
	{
	  goodCounter++;
	}
      else
	{
	  badCounter++;
	}
      delete tempFitFunction;
     
    }

  fprintf(stderr,"%i good baseline channles, %i bad baseline channels, maximum miss is %3.2f (%i.%i)\n",goodCounter,badCounter,maxOff,boardOff,inputOff);

  TFile * outfile=new TFile("BaseLines.root","RECREATE");

  outfile->cd();
  char bufferName[100];
  for(size_t i=0;i<ChannelBaselineVector.size();i++)
    {
      snprintf(bufferName,sizeof(bufferName),"Channel %i, %i.%i",ChannelBaselineVector[i]->ChannelID,ChannelBaselineVector[i]->BoardID,ChannelBaselineVector[i]->Input);
      ChannelBaselineVector[i]->BaseLineHist->SetName(bufferName);
      ChannelBaselineVector[i]->BaseLineHist->Write();
    }
  if(_load_mode)
    {
      
      MYSQL* conn=mysql_init(NULL);

      conn=mysql_real_connect(conn,"192.168.22.6","control_user","27K2boil","daq_interface",0,NULL,0);

      if(!conn)
	{
	  fprintf(stderr,"MySQL connection error \n");
	  return 0;
	}
      
      std::stringstream query;
      MYSQL_RES * result=NULL;
      if(_baseline_mode)
	{
	  fprintf(stderr,"Loading new baselines into Settings_PMTs table in mysql run 0 \n");
	  //load new baselines into mysql run 0
	  for(size_t i=0;i<ChannelBaselineVector.size();i++)
	    {
	      
	      query<<"update Settings_PMTs set baseline="<<ChannelBaselineVector[i]->BaseLine<<" where run_number=0 and pmt_id="<<ChannelBaselineVector[i]->ChannelID<<";";
	      
	      if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
		{
		  fprintf(stderr,"Error inserting new baselines %s\n",query.str().c_str());
		  return 0;
		}
	      query.str(std::string());

	      if(_nhitOffset >= 0)
		{
		  //Update the NHit threshold for the new baseline
		  query<<"update Settings_PMTs set hitthresh="<<ChannelBaselineVector[i]->BaseLine - _nhitOffset<<" where run_number=0 and pmt_id="<<ChannelBaselineVector[i]->ChannelID<<";";
		  if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
		    {
		      fprintf(stderr,"Error inserting new NHit offset %s\n",query.str().c_str());
		      return 0;
		    }
		  query.str(std::string());
		}
	      
	      if(_zleOffset >= 0)
		{
		  //Update the ZLE threshold for the new baseline
		  query<<"update Settings_PMTs set zs_thresh_val="<<ChannelBaselineVector[i]->BaseLine - _zleOffset<<" where run_number=0 and pmt_id="<<ChannelBaselineVector[i]->ChannelID<<";";
		  if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
		    {
		      fprintf(stderr,"Error inserting new ZLE threshold %s\n",query.str().c_str());
		      return 0;
		    }
		  query.str(std::string());
		}
	      
	    }
	  
	}
      else
	{
	  fprintf(stderr,"Loading new offsets in DAC_Offsets table in mysql \n");
	  //load new DAC_Offsets
	  query<<"truncate table DAC_Offsets;";
	  if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
	    {
	      fprintf(stderr,"Error truncating table\n");
	      return 0;
	    } 
	  query.str(std::string());
	 
	  for(size_t i=0;i<ChannelBaselineVector.size();i++)
	    {
	      query.str(std::string());
	      query<<"select wfd_pos,wfd_id,dac_offset from Settings_PMTs where pmt_id ="<<ChannelBaselineVector[i]->ChannelID<<" and run_number = 0;";
	      if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
		{
		  fprintf(stderr,"Error getting current dac_offset values from database\n");
		  return 0;
		}
	      result=mysql_store_result(conn);
	      MYSQL_ROW row=mysql_fetch_row(result);
	      int wfd_pos=atoi(row[0]);
	      int wfd_id=atoi(row[1]);
	      int dac_offset=atoi(row[2]);
	  
	      mysql_free_result(result);
	      result=NULL;

	      if(wfd_pos!=ChannelBaselineVector[i]->Input || wfd_id!=ChannelBaselineVector[i]->BoardID)
		{
		  fprintf(stderr,"Channel labelling problem for channel %i \n",ChannelBaselineVector[i]->ChannelID);
		  return 0;
		}
	 
	      int new_dac_offset=int(float(ChannelBaselineVector[i]->BaseLine-4000)*float(13.8)+dac_offset);
	      query.str(std::string());
	 
	      query<<"insert into DAC_Offsets values ("<<ChannelBaselineVector[i]->ChannelID<<", "<<ChannelBaselineVector[i]->Input<<", "<<ChannelBaselineVector[i]->BoardID<<", "<<new_dac_offset<<", 45);";
	      if(mysql_real_query(conn,query.str().c_str(),query.str().size())!=0)
		{
		  fprintf(stderr,"Error adding new DAC_Offsets %s\n",query.str().c_str());
		  return 0;
		}
	    }
	}



    }
  else
    {
      fprintf(stderr,"NOT loading new offsets into mysql table \n");
    }
  /* Board Timing, should be used in different context
  std::vector<BoardTimingInfo*> BoardTimingVector;
  
  int boardOneIndex=0;

  for(size_t j=0;j<ChannelBaselineVector.size();j++)
    {
      if(ChannelBaselineVector[j]->BoardID==1 && ChannelBaselineVector[j]->Input==_pulseInputs[0])
	{
	  boardOneIndex=j;
	  break;
	}

    }
  if(DAQFile)
    {
      fsetpos(InFile,&StartFilePos);
      MemoryBlock* cBlock=new MemoryBlock();  //compressed memBlock
      cBlock->buffer=new uint32_t[100];
      cBlock->allocatedSize=100;
      MemoryBlock * dBlock=new MemoryBlock();  //decompressed memBlock
      dBlock->buffer=new uint32_t[100];
      dBlock->allocatedSize=100;
 
      ReadMemBlock(cBlock);
      //Now decompress memblock if necessary
      if(cBlock->uncompressedSize>0)
	{
  
	  DecompressMemBlock(cBlock,dBlock);
	}
      else
	{
	  if(dBlock->allocatedSize<cBlock->dataSize)
	    {
	      delete (dBlock->buffer);
	      dBlock->buffer=new uint32_t[cBlock->dataSize];
	    }
	  memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
	  dBlock->dataSize=cBlock->dataSize;
	}

      DAQBufferPtr=dBlock->buffer;
      uint32_t * initPtr = DAQBufferPtr;
      if(!(DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr)))
	{
	  return 0;
	}
      sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	
      
    }
  
  if(DAQFile)
    {
      TotalEventNumber--;
    }
  for(int iEvent=0;iEvent<TotalEventNumber;iEvent++)
    {
      if(DAQFile)
	{
	   
	  if(sizeBlockRead==dBlock->dataSize)
	    { //End of memBlock, need to load new one
	 
	      int readRet=ReadMemBlock(cBlock);
	      if(readRet<0)
		{
		  return 0;
		}
	      else if(readRet==0);
	      {
		//fprintf(stderr,"Finished reading file \n");
		break;
	      }
      

	      if(cBlock->uncompressedSize>0)
		{
		  DecompressMemBlock(cBlock,dBlock);
		}
	      else
		{
		  if(dBlock->allocatedSize<cBlock->dataSize)
		    {
		      delete (dBlock->buffer);
		      dBlock->buffer=new uint32_t[cBlock->dataSize];
		    }
		  memcpy(dBlock->buffer,cBlock->buffer,cBlock->dataSize*sizeof(uint32_t));
		  dBlock->dataSize=cBlock->dataSize;
		}


	      DAQBufferPtr=dBlock->buffer;
	      uint32_t * initPtr = DAQBufferPtr;
	      DAQBufferPtr=ReadPastRunHeader(DAQBufferPtr);
	      sizeBlockRead=(uint32_t)(DAQBufferPtr-initPtr);
	    }
	  
	  if(ev)
	    {
	      delete ev;
	      ev=new RAT::DS::EV();
	    }
	  int32_t size = StreamBufftoEV((uint8_t*)DAQBufferPtr,ev);
	  if(size<0)
	    {
	      fprintf(stderr,"Error streaming ev \n");
	      return 0;
	    }
      
	  DAQBufferPtr+=int(ceil(size/sizeof(uint32_t)));
	  sizeBlockRead+=int(ceil(size/sizeof(uint32_t)));

	  

	  
	}

      else
	{
	  RAT::DSReader reader(_Input[0].c_str());		
	  RAT::DS::Root * ds = reader.GetEvent(iEvent);
	  ev = ds->GetEV(0);
	}

      if(ev->GetTriggerPattern()& 0x80)
	{
	 
	  int TotalBoardCount = ev->GetBoardCount();
	  double tRef=0;
	  for(int iBoard=0;iBoard<TotalBoardCount;iBoard++)
	    {
	      if(ev->GetBoard(iBoard)->GetID()==1)
		{
		  uint32_t boardOffset=(ev->GetBoard(iBoard)->GetHeader()[3]&0x7FFFFFFF)<< 1;	
		 
		  tRef=ComputeHalfTime(ev->GetBoard(iBoard)->GetChannel(_pulseInputs[0])->GetRawBlock(0)->GetSamples(),0,ChannelBaselineVector[boardOneIndex]->BaseLine,boardOffset+ev->GetBoard(iBoard)->GetChannel(_pulseInputs[0])->GetRawBlock(0)->GetOffset());
		  break;
		}
	    }

				       
	  for(int iBoard=0;iBoard<TotalBoardCount;iBoard++)
	    {
	      RAT::DS::Board * board = ev->GetBoard(iBoard);
	  
	      for(size_t i=0;i<_pulseInputs.size();i++)
		{
		  for(int iChannel=0;iChannel<board->GetChannelCount();iChannel++)
		    {
		      RAT::DS::Channel *channel=board->GetChannel(iChannel);
		      if(channel->GetInput()==_pulseInputs[i])
			{
			  double t=0;
			  for(size_t k=0;k<ChannelBaselineVector.size();k++)
			    {
			      if(ChannelBaselineVector[k]->ChannelID==channel->GetID())
				{
				  uint32_t boardOffset=(ev->GetBoard(iBoard)->GetHeader()[3]&0x7FFFFFFF)<< 1;	
			     

				  t=ComputeHalfTime(ev->GetBoard(iBoard)->GetChannel(iChannel)->GetRawBlock(0)->GetSamples(),0,ChannelBaselineVector[k]->BaseLine,boardOffset+ev->GetBoard(iBoard)->GetChannel(iChannel)->GetRawBlock(0)->GetOffset());
			     
				  break;
				}
			    }
			  bool foundBoard=false;
			  for(size_t j=0;j<BoardTimingVector.size();j++)
			    {
			      if(BoardTimingVector[j]->BoardID==board->GetID())
				{
				  foundBoard=true;
				  // fprintf(stderr,"t-tref = %f \n",t-tRef);
				  BoardTimingVector[j]->TimingHist->Fill(t-tRef);
				  break;
				}
			    }
			  if(!foundBoard)
			    {
			      
			      char nameT[100];
			      sprintf(nameT,"hb%i",ev->GetBoard(iBoard)->GetID());
			      BoardTimingInfo * tempTimingInfo=new BoardTimingInfo(nameT);
			      tempTimingInfo->TimingHist->Fill(t-tRef);
			      tempTimingInfo->BoardID=ev->GetBoard(iBoard)->GetID();
			      BoardTimingVector.push_back(tempTimingInfo);
			    }


			}
   
		  
		    }
		}


	    }

	}


    }
  
  if(BoardTimingVector.size()>0)
    {
     

      TFile * outfileT=new TFile("Timing.root","RECREATE");

 
  
      outfileT->cd();
      char bufferNameT[100];
    
      for(size_t i=0;i<BoardTimingVector.size();i++)
	{
	
	TF1 * timingFitFunction=new TF1("f","gaus(0)",-5,5);
	     int maxBin=0;
	     for(int j=0;j<BoardTimingVector[i]->TimingHist->GetNbinsX();j++)
	     {
	     if(BoardTimingVector[i]->TimingHist->GetBinContent(j)>maxBin)
	     {
	     maxBin=BoardTimingVector[i]->TimingHist->GetBinContent(j);
	     }
	     }
	     timingFitFunction->SetParameter(0,maxBin*10);
	     timingFitFunction->SetParameter(1,BoardTimingVector[i]->TimingHist->GetMean());
	     timingFitFunction->SetParameter(2,.1);
	     timingFitFunction->SetParLimits(0,0,maxBin*50);
	     timingFitFunction->SetParLimits(1,BoardTimingVector[i]->TimingHist->GetMean()-4,BoardTimingVector[i]->TimingHist->GetMean()+4);
	     timingFitFunction->SetParLimits(2,.001,1);
	     BoardTimingVector[i]->TimingHist->Fit(timingFitFunction,"QB");
	  

  	  fprintf(stderr,"Board %i is off by %3.2f ns \n",BoardTimingVector[i]->BoardID,4*BoardTimingVector[i]->TimingHist->GetMean());
      
      
	  snprintf(bufferNameT,sizeof(bufferNameT),"Board %i",BoardTimingVector[i]->BoardID);
	  BoardTimingVector[i]->TimingHist->SetName(bufferNameT);
	  BoardTimingVector[i]->TimingHist->Write();
	}

       outfileT->Close();
       delete outfileT;
  
    }
  */
  outfile->Close();
  delete outfile;
 
      //TCanvas * c=new TCanvas("c","c",600,600)
      //ChannelBaselineVector[0]->Hist->SaveAs("Event0.C");
      //c->Save("Event0.C");
      //ChannelBaselineVector[1]->Hist->Draw();
  

  return 0;
}
  /*
      //This segment continues with the calculation of the times of the pulses, and prints the histograms of these values for each board over all events.
      //By the end of this segment, the final timing values for each board appear in FinalTimingValues.
      
      if( DeltaTime[0] == -9999.99 )
	{
	  printf("No standard (Board 1) time recorded for Event %d. Skipping Event.\n" , iEvent );
	  whole_events_skipped++;
	  if(whole_events_skipped >= 400)
	    {
	      printf("Too many events without standard (Board 1) time. Quitting.");
	      return -1;
	    }
	  continue;
	}
      else
	{
	  for( size_t i = 1 ; i < DeltaTime.size() ; i++ )
	    {
	      if( DeltaTime[i] != -9999.99 && DeltaTime[i] != -1 )
		{
		  ((TH1F*) dtHistList[i-1])->Fill(DeltaTime[i]-DeltaTime[0]);
		  ((TGraph*)dtEventSeriesList[i-1])->SetPoint( iEvent , iEvent , DeltaTime[i]-DeltaTime[0]);
		  //		  ((TGraph*)dtTimeSeriesList[i-1])->SetPoint( iEvent , DeltaTime[0] , DeltaTime[i]-DeltaTime[0]);
		  ((TGraph*)dtTimeSeriesList[i-1])->SetPoint( iEvent , 
							      double(ev->GetTriggerSummary().utc.GetTimeSpec().tv_sec) + double(ev->GetTriggerSummary().utc.GetTimeSpec().tv_nsec*1.0E-9), 
							      DeltaTime[i]-DeltaTime[0]);
		  //		  fprintf(stderr,"%ld %ld\n",
		  //  ev->GetTriggerSummary().utc.GetTimeSpec().tv_sec,
		  //  ev->GetTriggerSummary().utc.GetTimeSpec().tv_nsec );
		}
	    }
	}
      for( size_t iTime = 0; iTime < FinalTimingValues.size() ; iTime++ )
	{
	  
	  if(DeltaTime[iTime] == -1)
	    {
	      if(iTime == 0)
		{
		  printf("Board %zu, Event %d, has an above-4095 baseline. Canceling timing calculations for this event.\n" , iTime+1 , iEvent );
		  break;
		}
	      if(iTime != 0)
		{
		  printf("Board %zu, Event %d, has an above-4095 baseline.\n" , iTime+1 , iEvent );
		  continue;
		}
	    }
	  else
	    {
	      if( DeltaTime[iTime] == -9999.99 )
		{
		  if(iTime == 0)
		    {
		      break;
		    }
		  if(iTime != 0)
		    {
		      continue;
		    }
		}
	      if( DeltaTime[iTime] != -9999.99 )
		{
		  FinalTimingValues[iTime] += (DeltaTime[iTime] - DeltaTime[0]);
		  goodEventCounters2[iTime] += 1; 
		}
	    }
	}

    }
    
  for( size_t iTime = 0 ; iTime < FinalTimingValues.size() ; iTime++ )
    {
      FinalTimingValues[iTime] = FinalTimingValues[iTime]/goodEventCounters2[iTime];
    }



  //This segment generates the fits to the baseline histograms. It also creates a histogram (SigmaHist) of the the sigmavalues of all of these histograms.
  //



  double_t p0 = 0;
  double_t p1 = 0;
  double_t p2 = 0;
  double_t CenterMax = 0;
  TF1 * F1 = new TF1("f1" ,"gaus", 0 , 4096);
  F1->SetNpx(200);
  for( size_t i = 0; i < BaselineArrays.size() ; i++ )
    {

      BaselineArrayObj.Add(BaselineArrays[i]);
      
    }
  for( size_t iHist = 0 ; iHist < BaselineArrays.size() ; iHist++ )
    {
      CenterMax = BaselineArrays[iHist]->GetBinCenter(BaselineArrays[iHist]->GetMaximumBin());
      F1->SetParameters(1000000,CenterMax,1);
      F1->SetParLimits(0, 0, 100000000);
      F1->SetParLimits(1, 0, 4096);
      F1->SetParLimits(2, .2, 10);
      BaselineArrays[iHist]->Fit("f1","B" "Q");
      p0 = F1->GetParameter(0);//one million
      p1 = F1->GetParameter(1);//4000
      p2 = F1->GetParameter(2);//one
      //printf("First Parameter is %f.\n" , p0);
      //printf("Second Parameter is %f.\n" , p1);
      //printf("Last parameter is %f.\n" , p2);
      Sigmas->Fill(p2);
      flaggedSigmaValues.push_back(p2);
    }
  delete F1;
   
  TF1 * F2 = new TF1("f2" , "gaus" , 0 , 4096);
  CenterMax = Sigmas->GetBinCenter(Sigmas->GetMaximumBin());
  F2->SetParameter(1,CenterMax);
  Sigmas->Fit("f2", "Q");
  p1 = F2->GetParameter(1);
  p2 = F2->GetParameter(2);
  printf("Sigma of Sigma Hist is: %f.\n", p2);
  if(flaggedSigmaValues.size() > 0 )
    {
      for( size_t iSigma = 0 ; iSigma < flaggedSigmaValues.size() ; iSigma++ )
	{
	  if( flaggedSigmaValues[iSigma]-4*p2 > p1 || flaggedSigmaValues[iSigma]+4*p2 < p1 )
	    {
	      printf("Problem sigma: Board %d, Channel %d.\n" , int(floor((iSigma)/channelCount))+1 , int(iSigma)%channelCount+1);
	    }
	}
    }    	    
  delete F2;    
  SigmaHistogramObj.Add(Sigmas);
  TFile r("Sigma_Histogram.root" , "recreate");
  SigmaHistogramObj.Write();
  r.Close();
  TFile f("Timing_Histograms.root" , "recreate");
  dtHistList.Write();
  f.Close();
  TFile d("Timing_Graphs.root" , "recreate");
  dtEventSeriesList.Write();
  dtTimeSeriesList.Write();
  d.Close();
  TFile s("BaselineHistograms.root" , "recreate");
  BaselineArrayObj.Write();
  s.Close();
  printf("\n\n");



  //This segment does the work of printing results to the screen.


 		      

  printf("**********************************************\n");
  printf("             Baseline Measurments\n");
  printf("**********************************************\n");
  for(size_t iBaseline = 0 ; iBaseline < BaselineValues.size() ; iBaseline++)
    {
      switch(readability)
	{
	case false:
	  {
	    if( BaselineValues[iBaseline] != 4095 )
	      {
		printf("%d %d %f\n" ,  int(floor((iBaseline)/channelCount))+1 , int(iBaseline)%channelCount , BaselineValues[iBaseline]);
	      }
	    else
	      {
		printf("%d %d %f\n" ,  int(floor((iBaseline)/channelCount))+1 , int(iBaseline)%channelCount , -1.00 );
	      }
	    break;
	  }
	case true:
	  {
	    if( BaselineValues[iBaseline] != 4095 )
	      {
		printf("Baseline Value for Board %d, Channel %d, is: %f.\n" , int(floor((iBaseline)/channelCount))+1 , int(iBaseline)%channelCount , BaselineValues[iBaseline]);
	      }
	    else
	      {
		printf("Baseline is above 4095 for Board %d, Channel %d.\n" , int(floor((iBaseline)/channelCount))+1 , int(iBaseline)%channelCount);
	      }
	    break;
	  }
	default:
	  printf("You have reached the unreachable. Bug in programming. Call Ryan at 509-496-8705.");
	  break;
	}
    }

  printf("**********************************************\n");
  printf("              Timing Measurements\n");
  printf("**********************************************\n");
  switch(readability)
    {
    case false:
      {
	for(size_t iOffset = 0 ; iOffset < DeltaTime.size() ; iOffset++ )
	  {
	    printf("%zu %f %f\n" , iOffset+1 , FinalTimingValues[iOffset]*4, FinalTimingValues[iOffset]);
	  }
	break;
      }
    case true:
      {
	for(size_t iOffset = 0; iOffset < DeltaTime.size() ; iOffset++ )
	  {
	    printf("Timing Offset for Board %zu is: %f nanoseconds and %f samples.\n", iOffset+1 , (FinalTimingValues[iOffset]), FinalTimingValues[iOffset]);
	  }
	break;
      }
    default:
      printf("You have reached the unreachable. Contact Ryan for inquisition-ing and flogging.");
    }

   return 0;
}	  
  */





