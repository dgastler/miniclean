#ifndef __PRINTERRORS__
#define __PRINTERRORS__
/*

 */

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

class PrintErrors : public DCThread 
{
 public:
  PrintErrors(std::string Name);
  ~PrintErrors();

  //Called by DCDAQ to setup your thread.
  //if this is returns false, the thread will be cleaned up and destroyed. 
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  
  virtual void MainLoop(){};
 private:
  virtual bool ProcessMessage(DCMessage::Message &message);

};
#endif
