//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TObjArray.h"

using namespace std;



int main(int argc, char ** argv)
{
  if(argc < 3)
    {
      cout << argv[0] << "RunMap inFiles" << endl;
      return(0);
    }

  TFile * InFile = 0;//new TFile();
  TH1F * LightYield = new TH1F("LightYield","Run Light Yield",500,0,500);
  TH1F * h1 =0;
  TF1  * f1 = 0;
  

  Float_t Yield = 0;
  Int_t   NY = 0;
  Float_t dYield = 0;
  Float_t Yield2 = 0;

  Float_t * Yields = new Float_t[(argc - 1)];

  vector<TH1F*> Histograms;

  for(int i = 2; i < argc;i++)
    {
      InFile = new TFile(argv[i],"READ");
      //Rebin Light spectrum
      h1 = (TH1F *) ((TH1F *) InFile->Get("cLight"))->Rebin(5,"h1");
      h1->GetXaxis()->SetRangeUser(200,700);
      
      f1 = new TF1("f1","gaus",
		   h1->GetBinCenter(h1->GetMaximumBin()-2),
		   h1->GetBinCenter(h1->GetMaximumBin()+2));
      h1->Fit(f1,"QR");
      
      std::string RunNumber(argv[i]);
      RunNumber = RunNumber.substr(RunNumber.find("run_")+4,8);

      //      cout << RunNumber << " " << h1->GetBinCenter(h1->GetMaximumBin()) << " " << h1->GetBinCenter(h1->GetMaximumBin())/122.0 << endl;

      cout << RunNumber << " " << f1->GetParameter(1) << " " << f1->GetParameter(1)/122.0 << endl;

      Yields[i-1] = f1->GetParameter(1);
      NY++;
      LightYield->SetBinContent(atoi(RunNumber.c_str()),f1->GetParameter(1));
      Histograms.push_back(h1);
    }
  for(int i = 0; i < argc - 1;i++)
    {
      Yield += Yields[i];
      Yield2 += Yields[i]*Yields[i];
    }
  Yield /= NY ;

  dYield = (1/sqrt(NY-1))*sqrt(Yield2 - NY*Yield*Yield);

  cerr << Yield << " " << Yield/122.0 << " +/- " << dYield/122.0 << endl;


  //Make LightYield list for neutron runs. 
  fstream RunMapFile;
  RunMapFile.open(argv[1],fstream::in);
  char * CharBuffer = new char[1000];
  std::string StringBuffer;
  std::vector<int> NMap;
  std::vector<int> CMap;
  while(!RunMapFile.eof())
    {
      RunMapFile.getline(CharBuffer,1000);
      StringBuffer.assign(CharBuffer);
      if(StringBuffer.find(" ") != std::string::npos)
	{
	  NMap.push_back(atoi(
			      (StringBuffer.substr(
						   0,
						   StringBuffer.find(" ")
						   )).c_str()));
	  CMap.push_back(atoi(
			      (StringBuffer.substr(
						   StringBuffer.find(" "),
						   StringBuffer.size() - StringBuffer.find(" ")
						   )).c_str()));
	  
	}
    }
  delete [] CharBuffer;
  TH1F * NeutronLightYield = (TH1F*) LightYield->Clone("NeutronLightYield");
  for(unsigned int i = 0; i < NMap.size();i++)
    {
      NeutronLightYield->SetBinContent(NMap[i],
				       LightYield->GetBinContent(CMap[i])/122.0  );
      printf("Using Cobalt run: %03d (%5f) for Neutron run %03d\n",
	     CMap[i],
	     LightYield->GetBinContent(CMap[i]),
	     NMap[i]);
    }
  TFile * OutFile = new TFile("Cal.root","RECREATE");
  LightYield->Write();
  NeutronLightYield->Write();
  for(int i = 0; i < Histograms.size();i++)
    {
      Histograms[i]->Write();
    }
  OutFile->Close();

  //Generate XML file
  fstream CalFile;
  CalFile.open("Cal.xml",fstream::trunc | fstream::out);
  CalFile << "<CAL keVee=\"" << Yield/122.0 << "\"/>" << endl;
  CalFile.close();
  
  return(0);
}
