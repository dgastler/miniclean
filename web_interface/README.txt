PHYSICS UI README

**********************************************************************************

color_scheme.php : 
Contains hex values of different shades of colors, which then can be assigned to different properties of the page

**********************************************************************************

ui.class.php :

Creates a PCBox object containing functions to interface between the database and the php scripts the page calls. All the mysql queries are executed and their results processed here.
Most of the functions are pretty self explanatory and only consist of a query to one table, except for getThreadData, processData, and getColumnNames, which are applied to all tables

functions:

__construct() & DbConnect()  = open and select database

getStatus() = returns current status and run number from RUNLOCK table

getPCs($runNumber) = returns all of the PCs for a given run number

getIPs($runNumber) = returns the IP address of all of the PCs for a given run number

getPCThreads($pcName,$runNumber) = returns the columns from table Thread_Threads for a given pc name and run number

getPCMMs($pcName,$runNumber) = returns the columns from table Memory_MemoryManagers for a given pc name and run number

getThreadTemplate($table) = returns an array of the thread_type entries in a given table

getThreadType($table) = returns an array of thread_type entries in a given table
(same as ThreadTemplate???)

getData($pcName,$type,$table,$runNumber) = select the data from a single column in a given table, corresponding to a specific pc name and run number, as well as a specified thread type or mm type ($type variable)

processData($pcName,$nameID,$table,$data,$dataIndex) = used to alter the data in a given table in the database depending on the parameters of the pc name, the thread name, mm name, board ID, or wfd_pos; and places the data input at the data index.

getColumnNames($pcName,$type,$table,$runNumber) = used to get a list of column names from a given table for the parameters of pc names and run numbers and name or id or type

getRunNumbers() = returns all of the run numbers from the RunNames table

stopRun() = sets the access field to 'STOP' in the database

startRun() = sets the access field top 'NEW_RUN' in the database

getWFDs($run_number) = returns all of the WFDs for a given run number

getSettings($run_number,$boardID,$table) = returns all of the settings for a given wfd board id for a given run number

getPMTs($run_number,$boardID) = returns the pmts for a given WFD for a run number

getPMTSettings($run_number,$boardID,$channel) = returns all of the data for a pmt channel for a boardID for a given run number

clearRunZero() = deletes all of the data stored in run 0 in all of the tables

copyRun($run_number) = moves all of the data from the given run number to a temporary table, and then copy it into run 0

copyIndividualRow($table,$runFrom,$name) = copies a single row (add it to what currently is stored) into run 0

**********************************************************************************

checkStatus.php :

Calls the checkStatus() function on the pcBox object created in the ui.class.php script. This returns a JSON string of what is contained in the RUNLOCK table (run status and current run number).

Command line example : (it does not take parameters, so you do not need to change anything from within the script or any input)
Run as: php checkStatus.php

When current status is FREE:

["FREE:0"]

When current status is RUNNING:

///ENTER SOMETHING HERE WHEN DAQ IS WORKING AGAIN

**********************************************************************************

startRuns.php

Calls the startRun() function on the pcBox object from the ui.class.php and returns whether it was successful or not

Run as: php startRuns.php

Returns: 1

**********************************************************************************

stopRuns.php

Calls the stopRun() funciton on the pcBox object from the ui.class.php and returns whether it was successful or not

Run as: php startRuns.php

Returns: 1

**********************************************************************************

clearExistingData.php

Calls the clearRunZero() function on the pcBox object from the ui.class.php and returns whether it was successful or not

Run as: php clearExistingData.php

Returns: 1

**********************************************************************************

copyRun.php

Calls the copyRun($run_number) from the pcBox object in the ui.class.php. To changethe run number manually, change the $run_number variable to an existing run number. It will return a 1 if the run has successfully been copied.

Run as: php copyRun.php 'runNumber=-5'
where runNumber is the number of the row you want to copy

Returns: 1

***********************************************************************************

processData.php

Calls the processData($pcName,$nameID,$table,$data,$dataindex) function from the pcBox object. The variables can either be posted from the web page or manually chosen by the user and changed. This prints the SQL query upon completion.

Can either post variable names from the web page to the script, and leave the manual input commented out, or enter in variable names and comment out the post calls.

To change all data for any nameID (thread_name for Thread_Threads, SThread_DecisionMaker, SThread_Data_Raw, SThread_Data_Compressed; BoardID for Settings_WFDs; wfd_pos for Settings_PMTs; mm_name for Memory_MemoryManagers), change the nameID variable to '.*'
This also applies to pcNames - you can change all data in a given pc

These are all regex strings! If you want something a little more specific than .* and less than changing an individual component on the webpage, you can do that to.

Changing the variables manually:
//The '.*' inside the nameID variable will change data_01 in 'Thread_Threads' for ANY $nameID (corresponding to thread_name for the 'Thread_Threads' table) where the pcName is 'DREBPC', and in run 0 (this is in the sql query; you can only change run 0)

run as: php processData.php 'table='Thread_Threads'&pcName='DREBPC'&nameID='.*'&dataindex=5&input_data='moo''

Run as: php processData.php

Returns: "UPDATE Thread_Threads SET data_01='moo' WHERE thread_name REGEXP '.*' AND pc_name REGEXP 'DREBPC' AND run_number=0"

This changes all of the data_01 fields in the Thread_Threads table to 'moo'
Before:

+------------+
| data_01    |
+------------+
| EBPC_MM    |
| EBPC_MM    |
| DRPC_MM    |
| 2          |
| EBPC_MM    |
| RAWMBLK_MM |
| DRPC_MM    |
| RATCopy_MM |
| DRPC_MM    |
| 2          |
| 2          |
| 2          |
| DRPC_MM    |
| DRPC_MM    |
| CMPMBLK_MM |
| DRPC_MM    |
| DRPC_MM    |
| EBPC_MM    |
+------------+

After:

+---------+
| data_01 |
+---------+
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
| moo     |
+---------+

***********************************************************************************

copyIndividualRow.php :

Calls the copyIndividualRow function from the pcBox instantiation of the ui.class.php class. This function takes in a run number to copy from, the table to copy to, and the row to copy.

This functions close to the copyRun.php script, but instead of copying over an entire run into run 0, it will just add a template row into a specified table. It is used for the 'Add a ...' menus, so you can add a thread/mm/wfd/channel without adding a whole new run.

You can either post the variables from the web page, or specify them manually inside the script.


**********************************************************************************

getColumnNames.php : 

Returns a json encoded string of all of the columns names for a given type, pcName, table, and run number. It calls the getColumnNames function from ui.class.php. To pass in variables, either post them from the webpage or manually enter them in the script.

To manually specify the variables, run as: php getColumnNames.php 'runNumber=-5&table='Thread_Threads'&pcName='DREBPC'&type='FE_Pusher''

where table corresponds to the table whose columns you want to view, and runNumber, pcName, and type correspond to the run and pc and type (i.e. thread type or mm type) whose field names you want to view

This will print an output of:
["run_number","order_id","thread_name","thread_type","pc_name","data_01","data_02","data_03","data_04","data_05","data_06","data_07","data_08","data_09","data_10","data_11","data_12","data_13","data_14","data_15","data_16","data_17","data_18","data_19","data_20","data_21","data_22","data_23","data_24","data_25","data_26","data_27","data_28","data_29","data_30","data_31","data_32","channel_map"]

And gives you all of the column names for that table

***********************************************************************************

getData.php : 

Calls the getData($pcName,$type,$table,$runNumber) function from the pcBox object, and returns the data stored in the table given as input, for the run number and pc name and field given. 

The variables can either be posted from the webpage, or entered manually. 

To manually specify variables, run as: php getData.php 'runNumber=-5&table='Memory_MemoryManagers'&pcName='FEPC1'&type='MemoryBlockManager''

where table instructs which table to get data from, and runNumber, pcName, and type correspond to the run and pc of the data, and what type (i.e. thread type or mm type)

This will give an output of:
["-1","1","MBLK1_MM","MemoryBlockManager","FEPC1","0x400200","40","","","","","","","",""]

Which is the data stored in the Memory_MemoryManagers table where run_number=-5, the pc_name = 'FEPC1', and the mm_type = 'MemoryBlockManager'

***********************************************************************************

getIPs.php : 

Calls the getIPs($runNumber) function from the pcBox object in ui.class.php. This returns all of the IP addresses stored for that run number in the NetworkConnections table

The run number can either be posted from the webpage or manually entered in the script.

To manually specify the variable, run as: php getIPs.php 'runNumber=-7'

where runNumber is the run number whose IP addresses you want to view

This returns:
["192.168.0.1:DREBPC","192.168.0.2:FEPC1","192.168.0.3:FEPC2","192.168.0.4:FEPC3","192.168.0.5:FEPC4"]

This is a JSON string of all of the IP addresses for that run number, and the PCs they correspond with

***********************************************************************************

getMMs.php : 

Calls the getPCMMs($pcName,$runNumber) function from the pcBox object in ui.class.php. This returns a JSON string of the memory manager types and names for that given pc and run number

To manually specify the variable, run as: php getMMs.php 'runNumber=-7&pcName='DREBPC''

where runNumber and pcName correspond to the pc and run whose memory managers you want to view

This returns:
["MemoryBlockManager:CMPMBLK_MM","MemoryBlockManager:RAWMBLK_MM","DRPCRatManager:DRPC_MM","RatDiskBlockManager:EBPC_MM"]

***********************************************************************************

getPCs.php : 

Calls the getPCs($runNumber) function from the pcBox object in ui.class.php. This returns a JSON string of all of the PCs stored in that run number

To manually specify the variable, run as: php getPCs.php 'runNumber=-5'

where runNumber is the run you want to see the PCs from

This returns:
["FEPC4:192.168.0.5:1717","FEPC3:192.168.0.4:1717","FEPC2:192.168.0.3:1717","FEPC1:192.168.0.2:1717","DREBPC:192.168.0.1:1717"]

This is a JSON string of all of the PCs, their IP addresses, and the ports they correspond with

***********************************************************************************

getPMTSettings.php : 

This calls the getPMTSettings($run_number,$boardID,$channel) function from the pcBox object in ui.class.php. This returns a JSON string of the data for the specified channel on a boardID

To manually specify the variables, run as: php getPMTSettings.php 'runNumber=-5&boardID=8&channel=0'

where runNumber represents the run, and boardID and channel represent the PMT on the WFD whose settings you want to see

This returns:
{"0":"-5","run_number":"-5","1":"64","pmt_id":"64","2":"0","wfd_pos":"0","3":"8","wfd_id":"8","4":"FEPC3","pc_name":"FEPC3","5":"4000","baseline":"4000","6":"5","presamples":"5","7":"2","zs_nsamp_nlbk":"2","8":"2","zs_nsamp_nlfwd":"2","9":"1","nsamples":"1","10":"1","channeltype":"1","11":"3995","hitthresh":"3995","12":"6610","dac_offset":"6610","13":"1","zs_thresh_edge_trig":"1","14":"3995","zs_thresh_val":"3995","15":"0","channel_type":"0"}

***********************************************************************************

getPMTs.php : 

This calls the getPMTs($run_number,$boardID) function from the pcBox object in ui.class.php. This returns a JSON string containing all of the PMTs on any given WFD for a specified run number

To manually specify the variables, run as: php getPMTs.php 'runNumber=-5&boardID=0'

where runNumber and boardID represent the run and wfd board whose channels you want to see

This returns:
["0","1","2","3","4","5","6","7"]

***********************************************************************************

getRuns.php : 

This calls the getRunNumbers() function from the pcBox object in ui.class.php. This returns a JSON string containing all of the distinct run numbers stored in the database. Because the function does not take any input from the user, it can just be run as: php getRuns.php

This returns:
["0:RAT_COPIER_test","-5:RAT_COPIER_test","-6:Fake V1720","-7:7WFDs DAQ calibrate","-8:6WFDs 8uS","-9:6WFDs 16uS","-10:13WFDs DAQ calibrate","-12:12WFDs 16uS","-13:13WFDs 16uS","-14:12WFDs 8uS","-15:13WFDs 8uS","-16:6WFDs DAQ calibrate","-17:12WFDs DAQ calibrate","-19:Veto testing","-26:End run testing","-300:test"]

This is a JSON string of all of the run numbers and their names

***********************************************************************************

getWFDSettings.php : 

This calls the getSettings($runNumber,$boardID,$table) function from the pcBox object in ui.class.php. This returns a JSON string containing all of the settings stored in a given WFD board ID for a given run number

To manually specify the variable, run as: php getSettings.php 'runNumber=-5&boardID=1&table='Settings_WFDs''

where runNumber and boardID represent the run and wfd ID you want to get the settings from, from the table 'Settings_WFDs'

This returns:
{"0":"-5","run_number":"-5","1":"FEPC1","pc_name":"FEPC1","2":"PCI","BdType":"PCI","3":"0","VMEBaseAddress":"0","4":"0","Link":"0","5":"0x38","VMEControl":"0x38","6":"0x20010","ChannelsConfig":"0x20010","7":"0xA","MemConfig":"0xA","8":"0x10281","FrontPanel":"0x10281","9":"0xC0000000","TriggerSource":"0xC0000000","10":"0xC0000000","TriggerOutput":"0xC0000000","11":"0x2","AcquisitionControl":"0x2","12":"0x0","MonitorMode":"0x0","13":"FEPC1_V1720","thread_name":"FEPC1_V1720","14":"1","CanStart":"1","15":"1","BoardID":"1","16":"1","BdNum":"1","17":"756","PostSamples":"756","18":"0","InternalDelay":"0","19":"1024","CustomSize":"1024","20":"1","zs_thresh_edge_trig":"1"}

***********************************************************************************

getThreads.php :

This calls the getPCThreads($pcName,$runNumber) function from the pcBox object in ui.class.php. This returns a JSON string containing all of the threads for a given PC in a run number

To manually specify the variable, run as: php getThreads.php 'runNumber=-5&pcName='DREBPC'

where runNumber and pcName correspond to the run and pc you want to get the threads from

This returns:
["RATCopier:RAT_Copier","DRPCAssembler:DRPC_Assembler1","DRPCAssembler:DRPC_Assembler2","DRPCAssembler:DRPC_Assembler3","DRPCAssembler:DRPC_Assembler4","DecisionMaker:Decision_Maker","FEPusher:FE_Pusher","EBPCAssembler:EBPC_Assembler_1","EBPCAssembler:EBPC_Assembler_2","EBPCAssembler:EBPC_Assembler_3","EBPCAssembler:EBPC_Assembler_4","DRPCBadEvent:DRPC_BadEvent","PackData:PackData1","MemBlockCompressor:Compressor1","MemBlockCompressor:Compressor2","MemBlockCompressor:Compressor3","MemBlockCompressor:Compressor4","MemBlockWriter:MemBlockWriter"]

This is a JSON string of the thread type:thread name for all of the threads

***********************************************************************************

getThreadType.php : 

This calls the getThreadType($table) function from the pcBoxObject in ui.class.php. This returns a JSON string of all of the different types of threads in the Thread_Threads database.

To manually specify the variable, run as: php getThreadType.php 'table='Thread_Threads''

This returns:
["DCMessageClient","MemBlockCompressor","PackData","DecisionMaker","DRTriggerAssembler","DRPCAssembler","EBPCAssembler","MemBlockWriter","DRPCBadEvent","FEPusher","V1720READER","FEPCAssembler","DRPusher","DRGetter","EBPusher","FEPCBadEvent","RunControl","V1720FakeReader","EBPCBadEvent","RATWriter","RATCopier"]

***********************************************************************************

getWFDs.php : 

This calls the getWFDs($runNumber) function from the pcBoxObject in ui.class.php. This returns a JSON string of all of the WFDs for a given run number.

To manually specify the variable, run as: php getWFDs.php 'runNumber=-8'

This returns:
["1","2","3","4","5","6"]

where runNumber is the number of the run you want to get the WFDs from

***********************************************************************************
