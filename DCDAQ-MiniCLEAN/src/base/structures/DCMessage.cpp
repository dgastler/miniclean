#include <DCMessage.h>



bool DCMessage::Message::Setup(xmlNode * setupNode)
{
  DCAddress dcAddress;
  if(FindSubNode(setupNode,"DEST") != NULL)
    {
      if(!dcAddress.Set(FindSubNode(setupNode,"DEST")))
	{
	  Clear();
	  return false;
	} 
      else
	{
	  SetDestination(dcAddress);
	}
    }
  else
    {
      dcAddress.Set();
      SetDestination(dcAddress);
    }

  if(FindSubNode(setupNode,"SOURCE") != NULL)
    {
      if(!dcAddress.Set(FindSubNode(setupNode,"SOURCE")))
	{
	  Clear();
	  return false;
	} 
      else
	{
	  SetSource(dcAddress);
	}
    }
  else
    {
      dcAddress.Set();
      SetSource(dcAddress);
    }

  
  std::string typeStr;
  if(FindSubNode(setupNode,"TYPE") != NULL)
    {
      GetXMLValue(setupNode,"TYPE","DCMessageSetup()",typeStr);    
      SetType(DCMessage::GetTypeFromName(typeStr.c_str()));
    }
  else
    {
      SetType(DCMessage::BLANK);
    }

  xmlNode * dataNode = NULL;
  if( (dataNode = FindSubNode(setupNode,"DATA")) != NULL)
    {      
      //Load text data
      //Load xml data
      if(FindSubNode(dataNode,"TEXT") != NULL)
	{
	  std::string text;
	  DCMessage::DCString dcString;
	  if(GetXMLValue(dataNode,"TEXT",
			 "DCMessageSetup()",
			 text) == 0)	  
	    {	 
	      std::vector<uint8_t> data = dcString.SetData(text);
	      SetData(&(data[0]),data.size());
	    }
	}
      else if(FindSubNode(dataNode,"XML") != NULL)
	{	 
	  xmlDoc * newDoc = xmlNewDoc(BAD_CAST "1.0");
	  if(FindSubNode(dataNode,"XMLFILE") == NULL)
	    {
	      xmlDocSetRootElement(newDoc,
				   FindIthSubNode(FindSubNode(dataNode,
							      "XML")
						  ,0));  
	    }
	  else
	    {
	      xmlDocSetRootElement(newDoc,
				   FindSubNode(dataNode,"XML"));  
	    }
	  xmlChar * launchXMLText;
	  int launchXMLTextSize;
	  xmlDocDumpFormatMemory(newDoc, &launchXMLText, &launchXMLTextSize, 1);
	  Data.resize(launchXMLTextSize);
	  memcpy(&(Data[0]),launchXMLText,Data.size());
	  free(launchXMLText);
	}
      //Load numerical data if no text data
      else if(FindSubNode(dataNode,"WORD32") != NULL)
	{
	  size_t numberOfWords = NumberOfNamedSubNodes(dataNode,"WORD32");
	  for(size_t iWord = 0; iWord < numberOfWords;iWord++)
	    {
	      uint32_t word;
	      if(GetIthXMLValue(dataNode,
				iWord,
				"WORD32",
				"DCMessageSetup()",
				word) == 0)
		{
		  Data.push_back(uint8_t((word >> 0 )&0xFF));
		  Data.push_back(uint8_t((word >> 8 )&0xFF));
		  Data.push_back(uint8_t((word >> 16)&0xFF));
		  Data.push_back(uint8_t((word >> 24)&0xFF));
		}
	    }
	}
      else
	{	  
	}
    }
  return true;
}

void DCMessage::Message::SetSource(std::string const &name,
				   struct sockaddr const * addr,
				   size_t addrSize)
{       
  Source.Set(name,addr,addrSize);
}

void DCMessage::Message::SetDestination(std::string const &name,
					struct sockaddr const * addr,
					size_t addrSize)
{       
  Destination.Set(name,addr,addrSize);
}


void DCMessage::Message::Clear()
{
  type = DCMessage::BLANK;
  timeStamp = 0;

  SetSource();
  SetDestination();

  Data.clear();
}


void DCMessage::Message::CopyObj(const DCMessage::Message &rhs)
{
  type = rhs.type;
  timeStamp = rhs.timeStamp;
  Data = rhs.Data;
  //Source information
  Source = rhs.Source;
  //Destination data
  Destination = rhs.Destination;
}

void DCMessage::Message::Set(const void * in,size_t bytes,std::vector<uint8_t> & store)
{
  //Make our vector large enough;
  store.resize(bytes);
  if(bytes >0)
    {
      //Copy the data from inData to Data
      memcpy((void *) &(store[0]),in,bytes);
    }
}

void DCMessage::Message::Add(const void * in, size_t bytes,std::vector<uint8_t> & store)
{
  //Make room for copy
  store.resize(store.size() + bytes);
  if(bytes > 0)
    {
      memcpy((uint8_t *)&(store[0]) + (store.size() - bytes),in,bytes);
    }
}

const DCMessage::DCAddress & DCMessage::Message::GetSource() const
{
  return Source;
}

const DCMessage::DCAddress & DCMessage::Message::GetDestination() const
{
  return Destination;
}

void DCMessage::Message::SetSource(const DCAddress &address)
{
  Source = address;
}

void DCMessage::Message::SetDestination(const DCAddress &address)
{
  Destination = address;
}

std::string DCMessage::Message::PrintPretty()
{
  //Print Message
  std::stringstream ss;
  ss << "\tSource: " << this->GetSource().GetStr() << std::endl;
  ss << "\tDestination: " << this->GetDestination().GetStr() << std::endl;
  ss << "\tType: " << DCMessage::GetTypeName(this->GetType()) << std::endl;
  ss << "\tData: " << std::endl; 
  std::vector<uint8_t> data = this->GetData();     
  std::string text;
  std::vector<std::string> strs;
  uint32_t * ptr32 = NULL;
  uint32_t ptr_size32 = 0;
  switch (this->GetType())
    {
    case DCMessage::WARNING:
    case DCMessage::TEXT:
    case DCMessage::LAUNCH:
      //convert data to char* to string
      text.assign((char*)(&data[0]));
      //Break up the new lines
      boost::split(strs,text,boost::is_any_of("\n"));
      for(size_t iLine = 0; iLine < strs.size();iLine++)
	{
	  ss << "\t\t" << strs[iLine] << std::endl;
	}
      break;
    default:
      ss << "\t\tBINARY" << std::endl;
      //Loop over 32bit words	  
      //convert data to an array of 32bit unsinged ints
      ptr32 = (uint32_t*) (&data[0]);
      ptr_size32 = data.size() >> 2;
      for(size_t iData = 0; iData < ptr_size32;iData++)
	{
	  ss << "\t\t0x" << std::hex << ptr32[iData] << std::endl;
	}
      break;
    }
  ss << std::endl;
  return ss.str();
}
