--This module encapsulates the send and receive MAC interfaces to the PHY.  
--Since it is a MAC, its main purpose is to deal with the connection's speed 
--autonegotiation, preamble bits, and trailer checksums. Some traditional 
--features (such as half-duplex mode) are not implemented.
--Original code by Xilinx.
--Translation into VHDL by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ge_mac_stream is Port(
	PHY_RESET_L : out STD_LOGIC;
	PHY_MDC : out STD_LOGIC;
	PHY_MDIO : inout STD_LOGIC;
	GE_RXCLK : in STD_LOGIC;
	GE_RXDV : in STD_LOGIC;
	GE_RXD : in STD_LOGIC_VECTOR(7 downto 0);
	GE_TXEN : out STD_LOGIC;
	GE_TXD : out STD_LOGIC_VECTOR(7 downto 0);
	FE_TXCLK : in STD_LOGIC;
	CLK125 : in STD_LOGIC;
	RESET : in STD_LOGIC;
	GOODBAD_FRAME : out STD_LOGIC_VECTOR(1 downto 0);
	ETH_RX_STREAM : out STD_LOGIC_VECTOR(9 downto 0);
	TX_EN : out STD_LOGIC;
	LINK_SPEED : out STD_LOGIC;
	ETH_TX_STREAM : in STD_LOGIC_VECTOR(8 downto 0));
end ge_mac_stream;

architecture Behavioral of ge_mac_stream is
	signal fe_txclk_inff : STD_LOGIC := '0';
	signal fe_txclk_dly : STD_LOGIC_VECTOR(1 downto 0) := "00";--used to align TXD with 25Mhz clk in MII mode
	signal fe_out_tick : STD_LOGIC;
	signal send_cke : STD_LOGIC;
	signal e_tgl : STD_LOGIC := '0';--toggles every detected rising edge of fe_txclk_inff
	signal mid_rx_stream : STD_LOGIC_VECTOR(9 downto 0);
	signal mid_tx_stream : STD_LOGIC_VECTOR(9 downto 0);
	signal mid_tx_stream2 : STD_LOGIC_VECTOR(9 downto 0);
	signal speed : STD_LOGIC_VECTOR(1 downto 0);

	component eth_mdio port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		E_RST_L : out STD_LOGIC;
		E_MDC : out STD_LOGIC;
		E_MDIO : inout STD_LOGIC;
		E_LINK_SPEED : out STD_LOGIC_VECTOR(1 downto 0));
	end component;

	component gmii_eth_tx_stream port(
		CLK125 : in STD_LOGIC;
		RESET : in STD_LOGIC;
		TXD : out STD_LOGIC_VECTOR(7 downto 0);
		TXCTRL : out STD_LOGIC;
		ETH_TX_STREAM : in STD_LOGIC_VECTOR(9 downto 0));
	end component;

	component gmii_eth_rcv_stream port(
		CLK125 : in STD_LOGIC;
		RESET : in STD_LOGIC;
		RXCLK : in  STD_LOGIC;
		RXD : in  STD_LOGIC_VECTOR(7 downto 0);
		RXCTRL : in STD_LOGIC;
		SPEED : in STD_LOGIC_VECTOR(1 downto 0);
		ETH_RX_STREAM : out STD_LOGIC_VECTOR(9 downto 0));
	end component;
	
	component eth_rx_crc port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		IN_ETH_STREAM : in STD_LOGIC_VECTOR(9 downto 0);
		OUT_ETH_STREAM : out STD_LOGIC_VECTOR(9 downto 0);
		OUT_OK_STB : out STD_LOGIC;
		OUT_BAD_STB : out STD_LOGIC);
	end component;
	
	component eth_tx_crc port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		IN_ETH_STREAM : in STD_LOGIC_VECTOR(9 downto 0);
		OUT_ETH_STREAM : out STD_LOGIC_VECTOR(9 downto 0));
	end component;
	
	component eth_tx_pad port(
		CLK : in STD_LOGIC;
		RESET : in STD_LOGIC;
		IN_ETH_STREAM : in STD_LOGIC_VECTOR(9 downto 0);
		OUT_ETH_STREAM : out STD_LOGIC_VECTOR(9 downto 0));
	end component;
begin
	-- Ethernet Link Controller
	eth_ctrl : eth_mdio port map(
		CLK => CLK125,
		RESET => RESET,
		E_RST_L => PHY_RESET_L,
		E_MDC => PHY_MDC,
		E_MDIO => PHY_MDIO,
		E_LINK_SPEED => speed);

	-- Transmitter
	trans : gmii_eth_tx_stream port map(
		CLK125 => CLK125,
		RESET => RESET,
		TXD => GE_TXD,
		TXCTRL => GE_TXEN,
		ETH_TX_STREAM=> mid_tx_stream2);

	-- Receiver
	receive : gmii_eth_rcv_stream port map(
		CLK125 => CLK125,
		RESET => RESET,
		RXCLK => GE_RXCLK,
		RXD => GE_RXD,
		RXCTRL => GE_RXDV,
		SPEED => speed,
		ETH_RX_STREAM => mid_rx_stream);

	receive_crc : eth_rx_crc port map(
		CLK => CLK125,
		RESET => RESET,
		IN_ETH_STREAM => mid_rx_stream,
		OUT_ETH_STREAM => ETH_RX_STREAM,
		OUT_OK_STB => GOODBAD_FRAME(1),
		OUT_BAD_STB => GOODBAD_FRAME(0));
		
	transmit_crc : eth_tx_crc port map(
		CLK => CLK125,
		RESET => RESET,
		IN_ETH_STREAM => mid_tx_stream,
		OUT_ETH_STREAM => mid_tx_stream2);

	pad_packet : eth_tx_pad port map(
		CLK => CLK125,
		RESET => RESET,
		IN_ETH_STREAM(9) => send_cke,
		IN_ETH_STREAM(8 downto 0) => ETH_TX_STREAM,
		OUT_ETH_STREAM => mid_tx_stream);

	TX_EN <= send_cke;
	LINK_SPEED <= '0' when speed = "01" else '1';

	process(CLK125)
	begin--Need to generate a 12.5MBps strobe from FE_TXCLK
		if CLK125'event and CLK125 = '1' then
			fe_txclk_inff <= FE_TXCLK;
			fe_txclk_dly <= fe_txclk_inff & fe_txclk_dly(1);
			if fe_txclk_dly(1) = '1' and fe_txclk_dly(0) = '0' then
				fe_out_tick <= e_tgl;
				e_tgl <= not e_tgl;
			else
				fe_out_tick <= '0';
				e_tgl <= e_tgl;
			end if;
			if speed = "01" then
				send_cke <= fe_out_tick;
			else
				send_cke <= '1';
			end if;
		end if;
	end process;
end Behavioral;

