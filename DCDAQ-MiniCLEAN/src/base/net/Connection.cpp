#include <Connection.h>

const int Connection::badFD = -1; 

Connection::Connection()
{
  thread = NULL;
  Clear();
}

void Connection::Clear() 
{ 
  port = 0;
  remoteAddr.Clear();
  fdSocket = badFD;
  if(thread)
    {
      delete thread;
      thread = NULL;
    }
  retryCountDown=2;
  retry = false;
}

Connection::~Connection()
{
  //Shut down our netThread and close the socket
  Shutdown();
  //reset all values
  Clear();
}

void Connection::Shutdown()
{ 
  if(thread != NULL)
    { 
      if(thread->IsRunning())
	{
	  thread->AddShutdownPacket();
	}
      //Send in the stop packet
      DCMessage::Message message;
      //Get the END_OF_THREAD message from thread
      //or a blank message if everything is shut down and there 
      //was no END_OF_THREAD message      
      do
	{
	  if(thread->IsRunning())
	    message = thread->GetMessageOut();
	  else
	    break;
	}while((message.GetType() != DCMessage::END_OF_THREAD) &&
	       (message.GetType() != DCMessage::BLANK));
      //Wait for thread to finish (should be instant because of above)
      pthread_join(thread->GetID(),NULL);

      //If the Thread still holds the fd
      if( (thread->GetSocketFD() != badFD) &&
	  (thread->GetSocketFD() == fdSocket))
	{
	  CloseSockFD(thread->GetSocketFD());
	}

      //delete the thread      
      delete thread;
      thread = NULL;
    }
  else
    {
      //If the thread is null, we have to clean up the FD
      if(fdSocket != badFD)
	{
	  CloseSockFD(fdSocket);
	}
    }

  //Clear connection values
  //Leaves IP/ports values intacted. 
  fdSocket = badFD;
  thread = NULL;
  retryCountDown=2;
  retry = false;
}

void Connection::SetRetry(int countdown)
{
  retryCountDown = countdown;
  retry = true;
}
bool Connection::Retry()
{
  if(retry)
    {
      //If our retry countdown has gotten to zero return true
      if(retryCountDown <= 0)
	{
	  retry = false;
	  return true;
	}
      //If we are above zero decrimate countdown and return false;
      else
	retryCountDown--;
    }    
  return false;
}


bool Connection::SetupRemoteAddr(xmlNode * SetupNode)
{
  //================================
  //Parse data for Client config and expected clients.
  //================================
  

  //================================
  //Get Client port.
  //================================
  if(FindSubNode(SetupNode,"PORT") == NULL)
    {
      PrintError("No port specified?\n");
      return false;
    }
  else
    {
      GetXMLValue(SetupNode,"PORT","Connection",port);
    }

  //================================
  //Get server address.
  //================================
  if(FindSubNode(SetupNode,"ADDR") == NULL)
    {
      PrintError("No server address!\n");
      return false;
    }
  else
    {      
      remoteAddr.Set(SetupNode);
      remoteAddr.SetPort(port);
    }
  return true;
}


//Start a asynchronous socket connect
//true if connection works instantly. 
//false on error or connection in progress.
//if false and fd == -1, then you have a problem. 
//if false and fd >= 0, then you are waiting for the connection to finish
bool Connection::AsyncConnectStart(int type, int protocol)
{
  //Open a socket
  fdSocket = socket(remoteAddr.GetAddr()->sa_family,type,protocol);  
  if(fdSocket < 0)
    {
      printf("Connection::Error in fdSocket %s : %d : %d. %s\n",remoteAddr.GetStr().c_str(),type,protocol,strerror(errno));
      return false;
    }

  //Set socket to non-blocking if it is not AF_LOCAL
  //  if((remoteAddr.GetAddr()->sa_family != AF_LOCAL) && (!SetNonBlocking(fdSocket,true)))
  if(!SetNonBlocking(fdSocket,true))
    {
      printf("Connection::Error in SetNonBlocking\n");
      Shutdown();
      return false;      
    }

  //fd is now non-blocking
  int connectReturn = connect(fdSocket,
			      (sockaddr *) remoteAddr.GetAddr(),
			      remoteAddr.GetAddrSize());
  //The connection happened in the blink of a transistor's eye.
  if(connectReturn == 0)
    {
      //Turn off non-blocking
      //(fail if it didn't work)
      if(!SetNonBlocking(fdSocket,false))
	{
	  Shutdown();
	  return false;      
	}

      //return that everything worked as well as possible
      return true;
    }
  else if(errno == EINPROGRESS)
    {
      //We are waiting for the connection to finish
      //You should go wait in select for read/write access to fd!
      return false;
    }
  printf("Error in AsyncConnectStart: %s\n",strerror(errno));  
  //An error occured
  Shutdown();
  return false;
}

//Check that our newly connected socket works. 
bool Connection::AsyncConnectCheck()
{
  //Check the socket option (SO_ERROR)
  int val_SO_ERROR;
  socklen_t val_SO_ERROR_Size = sizeof(val_SO_ERROR);
  int getSockOptReturn = getsockopt(fdSocket,SOL_SOCKET,SO_ERROR,
				    &val_SO_ERROR,&val_SO_ERROR_Size);
  //make sure getsockop didn't fail
  if(getSockOptReturn != 0)
    {
      Shutdown();
      return false;
    }

  //Check if connect actually connected
  //Reset blocking mode if we got a connections
  if(val_SO_ERROR == 0)
    {
      //Turn off non-blocking mode
      //(fail if it didn't work)
      if(!SetNonBlocking(fdSocket,false))
	{
	  Shutdown();
	  return false;      
	}
      //Yay!
      return true;
    }
  //There was an error, so close the socket are 
  //start another search
  Shutdown();
  return false;
}


bool Connection::SetupDCNetThread(int & _fd,DCMessage::DCAddress const & addr,
				  uint32_t _port,xmlNode * managerNode)
{
  fdSocket = _fd;
  _fd = badFD;
  remoteAddr = addr;
  port = _port;
  remoteAddr.SetPort(port);
  return SetupDCNetThread(managerNode);
}

bool Connection::SetupDCNetThread(xmlNode * managerNode)
{
  //create a DCNetThread for this 
  thread = new DCNetThread();
  if(thread == NULL)
    {      
      Shutdown();
      return false;
    }
  else
    {    
      //Turn off non-blocking mode
      //(fail if it didn't work)
      if(!SetNonBlocking(fdSocket,false))
	{
	  Shutdown();
	  return false;      
	} 
      //Setup this thread
      if(remoteAddr.GetAddr()->sa_family == AF_LOCAL)
	{
	  if(!(thread->SetupConnection(fdSocket,remoteAddr,remoteAddr)))
	    {
	      Shutdown();
	      return false;
	    }
	}
      else
	{
	  if(!(thread->SetupConnection(fdSocket)))
	    {
	      Shutdown();
	      return false;
	    }
	}
      //fdSocket is no longer valid!

      //Set up the memory manager for this DCNetThread
      if(!(thread->SetupPacketManager(managerNode)))
	{
	  Shutdown();
	  return false;
	}     
    }

  //Start the thread
  thread->Start(NULL);
  return true;
}

int Connection::GetFullPacketFD()
{
  int fd = badFD;
  if(thread != NULL)
    {
      fd = thread->GetInPacketManager()->GetFullFDs()[0];
    }
  return fd;
}

int Connection::GetMessageFD()
{
  int fd = badFD;
  if(thread != NULL)
    {
      fd = thread->GetMessageOutFDs()[0];
    }
  return fd;
}

DCNetPacketManager * Connection::GetInPacketManager()
{
  if(thread != NULL)
    return thread->GetInPacketManager();
  return NULL;
}
DCNetPacketManager * Connection::GetOutPacketManager()
{
  if(thread != NULL)
    return thread->GetOutPacketManager();
  return NULL;
}

DCMessage::Message Connection::GetMessageOut()
{
  if(thread != NULL)
    return thread->GetMessageOut();
  return DCMessage::Message();
}

bool Connection::RouteToLocal(DCMessage::DCAddress const & addr)
{
  if(thread)
    {
      if(addr.IsBroadcastAddr() || addr.IsEqualAddr(thread->GetLocalAddr()))
	{
	  return true; 
	}
    }
  return false;
}

bool Connection::RouteToRemote(DCMessage::DCAddress const & addr)
{
  if(thread)
    {
      if(addr.IsBroadcastAddr())
	return true;
      else if(addr.IsEqualAddr(thread->GetRemoteAddr()))
	return true; 
    }
  return false;
}

DCMessage::DCAddress Connection::GetRemoteAddress()
{
  if(thread != NULL)
    {
      return thread->GetRemoteAddr();
    }
  else
    {
      return remoteAddr;
    }
  return DCMessage::DCAddress();
}
DCMessage::DCAddress Connection::GetLocalAddress()
{
  if(thread != NULL)
    {
      return thread->GetLocalAddr();
    }
  return DCMessage::DCAddress();
}
