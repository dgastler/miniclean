#include "v1720.h"
#include "xmlHelper.h"

int v1720::GetXMLValue(xmlNode * baseNode,  char * Name,  char * ErrorBase,uint32_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindSubNode(baseNode,Name);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtoul(GetNodeText(xmlNodePointer),NULL,0);
  //  printf("%s:%s: 0x%08X\n",ErrorBase,Name,reg);
  return(ret);
}
int v1720::GetXMLValue(xmlNode * baseNode,  char * Name,  char * ErrorBase,int32_t & reg)
{
  int ret = 0;
  xmlNode * xmlNodePointer = NULL;
  //Look for NAME in baseNode
  xmlNodePointer = FindSubNode(baseNode,Name);
  if(xmlNodePointer == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML lookup failed\n",ErrorBase,Name);
      return(ret);
    }
  if(GetNodeText(xmlNodePointer) == NULL)
    {
      ret++;
      fprintf(stderr,"%s:%s XML bad text\n",ErrorBase,Name);
      return(ret);
    }
  reg = strtol(GetNodeText(xmlNodePointer),NULL,0);
  //  printf("%s:%s: 0x%08X\n",ErrorBase,Name,reg);
  return(ret);
}

