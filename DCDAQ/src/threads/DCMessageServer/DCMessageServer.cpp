#include <DCMessageServer.h>

DCMessageServer::DCMessageServer(std::string Name)
{
  SetType("DCMessageServer");
  SetName(Name);
 
  //Initialize server variables
  memset(&addrListen,0,sizeof(struct sockaddr_in));
  serverSocketFD = BadPipeFD;

  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>1024</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>1024</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  //  HACK=-1;
}

DCMessageServer::~DCMessageServer()
{
  //Close our server listening socket
  close(serverSocketFD);
  //Close all of our connections to remote DCDAQs
  while(RemoteDCDAQ.size()>0)
    {
      //Delete each connection.
      DeleteConnection(RemoteDCDAQ.size()-1);
    }

}

bool DCMessageServer::Setup(xmlNode * SetupNode,
			    std::map<std::string,MemoryManager*> &MemManager)
{  
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  //================================
  //Parse data for server config and expected clients.
  //================================

  //Address family
  if(FindSubNode(SetupNode,"AF") == NULL)
    {
      SocketFamily = AF_INET;
      PrintWarning("Using default AF_INET family.\n");
    }
  else
    {
      GetXMLValue(SetupNode,"AF",GetType().c_str(),SocketFamily);
    }

  //Get server port.
  if(FindSubNode(SetupNode,"PORT") == NULL)
    {
      ServerPort = 1717;
      PrintWarning("Using default listen port 1717.\n");
    }
  else
    {
      uint32_t tempServerPort;
      GetXMLValue(SetupNode,"PORT",GetType().c_str(),tempServerPort);
      ServerPort = tempServerPort;
    }

  //Setup the listening port and IP mask(0.0.0.0) 
  //This is the address that is listened to to get a connection
  addrListen.sin_family = SocketFamily;
  addrListen.sin_port = htons(ServerPort);
  inet_pton(SocketFamily,"0.0.0.0",&addrListen.sin_addr);

  //Create server socket
  serverSocketFD = socket(SocketFamily,SOCK_STREAM,0); 
  if(serverSocketFD < 0)
    {
      return(false);
    }
  //bind our socket to listen address and listen for connections
  bind(serverSocketFD,(struct sockaddr*) & addrListen,sizeof(addrListen)); 
  listen(serverSocketFD,14); //(#14 for linux)  P98 Unix Network Programming  

  //Add the new connection fd to select
  AddReadFD(serverSocketFD);
  SetupSelect();
  Ready=true;
  return(true);
}

void DCMessageServer::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(serverSocketFD))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < RemoteDCDAQ.size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(RemoteDCDAQ[iConnection].InPacketManagerFDs[0]))
	{
	  RouteRemoteDCMessage(RemoteDCDAQ[iConnection].Thread);
	}
      //Check for a new message
      if(IsReadReady(RemoteDCDAQ[iConnection].Thread->GetMessageOutFDs()[0]))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

bool DCMessageServer::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Determine if this is local or not
  std::string DestName;
  std::vector<uint8_t> addr;
  int32_t AF;
  message.GetDestination(DestName,addr,AF);
  if(AF != -1)
    {
      RouteLocalDCMessage(message);
    }
  else if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}



bool DCMessageServer::ProcessNewConnection()
{
  //================================
  //We should have a waiting connection, so we need to accept it
  //================================
  int fdRemoteSocket; //fd for incoming connection
  std::vector<uint8_t> addrRemote; //address info for incoming connection
  socklen_t addrRemoteLength = 0;
  std::string remoteIP;
  //Set our vector to the correct size for the type of socket we expect
  switch (SocketFamily)
    {
    case AF_INET:
      addrRemote.resize(sizeof(sockaddr_in));
      remoteIP.resize(INET_ADDRSTRLEN);
      break;
    case AF_INET6:
      addrRemote.resize(sizeof(sockaddr_in6));
      remoteIP.resize(INET6_ADDRSTRLEN);
      break;
    case AF_LOCAL:
      addrRemote.resize(sizeof(sockaddr_un));            
      remoteIP.resize(sizeof(sockaddr_un));
      break;
    }
  //Update the length variable with the actual size.
  addrRemoteLength = addrRemote.size();
  
  fdRemoteSocket = accept(serverSocketFD,
			  (struct sockaddr*) &(addrRemote[0]),
			  &addrRemoteLength);
  if(fdRemoteSocket == -1)
    {	  
      return(false);
    }
  
  //Check if this is in our netCclearList
  if(netClearList.size() > 0)
    {
      std::vector<std::vector<uint8_t> >::iterator it;
      for(it  = netClearList.begin();
	  it != netClearList.end();
	  it++)
	{
	  if(AddrCompare(SocketFamily,addrRemote,*it))
	    break;
	}
      //if we made it to the iterator's end then we 
      //should block this connection.
      if(it == netClearList.end())
	{
	  close(fdRemoteSocket);
	}
      return(false);
    }
  //Fillin our connection settings.
  Connection connection;
  connection.fdSocket = fdRemoteSocket;
  connection.addrRemote = addrRemote;
  connection.SocketFamily = SocketFamily;
  connection.Port = ServerPort;
  switch (SocketFamily)
    {
    case AF_INET:
      inet_ntop(connection.SocketFamily,
		&((sockaddr_in*)&connection.addrRemote[0])->sin_addr,
		&remoteIP[0],
		remoteIP.size());
      break;
    case AF_INET6:
      inet_ntop(connection.SocketFamily,
		&((sockaddr_in6*)&connection.addrRemote[0])->sin6_addr,
		&remoteIP[0],
		remoteIP.size());
      break;
    case AF_LOCAL:
      inet_ntop(connection.SocketFamily,
		&((sockaddr_un*)&connection.addrRemote[0])->sun_path,
		&remoteIP[0],
		remoteIP.size());
      break;
    }
  connection.RemoteIP = remoteIP;
  
//  //Get the string version of the remote IP and 
//  char * buffer = new char[INET6_ADDRSTRLEN];
//  connection.RemoteIP.assign(inet_ntop(SocketFamily,addrRemote,buffer,INET6_ADDRSTRLEN));
//  delete [] buffer;
  
  
  //create a DCNetThread for this 
  connection.Thread = new DCNetThread();
  if(connection.Thread == NULL)
    {
      close(fdRemoteSocket);
      return(false);
    }
  else
    {
      //Setup this thread
      if(!connection.Thread->SetupConnection(connection.fdSocket,
					     connection.SocketFamily,
					     connection.addrLocal,
					     connection.addrRemote))
	{
	  //Setup failed
	  PrintError("Error setting up DCNetThread Connection.");
	  close(connection.fdSocket);
	  delete connection.Thread;
	  connection.fdSocket = BadPipeFD;
	  return(false);
	}
      //Set up the memory manager for this DCNetThread
      if(!connection.Thread->SetupPacketManager(managerNode))
	{
	  //Allocation failed
	  PrintError("Error setting up DCNetThread packet manager.");
	  close(fdRemoteSocket);
	  delete connection.Thread;
	  connection.fdSocket = BadPipeFD;
	  return(false);
	}

      //Get the FDs for the free queue of the outgoing packet manager
      connection.Thread->GetInPacketManager()->
	GetFullFDs(connection.InPacketManagerFDs);      
      //Add the incoming packet manager fds to list
      AddReadFD(connection.InPacketManagerFDs[0]);

      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(connection.Thread->GetMessageOutFDs()[0]);      
      connection.Thread->Start(NULL);
      printf("DCMessage connection(%d) from: %s\n",
	     int(RemoteDCDAQ.size()) + 1,
	     connection.RemoteIP.c_str());
      RemoteDCDAQ.push_back(connection);      
      SetupSelect();
    }
  return(true);
}
void DCMessageServer::RouteRemoteDCMessage(DCNetThread * Thread)
{  
  std::vector<uint8_t> messageData;
  int packetCount = 1;
  //Loop over all the packets that make up this DCMessage
  for(int iPacket = 0; iPacket < packetCount; iPacket++)
    {
      int32_t iDCPacket = FreeFullQueue::BADVALUE;    
      //Try to get a full in packet index
      Thread->GetInPacketManager()->GetFull(iDCPacket,true);
      //Get the associated packet
      DCNetPacket * packet = Thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Fail if the packet is bad
      if(packet == NULL)
	{
	  PrintWarning("Bad net packet.\n");
	  return;
	}
     
      //Now we have a valid packet
      
      //Check it's type (if it is a partial packet increase packetCount;
      if(packet->GetType() == DCNetPacket::DCMESSAGE_PARTIAL)
	{
	  packetCount++;
	}
      else if(packet->GetType() != DCNetPacket::DCMESSAGE)
	{	  
	  PrintWarning("Bad incoming packet\n");
	  //return packet as free and return
	  Thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      
      //Now get the data from the packet and append it to the message data vector
      unsigned int existingMessageDataSize = messageData.size();
      //Resize our vector to hold the new data
      messageData.resize(existingMessageDataSize + packet->GetSize());
      //Check that the packet has a valid data pointer
      if(packet->GetData() == NULL)
	{
	  PrintWarning("Bad packet data\n");
	  //return packet as free and return
	  Thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      //Copy in the new data
      memcpy(&messageData[existingMessageDataSize],packet->GetData(),packet->GetSize());
      //return our packet
      Thread->GetOutPacketManager()->AddFree(iDCPacket);
    }
  //create a new DCPacket using the data from the packets
  DCMessage::Message  message;
  message.SetMessage(&messageData[0],messageData.size());
  //The Client will forward broadcast messages to this session  
  SendMessageOut(message,true);
}
bool DCMessageServer::RouteLocalDCMessage(DCMessage::Message &message)
{  
  bool ret = true;
  std::string DestName;
  std::vector<uint8_t> addr;
  int32_t AF;
  message.GetDestination(DestName,addr,AF);
  //Send to all. 
  if(AF == -2)
    {
      for(unsigned int i = 0; i < RemoteDCDAQ.size();i++)
	{
	  ret &= SendMessage(&(RemoteDCDAQ[i]),message);
	}
    }
  else
    {
      for(unsigned int i = 0; i < RemoteDCDAQ.size();i++)
	{
	  if(AddrCompare(AF,addr,RemoteDCDAQ[i].addrRemote))
	    {
	      ret &= SendMessage(&(RemoteDCDAQ[i]),message);
	    }
	}
    }
  return(ret);
}

bool DCMessageServer::SendMessage(Connection * conn,DCMessage::Message &message)
{
  //Get the data for this packet
  std::vector<uint8_t> stream;
  //get an vector of data for this message to send.
  int streamSize = message.StreamMessage(stream);
  if(streamSize < 0)
    return(false);
  uint8_t * dataPtr  = &(stream[0]);	  
  int packetNumber = 0;
  //Send loop over the data until all of it is sent
  while(streamSize >0)
    {
      //Get a free packet index
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      conn->Thread->GetOutPacketManager()->GetFree(iDCPacket,true);
      //Get the DCNetPacket for that index
      DCNetPacket * packet = conn->Thread->GetOutPacketManager()->GetPacket(iDCPacket);
      //Fail if our packet is bad
      if(packet == NULL)
	{
	  return(false);
	}
      //Fill packet
      packet->SetType(DCNetPacket::DCMESSAGE);
      if(streamSize > packet->GetMaxDataSize())
	{
	  packet->SetData(dataPtr,packet->GetMaxDataSize(),packetNumber);
	  dataPtr+=packet->GetMaxDataSize();
	  streamSize-=packet->GetMaxDataSize();
	  packetNumber++;
	}
      else
	{
	  packet->SetData(dataPtr,streamSize,packetNumber);
	  dataPtr+=streamSize;
	  streamSize-=streamSize;
	  packetNumber++;
	}
      //Send off packet
      conn->Thread->GetOutPacketManager()->AddFull(iDCPacket);
      packet = NULL;
    }
  return(true);
}

void DCMessageServer::ProcessChildMessage(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= RemoteDCDAQ.size())
    return;
  DCMessage::Message message = RemoteDCDAQ[iConn].Thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::BADCONNECTION)
    {
      DeleteConnection(iConn);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if((message.GetType() == DCMessage::PACKETSOUT) || 
	  (message.GetType() == DCMessage::PACKETSIN) || 
	  (message.GetType() == DCMessage::DATAOUT) || 
	  (message.GetType() == DCMessage::DATAIN))
    {
      //      SendMessageOut(message,true);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,"Unknown message (%d) from DCMessageServer's DCNetThread\n",message.GetType());
      PrintWarning(buffer);
      delete [] buffer;
    }
}

void DCMessageServer::DeleteConnection(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= RemoteDCDAQ.size())
    return;

  char buffer[100];
  sprintf(buffer,"Deleting connection from: %s\n",RemoteDCDAQ[iConn].RemoteIP.c_str());
  PrintError(buffer);

  //Send stop message.
  DCMessage::Message message;
  message.SetType(DCMessage::STOP);
  //Making sure that the thread actually exists
  if(RemoteDCDAQ[iConn].Thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(RemoteDCDAQ[iConn].InPacketManagerFDs[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(RemoteDCDAQ[iConn].Thread->GetMessageOutFDs()[0]);    
      //Send the stop message
      RemoteDCDAQ[iConn].Thread->SendMessageIn(message,true);
      //Wait for thread to finish
      pthread_join(RemoteDCDAQ[iConn].Thread->GetID(),NULL);
      //delete the thread      
      delete RemoteDCDAQ[iConn].Thread;
    }
  RemoteDCDAQ.erase(RemoteDCDAQ.begin() + iConn);
  SetupSelect();
}

void DCMessageServer::ProcessTimeout()
{
  //  time_t now = time(NULL);
  //  if(RemoteDCDAQ.size() >0)
  //    {
//      if(HACK == -1)
//	{
//	  std::string setupCommand("<SETUP><MEMORYMANAGERS><MANAGER><TYPE>MemoryBlockManager</TYPE><NAME>WAVEFORMS</NAME><BLOCKSIZE>0x800400</BLOCKSIZE><BLOCKCOUNT>20</BLOCKCOUNT></MANAGER><MANAGER><TYPE>BasicRATDSManager</TYPE><NAME>RAT</NAME><NUMDS>50</NUMDS><NUMBLOCKS>20</NUMBLOCKS></MANAGER></MEMORYMANAGERS><THREADS><THREAD><TYPE>V1720FakeReader</TYPE><NAME>FakeReader1</NAME><MEMORYMANAGER>WAVEFORMS</MEMORYMANAGER><EVENTSPERBLOCK>128</EVENTSPERBLOCK><FILENAME>file1.dat</FILENAME></THREAD><THREAD><TYPE>RATParse</TYPE><NAME>Parser_1</NAME><MEMORYMANAGER>WAVEFORMS</MEMORYMANAGER><MEMORYMANAGER>RAT</MEMORYMANAGER></THREAD><THREAD><TYPE>RATThrowAway</TYPE><NAME>Throwaway</NAME><MEMORYMANAGER>RAT</MEMORYMANAGER></THREAD><THREAD><TYPE>Control</TYPE><NAME>Control</NAME></THREAD></THREADS></SETUP>");
//	  DCMessage::Message message;
//	  message.SetType(DCMessage::LAUNCH);
//	  message.SetData(setupCommand.c_str(),setupCommand.size());
//	  message.SetDestination("",NULL,-2);
//	  RouteLocalDCMessage(message);
//	  printf("\nSending setup commands\n");
//	  HACK=now;
//	  HACKos = 0;
//	}
//      else if((difftime(now,HACK) >= 3)&&
//	      (difftime(now,HACK) <10))
//	{
//	  if(HACKos < 1)
//	    {
//	      DCMessage::Message message;
//	      message.SetType(DCMessage::GO);
//	      message.SetDestination("",NULL,-2);
//	      RouteLocalDCMessage(message);
//	      printf("\nRemote start\n");	     
//	      HACKos++;
//	    }
//	}
//      else if((difftime(now,HACK) >= 10)&&
//	      (difftime(now,HACK) < 20))
//	{
//	  if(HACKos < 2)
//	    {
//	      DCMessage::Message message;
//	      message.SetType(DCMessage::PAUSE);
//	      message.SetDestination("",NULL,-2);
//	      RouteLocalDCMessage(message);
//	      printf("\nRemote pause\n");
//	      HACKos++;
//	    }
//	}
//      else if((difftime(now,HACK) >= 20)&&
//	      (difftime(now,HACK) < 32))
//	{
//	  if(HACKos < 3)
//	    {
//	      DCMessage::Message message;
//	      message.SetType(DCMessage::GO);
//	      message.SetDestination("",NULL,-2);
//	      RouteLocalDCMessage(message);
//	      printf("\nRemote restart\n");
//	      HACKos++;
//	    }
//	}
//      else if(difftime(now,HACK) >= 32)
//	{
//	  if(HACKos < 4)
//	    {
//	      DCMessage::Message message;
//	      message.SetType(DCMessage::STOP);
//	      message.SetDestination("",NULL,-2);
//	      RouteLocalDCMessage(message);
//	      HACK=0;
//	      printf("\nRemote stop!\n");
//	      HACKos++;
//	    }
//	}
//      
//      //  DCMessage::Message message;
//      //  message.SetType(DCMessage::STOP);
//      //  message.SetDestination("",NULL,-2);
//      //  RouteLocalDCMessage(message);
//            HACK++;
//    }
}
