#ifndef ROOT_PEvent
#define ROOT_PEvent
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Event                                                                //
//                                                                      //
// Description of the event and track parameters                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>
#include <vector>

class PEvent : public TObject {
  
 private:
  
  Bool_t         fIsValid;  

  Bool_t         AbvTHRS;

  Int_t          iEventNumber;

  std::vector<Float_t> vFast0;
  std::vector<Float_t> vFast1;
  std::vector<Float_t> vFast2;
  std::vector<Float_t> vFast3;
  std::vector<Float_t> vFast4;
  std::vector<Float_t> vSlow0;
  std::vector<Float_t> vSlow1;
  std::vector<Float_t> vSlow2;
  std::vector<Float_t> vSlow3;
  std::vector<Float_t> vSlow4;
  std::vector<Float_t> vSlow; 
  std::vector<Float_t> vTOF; 
  std::vector<Float_t> vShift;
  std::vector<Float_t> vDelay;
  std::vector<Float_t> vMaxHeight;



 public:
  PEvent();
  virtual        ~PEvent();

  void           SetSize(UInt_t i);
  Float_t        GetSize();

  void           Fast(UInt_t i,UInt_t n,Float_t value);
  Float_t        Fast(UInt_t i,UInt_t n);
  void           Slow (UInt_t i,UInt_t n,Float_t value);
  Float_t        Slow (UInt_t i,UInt_t n);

  void           TOF   (UInt_t i,Float_t value){if(i<vTOF.size()){vTOF[i] = value;}}
  Float_t        TOF   (UInt_t i) {if(i < vTOF.size()){return(vTOF[i]);}return(0);}
  void           Shift (UInt_t i,Float_t value){if(i<vShift.size()){vShift[i] = value;}}
  Float_t        Shift (UInt_t i) {if(i < vShift.size()){return(vShift[i]);}return(0);}
  void           Delay (UInt_t i,Float_t value){if(i<vDelay.size()){vDelay[i] = value;}}
  Float_t        Delay (UInt_t i) {if(i < vDelay.size()){return(vDelay[i]);}return(0);}
  void           MaxHeight(UInt_t i,Float_t value){if(i<vMaxHeight.size()){vMaxHeight[i] = value;}}
  Float_t        MaxHeight(UInt_t i) {if(i < vMaxHeight.size()){return(vMaxHeight[i]);}return(0);}
  void           EventNumber(Int_t value){iEventNumber = value;}
  Int_t          EventNumber() {return(iEventNumber);}

  void           AboveTHRS(Bool_t value){AbvTHRS = value;};
  Bool_t         AboveTHRS() {return(AbvTHRS);}

  
  void           Clear(Option_t *option);
  Bool_t         IsValid() const { return fIsValid; }

  ClassDef(PEvent,1)  //PEvent structure
};


#endif
