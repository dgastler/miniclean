#include <DCMessageProcessor.h>

bool DCMessageProcessor::RouteLocalMessage(DCMessage::Message message)
{
  bool runReturn = true;

  //Find the routed to thread
  DCThread * thread = FindThread(message.GetDestination().GetName());
  if(thread != NULL)
    {
      //pass on the message
      thread->SendMessageIn(message);
    }
  //A local message explicity routed to DCDAQ for DCMessageProcessor function calling
  else if(boost::iequals(message.GetDestination().GetName(),"DCDAQ"))
    {
      void (DCMessageProcessor::* func )(DCMessage::Message &, bool &) ;
      if(TypeFunction(message,func))
	{      
	  (*this.*(func))(message,runReturn);
	}
    }
  else
    //Warning: thread name not found
    {	   
      std::stringstream ss;
      ss << "Can not find routed to thread " 
	 << message.GetDestination().GetName();
      DCMessage::Message message;
      message.SetSource("DCDAQ");
      message.SetType(DCMessage::WARNING);
      DCMessage::DCString dcString;
      std::vector<uint8_t> data = dcString.SetData(ss.str());
      message.SetData(&(data[0]),data.size());
      ProcessMessage(message);
    }
  return runReturn;
}

//void DCMessageProcessor::DumpLocalRoutingTable()
//{
//}
