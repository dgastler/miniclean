#include <DCAddress.h>
#include <StackTracer.h>

int16_t DCMessage::DCAddress::GetPort()
{
  struct sockaddr * addr = (struct sockaddr*) addrMem;
  struct sockaddr_in *  addr_in;  
  struct sockaddr_in6 * addr_in6; 
  switch (addr->sa_family)
    {
    case AF_INET:
      addr_in  = (sockaddr_in * ) addrMem;
      return ntohs(addr_in->sin_port);
    case AF_INET6:
      addr_in6 = (sockaddr_in6 *) addrMem;
      return ntohs(addr_in6->sin6_port);
      break;
    case AF_MAX:
    case AF_UNSPEC:
    case AF_UNIX:
    default:      
      break;
    }  
  return -1;
}

void DCMessage::DCAddress::SetPort(uint16_t _port)
{
  struct sockaddr * addr = (struct sockaddr*) addrMem;
  struct sockaddr_in *  addr_in;  
  struct sockaddr_in6 * addr_in6; 
  switch (addr->sa_family)
    {
    case AF_INET:
      addr_in  = (sockaddr_in * ) addrMem;
      addr_in->sin_port = htons(_port);
      break;
    case AF_INET6:
      addr_in6 = (sockaddr_in6 *) addrMem;
      addr_in6->sin6_port = htons(_port);
      break;
    case AF_MAX:
    case AF_UNSPEC:
    case AF_UNIX:
    default:      
      break;
    }  
}
void DCMessage::DCAddress::SetName(std::string const &_name)
{
  name = _name;
}

void DCMessage::DCAddress::SetAddr(struct sockaddr const * _addr,
				   size_t sockaddrSize)
{
  if((_addr != NULL) && (sockaddrSize > 0))
    memcpy(this->addrMem,_addr,sockaddrSize);
  else
    {
      if((_addr !=NULL) && (sockaddrSize == 0))
	{
	  if(_addr->sa_family != AF_UNSPEC)
	    {
	      printf("You are passing a !NULL pointer to a struct sockaddr, but saying it has size zero.  Why?\n");
	      print_trace();
	    }
	}
      ClearAddr();
    }
}

void DCMessage::DCAddress::SetAddr(std::string const & _addr)
{
  if(boost::algorithm::iequals(_addr,"BROADCAST"))
    {
      ((struct sockaddr *) addrMem)->sa_family = AF_MAX;
      return;
    }
  //check to see if this is a unix socket  
  else if(_addr.find("/") != std::string::npos) 
    // all unix sockets must include a 
    //"/", either in path or "./" for in name 
    {      
      struct sockaddr_un * addr = (struct sockaddr_un *) addrMem;
      addr->sun_family = AF_UNIX;
      //Find min path (-1 for NULL)
      size_t minSize = std::min(sizeof(addr->sun_path)-1,_addr.size());
      memcpy(addr->sun_path,_addr.c_str(),minSize);
      addr->sun_path[minSize] = '\0';
      return;
    }
  else 
    //Try IPV*
    {
      struct sockaddr_in * addr_in = (struct sockaddr_in *) addrMem;
      struct sockaddr_in6 * addr_in6 = (struct sockaddr_in6 *) addrMem;
      //Try IPV4
      if(inet_pton(AF_INET,_addr.c_str(),
		   &(addr_in->sin_addr)) == 1)
	//This is an IPV4 addr
       	{
	  addr_in->sin_family=AF_INET;
	  addr_in->sin_port = 0;
	  return;
	}
      //Try IPV6
      else if(inet_pton(AF_INET6,_addr.c_str(),
			&(addr_in6->sin6_addr)) == 1)
	//This is an IPV6 addr
	{
	  addr_in6->sin6_family=AF_INET6;
	  addr_in6->sin6_port = 0;	  
	  return ;
	}
      else
	//This must be local
	{
	  ((struct sockaddr *) addrMem)->sa_family = AF_UNSPEC;
	  return;
	}
    }
}
void DCMessage::DCAddress::Clear()
{
  ClearName();
  ClearAddr();
}

void DCMessage::DCAddress::ClearName()
{
  //  name.clear();
  name = BroadcastName;
}
void DCMessage::DCAddress::ClearAddr()
{
  memset(this->addrMem,0,sizeof(addrMem));
  //Make sure this is treated as local (AF_UNSPEC should be 0)
  ((struct sockaddr *) this->addrMem)->sa_family = AF_UNSPEC;
}

void DCMessage::DCAddress::CopyObj(DCAddress const &rhs)
{
  Clear();
  SetName(rhs.name);
  SetAddr(rhs.GetAddr(),rhs.GetAddrSize());
}
bool DCMessage::DCAddress::operator==(DCAddress const &rhs) const
{
  bool ret;
  //Compare thread names
  if(rhs.name.empty()&& name.empty())
    {
      ret = true;
    }
  else
    {
      ret = boost::algorithm::iequals(rhs.name,name);
    }

  ret = ret && IsEqualAddr(rhs);
  
  return ret;
}

bool DCMessage::DCAddress::Set(std::string const &_name,
			       struct sockaddr const * _addr,
			       size_t sockaddrSize)
{
  SetName(_name);
  SetAddr(_addr,sockaddrSize);
  return true; //kinda obvious this worked
}

bool DCMessage::DCAddress::Set(std::string const& _name,
			       std::string const& _addr)
{
  bool ret = true;
  SetName(_name);
  SetAddr(_addr);
  return ret;  
}

bool DCMessage::DCAddress::Set(xmlNode * addrNode)
{
  bool ret;
  std::string Name;
  std::string ADDR;
  if(FindSubNode(addrNode,"NAME") != NULL)
    {
      if(GetXMLValue(addrNode,"NAME","DCADDRESS",Name) != 0)
	{
	  Name=BroadcastName;
	}
    }
  else
    {
      Name=BroadcastName;
    }
  if(FindSubNode(addrNode,"ADDR") != NULL)
    {      
      if(GetXMLValue(addrNode,"ADDR","DCAddress",ADDR) == 0)
	{
	  ret = Set(Name,ADDR);
	}
      else
	{
	  ret = Set(Name);
	}
    }
  else
    ret = Set(Name);

  return ret;
}

std::string DCMessage::DCAddress::GetAddrStr() const
{
  std::string ret;
  struct sockaddr * addr = (struct sockaddr*) addrMem;
  struct sockaddr_un *  addr_un;  
  struct sockaddr_in *  addr_in;  
  struct sockaddr_in6 * addr_in6; 
  char buffer[INET6_ADDRSTRLEN];
  switch (addr->sa_family)
    {
    case AF_MAX:
      ret.assign("BROADCAST");
      break;
    case AF_UNSPEC:
      //      ret.assign("LOCAL");
      ret.assign("");
      break;
    case AF_UNIX:
      addr_un  = (sockaddr_un * ) addrMem;
      ret.assign(addr_un->sun_path);
      break;
    case AF_INET:
      addr_in  = (sockaddr_in * ) addrMem;
      ret.assign(inet_ntop(AF_INET,&(addr_in->sin_addr),
			   buffer,INET_ADDRSTRLEN));
      break;
    case AF_INET6:
      addr_in6 = (sockaddr_in6 *) addrMem;
      ret.assign(inet_ntop(AF_INET6,&(addr_in6->sin6_addr),
			   buffer,INET6_ADDRSTRLEN));
      break;
    default:      
      //      ret.assign("LOCAL");
      ret.assign("");
      break;
    }
  return ret;
}

std::string DCMessage::DCAddress::GetStr() const
{
  std::string ret("");  
  ret += GetAddrStr();
  ret += " : ";
  ret += GetName();
  return ret;
}

bool DCMessage::DCAddress::IsBlank() const
{
  bool ret;
  if(BroadcastName.empty()&& name.empty())
    {
      ret = true;
    }
  else
    {
      ret = boost::algorithm::iequals(name,BroadcastName);
    }
  ret = ret && (((struct sockaddr *) addrMem)->sa_family == AF_UNSPEC);
  return ret;
}

bool DCMessage::DCAddress::IsBroadcastAddr() const
{
  return (((struct sockaddr *) addrMem)->sa_family == AF_MAX);
}
bool DCMessage::DCAddress::IsDefaultAddr() const
{
  return (((struct sockaddr *) addrMem)->sa_family == AF_UNSPEC);
}

bool DCMessage::DCAddress::IsEqualAddr(DCMessage::DCAddress const & addr) const
{
  bool ret = false;
  sockaddr const * addr_local = (sockaddr const * )addrMem;
  sockaddr const * addr_other = (sockaddr const * )addr.addrMem;
  if((addr_local->sa_family == AF_MAX)||
     (addr_other->sa_family == AF_MAX))
    {
      ret = true;
    }
  else if(addr_local->sa_family == addr_other->sa_family)
    {
      sockaddr_un * addr_un;
      sockaddr_in * addr_in;
      sockaddr_in6 * addr_in6;
      sockaddr_un *  other_un;
      sockaddr_in *  other_in;
      sockaddr_in6 * other_in6;
      switch( addr_local->sa_family )
	{
	case AF_UNSPEC:
	  //no remote routing (intra DCDAQ only)
	  ret = true;
	case AF_UNIX:
	  addr_un = (sockaddr_un*) addrMem;
	  other_un = (sockaddr_un*) addr.addrMem;
	  ret = (boost::algorithm::iequals(addr_un->sun_path,
					   other_un->sun_path));
	  break;
	case AF_INET:
	  addr_in = (sockaddr_in*) addrMem;
	  other_in = (sockaddr_in*) addr.addrMem;
	  ret = (addr_in->sin_addr.s_addr == other_in->sin_addr.s_addr);
	  break;
	case AF_INET6:
	  addr_in6 = (sockaddr_in6*) addrMem;
	  other_in6 = (sockaddr_in6*) addr.addrMem;
	  ret = !memcmp(addr_in6->sin6_addr.s6_addr,
			other_in6->sin6_addr.s6_addr,
			sizeof(struct in6_addr));
	  break;
	default:
	  ret = false;
	  break;
	}
    }
  else
    {
      ret = false;
    }
  return ret;
}

const std::string    DCMessage::DCAddress::BroadcastName("");

bool DCMessage::DCAddress::operator() (DCAddress const & lhs, DCAddress const & rhs) const
{
  bool ret;
  if(lhs.GetAddr()->sa_family < lhs.GetAddr()->sa_family)
    {
      ret = true;
    }
  else if(lhs.GetAddr()->sa_family > lhs.GetAddr()->sa_family)
    {
      ret = false;
    }
  else //equal families
    {
      sockaddr_un * lhs_un;
      sockaddr_in * lhs_in;
      sockaddr_in6 * lhs_in6;
      sockaddr_un *  rhs_un;
      sockaddr_in *  rhs_in;
      sockaddr_in6 * rhs_in6;
      switch( lhs.GetAddr()->sa_family )
	{
	case AF_UNSPEC:
	  ret = false;
	case AF_UNIX:
	  lhs_un = (sockaddr_un*) lhs.addrMem;
	  rhs_un = (sockaddr_un*) rhs.addrMem;
	  ret = strcmp(lhs_un->sun_path,rhs_un->sun_path);
	  break;
	case AF_INET:
	  lhs_in = (sockaddr_in*) lhs.addrMem;
	  rhs_in = (sockaddr_in*) rhs.addrMem;
	  ret = (lhs_in->sin_addr.s_addr < rhs_in->sin_addr.s_addr);
	  break;
	case AF_INET6:
	  lhs_in6 = (sockaddr_in6*) lhs.addrMem;
	  rhs_in6 = (sockaddr_in6*) rhs.addrMem;
	  ret = memcmp(lhs_in6->sin6_addr.s6_addr,
		       rhs_in6->sin6_addr.s6_addr,
		       sizeof(struct in6_addr));
	  break;
	default:
	  ret = false;
	  break;
	}
    }
  return ret;
}

size_t DCMessage::DCAddress::GetAddrSize() const
{
  switch(GetAddr()->sa_family )
    {
    case AF_UNIX:
      //      return SUN_LEN((struct sockaddr_un *) GetAddr());
      return sizeof(addrMem);
      break;
    case AF_INET:
      return sizeof(struct sockaddr_in);
      break;
    case AF_INET6:
      return sizeof(struct sockaddr_in6);
      break;
    case AF_UNSPEC:
    case AF_MAX:
    default:
      break;      
    }
  return sizeof(unsigned short);//Size of struct's sa_family
}
