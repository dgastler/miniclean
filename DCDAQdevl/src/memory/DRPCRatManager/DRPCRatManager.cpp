#include <DRPCRatManager.h>

DRPCRATManager::DRPCRATManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("DRPCRATManager"));
  //Make sure our memory vector is empty
  Memory.clear();
}

DRPCRATManager::~DRPCRATManager()
{
  Deallocate();
}

void DRPCRATManager::Deallocate()
{
  if(!Lock.LockObject())
    {
      fprintf(stderr,"Lock error on deallocation\n");
      abort();
    }
  //Clear our containers
  Free.Clear(true);
  UnProcessed.Clear(true);
  EventStore.Clear(true);
  Respond.Clear(true);
  BadEvent.Clear(true); 
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //      delete DRPC event (check if it's not NULL)
      if(Memory.back().ev)
	{
	  delete Memory.back().ev;
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
  Lock.UnlockObject();
}

int64_t DRPCRATManager::GetEvent(int64_t Event,
				 int32_t &index,
				 uint64_t &password)
{  
#ifdef LOCK_EVERYTHING
  if(!Lock.LockObject())
    {
      return(returnDCVectorHash::BAD_MUTEX);
    }
#endif

  //The object is now mutex locked

  //Get the index at Event
  int64_t ret = EventStore.GetEntry(Event,index,password);
  if(ret == returnDCVectorHash::EMPTY_KEY)
    //The index was empty
    {      
      //Get a new index from the free queue
      int32_t freeIndex;
      bool gotFree = false;      
      while(!(gotFree = Free.Get(freeIndex)))
	{
	}
      if(gotFree)
	{
	  index = freeIndex;
	  ret = EventStore.UpdateEntry(Event,freeIndex,password); 	  
	}
      else
	//Failed to get a free
	{	  
	  index = EventStore.GetBadValue();
	  //Must block to make sure everything is consistent
	  ret = EventStore.ClearEntry(Event,password);
	}
    }
#ifdef LOCK_EVERYTHING
  //Unlock the mutex for this object
  UnlockObject();
#endif
  return(ret);
}

int64_t DRPCRATManager::SetEvent(int64_t Event,
				 int32_t &index,
				 uint64_t password)
{
  int64_t ret = returnDCVectorHash::OK;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = EventStore.UpdateEntry(Event,index,password);
      if(ret == returnDCVectorHash::OK)
	{
	  ret = EventStore.ClearPassword(Event,password);
	}
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);  
}

int64_t DRPCRATManager::ClearEvent(int64_t key, uint64_t password)
{
  int64_t ret = returnDCVectorHash::OK;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = EventStore.ClearEntry(key,password);
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);  
}

bool DRPCRATManager::AddBadEventNOFD(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      //Read off one from the FD to not cause select to fire on this event.
      fprintf(stderr,"This doesn't do what you think!\n");
      ret = BadEvent.Add(index);
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);  
}
bool DRPCRATManager::AddBadEvent(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = BadEvent.Add(index);
#ifdef LOCK_EVERYTHING
      UnlockObject();      
    }
#endif
  return(ret);  
}

bool DRPCRATManager::AllocateMemory(xmlNode * setupNode)
{
  if(!Lock.LockObject())
    {
      fprintf(stderr,"Lock error on allocation\n");
      abort();
    }
  bool ret = true;
  
  int NumDRPCBlocksERR = GetXMLValue(setupNode,"NUMDRPCBLOCKS","MANAGER",NumDRPCBlocks);
  if(NumDRPCBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMDRPC in DRPCRATManager.\n");
      ret = false;
    }
  int VectorHashSizeERR = GetXMLValue(setupNode,"VECTORHASHSIZE","MANAGER",VectorHashSize);
  if(VectorHashSizeERR != 0)
    {
      fprintf(stderr,"Error reading VECTORHASHSIZE in DRPCRATManager.\n");
      ret = false;
    }

  //Allocate the needed RAT DRPCs
  if(ret)
    {
      for(uint32_t iDRPCBlock = 0; iDRPCBlock < NumDRPCBlocks;iDRPCBlock++)
	{
	  DRPCBlock evBlock;	  
	  Memory.push_back(evBlock);
	  Memory.back().ev = new RAT::DS::EV;	  
	  int32_t index = int32_t(Memory.size())-1;
	  Free.Add(index);
	  if(Memory.back().ev == NULL)
	    {
	      ret = false;
	      break;
	    }
	}     
    }
  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash (-1 emtpy value, -2 bad value)
      EventStore.Setup(VectorHashSize,-1,-2);
    }
  Lock.UnlockObject();
  return(ret);
}

int64_t DRPCRATManager::MoveEventToUnProcessedQueue(int64_t Event,
						    int32_t &index,
						    uint64_t password)
{
  int64_t ret = returnDCVectorHash::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      int32_t indexRespond = index;
      bool moveWorked = UnProcessed.Add(indexRespond,true);
      if(!moveWorked)
	//The unprocessed queue was locked and we aren't waiting
	{
	  ret = returnDCVectorHash::EXTERNAL;
	}
      else
	//We moved this event, so now we need to clear it. 
	{
	  //We will always wait on this!
	  ret = EventStore.ClearEntry(Event,
				      password,
				      true);        
	  if(ret == returnDCVectorHash::OK)
	    //The caller no longer should know this
	    {
	      index = EventStore.GetBadValue();
	    }
	}
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);
}
//int64_t DRPCRATManager::MoveEventToRespondQueue(int64_t Event,
//						int32_t &index,
//						uint64_t password)
//{
//  int64_t ret = returnDCVectorHash::BAD_MUTEX;
//  if(Lock.LockObject())
//    {
//      //We need to unlock this event.
//      ret = EventStore.ClearPassword(Event,password,true);
//      if(ret == returnDCVectorHash::OK)
//	{
//	  if(!Respond.Add(index,true))
//	    {
//	      //We failed to add
//	      ret = returnDCVectorHash::EXTERNAL;
//	    }      
//	}
//      UnlockObject();
//    }
//  return(ret);
//}

int64_t DRPCRATManager::MoveEventToBadQueue(int64_t Event,
					    int32_t &index,
					    uint64_t password)
{  
  int64_t ret = returnDCVectorHash::BAD_MUTEX;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      int32_t indexRespond = index;
      bool moveWorked = BadEvent.Add(indexRespond);
      if(!moveWorked)
	//The bad queue was locked and we aren't waiting
	{
	  ret = returnDCVectorHash::EXTERNAL;
	}
      else
	//We moved this event, so now we need to clear it. 
	{
	  //We will always wait on this!
	  ret = EventStore.ClearEntry(Event,
				      password,
				      true);        
	  if(ret == returnDCVectorHash::OK)
	    //The caller no longer should know this
	    {
	      index = EventStore.GetBadValue();
	    }
	}
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);
}

void DRPCRATManager::PrintStatus(int indent)
{

  printf("\n");
  if(indent > 0)
    {
      for(int i = 0; i < indent;i++)
	{
	  printf(" ");
	}
    }
# if __WORDSIZE == 64
  char printCommand[] = "DRPCRATManager\n     Size: %04lu\n     Free: %04lu\n     Active Stored: %04lu(%04lu)\n     UnProcessed: %04lu\n     Respond: %04lu\n     BadEvent: %04lu\n";
# else
  char printCommand[] = "DRPCRATManager\n     Size: %04u\n     Free: %04u\n     Active Stored: %04u(%04u)\n     UnProcessed: %04u\n     Respond: %04u\n     BadEvent: %04u\n";
# endif
  printf(printCommand,
	 Memory.size(),
	 Free.GetSize(),
	 EventStore.GetActiveEntryCount(),
	 EventStore.GetSize(),
	 UnProcessed.GetSize(),
	 Respond.GetSize(),
	 BadEvent.GetSize());
}


bool DRPCRATManager::AddFree(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = Free.Add(index);
#ifdef LOCK_EVERYTHING
      //signal so that if anyone is in cond_wait in Get() they wake up
      WakeUpSignal();
      UnlockObject();
    }
#endif
  return(ret);
}

bool DRPCRATManager::GetUnProcessed(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = UnProcessed.Get(index);
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);
}

bool DRPCRATManager::GetRespond(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = Respond.Get(index);
#ifdef LOCK_EVERYTHING      
      UnlockObject();
    }
#endif
  return(ret);
}
bool DRPCRATManager::AddRespond(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING
  if(Lock.LockObject())
    {
#endif
      ret = Respond.Add(index);
#ifdef LOCK_EVERYTHING      
      UnlockObject();
    }
#endif
  return(ret);
}
bool DRPCRATManager::GetBadEvent(int32_t &index)
{
  bool ret = false;
#ifdef LOCK_EVERYTHING  
  if(Lock.LockObject())
    {
#endif
      ret = BadEvent.Get(index);
#ifdef LOCK_EVERYTHING
      UnlockObject();
    }
#endif
  return(ret);
}
