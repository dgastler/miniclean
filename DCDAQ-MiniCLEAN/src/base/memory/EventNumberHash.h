#ifndef __EVENTNUMBERHASH__
#define __EVENTNUMBERHASH__

#include <EventBuilder.h>

template <class T>
class EventNumberHash : public EventBuilder<T>
{
public:
  EventNumberHash() {};
  inline bool EquivalentKeys(int64_t key1, int64_t key2) 
  {
    return (key1 == key2);
  };
protected:
  //Hash functions used to turn an integer into a hash integer.
  virtual size_t GetKeyIndex(int64_t key)
  {
    return size_t( key % ( this->GetEntry()->size() ) );
  };
  
  //Don't impliment these because they should never be called
  EventNumberHash(const DCVectorEntry<T> & rhs);
  EventNumberHash & operator=(const EventNumberHash & rhs);
};
#endif
