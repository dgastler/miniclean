#include <CompressData_XML.h>
CompressData_XML::CompressData_XML(DCMySQL *conn,int runNumber)
  : Thread_XML(conn,runNumber)
{
}


bool CompressData_XML::Generate_XML(xmlNode * threadsNode,
				    std::string pcName,
				    std::string threadType,
				    std::string threadName,
				    xmlNode * responseNode)
{
  bool ret = Thread_XML::Generate_XML(threadsNode,
				      pcName,
				      threadType,
				      threadName,
				      responseNode);
  ret |= GetCompressData(pcName,threadType,threadName);
  return ret;
}

bool CompressData_XML::GetCompressData(std::string pcName,std::string threadType,std::string threadName)
{
  bool ret = true;
  //  std::stringstream compressDataQuery;
  //  std::stringstream rawDataQuery;

  std::stringstream query;
  query << "SELECT raw FROM SThread_Data_Raw WHERE thread_type='" << threadType 
	<< "' AND thread_name='" << threadName 
	<< "' AND pc_name='" << pcName 
	<< "' AND run_number=" << runNumber;
  
  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  std::vector<std::vector<std::string> > rawData;
  DCMySQLConnection->GetTable(rawData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  query << "SELECT compressed FROM SThread_Data_Compressed WHERE thread_type='" << threadType 
	<<"' AND thread_name='" << threadName 
	<< "' AND pc_name='" << pcName 
	<< "' AND run_number=" << runNumber;

  if(!DCMySQLConnection->RunQuery(query.str()))
    {
      ret = false;
      return ret;
    }
  std::vector<std::vector<std::string> > compressedData;
  DCMySQLConnection->GetTable(compressedData);
  DCMySQLConnection->FreeResult();
  query.str(std::string());

  xmlNode * packDataNode;
  for(size_t iRow=0;iRow< rawData.size();iRow++)
    {
      xmlNode * rawNode=xmlNewChild(threadNode,NULL,BAD_CAST "RAW",NULL);
      xmlNewTextChild(rawNode,NULL,
		      BAD_CAST "MEMORYMANAGER",
		      BAD_CAST (rawData[0][iRow]).c_str());
      if((rawData[iRow][0].compare("EBPC_MM")==0))
	{
	  packDataNode=xmlNewChild(rawNode,NULL,BAD_CAST "DISK",NULL);
	}
      if((rawData[iRow][0].compare("FEPC_MM")==0))
	{
	  packDataNode=xmlNewChild(rawNode,NULL,BAD_CAST "SEND",NULL);
	}
      if((rawData[iRow][0].compare("DRPC_MM")==0))
	{
	  packDataNode=xmlNewChild(rawNode,NULL,BAD_CAST "RESPOND",NULL);
	}
    }

  xmlNode *compressNode;
  xmlNode *mmCompressNode;
  for(size_t i=0;i<compressedData.size();i++)
    {
      compressNode=xmlNewChild(threadNode,NULL,BAD_CAST "COMPRESSED",NULL);
      mmCompressNode=xmlNewTextChild(compressNode,NULL,
				     BAD_CAST "MEMORYMANAGER",
				     BAD_CAST (compressedData[i][0].c_str()));
    }

  return ret;
}
