#include <DCNetPacket.h>

void DCNetPacket::PrintHeader(FILE * fd)
{  
  fprintf(fd,"\n");
  //          123456789ab123456789ab123456789ab123456789ab123456789ab123456789ab
  fprintf(fd,"Ctrl-Wrd   Type       size[0]    size[1]    PacketID\n");
  for(uint i = 0; ((i < DCNETPACKET_HEADER_SIZE)&&(i < MaxPacketSize));i++)
    {
      fprintf(fd,"0x%02x       ",PacketData[i]);
    }
  fprintf(fd,"\n");
  //Flush data
  fflush(fd);
}


void DCNetPacket::PrintPacket(FILE *fd)
{  
  fprintf(fd,"\n");
  //          123456789ab123456789ab123456789ab123456789ab123456789ab123456789ab
  //fprintf(fd,"Printing packet\n");
  for(uint i = 0; (i < MaxPacketSize);i++)
    {
      fprintf(fd,"0x%02x \n",PacketData[i]);
    }
}

int DCNetPacket::SetData(uint8_t * data,uint32_t size,uint8_t packetNumber)
{    
  int ret = 0;
  *PacketNumber = packetNumber;
  if(size > MaxDataSize) 
    {
      ret = -1;
    }
  else if(!Data)
    {
      ret = -2;
    }
  else
    {
      //Check if we need to copy data. 
      //If the pointers are the same, we dont' neeed to copy anything. 
      if(data != Data)
	{
	  //Copy data into packet
	  memcpy(Data,data,size);
	}
      DataSize = size;
      //Set the header word
      *HeaderWord = DCNETPACKET_HEADER_WORD;
      //Determine the packet size and set it in the header.
      uint32_t packetSize = DataSize + DCNETPACKET_HEADER_SIZE;
      PacketSize[0] = uint8_t ((0xFF000000 & packetSize)>>24);
      PacketSize[1] = uint8_t ((0x00FF0000 & packetSize)>>16); 
      PacketSize[2] = uint8_t ((0x0000FF00 & packetSize)>>8);
      PacketSize[3] = uint8_t (0x000000FF & packetSize);
	
    }
  return(ret);
}

int DCNetPacket::Deallocate()
{
  DataSize = 0;
  MaxDataSize = 0;
  PacketSize = 0;    
  //Clear convenience pointers
  HeaderWord = NULL;
  PacketSize = NULL;
  Type = NULL;
  PacketNumber = NULL;
  Data = NULL;

  if(PacketData != NULL)
    {
      delete [] PacketData;
    }
  PacketData = NULL;
  return(true);
}

int DCNetPacket::Allocate(uint32_t packetDataSize)
{
  int ret = 0;  
  Deallocate();
  DataSize = 0;
  MaxDataSize = packetDataSize;
  MaxPacketSize = MaxDataSize + DCNETPACKET_HEADER_SIZE;
  //Allocate data
  PacketData = new uint8_t[MaxPacketSize];
  if(!PacketData)
    {
      
      ret = -1;
    }
  else
    {
      //Zero header data.
      for(int i =0; i < DCNETPACKET_HEADER_SIZE;i++)
	{
	  PacketData[i] = 0;
	}
      //Set internal pointers.
      HeaderWord = PacketData + DCNETPACKET_HEADER_WORD_POS;
      Type = PacketData + DCNETPACKET_HEADER_TYPE_POS;
      *Type = BAD_PACKET;
      PacketSize = PacketData + DCNETPACKET_HEADER_SIZE_POS1;
      PacketNumber = PacketData + DCNETPACKET_HEADER_PACKETNUMBER_POS;
      *PacketNumber = 0;
      Data = PacketData + DCNETPACKET_HEADER_SIZE;
    }
  return(ret);
}


int DCNetPacket::SendPacket(int socketFD,time_t seconds,suseconds_t useconds)
{
  //Setup timeout structure
  struct timeval timeout;
  timeout.tv_sec = seconds;
  timeout.tv_usec = useconds;

  int ret = 0;
  if(!Data)
    {
      ret = BAD_ALLOCATION;
    }
  else if(*Type == BAD_PACKET)
    {
      ret = BAD_PACKET_TYPE;
    }
  else //Everything seems ok, so send the packet.  
    {   
      uint32_t packetSize = DataSize + DCNETPACKET_HEADER_SIZE;   
      int32_t writeReturn = writeN(socketFD,PacketData,packetSize,timeout);
      if(writeReturn == 0)
	{
	  ret = 0;
	}
      else if(writeReturn == -1)
	{
	  ret = -1; 
	}
      else if(writeReturn == -2)
	{
	  ret = -2;
	}
      else
	{
	  ret = writeReturn;
	  *Type = BAD_PACKET;
	  DataSize = 0;
	}
    }
  //fprintf(stderr,"Sent packet: \n");
  //PrintPacket(stderr);
  return ret;
}

int DCNetPacket::ListenForPacket(int socketFD,int &error_return,time_t seconds,suseconds_t useconds)
{
  //Setup timeout structure
  struct timeval timeout;
  timeout.tv_sec = seconds;
  timeout.tv_usec = useconds;

  int ret = 0;
  if(!Data)
    {
      ret = BAD_ALLOCATION;
    }
  else
    //Data allocated
    {            
      //=======================================
      //Read the header.
      //=======================================
      uint32_t packetSize = DCNETPACKET_HEADER_SIZE;
      int error_return = 0;
      int32_t readReturn = readN(socketFD,PacketData,packetSize,error_return);
      //=======================================
      //Valid header size.
      //=======================================
      if(readReturn == DCNETPACKET_HEADER_SIZE)
	//Read valid message header
	{
	  //=======================================
	  //check if the packet has the correct header.
	  //=======================================
	  if(*HeaderWord == DCNETPACKET_HEADER_WORD)
	    {	      
	      //Determine data size
	      uint32_t size = ((uint32_t(PacketSize[0])<<24) | (uint32_t(PacketSize[1])<<16)
	      		       | (uint32_t(PacketSize[2])<<8) | (uint32_t(PacketSize[3]))) - DCNETPACKET_HEADER_SIZE;
	      if(size > MaxDataSize)
		{
		  *Type = BAD_PACKET;
		  ret = PACKET_TOO_LARGE;
		  //give back the readout size in error_return
		  error_return = size;
		}
	      else
		{
		  uint8_t * ptr = Data;
		  readReturn = readN(socketFD,ptr,size,error_return);
		  
		  //=======================================
		  //Check data size versus read size.
		  //=======================================
		  if(uint32_t(readReturn) == size)
		    {
		      ret =  readReturn + DCNETPACKET_HEADER_SIZE;
		      DataSize = readReturn;
		    }
		  //=======================================
		  //EOF
		  //=======================================
		  else if(readReturn == 0)
		    {	  
		      *Type = BAD_PACKET;
		      ret = PACKET_EOF;
		      //give back nothing in error_return
		    }
		  else
		    {
		      fprintf(stderr,
			      "ERROR ListenForPacket: Read header error! %s(%d)\n",
			      strerror(error_return),error_return);
		      *Type = BAD_PACKET;
		      ret = PACKET_DATA_READ_SIZE;
		    }		  
		}
	    }//Header word check
	  else
	    {
	      fprintf(stderr,"ListenForPacket: Bad header word\n");
	      fprintf(stderr,"%02X",*HeaderWord);
	      fprintf(stderr,"%02X",*Type);
	      fprintf(stderr,"%02X",PacketSize[0]);
	      fprintf(stderr,"%02X",PacketSize[1]);
	      fprintf(stderr,"%02X",PacketSize[2]);
	      fprintf(stderr,"%02X",PacketSize[3]);
	      fprintf(stderr,"%02X\n",*PacketNumber);
	      //bad header
	      *Type = BAD_PACKET;
	      ret = PACKET_BAD_HEADER;
	      //Give back the bad header word in error_return
	      error_return = *HeaderWord;
	    }	  
	}
      //=======================================
      //EOF
      //=======================================
      else if(readReturn == 0)
	{	  
	  *Type = BAD_PACKET;
	  ret = PACKET_EOF;
	  //give back nothing in error_return
	}
      //=======================================
      //Other errors.
      //=======================================
      else
	{
	  fprintf(stderr,"ERROR ListenForPacket: Read header error! %s(%d)\n",strerror(error_return),error_return);
	  *Type = BAD_PACKET;
	  ret = PACKET_READ_ERROR;
	}
    }
  return ret;
}

void DCNetPacket::CopyObj(const DCNetPacket & rhs)
{
  //Clean this packet and resize it to rhs' size
  //This sets:
  //  MaxDataSize
  //  MaxPacketSize;
  //  HeaderWord pointer
  //  Type pointer (sets type to BAD_PACKET)
  //  PacketSize pointer (set to zero)
  //  PacketNumber pointer (sets packet number to zero)
  //  PacketData pointer
  this->Allocate(rhs.MaxDataSize);
  //Copy the data and set up this packet
  //This sets:
  //  PacketNumber
  //  Header word
  //  Data (memcopy)
  //  DataSize
  //  PacketSize
  this->SetData(rhs.PacketData,rhs.DataSize,*(rhs.PacketNumber));
  //set the type
  this->SetType(*(rhs.Type));
}
