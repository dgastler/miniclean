#include "TH2F.h"
#include "TH1D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "math.h"
#include "TList.h"

TF1 * PSDCut(TH2F * h,Int_t N);
