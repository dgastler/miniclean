//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//Class includes
#include "PEvent.h"
#include "TextArray.h"
#include "xmlParser.h"

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TGraph.h"

using namespace std;

Float_t Volts = 0;
Float_t Seconds = 0;
Float_t C = 0;

//Find Single photo electron by sliding through projections of
//the SPE integral vs. SPE height 2d histogram. 
Float_t FindSPE(TH2F * h2,TH1D *&h1,char * Name)
{
  Int_t N = 40;
  Int_t Step = 5;
  Float_t *x = new Float_t[N];
  Float_t *dy = new Float_t[N];
  Float_t *y = new Float_t[N];
  Float_t yMax = 0;
  Int_t xMax= 0;
  Float_t SPE = 0;

  x[0] = 0;
  dy[0] = 0;
  y[0] = 0;
  
  for(int i = 2; i < N; i++)
    {
      h1 = h2->ProjectionX(Name,i,h2->GetNbinsY());
      x[i] = h2->GetYaxis()->GetBinCenter(i);
      y[i] = h1->GetMaximumBin();
      dy[i] = (y[i] - y[i-1])/(i - 1);
      delete h1;
      if(yMax < dy[i])
	{
	  yMax = dy[i];
	  xMax = i;
	}
    }

  for(int i = 0; i < N;i++)
    {
      if(dy[i] > 0.7 * yMax)
	{
	  xMax = i;
	  break;
	}
    }

  h1 = h2->ProjectionX("h1",xMax,h2->GetNbinsY());
  Int_t Cut0 = 0;
  Int_t Cut1 = 0;
  for(int i = h1->GetMaximumBin(); i >0 ;i--)
    {
      if(h1->GetBinContent(i) < 0.7*h1->GetMaximum())
	{
	  Cut0 = i;
	  break;
	}
    }
  for(int i = h1->GetMaximumBin(); i < h1->GetNbinsX();i++)
    {
      if(h1->GetBinContent(i) < 0.7*h1->GetMaximum())
	{
	  Cut1 = i;
	  break;
	}
    }
  TF1 * myfit = new TF1("myfit","gaus");
  h1->Fit(myfit,"","",
	  h1->GetXaxis()->GetBinCenter(Cut0),
	  h1->GetXaxis()->GetBinCenter(Cut1));
  SPE = myfit->GetParameter(1);

  delete x;
  delete y;
  delete dy;
  delete myfit;

  return(SPE);
}



int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      cout << argv[0] << " inFile" << endl;
      return(0);
    }

  Volts = (1.0/255.0);             
  Seconds = 2E-9;                  
  C = Volts * Seconds * (1.0/50.0);

  TFile *InFile = new TFile(argv[1],"UPDATE");
  TTree *ptree  = (TTree*) InFile->Get("ptree");
  TTree *TextTree = (TTree*) InFile->Get("TextTree");
  Int_t NEvents = ptree->GetEntries();
  PEvent *pevent = 0;
  TextArray *RootText = 0;
  ptree->SetBranchAddress("PEvent branch",&pevent);
  TextTree->SetBranchAddress("Run Notes",&RootText);
  TextTree->GetEntry(1);
  char * TempPointer = RootText->GetArray();
  char Name0[] = "SPE0";
  char Name1[] = "SPE1";

  XMLNode DAQNode=XMLNode::parseString(TempPointer,"DAQ");

  TH2F * PMT0SPE  = (TH2F *) InFile->Get("PMT0SPE");
  TH1D * SPE0 = 0;
  Float_t Integral0 = FindSPE(PMT0SPE,SPE0,Name0);
  cerr << "#PMT0 " << DAQNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",0).getAttribute("PMTVoltage") << " " << Integral0 << endl;
  InFile->Delete("Name0");
  SPE0->Write(Name0);
  TH2F * PMT1SPE  = (TH2F *) InFile->Get("PMT1SPE");
  TH1D * SPE1 = 0;
  Float_t Integral1 = FindSPE(PMT1SPE,SPE1,Name1);
  cerr << "#PMT1 " << DAQNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",2).getAttribute("PMTVoltage") << " " << Integral1 << endl;
  InFile->Delete(Name1);
  SPE1->Write(Name1);
  InFile->Close();
  
  return(0);
}
