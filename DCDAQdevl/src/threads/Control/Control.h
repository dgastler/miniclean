#ifndef __CONTROL__
#define __CONTROL__

#include <DCThread.h>

#include <unistd.h>
#include <fcntl.h>

#include <vector>
#include <string>
#include <algorithm> // for transform
#include <cctype> // for tolower
#include <fstream> //opening files

class Control : public DCThread 
{
 public:
  Control(std::string Name);
  ~Control();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);  
  virtual void MainLoop();
 private:
  void LowerCase(std::string & Text);
  void RemoveEOL(std::string & Text);
  int bufferSize;
  char * buffer;

  //Commands
  void Quit(std::string arg);
  void Start(std::string arg);
  void Pause(std::string arg);
  void Help(std::string arg);
  void Echo(std::string arg);
  void SendOut(std::string arg);
  void Status(std::string arg);
  void Load(std::string arg);

  void ProcessCommand(std::string Command);
  std::vector<std::string> Commands;
  std::vector<void (Control::*)(std::string arg)> CommandFunctions;

  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
};


#endif
