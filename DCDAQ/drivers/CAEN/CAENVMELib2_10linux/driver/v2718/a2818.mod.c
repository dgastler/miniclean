#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__attribute_used__
__attribute__((section("__versions"))) = {
	{ 0x6109265e, "struct_module" },
	{ 0xc8be7b15, "_spin_lock" },
	{ 0x1d26aa98, "sprintf" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0x1e44be4b, "create_proc_entry" },
	{ 0x1fc91fb2, "request_irq" },
	{ 0x9eac042a, "__ioremap" },
	{ 0x4c3af445, "__request_region" },
	{ 0x677ed3c5, "register_chrdev" },
	{ 0xb87eba73, "dma_alloc_coherent" },
	{ 0xffd3c7, "init_waitqueue_head" },
	{ 0x91d6536d, "__mutex_init" },
	{ 0xceea49bb, "kmem_cache_alloc" },
	{ 0xd866b1c0, "kmalloc_caches" },
	{ 0x7df4bc8a, "pci_enable_device" },
	{ 0x88de2a18, "pci_get_subsys" },
	{ 0xa2268b59, "mutex_unlock" },
	{ 0x83cbc699, "mutex_lock" },
	{ 0xb2fd5ceb, "__put_user_4" },
	{ 0x2da418b5, "copy_to_user" },
	{ 0xf2a644fb, "copy_from_user" },
	{ 0x5e1389a, "finish_wait" },
	{ 0x17d59d01, "schedule_timeout" },
	{ 0x7c9049bf, "prepare_to_wait" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0x2d61163e, "per_cpu__current_task" },
	{ 0x546f0b06, "_spin_lock_irq" },
	{ 0xbaadbd11, "__wake_up" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x1b7d4074, "printk" },
	{ 0xc192d491, "unregister_chrdev" },
	{ 0xf67444c2, "remove_proc_entry" },
	{ 0x2fd1d81c, "vfree" },
	{ 0x37a0cba, "kfree" },
	{ 0x360d8c06, "dma_free_coherent" },
	{ 0x8bb33e7d, "__release_region" },
	{ 0xdc3eaf70, "iomem_resource" },
	{ 0xedc03953, "iounmap" },
	{ 0xf20dabd8, "free_irq" },
};

static const char __module_depends[]
__attribute_used__
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "68625743E801FC3335B12C8");
