#ifdef __RATDS__
#include <DRPCAssembler.h>

void DRPCAssembler::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.GetSocketFD()))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < server.Size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(server[iConnection].GetFullPacketFD()))
	{
	  ProcessDRPusher(server[iConnection]);
	}
      //Check for a new message
      if(IsReadReady(server[iConnection].GetMessageFD()))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

void DRPCAssembler::ProcessDRPusher(Connection & conn)
{
  //Get new incomming packet
  int32_t iDCPacket = FreeFullQueue::BADVALUE;
  conn.GetInPacketManager()->GetFull(iDCPacket,true);
  //Check for bad packet type
  if(iDCPacket != FreeFullQueue::BADVALUE)
    //valid packet
    {
      DCNetPacket * packet = conn.GetInPacketManager()->GetPacket(iDCPacket);
      int64_t retProcessEvent = 0;
      //Check if this is the correct kind of packet
      if(!((packet->GetType() == DCNetPacket::INTEGRATED_WINDOWS_PARTIAL_PACKET) ||
	   (packet->GetType() == DCNetPacket::INTEGRATED_WINDOWS_FULL_PACKET)))
	{
	  //ProcessBadPacket cleans up.
	  PrintError("Unexpected packet type!\n");
	  //return packet
	  conn.GetInPacketManager()->AddFree(iDCPacket);
	  //move on	  
	}
      else
	{
	  //Get the data from the packet.
	  uint8_t * packetData = packet->GetData();
	  uint8_t * currentPointer = packetData;
	  uint16_t packetDataSize = packet->GetSize();
	  uint16_t packetDataLeft = packetDataSize;    
	  //fprintf(stderr,"(%s) Packet data size: %u\n",GetName().c_str(),packetDataSize);

	  lastChannelWord = 0;
	  lastRawIntegral = 0;
	  lastRawStartTime = 0;
	  lastRawWidth = 0;
	  debugInformation.clear();

	  //Loop over all the Event structs and their data in this array
	  while(packetDataLeft > 0)
	    {
	      retProcessEvent = ProcessDRPusherEvent(currentPointer,packetDataLeft);
	      if(retProcessEvent==PacketParsingError::BAD_PASSWORD)
		{
		  conn.GetInPacketManager()->AddFull(iDCPacket);
		  return;
		}
	      if(retProcessEvent==PacketParsingError::OTHER_ERROR)
		{
		  ParseBadPacket(packet);
		  break;
		}
	    }      
	}
      //return packet
      if(retProcessEvent!=PacketParsingError::BAD_PASSWORD)
	{
	  conn.GetInPacketManager()->AddFree(iDCPacket);      
	}
    }
}

int32_t DRPCAssembler::ProcessDRPusherEvent(uint8_t * & data,uint16_t &maxSize)
{
  int32_t ret = 0;
  //Check if there is enough data for an event block
  if(maxSize < sizeof(DRPacket::Event))
    {
      fprintf(stderr,"Packet data left: %u, Minimal size of event block: %u\n",maxSize,int(sizeof(DRPacket::Event)));
      fprintf(stderr,"Not enough data left in packet for an event block.\n");
      return -1;
    }
  
  //Cast this packets event header to drEvent so we can
  //get the event number and move forward in the data array
  DRPacket::Event * drEvent = (DRPacket::Event *) data;

  data += sizeof(DRPacket::Event);
  maxSize -= sizeof(DRPacket::Event);
#ifdef SUPER_DEBUG
  DebugInformation debug;
  debug.type = 1;
  debug.sizeLeft = maxSize;
  debugInformation.push_back(debug);
#endif
  //Get this event from the Event hash
  int32_t evIndex;
  int64_t eventKey = DRPC_mem->GetKey(drEvent);
  RATEVBlock * evBlock = NULL;
  uint64_t password = DRPC_mem->GetBlankPassword();
  int64_t retDRPCmem = DRPC_mem->GetEvent(eventKey,
					  evIndex,
					  password);
#ifdef __GROUP_TIMING_DEBUG__
  fprintf(stderr,"DRPCAssembler Using key: %zd(0x%zX) Got index:%d\n",eventKey,eventKey,evIndex);
#endif 

#ifdef SUPER_DEBUG
  DebugInformation debug_drpcmem;
  debug_drpcmem.type = 5;
  debug.sizeLeft = retDRPCmem;
  debugInformation.push_back(debug_drpcmem);
#endif
  while(retDRPCmem >= 0)
    {
      //If there is a collision
      //Move old event to bad event queue and 
      //address the FEPCs at fault
      ProcessCollision(eventKey,retDRPCmem,evIndex,password);
      
      //Now things should be fixed and we should get our event
      retDRPCmem = DRPC_mem->GetEvent(eventKey,evIndex,password);      
    }   
  
  //No collision
  if(retDRPCmem == returnDCVector::BAD_PASSWORD)
    {
      return PacketParsingError::BAD_PASSWORD;
    }

  
  if(retDRPCmem == returnDCVector::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    {
      //Get our evBlock
      evBlock = DRPC_mem->GetDRPCBlock(evIndex);
      evBlock->reductionLevel = drEvent->DataType;
   
      // fprintf(stderr,"(%s) Event ID: %u\n", GetName().c_str(),drEvent->ID);
      // fprintf(stderr,"(%s) Event time: %u\n", GetName().c_str(),drEvent->EventTime);
      // fprintf(stderr,"(%s) Board count: %u\n", GetName().c_str(),drEvent->BoardCount);
      // fprintf(stderr,"(%s) Reduction level: %u\n", GetName().c_str(),evBlock->reductionLevel);
 
      if(evBlock->wfdEventID == -1)
	{
	  evBlock->wfdEventID = drEvent->ID;
	
	  evBlock->state = EventState::UNFINISHED;

	  //If not running TriggerAssembler, set event ID to wfdEventID.
	  if(!runningTrigger)
	    {
	      evBlock->eventID = drEvent->ID;
	      evBlock->ev->SetEventID(drEvent->ID);
	      evBlock->eventTime = drEvent->EventTime;
	    }
	}
	  
      //Process the block
      uint32_t toProcessSize = maxSize;
      if(!AddDataToEV(evBlock,
		      data,
		      toProcessSize,
		      drEvent->BoardCount))
	{
	 
	}
      //Calculate how much data we processed for this event structure
      ret = maxSize - toProcessSize;
      maxSize = toProcessSize;

      //Check if we have readout all the WFDs for this event.   
      if((evBlock->ev->GetBoardCount() == int(detectorInfo.GetWFDCount()))
	 &&(evBlock->ev->GetEventID()!=-1))
	{ 
	  // std::stringstream ss;
	  // ss << "Finished event. " << "Event ID: " << evBlock->ev->GetEventID() <<" Event time: " << evBlock->eventTime << std::endl;
	  // for(int WFDIndex = 0; WFDIndex<evBlock->ev->GetBoardCount(); WFDIndex++)
	  //   {
	  //     std::vector<uint32_t> boardHeader = evBlock->ev->GetBoard(WFDIndex)->GetHeader();	
	  //     int wfdID = evBlock->ev->GetBoard(WFDIndex)->GetID();
	  //     ss << "\t WFD " <<  wfdID
	  // 	 << "  Time:" << boardHeader[3]
	  // 	 << std::dec << "\n";
	  //   }
	  //fprintf(stderr,ss.str().c_str());
	  int64_t moveEventRet = DRPC_mem->MoveEventToUnProcessedQueue(eventKey,password);
	  if(moveEventRet!=returnDCVector::OK)
	    {
	      fprintf(stderr,"Something went wrong in MoveEventToUnProcessedQueue!\n");
	    }
	}
      else
	//if the channel add was fine, but we haven't fully filled
	//the event, return it to the DRPCRATManager
	{
       	  //std::stringstream ss;
	  //ss << "Event ID: " << evBlock->ev->GetEventID() << "\n     WFDEventID: " << evBlock->wfdEventID  << std::endl
	  //   << "Time: " << evBlock->eventTime << std::endl;
	  //ss << "Boards read out for this event: " << std::endl;
	  //for(int iBoard = 0; iBoard < evBlock->ev->GetBoardCount(); iBoard++)
	  //  {
	  //    ss << "WFD " << evBlock->ev->GetBoard(iBoard)->GetID() << "  Time:" << evBlock->ev->GetBoard(iBoard)->GetHeader()[3]  << std::endl;
	  //  }
	  //fprintf(stderr,ss.str().c_str());
	  DRPC_mem->SetEvent(eventKey,evIndex,password);
	}
    }

  else 
    { 
      PrintError("DRPCmem error!\n");
    }    
  return ret;
}

bool DRPCAssembler::AddDataToEV(RATEVBlock * event,uint8_t * &data,uint32_t &dataSizeMax,int boardCount)
{  
  bool ret = true;
  //Get the EV from the RATEVBlock.
  RAT::DS::EV * ev = event->ev;
  for(int iBoard = 0; iBoard < boardCount; iBoard++)
    {
      //Check if there is room for a board
      if(sizeof(DRPacket::Board) <= dataSizeMax)
	{
	  //cast a board object from the data pointer
	  DRPacket::Board * drBoard = (DRPacket::Board *) data;
	  data += sizeof(DRPacket::Board);
	  dataSizeMax -= sizeof(DRPacket::Board);

	  // fprintf(stderr,"(%s) BoardID: %u\n", GetName().c_str(),drBoard->BoardID);
	  // fprintf(stderr,"(%s) LVDSPattern: 0x%04X\n", GetName().c_str(), drBoard->LVDSPattern);
	  // fprintf(stderr,"(%s) ChannelPattern: 0x%02X\n", GetName().c_str(), drBoard->ChannelPattern);

#ifdef SUPER_DEBUG
	  DebugInformation debug_board;
	  debug_board.type = 2;
	  debug_board.sizeLeft = dataSizeMax;
	  debugInformation.push_back(debug_board);
	  fprintf(stderr,"Size of debugInformation: %i",
		  int(sizeof(debugInformation)));
#endif
		  
		  
	  //Add a new board to the data structure
	  RAT::DS::Board * board = ev->AddNewBoard();	  
	  board->SetID(drBoard->BoardID);
	  std::vector<uint32_t> headerVector (4,0);
	  headerVector[1] = (((drBoard->BoardID)<<27)
			     |((drBoard->LVDSPattern)<<8)
			     |(drBoard->ChannelPattern));

	  headerVector[3] = drBoard->Time;
	  //headerVector[3] = drBoard->Time;
	  board->SetHeader(headerVector);
	  
	  //Loop over channels
	  for(uint8_t channelShift = 0; channelShift < 8; channelShift++)
	    {
	      //Check if we have a channel for this bitshift
	      if(drBoard->ChannelPattern & ((uint8_t(0x1) << channelShift)))
		{
		  if(sizeof(DRPacket::Channel) <= dataSizeMax)
		    {
		      DRPacket::Channel * drChannel = (DRPacket::Channel *) data;
		      data += sizeof(DRPacket::Channel);
		      dataSizeMax -= sizeof(DRPacket::Channel);
#ifdef SUPER_DEBUG
		      DebugInformation debug_channel;
		      debug_channel.type = 3;
		      debug_channel.sizeLeft = dataSizeMax;
		      debugInformation.push_back(debug_channel);
#endif
		      RAT::DS::Channel * channel = board->AddNewChannel();
		      //channel->SetID(channelShift);
		      channel->SetInput(channelShift);
		      channel->SetID(detectorInfo.GetChannelMap(drBoard->BoardID,channelShift));
		      
		      uint8_t rawIntegralCount = drChannel->RawIntegralCount;
		      //fprintf(stderr,"(%s) Raw integral count: %u\n", GetName().c_str(),drChannel->RawIntegralCount);
		    
		      lastChannelWord = rawIntegralCount;
		      if(event->reductionLevel == 3)
			//ZLE integrals
			{
			  for(uint8_t iIntegral = 0; 			  
			      iIntegral < rawIntegralCount;
			      iIntegral++)
			    {
			      if(sizeof(DRPacket::RawIntegral) <= dataSizeMax)
				{
				  //Setup our rawIntegral ptr
				  DRPacket::RawIntegral * drRawIntegral = (DRPacket::RawIntegral *) data;
				  data += sizeof(DRPacket::RawIntegral);
				  dataSizeMax -= sizeof(DRPacket::RawIntegral);
#ifdef SUPER_DEBUG
				  DebugInformation debug_raw;
				  debug_raw.type = 4;
				  debug_raw.sizeLeft = dataSizeMax;
				  debugInformation.push_back(debug_raw);
#endif      
				  RAT::DS::RawBlock * rawBlock = channel->AddNewRawBlock();
				  //Set values
				  rawBlock->SetIntegral(drRawIntegral->integral);
				  rawBlock->SetOffset(drRawIntegral->startTime);
				  rawBlock->SetWidth(drRawIntegral->width);
				  
				  rawBlock->SetBaseline(-1);
				  rawBlock->SetBaselineRMS(-1);
				  
				  lastRawIntegral = drRawIntegral->integral;
				  lastRawStartTime = drRawIntegral->startTime;
				  lastRawWidth = drRawIntegral->width;
				}
			      else
				{
				  ret = false;
				}
			    }
			}
		      else
			//Prompt/total mode
			{
			
			  if(sizeof(DRPacket::PromptTotalBlock) <= dataSizeMax)
			    {
			      DRPacket::PromptTotalBlock * drPromptTotal = (DRPacket::PromptTotalBlock *) data;
			      data += sizeof(DRPacket::PromptTotalBlock);
			      dataSizeMax -= sizeof(DRPacket::PromptTotalBlock);

			      channel->SetPromptQ(drPromptTotal->prompt);
			      channel->SetTotalQ(drPromptTotal->total);
			    
			      //fprintf(stderr,"(%s) Prompt: %f\n", GetName().c_str(),drPromptTotal->prompt);
			      //fprintf(stderr,"(%s) Total: %f\n", GetName().c_str(),drPromptTotal->total);
			    
			    }
			  else
			    {
			      ret = false;
			    }
			}
#ifdef SUPER_DEBUG
		      fprintf(stderr,"Channel %u added; dataSizeMax: %u\n",
			      channelShift,dataSizeMax);
#endif
		    }
		  else
		    {
		      ret = false;
		    }
		}
	    }
	}
      else
	{
	  ret = false;
	}
    }
  return ret;
}
  
void DRPCAssembler::ProcessCollision(int64_t newEventKey,
				     int64_t oldEventKey,
				     int32_t evIndex,
				     uint64_t password)
{
  //We have a conflict

  //The data in the vector hash is for another event. 
  //We have a lock on it though!
  //We will move this event to the bad queue
  //Action concerning this bad event will be left to
  //another thread.

  RATEVBlock * block = DRPC_mem->GetDRPCBlock(evIndex);
  uint8_t boardCount = block->ev->GetBoardCount();

  std::stringstream ss;
  ss << "Collision!  Old key: " << oldEventKey
     << "   New key: " << newEventKey << std::endl
     << "   Board count: " << boardCount << std::endl;
  for(int WFDIndex = 0; WFDIndex<boardCount; WFDIndex++)
		    {
		      std::vector<uint32_t> boardHeader = block->ev->GetBoard(WFDIndex)->GetHeader();	
		      int wfdID = block->ev->GetBoard(WFDIndex)->GetID();
		      ss << "\t WFD " <<  wfdID
			 << "  Time:" << std::hex << boardHeader[3]
			 << std::dec << "\n";
		    }

  PrintWarning(ss.str().c_str());
  //fprintf(stderr,ss.str().c_str());

  //char * buffer = new char[100];
  // char formatString[] = "Collision! Old key: %zd; New key: %zd; Board count: %u\n";
  // sprintf(buffer,formatString,oldEventKey,newEventKey,boardCount);
  // delete [] buffer;

  //int64_t retCollision = DRPC_mem->MoveEventToBadQueue(oldEventKey,
  DRPC_mem->MoveEventToBadQueue(oldEventKey,
				password,
				Level::STORAGE_COLLISION);
  
  //PrintWarning("Address collision\n");
}

void DRPCAssembler::ParseBadPacket(DCNetPacket * packet)
{
  PrintWarning("Parsing packet error!");
  //Open file to write to.
  const size_t textBufferSize = 1000;
  char textBuffer[textBufferSize];
  
  //Get current time
  time_t errorTime = time(NULL);
  if(errorTime==lastErrorTime)
    {
      badCount++;
    }
  else
    {
      badCount = 0;
    }
  lastErrorTime = errorTime;
  snprintf(textBuffer,textBufferSize,
	   "bad_packet/DCNetPacket_CORRUPT_%010ld_%u_%u_%s.dat",
	   errorTime,
	   badCount,
	   packet->GetSize(),
	   GetName().c_str());
  FILE * errorFile = fopen(textBuffer,"w");
  if(errorFile)
    {
      packet->PrintPacket(errorFile);
      fprintf(stderr,"Printed bad packet to file.\n");
      fprintf(stderr,"Time: %010ld  Count: %u\n",errorTime,badCount);
      fprintf(stderr,"Last channel word: %u\n",lastChannelWord);
      fprintf(stderr,"Last raw integral: %u\n",lastRawIntegral);
      fprintf(stderr,"Last raw start time: %u\n",lastRawStartTime);
      fprintf(stderr,"Last raw width: %u\n",lastRawWidth);
      fclose(errorFile);
    }
  snprintf(textBuffer,textBufferSize,
	   "bad_packet/DCNetPacket_CORRUPT_%010ld_%u_%u_%s_debugging.dat",
	   errorTime,
	   badCount,
	   packet->GetSize(),
	   GetName().c_str());
  FILE * packetDebugFile = fopen(textBuffer,"w");
  if(packetDebugFile)
    {
      
      for(int debugIndex=0;debugIndex<int(debugInformation.size());debugIndex++)
	{
	  if(debugInformation[debugIndex].type==1)
	    {
	      fprintf(packetDebugFile,"Processed event header; size set to %u\n",
		      int(debugInformation[debugIndex].sizeLeft));
	    }
	  if(debugInformation[debugIndex].type==2)
	    {
	      fprintf(packetDebugFile,"Processed board header; size set to %u\n",
		      int(debugInformation[debugIndex].sizeLeft));
	    }
	  if(debugInformation[debugIndex].type==3)
	    {
	      fprintf(packetDebugFile,"Processed channel header; size set to %u\n",
		      int(debugInformation[debugIndex].sizeLeft));
	    }
	  if(debugInformation[debugIndex].type==4)
	    {
	      fprintf(packetDebugFile,"Processed raw window; size set to %u\n",
		      int(debugInformation[debugIndex].sizeLeft));
	    }
	  if(debugInformation[debugIndex].type==5)
	    {
	      fprintf(packetDebugFile,"Get event returned %u\n",
		      int(debugInformation[debugIndex].sizeLeft));
	    }
	}
      fclose(packetDebugFile);
      fprintf(stderr,"Closed debug file\n");
    }
}


#endif
