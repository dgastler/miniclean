#include <iostream>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include "vme_crate_lib.h"

using namespace std;

//!!!!!!!!!!!!!!!!!!!FIX VME RESET!

//----------------------------------------------------------------------------
//Crate constructor
//openes crate and does some test on it.
//----------------------------------------------------------------------------

VME_Crate::VME_Crate()
{
  cratefd = open(cratedev, O_RDWR);
  if(cratefd<1)
    {
      cout << "Failed to open " <<cratedev << endl;
      exit(-1);
    }
  
  int dmalen[] = {2,2};
  int ret = ioctl(cratefd, SIS1100_MINDMALEN, dmalen);
  if(ret != 0)
    {
      cout << "Failed to set mindmalen for reads and writes to 0\n";
      exit(-2);
    }
}



//----------------------------------------------------------------------------
//Crate deconstructor
//closes crate when done
//----------------------------------------------------------------------------
VME_Crate::~VME_Crate()
{  
  int ret = close(cratefd);
  if(ret == 0)
    {
      cout << "Failed to close" << cratedev << endl;
    }
}


//----------------------------------------------------------------------------
//Reset VME crate
//----------------------------------------------------------------------------
int VME_Crate::reset()
{
 return(vmesysreset(cratefd));

}
//----------------------------------------------------------------------------
//sis3100 control read
//----------------------------------------------------------------------------
int VME_Crate::controlread(int offset, u_int32_t* data)
{
  return(s3100_control_read(cratefd,offset,data));
}
//----------------------------------------------------------------------------
//sis3100 control write
//----------------------------------------------------------------------------
int VME_Crate::controlwrite(int offset, u_int32_t data)
{
  return(s3100_control_write(cratefd,offset,data));
}
//----------------------------------------------------------------------------
//IRQ Ackn
//----------------------------------------------------------------------------
int VME_Crate::Iack(u_int32_t irq, u_int8_t *data)
{
  return(vme_IACK_D8_read(cratefd,irq,data));
}

//----------------------------------------------------------------------------
//Geographic write 
//write to a wfd card using slot number
//----------------------------------------------------------------------------
int VME_Crate::geowrite(int slot, int offset, u_int8_t value)
{
  sis1100_vme_req req;
  
  req.size = sizeof(u_int8_t);
  req.am = crcsr;
  req.addr = static_cast<u_int32_t>( (slot<<19)+offset );
  req.data = value;

  int ret = ioctl(cratefd, SIS3100_VME_WRITE, &req);
  if(ret>= 0)
    {
    ret = req.error;
    }
  return(ret);
}

//----------------------------------------------------------------------------
//Geographic read 
//read from a wfd card using slot number
//----------------------------------------------------------------------------
int VME_Crate::georead(int slot, int offset, u_int8_t* value)
{
  sis1100_vme_req req;
  
  req.size = sizeof(u_int8_t);
  req.am = crcsr;
  req.addr = static_cast<u_int32_t>( (slot<<19)+offset );
  
  int ret = ioctl(cratefd, SIS3100_VME_READ, &req);
  if(ret>=0)
    {
      ret = req.error;
    }
  
  *value = req.data;
  
  return(ret);
}


//----------------------------------------------------------------------------
//32 bit address 32bit data write
//
//----------------------------------------------------------------------------
int VME_Crate::a32d32write(u_int32_t addr, u_int32_t value)
{
  sis1100_vme_req req;
  
  req.size = sizeof(u_int32_t);
  req.am = a32d32;
  req.addr = addr;
  req.data = value;

  int ret = ioctl(cratefd, SIS3100_VME_WRITE, &req);
  if(ret>= 0)
    {
    ret = req.error;
    }
  return(ret);
}


//----------------------------------------------------------------------------
//32 bit address 32bit data read
//
//----------------------------------------------------------------------------
int VME_Crate::a32d32read(u_int32_t addr, u_int32_t* value)
{
  sis1100_vme_req req;
  
  req.size = sizeof(u_int32_t);
  req.am = a32d32;
  req.addr = addr;
  
  int ret = ioctl(cratefd, SIS3100_VME_READ, &req);
  if(ret>=0)
    {
      ret = req.error;
    }
  
  *value = req.data;
  
  return(ret);
}

//----------------------------------------------------------------------------
//32 bit address 16bit data write
//
//----------------------------------------------------------------------------
int VME_Crate::a32d16write(u_int32_t addr, u_int16_t value)
{
  sis1100_vme_req req;
  
  req.size = sizeof(u_int16_t);
  req.am = a32d32;
  req.addr = addr;
  req.data = value;

  int ret = ioctl(cratefd, SIS3100_VME_WRITE, &req);
  if(ret>= 0)
    {
    ret = req.error;
    }
  return(ret);
}


//----------------------------------------------------------------------------
//32 bit address 16bit data read
//
//----------------------------------------------------------------------------
int VME_Crate::a32d16read(u_int32_t addr, u_int16_t* value)
{
  sis1100_vme_req req;
  
  req.size = sizeof(u_int16_t);
  req.am = a32d32;
  req.addr = addr;
  
  int ret = ioctl(cratefd, SIS3100_VME_READ, &req);
  if(ret>=0)
    {
      ret = req.error;
    }
  
  *value = req.data;
  
  return(ret);
}
//----------------------------------------------------------------------------
//32 bit address 64bit data block write
//
//----------------------------------------------------------------------------
int VME_Crate::a32mblt64write(u_int32_t addr, u_int32_t* buffer, u_int32_t count)
{
  sis1100_vme_block_req req;
  
  req.am = a32mblt64;
  req.addr = addr;
  req.fifo = 1;
  //      req.num = request_words;
  req.num = count;
  req.data =  reinterpret_cast<u_int8_t*>(buffer);
  req.size = 4;
  
  int ret = ioctl(cratefd, SIS3100_VME_BLOCK_WRITE, &req);
  if(ret<0)
    return ret;
  else
    return req.error;
}

//----------------------------------------------------------------------------
//32 bit address 64bit data block read
//
//----------------------------------------------------------------------------
int VME_Crate::a32mblt64read(u_int32_t addr, u_int32_t* buffer, u_int32_t count)
{
  sis1100_vme_block_req req;
  
  req.am = a32mblt64;
  req.addr = addr;
  req.fifo = 1;
  //      req.num = request_words;
  req.num = count;
  req.data = reinterpret_cast<u_int8_t*>(buffer);
  req.size = 4;
  
  int ret = ioctl(cratefd, SIS3100_VME_BLOCK_READ, &req);
  if(ret<0)
    return ret;
  else
    return (req.error);
}
