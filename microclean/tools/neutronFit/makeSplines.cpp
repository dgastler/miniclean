#include <iostream>
#include <math.h>
#include <vector>

#include "TFile.h"
#include "TH1F.h"
#include "THStack.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TTree.h"
#include "TF1.h"
#include "TColor.h"
#include "TLegend.h"
#include "TRandom.h"

//Take a float from 0 to 1 and return a color on the rainbow.
Color_t GetColor(double flevel)
{
  if(flevel < 0)
    flevel = 0;
  unsigned int MaxLevel = 256*6 - 1;
  unsigned int level = flevel*MaxLevel;
  if(level > MaxLevel)
    level = MaxLevel;
  unsigned int Leg = 0;
  unsigned int PositionOnLeg = 0;
  if(level)
    {
      Leg = floor(level/256);
      PositionOnLeg = level%256;
    }
  

  
  Color_t color;
  switch (Leg)
    {
    case 0:
      color = TColor::GetColor(255,PositionOnLeg,0);
      break;
    case 1:
      color = TColor::GetColor(255-PositionOnLeg,255,0);
      break;
    case 2:
      color = TColor::GetColor(0,255,PositionOnLeg);
      break;
    case 3:
      color = TColor::GetColor(0,255-PositionOnLeg,255);
      break;
    case 4:
      color = TColor::GetColor(PositionOnLeg,0,255);
      break;
    case 5:
      color = TColor::GetColor(255,0,255-PositionOnLeg);
      break;
    default:
      color = TColor::GetColor(255,255,255);
    }
  //  printf("Leg: %u P: %u Color: %d\n",Leg,PositionOnLeg,color);
  return(color);
}




TH1F * SmearSpectrum(TH1F * InSpectrum,double SmearingFactor)
{
  std::string Name(InSpectrum->GetName());
  std::string Title(InSpectrum->GetTitle());

  Name = std::string("Smeared") + Name;
  Title = std::string("Smeared ") + Title;

  
  TH1F * SmearedSpectrum = new TH1F(Name.c_str(),
				    Title.c_str(),
				    InSpectrum->GetNbinsX(),
				    InSpectrum->GetXaxis()->GetXmin(),
				    InSpectrum->GetXaxis()->GetXmax());
  SmearedSpectrum->SetFillColor(InSpectrum->GetFillColor());
  SmearedSpectrum->SetFillStyle(InSpectrum->GetFillStyle());
  
   
  TF1 * SmearingFunction = new TF1("SmearingFunction","gaus",-1.0*InSpectrum->GetXaxis()->GetXmax(),InSpectrum->GetXaxis()->GetXmax());
  SmearingFunction->SetNpx(10000);
  //Vector to contain the new energy of each even after smearing. 
  std::vector<double> EntryEnergies;
  //loop over all bins in this histogram
  for(int bin = 1;bin <= InSpectrum->GetNbinsX();bin++) 
    {
      //Set smearing function.
      //A gaussian centered around the current energy bin with width of the sqrt of the enrgy bin.
      Double_t sigma = sqrt(SmearingFactor*InSpectrum->GetBinCenter(bin)); 
      SmearingFunction->SetParameters(1.0,
				      InSpectrum->GetBinCenter(bin),
				      sigma);

      for(int entry = 0;entry < InSpectrum->GetBinContent(bin);entry++)
	{
	  EntryEnergies.push_back(SmearingFunction->GetRandom());
	}
    }
  delete SmearingFunction;
  //Fill smeared histogram with smeared events.
  for(unsigned int entry = 0; entry < EntryEnergies.size();entry++)
    {
      SmearedSpectrum->Fill(EntryEnergies[entry]);
    }
  return(SmearedSpectrum);
}




int main(int argc, char ** argv)
{
  if(argc < 3)
    {
      printf("%s RATfile.root SPESpectrum.root *TOFWidth *TBWidth\n",argv[0]);
      return(0);
    }
  Double_t TOFWidth = 6;
  Double_t TBWidth = 0.4;
  if(argc == 5)
    {
      TBWidth = fabs(atof(argv[4]));
      TOFWidth = fabs(atof(argv[3]));
    }
  else if(argc == 4)
    {
      TOFWidth = fabs(atof(argv[3]));
    }

  

  //Open, load and error check all RAT MC histograms
  TFile * RATFile = TFile::Open(argv[1],"READ");
  if(!RATFile)
    {
      fprintf(stderr,"Bad RAT MC file.\n");
      return(0);
    }
  TTree * T = (TTree *) RATFile->Get("T");
  if(!T)
    {
      fprintf(stderr,"Bad tree.\n");
      return(0);
    }
  if(!T->GetBranch("LSc_volID"))
    {
      fprintf(stderr,"Bad branch LSc_VolID.\n");
      return(0);
    }  
  if(!T->GetBranch("TOF"))
    {
      fprintf(stderr,"Bad branch TOF.\n");
      return(0);
    }
  if(!T->GetBranch("numPe"))
    {
      fprintf(stderr,"Bad branch numPE.\n");
      return(0);
    }
  if(!T->GetBranch("num_PMT_0"))
    {
      fprintf(stderr,"Bad branch num_PMT_0.\n");
      return(0);
    }
  if(!T->GetBranch("num_PMT_1"))
    {
      fprintf(stderr,"Bad branch num_PMT_1.\n");
      return(0);
    }
  if(!T->GetBranch("asym"))
    {
      fprintf(stderr,"Bad branch asym.\n");
      return(0);
    }
  if(!T->GetBranch("int_proc"))
    {
      fprintf(stderr,"Bad branch int_proc.\n");
      return(0);
    }
  if(!T->GetBranch("int_scat"))
    {
      fprintf(stderr,"Bad branch int_scat.\n");
      return(0);
    }
  if(!T->GetBranch("ext_scat"))
    {
      fprintf(stderr,"Bad branch ext_scat.\n");
      return(0);
    }
  if(!T->GetBranch("Edep"))
    {
      fprintf(stderr,"Bad branch Edep.\n");
      return(0);
    }


  TFile * SPEFile = TFile::Open(argv[2],"READ");
  if(!SPEFile)
    {
      fprintf(stderr,"Bad SPE file.\n");
      return(0);
    }
  
  TH1F * PMT0SPE = (TH1F*) SPEFile->Get("NormalizedPMT0SPE");
  if(!PMT0SPE)
    {
      fprintf(stderr,"No SPE0 histogram\n");
      return(0);
    }
  TH1F * PMT1SPE = (TH1F*) SPEFile->Get("NormalizedPMT1SPE");
  if(!PMT1SPE)
    {
      fprintf(stderr,"No SPE1 histogram\n");
      return(0);
    }
      

  TRandom * randomNumbers = new TRandom();
 
  Int_t ID;
  T->SetBranchAddress("LSc_volID",&ID);
  Double_t TOF;
  T->SetBranchAddress("TOF",&TOF);
  Int_t NumPE;
  T->SetBranchAddress("numPe",&NumPE);
  Int_t Num_PMT_0;
  T->SetBranchAddress("num_PMT_0",&Num_PMT_0);
  Int_t Num_PMT_1;
  T->SetBranchAddress("num_PMT_1",&Num_PMT_1);
  Double_t Asym;
  T->SetBranchAddress("asym",&Asym);
  Int_t IntScat;
  T->SetBranchAddress("int_scat",&IntScat);
  Int_t ExtScat;
  T->SetBranchAddress("ext_scat",&ExtScat);
  Char_t  IntProc[20];
  T->SetBranchAddress("int_proc",&IntProc);
  Double_t Edep;
  T->SetBranchAddress("Edep",&Edep);


  //  std::vector<double> Angle(20,0);
  //  std::vector<double> Energy(20,0);
  std::vector<int> IDs(20,0);
  IDs[0] = 0;
  IDs[1] = 10;
  IDs[2] = 1;
  IDs[3] = 11;
  IDs[4] = 2;
  IDs[5] = 12;
  IDs[6] = 3;
  IDs[7] = 13;
  IDs[8] = 4;
  IDs[9] = 14;
  IDs[10] = 5;
  IDs[11] = 15;
  IDs[12] = 6;
  IDs[13] = 16;
  IDs[14] = 7;
  IDs[15] = 17;
  IDs[16] = 8;
  IDs[17] = 18;
  IDs[18] = 9;
  IDs[19] = 19;
    
//  Double_t TOFMax = 83;
//  Double_t TOFMin = 61;
  Double_t TOFMax = 72+TOFWidth;
  Double_t TOFMin = 61-TOFWidth;
  Double_t AsymMax = TBWidth;
  Double_t AsymMin = -1.0*TBWidth;
 
  int BufferSize = 1000;
  char * Buffer = new char[BufferSize];
  TObjArray * Splines = new TObjArray();
  for(unsigned int position = 0; position < IDs.size();position++)
    {



      //TOF histograms
      sprintf(Buffer,"TOF_1_0_%02d",position);
      TH1F * hTOF10 = new TH1F(Buffer,Buffer,200,0,200e-9);
      sprintf(Buffer,"TOF_n_n_%02d",position);
      TH1F * hTOFnn = new TH1F(Buffer,Buffer,200,0,200e-9);
      sprintf(Buffer,"TOF_1_n_%02d",position);
      TH1F * hTOF1n = new TH1F(Buffer,Buffer,200,0,200e-9);
      sprintf(Buffer,"TOF_O_%02d",position);
      TH1F * hTOFO = new TH1F(Buffer,Buffer,200,0,200e-9);
      hTOF10->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      hTOFnn->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      hTOF1n->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      hTOFO->GetXaxis()->SetTitle("Neutron time of flight (ns)");
      //Set fill options for these histograms
      hTOF10->SetFillColor(kBlue);
      hTOF1n->SetFillColor(kRed);  hTOF1n->SetFillStyle(3001);
      hTOFnn->SetFillColor(kRed);  hTOFnn->SetFillStyle(3005);
      hTOFO->SetFillColor(kGreen);


      TF1 * Gaus = new TF1("gaus","gaus",0,500);
      for(int Event = 0; Event < T->GetEntries();Event++)
	{
	  T->GetEntry(Event);
	  if(ID == IDs[position])
	    {
	      if((Asym < AsymMax) &&
		 (Asym > AsymMin) &&
		// (IntScat == 1) &&
		 //		 (ExtScat == 0) &&
		(IntProc[0] == 'H'))
		{
		  if(IntScat == 1)
		    {
		      if(ExtScat == 0)
			{
			  hTOF10->Fill(TOF*1e-9);
			}
		      else
			{
			  hTOF1n->Fill(TOF*1e-9);
			}
		    }
		  else
		    {
		      hTOFnn->Fill(TOF*1e-9);
		    }
		}
	      else
		{
		  hTOFO->Fill(TOF*1e-9);
		}
	    }
	  
	}
      //Find single scattering neutron time of flight.
      Gaus->SetParameters(hTOF10->GetMaximum(),hTOF10->GetBinCenter(hTOF10->GetMaximumBin()),1);
      hTOF10->Fit(Gaus,"Q");
      //Set TOF cuts from this gaussian fit
      TOFMax = (Gaus->GetParameter(1) + 5e-9)*1e9;
      TOFMin = (Gaus->GetParameter(1) - 5e-9)*1e9;
      sprintf(Buffer,"Pos %02d TOF: mean %5e sigma %5e",position,Gaus->GetParameter(1),Gaus->GetParameter(2));
      printf("%s\n",Buffer);

      

      //EnergySpectrum histograms slit into types of scatters
      sprintf(Buffer,"EnergySpectrum_1_0_%02d",position);
      TH1F * EnergySpectrum10 = new TH1F(Buffer,Buffer,400,0,800);
      sprintf(Buffer,"EnergySpectrum_n_n_%02d",position);
      TH1F * EnergySpectrumnn = new TH1F(Buffer,Buffer,400,0,800);
      sprintf(Buffer,"EnergySpectrum_1_n_%02d",position);
      TH1F * EnergySpectrum1n = new TH1F(Buffer,Buffer,400,0,800);
      sprintf(Buffer,"EnergySpectrum_O_%02d",position);
      TH1F * EnergySpectrumO = new TH1F(Buffer,Buffer,400,0,800);
      EnergySpectrum10->GetXaxis()->SetTitle("Recoil energy (keVee)");
      EnergySpectrumnn->GetXaxis()->SetTitle("Recoil energy (keVee)");
      EnergySpectrum1n->GetXaxis()->SetTitle("Recoil energy (keVee)");
      EnergySpectrumO->GetXaxis()->SetTitle ("Recoil energy (keVee)");
      //Set fill options for these histograms
      EnergySpectrum10->SetFillColor(kBlue);
      EnergySpectrum1n->SetFillColor(kRed);  EnergySpectrum1n->SetFillStyle(3001);
      EnergySpectrumnn->SetFillColor(kRed);  EnergySpectrumnn->SetFillStyle(3005);
      EnergySpectrumO->SetFillColor(kGreen);

      //EDepSpectrum histograms slit into types of scatters
      sprintf(Buffer,"EDepSpectrum_1_0_%02d",position);
      TH1F * EDepSpectrum10 = new TH1F(Buffer,Buffer,400,0,800);
      sprintf(Buffer,"EDepSpectrum_n_n_%02d",position);
      TH1F * EDepSpectrumnn = new TH1F(Buffer,Buffer,400,0,800);
      sprintf(Buffer,"EDepSpectrum_1_n_%02d",position);
      TH1F * EDepSpectrum1n = new TH1F(Buffer,Buffer,400,0,800);
      sprintf(Buffer,"EDepSpectrum_O_%02d",position);
      TH1F * EDepSpectrumO = new TH1F(Buffer,Buffer,400,0,800);
      EDepSpectrum10->GetXaxis()->SetTitle("Recoil energy (keVee)");
      EDepSpectrumnn->GetXaxis()->SetTitle("Recoil energy (keVee)");
      EDepSpectrum1n->GetXaxis()->SetTitle("Recoil energy (keVee)");
      EDepSpectrumO->GetXaxis()->SetTitle ("Recoil energy (keVee)");
      //Set fill options for these histograms
      EDepSpectrum10->SetFillColor(kBlue);
      EDepSpectrum1n->SetFillColor(kRed);  EDepSpectrum1n->SetFillStyle(3001);
      EDepSpectrumnn->SetFillColor(kRed);  EDepSpectrumnn->SetFillStyle(3005);
      EDepSpectrumO->SetFillColor(kGreen);








      for(int Event = 0; Event < T->GetEntries();Event++)
	{
	  T->GetEntry(Event);
	  //Make sure we only process events at the current angle
	  if(ID == IDs[position])
	    {
	      //Smear the photons from each PMT for this event using the
	      //SPE function.
	      Double_t SmearedNumPE = 0;
	      for(int photon = 0; photon < Num_PMT_0;photon++)
		{
		  SmearedNumPE += PMT0SPE->GetRandom(); //2009-03-06
		  //SmearedNumPE += 1;
		}
	      for(int photon = 0; photon < Num_PMT_1;photon++)
		{
		  SmearedNumPE += PMT1SPE->GetRandom(); //2009-03-06
		  //SmearedNumPE +=1;
		}
	      

	      //Apply cuts 
	      if( (TOF < TOFMax)   &&
		  (TOF > TOFMin)   &&
		  (Asym < AsymMax) &&
		  (Asym > AsymMin) )
		  //		(IntProc[0] == 'H'))
		{
		  //Histograms to explain the low-e tail
		  if(IntScat == 1)
		    {
		      if(ExtScat == 0)
			{
			  EnergySpectrum10->Fill(SmearedNumPE/5.0);
			  EDepSpectrum10->Fill(Edep);
			}
		      else
			{
			  EnergySpectrum1n->Fill(SmearedNumPE/5.0,1.0);
			  EDepSpectrum1n->Fill(Edep);
			}
		    }
		  else
		    {
		      EnergySpectrumnn->Fill(SmearedNumPE/5.0,1.0);
		      EDepSpectrumnn->Fill(Edep);
		    }
		}
	      else
		{
		  //Other events, not used for analysis, but could be useful to have
		  EnergySpectrumO->Fill(SmearedNumPE/5.0);
		  EDepSpectrumO->Fill(Edep);
		}
	    }
	}


      //new way of smearing the data

//      TH1F * SmearedEnergySpectrum10 = SmearSpectrum(EnergySpectrum10,1-0.3);  
//      TH1F * SmearedEnergySpectrum1n = SmearSpectrum(EnergySpectrum1n,1-0.3);  
//      TH1F * SmearedEnergySpectrumnn = SmearSpectrum(EnergySpectrumnn,1-0.3);  
//      TH1F * SmearedEnergySpectrumO  = SmearSpectrum(EnergySpectrumO ,1-0.3);  

//      TH1F * SmearedEnergySpectrum10 = SmearSpectrum(EnergySpectrum10,0.1);  
//      TH1F * SmearedEnergySpectrum1n = SmearSpectrum(EnergySpectrum1n,0.1);  
//      TH1F * SmearedEnergySpectrumnn = SmearSpectrum(EnergySpectrumnn,0.1);  
//      TH1F * SmearedEnergySpectrumO  = SmearSpectrum(EnergySpectrumO ,0.1);  

//      TH1F * SmearedEnergySpectrum10 = SmearSpectrum(EnergySpectrum10,5.0*1.00);  //2009-03-06
//      TH1F * SmearedEnergySpectrum1n = SmearSpectrum(EnergySpectrum1n,5.0*1.00);  //2009-03-06
//      TH1F * SmearedEnergySpectrumnn = SmearSpectrum(EnergySpectrumnn,5.0*1.00);  //2009-03-06
//      TH1F * SmearedEnergySpectrumO  = SmearSpectrum(EnergySpectrumO ,5.0*1.00);  //2009-03-06

      //No smearing!
      TH1F * SmearedEnergySpectrum10 = (TH1F*) EnergySpectrum10->Clone((std::string("Smeared") + std::string(EnergySpectrum10->GetName())).c_str());
      TH1F * SmearedEnergySpectrum1n = (TH1F*) EnergySpectrum1n->Clone((std::string("Smeared") + std::string(EnergySpectrum1n->GetName())).c_str());
      TH1F * SmearedEnergySpectrumnn = (TH1F*) EnergySpectrumnn->Clone((std::string("Smeared") + std::string(EnergySpectrumnn->GetName())).c_str());
      TH1F * SmearedEnergySpectrumO  = (TH1F*) EnergySpectrumO->Clone ((std::string("Smeared") + std::string(EnergySpectrumO ->GetName())).c_str());



      //Build the spline's smeared energy function from the smeared scatter differentiated histograms     //      TH1F * SmearedEnergySpectrum = SmearSpectrum(EnergySpectrum,5.0);
      sprintf(Buffer,"Pos_%02d",position);
      TH1F * SmearedEnergySpectrum = new TH1F(Buffer,Buffer,400,0,800);
      SmearedEnergySpectrum->GetXaxis()->SetTitle("Recoil Energy (keVee)");
      SmearedEnergySpectrum->Add(SmearedEnergySpectrum10);
      SmearedEnergySpectrum->Add(SmearedEnergySpectrum1n);
      SmearedEnergySpectrum->Add(SmearedEnergySpectrumnn);


      //Build the normalized histogram used for the spline
      sprintf(Buffer,"%02d_NeutronSpectrum",position);
      TH1F * htemp = (TH1F*) SmearedEnergySpectrum->Clone(Buffer);
     

      if(position < 4) //25keVee
	{
	  htemp->Rebin(2);
	  //	  htemp->Smooth(1);
	}
      else if(position < 8) //50keVee
	{
	  htemp->Rebin(4);
	}
      else if(position < 13) //90
	htemp->Rebin(4);
      else if(position < 16) //190
	htemp->Rebin(5);
      else
	htemp->Rebin(8);


      htemp->Smooth(1);
      htemp->Scale(1.0/htemp->Integral());


      sprintf(Buffer,"%02d_Neutrons.root",position);
      TFile * OutFile = TFile::Open(Buffer,"CREATE");
      if(!OutFile)
	{
	  fprintf(stderr,"File already exists");
	  return(0);
	}

      SmearedEnergySpectrum->Write();
      delete SmearedEnergySpectrum;
      htemp->Write(); // dont' delete since we are also writing it to another file

      //Unsmeared Energy spectrum from stacked histograms
      sprintf(Buffer,"EnergySpectrumStack_pos%02d",position);
      THStack * EnergyStack = new THStack(Buffer,Buffer);      
      EnergyStack->Add(EnergySpectrum10);
      EnergyStack->Add(EnergySpectrum1n);
      EnergyStack->Add(EnergySpectrumnn);
      //      EnergyStack->Add(EnergySpectrumO ); 
      TLegend * EnergyLegend = new TLegend(0.6,0.6,0.95,0.95);
      sprintf(Buffer,"Energy spectrum pos%02d",position);
      EnergyLegend->SetHeader(Buffer);
      EnergyLegend->SetBorderSize(1);
      EnergyLegend->SetFillColor(0);
      EnergyLegend->AddEntry(EnergySpectrum10,"(1 int. : 0 ext.) scatters","f");
      EnergyLegend->AddEntry(EnergySpectrum1n,"(1 int. : N ext.) scatters","f");
      EnergyLegend->AddEntry(EnergySpectrumnn,"(M int. : N ext.) scatters","f");
      //      EnergyLegend->AddEntry(EnergySpectrumO ,"Other","f");
      EnergySpectrum10->GetListOfFunctions()->AddLast(EnergyLegend); //Since THStack doesn't have a function list, add the legend to the first histogram in the THStack.
      EnergySpectrum10->Write(); //write the stack's histograms to disk. 
      EnergySpectrum1n->Write(); //write the stack's histograms to disk. 
      EnergySpectrumnn->Write(); //write the stack's histograms to disk. 
      EnergySpectrumO->Write();  //write the stack's histograms to disk. 
      EnergyStack->Write();      // Write the histogram stack
      delete EnergySpectrum10;
      delete EnergySpectrum1n;
      delete EnergySpectrumnn;
      delete EnergySpectrumO; 
      delete EnergyStack;     



      //Smeared Energy spectrum from stacked histograms
      sprintf(Buffer,"SmearedEnergySpectrumStack_pos%02d",position);
      THStack * SmearedEnergyStack = new THStack(Buffer,Buffer);      
      SmearedEnergyStack->Add(SmearedEnergySpectrum10);
      SmearedEnergyStack->Add(SmearedEnergySpectrum1n);
      SmearedEnergyStack->Add(SmearedEnergySpectrumnn);
      //      SmearedEnergyStack->Add(SmearedEnergySpectrumO ); 
      TLegend * SmearedEnergyLegend = new TLegend(0.6,0.6,0.95,0.95);
      sprintf(Buffer,"Smeared Energy spectrum pos%02d",position);
      SmearedEnergyLegend->SetHeader(Buffer);
      SmearedEnergyLegend->SetBorderSize(1);
      SmearedEnergyLegend->SetFillColor(0);
      SmearedEnergyLegend->AddEntry(SmearedEnergySpectrum10,"(1 int. : 0 ext.) scatters","f");
      SmearedEnergyLegend->AddEntry(SmearedEnergySpectrum1n,"(1 int. : N ext.) scatters","f");
      SmearedEnergyLegend->AddEntry(SmearedEnergySpectrumnn,"(M int. : N ext.) scatters","f");
      //      SmearedEnergyLegend->AddEntry(SmearedEnergySpectrumO ,"Other","f");
      SmearedEnergySpectrum10->GetListOfFunctions()->AddLast(SmearedEnergyLegend); //Since THStack doesn't have a function list, add the legend to the first histogram in the THStack.
      SmearedEnergySpectrum10->Write(); //write the stack's histograms to disk. 
      SmearedEnergySpectrum1n->Write(); //write the stack's histograms to disk. 
      SmearedEnergySpectrumnn->Write(); //write the stack's histograms to disk. 
      SmearedEnergySpectrumO->Write();  //write the stack's histograms to disk. 
      SmearedEnergyStack->Write();      // Write the histogram stack
//      delete SmearedEnergySpectrum10;
//      delete SmearedEnergySpectrum1n;
//      delete SmearedEnergySpectrumnn;
//      delete SmearedEnergySpectrumO; 
      delete SmearedEnergyStack;     




      //TOF histos from stacked histograms
      sprintf(Buffer,"TOF_pos%02d",position);
      THStack * TOFStack = new THStack(Buffer,Buffer);      
      TOFStack->Add(hTOF10);
      TOFStack->Add(hTOF1n);
      TOFStack->Add(hTOFnn);
      TOFStack->Add(hTOFO); 
      TLegend * TOFLegend = new TLegend(0.5,0.5,0.9,0.9);
      TOFLegend->SetBorderSize(1);
      TOFLegend->SetFillColor(0);
      TOFLegend->AddEntry(hTOF10,"(1 int. : 0 ext.) scatters","f");
      TOFLegend->AddEntry(hTOF1n,"(1 int. : N ext.) scatters","f");
      TOFLegend->AddEntry(hTOFnn,"(M int. : N ext.) scatters","f");
      TOFLegend->AddEntry(hTOFO ,"Other","f");
      hTOF10->GetListOfFunctions()->AddLast(TOFLegend); //Since THStack doesn't have a function list, add the legend to the first histogram in the THStack.
      hTOF10->Write();   //write the stack's histograms to disk. 
      hTOF1n->Write();   //write the stack's histograms to disk. 
      hTOFnn->Write();   //write the stack's histograms to disk. 
      hTOFO->Write();    //write the stack's histograms to disk. 
      TOFStack->Write(); // Write the histogram stack


      //Write the Edep histograms to the individual recoil files
      EDepSpectrum10->Write();
      EDepSpectrumnn->Write();
      EDepSpectrum1n->Write();
      EDepSpectrumO->Write();

      
      //Save these histograms in a separate file for the actual spline fitting.
      Splines->Add(htemp);
      Splines->Add(hTOF10);
      Splines->Add(hTOF1n);
      Splines->Add(hTOFnn);
      Splines->Add(hTOFO);
      Splines->Add(TOFStack);

      Splines->Add(SmearedEnergySpectrum10);  
      Splines->Add(SmearedEnergySpectrum1n);  
      Splines->Add(SmearedEnergySpectrumnn);  
      Splines->Add(SmearedEnergySpectrumO );  


      
      OutFile->Close();
      delete OutFile;


    }
  delete Buffer;
  TFile * OutFile = TFile::Open("NeutronSplines.root","CREATE");
  if(!OutFile)
    {
      fprintf(stderr,"File already exists");
      return(0);
    }
  Splines->Write();
  OutFile->Close();
  delete OutFile;
  return(0);
}
