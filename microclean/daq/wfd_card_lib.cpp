#include <iostream>

#include "wfd_card_lib.h"
#include "vme_crate_lib.h"

using namespace std;

WFD_Card::WFD_Card()
{
}

WFD_Card::~WFD_Card()
{
}

int WFD_Card::setupWFD(VME_Crate *Crate_in,int slot_in,u_int32_t f0ADER_in,u_int32_t f1ADER_in,int personality_in, int WindowSize0, int WindowSize1, int WindowSize2, int WindowSize3,u_int8_t Offset0,u_int8_t Offset1,u_int8_t Offset2,u_int8_t Offset3)
{
  u_int8_t bytes[4];
  Crate = Crate_in;
  slot = slot_in;
  f0ADER = f0ADER_in;
  f1ADER = f1ADER_in;
  scale = 1;
  personality = personality_in;

  WindowSize[3] = WindowSize3;
  WindowSize[2]	= WindowSize2;
  WindowSize[1]	= WindowSize1;
  WindowSize[0] = WindowSize0;
  
  //Set Windows
  
  //WFD wants window in #microseconds which is windowsize divived by 125.  see
  //See vmedaq.cpp to see the definition of windowsize sent to setupWFD
  WindowSize3 /= 125;
  WindowSize2 /= 125;
  WindowSize1 /= 125;
  WindowSize0 /= 125;

  Crate->geowrite(slot,0x7fb53,WindowSize3);
  Crate->geowrite(slot,0x7fb4f,WindowSize2);
  Crate->geowrite(slot,0x7fb4b,WindowSize1);
  Crate->geowrite(slot,0x7fb47,WindowSize0);
  cout << "Windows " << hex 
       << (int) WindowSize3 << "\t"
       << (int) WindowSize2 << "\t" 
       << (int) WindowSize1 << "\t"
       << (int) WindowSize0 << "\t"
       << dec << endl;
  cout << "Check = " ;
  u_int8_t Check_window = 0;
  Crate->georead(slot,0x7fb53,&Check_window);
  cout << hex << (int) Check_window << "\t";
  Crate->georead(slot,0x7fb4f,&Check_window);
  cout << hex << (int) Check_window << "\t";
  Crate->georead(slot,0x7fb4b,&Check_window);
  cout << hex << (int) Check_window << "\t";
  Crate->georead(slot,0x7fb47,&Check_window);
  cout << hex << (int) Check_window << "\t";
  cout    << dec << endl;
  

 //Write bit 7 of the bit clear register 0x7fff7      
  Crate->geowrite(slot,0x7fff7, 0x40);
  cout << " .";
  
  //Write bit 5 of the bit set register ??doc says 4??        
  Crate->geowrite(slot,0x7fffb,0x10);
  cout << ".";

  // Write 0 to CSRctr0 and CSRctr1.  I don't know what these do, but    
  // I'm supposed to do it anyway.   
  Crate->geowrite(slot, 0x7fbc7, 0);
  Crate->geowrite(slot, 0x7fbcb, 0);
  cout << "." ;
  
  //set ader0 
  getbytes(f0ADER,bytes);
  Crate->geowrite(slot, 0x7ff63, bytes[0]);
  Crate->geowrite(slot, 0x7ff67, bytes[1]);
  Crate->geowrite(slot, 0x7ff6b, bytes[2]);
  Crate->geowrite(slot, 0x7ff6f, bytes[3]);
  cout <<".";
  
  //set ader1                          
  getbytes(f1ADER, bytes);
  Crate->geowrite(slot, 0x7ff73, bytes[0]);
  Crate->geowrite(slot, 0x7ff77, bytes[1]);
  Crate->geowrite(slot, 0x7ff7b, bytes[2]);
  Crate->geowrite(slot, 0x7ff7f, bytes[3]);
  cout << ".";


   //Set personality   --oscope mode is 0x01    
  //  Crate->geowrite(slot,0x7fbff,personality);
  Crate->geowrite(slot,0x7fbff,1);
  cout << ".";

  
  //Set DAC offset
  Crate->geowrite(slot,0x7fbc3,Offset3);
  Crate->geowrite(slot,0x7fbbf,Offset2);
  Crate->geowrite(slot,0x7fbbb,Offset1);
  Crate->geowrite(slot,0x7fbb7,Offset0);
  
  //Crate->geowrite(slot,0x7fbc3,0x09);
  //Crate->geowrite(slot,0x7fbbf,0x00);
  //Crate->geowrite(slot,0x7fbbb,0x06);
  //Crate->geowrite(slot,0x7fbb7,0x00);
  
  
  //in oscope mode datablockcounts + presamples give a 16 bit number used to 
  //define how many samples to keep. The value ranges from 0 to 1024 
  //(>1024 = 1024)   
  getbytes(scale,bytes);

  //Data Block count registers channels 1-4                   
  Crate->geowrite(slot, 0x7fb77,bytes[2]);
  Crate->geowrite(slot, 0x7fb7b,bytes[2]);
  Crate->geowrite(slot, 0x7fb7f,bytes[2]);
  Crate->geowrite(slot, 0x7fb83,bytes[2]);
  cout << ".";
  
  //presamples channesl 1-4                         
  Crate->geowrite(slot, 0x7fb87,bytes[3]);
  Crate->geowrite(slot, 0x7fb8b,bytes[3]);
  Crate->geowrite(slot, 0x7fb8f,bytes[3]);
  Crate->geowrite(slot, 0x7fb93,bytes[3]);
  cout << ".";
  
  // set all the channels to trigger untied           
  Crate->geowrite(slot, 0x7fbeb, 0x21);
  Crate->geowrite(slot, 0x7fbef, 0x84);
  cout << ".";
  
  // enable channels for writing.         
  Crate->geowrite(slot,0x7fbe7, 0xf0);
  cout << ".";

  //reset FIFOs      
  resetfifo(5);
  cout << ". Done\n";
  return(0);
}

int WFD_Card::getslot()
{
  return(slot);
}

u_int32_t WFD_Card::getf0ADER()
{
  return(f0ADER);
}
u_int32_t WFD_Card::getf1ADER()
{
  return(f1ADER);
}

int WFD_Card::getcardID()
{
  return(cardID);
}

int WFD_Card::getpersonality()
{
  return(personality);
}

int WFD_Card::start()
{
  return(Crate->geowrite(slot,0x7fbe3,0x1));
}

int WFD_Card::stop()
{
  return(Crate->geowrite(slot,0x7fbe3,0x2));
}

//-------------------------------------------------------------------------
// Converts 32bit number into 4 8bit numbers
//-------------------------------------------------------------------------
void WFD_Card::getbytes(u_int32_t word, u_int8_t bytes[4])
{
  bytes[0] = (word>>24) & 0xff;
  bytes[1] = (word>>16) & 0xff;
  bytes[2] = (word>>8)  & 0xff;
  bytes[3] =  word      & 0xff;
}

int WFD_Card::read_data(int channel,u_int32_t block_count, u_int32_t* data)
{
  int ret;
  u_int32_t addr;
  switch(channel)
    {
    case 0 :
      addr = f1ADER;
      break;
    case 1:
      addr = f1ADER + 0x08;
      break;
    case 2:
      addr = f1ADER + 0x10;
      break;
    case 3:
      addr = f1ADER + 0x18;
      break;
    default:
      cout << "Bad channel number/n";
      return(-2);
      break;
    }
  ret = Crate->a32mblt64read(addr,data,block_count);
  return(ret);
}

int WFD_Card::resetfifo(int channel)
{
  int ret = 0;
  switch (channel)
    {
    case 1:
      ret = Crate->geowrite(slot,0x7fbf3,0x01);
      break;
    case 2:
      ret = Crate->geowrite(slot,0x7fbf3,0x02);
      break;
    case 3:
      ret = Crate->geowrite(slot,0x7fbf3,0x04);
      break;
    case 4:
      ret = Crate->geowrite(slot,0x7fbf3,0x08);
      break;
    case 5: // reset all channels
      ret = Crate->geowrite(slot,0x7fbf3,0x0f);
      break;
    default:
      ret = -1;
      break;
    }
  return(ret);
}

int WFD_Card::blockcount(int channel)
{ 
  int ret = 0;
  u_int32_t data;
  u_int32_t addr;
  switch (channel)
    {
    case 0 :
      addr = f0ADER;
      break;
    case 1:
      addr = f0ADER + 0x04;
      break;
    case 2:
      addr = f0ADER + 0x08;
      break;
    case 3:
      addr = f0ADER + 0x0C;
      break;
    default:
      cout << "Bad channel number\n";
      return(-2);
      break;
    }
  ret = Crate->a32d32read(addr,&data);
  //  cout << "Channel " << channel << " blockcount: " << data << endl;
  if(data > 0)
    {
      ret = data;
    }
  return(ret);
}
int WFD_Card::getWindowSize(int i)
{
  if(i < 4)
    {
      return(WindowSize[i]);
    }
  return(-1);
}
void WFD_Card::setWindow(int i,u_int32_t * pointer)
{
  if(i < 4)
    {
      Window[i] = pointer;
    }
}
u_int32_t * WFD_Card::getWindow(int i)
{
  if(i < 4)
    {
      return(Window[i]);
    }
  return(0);
}
void WFD_Card::setChanID(int i, int id)
{
  if(i < 4)
    {
      ChanID[i] = id;
    }
}
int WFD_Card::getChanID(int i)
{
  if(i < 4)
    {
      return(ChanID[i]);
    }
  return(-1);
}
