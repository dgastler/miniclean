#include <iostream>
#include "PEvent.h"
#include "Event.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TextArray.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TF1.h"
#include "xmlParser.h"


using namespace std;

Float_t Volts;
Float_t seconds;
Float_t C;


Float_t Integrate(Float_t * Y,Int_t Start, Int_t End,Float_t Sigma,TH2F * h1)
//This function performs the integral of the waveform from Start to End time
//steps.   It also fills the 2-D histogram h1 that will be used for find the
//single photo-electron (pulse integral and pulse height) for this waveform. 
{
  Float_t SPE = 0;
  Float_t Height = 0;
  Int_t after = 35;//40;  //Time after threashold to start integrating pulse x2ns for time
  //  Int_t before = 5;//0; //Time before threashold to integrate the pulse
  Float_t Integral = 0;
  Sigma *= 2; //1;
  Int_t LastPoint = Start;
  for(int i = Start; i < End;i++)
    {
      //      if(Y[i] > 5*Sigma) //Old
      //	Integral += Y[i]; //Old
      //If the waveform goes above threshold
      if(Y[i] > Sigma)
	{
	  //initially set the integral window to after
	  Int_t Length = after;
	  //Go back one step if the the last pulse window was a while ago
	  if(i - LastPoint > 1)
	    {
	      i--;
	    }
	  for(int j = i; (j < End && j < i + after && j > 0) ;j++)
	    {
	      // stop integrating if the waveform is less than zero
	      // also skip this check if we are on the first sample
	      // otherwise we could get stuck in a loop.
	      if(Y[j] < 0 && Y[j+1] < 0 && j > i)
		{
		  Length = j-i;
		  break;
		}
	      //Find the max height
	      if(Y[j] > Height)
		{
		  Height = Y[j];
		}
	      SPE += Y[j];
	    }
	  //move i since we were advancing in j
	  i+=Length - 1;
	  //	  SPE *= C;
	  //Add window to integral if the height was greater than 3 (11mv)
	  if(Height > 3)
	    Integral += SPE;
	  Height *= Volts;
	  if(h1 != 0)
	    h1->Fill(SPE*C,Height);
	  SPE = 0;
	  Height = -100;
	  LastPoint = i;
	}
    }
  return(Integral*C);
}



Int_t Find_Waveform_Maximum(Int_t ChannelSize, Float_t * Y)
{
  //2;
  Float_t MaxY = 0; //3;
  Int_t   MaxYlocation = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Y[i] > MaxY)
	{
	  MaxY = Y[i];
	  MaxYlocation = i;
	}
    }
  return(MaxYlocation);
}

Int_t Find_Waveform_Start(Int_t ChannelSize, Float_t * Y)
{
  Float_t MaxY = 0;
  Int_t   MaxYlocation = 1;
  for(Int_t i = 1; i < ChannelSize;i++)
    {
      if(Y[i] > MaxY)
	{
	  MaxY = Y[i];
	  MaxYlocation = i;
	}
    }
  //Find 20% peak. 
  for(Int_t i = 0; MaxYlocation + i > 0;i--)
    {
      if(5.0*Y[MaxYlocation + i] < MaxY)
	return(MaxYlocation + i);
    }
  return(0);
}


Float_t MergeWaveforms(Int_t ChannelSize, Float_t * Ya,Float_t *Yb,Float_t Y_Offset)
{
  //4;
  Float_t start = 0;
  Float_t end = 0;
  Float_t deltaT = 0;
  Float_t trip = 200-Y_Offset;
  //  for(int i = 0; i < ChannelSize;i++)
  //Find position of channel a hitting 200-Y_Offset on the way up and down.
  for(int i = 1; i < ChannelSize;i++)
    {
      if((Ya[i] > trip)&&(start == 0))
	{
	  start = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) + i - 1;
	}
      if((Ya[i] < trip)&&(start !=0))
	{
	  end = (trip - Ya[i-1])/(Ya[i] - Ya[i-1]) +i - 1;
	  i = ChannelSize;
	}
      
    }
  
  deltaT = end - start;
  
  //Find B channel MAXes
  Int_t ChannelB_MAX = 1;
  for(int i = 1; i < ChannelSize;i++)
    {
      if(Yb[i] >= Yb[ChannelB_MAX])
	{
	  ChannelB_MAX = i;
	}
      if((i == 2*ChannelB_MAX)&&(Yb[ChannelB_MAX] > 5))
	break;
    }
  
  Int_t Tf = ChannelB_MAX;
  Int_t Ti = Tf-1;
  
  while( Tf - Ti < deltaT )
    {
      if(((Tf >= 0) && (Tf < ChannelSize)) && ((Ti >= 0) && (Ti < ChannelSize)))
      //      if ( Yb[Tf] > Yb[Ti] )
	{
	  if(Yb[Tf] > Yb[Ti])
	    {
	      Tf++;
	    }
	  else
	    {
	      Ti--;
	    }
	}
      else
	{
	  break;
	  Ti = 0xefffffff;
	  Tf = 0xefffffff;
	}
    }

	
  Float_t shift;
  shift = (start - Ti + end - Tf)/2.0;
  
  Float_t Gain = 10;
  for(int i = 0;i < ChannelSize-1;i++)
    {
      if((i - (int)shift) > 0 && (i - (int)shift) < ChannelSize)
	{
	  if(Ya[i] >= 255 - ceil(Y_Offset) && Gain*Yb[i-(int)shift] > Ya[i])
	    {
	      Ya[i] = Gain * Yb[i-(int)shift];
	    }
	}
    }
  
  return(shift);
}





int main(int argc, char **argv)
{
  Volts = (1.0/255.0);
  seconds = 2e-9;
  C = Volts * seconds * (1.0/50.0);

  //  signal(SIGSEGV,signal_handler);
  if(argc < 5)
    {
      cout << argv[0] << " InFile_base Ni Nf OutFile" << endl;
      return(0);
    }
  TFile   *    InFile = 0;
  TTree   *    tree = 0;
  Event   *    event = 0;
  TTree   *    inTextTree = 0;
  TextArray *  inRootText = 0;
  Char_t  *    filename = new Char_t[200];
  Char_t  *    filebase = argv[1];
  Int_t        ni = atoi(argv[2]);
  Int_t        nf = atoi(argv[3]);


  //  Char_t  *    OutDirectory = argv[4];
  TFile   *    OutFile = new TFile(argv[4],"recreate");
  OutFile->SetCompressionLevel(9);
  PEvent  *    pevent = new PEvent;
  TTree   *    ptree = new TTree("ptree","Run");
  TTree   *    TextTree = new TTree("TextTree","RunNotes");
  TextArray *  RootText = new TextArray;//new TextArray;
  ptree->SetAutoSave(100000);
  ptree->Branch("PEvent branch","PEvent",&pevent);//,64000,0);
  TextTree->Branch("Run Notes","TextArray",&RootText);
  

  Int_t        Size  = 3;
  Int_t        TotalEvents = 0;

  Int_t TotalEventNumber = 0;

  Int_t Skipped = 0;


  //Raw level Histograms
  TH1F * PMT0Charge  = new TH1F("PMT0Charge","PMT0 Charge",100,0,1e-9);
  PMT0Charge->GetXaxis()->SetTitle("Charge(C)");

  TH1F * PMT1Charge  = new TH1F("PMT1Charge","PMT1 Charge",100,0,1e-9);
  PMT0Charge->GetXaxis()->SetTitle("Charge(C)");

  TH1F * TotalCharge = new TH1F("TotalCharge","Total Charge",100,0,1e-9);
  TotalCharge->GetXaxis()->SetTitle("Charge(C)");

  TH1F * PMTasym     = new TH1F("PMTasym","PMT asymmetry",100,-1,1);

  TH2F * PMT0SPE     = new TH2F("PMT0SPE","PMT SPE",300,0,300*C,300,0,300*Volts);
  PMT0SPE->GetXaxis()->SetTitle("Charge(C)");
  PMT0SPE->GetYaxis()->SetTitle("Volts");

  TH2F * PMT1SPE     = new TH2F("PMT1SPE","PMT SPE",300,0,300*C,300,0,300*Volts);
  PMT1SPE->GetXaxis()->SetTitle("Charge(C)");
  PMT1SPE->GetYaxis()->SetTitle("Volts");
 
//  //To be generated later from PMT0SPE
//  TH1D *SPE0   = 0;
//  TH1D *SPE1   = 0;
//  TH1D *SPEV0  = 0;
//  TH1D *SPEV1  = 0;
//
//  Double_t PMT0_V = 0;
//  Double_t PMT1_V = 0;

  //File loop
  for(int filenumber = ni;filenumber <= nf; filenumber++)
    {
      //Open file in of type "base"filenumber.root
      sprintf(filename,"%s%.4i.root",filebase,filenumber);
      cout << "Opening file: " << filename << endl;
      InFile                 = new TFile(filename,"READ","",9);
      tree                   = (TTree*) InFile->Get("tree");
      inTextTree               = (TTree*) InFile->Get("TextTree");
      event                  = 0;
      inRootText             = 0;
      Int_t NEvents          = tree->GetEntries();
      tree->SetBranchAddress("Event branch",&event);
      inTextTree->SetBranchAddress("Run Notes",&inRootText);
      tree->GetEntry(0);
      
      Int_t ChannelSize                    = event->GetSize(0);
      Float_t * Channel1                   = new Float_t[ChannelSize];
      Float_t * Channel1b                  = new Float_t[ChannelSize];
      Float_t * Channel2                   = new Float_t[ChannelSize];
      Float_t * Channel2b                  = new Float_t[ChannelSize];
      Float_t * Channel3                   = new Float_t[ChannelSize];
      
      //Loop over events in file
      for(int EventN = 0; EventN < NEvents; EventN++)
      	{
	  pevent->SetSize(Size);
	  pevent->EventNumber(TotalEventNumber);
	  TotalEventNumber++;

	  tree->GetEntry(EventN);

	  bool FixGain[2]                      = {false,false};
	  Int_t ChannelSize                    = event->GetSize(0);
	  
	  Float_t Channel1_Offset              = 0;
	  Float_t Channel1b_Offset             = 0;
	  Float_t Channel2_Offset              = 0;
	  Float_t Channel2b_Offset             = 0;
	  Float_t Channel3_Offset              = 0;
	  
	  Float_t Channel1_OffsetSquared       = 0;
	  Float_t Channel1b_OffsetSquared      = 0;
	  Float_t Channel2_OffsetSquared       = 0;
	  Float_t Channel2b_OffsetSquared      = 0;
	  Float_t Channel3_OffsetSquared       = 0;
	  
	  Float_t Sigma_Channel1_Offset        = 0;
	  Float_t Sigma_Channel1b_Offset       = 0;
	  Float_t Sigma_Channel2_Offset        = 0;
	  Float_t Sigma_Channel2b_Offset       = 0;
	  Float_t Sigma_Channel3_Offset        = 0;
	  
	  bool Chan3 = false;
	  if(event->NRaw() > 4)
	    {
	      Chan3 = true;
	    }
	  
	  
	  //Read in Waveform and check if it needs to be merged with the second
	  // gain scale.

	  
	  for(int i = 0; i < 1000;i++)
	    {
	      Channel1_Offset  += event->GetPoint(0,i);
	      Channel1b_Offset += event->GetPoint(1,i);
	      Channel2_Offset  += event->GetPoint(2,i);
	      Channel2b_Offset += event->GetPoint(3,i);
	      
	      Channel1_OffsetSquared  += event->GetPoint(0,i)*event->GetPoint(0,i);
	      Channel1b_OffsetSquared  += event->GetPoint(1,i)*event->GetPoint(1,i);
	      Channel2_OffsetSquared  += event->GetPoint(2,i)*event->GetPoint(2,i);
	      Channel2b_OffsetSquared  += event->GetPoint(3,i)*event->GetPoint(3,i);
	      
	      if(Chan3)
		{
		  Channel3_Offset += event->GetPoint(4,i);
		  Channel3_OffsetSquared  += event->GetPoint(4,i)*event->GetPoint(4,i);
		}
	    }
	  
	  Sigma_Channel1_Offset    = sqrt((Channel1_OffsetSquared - Channel1_Offset*Channel1_Offset/1000)/(1000-1));
	  Sigma_Channel1b_Offset   = sqrt((Channel1b_OffsetSquared - Channel1b_Offset*Channel1b_Offset/1000)/(1000-1));
	  Sigma_Channel2_Offset    = sqrt((Channel2_OffsetSquared - Channel2_Offset*Channel2_Offset/1000)/(1000-1));
	  Sigma_Channel2b_Offset   = sqrt((Channel2b_OffsetSquared - Channel2b_Offset*Channel2b_Offset/1000)/(1000-1));
	  Sigma_Channel3_Offset    = sqrt((Channel3_OffsetSquared - Channel3_Offset*Channel3_Offset/1000)/(1000-1));
	  
	  Channel1_Offset  /= 1000.0;
	  Channel1b_Offset /= 1000.0;
	  Channel2_Offset  /= 1000.0;
	  Channel2b_Offset /= 1000.0;
	  Channel3_Offset /= 1000.0;
	  
	  for(int i = 0; i < ChannelSize;i++)
	    {
	      Channel1[i] = event->GetPoint(0,i);
	      Channel1b[i] = event->GetPoint(1,i);
	      Channel2[i] = event->GetPoint(2,i);
	      Channel2b[i] = event->GetPoint(3,i);
	      if(Chan3)
		Channel3[i] = event->GetPoint(4,i);
	      
	      if(Channel1[i] >= 254)  //==255)
		{
		  FixGain[0] = true;
		}
	      if(Channel2[i] >= 254)
		{
		  FixGain[1] = true;
		}
	      
	      Channel1[i]  -= Channel1_Offset;
	      Channel1b[i] -= Channel1b_Offset;
	      Channel2[i]  -= Channel2_Offset;
	      Channel2b[i] -= Channel2b_Offset;
	      if(Chan3)
		Channel3[i] -= Channel3_Offset;
	    }
	  
	  if(FixGain[0])
	    {
	      Float_t Shift = MergeWaveforms(ChannelSize,Channel1,Channel1b,Channel1_Offset);
	      pevent->Shift(0,Shift);
	    }
	  else
	    pevent->Shift(0,-1);
	  if(FixGain[1])
	    {
	      Float_t Shift = MergeWaveforms(ChannelSize,Channel2,Channel2b,Channel2_Offset);
	      pevent->Shift(1,Shift);
	      //	      cout << pevent->Shift(0) << endl;
	    }
	  else 
	    pevent->Shift(0,-1);
	  
	  Channel1b[0] = Channel1[0];
	  Channel2b[0] = Channel2[0];
	  Double_t RC = 1.0 + 0.00002;
	  RC = 1;
	  bool ValidEvent = true;
	  for(int i = 0; i < ChannelSize-1;i++)
	    {
	      Channel1b[i+1] = Channel1b[i] - Channel1[i] + Double_t(Channel1[i+1])*RC;
	      Channel2b[i+1] = Channel2b[i] - Channel2[i] + Double_t(Channel2[i+1])*RC;
	      if(Channel1b[i+1] > 250 && i > float(ChannelSize)/2.0)
		{
		  //		  ValidEvent = false; 
		}
	      if(Channel2b[i+1] > 250 && i > float(ChannelSize)/2.0)
		{
		  //		  ValidEvent = false; 
		}

	    }
	  
	  
	  if(Chan3)
	    {
	      //Delay Time   TOF!
	      Float_t Delay_PMT1 = (2e-6)*(Find_Waveform_Start(ChannelSize,Channel1b) - Find_Waveform_Start(ChannelSize,Channel3));
	      Float_t Delay_PMT2 = (2e-6)*(Find_Waveform_Start(ChannelSize,Channel2b) - Find_Waveform_Start(ChannelSize,Channel3));
	      
	      //	      Float_t Delay_Time = (Delay_PMT1 + Delay_PMT2)/2;
	      pevent->TOF(0,Delay_PMT1);
	      pevent->TOF(1,Delay_PMT2);
	      //pevent->TOFAve = Delay_Time;
	      //cout << Delay_Time << "\t" ;
	    }
	  else
	    {
	      pevent->TOF(0,0);
	      pevent->TOF(1,0);
	      //pevent->TOFAve = 0;
	    }
	  
	  Int_t Peak[3];
	  Peak[0] = Find_Waveform_Start(ChannelSize,Channel1b);
	  Peak[1] = Find_Waveform_Start(ChannelSize,Channel2b);
	  if(Chan3)
	    {
	      Peak[2] = Find_Waveform_Start(ChannelSize,Channel3);
	    }
	  pevent->Delay(0,Peak[0]);
	  pevent->Delay(1,Peak[1]);
	  pevent->MaxHeight(0,Channel1[Find_Waveform_Maximum(ChannelSize,Channel1b)]);
	  pevent->MaxHeight(1,Channel2[Find_Waveform_Maximum(ChannelSize,Channel2b)]);
	  if(Chan3)
	    {
	      pevent->MaxHeight(2,Channel3[Find_Waveform_Maximum(ChannelSize,Channel3)]);
	      pevent->Delay(2,Peak[2]);
	    }
	  //      cout << Peak[0] << "\t" << Peak[1] << endl;
	  Int_t Before = -10; //Number of points before the peak to integrate
	  Int_t After = 50; //Number of Points after the peak to integrate


	  Float_t FastPMT0[5] = {0,0,0,0,0}; 
	  Float_t FastPMT1[5] = {0,0,0,0,0};
	  
	  Float_t SlowPMT0[5] = {0,0,0,0,0}; 
	  Float_t SlowPMT1[5] = {0,0,0,0,0}; 

	  Float_t FastPMT2 = 0;
	  Float_t SlowPMT2 = 0;
	  
	  //Split the prompt range into five bins.
	  // 15 * ( i -2)
	  //first one is fixed
	  FastPMT0[0] = Integrate(Channel1b,Peak[0] + Before,Peak[0] + After + 15*(0 - 1),1.0,0);//Sigma_Channel1_Offset);
	  FastPMT1[0] = Integrate(Channel2b,Peak[1] + Before,Peak[1] + After + 15*(0 - 1),1.0,0);//,Sigma_Channel2_Offset);
	  SlowPMT0[0] = Integrate(Channel1b,Peak[0] + After + 15*(0 - 1),ChannelSize,1.0,PMT0SPE);//Sigma_Channel1_Offset);
	  SlowPMT1[0] = Integrate(Channel2b,Peak[1] + After + 15*(0 - 1),ChannelSize,1.0,PMT1SPE);//Sigma_Channel2_Offset);
	  pevent->Fast(0,0,FastPMT0[0]);
	  pevent->Fast(1,0,FastPMT1[0]);
	  pevent->Slow(0,0,SlowPMT0[0]);
	  pevent->Slow(1,0,SlowPMT1[0]);


	  for(int PromptT = 1; PromptT < 5; PromptT++)
	    {
	      FastPMT0[PromptT] = Integrate(Channel1b,Peak[0] + Before,Peak[0] + After + 15*(PromptT - 1),1.0,0);//Sigma_Channel1_Offset);
	      FastPMT1[PromptT] = Integrate(Channel2b,Peak[1] + Before,Peak[1] + After + 15*(PromptT - 1),1.0,0);//Sigma_Channel1_Offset);
	      SlowPMT0[PromptT] = Integrate(Channel1b,Peak[0] + After + 15*(PromptT - 1),ChannelSize,1.0,PMT0SPE);//Sigma_Channel1_Offset);
	      SlowPMT1[PromptT] = Integrate(Channel2b,Peak[1] + After + 15*(PromptT - 1),ChannelSize,1.0,PMT1SPE);//Sigma_Channel2_Offset);
	      pevent->Fast(0,PromptT,FastPMT0[PromptT]);
	      pevent->Fast(1,PromptT,FastPMT1[PromptT]);
	      pevent->Slow(0,PromptT,SlowPMT0[PromptT]);
	      pevent->Slow(1,PromptT,SlowPMT1[PromptT]);
	    }

	  
	  //cout << FastPMT0 << " " << SlowPMT0 << endl;
	  FastPMT2 = 0;
	  SlowPMT2 = 0;
	  
	  if(Chan3)
	    {
	      FastPMT2 = Integrate(Channel3,Peak[2] + Before,Peak[2] + After,1.0,0);//Sigma_Channel3_Offset);
	      SlowPMT2 = Integrate(Channel3,Peak[2] + After,ChannelSize,1.0,0);//Sigma_Channel3_Offset);
	    }
	  
	  pevent->AboveTHRS(false);
	  
	  if(Chan3)
	    {
	      pevent->Fast(2,0,FastPMT2);
	      pevent->Slow(2,0,SlowPMT2);
	    }
	  
	  //These are some basic plots that can be used to set bounds on histograms in reduction2
	  
	  PMT0Charge->Fill((FastPMT0[0] + SlowPMT0[0]));
	  PMT1Charge->Fill((FastPMT1[0] + SlowPMT1[0]));
	  TotalCharge->Fill((FastPMT0[0] + SlowPMT0[0] + FastPMT1[0] + SlowPMT1[0]));
	  PMTasym->Fill((FastPMT0[0] + SlowPMT0[0] - FastPMT1[0] - SlowPMT1[0])/(FastPMT0[0] + SlowPMT0[0] + FastPMT1[0] + SlowPMT1[0]));    
	  
	  //	  pevent->TOFAve = 5;
	  if(ValidEvent)
	    {
	      ptree->Fill();
	      TotalEvents++;
	    }
	  else
	    Skipped++;
	  pevent->Clear("C");
	}
      delete [] Channel1;
      delete [] Channel1b;
      delete [] Channel2 ;
      delete [] Channel2b;
      delete [] Channel3 ;

      cout << "Closing file : " << filename << endl;
      //If this is the last file take it's DAQ xml files and put them into the out file
      if(filenumber == ni)
	{
	  inTextTree->GetEntry(0);
	  char * TextTemp = inRootText->GetArray();
	  int TextTempSize = 0;
	  while(true)
	    {
	      if(TextTemp[TextTempSize] == '<' &&
		 TextTemp[TextTempSize+1] == '/' &&
		 TextTemp[TextTempSize+2] == 'D' &&
		 TextTemp[TextTempSize+3] == 'A' &&
		 TextTemp[TextTempSize+4] == 'Q' &&
		 TextTemp[TextTempSize+5] == '>')
		{
		  TextTempSize +=7;
		  break;
		}
	      else
		TextTempSize++;
	      if(TextTempSize > 10000)
		{
		  cerr << "stuck in loop" << endl;
		  return(0);
		}
		 
	    }
	  char * TextTemp2 = new char[TextTempSize];
	  for(int k = 0; k < TextTempSize -1;k++)
	    {
	      TextTemp2[k] = TextTemp[k];
	    }
	  TextTemp2[TextTempSize-1] = '\0';

//	  XMLNode TempNode=XMLNode::parseString(TextTemp,"DAQ");
//	  TextTemp = TempNode.createXMLString(1,&TextTempSize);
	  RootText->FillArray(TextTempSize,TextTemp2);
	  TextTree->Fill();
	  //	  cout << endl << TextTemp2 << endl;
	  delete [] TextTemp2;


	  inTextTree->GetEntry(1);
	  TextTemp = inRootText->GetArray();
	  TextTempSize = 0;
	  while(true)
	    {
	      if(TextTemp[TextTempSize] == '<' &&
		 TextTemp[TextTempSize+1] == '/' &&
		 TextTemp[TextTempSize+2] == 'D' &&
		 TextTemp[TextTempSize+3] == 'A' &&
		 TextTemp[TextTempSize+4] == 'Q' &&
		 TextTemp[TextTempSize+5] == '>')
		{
		  TextTempSize +=7;
		  break;
		}
	      else
		TextTempSize++;
	      if(TextTempSize > 10000)
		{
		  cerr << "stuck in loop" << endl;
		  return(0);
		}
	    }
	  TextTemp2 = new char[TextTempSize];
	  for(int k = 0; k < TextTempSize -1;k++)
	    {
	      TextTemp2[k] = TextTemp[k];
	    }
	  TextTemp2[TextTempSize-1] = '\0';
	  //	  XMLNode TempNode2=XMLNode::parseString(TextTemp,"DAQ");
	  //	  TextTemp = TempNode2.createXMLString(1,&TextTempSize);
	  RootText->FillArray(TextTempSize,TextTemp2);
	  TextTree->Fill();
	  //	  cout << endl << TextTemp2 << endl;
	  delete [] TextTemp2;
	}
      InFile->Close("R");
      //      delete InFile;
    }
  //  OutFile->Write();
//  OutFile->cd();
//  PMT0Charge->Write(); 
//  PMT1Charge->Write(); 
//  TotalCharge->Write(); 
//  PMTasym->Write(); 
//  PMT0SPE->Write();     
//  PMT1SPE->Write();     


  OutFile->Write();
  OutFile->Close("R");
  cout << "Skipped events = " << Skipped << endl;
  return(0);
}
