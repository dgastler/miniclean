set ylabel "Documentation (pages)"
set xdata time
set timefmt "%s"
set format x "%Y/%m"

plot "progress.dat" using 1:2 with lines title "Page count"
set term pdf size 10,6 lw 8
set output 'progress.pdf'
replot
