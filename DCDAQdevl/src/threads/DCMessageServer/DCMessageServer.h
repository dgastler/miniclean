#ifndef __DCMESSAGESERVER__
#define __DCMESSAGESERVER__

#include <DCThread.h>
#include <DCNetThread.h>

#include <ServerConnection.h>
#include <Connection.h>
#include <netHelper.h>

class DCMessageServer : public DCThread 
{
 public:
  DCMessageServer(std::string Name);
  ~DCMessageServer();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);  
  virtual void MainLoop();
 private:

  void ProcessChildMessage(unsigned int iConn);

  //Process messages in/out
  void RouteRemoteDCMessage(DCNetThread * Thread);
  bool RouteLocalDCMessage(DCMessage::Message &message);
  bool SendMessage(DCNetThread * netThread,DCMessage::Message &message);

  //Handle connections
  bool ProcessNewConnection();
  void DeleteConnection(unsigned int iConn);
  ServerConnection server;

  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;

  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  //  time_t HACK;
  //  int HACKos;
};


#endif
