#ifdef __RATDS__

#include <DecisionMaker.h>
#include <RAT/DS/ReductionLevel.hh>

DecisionMaker::DecisionMaker(std::string Name)
{
  Ready = false;
  SetType("DecisionMaker");
  SetName(Name);
  softwareTrigger = NULL;
  reductionLevelCount.resize(ReductionLevel::COUNT,0);
  triggerCount.resize(HardwareTrigger::COUNT,0);
  currentTime = time(NULL);
  lastTime = currentTime;
  statfprompt = 0;
  statpromptQ = 0;
  statearliestWindowTime = 0;
  statlatestWindowTime = 0;
  stattotalQ = 0;
  rawEventRate = 0;
  channelZLEWindows = NULL;
}

DecisionMaker::~DecisionMaker()
{
  //Write and reset fprompt vs. Q histogram
  TFile * histFile = TFile::Open(".status/QvFprompt.root","RECREATE","",1);
  if((histFile != NULL) && (histFile->IsOpen()))
    {
      histFile->cd();
      totalQvFprompt->Write();
      for(size_t iHist = 0; iHist < reductionQvFprompt.size();iHist++)
	{
	  if(reductionQvFprompt[iHist]!=NULL)
	    {
	      reductionQvFprompt[iHist]->Write();
	      reductionQvFprompt[iHist]->Reset();
	    }
	}
      histFile->Write();
      histFile->Close();
      totalQvFprompt->Reset();      
    }
}

bool DecisionMaker::Setup(xmlNode * SetupNode,
			  std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;

  channelZLEWindows = new TH1F("channelZLEWindows","Channel ZLE Windows",0,100,100);
  for(int i = 0; i < ReductionLevel::COUNT;i++)
    {
      std::stringstream ss;
      ss << "redLevel_" << i << "_QvFp";
      reductionQvFprompt[i] = new TH2F(ss.str().c_str(),
				       ss.str().c_str(),
				       1000,-1E6,1E6,  //charge 
				       60,0,1);    //fprompt
      
      if(reductionQvFprompt[i] == NULL)
	return false;
    }
  totalQvFprompt = new TH2F("Total_QvFp","Total_QvFp",1000,-1E6,1E6,60,0,1);


  //==============================================================
  //Load the DRPC memory manager
  //==============================================================
  if(!FindMemoryManager(DRPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }

  if(FindSubNode(SetupNode,"CHECKHEADERS") == NULL)
    {
      checkHeaders = 1;
    }
  else
    GetXMLValue(SetupNode,"CHECKHEADERS",GetName().c_str(),checkHeaders);

  //Load hardware trigger settings

  for(int trig = 0; trig < HardwareTrigger::COUNT; trig++)
    {
      HardwareTrigger::ReductionSetting reductionSetting;
      xmlNode * triggerNode = FindSubNode(SetupNode,HardwareTrigger::GetTypeName(HardwareTrigger::Type(trig)));
      if(triggerNode == NULL)
	{
	  reductionSetting.reductionLevel = ReductionLevel::CHAN_ZLE_WAVEFORM;
	  reductionSetting.softwareTrigger = false;
	}
      else
	{
	  if(FindSubNode(triggerNode,"Level") == NULL)
	    {
	      reductionSetting.reductionLevel = ReductionLevel::CHAN_ZLE_WAVEFORM; 
	    }
	  else
	    {
	      GetXMLValue(triggerNode,"Level",GetName().c_str(),reductionSetting.reductionLevel);
	    }
	  if(FindSubNode(triggerNode,"SoftwareTrigger") == NULL)
	    reductionSetting.softwareTrigger = false;
	  else
	    {
	      reductionSetting.softwareTrigger = true;
	    }
	}
      reductionSettings.push_back(reductionSetting);
    }
  
  //==============================================================
  //Load parameters for the software trigger and create it. 
  //==============================================================
  DRSettings triggerSettings;
  xmlNode * DRSettingsNode = FindSubNode(SetupNode,"DRSettings");
  if((DRSettingsNode == NULL) || !SetDRSettings(DRSettingsNode,triggerSettings))
      return false;
  
  softwareTrigger= new SoftwareTrigger(triggerSettings);
  if(softwareTrigger == NULL)
    {
      Ready = false;
      ret = false;
    }

 
  //If everything in setup worked, then set Ready to true;
  if(ret == true)
    {
      Ready = true;
    }
  return(ret);
}

bool DecisionMaker::SetDRSettings(xmlNode * Node, DRSettings &settings)
{
  if(FindSubNode(Node,"triggerMode") == NULL)		    
    {return false;}
  GetXMLValue(Node,"triggerMode","DecisionMaker",settings.triggerMode);
  if(FindSubNode(Node,"dataFormat") == NULL)		    
    {return false;}
  GetXMLValue(Node,"dataFormat","DecisionMaker",settings.dataFormat);
  if(FindSubNode(Node,"intQthresh") == NULL)		    
    {return false;}
  GetXMLValue(Node,"intQthresh","DecisionMaker",settings.intQthresh);
  if(FindSubNode(Node,"promptQthresh") == NULL)		    
    {return false;}
  GetXMLValue(Node,"promptQthresh","DecisionMaker",settings.promptQthresh);
  if(FindSubNode(Node,"maxQthresh") == NULL)
    {return false;}
  GetXMLValue(Node,"maxQthresh","DecisionMaker",settings.maxQthresh);
  if(FindSubNode(Node,"prescale") == NULL)
    {return false;}
  GetXMLValue(Node,"prescale","DecisionMaker",settings.prescale);
  if(FindSubNode(Node,"prescaleIntQ") == NULL)
    {return false;}
  GetXMLValue(Node,"prescaleIntQ","DecisionMaker",settings.prescaleIntQ);
  if(FindSubNode(Node,"prescalePromptQ") == NULL)
    {return false;}
  GetXMLValue(Node,"prescalePromptQ","DecisionMaker",settings.prescalePromptQ);
  if(FindSubNode(Node,"prescaleMaxQ") == NULL)
    {return false;}
  GetXMLValue(Node,"prescaleMaxQ","DecisionMaker",settings.prescaleMaxQ);
  if(FindSubNode(Node,"presamples") == NULL)
    {return false;}

  settings.zlePresamples.resize(NumberOfNamedSubNodes(Node,"zlePresamples"));
  for(size_t iNode = 0;iNode < settings.zlePresamples.size();iNode++)
    {
      GetIthXMLValue(Node,iNode,
		     "zlePresamples",
		     "DecisionMaker",
		     settings.zlePresamples[iNode]);
    }
  
  settings.zlePostsamples.resize(NumberOfNamedSubNodes(Node,"zlePostsamples"));
  for(size_t iNode = 0;iNode < settings.zlePostsamples.size();iNode++)
    {
      GetIthXMLValue(Node,iNode,
		     "zlePostsamples",
		     "DecisionMaker",
		     settings.zlePostsamples[iNode]);
    }

  settings.channelType.resize(NumberOfNamedSubNodes(Node,"channelType"));
  for(size_t iNode = 0;iNode < settings.channelType.size();iNode++)
    {
      GetIthXMLValue(Node,iNode,
		     "channelType",
		     "DecisionMaker",
		     settings.channelType[iNode]);
    }

  GetXMLValue(Node,"presamples","DecisionMaker",settings.presamples);
  if(FindSubNode(Node,"promptPresamples") == NULL)
    {return false;}
  GetXMLValue(Node,"promptPresamples","DecisionMaker",settings.promptPresamples);
  if(FindSubNode(Node,"promptPostsamples") == NULL)
    {return false;}
  GetXMLValue(Node,"promptPostsamples","DecisionMaker",settings.promptPostsamples);
  
  return true;
}


void DecisionMaker::ProcessTimeout()
{
  //Get current Time
  currentTime = time(NULL);
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCtime_t TimeStep = currentTime-lastTime;
      StatMessage::StatRate ssRate;
      DCMessage::Message message;


      //Send raw Event rate
      message.SetType(DCMessage::STATISTIC);
      message.SetDestination();
      
      ssRate.sBase.type = StatMessage::REDUCTION_RATE;
      ssRate.count = rawEventRate;
      ssRate.sInfo.time = currentTime;
      ssRate.sInfo.interval = TimeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::RAW_EVENT;
      ssRate.units = Units::NUMBER;
      
      message.SetData(ssRate);
      SendMessageOut(message);
      rawEventRate = 0;

      //Send all reduction level event rates.
      for(size_t iLevel = 0; 
	  iLevel < reductionLevelCount.size();
	  iLevel++)
	{
	  message.SetType(DCMessage::STATISTIC);
	  message.SetDestination();

	  ssRate.sBase.type = StatMessage::REDUCTION_RATE;
	  ssRate.count = reductionLevelCount[iLevel];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::BLANK;

	  switch (iLevel)
	    {
	    case ReductionLevel::CHAN_FULL_WAVEFORM :
	      ssRate.sInfo.level = Level::CHAN_FULL_WAVEFORM;
	      break;
	    case ReductionLevel::CHAN_ZLE_WAVEFORM  :
	      ssRate.sInfo.level = Level::CHAN_ZLE_WAVEFORM;
	      break;
	    case ReductionLevel::CHAN_ZLE_INTEGRAL  :
	      ssRate.sInfo.level = Level::CHAN_ZLE_INTEGRAL;
	      break;
	    case ReductionLevel::CHAN_PROMPT_TOTAL  :
	      ssRate.sInfo.level = Level::CHAN_PROMPT_TOTAL;
	      break;
	    case ReductionLevel::EVNT_FULL_WAVEFORM :
	      ssRate.sInfo.level = Level::EVNT_FULL_WAVEFORM;
	      break;
	    case ReductionLevel::EVNT_ZLE_WAVEFORM  :
	      ssRate.sInfo.level = Level::EVNT_ZLE_WAVEFORM;
	      break;
	    case ReductionLevel::EVNT_ZLE_INTEGRAL  :
	      ssRate.sInfo.level = Level::EVNT_ZLE_INTEGRAL;
	      break;
	    case ReductionLevel::EVNT_PROMPT_TOTAL  :
	      ssRate.sInfo.level = Level::EVNT_PROMPT_TOTAL;
	    case ReductionLevel::SAVE_NOTHING  :
	      ssRate.sInfo.level = Level::SAVE_NOTHING;
	      break;
		
	    default:
	      ssRate.sInfo.level = Level::UNKNOWN;
	      break;
	    }
	  ssRate.units = Units::NUMBER;

	  message.SetData(ssRate);
	  SendMessageOut(message);
	  reductionLevelCount[iLevel] = 0;
	}

      //Send all the hardware level trigger rates
      for(size_t iTrigger = 0; iTrigger < triggerCount.size(); iTrigger++)
	{
	  message.SetType(DCMessage::STATISTIC);
	  message.SetDestination();

	  ssRate.sBase.type = StatMessage::TRIGGER_RATE;
	  ssRate.count = triggerCount[iTrigger];
	  ssRate.sInfo.time = currentTime;
	  ssRate.sInfo.interval = TimeStep;
	  ssRate.sInfo.subID = SubID::BLANK;

	  switch(iTrigger)
	    {
	    case HardwareTrigger::VETO : 
	      ssRate.sInfo.level = Level::VETO;
	      break;
	    case HardwareTrigger::EVNT :
	      ssRate.sInfo.level = Level::EVNT;
	      break;
	    case HardwareTrigger::COMP :
	      ssRate.sInfo.level = Level::COMP;
	      break;
	    case HardwareTrigger::BUTN :
	      ssRate.sInfo.level = Level::BUTN;
	      break;
	    case HardwareTrigger::CAL0 :
	      ssRate.sInfo.level = Level::CAL0;
	      break;
	    case HardwareTrigger::CAL1 :
	      ssRate.sInfo.level = Level::CAL1;
	      break;
	    case HardwareTrigger::CAL2 :
	      ssRate.sInfo.level = Level::CAL2;
	      break;
	    case HardwareTrigger::PERI :
	      ssRate.sInfo.level = Level::PERI;
	      break;

	    default:
	      ssRate.sInfo.level = Level::UNKNOWN;
	      break;
	    }
	
	  ssRate.units = Units::NUMBER;

	  message.SetData(ssRate);
	  SendMessageOut(message);
	  triggerCount[iTrigger] = 0;
	}

      channelZLEWindows->Scale(1.0/double(TimeStep));
//      DCMessage::DCTObject rootObj;
//      std::vector<uint8_t> dataTemp;
//      dataTemp = rootObj.SetData((TObject * )(channelZLEWindows),DCMessage::DCTObject::H1F);
//      message.SetDestination();
//      message.SetType(DCMessage::HISTOGRAM);
//      message.SetData(&(dataTemp[0]),dataTemp.size());
//      message.SetTime(currentTime);
//      channelZLEWindows->Reset();
//      SendMessageOut(message,false);
//      std::map<int,TH2F*>::iterator redIt;      
//      for(redIt  = reductionQvFprompt.begin();
//	  redIt != reductionQvFprompt.end();
//	  redIt++)
//	{	  
//	  dataTemp = rootObj.SetData((TObject * )(redIt->second),DCMessage::DCTObject::H2F);
//	  message.SetDestination();
//	  message.SetType(DCMessage::HISTOGRAM);
//	  message.SetData(&(dataTemp[0]),dataTemp.size());
//	  message.SetTime(currentTime);
//	  printf("Sending histogram %s with size %zu\n",redIt->second->GetTitle(),dataTemp.size());
//	  redIt->second->Reset();
//	  SendMessageOut(message,false);
//	}

      //DCMessage::SingleDouble  singleDouble;
      //sprintf(singleDouble.text,"Average fprompt: ");
      //singleDouble.value = statfprompt/fpromptCount;
      //message.SetDataStruct(singleDouble);
      //message.SetType(DCMessage::STATISTIC);
      //SendMessageOut(message,false);
      statfprompt = 0;

      //sprintf(singleDouble.text,"Average Earliest Window Time: ");
      //singleDouble.value = statearliestWindowTime/fpromptCount;
      //message.SetDataStruct(singleDouble);
      //message.SetType(DCMessage::STATISTIC);
      //SendMessageOut(message,false);
      statearliestWindowTime = 0;
      
      //sprintf(singleDouble.text,"Average Latest Window Time: ");
      //singleDouble.value = statlatestWindowTime/fpromptCount;
      //message.SetDataStruct(singleDouble);
      //message.SetType(DCMessage::STATISTIC);
      //SendMessageOut(message,false);
      statlatestWindowTime = 0;
      
      //sprintf(singleDouble.text,"Average Total Charge: ");
      //singleDouble.value = stattotalQ/fpromptCount;
      //message.SetDataStruct(singleDouble);
      //message.SetType(DCMessage::STATISTIC);
      //SendMessageOut(message,false);
      stattotalQ = 0;
      
      //sprintf(singleDouble.text,"Average Prompt Charge: ");
      //singleDouble.value = statpromptQ/fpromptCount;
      //message.SetDataStruct(singleDouble);
      //message.SetType(DCMessage::STATISTIC);
      //SendMessageOut(message,false);
      statpromptQ = 0;
      
      fpromptCount=0;


   
      //Write and reset fprompt vs. Q histogram
      TFile * histFile = TFile::Open(".status/QvFprompt.root","RECREATE","",1);
      if((histFile != NULL) && (histFile->IsOpen()))
	{
	  histFile->cd();
	  totalQvFprompt->Write();
	  for(size_t iHist = 0; iHist < reductionQvFprompt.size();iHist++)
	    {
	      reductionQvFprompt[iHist]->Write();
	      reductionQvFprompt[iHist]->Reset();
	    }
	  histFile->Write();
	  histFile->Close();
	  totalQvFprompt->Reset();      
	}
      else
	{
	  PrintError("Could not open histFile!\n");
	}

      //record time
      lastTime = currentTime;
    }
}

uint8_t DecisionMaker::DecodeGrayCode(uint8_t in)
{
  uint8_t byte = in;
  //Convert LVDS pattern from Gray code
  for(size_t i = 1;i < 8;i++)
    {
      in = in >> 1;
      byte ^= in;
    }
  return byte;
}
#endif
