#ifndef __EVNT_PROMPT_TOTAL__
#define __EVNT_PROMPT_TOTAL__

#include <PacketBuilder.h>

class EVNT_PROMPT_TOTAL : public PacketBuilder
{
 public:
  EVNT_PROMPT_TOTAL()
  {
    ClearPacket();
  };
  virtual ~EVNT_PROMPT_TOTAL(){ClearPacket();};
  virtual bool Send(FEPCBlock * event)
  {
    printf("EVNT_PROMPT_TOTAL not written.  FAIL!\n");
    return(false);
  }

};

#endif
