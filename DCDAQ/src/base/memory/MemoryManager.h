#ifndef __MEMORYMANAGER__
#define __MEMORYMANAGER__

#include <LockedQueue.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <string>

class MemoryManager
{
 public:
  MemoryManager(){};
  virtual ~MemoryManager() {Deallocate();};
  virtual bool AllocateMemory(xmlNode * SetupNode) {return(false);};

  //Name get functions
  std::string GetManagerName() {return ManagerName;};
  std::string GetManagerType() {return ManagerType;};

 protected:
  void SetName(std::string _Name){ManagerName = _Name;};
  void SetType(std::string _Type){ManagerType = _Type;};
  
 private:
  std::string ManagerName;
  std::string ManagerType;
  virtual void Deallocate(){};
};

#endif
