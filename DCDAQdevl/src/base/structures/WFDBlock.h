#ifndef __WFDBLOCKS__
#define __WFDBLOCKS__

#include "RAT/DS/EV.hh"

struct WFDBlock
{
  RAT::DS::EV * array;
  uint32_t allocatedSize;
  uint32_t usedSize;
};
#endif
