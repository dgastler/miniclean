#ifdef __RATDS__

#ifndef __FEPCRATMANAGER__
#define __FEPCRATMANAGER__
#include <math.h>

#include <MemoryManager.h>
#include <xmlHelper.h>

#include <DCVector.h>
#include <DCQueue.h>
#include <BadEventStruct.h>
#include <StatMessage.h>
#include <DRDecisionPacket.h>

#include <RAT/DS/DCDAQ_RATEVBlock.hh>
#include <v1720Event.h>

#include <TFile.h>
#include <TH1F.h>
#include <TLegend.h>
#include <TCanvas.h>

#include <iostream>
#include <sstream>
#include <string>

#include <DetectorInfo.h>

//Not implimented yet!
////for error handling.  This means if we have a collision, we move the
////older event to the bad queue.  If we dont' define this, then we are going
////to move the newer event. 
//#define MOVE_OLD_EVENT


//#define USE_EVENT_TIME_FOR_KEY 
class FEPCRATManager : public MemoryManager
{
 public:
  //================================================================
  //Memory manager setup
  //================================================================
  FEPCRATManager(std::string Name);
  ~FEPCRATManager();
  bool AllocateMemory(xmlNode * setupNode);

#ifndef USE_EVENT_TIME_FOR_KEY
  int64_t GetKey(v1720Event & wfdEvent,DetectorInfo & info)
  {
    //Event #
    return wfdEvent.GetWFDEventID();
  };
  int64_t GetKey(RATEVBlock * block)
  {
    return block->wfdEventID;
  };
  int64_t GetKey(DRDecisionPacket::Event & decision)
  {
    return decision.wfdEventID;
  }


#else

  int64_t GetKey(v1720Event & wfdEvent,DetectorInfo & info)
  {    
#ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"FEPCRatManager::GetKey(BoardID=0x%X,WFDEvent=0x%X,Time=0x%X)\n",
	    wfdEvent.GetBoardID(),
	    wfdEvent.GetWFDEventID(),
	    wfdEvent.GetWFDTimeStamp());
#endif 

    //Time
    return wfdEvent.GetWFDTimeStamp(info.GetInternalDelay(wfdEvent.GetBoardID()));
  };
  int64_t GetKey(RATEVBlock * block)
  {
    return block->eventTime;
  };
  int64_t GetKey(DRDecisionPacket::Event & decision)
  {
    return decision.eventTime;
  }

#endif

  uint64_t GetBlankPassword(){return StorageEvent.GetBlankPassword();};
  
  //================================================================
  //Mutex control
  //================================================================
  //Wake up any mutex locks on contained objects
  virtual void WakeUp();
  //Shutdown the mutex waits in contained objects
  virtual void Shutdown();
  //Get the shutdown status
  virtual bool IsShutdown() {return(ShutdownStatus);};

  //================================================================
  //Memory manager status
  //================================================================
  virtual void PrintStatus(int indent = 0, int verbosity = 0);
  virtual void PrintVectorHash();

  //================================================================
  //File descriptor access
  //================================================================
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetUnSentFDs(){return(UnSent.GetFDs());};
  const int * GetToSendFDs(){return(Send.GetFDs());};
  const int * GetBadEventFDs(){return(BadEvent.GetFDs());};

  //================================================================
  //Memory access
  //================================================================
  //Get the defined bad value
  int32_t GetBadValue(){return badValue;};

  //Block access
  RATEVBlock * GetFEPCBlock(uint32_t iFEPC) 
  {
    if(iFEPC < Memory.size())
      {
	return(&(Memory[iFEPC]));
      }
    return(NULL);
  };  
  
  //================================================================
  //Index access
  //================================================================

  //================================================================
  //AssembledEvent access  (RATAssembler)
  //================================================================
  //Gets an event from the AssembledEvent vector-hash. Detects collisions
  int64_t GetAssembledEvent(int64_t eventKey,
			    int32_t &index,
			    uint64_t &password);
  //Returns and clears PW for the event at index. (does not molest index if there is a collision)
  int64_t ReturnAssembledEvent(int32_t &index,uint64_t password);
  int64_t MoveAssembledEventToUnSentQueue(int64_t eventKey,uint64_t password);  
  int64_t MoveAssembledEventToBadQueue(int64_t eventKey,
				       uint64_t password,
				       Level::Type errorCode);
  uint64_t GetAssembledBlankPassword(){return AssembledEvent.GetBlankPassword();};
  //================================================================

  //================================================================
  //Un-sent queue (DRPusher)
  //================================================================
  uint64_t GetStorageBlankPassword(){return StorageEvent.GetBlankPassword();};
  bool     GetUnSent(int32_t &index);
  bool     AddUnSent(int32_t &index);
  int64_t  GetStorageEvent(int64_t eventKey,
			   int32_t &index,
			   uint64_t & password);
  int64_t  ReleaseStorageEvent(int32_t &index,
			       uint64_t & password);
  int64_t  ClearStorageEvent(int64_t eventKey,
			     uint64_t password);
  int64_t  MoveStorageEventToBadQueue(int64_t eventKey,
				      uint64_t password,
				      Level::Type errorCode);
  //================================================================
  
  //================================================================
  //Storage Event access (DRGetter)
  //================================================================
  bool AddSend(int32_t &index);
  
  //================================================================

  //================================================================
  //To-sent queue (EBPusher)
  //================================================================
  bool GetSend(int32_t &index);
  bool AddFree(int32_t &index);
  //================================================================

  //================================================================
  //BadEvent queue
  //================================================================
  bool GetBadEvent(BadEvent::sBadEvent &value);
  bool AddBadEvent(int64_t eventID,
		   int32_t &index,
		   Level::Type errorCode);
  //================================================================
 
 private:   
  //================================================================
  //Data storage and sizes
  //================================================================
  //Memory vector and it's size from the setup XML
  uint32_t NumBlocks;
  std::vector<RATEVBlock> Memory;
  //Size of AssembledEvent vectorhash
  uint32_t AssembledEventSize;
  //Size of StorageEvent vectorhash
  uint32_t StorageEventSize;
  //Shutdown status
  volatile bool ShutdownStatus;

  int32_t badValue;
  int32_t emptyValue;

  //================================================================
  //Statistics
  //================================================================
  std::map<enum TimeStampCode::Code,TH1F*> hTimeStamp;
  void AddTimeStampHistogram(enum TimeStampCode::Code code);
  void PrintTimeStampHistograms(std::string filename);

  //================================================================
  //QUEUES
  //================================================================
  //queue of indices of free
//  DCQueue<int32_t,POSIXQSelect,LockedObject> Free;  
//  //queue of indices of un-sent memory blocks
//  DCQueue<int32_t,POSIXQSelect,LockedObject> UnSent;  
//  //queue of indices to send to the event builder.
//  DCQueue<int32_t,POSIXQSelect,LockedObject> Send; 
//  //queue of indices that have had problems. 
//  DCQueue<BadEvent::sBadEvent,POSIXQSelect,LockedObject> BadEvent; 
  //queue of indices of free
  DCQueue<int32_t,PipeSelect,LockedObject> Free;  
  //queue of indices of un-sent memory blocks
  DCQueue<int32_t,PipeSelect,LockedObject> UnSent;  
  //queue of indices to send to the event builder.
  DCQueue<int32_t,PipeSelect,LockedObject> Send; 
  //queue of indices that have had problems. 
  DCQueue<BadEvent::sBadEvent,PipeSelect,LockedObject> BadEvent; 

  //================================================================
  //Vector hashes
  //================================================================
#ifndef USE_EVENT_TIME_FOR_KEY
  //vector-hash of events being assembled
  DCVector<int32_t,LockedObject,EventNumberHash<int32_t> > AssembledEvent; 
  //vector-hash of stored sent events
  DCVector<int32_t,LockedObject,EventNumberHash<int32_t> > StorageEvent; 
#else
  //vector-hash of events being assembled
  DCVector<int32_t,LockedObject,EventTimeGroup<int32_t> > AssembledEvent; 
  //vector-hash of stored sent events
  DCVector<int32_t,LockedObject,EventTimeGroup<int32_t> > StorageEvent; 
#endif
  //================================================================
  //Deallocation of all the data objects
  //================================================================  
  void Deallocate();
};

#endif

#endif
