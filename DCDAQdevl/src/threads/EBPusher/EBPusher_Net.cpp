#include <EBPusher.h>

bool EBPusher::ProcessNewConnection()
{  
  bool ret = true;
  if(EBAssembler.SetupDCNetThread(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(EBAssembler.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(EBAssembler.thread->GetMessageOutFDs()[0]);      
      EBAssembler.thread->Start(NULL);
      printf("Connected to EBAssembler at: %s\n",EBAssembler.remoteIP.c_str());
      //If Loop is true then we need to add the FEPCRatMemory manager
      //to the select set. 
      //It wouldn't have been allowed to be added in the absence of a 
      //net thread connection
      if(Loop)
	{
	  AddReadFD(FEPC_mem->GetToSendFDs()[0]);      
	}
      SetupSelect();
      //Setup the PacketBuilders
      for(size_t i = 0; i < packetBuilder.size();i++)
	{
	  packetBuilder[i]->SetPacketManager(EBAssembler.thread->GetOutPacketManager());
	}
    }
  else
    {
      ret = false;
    }
  return(ret);
}
void EBPusher::DeleteConnection()
{
  //Making sure that the thread actually exists
  if(EBAssembler.thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(EBAssembler.thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(EBAssembler.thread->GetMessageOutFDs()[0]);
    }
  //See if we are actually connected
  if(EBAssembler.fdSocket != BadPipeFD)
    {
      //remove any selecting on fdSocket
      RemoveReadFD(EBAssembler.fdSocket);
      RemoveWriteFD(EBAssembler.fdSocket);
    }  

  //Shutdown the thread
  EBAssembler.Shutdown();

  //stop listening to the FEPC memory manager, since we can't process anymore events
  RemoveReadFD(FEPC_mem->GetToSendFDs()[0]);
  SetupSelect();
}

void EBPusher::ProcessNetThreadMessage()
{
  DCMessage::Message message = EBAssembler.thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::ERROR)
    {
      //Shut down our current thread
      DeleteConnection();
      //Start trying to get a new connection
      StartNewConnection();
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      SendMessageOut(message,true);
    }
  else
    {
      PrintWarning("Unknown message from DCMessageClient's DCNetThread\n");
    }
}


//Start an asynchronous socket and check if the connection immediatly works. 
//When it doesn't, then if there is also no error (fd == -1), add the socket
//to the read/write fd_sets. 
//returns true if either we connected or are waiting for a connection
//return false if something critical failed.
bool EBPusher::StartNewConnection()
{
  if(EBAssembler.AsyncConnectStart())
    {
      //Connection worked. 
      if(ProcessNewConnection())
	{
	  return(true);
	}
      else
	{
	  PrintError("Failed creating DCNetThread!");
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}

    }
  else 
    {
      if(EBAssembler.fdSocket >= 0)
	{
	  //Connection in progress (add fd to select screen)
	  AddReadFD (EBAssembler.fdSocket);
	  AddWriteFD(EBAssembler.fdSocket);
	  SetupSelect();
	}
      else
	{
	  PrintError("Failed getting socket for DCNetThread!");
	  //If we can't get a socket then something is really wrong. 
	  //We should fail!
	  DCMessage::Message message;
	  message.SetType(DCMessage::STOP);
	  SendMessageOut(message,true);
	  return(false);
	}
    }
  return(true);
}
