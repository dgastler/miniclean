#ifndef __XMLHELPER__
#define __XMLHELPER__

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string>
#include <cstring>
#include <vector>
#include <fstream>
#include <stdint.h> //uint*_t

bool      LoadFile(const std::string & filename , std::string &text);
char    * GetNodeText(xmlNode * baseNode);
int       SetNodeText(xmlNode * baseNode,const char * text);

xmlNode * FindIthSubNode(xmlNode * baseNode, const char * Name,int i);
xmlNode * FindIthSubNode(xmlNode * baseNode,int i);
xmlNode * FindSubNode(xmlNode * baseNode, const char * Name);

unsigned int       NumberOfNamedSubNodes(xmlNode * baseNode, const char * Name);
unsigned int       NumberOfSubNodes(xmlNode * baseNode);

xmlNode * AddNewSubNode(xmlNode * baseNode,const char * Name);


int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,uint32_t & reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,int32_t & reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,int16_t & reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,uint16_t & reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,int8_t & reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,uint8_t & reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,std::string &reg);
int GetIthXMLValue(xmlNode * baseNode,int i, const char * Name,const char * ErrorBase,double &reg);

template<class T>
int GetXMLValue(xmlNode * baseNode, const char * Name,const char * ErrorBase,T&t)
{
  return GetIthXMLValue(baseNode,0, Name, ErrorBase, t);
}

#endif
