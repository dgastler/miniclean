#include <MemBlockWriter.h>

MemBlockWriter::MemBlockWriter(std::string Name)
{
  SetName(Name);
  SetType(std::string("MemBlockWriter"));
  BlocksRead = 0;
  TimeStep_BlocksRead = 0;
  last = time(NULL);
  current = time(NULL);
  outfile = NULL;
  SubRunNumber = 0;
}

MemBlockWriter::~MemBlockWriter()
{
}

bool MemBlockWriter::Setup(xmlNode * SetupNode,
			   std::vector<MemoryManager*> &MemManager)
{
  if(!FindMemoryManager(Block_mem,SetupNode,MemManager))
    {
      return false;
    }
  Block_mem->IncrementListeningThreads();
  if(FindSubNode(SetupNode,"WRITEMODE") == NULL)
    {
      writeMode = 0;
    }
  else
    GetXMLValue(SetupNode,"WRITEMODE",GetName().c_str(),writeMode);
  if(FindSubNode(SetupNode,"BLOCKSPERFILE") == NULL)
    {
      blocksPerFile = 100;
    }
  else
    GetXMLValue(SetupNode,"BLOCKSPERFILE",GetName().c_str(),blocksPerFile);

  if(FindSubNode(SetupNode,"OUTPUTDIRECTORY") == NULL)
    {
      outputDirectory = "";
      fprintf(stderr,"Writing to current directory\n");
      PrintWarning("Writing to current directory");
    }
  else
    {
      GetXMLValue(SetupNode,"OUTPUTDIRECTORY",GetName().c_str(),outputDirectory);
      char * lastchar = &outputDirectory[outputDirectory.size()-1];
      std::string test = "/";
      if(strcmp(lastchar,test.c_str())!=0)
	{
	  outputDirectory.append("/");
	}
      
      //Check if this directory exists
      DIR * outputDirectoryTest = opendir(outputDirectory.c_str());
      if(outputDirectoryTest == NULL)
	{
	  PrintError("Failed to open directory\n");
	  return false;
	}
      closedir(outputDirectoryTest);
      fprintf(stderr,"Directory name: %s\n",outputDirectory.c_str());
    }
  //====================================================================
  //Number of timeouts to wait before shutting down if nothing is being written.
  //This should eventually be removed from DCDAQ and done in RunWatchdog.
  if(FindSubNode(SetupNode,"NOEVENTSTHRESHOLD") == NULL)
    {
      noBlocksThreshold = 10;
      PrintWarning("NoEventsThreshold not set; using default of 10");
    }
  else
    {
      GetXMLValue(SetupNode,"NOEVENTSTHRESHOLD",GetName().c_str(),noBlocksThreshold);
    }
  //====================================================================

  memBlockCounter = 0;
  memBlockIndex = FreeFullQueue::BADVALUE;

  BlockSizeHist = new TH1F("BlockSizeHist","MemBlock size",100,0,1000000);
  BlockSizeFile = TFile::Open("BlockSizeFile.root","RECREATE","",1);
  if(!BlockSizeFile->IsOpen())
    {
      PrintError("Could not open BlockSizeFile!\n");
    }

  Ready = true;
  return (true);
}

void MemBlockWriter::MainLoop()
{
  //Check for a new memory block event
  if(IsReadReady(Block_mem->GetFullFDs()[0]) 
     && (memBlockIndex == FreeFullQueue::BADVALUE))
    {
      //Get a new memory block
      Block_mem->GetFull(memBlockIndex);
      if((memBlockIndex != FreeFullQueue::BADVALUE)&&
	 (memBlockIndex != FreeFullQueue::SHUTDOWN))
	//We have a valid memBlock
	{
	  //Get the memory block for this blockIndex
	  memBlock = Block_mem->GetBlock(memBlockIndex);
	  
	  // //If the memblock is empty, we are shutting down.
	  // if(memBlock->dataSize == 0)
	  //   {
	  //     fclose(outfile);
	  //     outfile = NULL;
	  //     //	      fprintf(stdout,"Closed file\n");
	  //     printf("MemBlockWriter: We are shutting down\n");
	  //     //SendStop(false);
	  //     return;
	  //   }

	  //Check whether there is a file open
	  if(outfile==NULL)
	    {
	     
	      char * nameBuffer = new char[100];
	      sprintf(nameBuffer,"%sMCL_%06d_%06d.dat",outputDirectory.c_str(),GetRunNumber(),SubRunNumber);
	      outfile = fopen(nameBuffer,"w");
	      if(outfile==NULL)
		{
		  PrintError("Unable to open file\n");
		  Running=false;
		}
	      //outfile.open(nameBuffer);
	      delete [] nameBuffer;
	      fileBlockCounter = 0;
	    }

	  //Create File header 

	  blockHeader.sizeMagic= 0xAA;
	  blockHeader.size=memBlock->dataSize;
	 
	  if(memBlock->uncompressedSize>0)
	    {
	      blockHeader.compressionMagic= 0xBB;
	      blockHeader.compressionSize=memBlock->uncompressedSize;
	    }
	  else
	    blockHeader.compressionMagic= 0xCC;

	 
	  if(writeMode==1)
	    //Write in hex
	    {
	      //Write file header
	      uint8_t * ptr=(uint8_t *)&blockHeader;
	      for(size_t i=0;i<sizeof(blockHeader);)
		{
		  for(size_t j=0;(i<sizeof(blockHeader))&&(j<4);j++)
		    {
		      fprintf(outfile,"0x%08X",*ptr);
		      ptr++;
		      i++;
		    }
		  fprintf(outfile,"\n");
		}
		
	     
	     
	      for(uint32_t i = 0; i < memBlock->dataSize;i++)
		{
		  fprintf(outfile,"0x%08X\n",memBlock->buffer[i]);
		}
	    }
	  else
	    //Write in binary
	    {
	      //Write header word
	      size_t fwriteret = fwrite(&blockHeader,sizeof(blockHeader),1,outfile);
	     
	      if(fwriteret!=1)
		{
		  PrintError("Something went wrong writing the header word!");
		}

	      fwriteret = fwrite(memBlock->buffer,sizeof(memBlock->buffer[0]),memBlock->dataSize,outfile);
	      if(fwriteret!=memBlock->dataSize)
		{
		  PrintError("Error in fwrite!  Have you run out of disk space?");
		}
	    }

	  fileBlockCounter++;	
	  dataWordsWritten+=2;
	  dataWordsWritten += memBlock->dataSize;

	  //Add to the block size histogram
	  BlockSizeHist->Fill(memBlock->dataSize);

	  //Check whether we should close this file
	  if(fileBlockCounter>=blocksPerFile)
	    {
	      fclose(outfile);
	      outfile = NULL;
	      SubRunNumber++;
	      Print("Closed file");
	    }
	  
	  //=================================================
	  //Reset block counter. This is temporary.
	  noBlocksCounter = 0;
	  //=================================================
	}
      
      else if(memBlockIndex == FreeFullQueue::SHUTDOWN)
	//This means we should shut down.
	{	  
	  //	  printf("MemBlockWriter: We've been told to shut down\n");
	  Print("We've been told to shut down");
	  //Close the current file
	  fclose(outfile);
	  if(Block_mem->DeIncrementListeningThreads())
	    {
	      Print("Shutting down last MemBlockWriter thread.");
	      //	      printf("We're the last MemBlockWriter thread!\n");
	      SendStop(false);
	    }
	  Loop = false;
	  Running = false;
	  return;
	}

      memBlockCounter++;
      TimeStep_BlocksRead++;
      
      Block_mem->AddFree(memBlockIndex);
      memBlockIndex = FreeFullQueue::BADVALUE;
    }
}

void MemBlockWriter::ProcessTimeout()
{
  //=================================================================
  //Stuff that should eventually be removed (and done in RunWatchdog)
  //Increment noBlocksCounter
  noBlocksCounter++;

  //If we have had too many timeouts without writing a memblock, we should shut down
  if(noBlocksCounter > noBlocksThreshold)
    {
      PrintError("No memblocks are being written.");
    }
  //=================================================================

  current = time(NULL);
  if(current - last > GetUpdateTime())
    {
      DCtime_t timeStep = difftime(current,last);
      DCMessage::Message message;
      StatMessage::StatRate ssRate;

      ssRate.sBase.type = StatMessage::EVENT_RATE;
      ssRate.count = TimeStep_BlocksRead;
      ssRate.sInfo.time = current;
      ssRate.sInfo.interval = timeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.units = Units::NUMBER;
      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);
      
      ssRate.sBase.type = StatMessage::DATA_RATE;
      ssRate.count = dataWordsWritten*4;
      ssRate.sInfo.time = current;
      ssRate.sInfo.interval = timeStep;
      ssRate.sInfo.subID = SubID::BLANK;
      ssRate.sInfo.level = Level::BLANK;
      ssRate.units = Units::NUMBER;
      message.SetData(ssRate);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message);

      TimeStep_BlocksRead = 0;
      dataWordsWritten = 0;
      last = current;
 
//      //Send block size histogram
//      DCMessage::DCTObject rootObj;
//      std::vector<uint8_t> dataTemp;
//      dataTemp = rootObj.SetData((TObject * )(BlockSizeHist),DCMessage::DCTObject::H1F);
//      message.SetDestination();
//      message.SetType(DCMessage::HISTOGRAM);
//      message.SetData(&(dataTemp[0]),dataTemp.size());
//      message.SetTime(current);
//      BlockSizeHist->Reset();
//      SendMessageOut(message,false);
    }
}

bool MemBlockWriter::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the Full queue's FD so we know when we have a new 
      //memory block to process
      AddReadFD(Block_mem->GetFullFDs()[0]); //Add the FD to this threads mask
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}
