#ifndef __DCTHREAD__
#define __DCTHREAD__

#include <iostream>
#include <string>
#include <map>
#include <algorithm> //for std::sort

#include <pthread.h>

#include <LockedQueue.h>
#include <DCMessage.h>
#include <MemoryManager.h>
#include <ThreadStart.h>  

#include <timeHelper.h> // for printing time

#include <libxml/parser.h>
#include <libxml/tree.h>

//Value to check against for bad pipe
const int BadPipeFD = -1;

/*
  DCThread Class:  base class for all DCDAQ threads.
  This class is at the heart of DCDAQ.  ALL threads in DCDAQ are derived 
  from this class.  It provides the basic DCDAQ internal messaging system and 
  the interfaces to setup and launch a thread.

  Configuration:
    Setup(xmlNode, map):   This function is called by DCDAQ to handle the 
                           configuration of each derived class.   The xmlNode 
			   contains the settings for the derived class
                           and the map give the thread access to the memory 
			   managers that DCDAQ has been instructed to load.
			   Returns true if setup worked correctly.
    Add*FD(int):           Adds a read/write/exception FD to be listened to 
                           by select.
    Get*FDs():             Returns a vector of the * FDs being listened to by 
                           select.
    SetupSelect():         Sets up variables for select to wait for FDs

  Threaded functions:
    Start():               Calling this function creates the independent thread 
                           for this DCThread.
                           Returns false if something stopped us from creating 
			   the thread.
    
    Run():                 This is the main thread loop.  This is under the 
                           control of DCThread and is where the derived class's 
			   MainLoop function is called.  It contains two loops, 
			   one for when the pthread is running, but the
			   MainLoop is set to sleep.   In this mode it waits 
			   for a new message to be received from the 
			   InMessageQueue.   The second loop also checks
			   the InMessageQueue, but will also listen for other 
			   file descriptors (FD) the MainLoop cares about.  
			   If a non-InMessageQueue FD is ready, the MainLoop()
			   is called.
			   

    MainLoop():            Loop where the derived class's code is run.  
                           This is only run when a FD is ready.
			   You must register some FDs with the Add*FD functions 
			   and call the SetupSelect() function.

  Message Interfaces:
    GetMessageOut(bool):   This function gets a message from the MessageOut 
                           queue.   This is used by DCDAQ to get messages from 
			   the thread.   The bool sets if this function will 
			   block or not. 
                           Returns a message which will have the badValue type 
			   if there was nothing on the queue.

    GetMessageIn(bool):    This is the same as the the function above except it 
                           is for the DCThread to get messages from DCDAQ.

    SendMessageIn(Message,bool):   This function is used by DCDAQ to send a 
                                   message to DCThread.  Returns if the
                                   message was sent.   The bool passed to the 
				   function specifies if it should block or not.

    SendMessageOut(Message,bool):  Same as above but in the opposite direction. 
                                   DCThread -> DCDAQ

    IncomingMessageCount():        Returns the number of pending messages 
                                   coming into DCThread.

    OutgoingMesssageCount():       Returns the number of pending messages 
                                   going out of DCThread.

 


		    
			   
 */
class DCThread
{
  //===================================================================
 public://============================================================
  //===================================================================

  //================================
  //Constructor
  //================================
  DCThread();

  //================================
  //Destructor
  //================================
  virtual ~DCThread(){End(true);};

  //================================
  //Setup functions
  //================================
  virtual bool Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> &MemManager) 
  {
    return(false);
  };
  //Are these used anywhere? 
  //  virtual bool Setup(xmlNode * SetupNode) {return(false);};
  //  virtual bool Setup() {return(false);};

  //================================
  //Thread functions
  //================================
  bool Start(void * Arg);           //This function creates the thread (pthread)
  virtual void MainLoop() {};   //Derived class thread loop
  void *Run(void * Arg);            //DCThread thread loop

  //================================
  //DCThread <-> DCDAQ messaging (DCDAQ side)
  //================================
  int                OutgoingMessageCount(){return(MessagesOut.GetSize());};
  DCMessage::Message GetMessageOut(bool Wait);
  bool               SendMessageIn(DCMessage::Message &essage,bool Wait);
  const int * const  GetMessageOutFDs() {return(MessageOutPipeFD);};
  const int * const  GetMessageInFDs() {return(MessageInPipeFD);};


  //================================
  //Access to private book-keeping members
  //================================
  const pthread_t &   GetID(){return(ThreadID);};   
  const std::string & GetName(){return(ThreadName);};
  const std::string & GetType(){return(ThreadType);};
  const int &         GetRunNumber(){return(RunNumber);};
  void *              GetThreadResults(){return(ThreadResults);};
  const bool          IsRunning() {return(Running);};


  //===================================================================
 protected://============================================================    
  //===================================================================

  //================================
  //Thread/run loop control
  //================================
  volatile bool Ready;              //To force Setup to be run before Start.
  volatile bool Running;            //DCThread is running. 
                                    //Main loop might not be.
  volatile bool Loop;               //Main loop is running

  //================================
  //Access to private book-keeping members
  //================================
  void SetThreadID(pthread_t _ThreadID){ThreadID = _ThreadID;};
  void SetName(std::string _Name){ThreadName = _Name;};
  void SetType(std::string _Type){ThreadType = _Type;};
  void SetRunNumber(int _RunNumber){RunNumber=_RunNumber;};

  //================================
  //Error/warning printing
  //================================
  void PrintError(const char * text);
  void PrintWarning(const char * text);

  //================================
  //Setup message pipes
  //================================
  void SetupMessagePipes();         //Create pipes and fill in fds

  //================================
  //DCThread <-> DCDAQ messaging (DCThread side)
  //================================
  DCMessage::Message  badDCDAQMessage;
  int                 IncomingMessageCount(){return(MessagesIn.GetSize());};
  DCMessage::Message  GetMessageIn(bool Wait);
  bool                SendMessageOut(DCMessage::Message &message,bool Wait);

  virtual bool ProcessMessage(DCMessage::Message &message);   //Process a new message
  virtual void ProcessTimeout(){};   //Process a timeout

  void        SetUpdateTime(int seconds) {UpdateTime = seconds;};
  const int & GetUpdateTime() {return(UpdateTime);};
  int         GetSelectTimeoutSeconds(){return(DefaultSelectTimeout.tv_sec);};
  int         GetSelectTimeoutuSeconds(){return(DefaultSelectTimeout.tv_usec);};
  
  //================================
  //Select setup functions
  //================================
  void AddReadFD(int fd)         {AddToVectorWODup(fd,ReadFDs);};
  void AddWriteFD(int fd)        {AddToVectorWODup(fd,WriteFDs);};
  void AddExceptionFD(int fd)    {AddToVectorWODup(fd,ExceptionFDs);};  
  void RemoveReadFD(int fd)      {RemoveFromVector(fd,ReadFDs);};
  void RemoveWriteFD(int fd)     {RemoveFromVector(fd,WriteFDs);};
  void RemoveExceptionFD(int fd) {RemoveFromVector(fd,ExceptionFDs);};  

  const std::vector<int> & GetReadFDs(){return(ReadFDs);};
  const std::vector<int> & GetWriteFDs(){return(WriteFDs);};
  const std::vector<int> & GetExceptionFDs(){return(ExceptionFDs);};

  void SetupSelect();               //Call to setup select sets
  void ClearSelect();               //Clear all but InMessageQueue from the fd_sets
  void SetSelectTimeout(long seconds,long useconds);
  

  //================================
  //Select results
  //================================
  const fd_set &  GetReadSet() {return(retReadSet);};
  const fd_set &  GetWriteSet() {return(retWriteSet);};
  const fd_set &  GetExceptionSet() {return(retExceptionSet);};

  bool  IsReadReady(int fd)
  {
    if(fd >= 0)      
      return(FD_ISSET(fd,&GetReadSet()));
    return(false);
  };
  bool  IsWriteReady(int fd)
  {
    if(fd >= 0)      
      return(FD_ISSET(fd,&GetWriteSet()));
    return(false);
  };
  bool  IsExceptionReady(int fd)
  {
    if(fd >= 0)      
      return(FD_ISSET(fd,&GetExceptionSet()));
    return(false);
  };

  //===================================================================
 private://============================================================
  //===================================================================

  //================================
  //DCThread book-keeping values
  //================================
  int RunNumber;                    //DCDAQ run number
  pthread_t ThreadID;               //Unique thread ID
  void * ThreadResults;             //void pointer for return
  std::string ThreadName;           //DCDAQ name for this thread
  std::string ThreadType;           //DCDAQ type for this thread
  int  UpdateTime;                  //Time(seconds) to wait before updating 
                                    //statistics.

  //================================
  //DCDAQ message communications  
  //================================
  //Incomming messages from DCDAQ
  LockedQueue<DCMessage::Message> MessagesIn;  //Locked queue with messages 
  int MessageInPipeFD[2];                      //incomming message pipe
  //Outgoing messages from DCDAQ
  LockedQueue<DCMessage::Message> MessagesOut; //Locked queue with messages 
  int MessageOutPipeFD[2];                     //incomming message pipe

  //================================
  //Select control
  //================================
  fd_set ReadSet;                   //The set of FD(file descriptors) 
                                    //that we are waiting to read from.
  fd_set WriteSet;                  //that we are waiting to write to.
  fd_set ExceptionSet;              //Set of exceptions we are waiting for.

  fd_set retReadSet;                //The copy of ReadSet that we pass select
  fd_set retWriteSet;               //The copy of WriteSet that we pass select
  fd_set retExceptionSet;           //The copy of ExceptionSet that we pass 
                                    //select

  int    MaxFDPlusOne;              //The maximum FD plus one for select.
  struct timeval SelectTimeout;     //Timeout for select.  This shouldn't happen
  struct timeval DefaultSelectTimeout;     //defalt Timeout for select.
                                           //select modifies SelectTimeout

  std::vector<int> ReadFDs;              //A list of all read fds
  std::vector<int> WriteFDs;             //A list of all write fds.
  std::vector<int> ExceptionFDs;         //A list of all exceptions.  
  void AddToVectorWODup(int value,std::vector<int> &vec);
  void RemoveFromVector(int value,std::vector<int> &vec);

  //================================
  //Cleanup
  //================================
  void * End(bool Wait);            //Called by destructor

  //================================
  //Disallow coping of DCThread objects
  //================================
  DCThread(const DCThread&);              //Never implement these functions
  DCThread& operator=(const DCThread&);   //Never implement these functions
};

#endif
