#include <RATThrowAway.h>

void RATThrowAway::MainLoop()
{
  //Get a full block
  if(IsReadReady(Mem->GetFullFDs()[0]) &&
     (dsIndex == FreeFullQueue::BADVALUE))
    {
      //Get new ds block
      Mem->GetFull(dsIndex);
      if(dsIndex != FreeFullQueue::BADVALUE)
	{
	  dsBlock = Mem->GetDSBlock(dsIndex);
	}
    }
  
  if(dsIndex != FreeFullQueue::BADVALUE)
    {      
      EventNumber+=dsBlock->usedSize;  //Incriment the event number
      //Do some work on these so we force the CPU to load them into cache
      for(unsigned int i = 0; i < dsBlock->usedSize;i++)
	{
	  dsBlock->array[i].GetEVCount();
	}
      TimeStep_Events+=dsBlock->usedSize;
      //Pass off free memory block
      dsBlock->usedSize = 0;
      Mem->AddFree(dsIndex);
    }
}


void  RATThrowAway::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      DCMessage::Message message;
      DCMessage::TimedCount Count;
      //Data message
      sprintf(Count.text,"Events read");
      Count.count = TimeStep_Events;
      Count.dt = float(difftime(currentTime,lastTime));
      message.SetDataStruct(Count);
      message.SetType(DCMessage::STATISTIC);
      SendMessageOut(message,false);
      TimeStep_Events = 0;
      //Setup next time
      lastTime = currentTime;
    }
}
