#ifndef __MEMORYMANAGER__
#define __MEMORYMANAGER__

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <string>

class MemoryManager
{
 public:
  MemoryManager(){};
  virtual ~MemoryManager() {Deallocate();};
  //  virtual bool AllocateMemory(xmlNode * SetupNode) {return(false);};
  virtual bool AllocateMemory(xmlNode * SetupNode) = 0;

  virtual void PrintStatus(int indent = 0) = 0;

  virtual void Shutdown() = 0;
  virtual void WakeUp() = 0;

  //Name get functions
  std::string GetManagerName() {return ManagerName;};
  std::string GetManagerType() {return ManagerType;};

 protected:
  void SetName(std::string _Name){ManagerName = _Name;};
  void SetType(std::string _Type){ManagerType = _Type;};
  
 private:
  std::string ManagerName;
  std::string ManagerType;
  virtual void Deallocate(){};
};

#endif
