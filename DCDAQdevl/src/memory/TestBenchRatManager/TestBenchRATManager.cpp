#include <TestBenchRATManager.h>

TestBenchRATManager::TestBenchRATManager(std::string Name)
{
  SetName(Name);
  SetType(std::string("TestBenchRATManager"));
  //Make sure our memory vector is empty
  Memory.clear();
  ShutdownStatus = false;
  badValue = -2;
  emptyValue = -1;
}

TestBenchRATManager::~TestBenchRATManager()
{
  Deallocate();
}

void TestBenchRATManager::Deallocate()
{
  //Clear our containers
  Free.Clear(true);
  Disk.Clear(true);
  AssembledEvent.Clear(true);
  //Loop over all allocated DS events.
  while(Memory.size())
    {
      //      delete TestBench event (check if it's not NULL)
      if(Memory.back().ev)
	{
	  delete Memory.back().ev;
	}
      //remove it's entry in Memory
      Memory.pop_back();
    }
}

void TestBenchRATManager::WakeUp()
{
  //child objects
  Free.WakeUp();
  Disk.WakeUp();
  
  AssembledEvent.WakeUp();
}
void TestBenchRATManager::Shutdown()
{
  //Set to shutdown mode.   Now we can't wait on mutex locks
  //Child objects
  Free.ShutdownWait();
  Disk.ShutdownWait();

  
  AssembledEvent.ShutdownWait();

  ShutdownStatus = true;
  
  //call wakeup to force everyone out of a mutex condition wait.
  WakeUp();
}

bool TestBenchRATManager::AllocateMemory(xmlNode * setupNode)
{
  bool ret = true;
  
  int NumTestBenchBlocksERR = GetXMLValue(setupNode,"NUMTestBenchBLOCKS","MANAGER",NumTestBenchBlocks);
  if(NumTestBenchBlocksERR != 0)
    {
      fprintf(stderr,"Error reading NUMTestBenchBLOCKS in TestBenchRATManager.\n");
      ret = false;
    }

  int AssembledEventSizeERR = GetXMLValue(setupNode,"ASSEMBLEDEVENTSIZE","MANAGER",AssembledEventSize);
  if(AssembledEventSizeERR != 0)
    {
      fprintf(stderr,"Error reading AssembledEventSize in TestBenchRATManager.\n");
      ret = false;
    }

  //Allocate the needed RAT TestBench blocks
  if(ret)
    {
      for(uint32_t iTestBenchBlock = 0; iTestBenchBlock < NumTestBenchBlocks;iTestBenchBlock++)
	{
	  TestBenchBlock evBlock;	  
	  Memory.push_back(evBlock);
	  Memory.back().ev = new RAT::DS::EV;	  
	  int32_t index = Memory.size()-1;
	  Free.Add(index);
	  if(Memory.back().ev == NULL)
	    {
	      ret = false;
	      break;
	    }
	}     
    }
  //Since we have just setup this MM we should set shutdown status to false
  ShutdownStatus =false;


  //Setup the EventStore if the allocation worked
  if(ret)
    {
      //Setup the queues and vector-hash 
      AssembledEvent.Setup(AssembledEventSize,emptyValue,badValue);
    }

  //Setup Queues
  Free.Setup(badValue);
  Disk.Setup(badValue);

  return ret;
}

void TestBenchRATManager::PrintStatus(int indent)
{
  printf("\n");
  if(indent > 0)
    {
      for(int i = 0;i < indent;i++)
	{
	  printf(" ");
	}
    }
# if __WORDSIZE == 64
  char printCommand[] = "TestBenchRATManager\n     Size: %04lu\n     Free: %04lu\n     Active Assembled: %04lu(%04lu)\n     Disk: %04lu\n";
# else
  char printCommand[] = "TestBenchRATManager\n     Size: %04u\n     Free: %04u\n     Active Assembled: %04u(%04u)\n     Disk: %04u\n";
#endif
  printf(printCommand,
	 Memory.size(),
	 Free.GetSize(),
	 AssembledEvent.GetActiveEntryCount(),
	 AssembledEvent.GetSize(),
	 Disk.GetSize());

}


