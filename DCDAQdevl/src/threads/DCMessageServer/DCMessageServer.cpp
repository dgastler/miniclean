#include <DCMessageServer.h>

DCMessageServer::DCMessageServer(std::string Name)
{
  SetType("DCMessageServer");
  SetName(Name);
 
  Loop = true;         //This thread should start running
                       //as soon as the DCDAQ loop starts
  managerSettings.assign("<PACKETMANAGER><IN><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></IN><OUT><DATASIZE>32767</DATASIZE><PACKETCOUNT>200</PACKETCOUNT></OUT></PACKETMANAGER>");
  //  HACK=-1;
}

DCMessageServer::~DCMessageServer()
{
  //Close our server listening socket
  //Close all of our connections to remote DCDAQs 
  server.Clear();
}

bool DCMessageServer::Setup(xmlNode * SetupNode,
			    std::map<std::string,MemoryManager*> &/*MemManager*/)
{  
  managerDoc = xmlReadMemory(managerSettings.c_str(),
			     managerSettings.size(),
			     NULL,
			     NULL,
			     0);
  managerNode = xmlDocGetRootElement(managerDoc);

  //================================
  //Parse data for server config and expected clients.
  //================================
  if(server.Setup(SetupNode))
    {
      //Add the new connection fd to select
      AddReadFD(server.serverSocketFD);
      SetupSelect();
      Ready=true;
    }
  else
    {
      return(false);
    }
  return(true);
}

void DCMessageServer::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.serverSocketFD))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(unsigned int iConnection = 0; iConnection < server.Size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(server[iConnection].thread->GetInPacketManager()->GetFullFDs()[0]))
	{
	  RouteRemoteDCMessage(server[iConnection].thread);
	}
      //Check for a new message
      if(IsReadReady(server[iConnection].thread->GetMessageOutFDs()[0]))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

bool DCMessageServer::ProcessMessage(DCMessage::Message &message)
{  
  bool ret = true;
  //Determine if this is local or not
  std::string DestName;
  struct in_addr addr;
  message.GetDestination(DestName,addr);
  if(addr.s_addr > 0)
    {
      RouteLocalDCMessage(message);
    }
  else if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //This isn't needed for this thread because it will always run
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //This isn't needed for this thread.
    }
  else
    {
      ret = false;
    }
  return(ret);
}



bool DCMessageServer::ProcessNewConnection()
{
  //create a DCNetThread for this 
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().thread->GetInPacketManager()->GetFullFDs()[0]);
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().thread->GetMessageOutFDs()[0]);      
      server.Back().thread->Start(NULL);
      printf("DCMessage connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().remoteIP.c_str());
      SetupSelect();

      //================================================
      //Register this connection with DCMessageProcessor
      //================================================

      //Data structure that holds the routing information
      DCMessage::DCRoute routeData;
      std::vector<uint8_t> data = routeData.SetData("",
						    server.Back().GetRemoteRouteAddr(),
						    GetName());	
      //message of REGISTER_ADDR type to tell the DCMessageProcessor to route
      //messages with this address to this thread
      DCMessage::Message message;
      message.SetType(DCMessage::REGISTER_ADDR);
      message.SetDestination();
      message.SetData(&data[0],data.size());
      SendMessageOut(message,true);


    }
 else
   {
     fprintf(stderr,"Hi3\n");				     
     return(false);
   }
  return(true);
}
void DCMessageServer::RouteRemoteDCMessage(DCNetThread * Thread)
{  
  std::vector<uint8_t> messageData;
  int packetCount = 1;
  //Loop over all the packets that make up this DCMessage
  for(int iPacket = 0; iPacket < packetCount; iPacket++)
    {
      int32_t iDCPacket = FreeFullQueue::BADVALUE;    
      //Try to get a full in packet index
      Thread->GetInPacketManager()->GetFull(iDCPacket,true);
      //Get the associated packet
      DCNetPacket * packet = Thread->GetInPacketManager()->GetPacket(iDCPacket);
      //Fail if the packet is bad
      if(packet == NULL)
	{
	  PrintWarning("Bad net packet.\n");
	  return;
	}
     
      //Now we have a valid packet
      
      //Check it's type (if it is a partial packet increase packetCount;
      if(packet->GetType() == DCNetPacket::DCMESSAGE_PARTIAL)
	{
	  packetCount++;
	}
      else if(packet->GetType() != DCNetPacket::DCMESSAGE)
	{	  
	  PrintWarning("Bad incoming packet\n");
	  //return packet as free and return
	  Thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      
      //Now get the data from the packet and append it to the message data vector
      unsigned int existingMessageDataSize = messageData.size();
      //Resize our vector to hold the new data
      messageData.resize(existingMessageDataSize + packet->GetSize());
      //Check that the packet has a valid data pointer
      if(packet->GetData() == NULL)
	{
	  PrintWarning("Bad packet data\n");
	  //return packet as free and return
	  Thread->GetOutPacketManager()->AddFree(iDCPacket);
	  return;
	}
      //Copy in the new data
      memcpy(&messageData[existingMessageDataSize],packet->GetData(),packet->GetSize());
      //return our packet
      Thread->GetOutPacketManager()->AddFree(iDCPacket);
    }
  //create a new DCPacket using the data from the packets
  DCMessage::Message  message;
  message.SetMessage(&messageData[0],messageData.size());
  //The Client will forward broadcast messages to this session  
  SendMessageOut(message,true);
}
bool DCMessageServer::RouteLocalDCMessage(DCMessage::Message &message)
{  
  bool ret = true;
  std::string DestName;
  struct in_addr addr;
  message.GetDestination(DestName,addr);
  
  //Send to all. 
  if(addr.s_addr == 0xFFFFFFFF)
    {
      for(unsigned int i = 0; i < server.Size();i++)
	{
	  ret &= SendMessage((server[i].thread),message);
	}
    }
  else
    {
      for(unsigned int i = 0; i < server.Size();i++)
	{
	  if(addr.s_addr == server[i].addrRemote.sin_addr.s_addr)
	    {
	      ret &= SendMessage((server[i].thread),message);
	    }
	}
    }
  return(ret);
}

bool DCMessageServer::SendMessage(DCNetThread * netThread,DCMessage::Message &message)
{
  //Get the data for this packet
  std::vector<uint8_t> stream;
  //get an vector of data for this message to send.
  int streamSize = message.StreamMessage(stream);
  if(streamSize < 0)
    return(false);
  uint8_t * dataPtr  = &(stream[0]);	  
  int packetNumber = 0;
  //Send loop over the data until all of it is sent
  while(streamSize >0)
    {
      //Get a free packet index
      int32_t iDCPacket = FreeFullQueue::BADVALUE;
      netThread->GetOutPacketManager()->GetFree(iDCPacket,true);
      //Get the DCNetPacket for that index
      DCNetPacket * packet = netThread->GetOutPacketManager()->GetPacket(iDCPacket);
      //Fail if our packet is bad
      if(packet == NULL)
	{
	  return(false);
	}
      //Fill packet
      packet->SetType(DCNetPacket::DCMESSAGE);
      if(streamSize > packet->GetMaxDataSize())
	{
	  packet->SetData(dataPtr,packet->GetMaxDataSize(),packetNumber);
	  dataPtr+=packet->GetMaxDataSize();
	  streamSize-=packet->GetMaxDataSize();
	  packetNumber++;
	}
      else
	{
	  packet->SetData(dataPtr,streamSize,packetNumber);
	  dataPtr+=streamSize;
	  streamSize-=streamSize;
	  packetNumber++;
	}
      //Send off packet
      netThread->GetOutPacketManager()->AddFull(iDCPacket);
      packet = NULL;
    }
  return(true);
}

void DCMessageServer::ProcessChildMessage(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
  DCMessage::Message message = server[iConn].thread->GetMessageOut(true);
  if(message.GetType() == DCMessage::ERROR)
    {
      DeleteConnection(iConn);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      //      SendMessageOut(message,true);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,"Unknown message (%d) from DCMessageServer's DCNetThread\n",message.GetType());
      PrintWarning(buffer);
      delete [] buffer;
    }
}

void DCMessageServer::DeleteConnection(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
 
  //Making sure that the thread actually exists
  if(server[iConn].thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(server[iConn].thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(server[iConn].thread->GetMessageOutFDs()[0]);          
    }
  SetupSelect();
  server.DeleteConnection(iConn);
}

void DCMessageServer::ProcessTimeout()
{
}
