#ifndef __RATTHROWAWAY__
#define __RATTHROWAWAY__
//RATWrite processer takes in A rull event,
//adds it to a RAT DS structure and then passes
//it off to ROOT to be written to disk.
//At the moment this is designed for microCLEAN,
//so there is goign to be no event construction
//besides that done on the V1720 module.  The
//memory manager will have to be adjusted to
//deal with an intermediate event construtor
//thread combining CAEN events from multiple
//V1720s to make one physics event.  The RATWrite
//thread should only deal with stuffing physics
//events into RATDS.

#include <DCThread.h>
#include <BasicRATDSManager.h>
#include <xmlHelper.h>

#include "RAT/DS/Root.hh"

#include <v1720Event.h>



class RATThrowAway : public DCThread 
{
 public:
  RATThrowAway(std::string Name);
  ~RATThrowAway();

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  //  virtual void *Run(void *Arg);
  virtual void MainLoop();
 private:
  virtual void ProcessTimeout();
  virtual bool ProcessMessage(DCMessage::Message &message);

  BasicRATDSManager * Mem;
  int32_t dsIndex;
  DSBlock *dsBlock;

  //Statisticas
  //Events
  uint32_t TimeStep_Events;
  time_t lastTime;
  time_t currentTime;

  //Config file and variables
  std::string Settings;
  xmlDoc * XMLDoc;
  xmlNode * BaseNode;
  uint32_t EventNumber;

};


#endif
