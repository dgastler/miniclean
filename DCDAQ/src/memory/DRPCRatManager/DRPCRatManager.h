#ifndef __DRPCRATMANAGER__
#define __DRPCRATMANAGER__


#include <MemoryManager.h>
#include <DRPC.h>
#include <xmlHelper.h>
#include <EVBlock.h>
#include "RAT/DS/EV.hh"

class DRPCRatManager : public MemoryManager , public DRPC
{
 public:
  DRPCRatManager(std::string Name);
  ~DRPCRatManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  EVBlock * GetEVBlock(uint32_t iEV) 
    {
      if(iEV < Memory.size())
	{
	  return(&(Memory[iEV]));
      }
    return(NULL);
  }
 private: 
  uint32_t NumEV;
  uint32_t VectorHashSize;
  std::vector<EVBlock> Memory;
  void Deallocate();
};

#endif
