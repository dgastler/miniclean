#include <DCNetPacketManager.h>

DCNetPacketManager::DCNetPacketManager(std::string Name)
{
  //Make sure our memory block is empty.
  SetName(Name);
  SetType(std::string("DCNetPacketManager"));
  Deallocate();
  ShutdownStatus = false;
}

DCNetPacketManager::~DCNetPacketManager()
{
  Deallocate();
}

void DCNetPacketManager::Deallocate()
{
  while(Packets.size())
    {
      delete Packets.back();
      Packets.pop_back();
    }
  Packets.clear();
}

bool DCNetPacketManager::AllocateMemory(xmlNode * SetupNode)
{
  bool ret = true;
  //Get the block size;
  uint32_t dataSize;
  int dataSizeERR = GetXMLValue(SetupNode,"DATASIZE","DCNetPacket",dataSize);
  if(dataSizeERR){ret = false;}
  //Get the number of Blocks
  uint32_t nPackets;
  int packetCountERR = GetXMLValue(SetupNode,"PACKETCOUNT","MANAGER",nPackets);
  if(packetCountERR){ret = false;}
  

  Deallocate();  

  for(unsigned int i = 0;(ret && (i < nPackets));i++)
    {
      DCNetPacket * packet = new DCNetPacket(dataSize);      
      Packets.push_back(packet);
      int32_t index = int32_t(Packets.size())-1;
      AddFree(index);
    }

  return(ret);
}

void DCNetPacketManager::PrintStatus(int indent, int /*verbosity*/)
{
  StatMessage::StatFreeFull sStat;
  sStat.sBase.type = StatMessage::FREE_FULL;
  sStat.sInfo.time = time(NULL);
  sStat.sInfo.subID = SubID::BLANK;
  sStat.sInfo.level = Level::BLANK;
  sStat.allocatedSize = Packets.size();
  sStat.free.size     = FreeSize();
  sStat.free.addCount = FreeAddCount();
  sStat.full.size     = FullSize();
  sStat.full.addCount = FullAddCount();

#ifdef VERBOSE_MEMORY
  printf("\n");
  if(indent > 0)
    {
      for(int i = 0; i < indent; i++)
	{
	  printf(" ");
	}
    }
  printf("DCNetPacketManager     Size: %04"PRIu64"     Full: %04"PRIu64"(%12"PRIu64")     Free: %04"PRIu64"(%12"PRIu64")\n",
	 sStat.allocatedSize,
	 sStat.full.size,
	 sStat.full.addCount,
	 sStat.free.size,
	 sStat.free.addCount);
#endif
}
