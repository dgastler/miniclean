PCBNEW-LibModule-V1  Thu Dec  6 15:32:31 2012
# encoding utf-8
$INDEX
MOLEX
$EndINDEX
$MODULE MOLEX
Po 0 0 0 15 50C100B7 50C0FF78 ~~
Li MOLEX
Sc 50C0FF78
AR MOLEX
Op 0 0 0
T0 0 -2800 600 600 0 120 N V 21 N "MOLEX 2x4"
T1 0 2750 600 600 0 120 N V 21 N "Val**"
DS -3550 1950 3550 1950 150 21
DS 3550 1950 3550 -2400 150 21
DS 3550 -2400 -3550 -2400 150 21
DS -3550 -2350 -3550 1950 150 21
$PAD
Sh "6" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -827 1083
$EndPAD
$PAD
Sh "5" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2480 1083
$EndPAD
$PAD
Sh "7" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 827 1083
$EndPAD
$PAD
Sh "8" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2480 1083
$EndPAD
$PAD
Sh "1" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2480 -1083
$EndPAD
$PAD
Sh "2" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -827 -1083
$EndPAD
$PAD
Sh "3" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 827 -1083
$EndPAD
$PAD
Sh "4" C 1102 1102 0 0 0
Dr 787 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2480 -1083
$EndPAD
$EndMODULE  MOLEX
$EndLIBRARY
