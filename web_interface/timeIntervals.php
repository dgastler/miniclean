<?php
  //These are the time intervals between when the setInterval function is called in the webpage
  //IMPORTANT: they are measured in milliseconds!
  //so statusTime corresponds to 15 seconds, and pingTime to 5 seconds
$statusTime = 15000;
$pingTime = 5000;

?>