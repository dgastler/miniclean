#include "PSDFit.h"

TF1 * PSDCut(TH2F * h,Int_t N = 10)
{
  //  Double_t  P1 = 0;
  //  Double_t  P1E = 0;
  //  Double_t  P2 = 0;
  //  Double_t  P2E = 0;
  //Xaxis
  Double_t * X   = new Double_t[(Int_t) h->GetNbinsX() - N]; 
  //Cut values
  Double_t * Cut = new Double_t[(Int_t) h->GetNbinsX() - N];
  Double_t * CutE = new Double_t[(Int_t) h->GetNbinsX() - N];
  //Electronic points
  Double_t * P1 = new Double_t[(Int_t) h->GetNbinsX() - N];
  Double_t * P1E = new Double_t[(Int_t) h->GetNbinsX() - N];
  //Nuclear points
  Double_t * P2 = new Double_t[(Int_t) h->GetNbinsX() - N];
  Double_t * P2E = new Double_t[(Int_t) h->GetNbinsX() - N];

  
  TF1 * gaus1 = new TF1("gaus1","gaus");
  TF1 * gaus2 = new TF1("gaus2","gaus");
  TF1 * pol2  = new TF1("pol2","pol2");
  TF1 * Tgaus = new TF1("Tgaus","gaus(0) + gaus(3)");

  for(int i = 0; i < h->GetNbinsX() - N;i++)
    {
      //Setup bounds on fit functions.
      //      gaus1->SetParLimits(0,0,1E3);
      gaus1->SetParLimits(1,0,1);
      gaus1->SetParLimits(2,0,1);

      //      gaus2->SetParLimits(0,0,1E3);
      gaus2->SetParLimits(1,0,1);
      gaus2->SetParLimits(2,0,1);

      //      Tgaus->SetParLimits(0,0,1E3);
      Tgaus->SetParLimits(1,0,1);
      Tgaus->SetParLimits(2,0,1);
      //      Tgaus->SetParLimits(3,0,1E3);
      Tgaus->SetParLimits(4,0,1);
      Tgaus->SetParLimits(5,0,1);
  
      //Set current X value
      X[i] = (h->GetXaxis()->GetBinCenter(i) + h->GetXaxis()->GetBinCenter(i+N))/2.0 ;

      //Project down a slice of the 2d histo
      TH1D * slice = h->ProjectionY("tmp",i,i+N);

      //Rebin the histogram
      slice->Rebin(4);
      
      //First attempt to fit with one gaussian.  Should find background peak.
      gaus1->SetParameter(0,slice->Integral());
      if((slice->GetBinCenter(slice->GetMaximumBin()) > 1) || (slice->GetBinCenter(slice->GetMaximumBin()) < 0))
	gaus1->SetParameter(1,0.3);
      else
	gaus1->SetParameter(1,slice->GetBinCenter(slice->GetMaximumBin()));
      //      gaus1->SetParameter(1,slice->GetBinCenter(slice->GetMaximumBin()));
      gaus1->SetParameter(2,0.1);
      slice->Fit(gaus1,"NMQ","",
		 //		 slice->GetBinCenter(slice->GetMaximumBin()) -.2,
		 //		 slice->GetBinCenter(slice->GetMaximumBin()) +.2
		 0,1
		 );
      if((gaus1->GetParameter(1) > 0.7) || (gaus1->GetParameter(1) < 0))
	{
	  gaus1->SetParameter(1,0.4);
	  gaus1->SetParameter(2,0.1);
	}
	  

      //Clone projection and subtract the last fit.
      TH1D * slicetemp = (TH1D*) slice->Clone("slicetemp");
      slicetemp->Add(gaus1,-1.0,"I");
      gaus2->SetParameters(slice->Integral(),0.3,0.1);
      //Fit another gaussian below the mean of the last gaussian.
      slicetemp->Fit(gaus2,"NMQ","",0,gaus1->GetParameter(1));

      if((gaus2->GetParameter(1) > 0.7) || (gaus2->GetParameter(1) < 0))
	{
	  gaus2->SetParameter(1,0.3);
	  gaus2->SetParameter(2,0.1);
	}


      // Setup the two guassian function with the parameters from the previous
      // two gaussians. 
      for(int k = 0; k < 3;k++)
	{
	  Tgaus->SetParameter(k,gaus1->GetParameter(k));
	  Tgaus->SetParameter(3+k,gaus2->GetParameter(k));
	}      
      //Set new parameter limits on this function based on the last fits
      Tgaus->SetParLimits(1,gaus2->GetParameter(1) - 2*gaus2->GetParameter(2),
			  gaus1->GetParameter(1) + 2*gaus1->GetParameter(2));
      Tgaus->SetParLimits(4,gaus2->GetParameter(1) - 2*gaus2->GetParameter(2),
			  gaus1->GetParameter(1) + 2*gaus1->GetParameter(2));
      slice->Fit(Tgaus,"NMQ","",0,1);
      
      //Find the gaussian peaks.
      P1[i] = Tgaus->GetParameter(1);
      P1E[i] = Tgaus->GetParameter(2);
      P2[i] = Tgaus->GetParameter(4);
      P2E[i] = Tgaus->GetParameter(5);

      //Swap them if they are in the wrong order
      if(P1[i] < P2[i])
	{
	  Double_t temp = P1[i];
	  P1[i]  = P2[i];
	  P2[i]  = temp;
	  temp   = P1E[i];
	  P1E[i] = P2E[i];
	  P2E[i] = temp;
	}

      if(((fabs(P1[i] - gaus1->GetParameter(1)) >  2*gaus1->GetParameter(2))||
	  (fabs(P2[i] - gaus1->GetParameter(1)) >  2*gaus1->GetParameter(2))) && i != 0)
	{
	  P1[i] = P1[i-1];
	  P2[i] = P2[i-1];
	  P1E[i] = P1E[i-1];
	  P2E[i] = P2E[i-1];
	}


      //Find the minimum of the fit function between these two peaks.
      //If it's one of the peaks, then take halfway between them. 
      Cut[i] = Tgaus->GetMinimumX(P2[i],P1[i]);
      //      if(fabs(Cut[i] - P2[i]) < fabs(Cut[i] - P1[i]))
      //	CutE[i] = fabs(Cut[i] - P2[i]);
      //      else
      //	CutE[i] = fabs(Cut[i] - P1[i]);
      //    slice->Fit(pol2,"FQN","",P2[i],P1[i]);
      //      Cut[i] = -0.5*pol2->GetParameter(1)/pol2->GetParameter(2);
      //CutE[i] = sqrt(
      //		     ((pol2->GetParError(1)/(2*pol2->GetParameter(2)))*   
      //		      (pol2->GetParError(1)/(2*pol2->GetParameter(2)))) + 
      //		     ((pol2->GetParError(2)*pol2->GetParameter(1)/(2*pol2->GetParameter(2)*pol2->GetParameter(2)))*
      //		      (pol2->GetParError(2)*pol2->GetParameter(1)/(2*pol2->GetParameter(2)*pol2->GetParameter(2))))
      //		     );
      if(fabs(Cut[i] - P2[i]) < 0.001 || fabs(Cut[i] - P1[i]) < 0.001)
	//	if((Cut[i] > P1[i])||(Cut[i] < P2[i]))
	{
	  Cut[i] = (P2[i] + P1[i])/2.0;
	  //	  CutE[i] = fabs(Cut[i] - P2[i]);
	}
      delete slice;
      delete slicetemp;
    }
  //Build a graph of the Cut parameters
  TGraph * gCut = new TGraph((Int_t) h->GetNbinsX(),X,Cut);
  gCut->SetLineColor(kGreen);
  TGraph * gP1 = new TGraph((Int_t) h->GetNbinsX(),X,P1);
  gP1->SetLineColor(kViolet);
  TGraph * gP2 = new TGraph((Int_t) h->GetNbinsX(),X,P2);
  gP2->SetLineColor(kBlue);
  TF1 * CutFit = new TF1("CutFit","pol2",0.05E-9,0.3E-9);
  CutFit->SetParameters(0,1E9,-2E18);
  CutFit->SetLineColor(kRed);
  //  gCut->Fit(CutFit,"QMFN","",0.1E-9,0.15E-9);
  gCut->Fit(CutFit,"QMFN","",0.075E-9,0.225E-9);
  
  h->GetListOfFunctions()->AddLast(gCut);
  h->GetListOfFunctions()->AddLast(gP1);
  h->GetListOfFunctions()->AddLast(gP2);
  h->GetListOfFunctions()->AddLast(CutFit->Clone());

  delete [] X;

  delete [] Cut;
  delete [] CutE;

  delete [] P1;
  delete [] P1E;

  delete [] P2;
  delete [] P2E;

  delete gaus1;
  delete gaus2;
  delete Tgaus;
  delete pol2;
  return(CutFit);
}
