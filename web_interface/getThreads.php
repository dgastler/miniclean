<?php 
include "ui.class.php";
//corresponds to the getPCThreads function in ui.class.php
//returns a json encoded array from a selected pc from a given run number

if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1],$_POST);
}

$pcName=$_POST['pcName'];
$runNumber=$_POST['runNumber'];
$threads=$opt->getPCThreads($pcName,$runNumber);
echo json_encode($threads) . PHP_EOL;
?>
