#include <iostream>
#include <vector>
#include <string>
#include <math.h>

#include "TH1F.h"
#include "TFile.h"
#include "TMinuit.h"
#include "TTree.h"
#include "TF1.h"
#include "TROOT.h"


//=======================================================
//Fitting function and the global variables it uses.
//=======================================================
Float_t LY = 5.0;
std::vector<Int_t> NpePMT0_background;
std::vector<Int_t> NpePMT1_background; 
std::vector<Int_t> NpePMT0_signal;
std::vector<Int_t> NpePMT1_signal; 
std::vector<Float_t> PEs_background; 
std::vector<Float_t> PEs_signal; 

TH1F * PMT0SPE;
TH1F * PMT1SPE;

TH1F * hMC;
TH1F * hMCn;
TH1F * hData;
Double_t BaseSmear; //the last full smearing performed.

bool rebuild = true;
double smearing;
double SPEsampling = 1000;
double SmearingSampling = 1000;


void SetMCHistogram(double norm,double leff)
{

  BaseSmear = leff;
  TF1 * SmearingFunction = new TF1("SmearingFunction","gaus",0,hMC->GetXaxis()->GetXmax()*5);
  SmearingFunction->SetNpx(10000);
  
  PEs_background.clear(); 
  PEs_signal.clear(); 

  Double_t Light;
  Double_t SmearingFactor = smearing*smearing*(1-leff);
  //  Double_t SmearingFactor = 9;
      
  //Process the signal events.
  Int_t SignalN = NpePMT0_signal.size();
  for(int iSignal = 0;iSignal < SignalN;iSignal++)
    {
      Light = 0;
      //Calculate new PMT0 pe count
      Int_t PMT0 = floor(NpePMT0_signal[iSignal]*leff + 0.5);
      for(int iPMT0 = 0; iPMT0 < PMT0;iPMT0++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      Light+= PMT0SPE->GetRandom()/SPEsampling;
	    }
	  //Light+= PMT0SPE->GetRandom();
	  //Light++;
	}
      //Calculate new PMT0 pe count
      Int_t PMT1 = floor(NpePMT1_signal[iSignal]*leff + 0.5);
      for(int iPMT1 = 0; iPMT1 < PMT1;iPMT1++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      Light+= PMT1SPE->GetRandom()/SPEsampling;
	    }
	  //	  Light+= PMT1SPE->GetRandom();
	  //Light++;
	}
      if(Light > 0)
	{
	  SmearingFunction->SetParameters(1,Light,sqrt(SmearingFactor*Light));
	}
      else
	{
	  SmearingFunction->SetParameters(1,Light,1);
	}
      for(int iSmearingSampling = 0; iSmearingSampling < SmearingSampling;iSmearingSampling++)
	{
	  PEs_signal.push_back(SmearingFunction->GetRandom());
	}
    }
  //Process the background events.
  Int_t BackgroundN = NpePMT0_background.size();
  for(int iBackground = 0;iBackground < BackgroundN;iBackground++)
    {
      Light = 0;
      //Calculate new PMT0 pe count
      Int_t PMT0 = floor(NpePMT0_background[iBackground]*leff + 0.5);
      for(int iPMT0 = 0; iPMT0 < PMT0;iPMT0++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      Light+= PMT0SPE->GetRandom()/SPEsampling;
	    }
	  //	  Light+= PMT0SPE->GetRandom();
	  //Light++;
	}
      //Calculate new PMT0 pe count
      Int_t PMT1 = floor(NpePMT1_background[iBackground]*leff + 0.5);
      for(int iPMT1 = 0; iPMT1 < PMT1;iPMT1++)
	{
	  for(int iSPEsampling = 0; iSPEsampling < SPEsampling;iSPEsampling++)
	    {
	      Light+= PMT1SPE->GetRandom()/SPEsampling;
	    }
	  //	  Light+= PMT1SPE->GetRandom();
	  //Light++;
	}
      if(Light > 0)
	{
	  SmearingFunction->SetParameters(1,Light,sqrt(SmearingFactor*Light));
	}
      else
	{
	  SmearingFunction->SetParameters(1,Light,1);
	}
      for(int iSmearingSampling = 0; iSmearingSampling < SmearingSampling;iSmearingSampling++)
	{
	  PEs_background.push_back(SmearingFunction->GetRandom());
	}

    }



  delete SmearingFunction;
}

























void fcn(int &npar, double *gin, double &f, double *par, int iflag) 
{
  f = 0;  //initialize chi-squared to zero.
  Double_t normb = par[0];
  //  Double_t norms = par[1];
  Double_t norms;
  if(npar >2)
    {
      norms = par[2];
    }
  else
    {
      norms = par[0];
    }
  Double_t effSE = par[1]; //smearing factor the fitter sees.

  if(rebuild)
    {
      SetMCHistogram(1,effSE);
      rebuild = false;  
      //}
      hMC->Reset();
      hMCn->Reset();
      //      Double_t PEtoEnergy = 1.0/LY;
      Double_t PEtoEnergy = effSE/(LY*BaseSmear);
      unsigned int PEs_signalSize = PEs_signal.size();
      unsigned int PEs_backgroundSize = PEs_background.size();
      for(unsigned int i = 0; i < PEs_signalSize;i++)
	{
	  //	  hMC->Fill(PEtoEnergy*PEs_signal[i],1);
	  hMCn->Fill(PEtoEnergy*PEs_signal[i],1.0/SmearingSampling);
	}
      for(unsigned int i = 0; i < PEs_backgroundSize;i++)
	{
	  hMC->Fill(PEtoEnergy*PEs_background[i],1.0/SmearingSampling);
	}
    }
  
  
  
  effSE/=BaseSmear; //Smearing factor relative to the last time we rebuilt the histogram.
  
  Int_t nonzeros = 0;
  Int_t zeros = 0;


  //Calculate ChiSquared
  //  Int_t Nbins = hData->GetXaxis()->FindBin(hData->GetXaxis()->GetXmax());
  Int_t Nbins = hData->GetXaxis()->GetLast();
  //  Nbins /= 2.0;
  //Double_t MaxDatax = hMC->GetXaxis()->GetXmax();//hMC->GetBinCenter(hMC->GetXaxis()->GetLast());
  //  for(int bin = hData->GetXaxis()->FindBin(hData->GetXaxis()->GetFirst());
  for(int bin = hData->GetXaxis()->GetFirst();
      bin <= Nbins;bin++)
    {
      //      Double_t MCy = hMC->GetBinContent(bin);
      Double_t x = hData->GetBinCenter(bin)/effSE;
      Double_t MCy;

      MCy = normb*hMC->GetBinContent(hMC->FindBin(x));

      //      MCy = normb*hMC->GetBinContent(bin) + norms*hMCn->GetBinContent(bin);
      Double_t DATAy = hData->GetBinContent(bin);


      Double_t error = 0.5 + sqrt(normb*MCy + 0.25);
      f+= ((DATAy - MCy)*(DATAy - MCy))/(error*error);
    }
  fprintf(stderr,"#%16.8f %16.8f %16.8f %16.8f %d %d %d %d\n",normb,norms,effSE*BaseSmear,f,nonzeros,zeros,Nbins,hData->GetXaxis()->GetLast());
}


























//=======================================================
//Main routine
//=======================================================
int main(int argc, char ** argv)
{
  if(argc < 10)
    {
      printf("%s Datafile.root RATfile.root SPESpectrum.root TOFWidth TBWidth nval eval nval2 smearing\n",argv[0]);
      return(0);
    }
  //=======================================================
  //Check to see if we are using non-default TOF and TB widths.
  //=======================================================
  Double_t TOFWidth = 6;
  Double_t TBWidth = 0.4;
  TBWidth = fabs(atof(argv[5]));
  TOFWidth = fabs(atof(argv[4]));

  Double_t TOFMax = 72+TOFWidth;
  Double_t TOFMin = 72-TOFWidth;
  Double_t AsymMax = TBWidth;
  Double_t AsymMin = -1.0*TBWidth;
  //=======================================================
  Double_t SEguess = atof(argv[7]);
  Double_t Nguess = atof(argv[6]);
  Double_t N2guess = atof(argv[8]);
  
  bool separateSignal = true;
  if(N2guess == -1)
    {
      separateSignal = false;
    }

  smearing = atof(argv[9]);

  //Setup file name strings
  std::string DataFileName(argv[1]);
  std::string RATFileName(argv[2]);
  std::string SPEFileName(argv[3]);
  //Ok command line stuff is done.
 
  //Vector of Agle IDs 
  std::vector<float> Energy(20,0);
  Energy[0]=10.6;
  Energy[1]=14.5;
  Energy[2]=19.0;
  Energy[3]=22.3;
  Energy[4]=27.7;
  Energy[5]=32.6;
  Energy[6]=38.9;
  Energy[7]=44.5;
  Energy[8]=51.6;
  Energy[9]=59.1;
  Energy[10]=66.9;
  Energy[11]=72.3;
  Energy[12]=79.2;
  Energy[13]=90.6;
  Energy[14]=97.9;
  Energy[15]=114.2;
  Energy[16]=182.5;
  Energy[17]=190.7;
  Energy[18]=211.3;
  Energy[19]=238.9;

  //Allocate a general purpose buffer
  Int_t bufferSize = 100;
  char * buffer = new char[bufferSize];


  //=======================================================
  //Input Data files
  //=======================================================
  
  //Get the run ID from the filename.  ex: SE_01_pass2.root
  Int_t RunID = atoi( (DataFileName.substr(DataFileName.find("_")+1 ,2)).c_str() );
  RunID = RunID - 1;
  if(RunID >= Energy.size())
    {
      printf("Run ID %08d out of range.\n",RunID);
      delete [] buffer;
      return(0);
    }
  //Check if this is a subrun.   ex: SE_01_1_pass2.root
  Int_t SubRun = atoi( (DataFileName.substr(6,1)).c_str() );
  if(SubRun)
    {
      printf("Processing subrun %d\n",SubRun);
    }
  //Open Data file and get the spectrum.
  TFile * DataFile = TFile::Open(DataFileName.c_str(),"READ");
  if(!DataFile)
    {
      printf("Bad Data file: %s\n",buffer);
      delete [] buffer;
      return(0);
    }
  //Open dat Histogram
  if(SubRun)
    {
      sprintf(buffer,"%05.1f_Neutron_scatters_%02d",Energy[RunID],SubRun);
    }
  else
    {
      sprintf(buffer,"%05.1f_Neutron_scatters",Energy[RunID]);
    }
  hData = (TH1F*) DataFile->Get("cLight");
  gROOT->cd();
  hData = (TH1F*) hData->Clone(buffer);
  if(!hData)
    {
      printf("Bad data histogram\n");
      delete [] buffer;
      return(0);
    }
  Double_t Emin = 0;
  //Fit ranges!
  if(Energy[RunID] < 25)
    {
      hData->Rebin(2);
    }
  else if(Energy[RunID] < 50)
    {
      hData->Rebin(2);
    }
  else if(Energy[RunID] < 90)
    {
      hData->Rebin(2);
    }
  else if(Energy[RunID] < 190)
    {
      hData->Rebin(4);
    }
  else
    {
      hData->Rebin(5);
    }

  for(int i = 1; i < hData->GetXaxis()->FindBin(hData->GetXaxis()->GetLast());i++)
    {
      printf("%03d %03f %03.0f\n",i,hData->GetBinCenter(i),hData->GetBinContent(i));
    }

  //=======================================================




  //=======================================================
  //Input SPEfiles
  //=======================================================
  TFile * SPEFile = TFile::Open(SPEFileName.c_str(),"READ");
  if(!SPEFile)
    {
      printf("Bad SPE file: %s\n",SPEFileName.c_str());
      delete [] buffer;
      return(0);
    }
  PMT0SPE = (TH1F*) SPEFile->Get("NormalizedPMT0SPE");
  if(!PMT0SPE)
    {
      fprintf(stderr,"No SPE0 histogram\n");
      delete [] buffer;
      return(0);
    }
  PMT1SPE = (TH1F*) SPEFile->Get("NormalizedPMT1SPE");
  if(!PMT1SPE)
    {
      fprintf(stderr,"No SPE1 histogram\n");
      delete [] buffer;
      return(0);
    }
  //=======================================================




  //=======================================================
  //Input MC files
  //=======================================================
  //Open the MC files
  TFile * MCFile = TFile::Open(RATFileName.c_str(),"READ");
  if(!MCFile)
    {
      printf("Bad MC RAT file: %s\n",RATFileName.c_str());
      delete [] buffer;
      return(0);
    }
  //Open RAT tree
  TTree * T = (TTree *) MCFile->Get("T");
  if(!T)
    {
      fprintf(stderr,"Bad tree.\n");
      delete [] buffer;
      return(0);
    }
  //Variables and branch settings for all the MC values.
  Int_t ID;
  T->SetBranchAddress("LSc_volID",&ID);
  Double_t TOF;
  T->SetBranchAddress("TOF",&TOF);
  Int_t NumPE;
  T->SetBranchAddress("numPe",&NumPE);
  Int_t Num_PMT_0;
  T->SetBranchAddress("num_PMT_0",&Num_PMT_0);
  Int_t Num_PMT_1;
  T->SetBranchAddress("num_PMT_1",&Num_PMT_1);
  Double_t Asym;
  T->SetBranchAddress("asym",&Asym);
  Int_t IntScat;
  T->SetBranchAddress("int_scat",&IntScat);
  Int_t ExtScat;
  T->SetBranchAddress("ext_scat",&ExtScat);
  Char_t  IntProc[20];
  T->SetBranchAddress("int_proc",&IntProc);
  Double_t Edep;
  T->SetBranchAddress("Edep",&Edep);

  //Convert MC IDs to actual IDs.
  std::vector<int> IDs(20,0);
  IDs[0] = 0;
  IDs[1] = 10;
  IDs[2] = 1;
  IDs[3] = 11;
  IDs[4] = 2;
  IDs[5] = 12;
  IDs[6] = 3;
  IDs[7] = 13;
  IDs[8] = 4;
  IDs[9] = 14;
  IDs[10] = 5;
  IDs[11] = 15;
  IDs[12] = 6;
  IDs[13] = 16;
  IDs[14] = 7;
  IDs[15] = 17;
  IDs[16] = 8;
  IDs[17] = 18;
  IDs[18] = 9;
  IDs[19] = 19;

  //=======================================================





  //=======================================================
  //Fill pe vectors for fitting.
  //=======================================================
  for(int iEvent = 0; iEvent < T->GetEntries();iEvent++)
    {
      //Get the next MC event
      T->GetEntry(iEvent);
      //Make sure the event has the right angle
      if(ID == IDs[RunID])
	{
	  //Do most cuts
	  if( (TOF < TOFMax)   &&
	      (TOF > TOFMin)   &&
	      (Asym < AsymMax) &&
	      (Asym > AsymMin) )
	    {
	      //Do background/signal cuts
	      if((IntScat == 1) && (ExtScat == 0))
		{
		  NpePMT0_signal.push_back(Num_PMT_0);
		  NpePMT1_signal.push_back(Num_PMT_1);
		}
	      else
		{
		  NpePMT0_background.push_back(Num_PMT_0);
		  NpePMT1_background.push_back(Num_PMT_1);
		}
	    }
	}
    }
  //=======================================================
  //Set fit ranges
  if(Energy[RunID] < 25)
    {
      hData->GetXaxis()->SetRangeUser(Emin,80); // 80
    }
  else if(Energy[RunID] < 50)
    {
      hData->GetXaxis()->SetRangeUser(Emin,100);//100
    }
  //67
  else if(Energy[RunID] < 90)
    {
      hData->GetXaxis()->SetRangeUser(Emin,110);//110
    }
  else if(Energy[RunID] < 180)
    {
      hData->GetXaxis()->SetRangeUser(Emin,160);//160
    }
  else
    {
      hData->GetXaxis()->SetRangeUser(Emin,180); //180
    }
  
  sprintf(buffer,"%s_hMC",hData->GetName());
  hMC = (TH1F*) hData->Clone(buffer);
  hMC->Reset();
  sprintf(buffer,"%s_hMCn",hData->GetName());
  hMCn = (TH1F*) hData->Clone(buffer);
  hMCn->Reset();


  //=======================================================
  //Do the fitting!
  //=======================================================
  





  
  TMinuit * gMinuit = new TMinuit(2);
  gMinuit->SetFCN(fcn);
  
  Double_t arglist[10];
  Int_t ierflg = 0;
  //set maximum printout level
  
//  arglist[0] =1;
//  gMinuit->mnexcm("SET PRI", arglist, 1, ierflg);  


  //setup the fit histograms
  rebuild = true;  
  Int_t npar = 2;
  if(separateSignal)
    {
      npar = 3;
    }
  Double_t f;
  Double_t par[3];
  par[0] = Nguess;
  par[1] = SEguess;
  if(separateSignal)
    {
      par[2] = N2guess;
    }
  fcn(npar,arglist,f,par,ierflg);


  
  //Set error definition
  // 1   : Chi square
  // 0.5 : Negative log likelihood
  //      gMinuit->SetErrorDef(0.5);
  arglist[0] = 1;
  ierflg = 0;
  gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
  if(ierflg) {fprintf(stderr,"mnexcm:SET ERR gave: %d\n",ierflg);}

  //Minimization strategy
  // 0 : fewer function calls (less reliable)
  // 1 : standard
  // 2 : try to improve minimum (slower)
  arglist[0] = 2;
  ierflg = 0;
  gMinuit->mnexcm("SET STR", arglist, 1, ierflg);  
  if(ierflg) {fprintf(stderr,"mnexcm:SET STR gave: %d\n",ierflg);}

  //Set starting values, bounds, and step sizes for parameters
  
  //  Double_t vstart[3] = {Nguess,  /*normalization*/
  Double_t vstart[3] = {Nguess,  /*normalization*/
			SEguess, /*SE*/
			N2guess}; 
  //  static Double_t step[3]   = {.1,0.01,N2guess};
  static Double_t step[3]   = {Nguess/10.0,0.05,N2guess};
  static Double_t minvals[3]= {0,0,0};  // no limits
  //  static Double_t maxvals[3]= {5,1,5};  // no limits
  static Double_t maxvals[3]= {5,0.75,5};  // no limits
  ierflg = 0;
  gMinuit->mnparm(0,"nb",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
  if(ierflg) {fprintf(stderr,"mnparm nb gave: %d\n",ierflg);}
  ierflg = 0;
  gMinuit->mnparm(1,"e",vstart[1],step[1],minvals[1],maxvals[1],ierflg);
  if(ierflg) {fprintf(stderr,"mnparm e gave: %d\n",ierflg);}

  if(separateSignal)
    {
      ierflg = 0;
      gMinuit->mnparm(2,"ns",vstart[2],step[2],minvals[2],maxvals[2],ierflg);
      if(ierflg) {fprintf(stderr,"mnparm ns gave: %d\n",ierflg);}
    }
  
  //Do the fitting
  Double_t nvalb     = 0;
  Double_t nvalberrH = 0;
  Double_t nvalberrL = 0;

  Double_t nvals     = 0;
  Double_t nvalserrH = 0;
  Double_t nvalserrL = 0;

  Double_t eval     = 0;
  Double_t evalerrH = 0;
  Double_t evalerrL = 0;

  Double_t sval     = 0;
  Double_t svalerrH = 0;
  Double_t svalerrL = 0;


  arglist[0] = 5000;     //max number of iterations
  arglist[1] = 0.1;
  TString  name; // ignored
  Double_t bnd1, bnd2; // ignored
  ierflg = 0;      
  gMinuit->mnexcm("SIMPLEX", arglist, 2, ierflg);  // Fit with MINOS
  //  gMinuit->mnexcm("MINOS", arglist, 1, ierflg);  // Fit with MINOS
  //gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);  // Fit with MINOS
  if(ierflg) {fprintf(stderr,"mnexcm:MINOS gave: %d\n",ierflg);}


  ierflg = 0;      
  gMinuit->mnpout(0,name,nvalb,nvalberrH,bnd1,bnd2,ierflg);
  if(ierflg) {fprintf(stderr,"mnpout: 0 gave: %d\n",ierflg);}
  ierflg = 0;      
  if(separateSignal)
    {
      gMinuit->mnpout(2,name,nvals,nvalserrH,bnd1,bnd2,ierflg);
      if(ierflg) {fprintf(stderr,"mnpout: 2 gave: %d\n",ierflg);}
      ierflg = 0;      
    }
  gMinuit->mnpout(1,name,eval,evalerrH,bnd1,bnd2,ierflg);
  if(ierflg) {fprintf(stderr,"mnpout: 1 gave: %d\n",ierflg);}
  Double_t uselessVariable;
  Double_t nvalbparabolicError;
  Double_t nvalsparabolicError;
  Double_t evalparabolicError;
  gMinuit->mnerrs(0,nvalberrH,nvalberrL,nvalbparabolicError,uselessVariable);  //normalization errors
  gMinuit->mnerrs(1,evalerrH,evalerrL,evalparabolicError,uselessVariable);  // Leff error
  if(separateSignal)
    {
      gMinuit->mnerrs(2,nvalserrH,nvalserrL,nvalsparabolicError,uselessVariable);  //normalization errors
    }


  printf("ID: %02d\n",RunID);
  printf("nvalb: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",nvalb,nvalbparabolicError,nvalberrH,nvalberrL);
  printf("eval: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",eval,evalparabolicError,evalerrH,evalerrL);
  if(separateSignal)
    {
      printf("nvals: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",nvals,nvalsparabolicError,nvalserrH,nvalserrL);
    }

  //Fit a second time!
  rebuild = true;
  vstart[0] = nvalb;
  vstart[1] = eval;
  if(separateSignal)
    {
      vstart[2] = nvals;
    }

  //  step[0] = 0.01;
  step[0] = Nguess/5.0;
  step[1] = 0.1;
  if(separateSignal)
    {
      step[2] = 0.01;
    }




  nvalb     = 0;
  nvals     = 0;
  eval     = 0;
  sval     = 0;
  nvalberrH = 0;
  nvalberrL = 0;
  nvalserrH = 0;
  nvalserrL = 0;
  evalerrH = 0;
  evalerrL = 0;
  svalerrH = 0;
  svalerrL = 0;

  ierflg = 0;  
  gMinuit->mnparm(0,"nb",vstart[0],step[0],minvals[0],maxvals[0],ierflg);
  if(ierflg) {fprintf(stderr,"mnparm nb gave: %d\n",ierflg);}
  if(separateSignal)
    {
      ierflg = 0;  
      gMinuit->mnparm(2,"ns",vstart[2],step[2],minvals[2],maxvals[2],ierflg);
      if(ierflg) {fprintf(stderr,"mnparm ns gave: %d\n",ierflg);}
    }
  ierflg = 0;
  gMinuit->mnparm(1,"e",vstart[1],step[1],minvals[1],maxvals[1],ierflg);
  if(ierflg) {fprintf(stderr,"mnparm e gave: %d\n",ierflg);}
  ierflg = 0;      

  arglist[0] = 2;
  gMinuit->mnexcm("release",arglist,1,ierflg);

  arglist[0] = 5000;
  arglist[1] = 0.1;
  //  gMinuit->mnexcm("SIMPLEX", arglist, 2, ierflg);  // Fit with MINOS
  gMinuit->mnexcm("MINOS", arglist, 1, ierflg);  // Fit with MINOS
    //gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
  if(ierflg) {fprintf(stderr,"mnexcm:MINOS gave: %d\n",ierflg);}
  ierflg = 0;      
  gMinuit->mnpout(0,name,nvalb,nvalberrH,bnd1,bnd2,ierflg);
  if(ierflg) {fprintf(stderr,"mnpout: 0 gave: %d\n",ierflg);}
  if(separateSignal)
    {
      ierflg = 0;      
      gMinuit->mnpout(2,name,nvals,nvalserrH,bnd1,bnd2,ierflg);
      if(ierflg) {fprintf(stderr,"mnpout: 1 gave: %d\n",ierflg);}
    }
  ierflg = 0;      
  gMinuit->mnpout(1,name,eval,evalerrH,bnd1,bnd2,ierflg);
  if(ierflg) {fprintf(stderr,"mnpout: 2 gave: %d\n",ierflg);}

  gMinuit->mnerrs(0,nvalberrH,nvalberrL,nvalbparabolicError,uselessVariable);  //normalization errors
  gMinuit->mnerrs(1,evalerrH,evalerrL,evalparabolicError,uselessVariable);  // Leff error
  if(separateSignal)
    {
      gMinuit->mnerrs(2,nvalserrH,nvalserrL,nvalsparabolicError,uselessVariable);  //normalization errors
    }


  printf("ID: %02d\n",RunID);
  printf("nvalb: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",nvalb,nvalbparabolicError,nvalberrH,nvalberrL);
  printf("eval: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",eval,evalparabolicError,evalerrH,evalerrL);
  if(separateSignal)
    {
      printf("nvals: %5.3f (+/-)%7.5e +%7.5e %7.5e\n",nvals,nvalsparabolicError,nvalserrH,nvalserrL);
    }


  arglist[0] = 1;
  arglist[1] = 2;
  arglist[2] = 100;


  rebuild = true;  
  par[0] = nvalb;
  par[1] = eval;
  if(separateSignal)
    {
      par[2] = nvals;
    }
  fcn(npar,arglist,f,par,ierflg);


  hMC->Scale(nvalb);


  if(separateSignal)
    {
      hMCn->Scale(nvals);
    }
  else
    {
      hMCn->Scale(nvalb);
    }


  //Set draw ranges
  if(Energy[RunID] < 25)
    {
      hData->GetXaxis()->SetRangeUser(Emin,80); // 80
    }
  else if(Energy[RunID] < 50)
    {
      hData->GetXaxis()->SetRangeUser(Emin,100);//100
    }
  else if(Energy[RunID] < 90)
    {
      hData->GetXaxis()->SetRangeUser(Emin,110);//110
    }
  else if(Energy[RunID] < 190)
    {
      hData->GetXaxis()->SetRangeUser(Emin,160);//160
    }
  else
    {
      hData->GetXaxis()->SetRangeUser(Emin,180); //180
    }



  TFile * OutFile;
  if(separateSignal)
    {
      OutFile = TFile::Open("NewFit3.root","UPDATE");
    }
  else
    {
      OutFile = TFile::Open("NewFit2.root","UPDATE");
    }
  TTree * FitTree = (TTree*) OutFile->Get("T");

  double dEnergy = Energy[RunID];

  if(!FitTree)
    {
      FitTree = new TTree("T","Fit values");
      FitTree->Branch("chisqr"             ,&f                  ,"chisqr/D"             );
      FitTree->Branch("energy"             ,&dEnergy            ,"energy/D"             );
      FitTree->Branch("smearing"           ,&smearing           ,"smearing/D"           );
      FitTree->Branch("nvalb"              ,&nvalb              ,"nvalb/D"              );
      FitTree->Branch("nvalberrL"          ,&nvalberrL          ,"nvalberrL/D"          );
      FitTree->Branch("nvalberrH"          ,&nvalberrH          ,"nvalberrH/D"          );
      FitTree->Branch("nvalbparabolicError",&nvalbparabolicError,"nvalbparabolicError/D");
      FitTree->Branch("nvals"              ,&nvals              ,"nvals/D"              );
      FitTree->Branch("nvalserrL"          ,&nvalserrL          ,"nvalserrL/D"          );
      FitTree->Branch("nvalserrH"          ,&nvalserrH          ,"nvalserrH/D"          );
      FitTree->Branch("nvalsparabolicError",&nvalsparabolicError,"nvalsparabolicError/D");
      FitTree->Branch("eval"              ,&eval              ,"eval/D"              );
      FitTree->Branch("evalerrL"          ,&evalerrL          ,"evalerrL/D"          );
      FitTree->Branch("evalerrH"          ,&evalerrH          ,"evalerrH/D"          );
      FitTree->Branch("evalparabolicError",&evalparabolicError,"evalparabolicError/D");
      FitTree->Branch("hData"             ,"TH1F",&hData,128000,0);              
      FitTree->Branch("hMC"               ,"TH1F",&hMC,128000,0);              
      FitTree->Branch("hMCn"              ,"TH1F",&hMCn,128000,0);              
    }
  else
    {
      FitTree->SetBranchAddress("chisqr",&f,0);
      FitTree->SetBranchAddress("energy",&dEnergy,0);
      FitTree->SetBranchAddress("smearing",&smearing,0);
      FitTree->SetBranchAddress("nvalb",&nvalb,0);
      FitTree->SetBranchAddress("nvalberrL",&nvalberrL,0);
      FitTree->SetBranchAddress("nvalberrH",&nvalberrH,0);
      FitTree->SetBranchAddress("nvalbparabolicError",&nvalbparabolicError,0);
      FitTree->SetBranchAddress("nvals",&nvals,0);
      FitTree->SetBranchAddress("nvalserrL",&nvalserrL,0);
      FitTree->SetBranchAddress("nvalserrH",&nvalserrH,0);
      FitTree->SetBranchAddress("nvalsparabolicError",&nvalsparabolicError,0);
      FitTree->SetBranchAddress("eval",&eval,0);
      FitTree->SetBranchAddress("evalerrL",&evalerrL,0);
      FitTree->SetBranchAddress("evalerrH",&evalerrH,0);
      FitTree->SetBranchAddress("evalparabolicError",&evalparabolicError,0);
      FitTree->SetBranchAddress("hData"             ,&hData);              
      FitTree->SetBranchAddress("hMC"               ,&hMC  );              
      FitTree->SetBranchAddress("hMCn"              ,&hMCn );              
    }
  FitTree->Fill();
  FitTree->Write();
    //  hData->Write();
    //  hMC->Write();
    //  hMCn->Write();
  OutFile->Close();

  delete [] buffer;
}
