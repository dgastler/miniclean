#include <v1720FakeReadout.h>


bool v1720FakeReadout::Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager)
{
  //Find the memoryManager we need
  if(!FindMemoryManager(Mem,SetupNode,MemManager))
    {
      return  false;
    }
  
  //Find out what offset we should add when we loop
  if(FindSubNode(SetupNode,"LOOPOFFSET") == NULL)    
    {
      //Default value
      LoopOffset = 0;
    }
  else
    {
      GetXMLValue(SetupNode,"LOOPOFFSET",GetName().c_str(),LoopOffset);
    }
  
  //Find out if we should loop over the events or end after one loop
  int32_t _MemoryLoop = 1;
  if(FindSubNode(SetupNode,"MEMORYLOOP") == NULL)
    {
      //Default value
      MemoryLoop = true;
    }
  else
    {
      GetXMLValue(SetupNode,"MEMORYLOOP",GetName().c_str(),_MemoryLoop);
      MemoryLoop = _MemoryLoop;
    }

  //Get the input file name
  if(FindSubNode(SetupNode,"FILENAME") == NULL)
    {
      PrintError("FILENAME subnode not found");
      return false;
    }
  else    
    GetXMLValue(SetupNode,"FILENAME",GetName().c_str(),InFileName);
  InFile = fopen(InFileName.c_str(),"rb");
  if(!InFile)
    {
      printf("Error: %s not found.\n",InFileName.c_str());
      return false;
    }

  //Build the read file's full path.
  std::string fullPath;
  if(!std::getenv("PWD")){return false;};
  fullPath.assign(std::getenv("PWD"));
  fullPath+="/";
  fullPath+=InFileName;
  //Get file stats.
  struct stat fileStats;
  int statErr = stat(fullPath.c_str(),&fileStats);
  if(statErr){return false;}

  //Get the file's size in bytes and divide by four to get uint32_t size.
  //Allocate enough memory to hold the whole file in memory.
  FakeWFDData = new uint32_t[fileStats.st_size >> 2];
  if(!FakeWFDData) {return false;}
  FakeWFDDataSize = fileStats.st_size >>2;
  //read in the file.
  uint32_t readNumber = fread((uint8_t*)FakeWFDData,sizeof(uint8_t),FakeWFDDataSize <<2,InFile);
  if(readNumber != (FakeWFDDataSize <<2)){return false;}

  //Find events.
  for(uint32_t iWord = 0; iWord < FakeWFDDataSize;)
    {
      //Check for event header
      if((FakeWFDData[iWord]&0xF0000000) == 0xA0000000) 
	{
	  //Store pointer to this event.
	  FakeWFDEvent.push_back(FakeWFDData + iWord);
	  //Move forward by even size.
	  iWord += (FakeWFDData[iWord]&0x0FFFFFFF);
	}
      else
	{
	  iWord++;
	}
    }
  printf("v1720FakeReadout: Loaded %zu WFD events from file %s.\n",FakeWFDEvent.size(),fullPath.c_str());
 
  //Set the loop offset variable
  if(LoopOffset == 0)
    {
      //The user didn't specify an even offset value,
      //so use the event count
      FakeEventCount = FakeWFDEvent.size();
    }
  else
    {
      FakeEventCount = LoopOffset;
    }

  WFDsPerEvent = FakeWFDEvent.size()/FakeEventCount;

  //Get the wait time between events in frequency (overruled by ser period)
  SetEventPeriod(0);  //Default is max speed possible
  if(FindSubNode(SetupNode,"EVENTFREQUENCY") != NULL)
    {
      double freq = 0;
      GetXMLValue(SetupNode,"EVENTFREQUENCY",GetName().c_str(),freq);
      SetEventFrequency(freq);
    }
  //Get the wait time between events. (overrules set freq)
  if(FindSubNode(SetupNode,"EVENTPERIOD") != NULL)
    {
      int per;
      GetXMLValue(SetupNode,"EVENTPERIOD",GetName().c_str(),per);
      SetEventPeriod(per);
    }

  EventCountOffset = 0;
  Ready = true;

  //We no longer need fInFile open, so let's close it now.
  fclose(InFile);
  //Reset statistics
  lastTime = time(NULL);  

  //Set the current FAKE event index to zero
  iWFDEvent = 0;

  //Set real event ID and WFD event IDs to zero
  for(int iWFD=0;iWFD<14;iWFD++)
    {
      WFDEventID[iWFD] = 0;
      WFDFakeTime[iWFD] = iWFD;
    }

  return(true);
}

