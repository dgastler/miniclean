--Boston University Electronics Design Facility--Conor DuBois.
--Set device to xc6slx45 3csg324
--Project derived from work of Xilinx and Jeremy Mans.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity Atlys_top is Port(
	DEBUG : out STD_LOGIC_VECTOR(7 downto 0);

        -- trigger inputs from VENATOR
	PERIODIC : in STD_LOGIC;
	CALIBRATE : in STD_LOGIC_VECTOR(2 downto 0);
	VETO : in STD_LOGIC;
	EVNT : in STD_LOGIC;

        --Signal outputs for WFD triggering and run enable and clock
	ENABLED_P : out STD_LOGIC;
	ENABLED_N : out STD_LOGIC;
	TRIGGER_P : out STD_LOGIC;
	TRIGGER_N : out STD_LOGIC;
        HALF_CLOCK_P : out STD_LOGIC;
	HALF_CLOCK_N : out STD_LOGIC;

        -- Values for the 16bit LVDS fanout
	TRIGGER_ECHO : out STD_LOGIC_VECTOR(7 downto 0);
	COUNTERED : out STD_LOGIC_VECTOR(7 downto 0);

        --Buttons on the ATLYS board
	BUTTON : in STD_LOGIC_VECTOR(3 downto 0);

        --VENATOR LED display, 
	CODED_LED : out STD_LOGIC_VECTOR(5 downto 1);
	CLK_EXT : in STD_LOGIC;
	
        --Ethernet interface
	PHY_RXCLK : in STD_LOGIC;
	PHY_RESET_L : out STD_LOGIC;
	PHY_MDC : out STD_LOGIC;
	PHY_MDIO : inout STD_LOGIC;
	PHY_RXCTRL_RXDV : in STD_LOGIC;
	PHY_RXD : in STD_LOGIC_VECTOR(7 downto 0);
	PHY_TXC_GTXCLK : out STD_LOGIC;
	PHY_TXCTL_TXEN : out STD_LOGIC;
	PHY_TXD : out STD_LOGIC_VECTOR(7 downto 0);
	PHY_TXCLK : in STD_LOGIC;

        --clocks
        SYSCLK_N : in STD_LOGIC;        --62.5Mhz from WFD
	SYSCLK_P : in STD_LOGIC;
	ONBOARD_CLK : in STD_LOGIC);    --100Mhz from ATLYS board
end Atlys_top;

architecture Behavioral of Atlys_top is
	signal trigger_out : STD_LOGIC;
	signal trigger_out2 : STD_LOGIC;
	signal extra_clock : STD_LOGIC;
	signal next_extra : STD_LOGIC;
	signal enable : STD_LOGIC := '0';
	signal nenable : STD_LOGIC;
	signal computer : STD_LOGIC;
	signal clock_select : STD_LOGIC;
	signal trigger_mask : STD_LOGIC_VECTOR(7 downto 0);
	signal preclk125 : STD_LOGIC;
	signal clk125 : STD_LOGIC;
	signal clk125_rx : STD_LOGIC;
	signal feedback : STD_LOGIC;
	signal data_inbound : unsigned(31 downto 0);
	signal data_outbound : unsigned(31 downto 0);
	signal addr : unsigned(31 downto 0);
	signal strobe : STD_LOGIC;
	signal writing : STD_LOGIC;
	signal dtack : STD_LOGIC;
	type multiple is ARRAY(15 downto 1) of unsigned(31 downto 0);
	signal blocks : multiple;
	signal theBlock : unsigned(31 downto 0);
	constant myMAC : STD_LOGIC_VECTOR(47 downto 0) := x"00183E00D06C";
	constant myIP : STD_LOGIC_VECTOR(31 downto 0) := x"C0A80190";
	constant udpPort : STD_LOGIC_VECTOR(15 downto 0) := x"0317";
	signal txen : STD_LOGIC;
	signal stimulus : STD_LOGIC_VECTOR(2 downto 0) := "000";
	signal masked_trigger : STD_LOGIC_VECTOR(7 downto 0);
	signal accumulated_trigger : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal refresh : STD_LOGIC_VECTOR(7 downto 0);
	signal trigger_echo1 : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal trigger_echo2 : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal trigger_echo3 : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	signal clockee : unsigned(63 downto 0) := x"0000000000000000";
	signal clockee2 : unsigned(23 downto 0) := x"000000";
	signal gate : unsigned(15 downto 0) := x"0000";
	signal trigger_clock : STD_LOGIC;
	signal counter_out : unsigned(31 downto 0);
	signal counter : unsigned(31 downto 0) := x"00000001";
	signal reset : STD_LOGIC;
	signal event_counta : unsigned(11 downto 0);
	signal event_countb : unsigned(11 downto 0);
	signal event_count : unsigned(11 downto 0);--up to 4095 events can be stored
	signal trigger_time : unsigned(15 downto 0) := x"0000";
	signal trigger_time1 : unsigned(16 downto 0);
	signal wr_en : unsigned(3 downto 0) := x"0";
	signal rd_en : unsigned(3 downto 0) := x"0";
	type lessple is ARRAY(3 downto 0) of unsigned(31 downto 0);
	type alsople is ARRAY(3 downto 0) of unsigned(11 downto 0);
	signal inset : lessple;
	signal outset : lessple;
	signal count_set : alsople;
	signal out_of_set : unsigned(31 downto 0);
	signal addr2 : unsigned(1 downto 0);
	signal button_latch : STD_LOGIC;
	signal total_triggers : unsigned(27 downto 0) := x"0000000";
	signal single_trigger : STD_LOGIC;
	signal no_calibration : STD_LOGIC;
	signal sys_clk : STD_LOGIC;     --single ended version of differential
                                        --sysclock            
        --watchdog variables
	signal DCM_lock : STD_LOGIC;
        signal DCM_status : STD_LOGIC_VECTOR(7 downto 0);
        signal DCM_combo_lock_status : STD_LOGIC;

	signal watchdog_main_reset : STD_LOGIC;
        signal watchdog_DCM_reset : STD_LOGIC; 

        
	component sub_top port(
		CLK125 : in STD_LOGIC;
		CLK125_RX : in STD_LOGIC;
		PHY_RESET_L : out STD_LOGIC;
		PHY_MDC : out STD_LOGIC;
		PHY_MDIO : inout STD_LOGIC;
		PHY_RXCTRL_RXDV : in STD_LOGIC;
		PHY_RXD : in STD_LOGIC_VECTOR(7 downto 0);
		PHY_TXC_GTXCLK : out STD_LOGIC;
		PHY_TXCTL_TXEN : out STD_LOGIC;
		PHY_TXD : out STD_LOGIC_VECTOR(7 downto 0);
		PHY_TXCLK : in STD_LOGIC;
		myMAC : STD_LOGIC_VECTOR(47 downto 0);
		myIP : STD_LOGIC_VECTOR(31 downto 0);
		udpPort : STD_LOGIC_VECTOR(15 downto 0);
		DATA_I : in unsigned(31 downto 0);
		DATA_O : out unsigned(31 downto 0);
		STROBE : out STD_LOGIC;
		WRITING : out STD_LOGIC;
		DTACK : in STD_LOGIC;
		BERR : in STD_LOGIC;
		ADDR : out unsigned(31 downto 0);
		RESET : in STD_LOGIC);
	end component;
	
	component trigger_counter port(
		CLOCK : in STD_LOGIC;
		STIMULUS : in STD_LOGIC_VECTOR(1 downto 0);
		TRIGGER : in STD_LOGIC;
		COUNT : out unsigned(3 downto 0));
	end component;
	
	component event_queue port(
		RESET : in STD_LOGIC;
		CLK : in STD_LOGIC;
		DIN : in unsigned(31 downto 0);
		WR_EN : in STD_LOGIC;
		RD_EN : in STD_LOGIC;
		SAMPLES : out unsigned(11 downto 0);
		DOUT : out unsigned(31 downto 0));
	end component;
	
	component not_equals port(
		A : in  STD_LOGIC_VECTOR(16 downto 0);
		B : in  STD_LOGIC_VECTOR(17 downto 0);
		O : out  STD_LOGIC);
	end component;
		
        component vigilant_watchdog port(
                DCM_LOCK_STATUS : in STD_LOGIC;
                WATCHDOG_CLOCK : in STD_LOGIC;
                WATCHDOG_DCM_RESET : out STD_LOGIC;
                WATCHDOG_MAIN_RESET : out STD_LOGIC);
	end component;
      
begin
	controller: sub_top port map(
		CLK125 => clk125,
		CLK125_RX => clk125_rx,
		PHY_RESET_L => PHY_RESET_L,
		PHY_MDC => PHY_MDC,
		PHY_MDIO => PHY_MDIO,
		PHY_RXCTRL_RXDV => PHY_RXCTRL_RXDV,
		PHY_RXD => PHY_RXD,
		PHY_TXC_GTXCLK => PHY_TXC_GTXCLK,
		PHY_TXCTL_TXEN => txen,
		PHY_TXD => PHY_TXD,
		PHY_TXCLK => PHY_TXCLK,
		myMAC => myMAC,
		myIP => myIP,
		udpPort => udpPort,
		DATA_I => data_outbound,
		DATA_O => data_inbound,
		STROBE => strobe,
		WRITING => writing,
		DTACK => dtack,
		BERR => '0',
		ADDR => addr,
		RESET => reset);


        --generate all the clocks from the 62.5Mhz clock from the WFD.
        -- also talk to the watchdog so that we can restart the clock.
	DCM_SP_clk125tx : DCM_SP
          generic map(
		CLKDV_DIVIDE => 2.0, --we don't use this, so whatever 
		CLKFX_DIVIDE => 1, -- divide by 1 and then multiply by,
		CLKFX_MULTIPLY => 2, -- multiply by two to get to 125Mhz from 62.5.
		CLKIN_DIVIDE_BY_2 => FALSE, 
		CLKIN_PERIOD => 16.0,
		CLKOUT_PHASE_SHIFT => "NONE",
		CLK_FEEDBACK => "1X", 
		DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
		DLL_FREQUENCY_MODE => "HIGH",
		DUTY_CYCLE_CORRECTION => TRUE,
		PHASE_SHIFT => 0,
		STARTUP_WAIT => FALSE)
          port map (
		CLK0 => feedback,
		CLK180 => open,
		CLK270 => open,
		CLK2X => open,
		CLK2X180 => open,
		CLK90 => open,
		CLKDV => open,
		CLKFX => preclk125,
		CLKFX180 => open,
		LOCKED => DCM_lock,
		PSDONE => open,
		STATUS => DCM_status,                
		CLKFB => feedback,
                CLKIN => sys_clk,
		PSCLK => '0',
		PSEN => '0',
		PSINCDEC => '0',
                RST => watchdog_dcm_reset);


		

--convert the differential 62.5Mhz clock to an internal singal
	bufgsys_clk : IBUFGDS port map(
		I => SYSCLK_P,
		IB => SYSCLK_N,
		O => sys_clk);

-- clock stuff for Ethernet
	bufg125_tx : BUFG port map(
		O => clk125,
		I => preclk125);

	ibufg125rx : IBUFG port map(
		O => clk125_rx,
		I => PHY_RXCLK);

-- legacy external clock interface (not implemented in hardware...)
	clock_mux: BUFGMUX port map(
		O => trigger_clock,
		I0 => clk125,
		I1 => CLK_EXT,
		S => clock_select);
	
	lvds1: OBUFDS generic map(
		IOSTANDARD => "LVDS_25") port map(
		O => HALF_CLOCK_P,
		OB => HALF_CLOCK_N,
		I => extra_clock);

	lvds2: OBUFDS generic map(
		IOSTANDARD => "LVDS_25") port map(
		O => TRIGGER_P,
		OB => TRIGGER_N,
		I => trigger_out2);

	lvds3: OBUFDS generic map(
		IOSTANDARD => "LVDS_25") port map(
		O => ENABLED_P,
		OB => ENABLED_N,
		I => nenable);

        clock_watchdog: watchdog port map(
                DCM_LOCK_STATUS => DCM_combo_lock_status,    
                WATCHDOG_CLOCK => ONBOARD_CLK,
                WATCHDOG_DCM_RESET => watchdog_dcm_reset,
                WATCHDOG_MAIN_RESET => watchdog_main_reset);
        
	trigger_counters: for i in 0 to 7 generate
		trigger_counter0to7: trigger_counter port map(
			CLOCK => trigger_clock,
			STIMULUS => stimulus(2 downto 1),
			TRIGGER => trigger_echo2(i),
			COUNT => counter_out((4*i+3) downto (4*i)));
	end generate;
	
	retentive_registers: for i in 0 to 3 generate
		retentive_register0to3: event_queue port map(
			RESET => reset,
			CLK => trigger_clock,
			DIN => inset(i),
			WR_EN => wr_en(i),
			RD_EN => rd_en(i),
			SAMPLES => count_set(i),
			DOUT => outset(i));
	end generate;
	
	decoder: for i in 0 to 7 generate
		decoder0to7: not_equals port map(
			A => STD_LOGIC_VECTOR(clockee(16 downto 0)),
			B(17 downto 14) => STD_LOGIC_VECTOR(to_unsigned(i+1, 4)),
			B(13 downto 0) => "00000000000011",
			O => refresh(i));
	end generate;
	
        --ATLAS debugging LEDs
        DEBUG(0) <= DCM_lock;           --Debug LED 0 shows 62.5 Mhz clock lock
        DEBUG(1) <= not DCM_Status(1);      --clk_in valid on DCM
        DEBUG(2) <= not DCM_Status(2);  -- output is outputting :-p
        DEBUG(3) <= DCM_combo_lock_status;  --lock and not status(1)

        DEBUG(4) <= watchdog_main_reset;
        DEBUG(5) <= watchdog_dcm_reset;
        DEBUG(6) <= reset;
        
        DEBUG(7) <= clockee(26);         --signal that goes to the heartbeat LED
                                         --on VENATOR
        
        --signal that says the DCM is not working
        DCM_combo_lock_status <= (DCM_lock and (not DCM_Status(1)) and (not DCM_Status(2)));
        --reset signal made up of individual reset signals
	reset <= ((not BUTTON(1)) or watchdog_main_reset);

        --These should be commented
	next_extra <= not extra_clock;
	masked_trigger <= (PERIODIC & CALIBRATE & BUTTON(0) & computer & EVNT & veto) and trigger_mask;
	CODED_LED(5) <= clockee(26);--should flicker every half second or so
	computer <= blocks(4)(1);
	clock_select <= blocks(4)(2);
	no_calibration <= blocks(4)(3);
	trigger_mask <= STD_LOGIC_VECTOR(blocks(4)(11 downto 4));
	PHY_TXCTL_TXEN <= txen;
	rd_en <= x"0" when addr(31 downto 4) = x"0000000" or dtack = '1'
		or strobe = '0' or writing = '1' else x"1" when
		addr(1 downto 0) = "00" else x"2" when addr(1 downto 0) = "01"
		else x"4" when addr(1 downto 0) = "10" else x"8";
	wr_en(0) <= '1' when stimulus(2 downto 1) = "01" else '0';--start of trigger window
	wr_en(1) <= '1' when stimulus(2 downto 1) = "01" else '0';
	wr_en(2) <= '1' when stimulus(2 downto 1) = "10" else '0';--end of trigger window
	wr_en(3) <= '1' when stimulus(2 downto 1) = "01" else '0';
	inset(0) <= clockee(31 downto 0);
	inset(1) <= clockee(63 downto 32);
	inset(2) <= counter_out;
	inset(3) <= counter;
	data_outbound <= x"12345678" when addr = x"00000000" else
		event_count & theBlock(19 downto 1) & enable when addr = x"00000004"
		else theBlock when addr(31 downto 4) = x"0000000" else out_of_set;
	trigger_time1 <= ('0' & blocks(5)(31 downto 16)) + ('0' & x"00"& blocks(4)(19 downto 12));

	process(extra_clock)
	begin--extra clock inverted by LVDS
		if extra_clock'event and extra_clock = '1' then--so 1 is 0 and vice-versa
			nenable <= not enable;
			COUNTERED <= STD_LOGIC_VECTOR(counter(7 downto 0) xor 
				('0' & counter(7 downto 1)));--conversion to Gray code
			trigger_out2 <= trigger_out;
			TRIGGER_ECHO <= trigger_echo3;
		end if;
	end process;

	process(trigger_clock)
	begin
		if trigger_clock'event and trigger_clock = '1' then
			if clockee((27 - to_integer(blocks(7)(31 downto 28))) downto 0) 
				= (x"0000000") then
				total_triggers <= x"0000000";
			elsif stimulus(2 downto 1) = "01" then
				total_triggers <= total_triggers + x"0000001";
			else 
				total_triggers <= total_triggers;
			end if;
			
			extra_clock <= next_extra;
			trigger_echo2 <= trigger_echo1;
			trigger_echo1 <= masked_trigger;
			stimulus(2) <= stimulus(1);
			button_latch <= BUTTON(3);
			if enable = '0' or masked_trigger(0) = '1' or masked_trigger(7 downto 1) = "0000000" 
				or ((blocks(7)(27 downto 0) /= x"0000000") and (total_triggers >= blocks(7)(27 downto 0))) then
				stimulus(0) <= '0';
			else
				stimulus(0) <= '1';
			end if;
			if stimulus(2 downto 1) = "01" then
			   trigger_echo3 <= trigger_echo2;--responsible pattern is latched for the duration of the gate
			end if;
			
			if gate = x"0000" then
				if stimulus(0) = '1' then
					gate <= blocks(5)(15 downto 0);
					stimulus(1) <= '1';
					if trigger_time1 < ('0' & blocks(5)(15 downto 0)) then
						trigger_time <= trigger_time1(15 downto 0);
					else
						trigger_time <= blocks(5)(15 downto 0);
					end if;
				elsif single_trigger = '1' and no_calibration = '0' then--send a single trigger during initialization/calibration
					gate <= blocks(5)(15 downto 0);
					stimulus(1) <= '0';--only affects WFDs
					if trigger_time1 < ('0' & blocks(5)(15 downto 0)) then
						trigger_time <= trigger_time1(15 downto 0);
					else
						trigger_time <= blocks(5)(15 downto 0);
					end if;
				end if;
				trigger_out <= '1';
			elsif gate = x"0001" then
				gate <= x"0000";
				stimulus(1) <= '0';
				trigger_out <= '1';
			else
				gate <= gate - x"0001";
				if trigger_time = x"0000" then
					trigger_out <= '1';
				elsif trigger_time <= (x"00" & blocks(4)(19 downto 12)) then
					trigger_out <= '0';--trigger active only when so instructed
					trigger_time <= trigger_time - x"0001";
				else
					trigger_out <= '1';
					trigger_time <= trigger_time - x"0001";
				end if;
			end if;
			
			--leds serialized to 60 kHz: minimum time an led will be lit is 1 millisecond
			CODED_LED(4 downto 2) <= STD_LOGIC_VECTOR(clockee(16 downto 14));
			CODED_LED(1) <= accumulated_trigger(to_integer(clockee2(23 downto 21)));
			accumulated_trigger <= (accumulated_trigger or (masked_trigger(7 downto 1)
				& enable)) and refresh;--replace veto LED with enable LED
			clockee2 <= clockee2(20 downto 0) & clockee(16 downto 14);
			
			if count_set(0) < count_set(1) then
				event_counta <= count_set(0);
			else
				event_counta <= count_set(1);
			end if;
			if count_set(2) < count_set(3) then
				event_countb <= count_set(2);
			else
				event_countb <= count_set(3);
			end if;
			if event_counta < event_countb then--event_count is the minimum/infimum
				event_count <= event_counta;--of values in count_set
			else
				event_count <= event_countb;
			end if;
			
			if reset = '1' then
				clockee <= x"0000000000000000";
			elsif writing = '1' and strobe = '1' and addr = x"00000001" then
				clockee(31 downto 0) <= data_inbound;
			elsif writing = '1' and strobe = '1' and addr = x"00000002" then
				clockee(63 downto 32) <= data_inbound;
			else
				clockee <= clockee + x"0000000000000001";
			end if;
			if reset = '1' or BUTTON(2) = '1' then
				counter <= x"00000001";
			elsif writing = '1' and strobe = '1' and addr = x"00000003" then
				counter <= data_inbound;
			elsif stimulus(2 downto 1) = "10" then
				counter <= counter + x"00000001";
			end if;
			if reset = '1' then
				enable <= '0';
				single_trigger <= '0';
			elsif writing = '1' and strobe = '1' and addr = x"00000004" then
				enable <= data_inbound(0);
				if enable = '0' and data_inbound(0) = '1' then
					single_trigger <= '1';--send single trigger on rising edge of enable
				else
					single_trigger <= '0';
				end if;
			elsif BUTTON(3) = '1' and button_latch = '0' then--enable toggled by button
				enable <= not enable;
				if enable = '0' then
					single_trigger <= '1';
				else
					single_trigger <= '0';
				end if;
			end if;
			if writing = '1' and strobe = '1' and 
				addr(31 downto 4) = x"0000000" and addr(3 downto 2) /= "00" then
				blocks(to_integer(addr(3 downto 0))) <= data_inbound;
			end if;
			theBlock <= blocks(to_integer(addr(3 downto 0)));
			dtack <= strobe;
			out_of_set <= outset(to_integer(addr2));
			addr2 <= addr(1 downto 0);
			blocks(1) <= clockee(31 downto 0);
			blocks(2) <= clockee(63 downto 32);
			blocks(3) <= counter;
		end if;
	end process;
end Behavioral;
