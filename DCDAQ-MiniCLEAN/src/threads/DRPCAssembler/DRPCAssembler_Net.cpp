#ifdef __RATDS__
#include <DRPCAssembler.h>

void DRPCAssembler::DeleteConnection(size_t iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;

  RemoveReadFD(server[iConn].GetFullPacketFD());
  RemoveReadFD(server[iConn].GetMessageFD());
  SetupSelect();
  server.DeleteConnection(iConn);
}

bool DRPCAssembler::ProcessNewConnection()
{
  //create a DCNetThread for this 
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().GetFullPacketFD());
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().GetMessageFD());
      printf("FEPC connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().GetRemoteAddress().GetAddrStr().c_str());
      SetupSelect();
    }
 else
   {
     return false;
   }
  return true;
}
#endif
