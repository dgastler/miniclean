#ifdef __CAENLIBS__
#include <v1720FakeTrigReadout.h>

v1720FakeTrigReadout::v1720FakeTrigReadout(std::string Name)
{
  SetType("v1720FakeTrigReadout");
  SetName(Name);
  blockIndex = FreeFullQueue::BADVALUE;
  block = NULL;
  Mem = NULL;
  FakeWFDData = NULL;
  blockIndex = FreeFullQueue::BADVALUE;
}

v1720FakeTrigReadout::~v1720FakeTrigReadout()
{
  //  Cleanup setup data
  while(BaseXMLNodes.size())
    {
      BaseXMLNodes.pop_back();
    }
  xmlCleanupParser();

  FakeWFDEvent.clear();
  if(FakeWFDData)
    delete [] FakeWFDData;
  Mem->Shutdown();
}

bool v1720FakeTrigReadout::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      //Stop all the WFDS
      StopWFDs();
      //Shutdown the memory manager
      RemoveReadFD(Mem->GetFreeFDs()[0]);
      SetupSelect();
      Mem->Shutdown();
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      uint64_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Remove the memory manager's free queue from the select mask
      RemoveReadFD(Mem->GetFreeFDs()[0]);
      SetupSelect();
      //Stop the WFDs 
      StopWFDs();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //      SetSelectTimeout(0,2000);
      uint64_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());

      //Setup each of the WFDs
      //      SetupWFDs();
      
      //Start each WFD
      StartWFDs();

      //Get the free block queue's FDs and add it to the select set

      AddReadFD(Mem->GetFreeFDs()[0]);
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}

bool v1720FakeTrigReadout::StartWFDs()
{
  bool ret = true;
  uint32_t nWFDs = WFD.size();
  int startRet = 0;

  //Start the WFDS
  for(unsigned int iWFD = 0;
      (iWFD < nWFDs);
      iWFD++)
    {	      
      if((startRet = WFD[iWFD]->Start()) != 0)
	{
	  char * buffer = new char[100];
	  sprintf(buffer,
		  "WFD %d start failed with error code %d",
		  iWFD,startRet);
	  PrintError(buffer);
	  delete [] buffer;
	  ret = false;
	}
    }
  return(ret);
}

bool v1720FakeTrigReadout::StopWFDs()
{
  bool ret = true;
  uint32_t nWFDs = WFD.size();
  int stopRet = 0;

  //Stop the WFDS
  for(unsigned int iWFD = 0; iWFD < nWFDs;iWFD++)
    {
      if((stopRet = WFD[iWFD]->Stop()) != 0)
	{	  
	  char * buffer = new char[100];
	  sprintf(buffer,
		  "WFD %d stop failed with error code %d",
		  iWFD,stopRet);
	  PrintError(buffer);
	  delete [] buffer;
	  ret = false;
	}
    }
  return(ret);
}

#endif
