#ifdef __RATDS__
#include <DRGetter.h>

void DRGetter::ProcessDecision()
{  
  //  EventDecision decision;
  DRDecisionPacket::Event decision;
  //Get the next decision in the queue
  if(DecisionQueue.Get(decision,true))
    {
    }
  else
    {
      PrintWarning("Can not read from DecisionQueue!");
      return;
    }
 #ifdef __DRGETTER_WRITE_DECISIONS__
  if(outfile==NULL)
    {
      char * nameBuffer = new char[100];
      sprintf(nameBuffer,"DRGetter_decisions_run_%d",GetRunNumber());
      outfile = fopen(nameBuffer,"w");
      delete [] nameBuffer;
    }
  fprintf(outfile,
	  "wfdEventID: 0x%08X\neventID: 0x%016zX\neventTime: 0x%016zX\neventTriggers: 0x%08X\nreductionLevel: %u\n",
	  decision.wfdEventID,
	  (uint64_t)decision.eventID,
	  (uint64_t)decision.eventTime,
	  decision.eventTriggers,
	  decision.reductionLevel);
  #endif
  #ifdef DEBUG_ROLLOVER
  if(decision.eventID >= pow(2,DEBUG_ROLLOVER) - 5)
    {
      printf("DRGetter: Got decision\n   wfdEventID: 0x%08X\neventID: 0x%016zX\neventTime: 0x%016zX\neventTriggers: 0x%08X\nreductionLevel: %u\n",
	  decision.wfdEventID,
	  (uint64_t)decision.eventID,
	  (uint64_t)decision.eventTime,
	  decision.eventTriggers,
	  decision.reductionLevel);
    }
  #endif
  //Get this event from the Event hash
  int32_t evIndex = FEPC_mem->GetBadValue();      
  uint64_t password = FEPC_mem->GetStorageBlankPassword();
  int64_t eventKey = FEPC_mem->GetKey(decision);
  RATEVBlock * evBlock = NULL;
  //Get using WFD event ID
  int64_t retFEPCmem = FEPC_mem->GetStorageEvent(eventKey,
						 evIndex,
						 password);
  if(retFEPCmem == returnDCVector::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    { 
      #ifdef DEBUG_ROLLOVER
      if(decision.eventID >= (pow(2,DEBUG_ROLLOVER) - 5))
	{
	  printf("DRGetter: Got OK from storage for event %zi\n",decision.eventID);
	}
      #endif
      //Get our evBlock
      evBlock = FEPC_mem->GetFEPCBlock(evIndex);
      //Update the reduction decision.
      evBlock->reductionLevel = decision.reductionLevel;
      //Set the true event ID
      evBlock->eventID = decision.eventID;
      evBlock->ev->SetEventID(decision.eventID);
      evBlock->eventTime = decision.eventTime;
      evBlock->eventTriggers = decision.eventTriggers;

      //Remove the entry from the storage VH
      if(FEPC_mem->ClearStorageEvent(eventKey,password) !=
	 returnDCVector::OK)
	{	  
	  PrintError("ClearStorageEvent failed!");
	}
      else
	{
	  if(decision.reductionLevel == ReductionLevel::SAVE_NOTHING)
	    //We're done
	    {
	      return;
	    }
	  //Add the index to the Send queue
	  if(FEPC_mem->AddSend(evIndex))
	    {
	    }
	  else
	    {
	      PrintError("Could not add index to send queue!");
	    }
	}
    }
  else if(retFEPCmem >= returnDCVector::COLLISION)    
    //We have a collision
    {
      //Return the event to the DecisionQueue
      if(decision.attempts < maxDecisionAttempts)
	{
#ifdef DEBUG_ROLLOVER
	  if(decision.eventID >= pow(2,DEBUG_ROLLOVER) - 5)
	    {
	      printf("DRGetter: Collision on event %zi; keys: %zd,%zd\n",decision.eventID,eventKey,retFEPCmem);
	    }
#endif
	  decision.attempts++;
	  DecisionQueue.Add(decision,true);
	  char * textBuffer = new char[100];
	  sprintf(textBuffer,"Collision in DRGetter(key=%"PRId64",%"PRId64").\n",eventKey,retFEPCmem);
	  fprintf(stderr,"%s\n",textBuffer);
	  PrintWarning(textBuffer);
	  delete [] textBuffer;      
	}
      else
	{
#ifdef DEBUG_ROLLOVER
	  if(decision.eventID >= pow(2,DEBUG_ROLLOVER) - 5)
	    {
	      printf("DRGetter: Dropping collision on event %zi; keys: %zd,%zd\n",decision.eventID,eventKey,retFEPCmem);
	    }
#endif
	  char * textBuffer = new char[100];
	  sprintf(textBuffer,"Dropping Collision in DRGetter(key=%"PRId64",%"PRId64").\n",eventKey,retFEPCmem);
	  fprintf(stderr,"%s\n",textBuffer);
	  PrintWarning(textBuffer);
	  delete [] textBuffer;      
	}
      FEPC_mem->ReleaseStorageEvent(evIndex,password);
      //MoveToBad(retFEPCmem,Level::STORAGE_COLLISION,password);
    }
  else if(retFEPCmem == returnDCVector::EMPTY_KEY)
    //We have an empty value... this is very strange
    {
#ifdef DEBUG_ROLLOVER
      if(decision.eventID >= pow(2,DEBUG_ROLLOVER) - 5)
	{
	  printf("DRGetter: Empty key on event %zi; key: %zd, evIndex: %zi\n",decision.eventID,eventKey,retFEPCmem);
	}
#endif
      char * textBuffer = new char[100];
      sprintf(textBuffer,"Empty key in DRGetter!\n");
      sprintf(textBuffer,
	      "Lost decision.key: %"PRId64"  evIndex: %i\n",
	      FEPC_mem->GetKey(decision),evIndex);
      printf(textBuffer);
      decision.Print();
      delete [] textBuffer;
      MoveToBad(retFEPCmem,Level::STORAGE_EMPTY,password);
    }
  else if(retFEPCmem == returnDCVector::BAD_PASSWORD)
    {
#ifdef DEBUG_ROLLOVER
      if(decision.eventID >= pow(2,DEBUG_ROLLOVER) - 5)
	{
	  printf("DRGetter: Bad password on event %zi\n",decision.eventID);
	}
#endif
      //Return the event to the DecisionQueue
      if(decision.attempts < maxDecisionAttempts)
	{
	  decision.attempts++;
	  DecisionQueue.Add(decision,true);
	  //char * textBuffer = new char[100];
	  //sprintf(textBuffer,"Bad password(0x%"PRIx64") in DRGetter(key=%"PRId64").\n",password,eventKey);
	  //fprintf(stderr,"%s\n",textBuffer);
	  //	  PrintWarning(textBuffer);
	  // delete [] textBuffer;      
	}
      else
	{
	  char * textBuffer = new char[100];
	  sprintf(textBuffer,"Dropping Bad password(%"PRIx64") in DRGetter(key=%"PRId64").\n",password,eventKey);
	  fprintf(stderr,"%s\n",textBuffer);
	  PrintWarning(textBuffer);
	  delete [] textBuffer;      
	}
    }
  else if(retFEPCmem == returnDCVector::BAD_MUTEX)
    {
#ifdef DEBUG_ROLLOVER
      if(decision.eventID >= pow(2,DEBUG_ROLLOVER) - 5)
	{
	  printf("DRGetter: Bad mutex on event %zi\n",decision.eventID);
	}
#endif

      if(decision.attempts < maxDecisionAttempts)
	{
	  decision.attempts++;
	  //Return the event to the DecisionQueue
	  DecisionQueue.Add(decision,true);
	  char * textBuffer = new char[40];
	  sprintf(textBuffer,"Bad mutex in DRGetter.\n");
	  fprintf(stderr,"%s\n",textBuffer);
	  PrintWarning(textBuffer);
	  delete [] textBuffer;
	}
      else
	{
	  char * textBuffer = new char[60];
	  sprintf(textBuffer,"Dropping Bad mutex in DRGetter.\n");
	  fprintf(stderr,"%s\n",textBuffer);
	  PrintWarning(textBuffer);
	  delete [] textBuffer;
	}
    }
  else
    //Some other error that should never happen
    {
      char * buffer = new char[1000];
      sprintf(buffer,"Error in FEPC GetEvent: %"PRId64"(%u)\n",
	      retFEPCmem,
	      decision.wfdEventID);
      PrintError(buffer);
      delete [] buffer;
    }
}

void DRGetter::MoveToBad(int64_t eventKey,Level::Type errorCode, uint64_t password)
{
  int32_t badIndex = FEPC_mem->GetBadValue();
  if(FEPC_mem->AddBadEvent(eventKey,
			   badIndex,
			   errorCode))
    {	 
      if(errorCode != Level::STORAGE_EMPTY)
	{
	  FEPC_mem->ClearStorageEvent(eventKey,password);
	}
      else
	{
	  //THere is no entry in the event storage in this case, so dont' clear it.
	}
    }
  else
    {
      PrintError("Could not add event to bad queue");
    }
}
#endif
