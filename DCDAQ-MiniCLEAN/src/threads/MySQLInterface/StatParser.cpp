 #include <StatParser.h>
void StatParser::Setup()
{
  ssMap[StatMessage::EVENT_RATE] = &StatParser::ProcessRate;
  ssMap[StatMessage::PACKET_RATE] = &StatParser::ProcessRate;
  ssMap[StatMessage::DATA_RATE] = &StatParser::ProcessRate;
  ssMap[StatMessage::REDUCTION_RATE] = &StatParser::ProcessRate;
  ssMap[StatMessage::BAD_EVENT_RATE] = &StatParser::ProcessRate;
  ssMap[StatMessage::READOUT_RATE] = &StatParser::ProcessRate;

  ssMap[StatMessage::OCCUPANCY] = &StatParser::ProcessFloat;
  ssMap[StatMessage::BASELINE] = &StatParser::ProcessFloatWithoutSource;
  ssMap[StatMessage::ZLECOUNT] = &StatParser::ProcessFloatWithoutSource;

  ssMap[StatMessage::FEPC] = &StatParser::ProcessFEPC;
  ssMap[StatMessage::DRPC] = &StatParser::ProcessDRPC;
  ssMap[StatMessage::RAT_DISK] = &StatParser::ProcessRAT_DISK;
  ssMap[StatMessage::FREE_FULL] = &StatParser::ProcessFREE_FULL;

  shortName_StatMap[StatMessage::PACKET_RATE]    = "PRT"; 
  shortName_StatMap[StatMessage::DATA_RATE]      = "DRT"; 
  shortName_StatMap[StatMessage::READOUT_RATE]   = "RORT"; 
  shortName_StatMap[StatMessage::EVENT_RATE]     = "ERT"; 
  shortName_StatMap[StatMessage::REDUCTION_RATE] = "RRT"; 
  shortName_StatMap[StatMessage::BAD_EVENT_RATE] = "BRT"; 
  shortName_StatMap[StatMessage::OCCUPANCY]      = "OCC"; 

  shortName_LevelMap[Level::CHAN_FULL_WAVEFORM] = "CFW";
  shortName_LevelMap[Level::CHAN_ZLE_WAVEFORM]  = "CZQ";
  shortName_LevelMap[Level::CHAN_ZLE_INTEGRAL]  = "CZI"; 
  shortName_LevelMap[Level::CHAN_PROMPT_TOTAL]  = "CPT"; 
  shortName_LevelMap[Level::EVNT_FULL_WAVEFORM] = "EFW";
  shortName_LevelMap[Level::EVNT_ZLE_WAVEFORM]  = "EZQ"; 
  shortName_LevelMap[Level::EVNT_ZLE_INTEGRAL]  = "EZI"; 
  shortName_LevelMap[Level::EVNT_PROMPT_TOTAL]  = "EPT"; 
  shortName_LevelMap[Level::RAW_EVENT]  = "RAW"; 

  shortName_NameMap["FEPC1_V1720"] = "FE1";
  shortName_NameMap["FEPC2_V1720"] = "FE2";
  shortName_NameMap["FEPC3_V1720"] = "FE3";
  shortName_NameMap["FEPC4_V1720"] = "FE4";

  shortName_NameMap["FEPC1_Assembler"] = "FE1_ASM";
  shortName_NameMap["FEPC2_Assembler"] = "FE2_ASM";
  shortName_NameMap["FEPC3_Assembler"] = "FE3_ASM";
  shortName_NameMap["FEPC4_Assembler"] = "FE4_ASM";

  shortName_NameMap["FEPC1_DRPusher"] = "FE1_DRP";
  shortName_NameMap["FEPC2_DRPusher"] = "FE2_DRP";
  shortName_NameMap["FEPC3_DRPusher"] = "FE3_DRP";
  shortName_NameMap["FEPC4_DRPusher"] = "FE4_DRP";

  shortName_NameMap["FEPC1_DRGetter"] = "FE1_DRG";
  shortName_NameMap["FEPC2_DRGetter"] = "FE2_DRG";
  shortName_NameMap["FEPC3_DRGetter"] = "FE3_DRG";
  shortName_NameMap["FEPC4_DRGetter"] = "FE4_DRG";

  shortName_NameMap["FEPC1_EBPusher"] = "FE1_EBP";
  shortName_NameMap["FEPC2_EBPusher"] = "FE2_EBP";
  shortName_NameMap["FEPC3_EBPusher"] = "FE3_EBP";
  shortName_NameMap["FEPC4_EBPusher"] = "FE4_EBP";

  shortName_NameMap["FEPC1_BadEvent"] = "FE1_BAD";
  shortName_NameMap["FEPC2_BadEvent"] = "FE2_BAD";
  shortName_NameMap["FEPC3_BadEvent"] = "FE3_BAD";
  shortName_NameMap["FEPC4_BadEvent"] = "FE4_BAD";

  shortName_NameMap["DRPC_Assembler"] = "DR_ASM";
  shortName_NameMap["DRPC_Trigger"] = "DR_TRG";
  shortName_NameMap["Decision_Maker"] = "DM";
  shortName_NameMap["FE_Pusher"] = "FE_PSH";
  shortName_NameMap["EBPC_Assembler"] = "EB_ASM";
  shortName_NameMap["EBPC_Throw_Away"] = "EB_TWA";  
  shortName_NameMap["DRPC_BadEvent"] = "DR_BAD";
  shortName_NameMap["EBPCwrite"] = "EB_RATW";

  shortName_NameMap["FEPC1_MM"]="FE1MM";
  shortName_NameMap["FEPC2_MM"]="FE2MM";
  shortName_NameMap["FEPC3_MM"]="FE3MM";
  shortName_NameMap["FEPC4_MM"]="FE4MM";

  shortName_NameMap["DRPC_MM"]="DRMM";
  shortName_NameMap["EBPC_MM"]="EBMM";

  shortName_NameMap["MBLK1_MM"]="MBMM1";
  shortName_NameMap["MBLK2_MM"]="MBMM2";
  shortName_NameMap["MBLK3_MM"]="MBMM3";
  shortName_NameMap["MBLK4_MM"]="MBMM4";
  shortName_NameMap["RAWMBLK_MM"]="RBMM";
  shortName_NameMap["CMPMBLK_MM"]="CBMM";
}

std::string StatParser::ProcessStat(DCMessage::Message & message)
{
  StatMessage::StatBase ssBase;
  if(!message.GetData(ssBase))
    std::string();
  
  std::map< StatMessage::Type,
	    std::string (StatParser::*)(DCMessage::Message & message)>::iterator it;
  it = ssMap.find(ssBase.type);
  if(it == ssMap.end())
    return std::string();
  return ((*this).*(it->second))(message);
}

std::string StatParser::ProcessRate(DCMessage::Message &message)
{
  StatMessage::StatRate ssRate;
  if(!message.GetData(ssRate))
    return std::string();

  std::stringstream ss;
   
  ss << "INSERT INTO sc_sens_";
  ss << NameText(message.GetSource().GetName());
  ss << StatText(ssRate.sBase.type);
  if(ssRate.sInfo.subID != SubID::BLANK)
    {
      ss << SubIDText(ssRate.sInfo.subID);
    }
  if(ssRate.sInfo.level != Level::BLANK)
    {
      ss << LevelText(ssRate.sInfo.level);
    }
  ss << " (time, value)";
  ss << " VALUES ";
  ss << " (" << ssRate.sInfo.time
     << " , "
     << double(ssRate.count)/double(ssRate.sInfo.interval)  << " ) ";
  
  //  printf("%s\n",ss.str().c_str());  
  return ss.str();
}

std::string StatParser::ProcessFloat(DCMessage::Message & message)
{
  StatMessage::StatFloat ssFloat;
  if(!message.GetData(ssFloat))
    return std::string();

  std::stringstream ss;

  ss << "INSERT INTO sc_sens_";
  ss << NameText(message.GetSource().GetName());
  ss << StatText(ssFloat.sBase.type);
  if(ssFloat.sInfo.subID != SubID::BLANK)
    {
      ss << SubIDText(ssFloat.sInfo.subID);
    }
  if(ssFloat.sInfo.level != Level::BLANK)
    {
      ss << LevelText(ssFloat.sInfo.level);
    }
  ss << " (time, value)";
  ss << " VALUES ";
  ss << " (" << ssFloat.sInfo.time << " , " << ssFloat.val  << " ) ";
  
  //  printf("%s\n",ss.str().c_str());
  return ss.str();
}

std::string StatParser::ProcessFloatWithoutSource(DCMessage::Message & message)
{
  StatMessage::StatFloat ssFloat;
  if(!message.GetData(ssFloat))
    return std::string();

  std::stringstream ss;

  ss << "INSERT INTO sc_sens_";
  ss << StatText(ssFloat.sBase.type);
  ss << "_";
  if(ssFloat.sInfo.subID != SubID::BLANK)
    {
      ss << SubIDText(ssFloat.sInfo.subID);
    }
  if(ssFloat.sInfo.level != Level::BLANK)
    {
      ss << LevelText(ssFloat.sInfo.level);
    }
  ss << " (time, value)";
  ss << " VALUES ";
  ss << " (" << ssFloat.sInfo.time << " , " << ssFloat.val  << " ) ";
  
  //  printf("%s\n",ss.str().c_str());
  return ss.str();
}

std::string StatParser::SubIDText(SubID::Type type)
{
  std::string ret;
  std::map<SubID::Type,std::string>::iterator it;
  it = shortName_SubIDMap.find(type);
  if(it!=shortName_SubIDMap.end())
    ret =  it->second;
  else 
    ret = SubID::GetTypeName(type);
  return ret;
}
std::string StatParser::LevelText(Level::Type type)
{
  std::string ret;
  std::map<Level::Type,std::string>::iterator it;
  it = shortName_LevelMap.find(type);
  if(it!=shortName_LevelMap.end())
    ret =  it->second;
  else 
    ret = Level::GetTypeName(type);
  return ret;
}

std::string StatParser::StatText(StatMessage::Type type)
{
  std::string ret;
  std::map<StatMessage::Type,std::string>::iterator it;
  it = shortName_StatMap.find(type);
  if(it!=shortName_StatMap.end())
    ret =  it->second;
  else 
    ret = StatMessage::GetTypeName(type);
  return ret;
}

std::string StatParser::NameText(std::string type)
{
  std::string ret;
  std::map<std::string,std::string>::iterator it;
  it = shortName_NameMap.find(type);
  if(it!=shortName_NameMap.end())
    ret =  it->second;
  else 
    ret = type;
  return ret;
}

std::string StatParser::ProcessFEPC(DCMessage::Message & message)
{
  StatMessage::StatFEPCMM ssFEPC;
  if(!message.GetData(ssFEPC))
    return std::string();
  std::vector<std::string> dataNames;
  std::vector<uint64_t> data;
  dataNames.push_back("_BADE");
  dataNames.push_back("_FREE");
  dataNames.push_back("_ASMB");
  dataNames.push_back("_STOR");
  dataNames.push_back("_USNT");
  dataNames.push_back("_TSND");
  data.push_back(ssFEPC.badEvent.size);
  data.push_back(ssFEPC.free.size);
  data.push_back(ssFEPC.assembled.active);
  data.push_back(ssFEPC.storage.active);
  data.push_back(ssFEPC.unSent.size);
  data.push_back(ssFEPC.send.size);
  std::stringstream ss;
  for(size_t i=0;i<data.size();i++)
    {
      ss << "INSERT INTO sc_sens_";
      ss << NameText(message.GetSource().GetName());
      ss << dataNames.at(i);
      ss << " (time, value )";
      ss << " VALUES ";
      ss << " (" << ssFEPC.sInfo.time << " , " 
	 << data.at(i)<< " ); ";
    }
  //  printf("%s\n",ss.str().c_str());
  return ss.str();  
}

/*std::string StatParser::ProcessFEPC(DCMessage::Message & message)
{
  StatMessage::StatFEPCMM ssFEPC;
  if(!message.GetData(ssFEPC))
    return std::string();
  
  std::stringstream ss;
  ss << "INSERT INTO sc_sens_";
  ss << message.GetSource().GetName();
  ss << " (time, badevent, free, active_assembled, active_stored, unsent, tosend )";
  ss << " VALUES ";
  ss << " (" << ssFEPC.sInfo.time << " , " 
     << ssFEPC.badEvent.size  << " , " 
     << ssFEPC.free.size  << " , " 
     << ssFEPC.assembled.active  << " , " 
     << ssFEPC.storage.active  << " , " 
     << ssFEPC.unSent.size  << " , " 
     << ssFEPC.send.size << " ) ";
  
  //  printf("%s\n",ss.str().c_str());
  return ss.str();  
  }*/

std::string  StatParser::ProcessDRPC(DCMessage::Message & message)
{
  StatMessage::StatDRPCMM ssDRPC;
  if(!message.GetData(ssDRPC))
    return std::string();
  std::vector<std::string> dataNames;
  std::vector<uint64_t> data;
  dataNames.push_back("_BADE");
  dataNames.push_back("_FREE");
  dataNames.push_back("_UNPR");
  dataNames.push_back("_EVTST");
  dataNames.push_back("_RESP");
  dataNames.push_back("_EBPC");
  data.push_back(ssDRPC.badEvent.size);
  data.push_back(ssDRPC.free.size);
  data.push_back(ssDRPC.unProcessed.size);
  data.push_back(ssDRPC.eventStore.active);
  data.push_back(ssDRPC.respond.size);
  data.push_back(ssDRPC.EBPC.size);
  std::stringstream ss;
  for(size_t i=0;i<data.size();i++)
    {
      ss << "INSERT INTO sc_sens_";
      ss << NameText(message.GetSource().GetName());
      ss << dataNames.at(i);
      ss << " (time, value )";
      ss << " VALUES ";
      ss << " (" << ssDRPC.sInfo.time << " , " 
	 << data.at(i)<< " ); ";
    }
  
  //  printf("%s\n",ss.str().c_str());
  return ss.str();  
}
std::string  StatParser::ProcessRAT_DISK(DCMessage::Message & message)
{
  StatMessage::StatRATDiskMM ssRAT_DISK;
  if(!message.GetData(ssRAT_DISK))
    return std::string();
  std::vector<std::string> dataNames;
  std::vector<uint64_t> data;
  dataNames.push_back("_BADE");
  dataNames.push_back("_FREE");
  dataNames.push_back("_STOR");
  dataNames.push_back("_TDSK");
  data.push_back(ssRAT_DISK.badEvent.size);
  data.push_back(ssRAT_DISK.free.size);
  data.push_back(ssRAT_DISK.eventStore.active);
  data.push_back(ssRAT_DISK.toDisk.size);
   std::stringstream ss;
  for(size_t i=0;i<data.size();i++)
    {
      ss << "INSERT INTO sc_sens_";
      ss << NameText(message.GetSource().GetName());
      ss << dataNames.at(i);
      ss << " (time, value )";
      ss << " VALUES ";
      ss << " (" << ssRAT_DISK.sInfo.time << " , " 
	 << data.at(i)<< " ); ";
    }
  
  //  printf("%s\n",ss.str().c_str());
  return ss.str();  
}
std::string StatParser::ProcessFREE_FULL(DCMessage::Message & message)
{
  StatMessage::StatFreeFull ssFREE_FULL;
  if(!message.GetData(ssFREE_FULL))
    return std::string();
  std::vector<std::string> dataNames;
  std::vector <uint64_t> data;
  dataNames.push_back("_FREE");
  dataNames.push_back("_FULL");
  data.push_back(ssFREE_FULL.free.size);
  data.push_back(ssFREE_FULL.full.size);
  std::stringstream ss;
  for(size_t i=0;i<data.size();i++)
    {
      ss << "INSERT INTO sc_sens_";
      ss << NameText(message.GetSource().GetName());
      ss << dataNames.at(i);
      ss << " (time, value )";
      ss << " VALUES ";
      ss << " (" << ssFREE_FULL.sInfo.time << " , " 
	 << data.at(i)<< " ); ";
    }
  
  //  printf("%s\n",ss.str().c_str());
  return ss.str();  
}
