#include <BenchAssembler.h>

BenchAssembler::BenchAssembler(std::string Name)
{
  SetName(Name);
  SetType(std::string("BenchAssembler"));

  //EBPC memory manager
  EBPC_mem = NULL;
  collisionCount   = 0;
  lockBlockedCount = 0;

  //memBlock memory manager
  Block_mem = NULL;
  memBlock = NULL;
  memBlockIndex = FreeFullQueue::BADVALUE;
  
  //Statistics
  lastTime = time(NULL);
  currentTime = time(NULL);

  //Reset counters
  timeStepLockBlockedCount = 0;
  collisionCount = 0;


}

bool BenchAssembler::Setup(xmlNode * SetupNode,
			 std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;

  //================================
  //Load the map of board IDs and channels 
  //for the WFDs processed by this thread.
  //================================
  //pattern of data needed for this thread
  uint32_t neededInfo = (InfoTypes::PMT_COUNT |
			 InfoTypes::CHANNEL_MAP
			 );
  //Parse the data and return the pattern of parsed data
  uint32_t parsedInfo = detectorInfo.Setup(SetupNode);
  //Die if we missed something
  if((parsedInfo&neededInfo) != neededInfo)
    {
      PrintError("DetectorInfo not complete");
      return false;
    }
  
  //There should be two memory managers
  // MemoryBlockManager:  interface with raw CAEN data
  if(!FindMemoryManager(Block_mem,SetupNode,MemManager))
    {
      ret = false;
    }  
  //EBPCManager:   interface with the RAT world
  if(!FindMemoryManager(EBPC_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  
  if(!EBPC_mem)
    {
      PrintError("Missing EBPC RAT memory manager\n.");
      ret = false;
    }
  if(!Block_mem)
    {
      PrintError("Missing Block memory manager\n.");
      ret = false;
    }

  if(FindSubNode(SetupNode,"CHECKHEADERS") == NULL)
    {
      checkHeaders = 0;
    }
  else
    GetXMLValue(SetupNode,"CHECKHEADERS",GetName().c_str(),checkHeaders);

  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


bool BenchAssembler::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the FEPC RAT memory manager to shut down.
      EBPC_mem->Shutdown();
      Block_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      //DCMessage::DCtime_t timeTemp = message.GetTime();
      Print("Pausing thread.\n");
      RemoveReadFD(Block_mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      //DCMessage::DCtime_t timeTemp = message.GetTime();
      Print("Starting up thread.\n");
      
      //Register the WFD/EV block's free queue's FDs with select
      AddReadFD(Block_mem->GetFullFDs()[0]);

      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}

void  BenchAssembler::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      
      //Reset counters
      timeStepLockBlockedCount = 0;
      collisionCount = 0;

      //Setup next time
      lastTime = currentTime;
    }
}

//DELETE THIS
void  BenchAssembler::DumpBlock()
{
  if(memBlockIndex != FreeFullQueue::BADVALUE)
    {
      FILE * outfile = fopen("blockdump.dat","w");
      for(uint32_t i = 0; i < memBlock->dataSize;i++)
	{
	  fprintf(outfile,"0x%08X  0x%08X\n",i,memBlock->buffer[i]);
	}
      fclose(outfile);
    }
}
//DELETE THIS
