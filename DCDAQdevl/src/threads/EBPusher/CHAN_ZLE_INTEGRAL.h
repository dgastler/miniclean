#ifndef __CHAN_ZLE_INTEGRAL__
#define __CHAN_ZLE_INTEGRAL__

#include <PacketBuilder.h>

class CHAN_ZLE_INTEGRAL : public PacketBuilder
{
 public:
  CHAN_ZLE_INTEGRAL()
  {
    ClearPacket();
    packetTypeFull=DCNetPacket::EB_INTEGRATED_WINDOWS_FULL_PACKET;
    packetTypePartial=DCNetPacket::EB_INTEGRATED_WINDOWS_PARTIAL_PACKET;
  };
  virtual ~CHAN_ZLE_INTEGRAL(){ClearPacket();};
  
protected:
  virtual uint32_t CalculatePMTSize(RAT::DS::Raw * raw);
  virtual int AddData(RAT::DS::Raw * raw);
  virtual int AddChannelHeader(RAT::DS::PMT * pmt);
};

#endif
