#include <RATParse.h>

RATParse::RATParse(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATParse"));
  Block_mem = NULL;
  DS_mem = NULL;
  dsBlock = NULL;
  memBlock = NULL;
  dsIndex = FreeFullQueue::BADVALUE;
  memBlockIndex = FreeFullQueue::BADVALUE;

  //Input blocks
  BlocksRead = 0;
  TimeStep_BlocksRead= 0;
  
  //Events
  TimeStep_Events = 0;
  BadEvents = 0;
  TimeStep_BadEvents = 0;

}

bool RATParse::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> & MemManager)
{
  bool ret = true;

  //There should be two memory managers
  // MemoryBlockManager:  interface with raw CAEN data
  if(!FindMemoryManager(Block_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  // BasicRATdsManager:   interface with the RAT world
  if(!FindMemoryManager(DS_mem,SetupNode,MemManager))
    {
      ret = false;
    }
  
  if(!DS_mem)
    {
      fprintf(stderr,"Error %s: Missing DS memory manager\n.",GetName().c_str());
      ret = false;
    }
  if(!Block_mem)
    {
      fprintf(stderr,"Error %s: Missing Block memory manager\n.",GetName().c_str());
      ret = false;
    }
  
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


bool RATParse::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the RAT memory manager to shut down.
      DS_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(DS_mem->GetFreeFDs()[0]);
      RemoveReadFD(Block_mem->GetFullFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Register the memoryblockmanager's full queue's FD so we know when there are
      AddReadFD(Block_mem->GetFullFDs()[0]);
      
      //Register the RAT DS block's free queue's FDs with select
      AddReadFD(DS_mem->GetFreeFDs()[0]);
      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}
