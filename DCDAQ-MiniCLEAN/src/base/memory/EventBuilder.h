#ifndef __EVENTBUILDER__
#define __EVENTBUILDER__

#include <DCVectorEntry.h>

#include <vector>

#include <cstdio>
#include <stdint.h>

//Policy base class for event construction
/*
The job of classes that derive from this class is to build events
out of partial events.
This is done with a key from each partial event.  
Keys are associated with indexes of the entry container
You don't have to worry about collisions, other code does that,just return an index for each key via replacing GetKeyIndex
 */
template <class T>
class EventBuilder
{  
public:
  EventBuilder()
  {
    entry=NULL;
  }
  inline bool EquivalentKeys(int64_t key1, int64_t key2) {return false;};

protected:
  virtual size_t GetKeyIndex(int64_t key) = 0;

  void SetupEventBuilder(std::vector<DCVectorEntry<T> > * _entry)
  {
    entry=_entry;
  }
  inline std::vector<DCVectorEntry<T> > *const GetEntry()
  {
    return entry;
  }
private:
  std::vector<DCVectorEntry<T> > *  entry;

  //Don't implement these because they should never be called
  EventBuilder(const DCVectorEntry<T> & rhs);
  EventBuilder & operator=(const EventBuilder<T> & rhs);
};

#endif
