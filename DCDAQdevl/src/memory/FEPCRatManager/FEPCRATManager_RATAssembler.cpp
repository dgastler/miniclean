#include <FEPCRATManager.h>

int64_t FEPCRATManager::GetAssembledEvent(int64_t Event,
					  int32_t &index,
					  uint64_t &password)
{ 
  int64_t ret = returnDCVectorHash::BAD_MUTEX;  
  //Get the index at Event
  ret = AssembledEvent.GetEntry(Event,index,password);
  if(ret == returnDCVectorHash::EMPTY_KEY)
    //The index was empty
    {      
      //Get a new index from the free queue
      int32_t freeIndex;
      bool gotFree = false;
      while(!(gotFree = Free.Get(freeIndex)) && 
	    ConditionWait() &&
	    !ShutdownStatus)
	{
	  //We are in condition wait on the EBPCRatmanager level
	  //locks. 
	  //Either a free will be added or we will ShutDownLockWait()
	  //will break us out of this loop
	  //A free can be added because we are using ConditionWait()!
	}
      if(gotFree)
	//We got a free
	{
	  index = freeIndex;
	  ret = AssembledEvent.UpdateEntry(Event,freeIndex,password); 	  
	}
      else
	//Failed to get a free
	{	  
	  index = AssembledEvent.GetBadValue();
	  //Must block to make sure everything is consistent
	  ret = AssembledEvent.ClearEntry(Event,password);
	  if(ret == returnDCVectorHash::OK)
	    {
	      ret = returnDCVectorHash::NO_FREE;
	    }
	}
    }
  return(ret);
}

int64_t FEPCRATManager::ReturnAssembledEvent(int32_t &index,
					     uint64_t password)
{
  int64_t ret = returnDCVectorHash::OK;
  //Get the block for the index we are dealing with
  FEPCBlock * block = GetFEPCBlock(index);
  if(block != NULL)
    {
      int64_t eventID = block->eventID;      
      int32_t tempIndex = index;  //Temp index so we don't lose ours
      //Update the entry at eventID 
      ret = AssembledEvent.UpdateEntry(eventID,tempIndex,password);
      if(ret == returnDCVectorHash::OK)
	{
	  //clear the password for this event if everything worked
	  ret = AssembledEvent.ClearPassword(eventID,password);	  
	  if(ret == returnDCVectorHash::OK)
	    {
	      index = tempIndex;
	    }
	}
    }
  else
    {
      //The entry was empty.  Why did you call this? 
      ret = returnDCVectorHash::EMPTY_KEY;
    }
  return(ret);  
}


int64_t FEPCRATManager::MoveAssembledEventToUnSentQueue(int64_t eventID,
							uint64_t password)
{
  int64_t ret = returnDCVectorHash::BAD_MUTEX;  
  int32_t index;
  //Get the index for this eventID.
  ret = AssembledEvent.GetEntry(eventID,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventID.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVectorHash::OK)||(ret >= 0))
    {
      //Remote this event from the AssembledEvent vector hash
      ret = AssembledEvent.ClearEntry(eventID,password);
      if(ret == returnDCVectorHash::OK)
	//The event has been cleared from the vector hash
	{
	  //Add index to the UnSent queue
	  bool moveWorked = UnSent.Add(index);
	  if(moveWorked)
	    {
	      index = AssembledEvent.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVectorHash::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  fprintf(stderr,"Clear failed on FEPC entry %ld\n",eventID);
	}	
    }
  else
    {
      //Return the error we got from Get
# if __WORDSIZE == 64
      printf("Got error %ld from get\n",ret);
# else 
      printf("Got error %lld from get\n",ret);
# endif
    }

  return ret ;
}

int64_t FEPCRATManager::MoveAssembledEventToBadQueue(int64_t eventID,
						     uint64_t password,
						     int32_t errorCode)
{  
  int64_t ret = returnDCVectorHash::BAD_MUTEX;  
  int32_t index;
  //Get the index for this eventID.
  ret = AssembledEvent.GetEntry(eventID,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventID.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVectorHash::OK)||(ret >= 0))
    {
      //Remove this event from the AssembledEvent vector hash
      ret = AssembledEvent.ClearEntry(eventID,password);
      if(ret == returnDCVectorHash::OK)
	//The event has been cleared from the vector hash
	{
	  //Add index to the Bad queue
	  BadEvent::sBadEvent badEvent;
	  badEvent.errorCode = errorCode;
	  badEvent.index = index;
	  badEvent.eventID = eventID;
	  bool moveWorked = BadEvent.Add(badEvent);
	  if(moveWorked)
	    {
	      index = AssembledEvent.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVectorHash::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  //We should just return the error we had
	}	
    }
  else
    {
      //Return the error we got from Get
    }
  return(ret);
}
