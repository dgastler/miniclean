#ifdef __RATDS__

#ifndef __PACKDATA__
#define __PACKDATA__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <FEPCRATManager.h>
#include <DRPCRatManager.h>
#include <RATDiskBlockManager.h>
#include <xmlHelper.h>
#include <RAT/DS/DCDAQ_timeHelper.hh>
#include <RAT/DS/DCDAQ_EventStreamHelper.hh>
#include <DQMSender.h>


#include <RATCopyQueue.h>
#include <RAT/DS/DCDAQ_RATDSBlock.hh>
#include <RAT/DS/DCDAQ_RATEVBlock.hh>
#include <RAT/DS/DCDAQ_DCDAQEvent.hh>
#include <BadEventStruct.h>
#include <RunInfo.h>
#include <DetectorInfo.h>

#include <RAT/DS/Root.hh>
#include <RAT/DS/EV.hh>

#include <math.h>

#include <TFile.h>
#include <TH1.h>

class PackData : public DCThread
{
 public:
  PackData(std::string Name);
  ~PackData();

  bool Setup(xmlNode * SetupNode, std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();



  virtual void PrintStatus(){
    std::stringstream status;
    pid_t tid=syscall(SYS_gettid);
    status<<"id "<<tid;
    Print(status.str().c_str());
    DCThread::PrintStatus();
  }
 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();

  void CheckFEPCBad();
  void CheckFEPCSend();
  void CheckDRPCBad();
  void CheckDRPCRespond();
  void CheckEBPCBad();
  void CheckEBPCDisk();

 
  
  void ProcessRATBlock(RATEVBlock *event);
  void ProcessRATBlock(RATDSBlock *event);
  void ProcessEV(RAT::DS::EV * ev);

  uint32_t * WriteRunHeader(uint32_t * bufferPointer);
  uint32_t * WriteSimpleRunHeader(uint32_t * bufferPointer);

  void EndOfRun();

  bool writeFEPCBad;
  bool writeFEPCSend;
  bool writeDRPCBad;
  bool writeDRPCRespond;
  bool writeEBPCBad;
  bool writeEBPCDisk;
 

  bool firstBlock;

  //Memory managers
  MemoryBlockManager * Mem;
  MemoryBlock * block;
  int32_t blockIndex;
  RATCopyQueue * RATCopy_mem; 
 
  FEPCRATManager * FEPC_mem;
  DRPCRATManager * DRPC_mem;
  RATDiskBlockManager * EBPC_mem;
 

  //Detector information
  DetectorInfo detectorInfo;

  //Pointer to mem block
  uint32_t * bufferPointer;

  //Statistics
  time_t lastTime;
  time_t currentTime;
  uint32_t timeStepProcessedEvents;

  //Time for FIFO writing
  time_t lastFIFOTime;
  time_t currentFIFOTime;
  //Output histogram
  TH1F * EventSizeHist;
  TFile * EventSizeFile;

  uint32_t lastRunNumber;
  uint32_t presamples;
  uint32_t start_prompt;
  uint32_t end_prompt;


};

#endif
#endif

