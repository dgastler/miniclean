#include <DRFakeTriggerAssembler.h>

void DRFakeTriggerAssembler::MainLoop()
{
  int32_t evIndex;
  RATEVBlock *evBlock = NULL;
  uint64_t password = DRPC_mem->GetBlankPassword();
  
  //First go through the trigger queue and try to deal with those events.
  for(std::deque<int>::iterator it = triggerQueue.begin();it!=triggerQueue.end();)
    {
      int64_t retDRPCmem = DRPC_mem->GetEvent(*it,evIndex,password);
      while(retDRPCmem>=0)
	{
	  //If there is a collision, move old event to bad event queue and try again
	  ProcessCollision(triggerEventID,retDRPCmem,evIndex,password);
	  retDRPCmem = DRPC_mem->GetEvent(triggerEventID,evIndex,password);
	  PrintWarning("Collision in DRFakeTriggerAssembler!\n"); 
	}
      //No collision
      if(retDRPCmem == returnDCVector::OK)
	{
	  //Get our evBlock
	  evBlock = DRPC_mem->GetDRPCBlock(evIndex);
	  
	  //Set the event ID
	  evBlock->eventID = triggerEventID;
	  evBlock->ev->SetEventID(triggerEventID);
	  
	  //Check whether this event is done and ready to be moved to Unprocessed queue
	  if(evBlock->ev->GetBoardCount() == int(detectorInfo.GetWFDCount()))
	    {
	      // fprintf(stderr,"Event %i done and ready to be moved to unprocessed queue\n",*it);
	      DRPC_mem->MoveEventToUnProcessedQueue(triggerEventID,password);
	    }
	  else
	    {
	      //DRPCAssembler has not finished with this event yet.
	      //Return it to the DRPCRATManager.
	      DRPC_mem->SetEvent(triggerEventID,evIndex,password);
	    }
	  it = triggerQueue.erase(it);
	}
      ++it;
    }

  //Now move on to current event.
  //Get the event from the Event hash
  int64_t retDRPCmem = DRPC_mem->GetEvent(triggerEventID,evIndex,password);
  
  while(retDRPCmem >=0)
    {
      //If there is a collision, move old event to bad event queue and try again
      ProcessCollision(triggerEventID,retDRPCmem,evIndex,password);
      retDRPCmem = DRPC_mem->GetEvent(triggerEventID,evIndex,password);
      PrintError("Collision in DRFakeTriggerAssembler!\n");
    }
  //No collision
  if(retDRPCmem == returnDCVector::OK)
    {
      //Get our evBlock
      evBlock = DRPC_mem->GetDRPCBlock(evIndex);

      //Set the event ID
      evBlock->eventID = triggerEventID;
      evBlock->ev->SetEventID(triggerEventID);

      //Check whether this event is done and ready to be moved to Unprocessed queue
      if(evBlock->ev->GetBoardCount() == int(detectorInfo.GetWFDCount()))
	{
	  DRPC_mem->MoveEventToUnProcessedQueue(triggerEventID,password);  
      	}
      else
	{
	  //DRPCAssembler has not finished with this event yet.
	  //Return it to the DRPCRATManager.
	  DRPC_mem->SetEvent(triggerEventID,evIndex,password);
	}
    }
  else
    {
      if(retDRPCmem == returnDCVector::BAD_PASSWORD)
	{
	  triggerQueue.push_back(triggerEventID);
	}
      else
	{
	  PrintError("DRPC_mem error!\n");
	}
    }

  //Increment the trigger event ID
  triggerEventID += 1;
}


void DRFakeTriggerAssembler::ProcessTimeout()
{
  //Set time.
  currentTime = time(NULL);
  if(Period!=0)
    {
      MainLoop();
    }
}

void DRFakeTriggerAssembler::ProcessCollision(int64_t newEventID, int64_t oldEventID,
					      int32_t evIndex, uint64_t password)
{
  //We have a conflict
  char * buffer = new char[100];
# if __WORDSIZE == 64
  char formatString[] = "Collision! %ld %ld\n";
# else
  char  formatString[] = "Collision! %lld %lld\n";
# endif
  sprintf(buffer,formatString,oldEventID,newEventID);
  PrintError(buffer);
  delete [] buffer;
  //The data in the vector hash is for another event. 
  //We have a lock on it though!
  //We will move this event to the bad queue
  //Action concerning this bad event will be left to
  //another thread.
  //int64_t retCollision = DRPC_mem->MoveEventToBadQueue(oldEventID,
  DRPC_mem->MoveEventToBadQueue(oldEventID,
				password,
				Level::STORAGE_COLLISION);
  
  PrintError("Address collision\n");
  
}


