/**********************************************************************
* Header file for PID.c.
* author              Atmel Corporation: http://www.atmel.com
*****************************************************************************/
#ifndef PID_H
#define PID_H

#include <stdint.h>
#include <stdio.h>

class PID
{
   public:
   PID(uint8_t p_factor, uint8_t i_factor, uint8_t d_factor);
   ~PID(){};
   
   double update(uint16_t setPoint, uint16_t processValue);
   void reset_integrator();
   
   private:
   //Setpoints and data used by the PID control algorithm
   
   //Derivative term should not be used on first measurement
   bool first_time;
   //Last process value, used to find derivative of process value.
   uint16_t lastProcessValue;
   //Summation of errors, used for integrate calculations
   double sumError;
   //The Proportional tuning constant
   double P_Factor;
   //The Integral tuning constant
   double I_Factor;
   //The Derivative tuning constant
   double D_Factor;
};

#endif

