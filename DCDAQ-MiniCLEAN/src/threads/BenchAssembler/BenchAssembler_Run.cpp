
#include <BenchAssembler.h>

void BenchAssembler::MainLoop()
{

  //Check for a new memory block event
  //Don't do anything if we already have a blockIndex
  if(IsReadReady(Block_mem->GetFullFDs()[0]) &&
     (memBlockIndex == FreeFullQueue::BADVALUE))
    {
      //Get new memory block
      Block_mem->GetFull(memBlockIndex);
      if(memBlockIndex != FreeFullQueue::BADVALUE)       //We have a valid MemBlock
	{
	  //Get the memory bloc for the blockIndex
	  memBlock = Block_mem->GetBlock(memBlockIndex);
	  //Zero our current point in the memory block.
	  currentDataIndex = 0;
	  
	  //We no longer need a memblock.
	  //Remove memory block manager from the select set
	  RemoveReadFD(Block_mem->GetFullFDs()[0]);
	  	  
	  ///Apply select changes
	  SetupSelect();
	}
      else 
	{		  
	  //This means that the memory block manager
	  //has been set to shutdown.
	  //We probably have a message from DCDAQ waiting
	  //for us
	}      
    }
 


 //If we are in MainLoop, then something has changed with the state of 
  //either the memblock or evblock.   
  //Check that we have a valid memblock and a valid evBlock
  if(memBlockIndex != FreeFullQueue::BADVALUE)
    {
      //Push back an initial v1720 event into the v1720 list
      v1720_list.push_back(NewEvent);
      GotBadPassword = false;

      //Loop over the current memblock until it is empty
      int memBlockCounter = 0;
      while(currentDataIndex < memBlock->dataSize)
	{

	  //Parse the current CAEN V1720 style event.  
	  //It is located at memBlock->buffer + CurrentDataIndex

	  int32_t shift =  v1720_list.back().ProcessEvent( memBlock->buffer + currentDataIndex,
							   memBlock->dataSize - currentDataIndex);
	  memBlockCounter++;
	  
	  //If shift is positive, then we read out a valid event
	  if(shift > 0)
	    {			
	      
	      int processErr = ProcessOneEvent(v1720_list.back());	     
	      if(processErr == OK)
		{
		  if(GotBadPassword == true)
		    {
		      LastEventOK = true;
		    }
		  //We processed another event.  Yay!
		  TimeStep_Events++;
		  //Move CurrentDataIndex to the start of the next event. 
		  currentDataIndex += shift;  
		}
	      else if(processErr == FATAL_ERROR)
		{
		  PrintError("Error with WFD Event\n");
		  //FAIL for now
		  break;
		}
	      else if(processErr == WFD_NOT_PROCESSED)
		{
		}
	      else if(processErr == LOCK_BLOCKED)
		{
		  //This event is locked.  
		  //Let's leave it alone for now and add a new V1720 object for the next event.
		  v1720_list.push_back(NewEvent);
		  GotBadPassword = true;
		  lockBlockedCount++;	
		  LastEventOK = false;
		  currentDataIndex += shift; 

		}
	      else if(processErr == OUT_OF_MEMORY)
		{
		  break;
		}
	      else if(processErr == COLLISION)
		{
		}
	    }
	  else if(shift == V1720EVENT_JUNK_WORD)
	    {
	      currentDataIndex++ ;
	      //DELETE THIS
	      //	      DumpBlock();
	      //	      SendStop();
	      //DELETE THIS
	    }
	  else if(shift == V1720EVENT_SMALL_EVENT)
	    {
	      //DELETE THIS
	      //	      DumpBlock();
	      //	      SendStop();
	      //DELETE THIS
	      //No more events to parse, but there is still some data.
	      //This is strange and worthy of reporting
	      char * buffer = new char[100];
	      sprintf(buffer,"Found small event of size %d",memBlock->dataSize - currentDataIndex);
	      PrintWarning(buffer);
	      delete [] buffer;

	      currentDataIndex = memBlock->dataSize;
	      break;
	    }
	  else if(shift == V1720EVENT_CORRUPT_EVENT)
	    {
	      //DELETE THIS
	      //	      DumpBlock();
	      //	      SendStop();
	      //DELETE THIS
	      //we encountered a bad event
	      TimeStep_BadEvents++;//Bad data. 		 

	      char * buffer = new char[100];
	      sprintf(buffer,"Corrupt event. Lost %d bytes of data.",memBlock->dataSize - currentDataIndex);
	      PrintWarning(buffer);
	      delete [] buffer;	      

//	      DCMessage::Message message;      
//	      //Send a message back to the master and stop the inner run loop
//	      message.SetType(DCMessage::STOP);
//	      SendMessageOut(message,true);

	      currentDataIndex = memBlock->dataSize;
	      break;
	    }
	  else
	    {
	      //shouldn't happen.
	      PrintError("Unknown error (you win!)");
	      currentDataIndex = memBlock->dataSize;
	      break;
	    }	 	  
	}
      
      //Check if we should return the current memBlock
      if(currentDataIndex >= memBlock->dataSize)
	{
	  
	  //Before we can return the memBlock, we have to deal with the locked events that we skipped.
	  	    
	  v1720_list.pop_back(); 
	  
	  bool tryAgain = true;
	  int loopCount = 0;
	  v1720_NumberOfTries = 2;
	  while(GotBadPassword == true && v1720_list.size()>0 && tryAgain == true)
	    {
	      //Loop through locked events
	      for(std::list<v1720Event>::iterator it = v1720_list.begin(); it!=v1720_list.end();)
		{
		  int processErr = ProcessOneEvent(*it);
		  if(processErr==OK)
		    {
		      it = v1720_list.erase(it);
		    }
		  else
		    {
		      ++it;
		      if(processErr == FATAL_ERROR)
			{
			  PrintError("Error with WFD Event\n");
			  //FAIL for now
			  break;
			}
		    }
		}
	      loopCount++;
	      if(loopCount>=v1720_NumberOfTries) tryAgain = false;
	    }
	  //Clear the list of v1720s
	  v1720_list.clear();
	  //Zero memBlock's data size
	  memBlock->dataSize = 0;
	  //Forget the address of the memBlock (that way we don't mess with it)
	  memBlock = NULL; 
	  //Recycle the memblock by passing off its index
	  Block_mem->AddFree(memBlockIndex);
	  //We need to listen for a new full memBlock
	  AddReadFD(Block_mem->GetFullFDs()[0]);
	  SetupSelect();
	}
    }

}

