#ifndef __FEPCRATMANAGER__
#define __FEPCRATMANAGER__

#include <MemoryManager.h>
#include <xmlHelper.h>
#include <FEPCBlock.h>
#include "RAT/DS/EV.hh"

#include <DCVectorHash.h>
#include <DCQueue.h>
#include <BadEventStruct.h>

//Not implimented yet!
////for error handling.  This means if we have a collision, we move the
////older event to the bad queue.  If we dont' define this, then we are going
////to move the newer event. 
//#define MOVE_OLD_EVENT


class FEPCRATManager : public MemoryManager, public NotLockedObject
{
 public:
  //================================================================
  //Memory manager setup
  //================================================================
  FEPCRATManager(std::string Name);
  ~FEPCRATManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  //================================================================
  //Mutex control
  //================================================================
  //Wake up any mutex locks on contained objects
  virtual void WakeUp();
  //Shutdown the mutex waits in contained objects
  virtual void Shutdown();
  //Get the shutdown status
  virtual bool IsShutdown() {return(ShutdownStatus);};

  //================================================================
  //Memory manager status
  //================================================================
  virtual void PrintStatus(int indent = 0);

  //================================================================
  //File descriptor access
  //================================================================
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetUnSentFDs(){return(UnSent.GetFDs());};
  const int * GetToSendFDs(){return(Send.GetFDs());};
  const int * GetBadEventFDs(){return(BadEvent.GetFDs());};

  //================================================================
  //Memory access
  //================================================================
  //Get the defined bad value
  int32_t GetBadValue(){return badValue;};

  //Block access
  FEPCBlock * GetFEPCBlock(uint32_t iFEPC) 
  {
    if(iFEPC < Memory.size())
      {
	return(&(Memory[iFEPC]));
      }
    return(NULL);
  };  
  
  //================================================================
  //Index access
  //================================================================

  //================================================================
  //AssembledEvent access  (RATAssembler)
  //================================================================
  //Gets an event from the AssembledEvent vector-hash. Detects collisions
  int64_t GetAssembledEvent(int64_t eventID,
			    int32_t &index,
			    uint64_t &password);
  //Returns and clears PW for the event at index. (does not molest index if there is a collision)
  int64_t ReturnAssembledEvent(int32_t &index,uint64_t password);
  int64_t MoveAssembledEventToUnSentQueue(int64_t eventID,uint64_t password);  
  int64_t MoveAssembledEventToBadQueue(int64_t eventID,
				       uint64_t password,
				       int32_t errorCode);
  uint64_t GetAssembledBlankPassword(){return AssembledEvent.GetBlankPassword();};
  //================================================================

  //================================================================
  //Un-sent queue (DRPusher)
  //================================================================
  uint64_t GetStorageBlankPassword(){return StorageEvent.GetBlankPassword();};
  bool     GetUnSent(int32_t &index);
  bool     AddUnSent(int32_t &index);
  int64_t  GetStorageEvent(int64_t eventID,
			   int32_t &index,
			   uint64_t & password);
  int64_t  ReleaseStorageEvent(int32_t &index,
			       uint64_t & password);
  int64_t  ClearStorageEvent(int64_t eventID,
			     uint64_t password);
  bool     MoveStorageEventToBadQueue(int64_t eventID,
				      int32_t &index,
				      int32_t errorCode);
  //================================================================
  
  //================================================================
  //Storage Event access (DRGetter)
  //================================================================
  bool AddSend(int32_t &index);
  
  //================================================================

  //================================================================
  //To-sent queue (EBPusher)
  //================================================================
  bool GetSend(int32_t &index);
  bool AddFree(int32_t &index);
  //================================================================

  //================================================================
  //BadEvent queue
  //================================================================
  bool GetBadEvent(BadEvent::sBadEvent &value);
  bool AddBadEvent(int64_t eventID,
		   int32_t &index,
		   int32_t errorCode);
  //================================================================
 
 private:   
  //================================================================
  //Data storage and sizes
  //================================================================
  //Memory vector and it's size from the setup XML
  uint32_t NumFEPCBlocks;
  std::vector<FEPCBlock> Memory;
  //Size of AssembledEvent vectorhash
  uint32_t AssembledEventSize;
  //Size of StorageEvent vectorhash
  uint32_t StorageEventSize;
  //Shutdown status
  bool ShutdownStatus;

  int32_t badValue;
  int32_t emptyValue;

  //================================================================
  //QUEUES
  //================================================================
  //queue of indices of free
  DCQueue<int32_t,PipeSelect,LockedObject> Free;  
  //queue of indices of un-sent memory blocks
  DCQueue<int32_t,PipeSelect,LockedObject> UnSent;  
  //queue of indices to send to the event builder.
  DCQueue<int32_t,PipeSelect,LockedObject> Send; 
  //queue of indices that have had problems. 
  DCQueue<BadEvent::sBadEvent,PipeSelect,LockedObject> BadEvent; 

  //================================================================
  //Vector hashes
  //================================================================
  //vector-hash of events being assembled
  DCVectorHash<int32_t,LockedObject> AssembledEvent; 
  //vector-hash of stored sent events
  DCVectorHash<int32_t,LockedObject> StorageEvent; 

  //================================================================
  //Deallocation of all the data objects
  //================================================================  
  void Deallocate();
};

#endif
