#ifndef __RATASSEMBLER__
#define __RATASSEMBLER__
//RATAssembler 

#include <DCThread.h>

#include <xmlHelper.h>
#include <MemoryBlockManager.h>
#include <WFDBlockManager.h>
#include <WFDBlock.h>
#include <FEPCRATManager.h>
#include <FEPCBlock.h>
#include "RAT/DS/EV.hh"
#include <DSBlock.h>
#include <DCQueue.h>
#include <v1720Event.h>
#include <list>

#include <math.h>

#include <DetectorInfo.h>

class RATAssembler : public DCThread 
{
 public:
  RATAssembler(std::string Name);
  ~RATAssembler(){};

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  enum RATAssemberErrorCodes
  {
    OK = 0,
    LOCK_BLOCKED      = -1,
    WFD_NOT_PROCESSED = -2,
    FATAL_ERROR       = -3,
    BAD_WFD_HEADER    = -4,
    OUT_OF_MEMORY     = -5,
    BAD_WFD_EV        = -6,
    BAD_WFD_BOARD_ID  = -7,
    COLLISION         = -8
  };

  //======================================
  //RatAssembler_Run.cpp
  //======================================
  //Process a block of single WFD data
  int ProcessWFD(RAT::DS::EV * wfd);
  //Get an existing event and add WFDBlock event
  int MergeEvent(RAT::DS::EV * wfd,
		 RAT::DS::EV * event);  
  //Handle an event collision
  int ProcessCollision(RAT::DS::EV * wfd);
  //======================================
  
  //======================================
  //RatAssembler.cpp
  //======================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  //======================================

  //======================================
  //RatAssembler_Processing.cpp
  //======================================
  void ProcessZLEWindow(RAT::DS::Raw * raw,RAT::DS::RawWaveform * rawWaveform,int PMTID);
 

  //Memory Block manager interface
  MemoryBlockManager * Block_mem; //Input manager
  MemoryBlock * memBlock;//Local data structure that holds a pointer to data
  int32_t memBlockIndex;//Index to find our block from the block manager
  uint32_t currentDataIndex;

  /* //RATDS manager */
  /* WFDBlockManager * EV_mem; //Output manager */
  /* WFDBlock * evBlock; */
  /* int32_t evIndex; */

  /* //Single WFD manager */
  /* WFDBlockManager * WFD_mem; */
  /* DCQueue<unsigned int,NoSelect,NotLockedObject> lockedWFDBlocks; */
  /* WFDBlock * wfdBlock; */
  /* int32_t wfdIndex; */
  /* int32_t currentWFDIndex; */

  //FEPC Rat manager
  FEPCRATManager * FEPC_mem;
  
  
  //Statisticas
  time_t lastTime;
  time_t currentTime;
  int32_t collisionCount;
  int32_t lockBlockedCount;
  int32_t timeStepLockBlockedCount;

  //Input blocks
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;
  //Events
  uint32_t Events;
  uint32_t TimeStep_Events;
  uint32_t BadEvents;
  uint32_t TimeStep_BadEvents;

  //================================
  //Detector Info
  //================================
  DetectorInfo detectorInfo;

  //Put one event into a 
  int ProcessOneEvent(v1720Event &Event);
  //Event parsing vectors
  std::vector<uint32_t> WindowHeaderVector;
  std::vector<uint16_t> WindowDataVector;

  //v1720Event parser
  std::list<v1720Event> v1720_list;
  v1720Event NewEvent;
  int v1720_NumberOfTries;
  bool GotBadPassword;
  bool LastEventOK;

};


#endif
