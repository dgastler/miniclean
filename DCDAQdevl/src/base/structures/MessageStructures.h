#ifndef __MESSAGESTRUCTURES__
#define __MESSAGESTRUCTURES__
namespace DCMessage
{
  //===================================
  //Useful struct types for parsing message data
  //===================================
  //DCtime_t
  typedef uint64_t DCtime_t;

  enum 
  {
    TIMEDCOUNT,
    SINGLEUINT64,
    SINGLEINT64,
    DCROUTE,
    DCTYPEID,
    TIMEDDOUBLE,
    SINGLEDOUBLE
  };

  #define textSize 500

  //TimedCount
  struct TimedCount
  {
    uint8_t type;
    char text[textSize];
    int32_t ID;
    uint32_t count;
    float dt;    
    TimedCount(){type=TIMEDCOUNT;};
  };
  
 //TimedDouble
  struct TimedDouble
  {
    uint8_t type;
    char text[textSize];
    int32_t ID;
    double value;
    float dt;    
    TimedDouble(){type=TIMEDDOUBLE;};
  };

  //single uint64_t
  struct SingleUINT64
  {
    uint8_t type;
    char text[textSize];
    uint64_t i;
    SingleUINT64(){type=SINGLEUINT64;};
  };
  
  //single int64_t
  struct SingleINT64
  {
    uint8_t type;
    char text[textSize];
    int64_t i;
    SingleINT64(){type=SINGLEINT64;};
  };

  //single double
  struct SingleDouble
  {
    uint8_t type;
    char text[textSize];
    double value;
    SingleDouble(){type=SINGLEDOUBLE;};
  };

  //DCDAQ route data
  struct DCRoute
  {
    int8_t type;
    //Address information
    int32_t NameOffset;
    int32_t AddrOffset;
    //Thread name info
    int32_t ThreadOffset;
    DCRoute()
    {
      type=DCROUTE;
      NameOffset=-1;
      AddrOffset=-1;
      ThreadOffset=-1;
    };
    std::vector<uint8_t> SetData(std::string Name,
				 struct in_addr Addr,
				 std::string Thread)
    {
      //Create a vector to store the data to pass to DCMessage
      std::vector<uint8_t> data;
      //Copy this structure into it. 
      for(size_t i = 0; i < sizeof(DCRoute);i++)
	data.push_back( ((uint8_t*)(this))[i]);
      
      //Set Name Offset
      NameOffset = ((DCRoute *) (&data[0]))->NameOffset = (int32_t) data.size();
      //Copy Name
      for(size_t i = 0; i < Name.size();i++)
	data.push_back(Name[i]);
      
      //Set Addr Offset
      AddrOffset = ((DCRoute *) (&data[0]))->AddrOffset = (int32_t) data.size();
      //Copy Addr      
      for(size_t i = 0; i < sizeof(struct in_addr);i++)
	data.push_back(((uint8_t*)(&Addr))[i]);
      
      //Set Thread name Offset
      ThreadOffset = ((DCRoute *) (&data[0]))->ThreadOffset = (int32_t) data.size();
      //Copy Addr
      for(size_t i = 0; i < Thread.size();i++)
	data.push_back(Thread[i]);
      
      return(data);
    };
    bool GetData(const std::vector<uint8_t> &data,
		 std::string &Name,
		 struct in_addr &Addr,
		 std::string &Thread)
    {
      //Clear 
      Name.clear();
      Thread.clear();
      //Check for size
      if(data.size() >= sizeof(DCRoute))
	{
	  //Copy the header info
	  for(size_t i = 0; i < sizeof(DCRoute);i++)
	    {
	      ((uint8_t *) (this))[i] = data[i];
	    }

	  //Check the size can work
	  if((NameOffset >= 0) && 
	     (AddrOffset >= NameOffset) &&
	     (ThreadOffset >= AddrOffset) &&
	     (int32_t(data.size()) >= ThreadOffset))
	    {
	      //Copy Name
	      Name.resize(AddrOffset - NameOffset);
	      for(size_t i = NameOffset; i < uint32_t(AddrOffset);i++)
		Name[i - NameOffset] = data[i];
	      //Copy Addr
	      //	      for(size_t i = AddrOffset;i < uint32_t(ThreadOffset);i++)
		memcpy(&Addr,&data[AddrOffset],sizeof(struct in_addr));
	      //Copy Thread name
	      Thread.resize(data.size() - ThreadOffset);
	      for(size_t i = ThreadOffset;i < data.size();i++)
		{
		  Thread[i - ThreadOffset] = data[i];
		}
	    }
	  else
	    {
	      Clear();
	      return(false);
	    }
	}
      else
	{
	  Clear();
	  return(false);
	}
      return(true);
    };
    void Clear()
    {
      NameOffset=-1;
      AddrOffset=-1;
      ThreadOffset=-1;
    };
  };
  
  struct DCTypeID
  {
    uint8_t type;
    int32_t ID;
    int32_t NameOffset;
    DCTypeID()
    {
      type = DCTYPEID;
      ID = -1;
      NameOffset = -1;
    };
    std::vector<uint8_t> SetData(uint16_t _ID,
				 std::string Name)
    {
      //copy the ID (uint16 to int32)
      ID = int32_t(_ID);
      //Create a vector to store the data to pass to DCMessage
      std::vector<uint8_t> data;
      //Copy this structure into it. 
      for(size_t i = 0; i < sizeof(DCTypeID);i++)
	data.push_back( ((uint8_t*)(this))[i]);

      //Set Name Offset
      NameOffset = ((DCTypeID *) (&data[0]))->NameOffset = (int32_t) data.size();
      //Copy Name
      for(size_t i = 0; i < Name.size();i++)
	data.push_back(Name[i]);      
      
      return(data);
    };
    bool GetData(const std::vector<uint8_t> &data,
		 uint16_t &_ID,
		 std::string &Name)
    {
      Name.clear();
      //Check for size
      if(data.size() >= sizeof(DCTypeID))
	{
	  //Copy the header info
	  for(size_t i = 0; i < sizeof(DCTypeID);i++)
	    {
	      ((uint8_t *) (this))[i] = data[i];
	    }	 

	  //Check the size can work
	  if((NameOffset >= 0) && 	     
	     (int32_t(data.size()) >= NameOffset))
	    {
	      //Copy Name
	      Name.resize(data.size() - NameOffset);
	      for(size_t i = NameOffset; i < data.size();i++)
		Name[i - NameOffset] = data[i];
	    }
	  else
	    {
	      Clear();
	      return(false);
	    }
	}
      else
	{
	  Clear();
	  return(false);
	}
      _ID = uint16_t(ID);
      return(true);
    };
    void Clear()
    {
      type = DCTYPEID;
      ID = -1;
      NameOffset = -1;
    };
  };
}
#endif
