#ifndef __V1720EVILFAKEREADOUT__
#define __V1720EVILFAKEREADOUT__

#include <map>
#include <string>

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <RAT/DS/DCDAQ_DCTime.hh>
#include <xmlHelper.h>

#include <cstdlib> //for getenv()
#include <cstring> //for memcpy()
#include <sys/stat.h> //for file stats

class v1720EvilFakeReadout : public DCThread 
{
 public:
  v1720EvilFakeReadout(std::string Name);
  ~v1720EvilFakeReadout();
  virtual bool Setup(xmlNode * SetupNode,
		     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:
  //Input fake data file info and file decriptor
  std::string InFileName;
  FILE * InFile;

  //Statistics
  uint64_t BlocksRead;
  uint64_t TimeStep_BlocksRead;
  time_t lastTime;
  time_t currentTime;

  //Loaded fake data
  uint32_t FakeWFDSize;
  uint32_t * FakeWFD;  //all fake data pointer
  std::vector<uint32_t *> FakeEvent;   //Pointer to individual fake events in FAKEWFD
  unsigned int FakeEventCount;
  uint32_t EventCountOffset;

  //Event rate control variables.  (passed on to DCThread
  useconds_t Period; //Period we ask for
  void SetPeriod(useconds_t _Period);

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message &message);
  //Handle select timeout
  virtual void ProcessTimeout();

  //Vector of WFD event IDs
  std::vector<uint32_t> WFDEventID;
  uint32_t WFDID;
  //Real event number
  uint32_t RealEventID;
  //Current event index
  uint32_t iEvent;
  //Single loop tranfer bool
  bool MemoryLoop; // Controls if we keep sending all the events or we 

  //Memory manager access and opperations
  uint32_t EventsPerBlock;
  uint32_t LoopOffset;
  MemoryBlock * block;
  int32_t blockIndex;
  MemoryBlockManager * Mem; // Memory access

  //Evilness
  bool SkipThis;
  uint32_t skipWFD;
  uint32_t skipEvent;

  //Event ID
};
#endif
