#include <RunControl.h>

void RunControl::MainLoop()
{  
  //Process the next setup operation
  if(IsReadReady(opQueue.GetFDs()[0]))	
    {
      //Stop listening for the next setup operation
      RemoveReadFD(opQueue.GetFDs()[0]);
      SetupSelect();
      //Get the current op from the queue
      opQueue.Get(currentOp,true);
      //Get this op's state back to the beginning
      currentOp.Reset();
      //Get and send op message 
      usleep(100000);//sleep 1/10th of a second
      DCMessage::Message opMessage;
      if(currentOp.GetOpsMessage(opMessage))
	{
	  //process for thread names if it is a launch message
	  if(opMessage.GetType() == DCMessage::LAUNCH)
	    {
	      //Get XML text	      
	      std::vector<uint8_t> data = opMessage.GetData();
	      std::stringstream ss;
	      if(data.size() >0)
		{
		  //copy string data.
		  std::string setupText((char*)&data[0],data.size());
		  std::string::const_iterator begin = setupText.begin() + setupText.find("<THREADS>");
		  std::string::const_iterator end   = setupText.begin() + setupText.rfind("</THREADS>");
		  //Use regex to find all threads created in this LAUNCH message and add them to the shutdown list
		  boost::regex reg("(?i)<NAME>.*?</NAME>");
		  boost::smatch match;
		  while(boost::regex_search(begin,end,match,reg))
		    {
		      std::string threadName(match[0]);
		      threadName = threadName.substr(6,threadName.size()-6-7);
		      DCMessage::DCAddress DCAddr(threadName,
						  opMessage.GetDestination().GetAddr(),
						  opMessage.GetDestination().GetAddrSize());
		      begin=match[0].second;
		      stopAddresses.push_back(DCAddr);
		      ss.str(std::string());
		      ss << "Added " << stopAddresses.back().GetStr()
			 << " to shutdown list\n";
		      Print(ss.str().c_str());			  
		    }		  
		}
	    }

	  //Send out message
	  if(opMessage.GetSource().IsBlank())
	    {
	      SendMessageOut(opMessage);
	    }
	  //If we are faking the source, allow that
	  else 
	    {
	      ForwardMessageOut(opMessage);
	    }
	  //Check if the currentOp is done or has failed
	  //	  CheckDone(setupBlocks,opQueue);
	  CheckDone();
	}
      else
	{
	  //This should never happen, and we shoudl just stop if it does
	  PrintError("Bad op message!");
	  Stop(true);
	}
    }

}
bool RunControl::ProcessMessageNormal(DCMessage::Message & message)
{
  DCMessage::DCMessageType type = message.GetType();
  bool ret = false;
  if(type == DCMessage::ERROR)
    {      
      ProcessError(message);
      ret = true;
    }
  else if(type == DCMessage::WARNING)
    {
      ProcessWarning(message);
      ret = true;
    }
  else if(type == DCMessage::STOP)
    { 
      Print("shutting down.");
      Stop(false);
      ret = true;
    }
  else if(type == DCMessage::GO)
    {
      Print("Starting run setup");
      //Process operation blocks
      if(!Loop)
	Start();
      Loop = true;
      ret = true;
    }
  else if(type == DCMessage::PAUSE)
    {
      Print("Ignoring pause commmand.");
      ret = true;
    }
  else if(currentOp.IsMessageOfInterest(message))
    {
      std::stringstream ss; 
      ss << "Got: " << DCMessage::GetTypeName(type)
	 << " from " << message.GetSource().GetStr() << std::endl;
      Print(ss.str().c_str());
      //Check if the currentOp is done or has failed
//      if(!shuttingDownStopAddresses)
//	CheckDone(setupBlocks,opQueue);  
//      else
      CheckDone();//StopBlocks,stopBlockQueue);
      ret = true;
    }
  else if(type == DCMessage::NETWORK)
    {
      ProcessNetwork(message);
      ret = true;
    }
  return ret;
}

bool RunControl::ProcessMessageDuringShutdown(DCMessage::Message & message)
{
  DCMessage::DCMessageType type = message.GetType();
  bool ret = false;  
  if(currentOp.IsMessageOfInterest(message))
    {
      std::stringstream ss; 
      ss << "Got: " << DCMessage::GetTypeName(type)
	 << " from " << message.GetSource().GetStr() << std::endl;
      Print(ss.str().c_str());
      CheckDone();
      ret = true;
    }
  else if(type == DCMessage::COMMAND_RESPONSE)
    {
      //search for now shutdown threads and remove them from the list
      UpdateShutdownList(message);
      //	  usleep(100000);
      ret = true;
    }
  else if (type == DCMessage::STOP)
    {
      Print("Already shutting down.");
      ret= true;
    }
  else if (type == DCMessage::CAN_NOT_ROUTE)
    {
      //This thread must be gone because the PC went away	  
      //search for this thread and remove it. 
      std::stringstream ss;
      ss << "Can not route to thread " << message.GetSource().GetStr() << " deleting: ";
      bool found = false;
      for(std::vector<DCMessage::DCAddress>::iterator it = stopAddresses.begin();
	  it != stopAddresses.end();
	  it++)
	{	      
	  if(message.GetSource() == *it)
	    {
	      stopAddresses.erase(it);
	      found = true;
	      break;
	    }
	}
      if(found)
	{
	  ss << "found";
	}
      else
	{
	  ss << "failed";
	}
      Print(ss.str());
      ret = true;
    }
  else if (type == DCMessage::ERROR)
    {
      ProcessError(message);
      ret = true;
    }
  else if (type == DCMessage::WARNING)
    {
      ProcessWarning(message);
      ret = true;
    }
  else
    {
      PrintWarning(std::string("Got unknown Message of type ") + 
		   DCMessage::GetTypeName(type) + 
		   std::string(" during shutdown."));
    }
  return ret;
}
bool RunControl::ProcessMessage(DCMessage::Message & message)
{
  bool ret = false;
  if(!shuttingDownStopAddresses)
    //This is the normal run condition
    {
      ret = ProcessMessageNormal(message);
    }
  else
    {
      ret = ProcessMessageDuringShutdown(message);
    }
  return ret;
}

void RunControl::Start()
{
  //We are starting a run, so RunSetupComplete is obviously false;
  RunSetupComplete = false;
  //We are also not shutting down anything yet
  shuttingDownStopAddresses = false;
  //Add all the opBlocks to the opQueue
  opQueue.Clear();
  for(size_t iBlock = 0; iBlock < setupBlocks.size();iBlock++)
    {
      currentOp = setupBlocks[iBlock];
      opQueue.Add(currentOp);
    }
  //Clear currentOp
  currentOp.Clear();
  //Clear shutdown addresses
  stopAddresses.clear();
  //Set the Main loop to wake up for this queue
  AddReadFD(opQueue.GetFDs()[0]);      
  SetupSelect();
}

void RunControl::SendoutStopMessages()
{
  //Now that we are sending out stop messages, we need to change the run mode of this thread via 
  //shuttdingDownStopAddresses
  if(!shuttingDownStopAddresses)
    {
      inShutdownScript=false;
      Print("Starting shutdown sequence!");
      shuttingDownStopAddresses = true;
      threadShutdownCount = 0;
    }

  //keep track of how many times we have tried to shut down the current thread.
  threadShutdownCount++;  

  //If we have tried to shut down a thread more than maxThreadShutdownCount, skip it.
  if(threadShutdownCount > maxThreadShutdownCount)
    {
      std::stringstream ss;
      ss << "SendoutStopMessages(): The thread " << stopAddresses.back().GetStr() 
	 << " isn't responding to our stop commands. Skipping (stopaddress: " 
	 << stopAddresses.size() <<")";
      Print(ss.str());
      stopAddresses.pop_back();
      threadShutdownCount = 0;
      waitTime.tv_sec  = waitTimeBase.tv_sec;
      waitTime.tv_usec = waitTimeBase.tv_usec;
    }

  //If there are still threads to shut down
  if(stopAddresses.size() > 0)
    {     
      //If this thread is on another DCDAQ session, stop routing its stop message to us.
      if(!stopAddresses.back().IsDefaultAddr())
	{
	  //unroute stop remote stop messages
	  DCMessage::Message unroute_message;
	  unroute_message.SetType(DCMessage::UNREGISTER_TYPE);
	  unroute_message.SetDestination(std::string(""),
				 stopAddresses.back().GetAddr(),
				 stopAddresses.back().GetAddrSize()); 
	  DCMessage::DCString dcString;
	  std::vector<uint8_t> unroute_data = dcString.SetData(DCMessage::GetTypeName(DCMessage::STOP));
	  //route it to its DCDAQ command list
	  unroute_message.SetData(&(unroute_data[0]),unroute_data.size());
	  SendMessageOut(unroute_message);
	}
      
      //Send a stop message to the current thread 
      DCMessage::Message stop_message;
      stop_message.SetType(DCMessage::STOP);
      stop_message.SetDestination(stopAddresses.back());
      //Send stop message
      SendMessageOut(stop_message);
      //      usleep(10000);


      //Ask the DCDAQ session of the current thread to tell us which threads are running.
      //We will check the response for the threads just shut down.
      //We send this to the addr of the last stop address to make
      //sure it ends up on the correct PC
      DCMessage::Message list_message;
      DCMessage::DCAddress broadcast;
      broadcast.SetBroadcast();
      list_message.SetDestination(broadcast);
//      message.SetDestination(std::string(""),
//			     stopAddresses.back().GetAddr(),
//			     stopAddresses.back().GetAddrSize()
//			     );
      std::string command("listthreads");
      DCMessage::DCString stringParser;
      std::vector<uint8_t> list_data = stringParser.SetData(command);
      list_message.SetData(&list_data[0],list_data.size());
      list_message.SetType(DCMessage::COMMAND);
      SetSelectTimeout(0,10000);
      SendMessageOut(list_message);
    }
  else
    {
      StopRunControl();
    }
}

void RunControl::Stop(bool _run_error)
{
  run_error = _run_error;
 
  //Stop listening to the setup queues  
  RemoveReadFD(opQueue.GetFDs()[0]);
  SetupSelect();
  opQueue.Clear();
  //Send scripted stop messages if set
  if(RunSetupComplete)
    {
      inShutdownScript=true;
      //We now need to add all our shutdown blocks to the opBlock queue
      for(size_t iBlock = 0; iBlock < StopBlocks.size();iBlock++)
	{
	  currentOp=StopBlocks[iBlock];
	  opQueue.Add(currentOp);
	}
      AddReadFD(opQueue.GetFDs()[0]);
      SetupSelect();
      Print("Starting shutdown script");
      //Run Setup is no longer complete
      RunSetupComplete = false;
    }
  else
    {
      if(run_error)
	PrintWarning(currentOp.Print());
      SendoutStopMessages();
      //Stop this thread if we desire
      if(autostop)
	{
	  Loop=false;
	  Running=false;
	}
    }
}

void RunControl::CheckDone()
{
  if(currentOp.IsDone())
    {        
      std::stringstream ss; 
      ss << opQueue.GetSize() << " OpBlocks left";
      Print(ss.str().c_str());
      //Clear the current op
      currentOp.Clear();
      //The current op is done and we should setup the next
      if(opQueue.GetSize() > 0)
	{
	  AddReadFD(opQueue.GetFDs()[0]);	      
	  SetupSelect();
	}
      //We just finished setup
      else if(!RunSetupComplete && !shuttingDownStopAddresses) 
	{
	  RunSetupComplete = true;
	  Print("Run configuration complete! (backing up)");
	  //Tell the MySQL thread to save the run in run_number=0 to the current run number.
	  DCMessage::Message SQLBackupMessage;
	  SQLBackupMessage.SetDestination(updateRUNLOCK); //Thread is the same as updateRUNLOCK	  
	  SQLBackupMessage.SetType(DCMessage::COMMAND);
	  DCMessage::DCString stringParser;
	  std::string command("RUN_UPDATE");
	  std::vector<uint8_t> data = stringParser.SetData(command);
	  SQLBackupMessage.SetData(&data[0],data.size());
	  SendMessageOut(SQLBackupMessage);
	  
	  //Update the runlock table with the current run number.
	  DCMessage::Message RUNLOCKMessage;
	  RUNLOCKMessage.SetDestination(updateRUNLOCK);
	  RUNLOCKMessage.SetType(DCMessage::COMMAND);
	  command.assign("RUNLOCK RUNNING");
	  data = stringParser.SetData(command);
	  RUNLOCKMessage.SetData(&(data[0]),data.size());
	  RUNLOCKMessage.SetDestination(updateRUNLOCK);
	  SendMessageOut(RUNLOCKMessage);


	}
      //We just finished the Shutdown OpBlocks and we can go to shutdown
      //The shutdown list
      else
	{
	  shuttingDownStopAddresses = true;
	  Stop(false);
	}
    }
  else if(currentOp.HasFailed())
    {
      //Crap.
      DCMessage::Message errorMessage;
      DCMessage::SingleUINT64 errorData;
      size_t curSize = (RunSetupComplete ? StopBlocks.size():setupBlocks.size());
      size_t curPos = curSize - opQueue.GetSize();
      sprintf(errorData.text,"RunControl failed on op %zu of %zu",
	      curPos,
	      curSize);
      errorMessage.SetDataStruct(errorData);
      errorMessage.SetType(DCMessage::ERROR);
      SendMessageOut(errorMessage);
      Stop(true);
    }
}

void RunControl::UpdateShutdownList(DCMessage::Message & message)
{
  if(!shuttingDownStopAddresses)
    {
      PrintWarning("UpdateShutdownList called with StopOpBlocks still true");
      return;
    }


  //Get the list of thread from the DCMessage
  std::string response;
  DCMessage::DCString dcString;
  //Check if this unpacks ok
  if(!dcString.GetData(message.GetData(),response))
    {
      return;
    }
  //Check if the string has size
  if(response.empty())
    {
      return;
    }
  
  //Check if this is a thread list message
  if(response.find("Thread_Name Thread_Type") != std::string::npos)
    {
      //Check if the last thread has been removed
      if(stopAddresses.back().GetName().size() && 
	 (response.find(stopAddresses.back().GetName()) == std::string::npos)
	 )
	{
	  //The thread is now done
	  //we can now remove this thread's address
	  stopAddresses.pop_back();
	  if(stopAddresses.size() == 0)
	    {
	      //we are all done, stop RunControl
	      StopRunControl();
	    }
	  else
	    {
	      //Send stop to the next thread.
	      SendoutStopMessages();
	    }
	}
      else
	{
	  //If the thread is still there, check if we are going to give up on it. 
	  if(threadShutdownCount >  maxThreadShutdownCount)
	    {
	      std::stringstream ss;
	      ss << "The string " << stopAddresses.back().GetStr() 
		 << " isn't responding to our stop commands. Skipping (stopaddress: " 
		 << stopAddresses.size() <<")";
	      Print(ss.str());	      
	      //we are dropping this one
	      stopAddresses.pop_back();
	      threadShutdownCount = 0;
	      waitTime.tv_sec  = waitTimeBase.tv_sec;
	      waitTime.tv_usec = waitTimeBase.tv_usec;
	      //Go to the next thread.
	      SendoutStopMessages();
	    }
	}
    }
} 
  

void RunControl::ProcessTimeout()
{
  struct timeval currentTime;
  gettimeofday(&currentTime,NULL);
  if(shuttingDownStopAddresses)
    {
      struct timeval timePassed,timeCheck;
      
      //Find time since last stop
      timeval_subtract (&timePassed, 
			currentTime,
			lastTime);
      //Find check if this is larger than wait time      
      if(timeval_subtract(&timeCheck,waitTime,timePassed))
	{
	  gettimeofday(&lastTime,NULL);
	  SendoutStopMessages();
	}
    }
}

void RunControl::StopRunControl()
{

  //If we have set the updateRUNLOCK address, set this run to be stopped so that we will start
  //the next run automatically. 
  if(!updateRUNLOCK.IsBlank())
    {
      std::stringstream ss;
      ss << "Updating the RUNLOCK via " << updateRUNLOCK.GetStr();
      Print(ss.str());
      DCMessage::Message message;
      message.SetType(DCMessage::COMMAND);
      DCMessage::DCString stringParser;
      std::string command;
      if(!run_error)
	{
	  command.assign("RUNLOCK FREE");
	}
      else
	{
	  command.assign("RUNLOCK ERROR");	  
	}
      std::vector<uint8_t> data = stringParser.SetData(command);
      message.SetData(&data[0],data.size());
      message.SetDestination(updateRUNLOCK);
      SendMessageOut(message);
    }
  //Wait for the message to be sent
  time_t start = time(NULL);
  while(OutgoingMessageCount() > 0)
    {
      usleep(10000);
      if((time(NULL) - start) > 3)
	break;
    }
  

  Print("All stop addresses shut down\n");  
  //Shutdown this thead by hand. 
  Loop = false;
  Running = false;
}
