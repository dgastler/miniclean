#include <MemoryBlockManager.h>

MemoryBlockManager::MemoryBlockManager(std::string Name):Lock()
{
  //Make sure our memory block is empty.
  SetName(Name);
  SetType(std::string("MemoryBlockManager"));
  Memory.clear();
  ShutdownStatus = false;
  threadCountWriting = 0;
  threadCountListening = 0;
}

MemoryBlockManager::~MemoryBlockManager()
{
  Deallocate();
}

void MemoryBlockManager::Deallocate()
{
  //This isn't accurate because of threading, but let's take a look
  printf("Deallocating memory with free/full queues at (%zu,%zu)\n",FreeSize(),FullSize());
  //Make sure all the queues are empty. (so something can't mess with things)
  Clear();

    //Loop over allocated blocks and delete them.
  while(Memory.size() >0)
    {
      uint32_t * buffer = Memory.back().buffer;
      Memory.pop_back();
      if(buffer != NULL)
	{
	  delete [] buffer;
	}
    }
}

bool MemoryBlockManager::AllocateMemory(xmlNode * SetupNode)
{
  //Get the block size;
  int blockSizeERR = GetXMLValue(SetupNode,"BLOCKSIZE","MANAGER",blockSize);
  //Get the number of Blocks
  int blockCountERR = GetXMLValue(SetupNode,"BLOCKCOUNT","MANAGER",nBlocks);
    
  Deallocate();  
  if(blockSizeERR || blockCountERR)
    {
      printf("Error:    MemoryBlockManager XML parse error.\n");
      return(false);
    }
  
  MemoryBlock block;
 
  for(unsigned int i = 0; i < nBlocks;i++)
    {
  
      block.buffer = NULL;
      block.buffer = new uint32_t[blockSize];
      if(block.buffer == NULL)
	{
	  fprintf(stderr,"Error:  Block allocation failed!");
	  return(false);
	}
      block.allocatedSize = blockSize;
      block.dataSize = 0;
      Memory.push_back(block);
      int32_t index = int32_t(Memory.size())-1;
      AddFree(index);
    }
  return(true);
}

void MemoryBlockManager::PrintStatus(int indent, int /*verbosity*/)
{
  StatMessage::StatFreeFull sStat;
  sStat.sBase.type = StatMessage::FREE_FULL;
  sStat.sInfo.time = time(NULL);
  sStat.sInfo.subID = SubID::BLANK;
  sStat.sInfo.level = Level::BLANK;
  sStat.allocatedSize = Memory.size();
  sStat.free.size     = FreeSize();
  sStat.free.addCount = FreeAddCount();
  sStat.full.size     = FullSize();
  sStat.full.addCount = FullAddCount();


  DCMessage::Message message;
  message.SetType(DCMessage::STATISTIC);
  message.SetData(sStat);
  SendMessageOut(message);

#ifdef VERBOSE_MEMORY
  printf("\n");
  if(indent > 0)
    {
      for(int i = 0; i < indent;i++)
	{
	  printf(" ");
	}
    }
  printf("MemoryBlockManager(%s)\n     Size: %04"PRIu64"     Full: %04"PRIu64"     (%12"PRIu64")     Free: %04"PRIu64"     (%12"PRIu64")\n",
	 GetName().c_str(),
	 sStat.allocatedSize,
	 sStat.full.size,
	 sStat.full.addCount,
	 sStat.free.size,
	 sStat.free.addCount);
#endif
}
