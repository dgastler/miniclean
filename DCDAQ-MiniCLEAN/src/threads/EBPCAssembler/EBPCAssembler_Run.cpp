#ifdef __RATDS__
#include <EBPCAssembler.h>


void EBPCAssembler::MainLoop()
{
  //Check for a new connection
  if(IsReadReady(server.GetSocketFD()))
    {
      ProcessNewConnection();
    }

  //Check for activity in our connections
  for(size_t iConnection = 0; iConnection < server.Size();iConnection++)
    {
      //Check for a new packet
      if(IsReadReady(server[iConnection].GetFullPacketFD()))
	{
	  ProcessEBPusher(iConnection);
	}
      //Check for a new message
      if(IsReadReady(server[iConnection].GetMessageFD()))
	{
	  ProcessChildMessage(iConnection);
	}
    }  
}

void EBPCAssembler::ProcessEBPusher(size_t iConn)
{

  uint8_t * dataPointer = NULL;
  uint32_t  dataSize = 0;

  //Get new incomming packet
  int32_t iDCPacket = FreeFullQueue::BADVALUE;
  DCNetPacket * packet = NULL;
  server[iConn].GetInPacketManager()->GetFull(iDCPacket,true);
  //Check for bad packet type
  if(iDCPacket != FreeFullQueue::BADVALUE)
    //valid packet
    {
      packet = server[iConn].GetInPacketManager()->GetPacket(iDCPacket);
      //Check if this is the correct kind of packet
      if(!(
	   (packet->GetType() == DCNetPacket::EB_INTEGRATED_WINDOWS_PARTIAL_PACKET) ||
	   (packet->GetType() == DCNetPacket::EB_INTEGRATED_WINDOWS_FULL_PACKET)    ||
	   (packet->GetType() == DCNetPacket::EB_ZLE_WAVEFORMS_PARTIAL_PACKET)      ||
	   (packet->GetType() == DCNetPacket::EB_ZLE_WAVEFORMS_FULL_PACKET)         ||
	   (packet->GetType() == DCNetPacket::EB_PROMPT_TOTAL_PARTIAL_PACKET)       ||
	   (packet->GetType() == DCNetPacket::EB_PROMPT_TOTAL_FULL_PACKET)  
	   ))
	{
	  //ProcessBadPacket cleans up.
	  std::stringstream ss;
	  ss << "Unexpected packet type (" << int(packet->GetType()) << ")";
	  PrintError(ss.str().c_str());
	  //return packet
	  server[iConn].GetInPacketManager()->AddFree(iDCPacket);
	  //move on
	  return;
	}
      //Got a packet with real data. 
      dataPointer = (uint8_t *) packet->GetData();
      dataSize = packet->GetSize();
    }

  //Cast the data to an EBPacket::Event structure.
  if(dataSize >= sizeof(EBPacket::Event))
    {
      //Read the header Event structure from the packet data.
      EBPacket::Event * eventHeader = (EBPacket::Event *) dataPointer;
      //We now need to find the corresponding RATDSBlock event
      //Get this event from the Event hash
      int32_t index;
      RATDSBlock * event = NULL;
      uint64_t password = EBPC_mem->GetBlankPassword();
      //first try to get the event
      int64_t retEBPCmem = EBPC_mem->GetEvent(eventHeader->eventID,index,password);
      while(retEBPCmem >= 0)
	//Loop over this call until we don't have a collision	
	{
	  //Move old event to bad event queue and 
	  //address the FEPCs at fault
	  ProcessCollision(eventHeader->eventID,retEBPCmem,index,password);
	  //Now things should be fixed and we should get our event
	  retEBPCmem = EBPC_mem->GetEvent(eventHeader->eventID,index,password);      
	}      
      
      //No collision
      if(retEBPCmem == returnDCVector::BAD_PASSWORD)
	{
	  server[iConn].GetInPacketManager()->AddFull(iDCPacket);
	  return;
	}
      if(retEBPCmem == returnDCVector::OK)		     
	//We got the index from the vector hash and it's now locked to us. 
	{
	  //Get our event
	  event = EBPC_mem->GetRATDiskBlock(index);
	  
	  //Set up status values if this is a new event. 
	  //(if they haven't been set up yet
	  if(event->eventID == -1)
	    {
	      event->eventID = eventHeader->eventID;
	      event->reductionLevel = eventHeader->dataType;
	      event->state = EventState::UNFINISHED; 
	      int runNumber = GetRunNumber();
	      event->ds->SetRunID(runNumber);
	    }	    
	}
      else 
	{
	  char * textBuffer = new char[100];
	  sprintf(textBuffer,"EBPCmem error! GetEvent returned %"PRIi64"\n",retEBPCmem);
	  PrintError(textBuffer);
	}      

      //Now we have a RATDSBlock for this event and we can fill it with data
      //from the newly arrived packet. 



      int processedChannels = -1;  //A count of how many channels were 
                                   //actually parsed from the packet for
                                   //this event.
      //====================================
      //Event type switch.  Order in most probable first
      //====================================
      reductionDataRate[eventHeader->dataType] += dataSize;
      if(eventHeader->dataType == ReductionLevel::CHAN_ZLE_INTEGRAL)
	{
	  processedChannels = ProcessCHAN_ZLE_INTEGRAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::CHAN_ZLE_WAVEFORM)
	{
	  processedChannels = ProcessCHAN_ZLE_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::CHAN_PROMPT_TOTAL)
	{
	  processedChannels = ProcessCHAN_PROMPT_TOTAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_ZLE_WAVEFORM)
	{
	  processedChannels = ProcessEVNT_ZLE_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_ZLE_INTEGRAL)
	{
	  processedChannels = ProcessEVNT_ZLE_INTEGRAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_PROMPT_TOTAL)
	{
	  processedChannels = ProcessEVNT_PROMPT_TOTAL(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::EVNT_FULL_WAVEFORM)
	{
	  processedChannels = ProcessEVNT_FULL_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::CHAN_FULL_WAVEFORM)
	{
	  processedChannels = ProcessCHAN_FULL_WAVEFORM(dataPointer,dataSize,event);
	}
      else if(eventHeader->dataType == ReductionLevel::UNKNOWN)
	{
	  PrintError("UNKNOWN reduction level!\n");
	}
      else
	{
	  PrintError("Corrupted reduction level!\n");
	}	
	  
      //Check if we have read out all the WFDs for this event.
   
      if(event->WFD.size() == detectorInfo.GetWFDCount())
	{
	  //RAT::DS::EV * ev = event->ds->GetEV(0);

	  //Write a monitoring event
	  eventMonitor(event);	
	  EBPC_mem->MoveEventToDiskQueue(eventHeader->eventID,
					 index,
					 password);
	}
      else
	//if the channel add was fine, but we haven't fully filled
	//the event, return it to the EBPCRATManager
	{
	  EBPC_mem->SetEvent(eventHeader->eventID,index,password);
	}
    }
  else
    {
      PrintError("Packet too small!");
    }
  //return packet
  server[iConn].GetInPacketManager()->AddFree(iDCPacket);

  EBPCEventCount++;
}


void EBPCAssembler::ProcessCollision(int64_t newEventID,
				   int64_t oldEventID,
				   int32_t evIndex,
				   uint64_t password)
{
  //We have a conflict
  PrintWarning("Collision in EBPCAssembler!");
  //The data in the vector hash is for another event. 
  //We have a lock on it though!
  //We will move this event to the bad queue
  //Action concerning this bad event will be left to
  //another thread.
  EBPC_mem->MoveEventToBadQueue(oldEventID,
				evIndex,
				password,
				Level::STORAGE_COLLISION);
  }


void EBPCAssembler::ProcessBadPacketLostPMT(size_t iConn)
{
  //  PrintError("Lost PMT\n");
  PrintWarning("Lost PMT\n");
}
#endif
