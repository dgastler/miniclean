#include <FEPCRATManager.h>

bool FEPCRATManager::AddUnSent(int32_t &index)
{
  bool ret = false;
  ret = UnSent.Add(index);
  return(ret);
}



bool FEPCRATManager::GetBadEvent(BadEvent::sBadEvent &badEvent)
{
  bool ret = false;
  ret = BadEvent.Get(badEvent);
  return(ret);
}


bool FEPCRATManager::AddBadEvent(int64_t eventID,
				 int32_t &index,
				 int32_t errorCode)
{  
  BadEvent::sBadEvent badEvent;
  badEvent.eventID = eventID;
  badEvent.index = index;
  badEvent.errorCode = errorCode;

  return BadEvent.Add(badEvent);
}

