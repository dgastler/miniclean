#include <RATParse.h>

RATParse::RATParse(std::string Name)
{
  SetName(Name);
  SetType(std::string("RATParse"));
  Block_mem = NULL;
  DS_mem = NULL;
  dsBlock = NULL;
  memBlock = NULL;
  dsIndex = FreeFullQueue::BADVALUE;
  memBlockIndex = FreeFullQueue::BADVALUE;

  //Input blocks
  BlocksRead = 0;
  TimeStep_BlocksRead= 0;
  
  //Events
  TimeStep_Events = 0;
  BadEvents = 0;
  TimeStep_BadEvents = 0;

  //Set FDs to bad value
  MemoryBlockManagerFullFDs[0] = BadPipeFD;
  MemoryBlockManagerFullFDs[1] = BadPipeFD;
  BasicRATDSManagerFreeFDs[0]  = BadPipeFD;
  BasicRATDSManagerFreeFDs[1]  = BadPipeFD;
}

bool RATParse::Setup(xmlNode * SetupNode,
		     std::map<std::string,MemoryManager*> & MemManager)
{
  bool ret = true;

  //There should be two memory managers
  // MemoryBlockManager:  interface with raw CAEN data
  // BasicRATdsManager:   interface with the RAT world
  uint32_t XMLMemoryManagerCount = NumberOfNamedSubNodes(SetupNode,"MEMORYMANAGER");  
  if(XMLMemoryManagerCount != 2)    
    {
      PrintError("Memory managers count wrong");
      return(false);
    }
  for(uint32_t iMM = 0; iMM < XMLMemoryManagerCount; iMM++)
    {
      //Get name of memory manager
      std::string memoryManagerName;
      GetIthXMLValue(SetupNode,iMM,"MEMORYMANAGER","THREAD",memoryManagerName);
      //Check if it exists.
      std::map<std::string,MemoryManager*>::iterator itMM;
      if((itMM = MemManager.find(memoryManagerName)) == MemManager.end())
	{
	  char * buffer = new char[100];	  
	  sprintf(buffer,"Error Memory manager %s not found.",memoryManagerName.c_str());
	  PrintError(buffer);
	  delete [] buffer;
	  ret = false;
	}
      else
	{
	  if(itMM->second->GetManagerType() == std::string("MemoryBlockManager"))
	    {
	      if(Block_mem == NULL)
		{
		  Block_mem = dynamic_cast< MemoryBlockManager *> (itMM->second);
		}
	      else
		{
		  char * buffer = new char[100];	  
		  sprintf(buffer,"Error (%s:%s) Multiple memory block managers",itMM->second->GetManagerType().c_str(),itMM->second->GetManagerName().c_str());
		  PrintError(buffer);
		  delete [] buffer;
		  ret = false;
		}
	    }
	  else if(itMM->second->GetManagerType() == std::string("BasicRATDSManager"))
	    {
	      if(DS_mem == NULL)
		{
		  DS_mem = dynamic_cast< BasicRATDSManager * > (itMM->second);
		}
	      else	       
		{
		  char * buffer = new char[100];	  
		  sprintf(buffer,"Error (%s:%s) Multiple basic rat block managers",itMM->second->GetManagerType().c_str(),itMM->second->GetManagerName().c_str());
		  PrintError(buffer);
		  delete [] buffer;
		  ret = false;
		}
	    }
	}
    }
  

  if(!DS_mem)
    {
      fprintf(stderr,"Error %s: Missing DS memory manager\n.",GetName().c_str());
      ret = false;
    }
  if(!Block_mem)
    {
      fprintf(stderr,"Error %s: Missing Block memory manager\n.",GetName().c_str());
      ret = false;
    }
  
  if(ret)
    {
      Ready = true;
    }
  return(ret);
}


bool RATParse::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      //Tell the RAT memory manager to shut down.
      DS_mem->Shutdown();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      RemoveReadFD(BasicRATDSManagerFreeFDs[0]);
      RemoveReadFD(MemoryBlockManagerFullFDs[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Register the memoryblockmanager's full queue's FD so we know when there are
      Block_mem->GetFullFDs(MemoryBlockManagerFullFDs);
      AddReadFD(MemoryBlockManagerFullFDs[0]);
      
      //Register the RAT DS block's free queue's FDs with select
      DS_mem->GetFreeFDs(BasicRATDSManagerFreeFDs);
      AddReadFD(BasicRATDSManagerFreeFDs[0]);
      
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}
