#ifdef __RATDS__

#ifndef __RAT_DISK_BLOCK_MANAGER__
#define __RAT_DISK_BLOCK_MANAGER__

#include <MemoryManager.h>
#include <xmlHelper.h>

#include <DCVector.h>
#include <DCQueue.h>
#include <BadEventStruct.h>
#include <StatMessage.h>

#include <RAT/DS/DCDAQ_RATDSBlock.hh>


class RATDiskBlockManager : public MemoryManager
{
 public:
  RATDiskBlockManager(std::string Name);
  ~RATDiskBlockManager();
  bool AllocateMemory(xmlNode * setupNode);

  uint64_t GetBlankPassword(){return EventStore.GetBlankPassword();};
  
  //Block access
  RATDSBlock * GetRATDiskBlock(uint32_t iBlock) 
    {
      if(iBlock < Memory.size())
	{
	  return (&(Memory[iBlock]));
	}
      return NULL;
    };  
  //FD access
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetToDiskFDs(){return(ToDisk.GetFDs());};
  const int * GetBadEventFDs(){return(BadEvent.GetFDs());};

  //Shutdown
  virtual void Shutdown()
  {
    //Set to shutdown mode.   Now we can't wait on mutex locks
    //This object
    Lock.ShutdownLockWait();
    //Child objects
    Free.ShutdownWait();
    EventStore.ShutdownWait();
    ToDisk.ShutdownWait();
    BadEvent.ShutdownWait();

    //call wakeup to force everyone out of a mutex condition wait.
    WakeUp();
    ShutdownStatus = true;
  };
  virtual bool IsShutdown(){return(ShutdownStatus);};

  //Get the defined bad value
  int32_t GetBadValue(){return(EventStore.GetBadValue());};

  bool AddFree(int32_t &index);
  bool GetFree(int32_t &index);

  //ToDisk
  bool GetToDisk(int32_t &index);
  bool AddToDisk(int32_t &index);

  //BadEvent queue
  bool GetBadEvent(BadEvent::sBadEvent &badEvent);
  bool AddBadEventNOFD(int64_t eventID, int32_t &index, Level::Type errorCode);


  //Vector hash access
  int64_t GetEvent(int64_t Event,int32_t &index,uint64_t &password);
  int64_t SetEvent(int64_t Event,int32_t &index,uint64_t password);
  int64_t ClearEvent(int64_t key, uint64_t password);
     
  //Autopassing functions
  int64_t MoveEventToDiskQueue(int64_t Event,int32_t &index,uint64_t password);
  int64_t MoveEventToBadQueue(int64_t Event,int32_t &index,uint64_t password, Level::Type errorCode);
 
  virtual void PrintStatus(int indent = 0, int verbosity = 0);

  void WakeUp()
  {
    //THis object
    Lock.WakeUpSignal();
    //child objects
    Free.WakeUp();
    EventStore.WakeUp();
    ToDisk.WakeUp();
    BadEvent.WakeUp();
  }

 private: 
  uint32_t NumBlocks;
  uint32_t VectorHashSize;
  std::vector<RATDSBlock> Memory;

  LockedObject Lock;

  //Locked queues
  DCQueue<int32_t,PipeSelect,LockedObject> Free;  //queue of indicies of fre
  DCVector<int32_t,LockedObject> EventStore; //vector-hash of stored events.
  DCQueue<int32_t,PipeSelect,LockedObject> ToDisk; //queue of indicies to disk
  DCQueue<BadEvent::sBadEvent,PipeSelect,LockedObject> BadEvent; //queue of indexes that have collided. 

  void Deallocate();

  volatile bool ShutdownStatus;
};

#endif

#endif
