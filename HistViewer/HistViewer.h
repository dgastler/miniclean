#ifndef __HistViewer__
#define __HistViewer__

#include <TTimer.h>
#include <TH1F.h>
#include  <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <mysql/mysql.h>
#include <sstream>
#include <stdlib.h>
#include <TGButton.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TTimeStamp.h>
class HistViewer: public TGMainFrame{

 private:
  HistViewer();
  TH1F * oneHourHist, * fiveHourHist, *oneDayHist, * tenHourHist, *customHist;
  TGHorizontalFrame * windowFrame,*DateFrame;
  TGVerticalFrame * controlBar,*customFrame;
  TGTextButton * oneHourButton,*fiveHourButton,*tenHourButton,*oneDayButton,*combineButton,*customButton,*DrawButton,*ResetAxesButton;
  TGNumberEntry * DateStart, * DateEnd, *TimeStart, *TimeEnd;
  TGMainFrame * CustomWindow;
  TCanvas * customCanvas;
  TGLabel * startLabel,*spaceLabel,*endLabel;
  TRootEmbeddedCanvas * histECan,*customECan;
  TGLayoutHints *layoutHint,*layoutHintBar;
  TCanvas * histTCanvas;
  unsigned int width,height;
  MYSQL * mysqlConn;
  TTimer * mysqlTimer;
  struct CountInfo{
    int time;
    float count;
  };
  int checktime;
  std::vector<std::vector<CountInfo> > oneHourInfo;
  std::vector<std::vector<CountInfo> > fiveHourInfo;
  std::vector<std::vector<CountInfo> > tenHourInfo;
  std::vector<std::vector<CountInfo> > oneDayInfo;
 
  enum ActivePlotEnum{
    oneHour=0,
    fiveHour,
    tenHour,
    oneDay,
    combine,
  } ActivePlot;


public:
  HistViewer(const TGWindow *p, UInt_t w, UInt_t h);
  virtual ~HistViewer();

  void SetupGUI();
  void SetupMySQL();
  void CheckMySQL();
  void CheckMySQL(int,int);
  void DrawHists();
  void SetupHists();
  void CloseWindow();
  void oneHourActive();
  void fiveHourActive();
  void tenHourActive();
  void oneDayActive();
  void combineActive();
  void customTime();
  void CustomWindowClosed();
  void DrawCustomHist();
  void ResetAxes();

  ClassDef(HistViewer,0)
};
    
    

#endif
