#include "v1720Event.h"
#include <math.h>

v1720Window::v1720Window()
{
  rawData = 0;
  size = 0;
  timeOffset = 0;
  twoPack = false;
}
v1720Window::~v1720Window()
{
  rawData = 0;
  size = 0;
  timeOffset = 0;
  twoPack = false;
}

void v1720Window::Setup(uint32_t * _rawData,uint32_t _size,bool _twoPack)
{
  rawData = _rawData;
  size = _size;
  twoPack = _twoPack;
}

uint16_t v1720Window::GetSample(uint32_t i)
{
  uint16_t ret = 0;
  //If we are using two packing use this function.
  if(twoPack)
    {
      ret = GetSample20pk(i);
    }
  //If we are using 2.5 packing use this function.
  else
    {
      ret = GetSample25pk(i);
    }
  return(ret);
}

uint32_t v1720Window::GetSize()
{
  uint32_t ret = 0;
  if(twoPack)
    {
      ret = 2*size;
    }
  else
    {
      ret = (uint32_t) ( 2.5*size);
    }
  return(ret);
}

uint16_t v1720Window::GetSample20pk(uint32_t i)
{
  uint16_t ret = 0xFFFF;
  if(i >= 0 && (i < (size<<1)) )
    {
      ret = ((uint16_t *) rawData)[i];
      if(ret&0xF000)
	{
	  fprintf(stderr,"Bad 2.0 packed word\n");
	}
    }
  return(ret);
}

uint16_t v1720Window::GetSample25pk(uint32_t i)
{
  uint16_t ret = 0xFFFF;
  //Parse everything in 64bit chuncks so we have an integer
  //number of samples in our array elements.
  uint64_t * rawData64 = (uint64_t *) rawData;
  uint64_t blocktemp = 0;
  uint64_t block = 0;
  //  uint64_t bigMask = uint64_t (0xFFFFFFF C0000000);
  //The next three lines are a hack to build a mask
  //used to get the correct bits from the shifted
  //upper 32 bits from the 64bit word
  uint64_t bigMask = 0xFFFFFFF;
  bigMask = bigMask << 32;
  bigMask |= 0xC0000000;
  if((i >= 0) && (i < (size*2.5)))
    {
      //Find the correct 64bit word.
      blocktemp = rawData64[i/5];
      //Set block to have the correct lower 30 bits.
      block = blocktemp & 0x3FFFFFFF;
      //Shift block temp by two bits, to cover for the
      //00 padding on the 32 bit words.
      blocktemp = (blocktemp >> 2 );
      //Or block with the masked bits(60-30) of the blocktemp
      block = block | ( blocktemp & bigMask);
      //Shift the bits till the correct 12bit sample is at the bottom.
      block = block >> (i%5)*12;
      //mask off those 12 bits and return them.
      ret = block & 0xFFF;
    }
  return(ret);
//  uint16_t ret = 0xFFFF;
//  //  if(i >= 0 && i < (size * 1.6) )
//  if(i >= 0 && i < (size * 2.5) )
//    {
//      //Find 16bit word with the lowest bit of the ith sample in it.
//      //There are 0.8 16bit words per sample.
//      uint16_t lower = ((uint16_t *) rawData)[(uint32_t)floor(i*0.8)];      
//      //Find 16bit word with the highest bit of the ith sample in it.
//      uint16_t upper = ((uint16_t *) rawData)[(uint32_t)ceil(i*0.8)];
//      //Find how much we need to shift the lower bits to put the lowest
//      //bit at bit 0.
//      //Take decimal leftover from the index (i*0.8)%1 and then multiply
//      //it by the number of bits in a 16 bit word.
//      uint32_t shift = (uint32_t) floor(16*(    ((uint32_t)(i*0.8))%1   )   );     
//      //Shift the lower index by this amount.
//      lower = lower >> shift;
//      //shift the upper index by 16- this amount.
//      upper = upper << (16 - shift);
//      //Or the lower and upper bits and mask off the top 4 bits.
//      ret = (lower | upper)&0xFFF;
//    }
//  return(ret);
}




