#ifndef __STACKTRACER__
#define __STACKTRACER__
//#include <execinfo.h>
//#include <stdio.h>
//#include <stdlib.h>



extern "C" {
#include <backtrace-symbols.h>
}
#include <cxxabi.h>
#include <string>
#include <dlfcn.h>

/* Obtain a backtrace and print it to stdout. */
void print_trace();
template<class T>
char * GetFunctionName(char * buffer,size_t size, T funcPtr)
{
  //structure to store the function info
  Dl_info info;
  memset(&info,0,sizeof(info));
  
  //Fill info for funcPtr
  //To get around casting, we have to take the function pointer, get a pointer to it, 
  //recast that pointer to a function pointer to a void **, then derefecence to void *.
  //Ugly
  dladdr( *((void**) & funcPtr),&info);

  int status;
  abi::__cxa_demangle(info.dli_sname,buffer,&size,&status);
  //Check if this was really a demangled C++ name, if not return the original
  if(status != 0)
    {
      snprintf(buffer,size,info.dli_sname);
    }
  return buffer;
}
#endif
