#ifdef __RATDS__

#ifndef __DRPCASSEMBLER__
#define __DRPCASSEMBLER__
//DRPCAssembler 

#include <DCThread.h>
#include <xmlHelper.h>

#include <ServerConnection.h>
#include <Connection.h>
#include <DRDataPacket.h>

#include <DRPCRatManager.h>

#include <DetectorInfo.h>

namespace PacketParsingError
{
  enum
  {
    COLLISION = -1,
    BAD_PASSWORD = -2,
    OTHER_ERROR = -3
  };
}

class DRPCAssembler : public DCThread 
{
 public:
  DRPCAssembler(std::string Name);
  ~DRPCAssembler() {};

  bool Setup(xmlNode * SetupNode,
	     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  enum RATAssemberErrorCodes
  {
    OK = 0,
    LOCK_BLOCKED    = -1,
    WFD_NOT_PROCESSED = -2,
    FATAL_ERROR       = -3,
    BAD_WFD_HEADER    = -4
  };

  
  //======================================
  //DRPCAssembler.cpp
  //======================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ProcessChildMessage(size_t iConn);
  virtual void PrintStatus();
  //======================================

  //======================================
  //DRPCAssembler_Run.cpp
  //======================================
  void ProcessDRPusher(Connection & conn);
  int32_t ProcessDRPusherEvent(uint8_t * & data,
			       uint16_t &maxSize);			    
  void ProcessCollision(int64_t newEventID,
			int64_t oldEventID,
			int32_t evIndex,
			uint64_t password);
  bool AddDataToEV(RATEVBlock * event,
		   uint8_t * &data,
		   uint32_t &dataSizeMax,
		   int boardCount);
  void ParseBadPacket(DCNetPacket * packet);
  //  void ProcessBadPacketLostPMT(DCNetPacket *packet);
  //======================================

  //======================================
  //DRPCAssembler_Net.cpp
  //======================================
  bool ProcessNewConnection();
  void DeleteConnection(size_t iConn);
  //======================================

  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  ServerConnection server;
  
  //Data reduction PC memory manager
  DRPCRATManager * DRPC_mem;
  DetectorInfo detectorInfo;

  uint8_t badCount;
  time_t lastErrorTime;

  uint8_t lastChannelWord;
  uint16_t lastRawIntegral;
  uint16_t lastRawStartTime;
  uint8_t lastRawWidth;
  
  struct DebugInformation
  {
    uint8_t type;
    size_t sizeLeft;
  };

  std::vector<DebugInformation> debugInformation;

  int32_t runningTrigger;

  virtual void  ThreadCleanUp();

};


#endif

#endif
