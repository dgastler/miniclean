#include "DAQFile.h"

DAQFile::DAQFile()
{
  outFile = NULL;
  validData = false;
}

DAQFile::~DAQFile()
{
  CloseFile();
}

void DAQFile::CloseFile()
{
  if(outFile)
    {
      fclose(outFile);
    }
  outFile = NULL;
  filename.clear();
}

int DAQFile::SetFileName(std::string name)
{
  //If a file already exists close the file.
  if(outFile)
    {
      CloseFile();
    }
  //Create new file
  filename = name;
  outFile = fopen(filename.c_str(),"w");
  if(outFile == NULL)
    return (-1);
  return(0);
}

int DAQFile::Process(RAT::DS::Root * ds)
{
  int ret = 0;
  validData = false;
  std::vector<FakeWFD *>::iterator it;

  //Have each WFD process this event
  for(it = WFDs.begin(); it < WFDs.end(); it++)
    {      
      //Process the current event
      ret += (*it)->Process(ds);
      // if somethign bad happens while processing return the negative
      // return code
      if(ret < 0)
	{	
	  break;
	}
    }
  if(ret == 0)
    {
      validData = true;
    }
  return(ret);
}

int DAQFile::Write()
{
  //return -1 if there isn't any valid data to write
  if(!validData)
      return(-1);
  
  int ret = 0;
  //write data
  const uint32_t * dataBuffer = NULL;
  uint32_t dataSize = 0;

  size_t writeSize = 0;
  std::vector<FakeWFD *>::iterator it;  
  for(it = WFDs.begin(); it < WFDs.end(); it++)
    {      
      //Get a pointer to this WFD'a data and the data size.
      dataBuffer = (*it)->GetData(dataSize);
      if(dataBuffer)
	{
	  //write data
	  size_t size = fwrite(dataBuffer,sizeof(uint32_t),dataSize,outFile);
	  if(size != dataSize)
	    {
	      ret = -1;
	      break;
	    }
	  writeSize += size*sizeof(uint32_t);
	}
    }  
  validData = false;
  if(ret != -1)
    ret = writeSize;
  return(ret);
}
