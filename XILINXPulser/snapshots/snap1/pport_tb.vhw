--------------------------------------------------------------------------------
-- Copyright (c) 1995-2003 Xilinx, Inc.
-- All Right Reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 10.1.03
--  \   \         Application : ISE
--  /   /         Filename : pport_tb.vhw
-- /___/   /\     Timestamp : Tue Nov 25 16:59:36 2008
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: 
--Design Name: pport_tb
--Device: Xilinx
--

library UNISIM;
use UNISIM.Vcomponents.ALL;
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE STD.TEXTIO.ALL;

ENTITY pport_tb IS
END pport_tb;

ARCHITECTURE testbench_arch OF pport_tb IS
    FILE RESULTS: TEXT OPEN WRITE_MODE IS "results.txt";

    COMPONENT pport
        PORT (
            CLOCK : In std_logic;
            PPORT : In std_logic_vector (7 DownTo 0);
            LED : Out std_logic_vector (7 DownTo 0);
            PULSE : Out std_logic
        );
    END COMPONENT;

    SIGNAL CLOCK : std_logic := '0';
    SIGNAL PPORT : std_logic_vector (7 DownTo 0) := "00000000";
    SIGNAL LED : std_logic_vector (7 DownTo 0) := "00000000";
    SIGNAL PULSE : std_logic := '0';

    constant PERIOD : time := 0 us;
    constant DUTY_CYCLE : real := 0.5;
    constant OFFSET : time := 0 us;

    BEGIN
        UUT : pport
        PORT MAP (
            CLOCK => CLOCK,
            PPORT => PPORT,
            LED => LED,
            PULSE => PULSE
        );

        PROCESS    -- clock process for CLOCK
        BEGIN
            WAIT for OFFSET;
            CLOCK_LOOP : LOOP
                CLOCK <= '0';
                WAIT FOR (PERIOD - (PERIOD * DUTY_CYCLE));
                CLOCK <= '1';
                WAIT FOR (PERIOD * DUTY_CYCLE);
            END LOOP CLOCK_LOOP;
        END PROCESS;

        PROCESS    -- Process for CLOCK
            BEGIN
                WAIT FOR 1000 us;

            END PROCESS;

            PROCESS    -- Process for Asynchronous Signals
                BEGIN
                    -- -------------  Current Time:  108us
                    WAIT FOR 108 us;
                    PPORT <= "01111111";
                    -- -------------------------------------
                    WAIT FOR 891 us;

                END PROCESS;

        END testbench_arch;

