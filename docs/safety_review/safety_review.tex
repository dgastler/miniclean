\documentclass[11pt]{article}
\usepackage{amsmath,amsfonts,amsthm,amssymb}
\usepackage{setspace}
\usepackage{color}
%\usepackage{float}
%\usepackage[pdftex]{graphicx}
\usepackage[pdftex]{graphicx}
\usepackage{caption}
\usepackage{multirow}

% In case you need to adjust margins:
\topmargin=-0.4in      %
\evensidemargin=0in     %
\oddsidemargin=0in      %
\textwidth=6.75in        %
\textheight=9.0in       %
\headsep=0.25in         %

\title{MiniCLEAN Electronics \& DAQ System}
\date{}
\author{Dan Gastler, Steve Linden, \& Ed Kearns}

\begin{document}
\maketitle
\section{Specifications of the Electronics \& DAQ Subsystem}
\label{sec:requirements}
The MiniCLEAN electronics and data acquisition system is designed to power and record the 92 PMTs used in the experiment. 
Because of the importance of photon counting and timing for pulse shape discrimination (PSD), waveforms of each PMT will be saved with a sampling rate of $250$~Mhz at 12-bit resolution and given the time constant of late scintillation light, these waveforms must be of order $10~\mu$s.
MiniCLEAN will employ 13 CAEN V1720 waveform digitizers (WFDs) to digitize this data. 

The background rate in MiniCLEAN is projected to be approximately $600$~hz.
Saving the waveforms for each of these events in full would lead to a data rate that would be too large to handle.
To address this, two layers of data reduction are used to reduce the data rate. 
First, the V1720 WFD will perform zero suppression in hardware, removing empty sections of the waveforms.
Later, the DAQ software will further reduce the data rate by only saving summary data for events outside of our region of interest. 
\section{System Overview}
\label{sec:System_Overview}
The basic purpose of the electronics and DAQ systems is to digitize and save the activity of the 92 MiniCLEAN photo-tubes for events of interest. 
The system provides the PMTs with high voltage through custom built HV-Block electronics.  
This component will also pick off the PMT pulses from the HV line and pass them on to the WFDs for digitization. 
The 13 WFDs will be located in one VME64 crate and will be placed in four groups, with each group read out by its own front-end PC (FEPC). 

With the exception of the 13th WFD used for the VETO electronics, the WFDs each output an internal hit-sum that are all passed to an analog summing and discrimination electronics located in one NIM crate.
This will produce an event trigger when there is a coincidence of order five hits. 
This trigger will be passed off to the V1720 Event Number And Trigger ORganizer (VENATOR) Module in combination with any other triggers. 
The VENATOR module is a custom built, FPGA based, module that controls the WFDs and determines the times, trigger patterns and event numbers of MiniCLEAN events. 

Partial events from the WFDs are read out optically by the four FEPCs and pulse charges and times from each event are sent over a network to a data reduction PC (DRPC).
This summary event is then processed and a decision is made as to what level of software reduction will be applied to this event, and then the decision is sent back to the FEPCs.
The FEPCs then send the required data to a final event builder PC (EBPC) to be written to disk. 
Currently the DRPC and EBPC are going to be one PC, but can be two PCs if required. 
A system diagram is shown in Fig.~\ref{fig:System_Diagram}.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=5in]{figs/MiniCLEAN-Electronics-Diagram.pdf}
  \end{center}
  \caption{A system diagram for the MiniCLEAN DAQ system from the HV supply, through the WFDs, DAQ software and to disk.}
  \label{fig:System_Diagram}
\end{figure}

\section{Hardware}
\label{sec:Hardware}
This section gives details about the individual components of the DAQ and a summary table of the power required. All components listed here will be ESA inspected prior to installation.
\subsection{Purchased Hardware}
\label{ssec:Purchased}
\subsubsection{HV Supply}
\label{sssec:HV_supply}
VISyN 1458 crate with eight VISyN 1451 HV modules.
$2.5$~mA/channel current limiter.
\subsubsection{MCX Cables}
\label{sssec:MCX_cables}
To connect the low voltage signal output of the HV-Block to the V1720 WFDs, 92 MCX to MCX cables are used. 
These cables were built by Pasternack and are each 72" Long and are made out of RG316U cable with PTFE/FEP jacket material to reduce fire hazards for short cables. 
\subsubsection{CAEN V1720 Waveform Digitizer}
\label{sssec:WFDs}
The PMT signals are read out by 13 CAEN V1720 Waveform Digitizers.  Each digitizer draws $5$A at $5$V, $0.2$A at $12$V and $0.2$A at $-12$V, all of which is supplied by the custom VME crate in which the digitizers are housed (see below). 
\subsubsection{Front-end, Data reduction, Event Builder, and shift PCs}
\label{sssec:DAQPCs}
The PCs used in the DAQ system are standard high end rack-mount computers and will reside in the second rack.

\subsection{Custom Built Hardware}
\label{ssec:Custom}
\subsubsection{VME Crate}
\label{sssec:VMECrate}
The VME crate that will be used is a 22 slot VME64 crate powered by two $12$~VDC and one $5$~VDCa TDK-Lambda power supplies. 
The HWS600-5 supply is a $600$~Watt, $5$~VDC supply which is capable of driving the 70A $5$~V line currents.
The $\pm 12$~V supplies are also from TDK-Lamba and are lower $125$~Watt max supplies. 
\subsubsection{HV \& PMT Cables}
\label{sssec:HVCables}
The HV cables have not yet been purchased, but will be $50~\Omega$, equal length cables rated for high voltage.  
The cables will be FT4 rated and follow all other safety requirements for long cables. 
These will be grounded to the electronics racks via the HV Block' connectors. 
\subsubsection{HV Block \& Pulse Injection}
\label{sssec:HVBlock}
The HV-Block will be a custom built rack-mount crate that will be used to pick off the PMT signal from the HV lines. 
It will connect to the HV supply and the PMTs via HV cable with SHV connectors and connect to the WFDs via low voltage MCX cables/connectors. 
The design of these board will be done with the assistance of the Boston University Electronics Design Facility (EDF) and consist of 8 channel HV blocking cards (1 per WFD) with one channel having pulse injection for timing alignment of the WFDs.   
\subsubsection{VENATOR Module}
\label{sssec:Venator}
The VENATOR Module processes triggers and organizes the WFD triggering and digitization. 
It will have three small DC power supplies consuming approximately $30$~Watts in total. 

\subsection{Power Requirements}
\label{ssec:Power_requirements}
The power consumption of the two racks is detailed in table \ref{tb:power_requirements}.
\begin{table}[hbt]
\begin{center}
  \begin{tabular}{|c|r|c|c|c|}\hline
    Rack & Hardware & Voltage(V) & Current(A) & Power (Watts) \\ \hline
    \multirow{11}{*}{1} & NIM Crate & AC & - & 150 (max) \\ \cline{2-5}
    & HV-Block & AC & - & 30 \\ \cline{2-5}
    & \multirow{4}{*}{VME Crate} & 5-DC & 65 & 325 \\ \cline{3-5}
    &  & 12-DC & 2.6 & 31.2 \\ \cline{3-5}
    &  & -12-DC & 2.6 & 31.2 \\ \cline{3-5}
    & \bf{Total:} & AC & - & \bf{400} \\ \cline{2-5}
    &\multirow{4}{*}{VENATOR Module} & -5-DC & 1.1 & 5.5 \\ \cline{3-5}
    & & 5-DC & 1.2 & 6 \\ \cline{3-5}
    & & 3.3-DC & 5 & 16.5 \\ \cline{3-5}
    & \bf{Total:} & AC & - & \bf{30} \\ \cline{2-5}
    & HV Supply & AC & - & 50 \\ \hline \hline
    1 & \bf{Total:} & - & - & \bf{700} \\ \hline \hline
    
    \multirow{7}{*}{2} & Data Reduction/Event Builder - PC & AC & - & 550 (max) \\ \cline{2-5}
    & Front End PC-1 & AC & - & 550 (max) \\ \cline{2-5}
    & Front End PC-2 & AC & - & 550 (max) \\ \cline{2-5}
    & Front End PC-3 & AC & - & 550 (max) \\ \cline{2-5}
    & Front End PC-4 & AC & - & 550 (max) \\ \cline{2-5}
    & Shift PC & AC & - & 300 (max) \\ \cline{2-5}
    & network switch & AC & 1.3 & 150 \\ \hline \hline
    2 & \bf{Total:} & - & - & \bf{3200} \\ \hline \hline

    \bf{Total} & - & AC & - & 4000 \\ \hline
  \end{tabular}
  \caption{Table of electronics \& DAQ system power by rack.}
  \label{tb:power_requirements}
\end{center}
\end{table}

\subsection{Grounding}

Electrical ground for all components of the DAQ subsystem will ultimately be provided through the MiniCLEAN power panel, 423-PP-MC-02.  The two DAQ racks will be grounded to the power panel and to each other.  All DAQ components will be grounded to the DAQ racks.  PMT cables will also be grounded to the DAQ racks.

\section{Assembly of the Electronics and DAQ subsystem}
The electronics will be delivered to SNOLab in two racks.  All custom modules will already have been built, tested, and housed in the racks before being moved to SNOLab.  The only assembly required on-site will be the cabling of the electronics and HV block.  In most cases, and in particular for all cables carrying HV, the connectors are such that they can only be plugged in to the correct port; connecting the cables will therefore be a very straightforward procedure.

\section{Hazards and Mitigation for the Electronics and DAQ subsystem}
During the instillation, operation, and maintenance of the components of these systems there are several potential electrical hazards.
The PMTs must be provided with low current high voltage to operate and this must pass through custom electronics before digitization. 
The 13 WFDs require sizable currents at low voltages via a custom-built VME crate.
Additionally, all components of the system will require standard AC for power.

Thought has been given to the hazards from these systems and safety is the most important part of the design of the components. 
To mitigate the risk of damage and injury from the HV electronics, the HV supply is outfitted by the manufacturer with a HV-trip that will cut the HV in the case of an accidental short.  As noted above, cables carrying HV will all use SHV connectors, mitigating the danger of an accidental short.
Since the HV-blocking electronics will interact with the high voltage, it will be contained inside of its own box to prevent accidental exposure to HV and will be grounded through the rack. 
Design of this component has been in association with the Boston University Electronics Design Facility, which has experience with the design and operation of HV electronics and specifically with HV blocking electronics for PMTs.

The VME power supplies are of low voltage ($5$,$\pm12$), but do supply substantial currents ($70$~amps @ $5$~V). 
The supplies used are commercial supplies designed to be incorporated inside of crates like those that will be used and instructions from the manufacture have been followed in their incorporation into the crates. 
Maintenance on these crates will only be done by or with the approval of the system expert and cables carrying high currents will be placed to minimize the risk of accidental contact. 

All other supplies are of low current and voltage and will also be placed inside of rack mounted boxes for grounding.   
Basic maintenance of AC power lines will always be conducted with power off and disconnected. 
\end{document}
