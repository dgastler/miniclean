#include "DAQTestData.h"

DAQTestData::~DAQTestData()
{
  //Loop over all the files and delete them.
  while(Files.size())
    {
      delete Files.back(); //Delete the data
      Files.pop_back(); //remove pointer from vector
    }
}

int DAQTestData::Process(RAT::DS::Root * ds)
{
  int writeSize = 0; //Return value
  std::vector<DAQFile *>::iterator it;
  //Process this event
  for(it = Files.begin(); it < Files.end(); it++)
    {      
      //Process the current event
      int ret = (*it)->Process(ds);
      // if somethign bad happens while processing return the negative
      // return code
      if(ret < 0)
	{	
	  writeSize = ret;
	  break;
	}
    }
  if(writeSize ==0) //If this is non-zero something bad happened in parse
    {
      //Write the processed event to disk
      for(it = Files.begin(); it < Files.end(); it++)
	{
	  int ret = (*it)->Write();
	  //Something bad happened and we will return failure.
	  if(ret < 0)
	    {
	      writeSize = ret;
	      break;
	    }
	  //We wrote to disk, so keep track of how much we wrote.
	  else
	    {					
	      writeSize +=ret;
	    }
	}
    }
  return(writeSize);
}
