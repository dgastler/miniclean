#ifndef __TEST_BENCH_BLOCK__
#define __TEST_BENCH_BLOCK__

#include <vector>
#include "RAT/DS/EV.hh"

struct TestBenchBlock
{   
  //Pointer to RAT EV structure
  RAT::DS::EV * ev;
  //Vector of heard from WFDs
  std::vector<uint8_t> WFD;
  int64_t eventID;
  
  TestBenchBlock(){ev = NULL;Clear();};
  void Clear()
  {
    eventID = -1;
    WFD.clear();
    if(ev != NULL)      
      ev->PrunePMT();
  };
  ~TestBenchBlock()
  {
  }
};
#endif
