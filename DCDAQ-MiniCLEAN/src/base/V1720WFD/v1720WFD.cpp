#ifdef __CAENLIBS__

#include "v1720WFD.h"
#include "xmlHelper.h"

v1720::v1720()
{
  Handle = -1;
  BdType = CAEN_DGTZ_PCI_OpticalLink;
  BdName.assign("NONE");
  Link = 0;
  BdNum = 0;
  VMEBaseAddress = 0;
  ChannelsConfig = 0;
  MemConfig = 0;
  ROCFPGA = 0;
  Ready = false;
  BoardID = 0xFFFFFFFF;
  sendStart = false;
}

v1720::~v1720()
{
  if(Handle != -1)
    {
      //Try to reset the device as a desperate attempt to make
      //the CAEN WFDs not jam forever.... (Why can't STRUCK jsut invade CAEN)
      //      CAENVME_DeviceReset(Handle);
      //CLose the fd
      CAEN_DGTZ_Reset(Handle);
      CAEN_DGTZ_CloseDigitizer(Handle);
    }
  Channels.clear();
  //clean up XML somethings.  
}

int v1720::Start()
{
  int ret = 0;
  if(sendStart)
    {
      if(Ready)
	{
	  if(ROCFirmware >=3.4)
	    {
	      //sync
	      //	      ret += SetRegister(VMEBaseAddress + 0x813c,1);
	      //	      sleep(1);
	      //Start a run
	      //	      ret += CAEN_DGTZ_SendSWtrigger(Handle);
	      //enable external triggers to this board
	      //	      ret += SetRegister(VMEBaseAddress + 0x817C,0);
	      //	      printf("Starting WFD %d\n",BoardID);
	    }
	  else
	    {
	      //	      ret += CAEN_DGTZ_SWStartAcquisition(Handle);
	      //	      printf("Starting WFD %d\n",BoardID);
	    }
	}
      else 
	{
	  ret++;
	}
    }
  return ret;
}

int v1720::Stop()
{
  int ret = 0;
  if(Ready)
    {
      //Keep all bits the same except for bit 2
//      ret += a32d32w(VMEBaseAddress + 0x8100,
//		     AcquisitionControl&0xFFFFFFFB);
      ret += CAEN_DGTZ_SWStopAcquisition(Handle);
    }
  else 
    {
      ret++;
    }
  return(ret);
}

int v1720::SoftwareTrigger()
{
  int ret = 0;
  if(Ready)
    {
      //      ret += a32d32w(VMEBaseAddress + 0x8108,0x0);
      ret += CAEN_DGTZ_SendSWtrigger(Handle);
    }
  else
    {
      ret++;
    }
  return(ret);
}

int v1720::Readout(MemoryBlock &block,size_t maxEventsToRead)
{
  occupancy = 0;
  goodBERR = 0;
  badBERR = 0;
  VMEtransfers = 0;
  numberOfEvents = 0;
  TransferredBytes = 0;
  int ret = OK;
  if(Ready)
    {
      //Check out how many events are available
      int CAENret = ReadRegister(VMEBaseAddress + 0x812C,occupancy);      
      if(CAENret != 0)
	{
	  return CAENret;
	}
      if(occupancy>0)
	{	  
	  if(occupancy < maxEventsToRead)
	    maxEventsToRead = occupancy;
	  
	  if((maxEventsToRead*EventSize) < (block.allocatedSize - block.dataSize))
	    {
	      uint32_t transferSize = 0;
	      //Set the block transfer to maxEventsToRead
	      CAENret = CAEN_DGTZ_SetMaxNumEventsBLT(Handle,maxEventsToRead);
	      if(CAENret != 0)
		{
		  return CAENret;
		}
	      CAENret = CAEN_DGTZ_ReadData(Handle,
					   CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT,
					   (char*) (block.buffer + block.dataSize),
					   &transferSize
					   );
	      if(CAENret == CAEN_DGTZ_Success)
		{
		  CAENret = CAEN_DGTZ_Success;
		  //If there is an actual event, update internals
		  if(transferSize > 0)
		    {
		      block.dataSize += (transferSize >> 2);
		      TransferredBytes += (transferSize); //conver to bytes
		      numberOfEvents+=maxEventsToRead;
		    }		      
		}
	      else
		{
		  printf("ERROR! in v1720 Readout: %d (Events to read %zu (%u words))\n",
			 CAENret,maxEventsToRead,transferSize);
		}
	    }
	  else
	    {
	      ret = BUFFERFULL;
	    }
	}
    }
  else
    {
      ret = NOTREADY;
    }
  return ret;
}

bool v1720::TwoPack()
{
  bool ret = true;
  if((0x1 & (ChannelsConfig >> 11)) == 0x1)
    {
      ret = false;
    }
  return(ret);
}

#endif
