#ifndef __MEMORYBLOCKMANAGER__
#define __MEMORYBLOCKMANAGER__

#include <RAT/DS/DCDAQ_MemBlocks.hh>
#include <MemoryManager.h>
#include <FreeFullQueue.h> 
#include <xmlHelper.h>
#include <StatMessage.h>
#include <LockedObject.h>

class MemoryBlockManager : public FreeFullQueue, public MemoryManager 
{
 public:
  MemoryBlockManager(std::string Name);
  ~MemoryBlockManager();
  bool AllocateMemory(xmlNode * setupNode);

  virtual void Shutdown()
  {
    Lock.ShutdownLockWait();
    ShutdownQueues(); 
    ShutdownStatus = true;
  };
  virtual bool IsShutdown(){return(ShutdownStatus);};
  virtual void WakeUp(){WakeUpQueues(); Lock.WakeUpSignal();};

  virtual void PrintStatus(int indent = 0, int verbosity = 0);

  virtual bool AddFree(int32_t & Index)
  {
    if((Index < int32_t(Memory.size())) && (Index >= 0))
      {
	Memory[Index].dataSize=0;
	Memory[Index].uncompressedSize=-1;
      }
    return FreeFullQueue::AddFree(Index);
  }

  virtual bool AddFull(int32_t & Index)
  {
    return FreeFullQueue::AddFull(Index);
  }

  virtual bool GetFull(int32_t & index,bool wait = true)
  {
    return FreeFullQueue::GetFull(index);
  }

  MemoryBlock * GetBlock(int32_t i)
  {    
    if((i < int32_t(Memory.size())) && (i >= 0))
      return &(Memory[i]);
    return NULL;
  }

  virtual bool AddEndOfRun(int32_t & Index)
  {
    if(!Lock.LockObject(true))
      {
	fprintf(stderr,"Failed to lock object!\n");
	return false;
      }
    bool ret = false;
     fprintf(stderr,"AddEndOfRun has been called with index %i\n",Index);
    if(Index==FreeFullQueue::SHUTDOWN)
      {
	//Add (threadCountListening) shutdown indices
	for(int i = 0; i < threadCountListening; i++)
	  {
	    int32_t shutdown = FreeFullQueue::SHUTDOWN;
	    FreeFullQueue::AddFull(shutdown);
	  }
	ret = true;
      }
    else
      {
	//Add (threadCountListening - 1) shutdown indices
	for(int i = 1; i < threadCountListening; i++)
	  {
	    int32_t shutdown = FreeFullQueue::SHUTDOWN;
	    FreeFullQueue::AddFull(shutdown);
	  }
	//Add the end of run block
	ret = FreeFullQueue::AddFull(Index);
	//Add one more shutdown index
	int32_t shutdown = FreeFullQueue::SHUTDOWN;
	FreeFullQueue::AddFull(shutdown);
      }
    Lock.UnlockObject();
    return ret;
  }

  uint32_t GetBlockSize(){return blockSize;}

  //Tell the MM how many threads are interacting with it
  virtual void IncrementWritingThreads(){threadCountWriting++;};
  virtual void IncrementListeningThreads(){threadCountListening++;};

  virtual bool DeIncrementWritingThreads()
  {
    bool ret = false;
    if(!Lock.LockObject(true))
      {
       	fprintf(stderr,"Failed to lock object!\n");
	return false;
      }
    threadCountWriting--;
    if(threadCountWriting==0)
      {
	ret = true;
      }
    else
      ret = false;

    Lock.UnlockObject();
    return ret;
  };

  virtual bool DeIncrementListeningThreads()
  {
    bool ret = false;
    if(!Lock.LockObject(true))
      {
	fprintf(stderr,"Failed to lock object!\n");
	return false;
      }
    threadCountListening--;
    if(threadCountListening==0)
      ret = true;     
    else
      ret = false;

    Lock.UnlockObject();
    return ret;
  };

 private:
  uint32_t blockSize;
  uint32_t nBlocks;
  std::vector<MemoryBlock> Memory;
  void Deallocate();
  volatile bool ShutdownStatus; 
  LockedObject Lock;
  MemoryBlockManager();

};

#endif
