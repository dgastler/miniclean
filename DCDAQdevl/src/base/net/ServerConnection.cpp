#include <ServerConnection.h>

ServerConnection::~ServerConnection()
{
  Clear();
}

void ServerConnection::Clear()
{
  while(client.size() > 0)
    {
      DeleteConnection(client.size()-1);      
    }
  serverPort = 0;
  serverSocketFD = badFD;
  memset(&addrLocal,0,sizeof(struct sockaddr_in));
  memset(&addrListen,0,sizeof(struct sockaddr_in));
  listenAddress.clear();
  acceptList.clear();
}

bool ServerConnection::Setup(xmlNode * SetupNode)
{
  //================================
  //Parse data for server config and expected clients.
  //================================

  //Get server port.
  if(FindSubNode(SetupNode,"PORT") == NULL)
    {
      PrintError("No port specified\n");
      return(false);
    }
  else
    {
      uint32_t tempServerPort;
      GetXMLValue(SetupNode,"PORT","ServerConnection",tempServerPort);
      serverPort = tempServerPort;
    }


  //Get server listen address.
  if(FindSubNode(SetupNode,"LISTENADDRESS") == NULL)
    {
      PrintError("No listen address specified.  Using default\n");
    }
  else
    {
      GetXMLValue(SetupNode,"LISTENADDRESS","ServerConnection",listenAddress);
    }

    

  //Set the socket family
  addrListen.sin_family = AF_INET;
  //Set the port
  addrListen.sin_port = htons(serverPort);
  //Set the listen address mask
  if(listenAddress.empty())  //Default      
    inet_pton(AF_INET,
	      "0.0.0.0",
	      &(addrListen.sin_addr));
  else                       //Specified
    inet_pton(AF_INET,
	      listenAddress.c_str(),
	      &(addrListen.sin_addr));		 

  //Create server socket
  serverSocketFD = socket(AF_INET,SOCK_STREAM,0); 
  if(serverSocketFD < 0)
    {
      return(false);
    }
  //bind our socket to listen address and listen for connections
  bind(serverSocketFD,(sockaddr *) &addrListen,sizeof(addrListen)); 
  listen(serverSocketFD,14); //(#14 for linux)  P98 Unix Network Programming  
  return(true);
}
bool ServerConnection::ProcessConnection(xmlNode * managerNode)
{
  //================================
  //We should have a waiting connection, so we need to accept it
  //================================
  int fdRemoteSocket; //fd for incoming connection
  struct sockaddr_in addrRemote; //address info for incoming connection
  socklen_t addrRemoteLength = sizeof(addrRemote);
  std::string remoteIP;
  
  fdRemoteSocket = accept(serverSocketFD,
			  (sockaddr *) &addrRemote,
			  &addrRemoteLength);
  if(fdRemoteSocket == -1)
    {	  
      return(false);
    }
  //Check if this is in our netCclearList
  if(acceptList.size() > 0)
    {
      std::vector< uint32_t >::iterator it;
      for(it  = acceptList.begin();
	  it != acceptList.end();
	  it++)
	{
	  //	  if(AddrFullCompare(socketFamily,addrRemote,*it))
	  if(addrRemote.sin_addr.s_addr == *it)
	    break;
	}
      //if we made it to the iterator's end then we 
      //should block this connection.
      if(it == acceptList.end())
	{
	  close(fdRemoteSocket);
	}
      return(false);
    }
  
  //================================
  //Build a Connection object for this socket
  //================================

  //Fillin our connection settings.
  Connection * connection = new Connection;
  connection->fdSocket = fdRemoteSocket;
  connection->addrRemote = addrRemote;
  connection->port = serverPort;
  remoteIP.resize(INET_ADDRSTRLEN);

  inet_ntop(AF_INET,
	    &connection->addrRemote.sin_addr,
	    &remoteIP[0],
	    remoteIP.size());

  connection->remoteIP = remoteIP;
  //Setup the DCNetThread
  if(connection->SetupDCNetThread(managerNode))
    {
      //on sucess add this connection to our list
      client.push_back(connection);
    }
  else
    {
      //fail 
      delete connection;
      return(false);
    }	
  return(true);
}

void ServerConnection::DeleteConnection(size_t i)
{
  //Check that iConn is physical
  if(i >= client.size())
    return;

  char buffer[100];
  sprintf(buffer,"Deleting connection from: %s\n",client[i]->remoteIP.c_str());
  PrintError(buffer);
  
  //Send a shutdown packet
  int32_t iDCPacket;
  client[i]->thread->GetOutPacketManager()->GetFree(iDCPacket,true);
  DCNetPacket * packet = client[i]->thread->GetOutPacketManager()->GetPacket(iDCPacket);
  packet->SetType(DCNetPacket::SHUTDOWN);
  client[i]->thread->GetOutPacketManager()->AddFull(iDCPacket);
    
  //Shutdown the net thread and close the network socket
  client[i]->Shutdown();

  delete client[i];
  client.erase(client.begin()+i);
}
