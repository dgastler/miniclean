//*Be polite to others who have to work on this code; document your changes, keep conventions. Comments are good!*/
#ifndef __CLEANViewer__
#define __CLEANViewer__


#define PERM_FILE   (S_IRUSR | S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH| S_IWOTH)

#include <signal.h>
#include <ctime>
//ROOT GUI Libraries
//#include <TPolyLineShape.h>
#include <TGedEditor.h>
#include <TImage.h>
#include <TGClient.h>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TGTab.h>
#include <TGToolBar.h>
#include <TG3DLine.h>
#include <TPolyLine3D.h>
#include <TGMenu.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <TGComboBox.h>
#include <TGProgressBar.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TApplication.h>
#include <TQObject.h>
#include <RQ_OBJECT.h>
#include "TNamed.h"
#include "Riostream.h"
#include "TCanvas.h"
#include "TPolyLine.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TBranch.h"
#include "TTree.h"
#include "TTimer.h"
#include "TH1.h"
#include "THStack.h"
#include "TH2.h"
#include "TF1.h"
#include "TBox.h"
#include "TGDoubleSlider.h"
#include "TGDimension.h"
#include "TGSlider.h"
#include "TText.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TArc.h"
#include "TColor.h"
#include "TGText.h"
#include "TClassMenuItem.h"
#include "TSysEvtHandler.h"
#include "TGFileDialog.h"
#include "TPolyLine3D.h"
#include "TView.h"
#include "TGeoSphere.h"
#include "TGeoVolume.h"
#include "TGeoManager.h"
#include "TPolyMarker3D.h"
#include "TRotation.h"
#include "TGeoMatrix.h"
#include "TGeoXtru.h"
#include "TGeoCompositeShape.h"
#include "TGLViewer.h"
#include "TGLSAViewer.h"
#include "TViewerX3D.h"
#include "LocalEventFilter.h"
#include "TMacro.h"
//#include "TEveManager.h"
//#include "TEveTriangleSet.h"

//C++ Libraries
#include <iostream>
#include <string>
#include <algorithm>
#include <math.h>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <map>

//RAT Libraries

#include "RAT/DS/Root.hh"
#include "RAT/DSReader.hh"
#include "RAT/DS/EV.hh"
#include "RAT/DS/Board.hh"
#include "RAT/DS/Channel.hh"
#include "RAT/DS/RawBlock.hh"
#include "RAT/DS/ReductionLevel.hh"

#include "RAT/DB.hh"
#include "RAT/DetectorConfig.hh"
#include "RAT/MCcassetteInfo.hh"
#include "RAT/OutNetProc.hh"

//Waveform header file
#include "Waveform.h"
//zmq for socket
#include <zmq.hpp>
//#include <zmq.h>



//#include "TPoints3D.h"



#include "RAT/DS/DCDAQ_DQMStructs.hh"
#include "RAT/DS/DCDAQ_MemBlocks.hh"
#include "RAT/DS/DCDAQ_EventStreamHelper.hh"
#include "RAT/DS/DCDAQ_DCDAQEvent.hh"
#include "RAT/DS/DCDAQ_RAWDataStructs.hh"
#include "RAT/DS/DCDAQ_timeHelper.hh"

//compression
#include <zlib.h>

class CLEANViewer : public TGMainFrame {

 private:

  CLEANViewer();


   //Menu and ToolBar associated pointers
  TGMenuBar               *guiMenu;
  TGPopupMenu             *popupFile, *popupSave, *popupView,
                          *popupRAT, *popupEV,
                          *popupHelp, *popupWFD,
                          *popupGridLines,*popupOrdering,
                          *popupLabeling, *popupUnsnap,
                          *popupDQM, *popupBuffer, 
    *popupFilters, * popupRemoveFilters,
    *popup3D, *popup3DSize, * popup3DColor;
  std::vector<
    TGPopupMenu*>         wfdMenuVector;
  TGToolBar               *guiToolBar;
  TGNumberEntry           *bufferSizeNumberEntry;
  TGGroupFrame            *bufferGroup;
  TGVerticalFrame         *Frames3D;
  TGGroupFrame            *StreamFrame3D,*PMTSelect3D,*ScaleSelectGroup;
  TGComboBox              *ColorScaleSelectBox,*SizeScaleSelectBox;
  TGLabel                 * colorUnitsText,*sizeUnitsText;
  TGHorizontalFrame       *colorScaleHoriz,*sizeScaleHoriz;
  TGNumberEntry           *pmtSelectEntry3D,*colorScaleMaxEntry,*sizeScaleMaxEntry;
  TGCheckButton           *highLightPMTButton;
  TGPictureButton          *StopButton3D,*StartButton3D;
  //Horizontal Parent Frame for Tabs and group frames
  TGHorizontalFrame       *guiHorizontal;

  //Waveform and Display Tabs associated pointers
  TGVerticalFrame         *tabVertical;
  TGTab                   *guiTab,*selectionTab;
  TGCompositeFrame        *tabRawWave, *tabCalWave, 
                          *tabEventDis, *tabHistogram,
    *tabBoard,*tabPMT;
  TRootEmbeddedCanvas     *rawECanvas, *calECanvas, 
    *displayECanvas, *histogramECanvas,
    displayTabECanvas;
  TPad                    *displayTPad;
    // TView                   *displayTView;
  TCanvas                 *rawTCanvas, *histogramTCanvas,
    *calibTCanvas,*displayTCanvas;

  TGLSAViewer        *v;
  //TCanvas                 *displayTCanvas;

  TText                   *yLabel,*yLabelCalib;
  TGGroupFrame            *zoomGroup;
  TGHorizontalFrame       *zoomButtonFrame;
  TGDoubleHSlider         *zoomSlider;
  TGTextButton            *fullWaveformButton, *allPMTs, 
                          *noPMTs;
  
  TPaveText                   *InfoTopText, *InfoMidText;
  TGCheckButton           *summedWaveformButton;
  TGCheckButton           *gridLinesCheck, *commonScaleCheck,
                          *cursorSelectCheckButton;
  TGRadioButton           *orderByBoardButton,*orderByPMTButton,
                          *labelByBoardButton,*labelByPMTButton;
  TGButtonGroup           *orderButtonGroup,*labelButtonGroup;

  //Group Frames for Ev, PMT, ordering, and event selection
  TGVerticalFrame         *groupVertical,*orderingVertical;
  TGGroupFrame            *eventGroup,*orderingGroup,*groupRadio,*eventGroup3D;
  TGCanvas                *pmtGroupCanvas,*pmtOrderGroupCanvas;
  TGCompositeFrame        *pmtGroup,*pmtOrderGroup;
  TGGroupFrame             *pmtSelect;
  //TGContainer              *pmtContainer;
 
  //TGContainer              *pmtGroup;
  //TGViewport               *pmtGroupViewport;

  //associated pointers for EventSelection
  TGHorizontalFrame       *eventHorizontal, *eventHorizontal3D;
  TGNumberEntry           *eventNumberEntry,*eventNumberEntry3D;
  TGHProgressBar          *eventProgress;
  TGTextButton            *nextEvent, *prevEvent,*nextEvent3D, *prevEvent3D;
  
  //For filter window
  TGMainFrame * winFilter;// *winSleep;
  TGNumberEntry *zmqPeriodNumberEntry;
  TGVerticalFrame * winFilterFrame;
  TGGroupFrame * zmqPeriodGroup, *zmqReductionLevelGroup, *zmqHardwareTrigGroup;
  TGVerticalFrame *zmqPeriodFrame;
  TGHorizontalFrame * zmqReductionLevelFrame,*zmqHardwareTrigFrame;
  TGTextButton *sendButton, *deleteButton;
  TGVerticalFrame *zmqReductionLabelFrame,*zmqReductionCheckFrame,*zmqHardwareCheckFrame,*zmqHardwareLabelFrame;
  vector<TGLabel *> vectorReductionLabels,vectorHardwareLabels;
  vector<TGCheckButton *> vectorReductionChecks,vectorHardwareChecks;

  TGMainFrame * winFilterLocal;
  TGNumberEntry * LocalChargeEntry;
  TGVerticalFrame * winFilterFrameLocal;
  TGGroupFrame  *LocalReductionLevelGroup, *LocalHardwareTrigGroup, *LocalChargeGroup;
  TGVerticalFrame * LocalChargeFrame;
  TGHorizontalFrame * LocalReductionLevelFrame,*LocalHardwareTrigFrame;
  TGTextButton *LocalsendButton, *LocaldeleteButton;
  TGVerticalFrame *LocalReductionLabelFrame,*LocalReductionCheckFrame,*LocalHardwareCheckFrame,*LocalHardwareLabelFrame;
  vector<TGLabel *> LocalvectorReductionLabels,LocalvectorHardwareLabels;
  vector<TGCheckButton *> LocalvectorReductionChecks,LocalvectorHardwareChecks;

  LocalEventFilter * LocalFilter;
 
  std::vector<StreamFilter *> zmqEventFilterVec;
  std::vector<std::string> filterNames;
  int32_t filterNumSelected;
  TGMainFrame * winHist;
  TGCompositeFrame * frameHist;
  TRootEmbeddedCanvas * histECanWin;


  
  TGMainFrame * winRaw;
  TGCompositeFrame * tabRawWin;
  TRootEmbeddedCanvas * rawECanWin;
  
  TGMainFrame * winCalib;
  TGCompositeFrame * tabCalibWin;
  TRootEmbeddedCanvas * calibECanWin;
  
  TGMainFrame* ColorScaleWin;
  TGCompositeFrame * colorScaleFrame;
  TRootEmbeddedCanvas * colorScaleECan;
  
  TCanvas * colorScaleCanvas;

  //  TGMainFrame * InfoWindow;
  TGCompositeFrame* InfoFrame;
  TRootEmbeddedCanvas * InfoECanvas;
  TCanvas * InfoCanvas;
  TPad * InfoTextPad, *InfoPlotPad;
  //PMT selection pointers (frames and checkboxes) and variables
  std::vector<
    TGCheckButton*>       pmtOrderCheckVector;
  std::vector<
    TGHorizontalFrame*>   pmtFrameVector,pmtOrderFrameVector;
  std::vector<
    TGGroupFrame*>     boardGroupFrameVector;
  std::vector<TGButtonGroup*>     pmtGroupVector,pmtOrderGroupVector,
                                  wfdGroupVector;
  std::vector<int>         NChannelsClickedOnBoardVector;
  


  struct pmtCheckInfo{
    TGCheckButton* CheckButton;
    TGCheckButton* CheckOrderButton;
    int Board;
    int  Channel;
    int PMTID;
    //bool isDown;
  };

  std::vector<pmtCheckInfo>  pmtCheckInfoVector;

  map<int,int> pmtCheckIDMap;
  map<int,int> idGroupVectorMap;
  map<int,int> idOrderGroupVectorMap;
  
  std::vector<int> pmtIDsVector;

  std::vector<std::vector<int> > idsOnBoardVector;
  //map<int,int> pmtOrderCheckIDMap;
  //Layout Hints pointers
  TGLayoutHints           *horizontalLayout, *tabLayout, *groupLayout;

  //Gloabal RAT Pointers and variables
  RAT::DSReader           *ratReader;
  RAT::DS::Root           *ratDS;
  RAT::DS::EV             *ratEV;
   std::vector<RAT::DS::EV> ratEVBufferVector;
  RAT::OutNetProc          *ratSend;
  std::vector<fpos_t> memBlockStartVec;

  RAT::DetectorConfig     *ratDC;

  size_t bufferSize;
  int bufferEvtNumber;
  int bufferMemNumber;
  TFileHandler * handler,*namehandler, * zmqhandler;
  int inFD;
  int outFD;
  int inotifyFD;
  int inotifyWD;
  int zmqFD;
  zmq::socket_t* daqSocketRequest;
  zmq::socket_t * daqSocketData;
  zmq::context_t * context;
  int zmqPort;
  FILE * InFile;


  MemoryBlock * cBlock;
  MemoryBlock * dBlock;

  uint32_t sizeBlockRead;

  int selectPMT;
  Long64_t                currentEvent;
  Long64_t                firstEvent;
  Long64_t                nEvents;
  Long64_t                nEV;
  Long64_t                nBoards;
  Long64_t                nChannels;
  Long64_t                nPMTs;
  unsigned                reductionLevelTrue;
  unsigned                reductionLevel; 
  std::vector<double>     SummedChargeVector;
  uint32_t                runNumber;
  



  //PolyLine and Histogram vectors and variables
  //Waveform                *SumCalibWaveform;
  std::vector <Waveform>  WaveformVector;
  std::vector<TPolyLine*> PolyLineVector;
  std::vector<TPolyLine*> PolyLineCalibVector;
  std::vector<TPolyLine3D*> CassettePolyLines;
  std::vector <TGaxis*>   AxisVector;
  std::vector<TGaxis*>    AxisVectorCalib;
  std::vector<TText*>     PmtLabelVector;

  TGeoSphere * sphereShape;
  TGeoVolume * sphereVol;
  TGeoManager * man;
  int sizeScaleState;
  int colorScaleState;
  double colorScaleMax,sizeScaleMax;
  struct PlotIDs{
    Int_t    ChannelID;
    Int_t    WFDID;
    Int_t    InputID;};

  std::vector<PlotIDs> PlotIDsVector;


  std::vector<TArc*> ChannelDisplayVector;
  std::vector<double> ChannelLocsX;
  std::vector<double> ChannelLocsY;

  std::vector<double> ChannelLocsXCentered;
  std::vector<double> ChannelLocsYCentered;
  std::vector<float> xCas,yCas,zCas;


  std::vector<RAT::DS::Board> OrderedBoards;
 
 
  map<string,int> InputMap;
  map<int,int> ChannelIDOrderMap;
  int PMTIDStart;
 
  int ColI;

  vector<int> col;
  UInt_t cursorOverPMT;

  TText                   *chargeValueText;

  TH1F                    *PromptHist,*PromptCalibHist;
  TH1F                    *LateHist,*LateCalibHist;
  TH1F                    *PromptChannelsHist,*PromptCalibChannelsHist;
  TH1F                    *LateChannelsHist,*LateCalibChannelsHist,*miniPromptHist,*miniLateHist;
 


  TGaxis                  *miniPromptAxis,*miniLateAxis;
  UInt_t                  activePlots;
  UInt_t                  whichPlot;
  int                     canvasMinX, canvasMaxX, canvasMinY, canvasMaxY;
  double                  canvasMinYCalib, canvasMaxYCalib;

  Bool_t                  zoomAdjusted;
  Bool_t                  gridLinesX,gridLinesY;
  Bool_t                  DEBUG;
  int                     AVG;
  int                     timeOutFactor;
  Bool_t                  ZMQ;
  Bool_t                  drawFill;
  Bool_t                  drawPromptTotal;
  Bool_t                  calibrated;
  Bool_t                  commonScale;
  Bool_t                  drawSeperator;
  Bool_t                  AxisSelecting;
  Bool_t                  orderByBoard;
  Bool_t                  labelByBoard;
  Bool_t                  SumIncluded;
  Bool_t                  AutoScaling;
  Bool_t                  AvgDraw;
  Bool_t                  FirstRead;
  Bool_t                  DAQdat;
  Bool_t                  RATServerMode;
  Bool_t                  Draw3D;
  bool                    SupressEmpty;
   int                       currentTabnumber;

  TStyle                  *drawStyle;
  
  double                  padXmin;
  double                  padXmax;
  double                  padYmin;
  double                  padYmax;
  double                  padXcentre;

  int                     selectXStart;
  TLine                   *selectStartLine,*selectEndLine;
  TGaxis                  *miniyAxis, * minixAxis;

  //Save Canvas Dialog Window
  TGTransientFrame        *canvasSave;
  TGVerticalFrame         *saveVertical;
  TGHorizontalFrame       *nameHorizontal, *directoryHorizontal,
                          *buttonHorizontal;
  TGTextEntry             *nameTextEntry, *directoryTextEntry;
  TGComboBox              *extComboEntry;
  TGLabel                 *nameLabel, *extLabel, *directoryLabel;
  TGTextButton            *saveButton, *cancelButton;
  std::string             saveCanvasName, 
                          saveCanvasExt,
                          saveCanvasDirectory;
  
  //other
  char                    *dataFile;

  uint32_t * DAQBufferPtr;
  
 public:




  /*CLEANViewer.cpp*/
  //MyMainFrame constructor and destructor
  CLEANViewer (const TGWindow *p, UInt_t w, UInt_t h, const char * _DataFile, Bool_t _DebugSwitch, UInt_t _AvgValue, Bool_t _ZMQSwitch,UInt_t _Period,vector<unsigned> _ReducVect,std::string _RATServer,Bool_t Enable3DMode,size_t BufferSize);
  
  virtual ~CLEANViewer();

 private:
  /*CLEANViewer_GUISetup.cpp*/
  //functions for initial setup gui layout
  void SetMenu();
  void SetToolBar();
  void FillPmtVector();
  void SetTabs();
  void SetGroupFrames();

  static bool orderbyIDs(const pmtCheckInfo &a,const pmtCheckInfo &b);
  static bool orderbyBoards(const pmtCheckInfo &a,const pmtCheckInfo &b);

  /*CLEANViewer_Signals.cpp*/
  //function for processing messages
 public:
  Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
  void NextEventPressed();
  void PrevEventPressed();
  void EventChange();
  void EventChange3D();
  void highLight();
  void pmtSelectChanged3D();
  void ColorScaleChange();
  void ColorScaleSelected(Int_t);
  void SizeScaleChange();
  void SizeScaleSelected(Int_t);
  void PlotChannel();
  void XAxisChange();
  void ZoomFull();
  void CheckAllPMTs();
  void UncheckAllPMTs();
  void FileMenu(Int_t id);
  void ResetFile();
  void ViewMenu(Int_t id);
  void RATMenu(Int_t id);
  void EVMenu(Int_t id);
  void WFDMenu(Int_t id);
  void PlotMenu(Int_t id);
  void DQMMenu(Int_t id);
  void ThreeDMenu(Int_t id);
  void StreamGo();
  void StreamStop();
  void SetNewAlarm();
  //void SleepWindow();
  void AddNewFilter();
  void RemoveFilter(int32_t filterNum);
  void FilterWindow();
  void LocalFilterWindow();
  void SendFilterRequest(int32_t filterNum);
  void SetFilter(int32_t filterNum);
  void SendFilterFromWindow();
  void SetLocalFilter();
  void SaveCanvas(char * path, int id);
  void pmtOrderButtonClicked(Int_t id);
  void pmtButtonClicked(Int_t id);
  void PMTClicked(Int_t event,Int_t x,Int_t y,TObject* select);
  
  void MouseOver3DViewer(TObject* obj,UInt_t state);

// void PMTDisplayClicked(Int_t event,Int_t x,Int_t y,TObject* select);
  void wfdButtonClicked(Int_t id);
  void RawCanvasClicked(Int_t event,Int_t x,Int_t y,TObject* select);
  void CalibCanvasClicked(Int_t event,Int_t x,Int_t y,TObject* select);
  void cursorSelectChecked();
  void RawWindowClosed();
  void CalibWindowClosed();
  void Viewer3DClosed();
  void HistWindowClosed();
  void ColorScaleClosed();
  //void InfoWindowClosed();
  // void DisplayWindowClosed();
  //void SleepWindowClosed();
  void FilterWindowClosed();
  void LocalFilterWindowClosed();
  void ZMQReadNewEvent();
  
  static bool OrderbyChannel(const PlotIDs &a,const PlotIDs &b);
  static bool OrderbyBoard(const PlotIDs &a,const PlotIDs &b);
  static bool OppOrderbyChannel(const PlotIDs &a,const PlotIDs &b);
  static bool OppOrderbyBoard(const PlotIDs &a,const PlotIDs &b);
  static bool OrderButtonsbyBoard(const pmtCheckInfo &a,const pmtCheckInfo &b);
  //Exit Functions
  void CloseWindow();
  void Exit();
  //Must be public because called by sig handler
 

 private:
  /*CLEANViewer_RAT.cpp*/
  //functions for opening RAT data file
  void LoadFile(const char * DataFile);
  void LoadFileDAQ(const char * DataFile);
  Bool_t LoadShiftModeZMQ(const char * daqZMQName);
  int NumberEV(Bool_t Prev,bool first=false);
  void EventCheck(Bool_t Prev);
  void LoadChannels();

  uint32_t * ReadPastRunHeader(uint32_t * bufferPtr);
  void ReadMemBlock(MemoryBlock * mBlock);
  void DecompressMemBlock(MemoryBlock * cBlock,MemoryBlock * dBlock);

 /*CLEANViewer_RAT.cpp*/
//builds Maps from PMTIDs to Bords and Channels
  void BuildMaps();
  void SumWaveforms();
  void CheckForEntries();
  /*CLEANViewer_RAT.cpp*/
  //sets channel location arrays
  void SetChannelLocations();
  void CreateCenteredArrays(int centeredPMT);

  typedef int32_t (*StreamBufftoEV_t)(uint8_t * data,RAT::DS::EV * ev);
  StreamBufftoEV_t StreamBufftoEV_func;

  /*CLEANViewer_Draw.cpp*/
  //fucntions for drawing histograms and waveforms and event display
  void DrawHistogram();
  void DrawPolyLine();
  void PolyLineZoomAdjust();
  void DrawPromptTotalHist(Int_t activeChannels);
  void DrawHitMap();
  void DrawEventDisplay();
  void DrawEventDisplay3D();
  void DrawEventDisplay(int iPMT);
  void DrawColorScale(int numCols);
  void DrawMiniWaveform(int pmtID);
  void DrawCentered(TObject *pmt);
  void CreateInfoWindow();
  //void Draw3D();

 ssize_t writen(int fd, const void *vptr, size_t n)
  {
    size_t nleft;
    ssize_t nwritten;
    const char * ptr;

    ptr=(char*)vptr;
    nleft=n;
    while(nleft>0)
      {
	if((nwritten=write(fd,ptr,nleft))<=0)
	  {
	    if(errno==EINTR)
	      nwritten=0;
	    else
	      return(-1);
	  }
	nleft-=nwritten;
	ptr+=nwritten;
      }
    return(n);
  }

  ClassDef(CLEANViewer, 0)
    };


#endif
