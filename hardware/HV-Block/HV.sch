EESchema Schematic File Version 2  date Thu 12 Sep 2013 05:09:26 PM EDT
LIBS:mcx
LIBS:conn
LIBS:power
LIBS:pspice
LIBS:device
LIBS:lemo
LIBS:HV-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "12 sep 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 9750 4550
Wire Wire Line
	9500 4550 9750 4550
Wire Wire Line
	9750 5300 9750 5350
Wire Wire Line
	9300 5900 9300 5950
Wire Wire Line
	9750 4800 9750 4250
Wire Wire Line
	9750 4250 10300 4250
Wire Wire Line
	9400 4800 5950 4800
Wire Wire Line
	10700 5700 10700 7150
Wire Wire Line
	10700 7150 9500 7150
Wire Wire Line
	9500 7150 9500 5050
Connection ~ 10500 6000
Wire Wire Line
	10500 5700 10500 6550
Wire Wire Line
	10300 6450 10300 6500
Wire Wire Line
	10300 6050 10300 5700
Wire Wire Line
	11050 5900 11050 5850
Wire Wire Line
	10800 5700 10800 5800
Wire Wire Line
	10400 5700 10400 5800
Wire Wire Line
	11100 4900 11100 5000
Connection ~ 6300 5400
Wire Wire Line
	6300 5500 6300 5400
Wire Wire Line
	6300 5950 6300 5900
Connection ~ 11050 5850
Wire Wire Line
	5950 4800 5950 5400
Wire Wire Line
	5950 5400 6700 5400
Wire Wire Line
	7950 3200 8400 3200
Wire Wire Line
	1400 3150 1850 3150
Wire Wire Line
	4600 1300 5050 1300
Wire Wire Line
	4600 3400 4600 3500
Wire Wire Line
	4600 4400 4600 4500
Wire Wire Line
	4600 4200 4900 4200
Wire Wire Line
	7950 3400 7950 3500
Wire Wire Line
	7950 4400 7950 4500
Wire Wire Line
	7950 4200 8250 4200
Wire Wire Line
	1400 5300 1400 5400
Wire Wire Line
	1400 6300 1400 6400
Wire Wire Line
	1400 6100 1700 6100
Wire Wire Line
	1400 3350 1400 3450
Wire Wire Line
	1400 4350 1400 4450
Wire Wire Line
	1400 4150 1700 4150
Wire Wire Line
	1400 1500 1400 1600
Wire Wire Line
	1400 2500 1400 2600
Wire Wire Line
	1400 2300 1700 2300
Wire Wire Line
	7950 1500 7950 1600
Wire Wire Line
	7950 2500 7950 2600
Wire Wire Line
	7950 2300 8250 2300
Wire Wire Line
	4600 1500 4600 1600
Wire Wire Line
	4600 2500 4600 2600
Wire Wire Line
	4600 2300 4900 2300
Wire Wire Line
	9150 700  8900 700 
Wire Wire Line
	6750 6200 6300 6200
Wire Wire Line
	7200 3350 7200 3400
Wire Wire Line
	10500 3350 10500 3400
Wire Wire Line
	3950 5250 3950 5300
Wire Wire Line
	3950 3300 3950 3350
Wire Wire Line
	3950 1450 3950 1500
Wire Wire Line
	10400 1450 10400 1500
Wire Wire Line
	7150 1450 7150 1500
Wire Wire Line
	8700 6350 8700 6400
Wire Wire Line
	5200 4100 5200 4150
Wire Wire Line
	4900 3700 5200 3700
Wire Wire Line
	8550 4100 8550 4150
Wire Wire Line
	8250 3700 8550 3700
Wire Wire Line
	2000 6000 2000 6050
Wire Wire Line
	1700 5600 2000 5600
Wire Wire Line
	2000 4050 2000 4100
Wire Wire Line
	1700 3650 2000 3650
Wire Wire Line
	2000 2200 2000 2250
Wire Wire Line
	1700 1800 2000 1800
Wire Wire Line
	8550 2200 8550 2250
Wire Wire Line
	8250 1800 8550 1800
Wire Wire Line
	5200 2200 5200 2250
Wire Wire Line
	4900 1800 5200 1800
Wire Wire Line
	6600 6700 6900 6700
Wire Wire Line
	6900 7100 6900 7150
Connection ~ 6000 1300
Wire Wire Line
	5550 700  5800 700 
Wire Wire Line
	5550 700  5550 900 
Wire Wire Line
	6000 1800 6000 1700
Wire Wire Line
	6000 1800 5800 1800
Wire Wire Line
	6300 2100 6300 2150
Wire Wire Line
	6000 1700 6300 1700
Wire Wire Line
	5800 700  5800 900 
Connection ~ 5800 1300
Wire Wire Line
	6850 1300 5450 1300
Connection ~ 4900 1300
Connection ~ 8250 1300
Wire Wire Line
	10100 1300 8800 1300
Connection ~ 9150 1300
Wire Wire Line
	9150 700  9150 900 
Wire Wire Line
	9650 1700 9350 1700
Wire Wire Line
	9650 2100 9650 2150
Wire Wire Line
	9150 1800 9350 1800
Wire Wire Line
	9350 1800 9350 1700
Wire Wire Line
	8900 700  8900 900 
Connection ~ 9350 1300
Connection ~ 2800 3150
Wire Wire Line
	2350 2550 2600 2550
Wire Wire Line
	2350 2550 2350 2750
Wire Wire Line
	2800 3650 2800 3550
Wire Wire Line
	2800 3650 2600 3650
Wire Wire Line
	3100 3950 3100 4000
Wire Wire Line
	2800 3550 3100 3550
Wire Wire Line
	2600 2550 2600 2750
Connection ~ 2600 3150
Wire Wire Line
	3650 3150 2250 3150
Connection ~ 1700 3150
Connection ~ 1700 1300
Wire Wire Line
	3650 1300 2250 1300
Connection ~ 2600 1300
Wire Wire Line
	2600 900  2600 700 
Wire Wire Line
	3100 1700 2800 1700
Wire Wire Line
	3100 2100 3100 2150
Wire Wire Line
	2600 1800 2800 1800
Wire Wire Line
	2800 1800 2800 1700
Wire Wire Line
	2350 900  2350 700 
Wire Wire Line
	2350 700  2600 700 
Connection ~ 2800 1300
Connection ~ 1700 5100
Wire Wire Line
	3650 5100 2250 5100
Connection ~ 2600 5100
Wire Wire Line
	2600 4700 2600 4500
Wire Wire Line
	3100 5500 2800 5500
Wire Wire Line
	3100 5900 3100 5950
Wire Wire Line
	2600 5600 2800 5600
Wire Wire Line
	2800 5600 2800 5500
Wire Wire Line
	2350 4700 2350 4500
Wire Wire Line
	2350 4500 2600 4500
Connection ~ 2800 5100
Connection ~ 9350 3200
Wire Wire Line
	8900 2600 9150 2600
Wire Wire Line
	8900 2600 8900 2800
Wire Wire Line
	9350 3700 9350 3600
Wire Wire Line
	9350 3700 9150 3700
Wire Wire Line
	9650 4000 9650 4050
Wire Wire Line
	9350 3600 9650 3600
Wire Wire Line
	9150 2600 9150 2800
Connection ~ 9150 3200
Wire Wire Line
	10200 3200 8800 3200
Connection ~ 8250 3200
Connection ~ 4900 3200
Wire Wire Line
	6900 3200 5450 3200
Connection ~ 5800 3200
Wire Wire Line
	5800 2800 5800 2600
Wire Wire Line
	6300 3600 6000 3600
Wire Wire Line
	6300 4000 6300 4050
Wire Wire Line
	5800 3700 6000 3700
Wire Wire Line
	6000 3700 6000 3600
Wire Wire Line
	5550 2800 5550 2600
Wire Wire Line
	5550 2600 5800 2600
Connection ~ 6000 3200
Wire Wire Line
	7700 6700 7700 6600
Wire Wire Line
	7700 6700 7500 6700
Wire Wire Line
	8000 7000 8000 7050
Wire Wire Line
	7700 6600 8000 6600
Connection ~ 7700 6200
Wire Wire Line
	7250 5800 7250 5600
Wire Wire Line
	7250 5600 7500 5600
Wire Wire Line
	7500 5600 7500 5800
Wire Wire Line
	7900 5250 7900 5200
Connection ~ 7900 5650
Connection ~ 7500 6200
Connection ~ 6600 6200
Wire Wire Line
	7900 5200 7700 5200
Wire Wire Line
	7300 5200 7000 5200
Wire Wire Line
	7000 5200 7000 5000
Wire Wire Line
	6250 5000 6500 5000
Wire Wire Line
	7000 5600 7000 5700
Connection ~ 3450 3150
Connection ~ 8300 6200
Connection ~ 7700 6600
Connection ~ 7900 5200
Connection ~ 7000 5200
Connection ~ 6000 1700
Connection ~ 6650 1300
Connection ~ 9950 1300
Connection ~ 3450 5100
Connection ~ 2800 5500
Connection ~ 2800 3550
Connection ~ 3450 1300
Connection ~ 2800 1700
Connection ~ 9350 3600
Connection ~ 10000 3200
Connection ~ 6000 3600
Connection ~ 6650 3200
Wire Wire Line
	6300 6400 6300 6500
Wire Wire Line
	6300 7400 6300 7500
Wire Wire Line
	6300 7200 6600 7200
Wire Wire Line
	7950 1300 8400 1300
Wire Wire Line
	1400 1300 1850 1300
Wire Wire Line
	1400 5100 1850 5100
Wire Wire Line
	4600 3200 5050 3200
Wire Wire Line
	8400 5200 8500 5200
Wire Wire Line
	5950 5900 5950 5950
Wire Wire Line
	10200 5700 10200 5800
Wire Wire Line
	10600 5700 10600 5800
Wire Wire Line
	10900 5700 10900 5850
Wire Wire Line
	10900 5850 11050 5850
Wire Wire Line
	10200 6000 10300 6000
Connection ~ 10300 6000
Wire Wire Line
	10400 6000 10500 6000
Wire Wire Line
	10500 6950 10500 7050
Wire Wire Line
	10600 4400 10600 4450
Wire Wire Line
	9300 5400 9500 5400
Connection ~ 9500 5400
Connection ~ 7900 6200
Wire Wire Line
	8400 6200 7150 6200
$Comp
L JUMPER JP2
U 1 1 511AAC6D
P 7900 5900
F 0 "JP2" H 7900 6050 60  0000 C CNN
F 1 "JUMPER" H 7900 5820 40  0000 C CNN
	1    7900 5900
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 511AAB91
P 9750 5350
F 0 "#PWR01" H 9750 5350 30  0001 C CNN
F 1 "GND" H 9750 5280 30  0001 C CNN
	1    9750 5350
	1    0    0    -1  
$EndComp
$Comp
L R R_G2
U 1 1 511AAB90
P 9750 5050
F 0 "R_G2" V 9850 5050 50  0000 C CNN
F 1 "50" V 9750 5050 50  0000 C CNN
	1    9750 5050
	1    0    0    -1  
$EndComp
$Comp
L R R_G3
U 1 1 511AAB86
P 9300 5650
F 0 "R_G3" V 9400 5650 50  0000 C CNN
F 1 "100" V 9300 5650 50  0000 C CNN
	1    9300 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 511AAB85
P 9300 5950
F 0 "#PWR02" H 9300 5950 30  0001 C CNN
F 1 "GND" H 9300 5880 30  0001 C CNN
	1    9300 5950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 511AAB19
P 10600 4450
F 0 "#PWR03" H 10600 4450 30  0001 C CNN
F 1 "GND" H 10600 4380 30  0001 C CNN
	1    10600 4450
	1    0    0    -1  
$EndComp
$Comp
L LEMO CONN1
U 1 1 511AAAE0
P 10450 4250
F 0 "CONN1" H 10500 4100 60  0000 C CNN
F 1 "LEMO" H 10450 4350 60  0000 C CNN
	1    10450 4250
	-1   0    0    -1  
$EndComp
$Comp
L JUMPER3 JP1
U 1 1 511AA991
P 9500 4800
F 0 "JP1" H 9550 4700 40  0000 L CNN
F 1 "JUMPER3" H 9500 4900 40  0000 C CNN
	1    9500 4800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR04
U 1 1 51196F65
P 11100 5000
F 0 "#PWR04" H 11100 5000 30  0001 C CNN
F 1 "GND" H 11100 4930 30  0001 C CNN
	1    11100 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 51196F03
P 10800 5800
F 0 "#PWR05" H 10800 5800 30  0001 C CNN
F 1 "GND" H 10800 5730 30  0001 C CNN
	1    10800 5800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 51196EFE
P 10600 5800
F 0 "#PWR06" H 10600 5800 30  0001 C CNN
F 1 "GND" H 10600 5730 30  0001 C CNN
	1    10600 5800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 51196EE7
P 10400 5800
F 0 "#PWR07" H 10400 5800 30  0001 C CNN
F 1 "GND" H 10400 5730 30  0001 C CNB
	1    10400 5800
	1    0    0    -1  
$EndComp
$Comp
L RJ45 J1
U 1 1 5119694C
P 10550 5250
F 0 "J1" H 10750 5750 60  0000 C CNN
F 1 "RJ45" H 10400 5750 60  0000 C CNN
	1    10550 5250
	1    0    0    -1  
$EndComp
$Comp
L ZENER GDPN1
U 1 1 51196322
P 6300 5700
F 0 "GDPN1" H 6300 5800 50  0000 C CNN
F 1 "ZENER" H 6300 5600 40  0000 C CNN
	1    6300 5700
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR08
U 1 1 5112F95A
P 5950 5950
F 0 "#PWR08" H 5950 5950 30  0001 C CNN
F 1 "GND" H 5950 5880 30  0001 C CNN
	1    5950 5950
	1    0    0    -1  
$EndComp
$Comp
L R R_G1
U 1 1 5112F93B
P 5950 5650
F 0 "R_G1" V 6050 5650 50  0000 C CNN
F 1 "1k" V 5950 5650 50  0000 C CNN
	1    5950 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5112F885
P 6300 5950
F 0 "#PWR09" H 6300 5950 30  0001 C CNN
F 1 "GND" H 6300 5880 30  0001 C CNN
	1    6300 5950
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 PMT5
U 1 1 50C0F132
P 4250 3300
F 0 "PMT5" V 4200 3300 40  0000 C CNN
F 1 "CONN_2" V 4300 3300 40  0000 C CNN
	1    4250 3300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 50C0F131
P 4600 3500
F 0 "#PWR010" H 4600 3500 30  0001 C CNN
F 1 "GND" H 4600 3430 30  0001 C CNN
	1    4600 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 50C0F130
P 4600 4500
F 0 "#PWR011" H 4600 4500 30  0001 C CNN
F 1 "GND" H 4600 4430 30  0001 C CNN
	1    4600 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV5
U 1 1 50C0F12F
P 4250 4300
F 0 "HV5" V 4200 4300 40  0000 C CNN
F 1 "CONN_2" V 4300 4300 40  0000 C CNN
	1    4250 4300
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 PMT6
U 1 1 50C0F12E
P 7600 3300
F 0 "PMT6" V 7550 3300 40  0000 C CNN
F 1 "CONN_2" V 7650 3300 40  0000 C CNN
	1    7600 3300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 50C0F12D
P 7950 3500
F 0 "#PWR012" H 7950 3500 30  0001 C CNN
F 1 "GND" H 7950 3430 30  0001 C CNN
	1    7950 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 50C0F12C
P 7950 4500
F 0 "#PWR013" H 7950 4500 30  0001 C CNN
F 1 "GND" H 7950 4430 30  0001 C CNN
	1    7950 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV6
U 1 1 50C0F12B
P 7600 4300
F 0 "HV6" V 7550 4300 40  0000 C CNN
F 1 "CONN_2" V 7650 4300 40  0000 C CNN
	1    7600 4300
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 PMT7
U 1 1 50C0F128
P 1050 5200
F 0 "PMT7" V 1000 5200 40  0000 C CNN
F 1 "CONN_2" V 1100 5200 40  0000 C CNN
	1    1050 5200
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 50C0F127
P 1400 5400
F 0 "#PWR014" H 1400 5400 30  0001 C CNN
F 1 "GND" H 1400 5330 30  0001 C CNN
	1    1400 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 50C0F126
P 1400 6400
F 0 "#PWR015" H 1400 6400 30  0001 C CNN
F 1 "GND" H 1400 6330 30  0001 C CNN
	1    1400 6400
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV7
U 1 1 50C0F125
P 1050 6200
F 0 "HV7" V 1000 6200 40  0000 C CNN
F 1 "CONN_2" V 1100 6200 40  0000 C CNN
	1    1050 6200
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 PMT4
U 1 1 50C0F123
P 1050 3250
F 0 "PMT4" V 1000 3250 40  0000 C CNN
F 1 "CONN_2" V 1100 3250 40  0000 C CNN
	1    1050 3250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 50C0F122
P 1400 3450
F 0 "#PWR016" H 1400 3450 30  0001 C CNN
F 1 "GND" H 1400 3380 30  0001 C CNN
	1    1400 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 50C0F121
P 1400 4450
F 0 "#PWR017" H 1400 4450 30  0001 C CNN
F 1 "GND" H 1400 4380 30  0001 C CNN
	1    1400 4450
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV4
U 1 1 50C0F120
P 1050 4250
F 0 "HV4" V 1000 4250 40  0000 C CNN
F 1 "CONN_2" V 1100 4250 40  0000 C CNN
	1    1050 4250
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 PMT1
U 1 1 50C0F11F
P 1050 1400
F 0 "PMT1" V 1000 1400 40  0000 C CNN
F 1 "CONN_2" V 1100 1400 40  0000 C CNN
	1    1050 1400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 50C0F11E
P 1400 1600
F 0 "#PWR018" H 1400 1600 30  0001 C CNN
F 1 "GND" H 1400 1530 30  0001 C CNN
	1    1400 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 50C0F11D
P 1400 2600
F 0 "#PWR019" H 1400 2600 30  0001 C CNN
F 1 "GND" H 1400 2530 30  0001 C CNN
	1    1400 2600
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV1
U 1 1 50C0F11C
P 1050 2400
F 0 "HV1" V 1000 2400 40  0000 C CNN
F 1 "CONN_2" V 1100 2400 40  0000 C CNN
	1    1050 2400
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 PMT3
U 1 1 50C0F116
P 7600 1400
F 0 "PMT3" V 7550 1400 40  0000 C CNN
F 1 "CONN_2" V 7650 1400 40  0000 C CNN
	1    7600 1400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 50C0F115
P 7950 1600
F 0 "#PWR020" H 7950 1600 30  0001 C CNN
F 1 "GND" H 7950 1530 30  0001 C CNN
	1    7950 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 50C0F114
P 7950 2600
F 0 "#PWR021" H 7950 2600 30  0001 C CNN
F 1 "GND" H 7950 2530 30  0001 C CNN
	1    7950 2600
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV3
U 1 1 50C0F113
P 7600 2400
F 0 "HV3" V 7550 2400 40  0000 C CNN
F 1 "CONN_2" V 7650 2400 40  0000 C CNN
	1    7600 2400
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 PMT2
U 1 1 50C0F111
P 4250 1400
F 0 "PMT2" V 4200 1400 40  0000 C CNN
F 1 "CONN_2" V 4300 1400 40  0000 C CNN
	1    4250 1400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 50C0F110
P 4600 1600
F 0 "#PWR022" H 4600 1600 30  0001 C CNN
F 1 "GND" H 4600 1530 30  0001 C CNN
	1    4600 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 50C0F10F
P 4600 2600
F 0 "#PWR023" H 4600 2600 30  0001 C CNN
F 1 "GND" H 4600 2530 30  0001 C CNN
	1    4600 2600
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 HV2
U 1 1 50C0F10E
P 4250 2400
F 0 "HV2" V 4200 2400 40  0000 C CNN
F 1 "CONN_2" V 4300 2400 40  0000 C CNN
	1    4250 2400
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 HV8
U 1 1 50C0F02F
P 5950 7300
F 0 "HV8" V 5900 7300 40  0000 C CNN
F 1 "CONN_2" V 6000 7300 40  0000 C CNN
	1    5950 7300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 50C0F02E
P 6300 7500
F 0 "#PWR024" H 6300 7500 30  0001 C CNN
F 1 "GND" H 6300 7430 30  0001 C CNN
	1    6300 7500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 50C0F004
P 6300 6500
F 0 "#PWR025" H 6300 6500 30  0001 C CNN
F 1 "GND" H 6300 6430 30  0001 C CNN
	1    6300 6500
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 PMT8
U 1 1 50C0EFC3
P 5950 6300
F 0 "PMT8" V 5900 6300 40  0000 C CNN
F 1 "CONN_2" V 6000 6300 40  0000 C CNN
	1    5950 6300
	-1   0    0    -1  
$EndComp
$Comp
L MCX WFD5
U 1 1 50C0EECD
P 7050 3200
F 0 "WFD5" H 7100 3050 60  0000 C CNN
F 1 "MCX" H 7050 3300 60  0000 C CNN
	1    7050 3200
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 50C0EECC
P 7200 3400
F 0 "#PWR026" H 7200 3400 30  0001 C CNN
F 1 "GND" H 7200 3330 30  0001 C CNN
	1    7200 3400
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD6
U 1 1 50C0EECA
P 10350 3200
F 0 "WFD6" H 10400 3050 60  0000 C CNN
F 1 "MCX" H 10350 3300 60  0000 C CNN
	1    10350 3200
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 50C0EEC9
P 10500 3400
F 0 "#PWR027" H 10500 3400 30  0001 C CNN
F 1 "GND" H 10500 3330 30  0001 C CNN
	1    10500 3400
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD7
U 1 1 50C0EEC4
P 3800 5100
F 0 "WFD7" H 3850 4950 60  0000 C CNN
F 1 "MCX" H 3800 5200 60  0000 C CNN
	1    3800 5100
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR028
U 1 1 50C0EEC3
P 3950 5300
F 0 "#PWR028" H 3950 5300 30  0001 C CNN
F 1 "GND" H 3950 5230 30  0001 C CNN
	1    3950 5300
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD4
U 1 1 50C0EEC1
P 3800 3150
F 0 "WFD4" H 3850 3000 60  0000 C CNN
F 1 "MCX" H 3800 3250 60  0000 C CNN
	1    3800 3150
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 50C0EEC0
P 3950 3350
F 0 "#PWR029" H 3950 3350 30  0001 C CNN
F 1 "GND" H 3950 3280 30  0001 C CNN
	1    3950 3350
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD1
U 1 1 50C0EEBD
P 3800 1300
F 0 "WFD1" H 3850 1150 60  0000 C CNN
F 1 "MCX" H 3800 1400 60  0000 C CNN
	1    3800 1300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR030
U 1 1 50C0EEBC
P 3950 1500
F 0 "#PWR030" H 3950 1500 30  0001 C CNN
F 1 "GND" H 3950 1430 30  0001 C CNN
	1    3950 1500
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD3
U 1 1 50C0EEB8
P 10250 1300
F 0 "WFD3" H 10300 1150 60  0000 C CNN
F 1 "MCX" H 10250 1400 60  0000 C CNN
	1    10250 1300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 50C0EEB7
P 10400 1500
F 0 "#PWR031" H 10400 1500 30  0001 C CNN
F 1 "GND" H 10400 1430 30  0001 C CNN
	1    10400 1500
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD2
U 1 1 50C0EEB3
P 7000 1300
F 0 "WFD2" H 7050 1150 60  0000 C CNN
F 1 "MCX" H 7000 1400 60  0000 C CNN
	1    7000 1300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR032
U 1 1 50C0EEB2
P 7150 1500
F 0 "#PWR032" H 7150 1500 30  0001 C CNN
F 1 "GND" H 7150 1430 30  0001 C CNN
	1    7150 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR033
U 1 1 50C0EE75
P 8700 6400
F 0 "#PWR033" H 8700 6400 30  0001 C CNN
F 1 "GND" H 8700 6330 30  0001 C CNN
	1    8700 6400
	1    0    0    -1  
$EndComp
$Comp
L MCX WFD8
U 1 1 50C0EDFD
P 8550 6200
F 0 "WFD8" H 8600 6050 60  0000 C CNN
F 1 "MCX" H 8550 6300 60  0000 C CNN
	1    8550 6200
	-1   0    0    -1  
$EndComp
$Comp
L R R_CL5
U 1 1 50BE6EB4
P 4900 3450
F 0 "R_CL5" V 5000 3450 50  0000 C CNN
F 1 "100k" V 4900 3450 50  0000 C CNN
	1    4900 3450
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF5
U 1 1 50BE6EB3
P 4900 3950
F 0 "R_HVF5" V 5000 3950 50  0000 C CNN
F 1 "100k" V 4900 3950 50  0000 C CNN
	1    4900 3950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR034
U 1 1 50BE6EB2
P 5200 4150
F 0 "#PWR034" H 5200 4150 30  0001 C CNN
F 1 "GND" H 5200 4080 30  0001 C CNN
	1    5200 4150
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF5
U 1 1 50BE6EB1
P 5200 3900
F 0 "C_HVF5" H 4950 4000 50  0000 L CNN
F 1 "4.7nF" H 5100 3800 50  0000 L CNN
	1    5200 3900
	-1   0    0    1   
$EndComp
$Comp
L R R_CL6
U 1 1 50BE6EAF
P 8250 3450
F 0 "R_CL6" V 8350 3450 50  0000 C CNN
F 1 "100k" V 8250 3450 50  0000 C CNN
	1    8250 3450
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF6
U 1 1 50BE6EAE
P 8250 3950
F 0 "R_HVF6" V 8350 3950 50  0000 C CNN
F 1 "100k" V 8250 3950 50  0000 C CNN
	1    8250 3950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR035
U 1 1 50BE6EAD
P 8550 4150
F 0 "#PWR035" H 8550 4150 30  0001 C CNN
F 1 "GND" H 8550 4080 30  0001 C CNN
	1    8550 4150
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF6
U 1 1 50BE6EAC
P 8550 3900
F 0 "C_HVF6" H 8300 4000 50  0000 L CNN
F 1 "4.7nF" H 8450 3800 50  0000 L CNN
	1    8550 3900
	-1   0    0    1   
$EndComp
$Comp
L R R_CL7
U 1 1 50BE6EAA
P 1700 5350
F 0 "R_CL7" V 1800 5350 50  0000 C CNN
F 1 "100k" V 1700 5350 50  0000 C CNN
	1    1700 5350
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF7
U 1 1 50BE6EA9
P 1700 5850
F 0 "R_HVF7" V 1800 5850 50  0000 C CNN
F 1 "100k" V 1700 5850 50  0000 C CNN
	1    1700 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 50BE6EA8
P 2000 6050
F 0 "#PWR036" H 2000 6050 30  0001 C CNN
F 1 "GND" H 2000 5980 30  0001 C CNN
	1    2000 6050
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF7
U 1 1 50BE6EA7
P 2000 5800
F 0 "C_HVF7" H 1750 5900 50  0000 L CNN
F 1 "4.7nF" H 1900 5700 50  0000 L CNN
	1    2000 5800
	-1   0    0    1   
$EndComp
$Comp
L R R_CL4
U 1 1 50BE6EA5
P 1700 3400
F 0 "R_CL4" V 1800 3400 50  0000 C CNN
F 1 "100k" V 1700 3400 50  0000 C CNN
	1    1700 3400
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF4
U 1 1 50BE6EA4
P 1700 3900
F 0 "R_HVF4" V 1800 3900 50  0000 C CNN
F 1 "100k" V 1700 3900 50  0000 C CNN
	1    1700 3900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 50BE6EA3
P 2000 4100
F 0 "#PWR037" H 2000 4100 30  0001 C CNN
F 1 "GND" H 2000 4030 30  0001 C CNN
	1    2000 4100
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF4
U 1 1 50BE6EA2
P 2000 3850
F 0 "C_HVF4" H 1750 3950 50  0000 L CNN
F 1 "4.7nF" H 1900 3750 50  0000 L CNN
	1    2000 3850
	-1   0    0    1   
$EndComp
$Comp
L R R_CL1
U 1 1 50BE6EA1
P 1700 1550
F 0 "R_CL1" V 1800 1550 50  0000 C CNN
F 1 "100k" V 1700 1550 50  0000 C CNN
	1    1700 1550
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF1
U 1 1 50BE6EA0
P 1700 2050
F 0 "R_HVF1" V 1800 2050 50  0000 C CNN
F 1 "100k" V 1700 2050 50  0000 C CNN
	1    1700 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR038
U 1 1 50BE6E9F
P 2000 2250
F 0 "#PWR038" H 2000 2250 30  0001 C CNN
F 1 "GND" H 2000 2180 30  0001 C CNN
	1    2000 2250
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF1
U 1 1 50BE6E9E
P 2000 2000
F 0 "C_HVF1" H 1750 2100 50  0000 L CNN
F 1 "4.7nF" H 1900 1900 50  0000 L CNN
	1    2000 2000
	-1   0    0    1   
$EndComp
$Comp
L R R_CL3
U 1 1 50BE6E9C
P 8250 1550
F 0 "R_CL3" V 8350 1550 50  0000 C CNN
F 1 "100k" V 8250 1550 50  0000 C CNN
	1    8250 1550
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF3
U 1 1 50BE6E9B
P 8250 2050
F 0 "R_HVF3" V 8350 2050 50  0000 C CNN
F 1 "100k" V 8250 2050 50  0000 C CNN
	1    8250 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR039
U 1 1 50BE6E9A
P 8550 2250
F 0 "#PWR039" H 8550 2250 30  0001 C CNN
F 1 "GND" H 8550 2180 30  0001 C CNN
	1    8550 2250
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF3
U 1 1 50BE6E99
P 8550 2000
F 0 "C_HVF3" H 8300 2100 50  0000 L CNN
F 1 "4.7nF" H 8450 1900 50  0000 L CNN
	1    8550 2000
	-1   0    0    1   
$EndComp
$Comp
L R R_CL2
U 1 1 50BE6E95
P 4900 1550
F 0 "R_CL2" V 5000 1550 50  0000 C CNN
F 1 "100k" V 4900 1550 50  0000 C CNN
	1    4900 1550
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF2
U 1 1 50BE6E94
P 4900 2050
F 0 "R_HVF2" V 5000 2050 50  0000 C CNN
F 1 "100k" V 4900 2050 50  0000 C CNN
	1    4900 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR040
U 1 1 50BE6E93
P 5200 2250
F 0 "#PWR040" H 5200 2250 30  0001 C CNN
F 1 "GND" H 5200 2180 30  0001 C CNN
	1    5200 2250
	1    0    0    -1  
$EndComp
$Comp
L C C_HVF2
U 1 1 50BE6E92
P 5200 2000
F 0 "C_HVF2" H 4950 2100 50  0000 L CNN
F 1 "4.7nF" H 5100 1900 50  0000 L CNN
	1    5200 2000
	-1   0    0    1   
$EndComp
$Comp
L C C_HVF8
U 1 1 50BE6E05
P 6900 6900
F 0 "C_HVF8" H 6650 7000 50  0000 L CNN
F 1 "4.7nF" H 6800 6800 50  0000 L CNN
	1    6900 6900
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR041
U 1 1 50BE6E04
P 6900 7150
F 0 "#PWR041" H 6900 7150 30  0001 C CNN
F 1 "GND" H 6900 7080 30  0001 C CNN
	1    6900 7150
	1    0    0    -1  
$EndComp
$Comp
L R R_HVF8
U 1 1 50BE6DB1
P 6600 6950
F 0 "R_HVF8" V 6700 6950 50  0000 C CNN
F 1 "100k" V 6600 6950 50  0000 C CNN
	1    6600 6950
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 50BD0DE5
P 6650 3450
F 0 "R5" V 6750 3450 50  0000 C CNN
F 1 "1k" V 6650 3450 50  0000 C CNN
	1    6650 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR042
U 1 1 50BD0DE4
P 6650 3700
F 0 "#PWR042" H 6650 3700 30  0001 C CNN
F 1 "GND" H 6650 3630 30  0001 C CNN
	1    6650 3700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR043
U 1 1 50BD0DDD
P 10000 3700
F 0 "#PWR043" H 10000 3700 30  0001 C CNN
F 1 "GND" H 10000 3630 30  0001 C CNN
	1    10000 3700
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 50BD0DDC
P 10000 3450
F 0 "R6" V 10100 3450 50  0000 C CNN
F 1 "1k" V 10000 3450 50  0000 C CNN
	1    10000 3450
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 50BD0DD5
P 3450 1550
F 0 "R1" V 3550 1550 50  0000 C CNN
F 1 "1k" V 3450 1550 50  0000 C CNN
	1    3450 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR044
U 1 1 50BD0DD4
P 3450 1800
F 0 "#PWR044" H 3450 1800 30  0001 C CNN
F 1 "GND" H 3450 1730 30  0001 C CNN
	1    3450 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 50BD0DCB
P 3450 3650
F 0 "#PWR045" H 3450 3650 30  0001 C CNN
F 1 "GND" H 3450 3580 30  0001 C CNN
	1    3450 3650
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 50BD0DCA
P 3450 3400
F 0 "R4" V 3550 3400 50  0000 C CNN
F 1 "1k" V 3450 3400 50  0000 C CNN
	1    3450 3400
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 50BD0DC3
P 3450 5350
F 0 "R7" V 3550 5350 50  0000 C CNN
F 1 "1k" V 3450 5350 50  0000 C CNN
	1    3450 5350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR046
U 1 1 50BD0DC2
P 3450 5600
F 0 "#PWR046" H 3450 5600 30  0001 C CNN
F 1 "GND" H 3450 5530 30  0001 C CNN
	1    3450 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR047
U 1 1 50BD0DB8
P 9950 1800
F 0 "#PWR047" H 9950 1800 30  0001 C CNN
F 1 "GND" H 9950 1730 30  0001 C CNN
	1    9950 1800
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 50BD0DB7
P 9950 1550
F 0 "R3" V 10050 1550 50  0000 C CNN
F 1 "1k" V 9950 1550 50  0000 C CNN
	1    9950 1550
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 50BD0DA8
P 6650 1550
F 0 "R2" V 6750 1550 50  0000 C CNN
F 1 "1k" V 6650 1550 50  0000 C CNN
	1    6650 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR048
U 1 1 50BD0DA7
P 6650 1800
F 0 "#PWR048" H 6650 1800 30  0001 C CNN
F 1 "GND" H 6650 1730 30  0001 C CNN
	1    6650 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR049
U 1 1 50BD0C83
P 8300 6700
F 0 "#PWR049" H 8300 6700 30  0001 C CNN
F 1 "GND" H 8300 6630 30  0001 C CNN
	1    8300 6700
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 50BD0C7A
P 8300 6450
F 0 "R8" V 8400 6450 50  0000 C CNN
F 1 "1k" V 8300 6450 50  0000 C CNN
	1    8300 6450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR050
U 1 1 50BD0A8B
P 11050 6300
F 0 "#PWR050" H 11050 6300 30  0001 C CNN
F 1 "GND" H 11050 6230 30  0001 C CNN
	1    11050 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR051
U 1 1 50BD0A87
P 10500 7050
F 0 "#PWR051" H 10500 7050 30  0001 C CNN
F 1 "GND" H 10500 6980 30  0001 C CNN
	1    10500 7050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR052
U 1 1 50BD0A84
P 10300 6500
F 0 "#PWR052" H 10300 6500 30  0001 C CNN
F 1 "GND" H 10300 6430 30  0001 C CNN
	1    10300 6500
	1    0    0    -1  
$EndComp
$Comp
L C C_-6V1
U 1 1 50BD0A79
P 10300 6250
F 0 "C_-6V1" H 10350 6350 50  0000 L CNN
F 1 "0.1uF" H 10200 6150 50  0000 L CNN
	1    10300 6250
	-1   0    0    1   
$EndComp
$Comp
L C C_+1V1
U 1 1 50BD0A6F
P 10500 6750
F 0 "C_+1V1" H 10550 6850 50  0000 L CNN
F 1 "0.1uF" H 10400 6650 50  0000 L CNN
	1    10500 6750
	-1   0    0    1   
$EndComp
$Comp
L C C_+3V1
U 1 1 50BD0A57
P 11050 6100
F 0 "C_+3V1" H 11100 6200 50  0000 L CNN
F 1 "0.1uF" H 10950 6000 50  0000 L CNN
	1    11050 6100
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR053
U 1 1 50B7E96C
P 10200 5800
F 0 "#PWR053" H 10200 5800 30  0001 C CNN
F 1 "GND" H 10200 5730 30  0001 C CNN
	1    10200 5800
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR70
U 1 1 50B7E957
P 10200 6000
F 0 "#PWR70" H 10200 6120 20  0001 C CNN
F 1 "-6V" H 10200 6100 20  0000 C CNN
	1    10200 6000
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR36
U 1 1 50B7E8E8
P 5800 3700
F 0 "#PWR36" H 5800 3820 20  0001 C CNN
F 1 "-6V" H 5800 3800 20  0000 C CNN
	1    5800 3700
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR38
U 1 1 50B7E8DB
P 9150 3700
F 0 "#PWR38" H 9150 3820 20  0001 C CNN
F 1 "-6V" H 9150 3800 20  0000 C CNN
	1    9150 3700
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR10
U 1 1 50B7E8D1
P 2600 1800
F 0 "#PWR10" H 2600 1920 20  0001 C CNN
F 1 "-6V" H 2600 1900 20  0000 C CNN
	1    2600 1800
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR34
U 1 1 50B7E8CC
P 2600 3650
F 0 "#PWR34" H 2600 3770 20  0001 C CNN
F 1 "-6V" H 2600 3750 20  0000 C CNN
	1    2600 3650
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR57
U 1 1 50B7E8C4
P 2600 5600
F 0 "#PWR57" H 2600 5720 20  0001 C CNN
F 1 "-6V" H 2600 5700 20  0000 C CNN
	1    2600 5600
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR14
U 1 1 50B7E8BD
P 9150 1800
F 0 "#PWR14" H 9150 1920 20  0001 C CNN
F 1 "-6V" H 9150 1900 20  0000 C CNN
	1    9150 1800
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR12
U 1 1 50B7E8B4
P 5800 1800
F 0 "#PWR12" H 5800 1920 20  0001 C CNN
F 1 "-6V" H 5800 1900 20  0000 C CNN
	1    5800 1800
	1    0    0    -1  
$EndComp
$Comp
L -6V #PWR78
U 1 1 50B7E868
P 7500 6700
F 0 "#PWR78" H 7500 6820 20  0001 C CNN
F 1 "-6V" H 7500 6800 20  0000 C CNN
	1    7500 6700
	1    0    0    -1  
$EndComp
$Comp
L +1V #PWR054
U 1 1 50B76C1A
P 10400 6000
F 0 "#PWR054" H 10400 6140 20  0001 C CNN
F 1 "+1V" H 10400 6110 30  0000 C CNN
	1    10400 6000
	1    0    0    -1  
$EndComp
$Comp
L +3V #PWR055
U 1 1 50B76C13
P 11050 5850
F 0 "#PWR055" H 11050 5990 20  0001 C CNN
F 1 "+3V" H 11050 5960 30  0000 C CNN
	1    11050 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR056
U 1 1 50B663BB
P 5550 900
F 0 "#PWR056" H 5550 900 30  0001 C CNN
F 1 "GND" H 5550 830 30  0001 C CNN
	1    5550 900 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR057
U 1 1 50B663B9
P 6300 2150
F 0 "#PWR057" H 6300 2150 30  0001 C CNN
F 1 "GND" H 6300 2080 30  0001 C CNN
	1    6300 2150
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 50B663B8
P 6300 1900
F 0 "C2" H 6350 2000 50  0000 L CNN
F 1 "0.01uF" H 6200 1800 50  0000 L CNN
	1    6300 1900
	-1   0    0    1   
$EndComp
$Comp
L C C_HV2
U 1 1 50B663B7
P 5250 1300
F 0 "C_HV2" V 5400 1150 50  0000 L CNN
F 1 "10nF" V 5100 1200 50  0000 L CNN
	1    5250 1300
	0    1    1    0   
$EndComp
$Comp
L DIODE DPN2
U 1 1 50B663B2
P 6000 1500
F 0 "DPN2" H 6000 1600 40  0000 C CNN
F 1 "DIODE" H 6000 1400 40  0000 C CNN
	1    6000 1500
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP2
U 1 1 50B663B1
P 5800 1100
F 0 "DPP2" H 5800 1200 40  0000 C CNN
F 1 "DIODE" H 5800 1000 40  0000 C CNN
	1    5800 1100
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP3
U 1 1 50B663B0
P 9150 1100
F 0 "DPP3" H 9150 1200 40  0000 C CNN
F 1 "DIODE" H 9150 1000 40  0000 C CNN
	1    9150 1100
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPN3
U 1 1 50B663AF
P 9350 1500
F 0 "DPN3" H 9350 1600 40  0000 C CNN
F 1 "DIODE" H 9350 1400 40  0000 C CNN
	1    9350 1500
	0    -1   -1   0   
$EndComp
$Comp
L C C_HV3
U 1 1 50B663AA
P 8600 1300
F 0 "C_HV3" V 8750 1150 50  0000 L CNN
F 1 "10nF" V 8450 1200 50  0000 L CNN
	1    8600 1300
	0    1    1    0   
$EndComp
$Comp
L C C3
U 1 1 50B663A9
P 9650 1900
F 0 "C3" H 9700 2000 50  0000 L CNN
F 1 "0.01uF" H 9550 1800 50  0000 L CNN
	1    9650 1900
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR058
U 1 1 50B663A8
P 9650 2150
F 0 "#PWR058" H 9650 2150 30  0001 C CNN
F 1 "GND" H 9650 2080 30  0001 C CNN
	1    9650 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR059
U 1 1 50B663A6
P 8900 900
F 0 "#PWR059" H 8900 900 30  0001 C CNN
F 1 "GND" H 8900 830 30  0001 C CNN
	1    8900 900 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR060
U 1 1 50B663A5
P 2350 2750
F 0 "#PWR060" H 2350 2750 30  0001 C CNN
F 1 "GND" H 2350 2680 30  0001 C CNN
	1    2350 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR061
U 1 1 50B663A3
P 3100 4000
F 0 "#PWR061" H 3100 4000 30  0001 C CNN
F 1 "GND" H 3100 3930 30  0001 C CNN
	1    3100 4000
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 50B663A2
P 3100 3750
F 0 "C4" H 3150 3850 50  0000 L CNN
F 1 "0.01uF" H 3000 3650 50  0000 L CNN
	1    3100 3750
	-1   0    0    1   
$EndComp
$Comp
L C C_HV4
U 1 1 50B663A1
P 2050 3150
F 0 "C_HV4" V 2200 3000 50  0000 L CNN
F 1 "10nF" V 1900 3050 50  0000 L CNN
	1    2050 3150
	0    1    1    0   
$EndComp
$Comp
L DIODE DPN4
U 1 1 50B6639C
P 2800 3350
F 0 "DPN4" H 2800 3450 40  0000 C CNN
F 1 "DIODE" H 2800 3250 40  0000 C CNN
	1    2800 3350
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP4
U 1 1 50B6639B
P 2600 2950
F 0 "DPP4" H 2600 3050 40  0000 C CNN
F 1 "DIODE" H 2600 2850 40  0000 C CNN
	1    2600 2950
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP1
U 1 1 50B6639A
P 2600 1100
F 0 "DPP1" H 2600 1200 40  0000 C CNN
F 1 "DIODE" H 2600 1000 40  0000 C CNN
	1    2600 1100
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPN1
U 1 1 50B66399
P 2800 1500
F 0 "DPN1" H 2800 1600 40  0000 C CNN
F 1 "DIODE" H 2800 1400 40  0000 C CNN
	1    2800 1500
	0    -1   -1   0   
$EndComp
$Comp
L C C_HV1
U 1 1 50B66394
P 2050 1300
F 0 "C_HV1" V 2200 1150 50  0000 L CNN
F 1 "10nf" V 1900 1200 50  0000 L CNN
F 3 "399-5851-1-ND" H 2050 1300 60  0001 C CNN
	1    2050 1300
	0    1    1    0   
$EndComp
$Comp
L C C1
U 1 1 50B66393
P 3100 1900
F 0 "C1" H 3150 2000 50  0000 L CNN
F 1 "0.01uF" H 3000 1800 50  0000 L CNN
	1    3100 1900
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR062
U 1 1 50B66392
P 3100 2150
F 0 "#PWR062" H 3100 2150 30  0001 C CNN
F 1 "GND" H 3100 2080 30  0001 C CNN
	1    3100 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR063
U 1 1 50B66390
P 2350 900
F 0 "#PWR063" H 2350 900 30  0001 C CNN
F 1 "GND" H 2350 830 30  0001 C CNN
	1    2350 900 
	1    0    0    -1  
$EndComp
$Comp
L DIODE DPP7
U 1 1 50B6638F
P 2600 4900
F 0 "DPP7" H 2600 5000 40  0000 C CNN
F 1 "DIODE" H 2600 4800 40  0000 C CNN
	1    2600 4900
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPN7
U 1 1 50B6638E
P 2800 5300
F 0 "DPN7" H 2800 5400 40  0000 C CNN
F 1 "DIODE" H 2800 5200 40  0000 C CNN
	1    2800 5300
	0    -1   -1   0   
$EndComp
$Comp
L C C_HV7
U 1 1 50B66389
P 2050 5100
F 0 "C_HV7" V 2200 4950 50  0000 L CNN
F 1 "10nf" V 1900 5000 50  0000 L CNN
	1    2050 5100
	0    1    1    0   
$EndComp
$Comp
L C C7
U 1 1 50B66388
P 3100 5700
F 0 "C7" H 3150 5800 50  0000 L CNN
F 1 "0.01uF" H 3000 5600 50  0000 L CNN
	1    3100 5700
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR064
U 1 1 50B66387
P 3100 5950
F 0 "#PWR064" H 3100 5950 30  0001 C CNN
F 1 "GND" H 3100 5880 30  0001 C CNN
	1    3100 5950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR065
U 1 1 50B66385
P 2350 4700
F 0 "#PWR065" H 2350 4700 30  0001 C CNN
F 1 "GND" H 2350 4630 30  0001 C CNN
	1    2350 4700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR066
U 1 1 50B66384
P 8900 2800
F 0 "#PWR066" H 8900 2800 30  0001 C CNN
F 1 "GND" H 8900 2730 30  0001 C CNN
	1    8900 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR067
U 1 1 50B66382
P 9650 4050
F 0 "#PWR067" H 9650 4050 30  0001 C CNN
F 1 "GND" H 9650 3980 30  0001 C CNN
	1    9650 4050
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 50B66381
P 9650 3800
F 0 "C6" H 9700 3900 50  0000 L CNN
F 1 "0.01uF" H 9550 3700 50  0000 L CNN
	1    9650 3800
	-1   0    0    1   
$EndComp
$Comp
L C C_HV6
U 1 1 50B66380
P 8600 3200
F 0 "C_HV6" V 8750 3050 50  0000 L CNN
F 1 "10nF" V 8450 3100 50  0000 L CNN
	1    8600 3200
	0    1    1    0   
$EndComp
$Comp
L DIODE DPN6
U 1 1 50B6637B
P 9350 3400
F 0 "DPN6" H 9350 3500 40  0000 C CNN
F 1 "DIODE" H 9350 3300 40  0000 C CNN
	1    9350 3400
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP6
U 1 1 50B6637A
P 9150 3000
F 0 "DPP6" H 9150 3100 40  0000 C CNN
F 1 "DIODE" H 9150 2900 40  0000 C CNN
	1    9150 3000
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP5
U 1 1 50B66379
P 5800 3000
F 0 "DPP5" H 5800 3100 40  0000 C CNN
F 1 "DIODE" H 5800 2900 40  0000 C CNN
	1    5800 3000
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPN5
U 1 1 50B66378
P 6000 3400
F 0 "DPN5" H 6000 3500 40  0000 C CNN
F 1 "DIODE" H 6000 3300 40  0000 C CNN
	1    6000 3400
	0    -1   -1   0   
$EndComp
$Comp
L C C_HV5
U 1 1 50B66373
P 5250 3200
F 0 "C_HV5" V 5400 3050 50  0000 L CNN
F 1 "10nF" V 5100 3100 50  0000 L CNN
	1    5250 3200
	0    1    1    0   
$EndComp
$Comp
L C C5
U 1 1 50B66372
P 6300 3800
F 0 "C5" H 6350 3900 50  0000 L CNN
F 1 "0.01uF" H 6200 3700 50  0000 L CNN
	1    6300 3800
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR068
U 1 1 50B66371
P 6300 4050
F 0 "#PWR068" H 6300 4050 30  0001 C CNN
F 1 "GND" H 6300 3980 30  0001 C CNN
	1    6300 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR069
U 1 1 50B6636F
P 5550 2800
F 0 "#PWR069" H 5550 2800 30  0001 C CNN
F 1 "GND" H 5550 2730 30  0001 C CNN
	1    5550 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR070
U 1 1 50B66357
P 8000 7050
F 0 "#PWR070" H 8000 7050 30  0001 C CNN
F 1 "GND" H 8000 6980 30  0001 C CNN
	1    8000 7050
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 50B66356
P 8000 6800
F 0 "C8" H 8050 6900 50  0000 L CNN
F 1 "0.01uF" H 7900 6700 50  0000 L CNN
	1    8000 6800
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR071
U 1 1 50B66355
P 7250 5800
F 0 "#PWR071" H 7250 5800 30  0001 C CNN
F 1 "GND" H 7250 5730 30  0001 C CNN
	1    7250 5800
	1    0    0    -1  
$EndComp
$Comp
L C C_HV8
U 1 1 50B66354
P 6950 6200
F 0 "C_HV8" V 7100 6050 50  0000 L CNN
F 1 "10nF" V 6800 6100 50  0000 L CNN
	1    6950 6200
	0    1    1    0   
$EndComp
$Comp
L R R_CL8
U 1 1 50B66353
P 6600 6450
F 0 "R_CL8" V 6700 6450 50  0000 C CNN
F 1 "100k" V 6600 6450 50  0000 C CNN
	1    6600 6450
	1    0    0    -1  
$EndComp
$Comp
L DIODE DPN8
U 1 1 50B6634F
P 7700 6400
F 0 "DPN8" H 7700 6500 40  0000 C CNN
F 1 "DIODE" H 7700 6300 40  0000 C CNN
	1    7700 6400
	0    -1   -1   0   
$EndComp
$Comp
L DIODE DPP8
U 1 1 50B6634E
P 7500 6000
F 0 "DPP8" H 7500 6100 40  0000 C CNN
F 1 "DIODE" H 7500 5900 40  0000 C CNN
	1    7500 6000
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D_pulse1
U 1 1 50B6634D
P 7900 5400
F 0 "D_pulse1" H 7900 5500 40  0000 C CNN
F 1 "DIODE" H 7900 5300 40  0000 C CNN
	1    7900 5400
	0    -1   -1   0   
$EndComp
$Comp
L R R_discharge1
U 1 1 50B6634C
P 8150 5200
F 0 "R_discharge1" V 8050 5200 50  0000 C CNN
F 1 "100" V 8150 5200 50  0000 C CNN
	1    8150 5200
	0    1    1    0   
$EndComp
$Comp
L +1V #PWR072
U 1 1 50B6634B
P 8500 5200
F 0 "#PWR072" H 8500 5340 20  0001 C CNN
F 1 "+1V" H 8500 5310 30  0000 C CNN
	1    8500 5200
	1    0    0    -1  
$EndComp
$Comp
L +3V #PWR073
U 1 1 50B6634A
P 6250 5000
F 0 "#PWR073" H 6250 5140 20  0001 C CNN
F 1 "+3V" H 6250 5110 30  0000 C CNN
	1    6250 5000
	1    0    0    -1  
$EndComp
$Comp
L C C_pulse1
U 1 1 50B66349
P 7500 5200
F 0 "C_pulse1" V 7650 5050 50  0000 L CNN
F 1 "10 nF" V 7350 5100 50  0000 L CNN
	1    7500 5200
	0    1    1    0   
$EndComp
$Comp
L R Rcharge1
U 1 1 50B66348
P 6750 5000
F 0 "Rcharge1" V 6830 5000 50  0000 C CNN
F 1 "1k" V 6750 5000 50  0000 C CNN
	1    6750 5000
	0    1    1    0   
$EndComp
$Comp
L GND #PWR074
U 1 1 50B66346
P 7000 5700
F 0 "#PWR074" H 7000 5700 30  0001 C CNN
F 1 "GND" H 7000 5630 30  0001 C CNN
	1    7000 5700
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q1
U 1 1 50B66345
P 6900 5400
F 0 "Q1" H 6910 5570 60  0000 R CNN
F 1 "MOSFET_N" H 6910 5250 60  0000 R CNN
	1    6900 5400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
