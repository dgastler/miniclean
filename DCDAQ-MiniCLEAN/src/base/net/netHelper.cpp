#include <netHelper.h>

int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint32_t size,
	       struct timeval /*timeout*/)
{
  int32_t ret = 0;

  //From Unix Network Programming: Stevens.
  uint8_t * writePtr = ptr;
  int32_t nLeft = size;
  
  int32_t nWritten = 0;

  //Loop over what is left to write
  while(nLeft >0)
    {
      //take a shot at writing the data
      if( (nWritten = write(socketFD,writePtr,nLeft)) <=0)
	//Process any errors
	{
	  if(errno == EINTR)
	    {
	      nWritten = 0;
	    }
	  else
	    {
	      ret = -errno;
	      break;
	    }
	}
      //remote the transmitted data from the send "queue" and keep going
      nLeft -= nWritten;
      writePtr += nWritten;
    }
  if(ret >= 0)
    ret = size;
  return ret;
}

int32_t readN(int socketFD,
	      uint8_t * ptr,
	      uint32_t readSize,
	      int &ret_errno)
{
  //Read readSize bytes from the socket
  //If return != readSize, the socket died

  //From Unix Network Programming: Stevens.
  int32_t nLeft = readSize;
  int32_t nRead = 0;
  uint8_t * readPtr = ptr;
  
  while(nLeft >0)
    {
      ret_errno = 0;
      //Read some data
      if( (nRead = read(socketFD,readPtr,nLeft)) < 0)
	//process errors
	{
	  if(errno == EINTR)
	    {
	      nRead = 0;
	    }
	  else
	    {
	      ret_errno = errno;
	      return -1; //error
	    }
	}
      else if (nRead == 0)
	{
	  break; /* EOF */
	}

      //move along in our read to queue for our read
      nLeft -= nRead;
      readPtr += nRead;
    }  
  return (readSize - nLeft);
}


bool SetNonBlocking(int &fd,bool value)
{
  //Get the previous flags
  int currentFlags = fcntl(fd,F_GETFL,0);
  if(currentFlags < 0)
    {
      return(false);
    }
  //Make the socket non-blocking
  if(value)
    {
      currentFlags |= O_NONBLOCK;
    }
  else
    {
      currentFlags &= ~O_NONBLOCK;
    }

  int currentFlags2 = fcntl(fd,F_SETFL,currentFlags);
  if(currentFlags2 < 0)
    {
      return(false);
    }
  return(true);
}

//Get the local struct in_addr for a socket.
//Returns empty if it isn't an IPV4 address
DCMessage::DCAddress GetLocalAddress(int fd)
{
  struct sockaddr_un s_sockaddr; //Use sockaddr_un because it can hold all structures
  socklen_t s_sockaddr_size = sizeof(s_sockaddr);
  int ret = getsockname(fd,(struct sockaddr*) &s_sockaddr,&s_sockaddr_size);    
  if(ret == 0)
    {
      //bug in getsockname fails on AF_LOCAL! We will use the remote addr
      if(s_sockaddr.sun_family == AF_LOCAL)
	{
	  return GetRemoteAddress(fd);
	}
      else
	{
	  return DCMessage::DCAddress("",(struct sockaddr*) &s_sockaddr,s_sockaddr_size);
	}
    }
  return DCMessage::DCAddress();
}

//Get the remote struct in_addr for a socket.
//Returns empty if it isn't an IPV4 address
DCMessage::DCAddress GetRemoteAddress(int fd)
{
  struct sockaddr_un s_sockaddr; //Use sockaddr_un because it can hold all structures
  socklen_t s_sockaddr_size = sizeof(s_sockaddr);
  int ret = getpeername(fd,(struct sockaddr*) &s_sockaddr,&s_sockaddr_size);  
  if(ret == 0)
    {
      return DCMessage::DCAddress("",(struct sockaddr*) &s_sockaddr,s_sockaddr_size);
    }
  return DCMessage::DCAddress();
}

bool CloseSockFD(int fd)
{
  bool ret = true;
  if(fd >= 0)
    {
      SetNonBlocking(fd,false);  //turn off non-blocking before close
      if(close(fd) != 0)
	ret = false;
    }
  else
    {
      ret = false;
    }
  return ret;
}

bool SetSocketTTL(int fd, int time)
{
  if(setsockopt(fd,AF_INET,IP_TTL,&time,sizeof(time)) != 0)
    return false;
  return true;
}
