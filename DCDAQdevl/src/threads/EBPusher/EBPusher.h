#ifndef __EBPUSHER__
#define __EBPUSHER__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>

#include <Connection.h>
#include <EBDataPacket.h>

#include <FEPCRATManager.h>

#include <math.h>

#include <DetectorInfo.h>
//Fabs

#include <PacketBuilder.h>

#include <CHAN_FULL_WAVEFORM.h>
#include <CHAN_PROMPT_TOTAL.h>
#include <CHAN_ZLE_INTEGRAL.h>
#include <CHAN_ZLE_WAVEFORM.h>
#include <EVNT_FULL_WAVEFORM.h>
#include <EVNT_PROMPT_TOTAL.h>
#include <EVNT_ZLE_INTEGRAL.h>
#include <EVNT_ZLE_WAVEFORM.h>
#include <UNKNOWN.h>


class EBPusher : public DCThread 
{
 public:
  EBPusher(std::string Name);
  ~EBPusher()
    {
    };
  virtual bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  
  //_Net.cpp
  bool ProcessNewConnection();
  void DeleteConnection();
  void ProcessNetThreadMessage();
  bool StartNewConnection();
  //Connection to our server
  Connection EBAssembler;
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;

  //_EventProcess.cpp
  void ProcessEvent();
  bool SendData(FEPCBlock * event);
  
  //Packet building
  std::vector<PacketBuilder*> packetBuilder;

  //Detector information
  DetectorInfo detectorInfo;
  
  //DCThread overloads
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ThreadCleanUp(){DeleteConnection();};
  void PrintStatus();

  //Update control
  time_t lastTime;
  time_t currentTime;

  //Stats
  uint32_t timeStepProcessedEvents;

  // Memory access
  //FEPC Rat manager
  FEPCRATManager * FEPC_mem;
};


#endif
