#ifndef __MEM_BLOCK_READER__
#define __MEM_BLOCK_WRITER__

#include <DCThread.h>
#include <MemoryBlockManager.h>
#include <xmlHelper.h>
#include <RAT/DS/DCDAQ_DCTime.hh>
#include <RAT/DS/DCDAQ_RAWDataStructs.hh>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>

namespace FillReturn
{
  enum Type 
    {OK,END_OF_FILE,BAD_SIZE,ERROR,BAD_HEADER,BAD_READ};
};


class MemBlockReader : public DCThread
{
 public:
  MemBlockReader(std::string Name);
  ~MemBlockReader()
  {
  };
  virtual bool Setup(xmlNode * SetupNode, 
		     std::vector<MemoryManager*> &MemManager);
  virtual void MainLoop();

 private:
  virtual bool ProcessMessage(DCMessage::Message &message);
  
  FillReturn::Type FillMemBlock(MemoryBlock * block);
  //Memory manager access
  MemoryBlockManager * Mem;

  //A queue of files to read in and convert
  DCQueue<std::string> InFileNames;

  //====File Access====
  bool OpenFile();
  bool CloseFile();

  //Current open file
  std::string currentFileName;
  FILE * InFile;
  int blockCount;
  
};

#endif
