#ifndef __EBASSEMBLER__
#define __EBASSEMBLER__
//EBAssembler 

#include <DCThread.h>

#include <xmlHelper.h>

#include <EBDataPacket.h>
#include "RAT/DS/Root.hh"
#include "RAT/DS/EV.hh"

#include <EBPCRatManager.h>

#include <ServerConnection.h>
#include <Connection.h>

class EBAssembler : public DCThread 
{
 public:
  EBAssembler(std::string Name);
  ~EBAssembler();

  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  enum RATAssemberErrorCodes
  {
    OK = 0,
    LOCK_BLOCKED    = -1,
    WFD_NOT_PROCESSED = -2,
    FATAL_ERROR       = -3,
    BAD_WFD_HEADER    = -4
  };

  
  //======================================
  //EBAssembler.cpp
  //======================================
  virtual bool ProcessMessage(DCMessage::Message &message);
  virtual void ProcessTimeout();
  virtual void ProcessChildMessage(size_t iConn);
  virtual void PrintStatus();
  //======================================

  //======================================
  //EBAssembler_Run.cpp
  //======================================
  void ProcessEBPusher(DCNetThread * thread);
  void ProcessCollision(int64_t newEventID,
			int64_t oldEventID,
			int32_t evIndex,
			uint64_t password);
  void ProcessBadPacketLostPMT(DCNetPacket *);
  //======================================

  //======================================
  //EBAssembler_ProcessCHAN_ZLE_INTEGRAL.cpp
  //======================================
  int ProcessCHAN_ZLE_INTEGRAL(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessCHAN_ZLE_WAVEFORM.cpp
  //======================================
  int ProcessCHAN_ZLE_WAVEFORM(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessCHAN_PROMPT_TOTAL.cpp
  //======================================
  int ProcessCHAN_PROMPT_TOTAL(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessEVNT_ZLE_WAVEFORM.cpp
  //======================================
  int ProcessEVNT_ZLE_WAVEFORM(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessEVNT_ZLE_INTEGRAL.cpp
  //======================================
  int ProcessEVNT_ZLE_INTEGRAL(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessEVNT_PROMPT_TOTAL.cpp
  //======================================
  int ProcessEVNT_PROMPT_TOTAL(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessEVNT_FULL_WAVEFORM.cpp
  //======================================
  int ProcessEVNT_FULL_WAVEFORM(uint8_t * data, uint16_t size,EBPCBlock * event);

  //======================================
  //EBAssembler_ProcessCHAN_FULL_WAVEFORM.cpp
  //======================================
  int ProcessCHAN_FULL_WAVEFORM(uint8_t * data, uint16_t size,EBPCBlock * event);




  //======================================
  //EBAssembler_Net.cpp
  //======================================
  bool ProcessNewConnection();
  void DeleteConnection(unsigned int iConn);
  //======================================


  //Network data members
  std::string managerSettings;
  xmlDoc * managerDoc;
  xmlNode * managerNode;
  ServerConnection server;
  
  //Data reduction PC memory manager
  EBPCRATManager * EBPC_mem;


  //PMT database
  struct PMTInfo
  {
    uint32_t ID;
    uint16_t FEPC;
    uint16_t WFD;
  };
  std::vector<PMTInfo> pmtList;
  template<class T>
  bool ReadPMTInfo(xmlNode * pmtNode,const char * entry, T & value);

  //DataProcessing values
  uint32_t promptStartTime;
  uint32_t lateStartTime;

  std::vector<RAT::DS::PMT*> pmtCopies;
  std::vector<RAT::DS::Raw*> rawCopies;
  std::vector<RAT::DS::RawIntegral*> rawIntegralCopies;
  std::vector<RAT::DS::RawWaveform*> rawWaveformCopies;

  RAT::DS::EV * ev; 
  
  uint8_t nCopies;

};


#endif
