/*==DAQ Test Data==============
  This class manages several DAQFile classes that write out
  WFD data from a parsed RAT data structure.
  The final files can be used to feed the FakeV1720 processor
  in DCDAQ and simulate the opperation of the DAQ.
====DAQ Test Data============*/

#ifndef __DAQTESTDATA__
#define __DAQTESTDATA__

//System includes
#include <vector>

//Fake DAQ includes
#include "DAQFile.h"

//RAT includes
#include <RAT/DS/Root.hh>

class DAQTestData
{
 public:
  DAQTestData(){};
  ~DAQTestData();

  //File access
  uint32_t GetDAQFileCount() {return(Files.size());}
  DAQFile * GetDAQFile(uint32_t i)
  {
    if(i<Files.size())
      return(Files[i]);
    return(NULL);
  }
  DAQFile * AddDAQFile()
  {
    DAQFile * newDAQFile = new DAQFile;
    Files.push_back(newDAQFile);
    return(newDAQFile);
  }
  

  //Process a new event
  int Process(RAT::DS::Root * ds);
  
 private:
  std::vector<DAQFile *> Files;  
  
};

#endif
