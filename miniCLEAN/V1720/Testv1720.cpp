#include <libxml/parser.h>
#include <libxml/tree.h>
#include "v1720.h"

#include "MemBlocks.h"

#include <string>

int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      printf("Usage: %s XML-file\n",argv[0]);
      return(0);
    }
  
  xmlDoc *doc = NULL;
  xmlNode *root_element = NULL;
  
  //Open and parse file.
  doc = xmlReadFile(argv[1], NULL, 0);
  if (doc == NULL) 
    {
      printf("error: could not parse file %s\n", argv[1]);
    }
  
  //Get the root node
  root_element = xmlDocGetRootElement(doc);

  MemoryBlock block;
  block.allocatedSize = 0x3E8FA0;//0x40020;
  block.dataSize = 0;
  block.buffer = new char[block.allocatedSize];
  if(block.buffer == NULL)
    {
      fprintf(stderr,"WTF Mate? allocation error?\n");
      return(1);
    }
  v1720 * WFD = new v1720();
  WFD->LoadXMLConfig(root_element);
  WFD->Start();
  sleep(10);
  WFD->Readout(block);
  WFD->Stop();
  //  for(int i = 0; i < (block.dataSize >> 2) ;i++)
  for(int i = 0; i < 0x1004 ;i++)
    {
      if(i > 0)
	{
	  if(((uint16_t *) ( (uint32_t *) block.buffer))[2*i+1] == 0xA000)
	    {
	      break;
	    }
	}
      if(i > 3)
	{
	  printf("%05d %05d 0x%04X\n",
		 2*i,
		 ((uint16_t *) ( (uint32_t *) block.buffer))[2*i],
		 ((uint16_t *) ( (uint32_t *) block.buffer))[2*i]);
	  printf("%05d %05d 0x%04X\n",
		 2*i+1,
		 ((uint16_t *) ( (uint32_t *) block.buffer))[2*i+1],
		 ((uint16_t *) ( (uint32_t *) block.buffer))[2*i+1]); 
	}
      else
	{
	  fprintf(stderr,"0x%08X\n",
		  ((uint32_t *) block.buffer)[i]);
	}
    }

  std::string outFileName(argv[1]);
  outFileName = (outFileName.substr(0,outFileName.find(".xml")) + std::string("Out.xml"));
  xmlSaveFormatFile(outFileName.c_str(),doc,1);
  
  xmlFreeDoc(doc);
  xmlCleanupParser();

  return(0);
}
