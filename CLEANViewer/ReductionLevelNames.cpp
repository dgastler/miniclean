#include "ReductionLevelNames.h"
static const char * const Names[ReductionLevel::COUNT]=
  {
    "UNKNOWN",
    "CHAN_FULL_WAVEFORM",
    "CHAN_ZLE_WAVEFORM",
    "CHAN_ZLE_INTEGRAL",
    "CHAN_PROMPT_TOTAL",
    "EVNT_FULL_WAVEFORM",
    "EVNT_ZLE_WAVEFORM",
    "EVNT_ZLE_INTEGRAL",
    "EVNT_PROMPT_TOTAL"
   };

  const char * GetRLN(int RL)
  {
    if(RL<ReductionLevel::COUNT)
      return Names[RL];
    return NULL;
  };



