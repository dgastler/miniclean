#include <libxml/parser.h>
#include <libxml/tree.h>

void printNode(xmlNode * baseNode,int layer)
{
  xmlNode * curNode = NULL;
  for(curNode = baseNode; curNode; curNode = curNode->next)
    {
      if(curNode->type == XML_ELEMENT_NODE)
	{
	  for(int i = 0;i < layer;i++)
	    {
	      printf(" ");
	    }
	  printf("node %s\n",curNode->name);
	}
      else if((curNode->type == XML_TEXT_NODE) &&
	      (curNode->content[0] != '\n') &&
	      (curNode->prev == NULL)
	      )
	{
	  for(int i = 0;i < layer;i++)
	    {
	      printf(" ");
	    }
	  printf("%s\n",curNode->content);
	}
      printNode(curNode->children,layer+1);
    }
}

int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      printf("Usage: %s XML-file\n",argv[0]);
      return(0);
    }

  xmlDoc *doc = NULL;
  xmlNode *root_element = NULL;

  //Open and parse file.
  doc = xmlReadFile(argv[1], NULL, 0);
  if (doc == NULL) 
    {
      printf("error: could not parse file %s\n", argv[1]);
    }

  //Get the root node
  root_element = xmlDocGetRootElement(doc);
  
  printNode(root_element,0);
  
  xmlFreeDoc(doc);
  xmlCleanupParser();

  return(0);
}
