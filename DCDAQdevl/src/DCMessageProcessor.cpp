#include <DCMessageProcessor.h>

DCMessageProcessor::DCMessageProcessor(DCLauncher * _Launcher)
{
  Name.assign("DCMessageProcessor");
  Launcher = _Launcher;
  //Supplied message functions
  messageTypeToFunctionMap[DCMessage::LAUNCH]         
    = &DCMessageProcessor::Launch;          
  messageTypeToFunctionMap[DCMessage::REGISTER_ADDR]  
    = &DCMessageProcessor::RegisterAddr; 
  messageTypeToFunctionMap[DCMessage::UNREGISTER_ADDR]  
    = &DCMessageProcessor::UnRegisterAddr; 
  messageTypeToFunctionMap[DCMessage::REGISTER_TYPE]  
    = &DCMessageProcessor::RegisterType; 
  messageTypeToFunctionMap[DCMessage::UNREGISTER_TYPE]  
    = &DCMessageProcessor::UnRegisterType; 
  messageTypeToFunctionMap[DCMessage::STOP]           
    = &DCMessageProcessor::Stop;
  messageTypeToFunctionMap[DCMessage::GO]             
    = &DCMessageProcessor::Go;
  messageTypeToFunctionMap[DCMessage::PAUSE]          
    = &DCMessageProcessor::Pause;
  messageTypeToFunctionMap[DCMessage::RUN_NUMBER]     
    = &DCMessageProcessor::RunNumber;
  messageTypeToFunctionMap[DCMessage::GET_STATS]
    = &DCMessageProcessor::GetStats;
  
  recursiveCount= 0;
  maxRecursiveCount = 1;
}

bool DCMessageProcessor::ProcessMessage(int selectReturn,
					fd_set retReadSet,
					fd_set /*retWriteSet*/,
					fd_set /*retExceptionSet*/)
{
  //Iterator for out command lookup.
  std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,DCThread *,bool &)>::iterator it;
  //Iterator for our FD-thread map
  std::map<int,size_t>::const_iterator FDit;

  bool runReturn = true;
  //Loop over all the entries in the FDMap and find those that are readible
  for(FDit = Launcher->GetFDMap().begin();
      ((FDit != Launcher->GetFDMap().end()) &&
       (selectReturn >0));
      FDit++)
    {
      //Ask if this FD was ready
      if(FD_ISSET(FDit->first,&retReadSet))
	{
	  //report that we've addressed this fd
	  selectReturn--;
	  //Get the message from the read thread and pass it to ProcessMessage(message)
	  runReturn = (runReturn && 
		       ProcessMessage(Launcher->GetThreads()[FDit->second]->GetMessageOut(true)));
	}
    }
  return(runReturn);
}

bool DCMessageProcessor::ProcessMessage(DCMessage::Message message)
{
  bool runReturn = true;

  //First check if this message should be routed outside of this DCDAQ session
  std::string destinationName;
  struct in_addr destinationAddr;
  message.GetDestination(destinationName,
			 destinationAddr);
  
  if(destinationAddr.s_addr > 0)
    //we have a message for outside of this DCDAQ session
    {
      runReturn = RouteRemoteMessage(message);
    }
  else
    //No remote adddres so it is a message for a local thread or a command
    {
      //check if there is a thread destination
      if(destinationName.size() > 0)
	//Route to the local thread
	{
	  runReturn = RouteLocalMessage(message);
	}
      else
	//Check for a type function
	{
	  //	  runReturn = FindMessageFunction(message);
	  runReturn = RouteMessageType(message);
	}
    }
  return(runReturn);
}

DCThread * DCMessageProcessor::FindThread(std::string Name)
{
  DCThread * thread = NULL;
  //Search through all the threads in the thread vector for a thread
  //with name Name
  for(std::vector<DCThread*>::const_iterator it = Launcher->GetThreads().begin(); 
      it != Launcher->GetThreads().end();
      it++)
    {
      //check for the correct name
      if(Name.compare((*it)->GetName()) == 0)
	{
	  //copy the thread pointer
	  thread = *it;
	  //break from the loop
	  break;
	}
    }
  return(thread);
}

void DCMessageProcessor::InstallPrintErrorThread()
{
  printf("No error handler installed.  Installing default local thread\n");
  //Hardcoded config to start the basic local ErrorPrinter
  std::string setupString("<SETUP><THREADS><THREAD><TYPE>PrintErrors</TYPE><NAME>LocalErrorPrinter</NAME></THREAD></THREADS></SETUP>");
  DCMessage::Message message;
  message.SetData(setupString.c_str(),setupString.size());
  message.SetType(DCMessage::LAUNCH);
  ProcessMessage(message);
}

bool DCMessageProcessor::RouteRemoteMessage(DCMessage::Message message)
{
  bool runReturn = true;

  //Get the routing information
  std::string destinationName;
  struct in_addr destinationAddr;
  message.GetDestination(destinationName,
			 destinationAddr);

  //An iterator for the messageAddrToNameMap
  std::map< uint32_t , std::string >::iterator it;
  bool messageSent = false;
  //First search by destinationAddr
  for(it = messageAddrToNameMap.begin(); 
      it != messageAddrToNameMap.end();
      it++)
    {
      //If we have an address match
      //it->first is the uint32_t part of a struct in_addr	     
      if(it->first == destinationAddr.s_addr)	  	  
	{
	  DCThread * thread = FindThread(it->second);
	  if(thread != NULL)
	    {
	      recursiveCount = 0;
	      thread->SendMessageIn(message,true);
	      messageSent = true;
	    }
	  else
	    {		
	      //Check if we are in a recursive error loop
	      if(recursiveCount < maxRecursiveCount)
		{  
		  recursiveCount++;
		  DCMessage::Message errorMessage;
		  errorMessage.SetType(DCMessage::ERROR);
		  errorMessage.SetDestination();
		  errorMessage.SetSource("DCLauncher");
		  
		  DCMessage::SingleUINT64 errorData;
		  sprintf(errorData.text,
			  "Can not find routing thread %s.\n",
			  it->second.c_str());

		  errorMessage.SetDataStruct(errorData);
		  ProcessMessage(errorMessage);		  
		}
	      else
		{
		  recursiveCount = 0;
		  InstallPrintErrorThread();
		}
	    }
	  break;
	}
    }
  
  //Now look for a broadcast connection if we haven't sent the message yet
  if(!messageSent)
    {
      for(it = messageAddrToNameMap.begin(); 
	  it != messageAddrToNameMap.end();
	  it++)
	{
	  if(it->first == 0xFFFFFFFF)  //Broadcast address
	    {
	      DCThread * thread = FindThread(it->second);
	      if(thread != NULL)
		{
		  recursiveCount = 0;
		  thread->SendMessageIn(message,true);
		  messageSent = true;
		}
	      else
		{
		  //Check if we are in a recursive error loop
		  if(recursiveCount < maxRecursiveCount)
		    {
		      recursiveCount++;
		      DCMessage::Message errorMessage;
		      errorMessage.SetType(DCMessage::ERROR);
		      errorMessage.SetDestination();
		      errorMessage.SetSource("DCLauncher");
		      
		      DCMessage::SingleUINT64 errorData;
		      sprintf(errorData.text,
			      "Can not find routing thread  %s.\n",
			      it->second.c_str());
		      errorMessage.SetDataStruct(errorData);
		      ProcessMessage(errorMessage);  
		    }
		  else
		    {
		      recursiveCount = 0;
		      InstallPrintErrorThread();
		      
		    }
		}
	      break;
	    }
	}
    }
  
  //If the message could not be routed send an error
  if(!messageSent)
    {
      //Check if we are in a recursive error loop
      if(recursiveCount < maxRecursiveCount)
	{
	  recursiveCount++;
	  DCMessage::Message errorMessage;
	  errorMessage.SetType(DCMessage::ERROR);
	  errorMessage.SetDestination();
	  errorMessage.SetSource("DCLauncher");
	  std::string addressIP(INET_ADDRSTRLEN,' ');

	  DCMessage::SingleUINT64 errorData;
	  sprintf(errorData.text,"Can not route address %s:%s .\n",
		  destinationName.c_str(),
		  inet_ntop(AF_INET,
			    &destinationAddr,
			    &addressIP[0],
			    addressIP.size()));
	  
	  errorMessage.SetDataStruct(errorData);
	  ProcessMessage(errorMessage);  
	}
      else
	{
	  recursiveCount = 0;
	  InstallPrintErrorThread();
	  
	}
    }
  return(runReturn);
}

bool DCMessageProcessor::RouteLocalMessage(DCMessage::Message message)
{
  bool runReturn = true;

  //Get routing information
  std::string destinationName;
  struct in_addr destinationAddr;
  message.GetDestination(destinationName,
			 destinationAddr);

  //Find the routed to thread
  DCThread * thread = FindThread(destinationName);
  if(thread != NULL)
    {
      recursiveCount=0;
      //pass on the message
      thread->SendMessageIn(message,true);
    }
  else
    //Error: thread name not found
    {	   
      if(recursiveCount < maxRecursiveCount)
	{
	  recursiveCount++;
	  DCMessage::Message errorMessage;
	  errorMessage.SetType(DCMessage::ERROR);
	  errorMessage.SetDestination();
	  errorMessage.SetSource("DCLauncher");
	  
	  DCMessage::SingleUINT64 errorData;
	  sprintf(errorData.text,
		  "Can not find routed to thread %s\n",
		  destinationName.c_str());
	  errorMessage.SetDataStruct(errorData);
	  ProcessMessage(errorMessage);  	      	     
	}
      else
	{
	  recursiveCount = 0;
	  InstallPrintErrorThread();
	  
	}
    }
  return(runReturn);
}

bool DCMessageProcessor::RouteMessageType(DCMessage::Message message)
{
  bool runReturn = true;

  //=================================================================
  //Check for a type route to a thread
  //=================================================================

  //Iterator for type lookup.
  std::map<uint16_t,std::string>::iterator itMessageTypeToNameMap;
  //Search for a mapping between type ID and a thread name
  itMessageTypeToNameMap = messageTypeToNameMap.find(message.GetType());
  if(itMessageTypeToNameMap != messageTypeToNameMap.end())
    {
      DCThread * thread = FindThread(itMessageTypeToNameMap->second);
      if(thread != NULL)
	{
	  recursiveCount = 0;
	  thread->SendMessageIn(message,true);
	}
      else
	{		
	  //Check if we are in a recursive error loop
	  if(recursiveCount < maxRecursiveCount)
	    {  
	      recursiveCount++;
	      DCMessage::Message errorMessage;
	      errorMessage.SetType(DCMessage::ERROR);
	      errorMessage.SetDestination();
	      errorMessage.SetSource("DCLauncher");
	      
	      DCMessage::SingleUINT64 errorData;
	      sprintf(errorData.text,
		      "Can not find routing thread %s.\n",
		      itMessageTypeToNameMap->second.c_str());
	      
	      errorMessage.SetDataStruct(errorData);
	      ProcessMessage(errorMessage);	
	      ProcessMessage(message);
	    }
	  else
	    {
	      recursiveCount = 0;
	      InstallPrintErrorThread();
	    }
	}
    }
  else
    {
      //=================================================================
      //Check for a function mapped from a type
      //=================================================================
      
      //Iterator for out command lookup.
      std::map<uint16_t,void (DCMessageProcessor::*) (DCMessage::Message &,bool &)>::iterator itMessageTypeToFunctionMap;
      //Search for the correct command function for this message type.
      itMessageTypeToFunctionMap = messageTypeToFunctionMap.find(message.GetType());
      if(itMessageTypeToFunctionMap != messageTypeToFunctionMap.end())
	{
	  //Pass off the message, the thread that sent it and the DCDAQ loop
	  //running bool to the command function
	  (*this.*(itMessageTypeToFunctionMap->second))(message,
							runReturn);
	}
      else
	{
	  //Check if we are in a recursive error loop
	  if(recursiveCount < maxRecursiveCount)
	    {
	      recursiveCount++;
	      DCMessage::Message errorMessage;
	      errorMessage.SetType(DCMessage::ERROR);
	      errorMessage.SetDestination();
	      errorMessage.SetSource("DCLauncher");
	      
	      DCMessage::SingleUINT64 errorData;
	      sprintf(errorData.text,
		      "Can not find a function for ID= %u.\n",
		      message.GetType());
	      errorMessage.SetDataStruct(errorData);
	      ProcessMessage(errorMessage);  	      	     
	    }
	  else
	    {
	      recursiveCount = 0;
	      InstallPrintErrorThread();
	    }
	}
    }
  return(runReturn);
}

void DCMessageProcessor::SendRemoteSetupMessage(std::string setupText, unsigned int remoteNodeNumber)
{
  //Parse the configuration string into an XML structure
  xmlDoc * configXML = xmlReadMemory(setupText.c_str(),
				     (int) setupText.size(),
				     NULL,
				     NULL,
				     0);
  //Check that the text was correctly parsed into XML
  if(configXML != NULL)
    {
      //Get the base node of the xml
      xmlNode * baseNode = xmlDocGetRootElement(configXML);
      //Go to the remoteNOdeNumberth RemoteLaunch node
      baseNode = FindIthSubNode(baseNode,"RemoteLaunch",remoteNodeNumber);
      

      //Get the routing information we need for the DCMessage
      std::string destinationName;
      struct in_addr destinationAddr;
      std::string destinationAddress;
      //open XML node with the AF and IPish entries
      xmlNode * destinationNode = FindSubNode(baseNode,"DESTINATION");
            
      //Read in the IP address like value (fail if not found)
      if(FindSubNode(destinationNode,"ADDRESS") == NULL) {return;}
      else
	{
	  GetXMLValue(destinationNode,"ADDRESS",GetName().c_str(),destinationAddress);
	}
      
      //Fill the destinationAddr withe the data from destiantionAddress
      inet_pton(AF_INET,destinationAddress.c_str(),&destinationAddr);
      
      //Find the DCDAQ thread/mm setup string
      if(FindSubNode(baseNode,"SETUP") == NULL) {return;}
      else
	{
	  baseNode = FindSubNode(baseNode,"SETUP");
	}
      
     
      //Build a new XML document containing only the SETUP node and put it in a string
      xmlDoc * newDoc = xmlNewDoc(BAD_CAST "1.0");
      xmlDocSetRootElement(newDoc,baseNode);  
      xmlChar * launchXMLText;
      int launchXMLTextSize;
      xmlDocDumpFormatMemory(newDoc, &launchXMLText, &launchXMLTextSize, 1);
      
      //BUild the DCMessage
      DCMessage::Message message;
      message.SetData(launchXMLText,launchXMLTextSize);
      message.SetType(DCMessage::LAUNCH);
      message.SetDestination(std::string(""),
			     destinationAddr);
      message.SetSource("DCMessageProcessor");
      ProcessMessage(message);
      
      //Free the XMl data
      free(launchXMLText);
      
      
    }
  else
    {					       
      printf("WTF mate?\n");
    }
}
