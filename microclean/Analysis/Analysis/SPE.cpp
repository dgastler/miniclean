#include "SPEExpo.h"

SPEExpo::SPEExpo(char * filename)
{
  Set(filename);
}
SPEExpo::Set(char *filename)
{
  XMLNode xMainNode=XMLNode::openFileHelper(filename,"SPE");
  // handle error
  m = atof(xMainNode.GetChildNode("Expo").GetAttribute("m"));
  b = atof(xMainNode.GetChildNode("Expo").GetAttribute("b"));
}
