#include <netHelper.h>

int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint16_t size,
	       struct timeval timeout)
{
  int32_t ret = 0;

  //From Unix Network Programming: Stevens.
  uint8_t * writePtr = ptr;
  int32_t nLeft = size;
  
  int32_t nWritten = 0;
  
  while(nLeft >0)
    {
      if( (nWritten = write(socketFD,writePtr,nLeft)) <=0)
	{
	  if(errno == EINTR)
	    {
	      nWritten = 0;
	    }
	  else if(errno == EPIPE)
	    {
	      ret = -2;
	      break;
	    }
	  else
	    {
	      printf("Socket write error(%d): %s\n",errno,strerror(errno));
	      ret = -1;
	      break;
	    }
	}
      nLeft -= nWritten;
      //      ptr += nWritten;
      writePtr += nWritten;
    }
  if(ret >= 0)
    ret = size;
  return(ret);
}

int32_t readN(int socketFD,
	       uint8_t * ptr,
	       uint16_t maxSize,
	       struct timeval timeout)
{
   int32_t ret = 0;

   //From Unix Network Programming: Stevens.
   uint8_t * readPtr = ptr;
   int32_t nLeft = maxSize;
   
   int32_t nRead = 0;
   
   while(nLeft >0)
     {
       if( (nRead = read(socketFD,readPtr,nLeft)) < 0)
	 {
	   if(errno == EINTR)
	     {
	       nRead = 0;
	     }
	   else if(errno == EPIPE)
	     {
	       ret = -2;
	       break;
	     }
	   else
	     {
	       ret = -1;
	       break;
	     }
	 }
       else if (nRead == 0)
	 {
	   break;
	 }
       nLeft -= nRead;
       //       ptr += nRead;
       readPtr += nRead;
     }
   
   if(ret >= 0)
     ret = maxSize - nLeft;   
   
   return (ret);
}

bool CheckLocal(std::string IP,int fd)
{
  bool ret = false;
  //Structure that stores all of our connections
  struct ifconf ifc;
  char * buffer;
  int bufferSize;
  int lastBufferSize;
  bufferSize = 10*sizeof(struct ifreq);
  lastBufferSize = 0;
  
  //Find out how big to make the buffer in ifc
  while(true)
    {
      //Allocate room for the ifreq objects in ifconf object
      buffer = new char[bufferSize];
      ifc.ifc_len = bufferSize;
      ifc.ifc_ifcu.ifcu_buf = buffer;
      
      if(ioctl(fd,SIOCGIFCONF,&ifc,sizeof(struct ifconf)) < 0)
	{
	  //ioctl error with SIOCGIFCONF
	  fprintf(stderr,"Error in SIOCGIFCONF\n");
	  delete [] buffer;
	  buffer = NULL;
	  break;
	}
      else
	{
	  if(ifc.ifc_len == lastBufferSize)
	    {
	      //Our buffer is big enough.
	      break;
	    }
	  else
	    {
	      //Record the current buffer size for the next loop.
	      lastBufferSize = ifc.ifc_len;
	      bufferSize = bufferSize << 2;		      
	    }
	}            
      //Clean up our buffer (doesn't happen when we sucessfully find the size)
      delete [] buffer;
    }//Finding buffer size and getting data.
  


  //At this point we either have valid data in buffer or it is NULL and things have failed.
  if(buffer)
    {
      unsigned int textBufferSize = INET6_ADDRSTRLEN; //size for ipv6, ipv4 is smaller.
      char * textBuffer = new char[textBufferSize];
      //Loop over the returned objects and see if our IP address is there.
      int size = (ifc.ifc_len)/(sizeof(struct ifreq));  //Number of ifc_req structs
      for(int i = 0; i < size; i++)
	{
	  //Check the current ifc_req object's IP address is equal to IP
	  struct sockaddr_in * addr_ptr = (struct sockaddr_in *) &(ifc.ifc_ifcu.ifcu_req[i].ifr_ifru.ifru_addr);
	  inet_ntop(addr_ptr->sin_family,&(addr_ptr->sin_addr),textBuffer,textBufferSize);
	  if(IP.compare(textBuffer) == 0)
	    {
	      //We found the IP we were looking for. BREAK!
	      ret = true;
	      break;
	    }
	}
      //clean up.
      delete [] buffer;
      delete [] textBuffer;
    }//Analyzing the returned data
  
  return(ret);
}

bool AddrCompare(int AF,std::vector<uint8_t> addr1,std::vector<uint8_t> addr2)
{
  bool ret = true;
  if(AF == AF_INET)
    {
      //Pointers to (AF_INET) typesockaddr
      struct sockaddr_in * sockaddr1;
      struct sockaddr_in * sockaddr2;
      //check that the sizes are correct for this type
      if((addr1.size() != sizeof(struct sockaddr_in)) ||
	 (addr2.size() != sizeof(struct sockaddr_in)))
	{
	  ret = false;
	}
      else
	{
	  //If the sizes are correct, then recast them to our pointers
	  sockaddr1 = (struct sockaddr_in *) &(addr1[0]);
	  sockaddr2 = (struct sockaddr_in *) &(addr2[0]);
	  //Check that both addr have the correcty type
	  if((sockaddr1->sin_family != AF_INET) ||
	     (sockaddr2->sin_family != AF_INET))
	    {
	      ret = false;
	    }
	  //Check that both addrs use the same port
	  if(sockaddr1->sin_port != sockaddr2->sin_port)
	    {
	      ret = false;
	    }
	  //Check that both addrs have the same address
	  if(sockaddr1->sin_addr.s_addr != sockaddr2->sin_addr.s_addr)
	    {
	      ret = false;
	    }
	}
    }
  else if(AF == AF_INET6)
    {
      //Pointers to (AF_INET6) typesockaddr
      struct sockaddr_in6 * sockaddr1;
      struct sockaddr_in6 * sockaddr2;
      //check that the sizes are correct for this type
      if((addr1.size() != sizeof(struct sockaddr_in6)) ||
	 (addr2.size() != sizeof(struct sockaddr_in6)))
	{
	  ret = false;
	}
      else
	{
	  //If the sizes are correct, then recast them to our pointers
	  sockaddr1 = (struct sockaddr_in6 *) &(addr1[0]);
	  sockaddr2 = (struct sockaddr_in6 *) &(addr2[0]);
	  //Check that both addr have the correcty type
	  if((sockaddr1->sin6_family != AF_INET6) ||
	     (sockaddr2->sin6_family != AF_INET6))
	    {
	      ret = false;
	    }
	  //Check that both addrs use the same port
	  if(sockaddr1->sin6_port != sockaddr2->sin6_port)
	    {
	      ret = false;
	    }
	  //Check that both addrs have the same address
	  if(memcmp(sockaddr1->sin6_addr.s6_addr,
		    sockaddr2->sin6_addr.s6_addr,
		    sizeof(sockaddr1->sin6_addr.s6_addr)) == 0)
	
	    {
	      ret = false;
	    }
	}
    }
  else if(AF == AF_LOCAL)
    {
      //Pointers to (AF_LOCAL) typesockaddr
      struct sockaddr_un * sockaddr1;
      struct sockaddr_un * sockaddr2;
      //check that the sizes are correct for this type
      if((addr1.size() != sizeof(struct sockaddr_un)) ||
	 (addr2.size() != sizeof(struct sockaddr_un)))
	{
	  ret = false;
	}
      else
	{
	  //If the sizes are correct, then recast them to our pointers
	  sockaddr1 = (sockaddr_un *) &(addr1[0]);
	  sockaddr2 = (sockaddr_un *) &(addr2[0]);
	  //Check that both addr have the correcty type
	  if((sockaddr1->sun_family != AF_LOCAL) ||
	     (sockaddr2->sun_family != AF_LOCAL))
	    {
	      ret = false;
	    }
	  //Check that the path size is the same for both
	  if(SUN_LEN(sockaddr1) != SUN_LEN(sockaddr2))
	    {
	      ret = false;
	    }
	  if(memcmp(sockaddr1->sun_path,
		    sockaddr2->sun_path,
		    SUN_LEN(sockaddr1) - sizeof(sockaddr1->sun_family)) != 0)
	    {
	      ret = false;
	    }      
	}
    }
  else
    {
      ret = false;
    }
  return(ret);
}

//Start a asyncronous socket connect
//true if connection works instantly. 
//false on error or connection in progress.
//if false and fd == -1, then you have a problem. 
//if false and fd >= 0, then you are waiting for the connection to finish
bool AsyncConnectStart(int & fd,int AF,std::vector<uint8_t> &addr,int type, int protocol)
{
  int badFD = -1;
  //Open a socket
  fd = socket(AF,type,protocol);
  if(fd < 0)
    {
      return(false);
    }
  //Set socket to non-blocking
  if(!SetNonBlocking(fd,true))
    {
      close(fd);
      fd = badFD;
      return(false);      
    }

  //fd is now non-blocking
  int connectReturn = connect(fd,
			      (sockaddr *) &(addr[0]),
			      addr.size());
  //The connection happened in the blink of a transistor's eye.
  if(connectReturn == 0)
    {
      //Turn off non-blocking
      //(fail if it didn't work)
      if(!SetNonBlocking(fd,false))
	{
	  close(fd);
	  fd = badFD;
	  return(false);      
	}

      //return that everything worked as well as possible
      return(true);
    }
  else if(errno == EINPROGRESS)
    {
      //We are waiting for the connection to finish
      //You should go wait in select for read/write access to fd!
      return(false);
    }
  
  //An error occured
  close(fd);
  fd = badFD;
  return(false);
}

//Check that our newly connected socket works. 
bool AsyncConnectCheck(int & fd)
{
  int badFD = -1;
  //Check the socket option (SO_ERROR)
  int val_SO_ERROR;
  socklen_t val_SO_ERROR_Size = sizeof(val_SO_ERROR);
  int getSockOptReturn = getsockopt(fd,SOL_SOCKET,SO_ERROR,
				    &val_SO_ERROR,&val_SO_ERROR_Size);
  //make sure getsockop didn't fail
  if(getSockOptReturn != 0)
    {
      close(fd);
      fd=badFD;
      return(false);
    }

  //Check if connect actually connected
  //Reset blocking mode if we got a connections
  if(val_SO_ERROR == 0)
    {
      //Turn off non-blocking mode
      //(fail if it didn't work)
      if(!SetNonBlocking(fd,false))
	{
	  close(fd);
	  fd = badFD;
	  return(false);      
	}
      //Yay!
      return(true);
    }
  //There was an error, so close the socket are 
  //start another search
  close(fd);
  fd = badFD;
  return(false);
}

bool SetNonBlocking(int &fd,bool value)
{
  //Get the previous flags
  int currentFlags = fcntl(fd,F_GETFL,0);
  if(currentFlags < 0)
    {
      return(false);
    }
  //Make the socket non-blocking
  if(value)
    {
      currentFlags |= O_NONBLOCK;
    }
  else
    {
      currentFlags &= ~O_NONBLOCK;
    }

  int currentFlags2 = fcntl(fd,F_SETFL,currentFlags);
  if(currentFlags2 < 0)
    {
      return(false);
    }
  return(true);
}
