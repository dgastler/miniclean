#ifndef __RATCopyQueue__
#define __RATCopyQueue__

#include <RAT/DS/DCDAQ_MemBlocks.hh>
#include <MemoryManager.h>
#include <DCQueue.h>
#include <xmlHelper.h>
#include <RAT/DS/Root.hh>
#include <RAT/DS/DCDAQ_DQMStructs.hh>


class RATCopyQueue : public DCQueue<EventToPorts *>, public MemoryManager
{


public:
  RATCopyQueue(std::string Name);
  ~RATCopyQueue();
  bool AllocateMemory(xmlNode * setupNode);

  virtual void Shutdown(){ShutdownWait();WakeUp();ShutdownStatus=true;};

  virtual bool IsShutdown(){return(ShutdownStatus);};
  virtual void WakeUp(){DCQueue<EventToPorts *>::WakeUp();};

  virtual void PrintStatus(int indent = 0, int verbosity = 0);

  virtual bool Add(EventToPorts* ev);

  bool AddEntrySocket(uint16_t port,zmqInfo * socketInfo,bool Wait = true);
  bool RemoveEntrySocket(uint16_t port,bool Wait = true);
  bool RemoveEntryFilter(uint16_t port,size_t filterNum,bool Wait = true);
  bool EditEntrySocket(uint16_t port,size_t filterNum,StreamFilter * filter ,bool Wait = true);
  bool PauseEntrySocket(uint16_t port,bool Wait = true);
  bool GoEntrySocket(uint16_t port,bool Wait = true);

  bool CheckEntriesSocket(RAT::DS::EV * ev,bool Wait = false);
    
private:
  volatile bool ShutdownStatus;
  unsigned int maxSize;

  typedef std::vector<zmqInfo *> zmqInfoSet_t;
  typedef std::map<uint16_t,zmqInfoSet_t> zmqInfoSetMap_t;

  zmqInfoSetMap_t zmqSocketMap;

  time_t currentTime;
  LockedObject Lock;
};

#endif
