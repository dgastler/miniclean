#ifndef CL_HELPER
#define CL_HELPER

#include <string>
#include <Control.h>

void Callback(const char *);
void SetObjectPtr(Control *);
void ResetObjectPtr();
bool IsSet();

#endif
