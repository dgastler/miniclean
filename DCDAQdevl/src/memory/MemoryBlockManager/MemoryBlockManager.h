#ifndef __MEMORYBLOCKMANAGER__
#define __MEMORYBLOCKMANAGER__

#include <MemBlocks.h>
#include <MemoryManager.h>
#include <FreeFullQueue.h> 
#include <xmlHelper.h>

class MemoryBlockManager : public FreeFullQueue, public MemoryManager 
{
 public:
  MemoryBlockManager(std::string Name);
  ~MemoryBlockManager();
  bool AllocateMemory(xmlNode * setupNode);

  virtual void Shutdown(){ShutdownQueues();};
  virtual void WakeUp(){WakeUpQueues();};

  virtual void PrintStatus(int indent = 0);

  std::vector<MemoryBlock> Memory;
 private:
  void Deallocate();
};

#endif
