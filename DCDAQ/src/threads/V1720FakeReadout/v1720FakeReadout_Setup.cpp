#include <v1720FakeReadout.h>

bool v1720FakeReadout::Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager)
{
  //Find the memoryManager we need
  std::string memoryManagerName;
  GetXMLValue(SetupNode,"MEMORYMANAGER","THREAD",memoryManagerName);
  Mem = (MemoryBlockManager *) (MemManager[memoryManagerName]);

  //Get the Events per block.
  if(FindSubNode(SetupNode,"EVENTSPERBLOCK") == NULL)
    {
      //Default if we can't find value
      PrintWarning("Using default events per block");
      EventsPerBlock = 128;
    }
  else
    GetXMLValue(SetupNode,"EVENTSPERBLOCK",GetName().c_str(),EventsPerBlock);

  //Get the wait time between events.
  if(FindSubNode(SetupNode,"EVENTPERIOD") == NULL)
    {
      Period = 0;
    }
  else
    GetXMLValue(SetupNode,"EVENTPERIOD",GetName().c_str(),Period);
  //Set the actual wait period, taking into account EventsPerBlock
  //SetPeriod(Period);

  
  //Find out if we should loop over the events or end after one loop
  int32_t _MemoryLoop = 1;
  if(FindSubNode(SetupNode,"MEMORYLOOP") == NULL)
    {
      //Default value
      MemoryLoop = true;
    }
  else
    {
      GetXMLValue(SetupNode,"MEMORYLOOP",GetName().c_str(),_MemoryLoop);
      MemoryLoop = _MemoryLoop;
    }

  //Get the input file name
  if(FindSubNode(SetupNode,"FILENAME") == NULL)
    {
      PrintError("FILENAME subnode not found");
      return false;
    }
  else    
    GetXMLValue(SetupNode,"FILENAME",GetName().c_str(),InFileName);
  InFile = fopen(InFileName.c_str(),"rb");
  if(!InFile)
    {
      printf("Error: %s not found.\n",InFileName.c_str());
      return false;
    }

  //Build the read file's full path.
  std::string fullPath;
  if(!std::getenv("PWD")){return false;};
  fullPath.assign(std::getenv("PWD"));
  fullPath+="/";
  fullPath+=InFileName;
  //Get file stats.
  struct stat fileStats;
  int statErr = stat(fullPath.c_str(),&fileStats);
  if(statErr){return false;}

  //Get the file's size in bytes and divide by four to get uint32_t size.
  //Allocate enough memory to hold the whole file in memory.
  FakeWFD = new uint32_t[fileStats.st_size >> 2];
  if(!FakeWFD) {return false;}
  FakeWFDSize = fileStats.st_size >>2;
  //read in the file.
  uint32_t  readNumber = fread((uint8_t*)FakeWFD,sizeof(uint8_t),FakeWFDSize <<2,InFile);
  if(readNumber != (FakeWFDSize <<2)){return false;}

  //Find events.
  for(uint32_t iWord = 0; iWord < FakeWFDSize;)
    {
      //Check for event header
      if((FakeWFD[iWord]&0xF0000000) == 0xA0000000) 
	{
	  //Store pointer to this event.
	  FakeEvent.push_back(FakeWFD + iWord);
	  //Move forward by even size.
	  iWord += (FakeWFD[iWord]&0x0FFFFFFF);
	}
      else
	{
	  iWord++;
	}
    }
# if __WORDSIZE == 64
  printf("v1720FakeReadout: Loaded %lu events from file %s.\n",FakeEvent.size(),fullPath.c_str());
# else
  printf("v1720FakeReadout: Loaded %u events from file %s.\n",FakeEvent.size(),fullPath.c_str());
#endif
  Ready = true;

  //We no longer need fInFile open, so let's close it now.
  fclose(InFile);
  //Reset statistics
  BlocksRead = 0;
  TimeStep_BlocksRead= 0;
  lastTime = time(NULL);  

  //Set the current FAKE event index to zero
  iEvent = 0;

  return(true);
}

