#ifndef __EVNT_ZLE_INTEGRAL__
#define __EVNT_ZLE_INTEGRAL__

#include <PacketBuilder.h>

class EVNT_ZLE_INTEGRAL : public PacketBuilder
{
 public:
  EVNT_ZLE_INTEGRAL()
  {
    ClearPacket();
  };
  virtual ~EVNT_ZLE_INTEGRAL(){ClearPacket();};
  virtual bool Send(FEPCBlock * event)
  {
    printf("EVNT_ZLE_INTEGRAL not written.  FAIL!\n");
    return(false);
  }

};

#endif
