#include <v1720FakeReadout.h>

v1720FakeReadout::v1720FakeReadout(std::string Name)
{
  SetName(Name);
  SetType(std::string("v1720FakeReadout"));
  FreeFDs[0] = BadPipeFD;
  FreeFDs[1] = BadPipeFD;
  blockIndex = FreeFullQueue::BADVALUE;
  block = NULL;
  Mem = NULL;
}

v1720FakeReadout::~v1720FakeReadout()
{
  //clear pointer vector
  FakeEvent.clear();
  if(FakeWFD)
    {
      delete [] FakeWFD;
    }
  Mem->Shutdown();
}

void v1720FakeReadout::SetPeriod(useconds_t _Period)
{
  Period = _Period;
  SetSelectTimeout(
		   ((Period*EventsPerBlock)/1000000),  //Seconds
		   ((Period*EventsPerBlock)%1000000)   //useconds
		   );
}

bool v1720FakeReadout::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCMessage::DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
      RemoveReadFD(FreeFDs[0]);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCMessage::DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the free queue's FD so we know when there are

      Mem->GetFreeFDs(FreeFDs);
      AddReadFD(FreeFDs[0]);
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}
