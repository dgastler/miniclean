#ifndef __DCVECTORHASH__
#define __DCVECTORHASH__

#include <pthread.h>

#include <LockedObject.h>
#include <NotLockedObject.h>

#include <vector>

#include <StackTracer.h>

namespace returnDCVectorHash
{
  //Return value
  enum 
    {
      COLLISION       = 0,
      OK              = -1,
      EMPTY_KEY       = -2,
      BAD_PASSWORD    = -3,
      BAD_MUTEX       = -4,
      EXTERNAL        = -5,
      NO_FREE         = -6,
      OTHER           = -7
    };
}

//==========================================================================
//==========================================================================
//DCVectorHashEntry
//==========================================================================
//==========================================================================
//A structure to store the contents of the vector hash
template<class T>
class DCVectorHashEntry
{
public:
  DCVectorHashEntry(T _Obj_default,int64_t _key_default,uint64_t _blank_password)
  {
    Obj_default = _Obj_default;
    key_default = _key_default;
    blank_password = _blank_password;
    Clear();
  }
  DCVectorHashEntry(const DCVectorHashEntry & rhs){Copy(rhs);}
  DCVectorHashEntry & operator=(const DCVectorHashEntry & rhs){Copy(rhs);return(*this);}
  T Obj;   //Object being stored
  int64_t key;  //Key used to set this object
  uint64_t password;  //the password for this key
  int64_t passwordBlocks;  //number of times the password has stopped someone. (reset on clearing of password)
  void ClearPassword()
  {
    password = blank_password;
    passwordBlocks = 0;
  }
  bool ExistPassword(){return(password != blank_password);}
  void Clear()
  {
    Obj = Obj_default;
    key = key_default;
    ClearPassword();
  };
  void Copy(const DCVectorHashEntry & rhs)
 {
    Obj  = rhs.Obj;
    key  = rhs.key;
    password = rhs.password;
    passwordBlocks = rhs.passwordBlocks;
      
    Obj_default    = rhs.Obj_default;
    key_default    = rhs.key_default;
    blank_password = rhs.blank_password;
  }
private:
  T Obj_default;
  uint64_t key_default;
  uint64_t blank_password;
};



































//==========================================================================
//==========================================================================
//DCVectorHash
//==========================================================================
//==========================================================================

template<class T ,
	 class ObjectModel = LockedObject>
class DCVectorHash
{
public:
  //================================================================
  //Setup
  //================================================================
  //Setup(size,emptyValue,badValue):
  //  Input:
  //    size:       sets the size of the entry vector
  //    emptyValue: what will be returned for an empty slot 
  //    badValue:   or what will be returned if there is an error (badValue)
  //
  //================================================================
  DCVectorHash()
  {
    blank_password = 0;
  }
  ~DCVectorHash()
  {
    Clear();
  };
  void ShutdownWait()
  {
    Lock.ShutdownLockWait();
  }

  void WakeUp()
  {
    Lock.WakeUpSignal();
  }
  void Setup(uint32_t size, T _emptyValue, T _badValue) 
  {
    if(!Lock.LockObject())
      {
	return;
      }

    //Set return value for no data.
    badValue = _badValue;
    //Set return value for locked data.
    emptyValue = _emptyValue;
    //Allocate a vector of entries
    DCVectorHashEntry<T> _entry(emptyValue,	  //empty value
				returnDCVectorHash::EMPTY_KEY,     //empty last key
				blank_password);    //blank password value
    //Clear the entry vector before we fill it. 
    entry.clear();
    for(uint32_t i = 0; i < size;i++)
      {	
	entry.push_back(_entry);
      }
    
    //Zero the active entries
    ActiveEntries = 0;
    
    //Unlock
    Lock.UnlockObject();
  };

  //================================================================
  //Special value functions
  //================================================================
  //Size():            Returns vector size
  //
  //================================================================
  size_t GetSize(){return(entry.size());};
  const T GetEmptyValue(){return(emptyValue);};
  const T GetBadValue(){return(badValue);};
  size_t GetActiveEntryCount() {return ActiveEntries;};
  uint64_t GetBlankPassword(){return blank_password;};
  int64_t GetPasswordBlocks(int64_t key)
  {
    //Since this is just a read, we won't use a mutex.
    //Always assume that this value could change while you get it. 
    //Get the hash value for our key
    uint32_t hash = HashFunction(key);    
    int64_t ret = entry[hash].passwordBlocks;    
    return(ret);   
  }


  //================================================================
  //Public Interfaces
  //================================================================
  //GetEntry(key, &Entry, &password, wait):  
  //  Input:
  //    key(int64_t): The value that is put through the hash function 
  //                   to determine which vector entry to use.
  //                  This is compared to the current stored key to
  //                   catch hash-collisions. 
  //  Output:
  //    return(int64_t): OK        (-1): everything returned well
  //                     EMPTY_KEY (-2): if the Entry at the key was empty   
  //                     BAD_PASSWORD (-3): if the Entry was locked and you 
  //                                         didn't supply the correct password
  //                     BAD_MUTEX (-4): if the mutex didn't lock 
  //                                      (only when wait = false) 
  //                     >=0           : if this key's entry already has data, 
  //                                     it will return the previously used key
  //                                     This allows us to understand what 
  //                                     collisions may have happened. 
  //           
  //    Entry(Type):   If return is EMPTY_KEY, Type will be "emptyValue" 
  //                    (entry is now locked! password set)
  //                   If return is BAD_PASSWORD, Type will be "badValue"
  //                   If return is BAD_MUTEX, Type will be "badValue"
  //                   If return is >=0, this will return the Entry associated 
  //                    with the (old) key. (entry is now locked! password set)
  //                   If return is OK, this will return the Entry associated 
  //                    with key. (entry is now locked! password set)
  //                    
  //  In/Out:
  //    Password(unsigned int): As an input this sets the password you need to 
  //                             access the entry if you are not unlocking 
  //                             something the input isn't used. 
  //                            As an output it is set to the lock password if 
  //                             you checked out an unlocked Entry.  
  //
  //UpdateEntry(key, &Entry, &password, wait):  
  //  Input:
  //    key(int64_t): The value that is put through the hash function
  //                   to determine which vector entry to use.
  //                  This is compared to the current stored key to
  //                   catch hash-collisions. 
  //    Password(unsigned int): This is the password you need to access 
  //                             the entry if you are not unlocking something 
  //                             the input isn't used. 
  //
  //  Output:
  //    return(int64_t): OK           (-1): everything returned well
  //                     BAD_PASSWORD (-3): if the Entry was locked and you 
  //                                        didn't supply the correct password
  //                     BAD_MUTEX    (-4): if the mutex didn't lock 
  //                                        (only when wait = false) 
  //           
  //  In/Out:
  //    Entry(Type): As an input this is a reference to the Type you want to 
  //                 put into Entry
  //                 As an output it's set to badValue on return if Put worked.
  //                 It is unchanged if unlock failed.   
  // 
  //ClearEntry(key, password, wait):  
  //  Input: 
  //    key(uint64_t) Key to clear entry for
  //    password      Password to clear, if entry is locked. 
  //  Return:
  //    return(int64_t): OK        (-1): everything returned well
  //                     BAD_PASSWORD    (-3): if Entry was locked + you didn't
  //                                           supply the correct password
  //                     BAD_MUTEX (-4): if the mutex didn't lock  
  //                                     (only when wait = false) 
  //             
  //
  //================================================================
  int64_t GetEntry(int64_t key, T & _entry,uint64_t &password, bool wait = true)
  {    
    if(!Lock.LockObject(wait))
      {
	_entry = badValue;
	return(returnDCVectorHash::BAD_MUTEX);
      }    
    //We now have the mutex!    
    int64_t ret = returnDCVectorHash::OK;

    //Get the entry at key if password is correct
    ret = _Get(key,_entry,password);    
    Lock.UnlockObject();     
    return(ret);
  };
  
  int64_t UpdateEntry(uint64_t key, T & _entry, uint64_t password, bool wait=true)
  {
    if(!Lock.LockObject(wait))
      {
	return(returnDCVectorHash::BAD_MUTEX);
      }

    //Return value
    int64_t ret = returnDCVectorHash::OK;
    ret = _Update(key,_entry,password);
    Lock.UnlockObject();
    return(ret);
  }

  int64_t ClearEntry(int64_t key, uint64_t password,bool wait=true)
  {
    if(!Lock.LockObject(wait))
      {
	return(returnDCVectorHash::BAD_MUTEX);
      }

    //Return value
    int64_t ret = returnDCVectorHash::OK;

    ret = _Update(key,emptyValue,password);
    
    Lock.UnlockObject();

    return(ret);
  };
  //================================================================
  //Public Interfaces (special)
  //================================================================
  //ClearPassword(key, password, wait)
  //  Input:
  //    key(int64_t): The value that is put through the hash function             
  //                   to determine which vector entry to use.
  //                  This is compared to the current stored key to
  //                   catch hash-collisions. 
  //    Password(unsigned int): This is the password you want need to access the entry
  //                             if you are not unlocking something the input isn't used. 
  //  Output:
  //    return(int64_t):  OK if unlock worked (key now unlocked)
  //                      BAD_PASSWORD if we gave the wrong password
  //                      BAD_MUTEX if we couldn't get the mutex lock
  //                      >= 0 if the lock is from another key.  (no unlock!)
  //
  //================================================================
  int64_t ForceGet(int64_t key, T & _entry,uint64_t &password,bool wait = true)  
  {
    if(!Lock.LockObject(wait))
      {
	_entry = badValue;
	return(returnDCVectorHash::BAD_MUTEX);
      }    
    //We now have the mutex!    
    int64_t ret = returnDCVectorHash::OK;

    password = _GetPassword(key);
    ret = _Get(key,_entry,password);    
    Lock.UnlockObject();    
    return(ret);
  }
    
  int64_t ClearPassword(int64_t key, uint64_t password,bool wait=true)
  {
    if(!Lock.LockObject(wait))
      {
	return(returnDCVectorHash::BAD_MUTEX);
      }

    int64_t ret = returnDCVectorHash::OK;

    //We now have the mutex!
    ret = _ClearPassword(key,password);
    Lock.UnlockObject();
    return(ret);
  }

  //================================================================
  //Public Interfaces (special)
  //================================================================
  //Clear():           Clears all data from vector and removes all locks.
  //
  //================================================================
  bool Clear(bool wait = true)
  {
    if(!Lock.LockObject(wait))
      {
	return(false);
      }

    //We have the mutex

    
    for(uint32_t i = 0; i < entry.size();i++)
      {
	if(entry[i].key != returnDCVectorHash::EMPTY_KEY)
	  ActiveEntries--;
	//Reset entry to empty
	entry[i].Clear();

      }
    
    Lock.UnlockObject();
    return(true);
  };
  

private:

  //================================================================
  //Private Interfaces to passwords
  //================================================================
  uint64_t _GetPassword(int64_t key)
  {
    //Get the hash value for our key
    uint32_t hash = HashFunction(key);
    //Return the password
    return(entry[hash].password);    
  }
  int64_t _ClearPassword(int64_t key, uint64_t password)
  {
    //Get the hash value for our key
    uint32_t hash = HashFunction(key);
    //Check if the object is password protected
    int64_t ret = _CheckPassword(hash,password);
    if(ret != returnDCVectorHash::BAD_PASSWORD)
      {
	if(key == entry[hash].key)
	  {
	    //Clear the password the entry
	    entry[hash].ClearPassword();
	    //reset the password block count
	    entry[hash].passwordBlocks = 0;
	    ret = returnDCVectorHash::OK;
	  }
	else
	  {
	    ret = entry[hash].key;
	  }	
      }
    return(ret);
  }
  //Standard way to check if there is a password on the entry corresponding to key.
  //You MUST have the mutex locked when you are doing this. 
  int64_t _CheckPassword(int32_t hash,uint64_t password)
  {
    int64_t ret = returnDCVectorHash::OK;
    //Check if the object is password locked
    if(entry[hash].ExistPassword() && !Lock.GetShutdown())
      {
	//Check if password unlocks
	if(password != entry[hash].password)
	  {
	    //password didn't check out, so we should return BAD_PASSWORD
	    ret = returnDCVectorHash::BAD_PASSWORD;
//	    fprintf(stderr,"We got a password block: %lu %u %ld\n",
//		    entry[hash].key,
//		    uint32_t(entry[hash].Obj),
//		    entry[hash].passwordBlocks);
	    //Increase the number of times the lock blocked use. 
	    entry[hash].passwordBlocks++;
	  }
      }  
    return(ret);
  };
  //================================================================
  //Private Interfaces to entries
  //================================================================
  int64_t _Update(uint64_t key, T & _entry, uint64_t password)
  {
    //Get the hash value for our key
    uint32_t hash = HashFunction(key);
    //Check if the object is locked
    int64_t ret = _CheckPassword(hash,password);
    
    //Check if things are still ok (will be BAD_PASSWORD if we had a password problem)
    if(ret == returnDCVectorHash::OK)
      {
	if(entry[hash].Obj == emptyValue && 
	   _entry != emptyValue)
	  {
	    ActiveEntries++;	    
	  }
	//Copy in new entry
	entry[hash].Obj = _entry;
	if(_entry == emptyValue)
	  {
	    ActiveEntries--;
	    //	    entry[hash].key = returnDCVectorHash::EMPTY_KEY;
	    entry[hash].Clear();
	  }
	else if(_entry != badValue)
	  {
	    entry[hash].key = key;
	  }	
	else
	  {
	    fprintf(stderr,"badValue in DCVectorHash\n");
	    entry[hash].key = key;
	  }
      }
    return(ret);
  }

  int64_t _Get(int64_t key, T & _entry,uint64_t &password)
  {
    //Get the hash value for our key
    uint32_t hash = HashFunction(key);
    //Check if the object is locked
    int64_t ret = _CheckPassword(hash,password);
    if(ret == returnDCVectorHash::BAD_PASSWORD)
      {
	_entry = badValue;
      }
    
    //Check if things are still ok (will be BAD_PASSWORD if we had a password problem)
    if(ret == returnDCVectorHash::OK)
      {
	//There are three possiblities now 
	//EMPTY_KEY, returnDCVectorHash::OK, and a collision

	//return the entry
	_entry = entry[hash].Obj;
	//set the entries password
	entry[hash].password = PasswordFunction();
	password=entry[hash].password;	  
	
	//set our return value
	if(_entry == emptyValue)
	  {
	    //We have an empty element
	    //Set the key to the current key
	    entry[hash].key = key;
	    //set return value
	    ret = returnDCVectorHash::EMPTY_KEY;
	  }
	else if(key != entry[hash].key)
	  {
	    //set return value to previous key
	    ret = entry[hash].key;
	  }
	//else
	//  ret already OK
	//  key already correct
      }     
    return(ret);
  }


  //================================================================
  //Special number functions
  //================================================================  
  //Hash functions used to turn an integer into a hash integer.
  uint32_t HashFunction(int64_t i){return(uint32_t(i%entry.size()));};
  //Password function (simple, just for safety)
  uint64_t PasswordFunction() 
  {
    //Check that the new pw isn't our notLocked value
    if((++lastPW) == blank_password)
      {
	++lastPW;
      }
    return(lastPW);
  };

  //================================================================
  //Data members
  //================================================================  
  //Last password used.
  uint64_t lastPW;  
  //blank password value
  uint64_t blank_password;
  
  //Current count of active entries
  uint32_t ActiveEntries;

  ObjectModel Lock;
  //vector of entries
  std::vector< DCVectorHashEntry<T> > entry;   //Vector of data
  T                 emptyValue; //Value to return if there is no data for our hash.
  T                 badValue;//Value for bad data

};



#endif
