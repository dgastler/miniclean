#ifdef __RATDS__
#include <DRPusher.h>
#include <math.h>


void DRPusher::ProcessUnSentEvent()
{
  //Get next unsent event
  int32_t index;      
  if(FEPC_mem->GetUnSent(index)) 
    //We got a new "Un-sent" event.
    {
      //Pointer to RATEVBlock
      RATEVBlock * block = NULL;
      //Check and get the RATEVBlock
      if(index != FEPC_mem->GetBadValue() &&
	 ((block = FEPC_mem->GetFEPCBlock(index)) != NULL))
	{
	  #ifdef DEBUG_ROLLOVER
	  if(block->wfdEventID >= (pow(2,DEBUG_ROLLOVER) - 5))
	    rollover = true;
	  if(rollover)
	    {
	      printf("DRPusher: Got block for wfdEventID %"PRIu32"\n",block->wfdEventID);
	    }
	  #endif
	  //Check that this event has the correct number of WFDs
	  if(block->WFD.size() == detectorInfo.GetWFDCount())	    
	    //A valid event
	    {	  
	      //FEPC interface values
	      uint64_t password = FEPC_mem->GetStorageBlankPassword(); 
	      int32_t tempIndex = FEPC_mem->GetBadValue();
	      //fprintf(stderr,"Index set to %i, password set to %lu\n",tempIndex,password);
	      //Get a place in the storage VecHash for this event
	      int64_t retFEPC= FEPC_mem->GetStorageEvent(FEPC_mem->GetKey(block),	   
							 tempIndex,
							 password);
	      if(retFEPC == returnDCVector::EMPTY_KEY)
		{
		  #ifdef DEBUG_ROLLOVER
		  if(rollover)
		    {
		      printf("DRPusher: Empty key for wfdEventID %u (this is good)\n",block->wfdEventID);
		    }
		  #endif
		  SendEvent(index,password);
		}
	      else if(retFEPC >= returnDCVector::COLLISION)		  
		{
		  #ifdef DEBUG_ROLLOVER
		  if(rollover)
		    {
		      printf("DRPusher: Collision for wfdEventID %u\n",block->wfdEventID);
		    }
		  #endif
		 
		  MoveBad(retFEPC,tempIndex,Level::STORAGE_COLLISION,
			  index,password);
		}
	      else if(retFEPC == returnDCVector::BAD_MUTEX)
		{
		  #ifdef DEBUG_ROLLOVER
		  if(rollover)
		    {
		      printf("DRPusher: Bad mutex for wfdEventID %u\n",block->wfdEventID);
		    }
		  #endif
		  PrintWarning("BAD_MUTEX in event storage");
		  printf("DRPusher: BAD_MUTEX for wfdEventID %u; index: %u; password: %"PRIu64"\n",block->wfdEventID,index,password);
		  if(!FEPC_mem->AddUnSent(index))
		    {
		      //The only way for a failure here is that the mutex 
		      //lock has been turned off and we should shutdown.
		      char * buffer = new char[100];
		      sprintf(buffer,"Unable to return event %d to the UnSent DCQueue(after BAD_MUTEX)",
			      block->wfdEventID);
		      fprintf(stderr,"%s\n",buffer);
		      PrintError(buffer);
		      delete [] buffer;
		    }   
		}
	      else if(retFEPC == returnDCVector::BAD_PASSWORD)
		{
		  #ifdef DEBUG_ROLLOVER
		  if(rollover)
		    {
		      printf("DRPusher: Bad password for wfdEventID %u\n",block->wfdEventID);
		    }
		  #endif
		  //we expect this to happen with a very low probability 
		  if(!FEPC_mem->AddUnSent(index))
		    {
		      //The only way for a failure here is that the mutex 
		      //lock has been turned off and we should shutdown.
		      char * buffer = new char[100];
		      sprintf(buffer,"Unable to return event %d to the UnSent DCQueue(after BAD_PASSWORD)",
			      block->wfdEventID);
		      fprintf(stderr,"%s\n",buffer);
		      PrintError(buffer);
		      delete [] buffer;
		    }   
		}
	      else if(retFEPC == returnDCVector::OK)
		{
		  #ifdef DEBUG_ROLLOVER
		  if(rollover)
		    {
		      printf("DRPusher: OK for wfdEventID %u (this is bad)\n",block->wfdEventID);
		    }
		  #endif
		  char * buffer = new char[100];
		  sprintf(buffer,"DRPusher got an OK from the event storage for wfdEventID %u.  This is really strange\n",block->wfdEventID);
		  PrintWarning(buffer);
		    delete [] buffer;
		    RATEVBlock * unexpectedBlock = NULL;
		    unexpectedBlock = FEPC_mem->GetFEPCBlock(tempIndex);
		    
		    // printf("DRPusher: OK for wfdEventID %u; time: %zu; wfdEventID of event in storage: %u; time of event in storage: %zu; index: %u; password: %zu\n",block->wfdEventID, block->eventTime, unexpectedBlock->wfdEventID, unexpectedBlock->eventTime, index, password);
		    printf("DRPusher: OK for wfdEventID %u; time: %"PRIu64"; wfdEventID of event in storage: %u; time of event in storage: %"PRIu64"; index: %u; password: %"PRIu64"\n",block->wfdEventID, block->eventTime, unexpectedBlock->wfdEventID, unexpectedBlock->eventTime, index, password);

		    //MoveBad(retFEPC,tempIndex,Level::STORAGE_COLLISION,
		    //index,password);
		    //			  block->wfdEventID,index);
		    SendEvent(index,password);
		}
	      else
		{
		  PrintError("Unknown error in GetStorageEvent");
		}
	  //No matter what happens in this function we need to 
	  //forget all the info we were passed. (it's no longer valid)
	  block = NULL;
	  index = FEPC_mem->GetBadValue();
	    }
	  else
	    {
	      char * buffer = new char[100];
	      sprintf(buffer,"Wrong number of WFDs; expected %zu but got %zu\n",
		      detectorInfo.GetWFDCount(),block->WFD.size());
	      PrintError(buffer);
	    }
	}
      else
	{
	  PrintError("bad block");
	}		
    }
  else
    {
      PrintError("Error getting unsent event.");
    }  
}

void DRPusher::MoveBad(int64_t badEventKey,int32_t badIndex,Level::Type errorCode,
		       int32_t index,uint64_t password)
{
  RATEVBlock * block = FEPC_mem->GetFEPCBlock(badIndex);
  if(block != NULL)
    {
      
      uint8_t boardCount = block->ev->GetBoardCount();

      std::stringstream ss;
      ss << "Collision in DRPusher!  Bad key: " << badEventKey
	 << "   Bad index: " << badIndex
	 << "   Index: "     << index << std::endl
	 << "   Board count: " << boardCount << std::endl;
      for(int WFDIndex = 0; WFDIndex<boardCount; WFDIndex++)
	{
	  std::vector<uint32_t> boardHeader = block->ev->GetBoard(WFDIndex)->GetHeader();	
	  int wfdID = block->ev->GetBoard(WFDIndex)->GetID();
	  ss << "\t WFD " <<  wfdID
	     << "  Time:" << std::hex << boardHeader[3]
	     << std::dec << "\n";
	}
      
      PrintWarning(ss.str().c_str());
    }
  else
    {
      PrintWarning("Got a NULL pointer from a bad event");
    }
  
  if(FEPC_mem->MoveStorageEventToBadQueue(badEventKey,
					  password,
					  errorCode) == returnDCVector::OK)
    {
      SendEvent(index,password);
    }
//  if(FEPC_mem->AddBadEvent(badEventKey,
//			   badIndex,
//			   errorCode))
//    {
//      SendEvent(index,password);
//    }
  else
    {
      char * buffer = new char[100];
      sprintf(buffer,"Failed to move bad event %"PRId64"",badEventKey);
      PrintError(buffer);
      delete [] buffer;
    }
}


void DRPusher::SendEvent(int32_t index, uint64_t password)
{
  //Load event
  RATEVBlock * event = FEPC_mem->GetFEPCBlock(index);
  int wfdsProcessed = 0; //number of WFDs processed for this event
  int pmtsProcessed = 0; //number of total PMTs processed for this event

  //====================================
  //packet interface 
  //====================================
  DCNetPacket * packet = NULL;          //Pointer to packet object
  int32_t iDCPacket = FreeFullQueue::BADVALUE; //packet index
  uint8_t *packetDataPtr = NULL; //Ptr to the beginning of the data pointer
  uint8_t *currentPacketDataPtr = NULL; //Ptr to current position in the packet
  uint16_t packetSizeLeft = 0;       //remaining packet data size


  //====================================
  //event parsing 
  //====================================
  DRPacket::Event * drEvent = NULL;     //Pointer to the current Event struct
          

  //Loop over all the WFDs in this event
  //Make sure we send packets in units of complete boards
  while(wfdsProcessed < event->ev->GetBoardCount())
    {
      //Calcuate WFD size
      RAT::DS::Board * board = event->ev->GetBoard(wfdsProcessed);
      uint32_t wfdSize = sizeof(DRPacket::Board);
      uint32_t wfdChannels = board->GetChannelCount();
      uint32_t wfdChannelsProcessed = 0;
      if(!promptTotalMode)
	{
	  while(wfdChannelsProcessed < wfdChannels)
	    {
	      
	      //We are using ZLE integrals for DR decisions
	      {
	      RAT::DS::Channel * channel = board->GetChannel(wfdChannelsProcessed);	      
	      int rawIntegralCount = channel->GetRawBlockCount();
	      wfdSize += (sizeof(DRPacket::Channel) + 
			  (rawIntegralCount*sizeof(DRPacket::RawIntegral)));
	      wfdChannelsProcessed++;
#ifdef __DRPUSHER_SEGFAULT_DEBUG__
	      if(rawIntegralCount < 0)
		{
		  fprintf(stderr,"Negative rawIntegralCount: %d\n",rawIntegralCount);
		}
#endif
	      }
	    }
	}
      else
	//We are using prompt/total information for DR decisions
	{
	  wfdSize += wfdChannels*((sizeof(DRPacket::Channel) 
				  + sizeof(DRPacket::PromptTotalBlock)));
	}
	
      if(packetSizeLeft < wfdSize)
	{
	  //We need a new packet
	  
	  //Send old packet if it exists
	  if(packet != NULL)
	    {
	      packet->SetType(DCNetPacket::INTEGRATED_WINDOWS_FULL_PACKET);
	      packet->SetData(packetDataPtr,
			      uint32_t(currentPacketDataPtr-packetDataPtr));	      
	      if(!DRAssembler.GetOutPacketManager()->AddFull(iDCPacket))
		{
		  PrintWarning("Error in building packet.  Event lost!\n");
		  return;
		}
	    }
	  //Get new packet
	  DRAssembler.GetOutPacketManager()->GetFree(iDCPacket,true);
	  packet = DRAssembler.GetOutPacketManager()->GetPacket(iDCPacket);
	  if(packet == NULL)
	    {
	      PrintWarning("Bad new packet!\n");
	      return;
	    }
	  currentPacketDataPtr = packet->GetData();
	  packetDataPtr = currentPacketDataPtr;
	  packetSizeLeft = packet->GetMaxDataSize(); 
	  //check for room in this packet
	  if(packetSizeLeft < wfdSize)
	    {
	      PrintWarning("wfdSize is larger than the max packet size!\n");
	      return;
	    }

	  //Add event header
	  drEvent = (DRPacket::Event *) currentPacketDataPtr;
	  currentPacketDataPtr += sizeof(DRPacket::Event); 
	  packetSizeLeft -= sizeof(DRPacket::Event);

	  drEvent->ID = event->wfdEventID;
	  drEvent->BoardCount = 0;
	  drEvent->EventTime = event->eventTime;
	  if(!promptTotalMode)
	    drEvent->DataType = 3;
	  else
	    drEvent->DataType = 4;
	} 
      //Add WFD data
      AddToPacket(board,currentPacketDataPtr,packetSizeLeft);	    
      //Move forward in WFDs
      wfdsProcessed++;
      drEvent->BoardCount++;
      pmtsProcessed+=wfdChannels;      
    }

  //Send old packet if it exists
  if(packet != NULL)
    {
      packet->SetType(DCNetPacket::INTEGRATED_WINDOWS_FULL_PACKET);
      packet->SetData(packetDataPtr,
		      uint32_t(currentPacketDataPtr-packetDataPtr));	      
      if(!DRAssembler.GetOutPacketManager()->AddFull(iDCPacket))
	{
	  PrintWarning("Error in building packet.  Event lost!\n");
	  return;
	}
    }

  //Return storage event.   
  //this will always work because we hold the password
  int64_t retFEPC_mem;
  retFEPC_mem = FEPC_mem->ReleaseStorageEvent(index,
					      password);
}

#endif
