#ifndef __V1720_EVENT__
#define __V1720_EVENT__

#include <stdint.h> //uint32_t
#include <cstdio>
#include <cstring> //memcpy and memset
#include <vector>

#include <v1720EventChannel.h>

enum
  {
    V1720EVENT_OK = 0,
    V1720EVENT_SMALL_EVENT = -1,
    V1720EVENT_CORRUPT_EVENT = -2,
    V1720EVENT_OTHER = -3,
    V1720EVENT_JUNK_WORD = -4,
    V1720EVENT_NULL_POINTER = -5
  };

#define V1720_CHANNEL_COUNT 8
#define V1720_HEADER_SIZE 4

class v1720Event
{
 public:
  v1720Event();
  ~v1720Event();
  
  //Clear but don't delete sub structures
  void Clear();

  //Get the parsed copy of the Header words
  uint32_t                     GetHeaderWord(size_t i);

  //Get the header as a std::vector
  std::vector<uint32_t>        GetHeader(){return std::vector<uint32_t>(header,header+V1720_HEADER_SIZE);}

  //Get the Board ID
  uint32_t GetBoardID(){return ((header[1] >> 27)&0x1F);}

  //Get the WFD Event ID
  uint32_t GetWFDEventID(){return (0x00FFFFFF & (header[2]));}

  //Get the WFD timestamp
  uint32_t GetWFDTimeStamp(uint16_t delay = 0){return (0x7FFFFFFF & (header[3] + delay));}

  //Get the parsed channel data structures
  const v1720EventChannel &    GetChannel(size_t i);
  size_t                       GetChannelCount(){return(V1720_CHANNEL_COUNT);}

  //Return if a channel is active
  bool ValidChannel(size_t i)
  {
    uint8_t channelMask = 0xFF&(header[1]);
    return (channelMask & (0x1<<i));
  }

  //returns the BYTE(!) offset of the next possible position 
  //of an event in the _rawData array.
  int32_t                      ProcessEvent(uint32_t * _rawData, 
					    uint32_t _rawSizeMax,
					    bool BasicParseOnly = false);
  
  //Get event properties
  bool                         IsZLE(){return(ZLE);};
  bool                         IstwoPack(){return(twoPack);};

  //Interface to raw data the parsed structures are built from. 
  //returns 32bit pointer and 32bit size.
  uint32_t * GetRawData(uint32_t &bufferSize){bufferSize = size;return(rawData);};
  void    WriteCorruptEvent();
 private:
  //Raw data parsing functions (Called by ProcessEvent)
  int32_t ProcessHeader();
  int     ProcessZLE(uint8_t channelID,uint32_t &position);
  int     ProcessFULL(uint8_t channelID,uint32_t &position);
  uint8_t BitSum(uint32_t data);  


  //Array of v1720EventChannels in this event.  (8 channels)
  v1720EventChannel channel[V1720_CHANNEL_COUNT];
  //Internal copy of the event header (4 32bit words)
  uint32_t header[V1720_HEADER_SIZE];
  //Parsed event attributes
  bool ZLE;
  bool twoPack;
  uint32_t boardID;
  uint32_t IOPattern;
  uint32_t channelPattern;
  uint8_t  numberOfActiveChannels;
  uint32_t eventNumber;
  uint32_t baseTime;

  //This class doesn't own this data
  uint32_t * rawData;
  uint32_t size;
  uint32_t rawSizeMax;
};

#endif
