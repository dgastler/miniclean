#ifndef __DCLAUNCHER_ERRORCODES__
#define __DCLAUNCHER_ERRORCODES__
enum 
  {
    SETUP_OK = 0x0,
    BAD_XML = 0x1,
    FAIL = 0x2,
    
    //Memory manager errors
    MM_TOO_MANY   = 0x1000000000000ULL,  //too many mm to set up in one command
    MM_XML_TYPE   = 0x2000000000000ULL,  //Bad type xml
    MM_XML_NAME   = 0x3000000000000ULL,  //Bad name xml
    MM_UNKNOWN    = 0x4000000000000ULL,  //Unknown memory manager type
    MM_NEW_FAIL   = 0x5000000000000ULL,  //MM creation failed
    MM_ALLOC_FAIL = 0x6000000000000ULL,  //MM memory allocation failed
    MM_DUP        = 0x7000000000000ULL,      //name collision
    MM_NOT_FOUND  = 0x8000000000000ULL,  //Can't find the MM in the XML (WTF?)

    THREAD_TOO_MANY     = 0x10000000000000ULL,   //too many threads to set up in one command
    THREAD_MISSING_MM   = 0x20000000000000ULL,   //One of the requested memory managers is missing
    THREAD_MISSING_NAME = 0x30000000000000ULL,   //No name for this thread
    THREAD_MISSING_TYPE = 0x40000000000000ULL,   //No name for this thread
    THREAD_SETUP        = 0x50000000000000ULL,   //Error in thread setup
    THREAD_UNKNOWN      = 0x60000000000000ULL,   //unknown thread type
    THREAD_BAD_NODE     = 0x70000000000000ULL    //THis shouldn't happen@!
  };

#endif
