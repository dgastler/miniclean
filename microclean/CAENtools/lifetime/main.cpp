#include <iostream>
#include <string>

#include "Event.h"
#include "TextArray.h"
#include "xmlParser.h"

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1D.h"
#include "TH2D.h"

#include "SPEFit.h"
#include "WaveformProcessing.h"
#include "Integrate.h"

using namespace std;

volatile bool stoprun = false;
void signal_handler(int num)
{
  if(num == SIGINT)
    stoprun = true;
  else
    cerr << "Signal:" << num << endl;
}










int main(int argc, char **argv)
{

  
  Float_t Volts;
  Float_t seconds;
  Float_t C;


  signal(SIGINT,signal_handler);

  Volts = (1.0/255.0);
  seconds = 2e-9;
  C = Volts * seconds * (1.0/50.0);
  
  //Array with timing windows
  Int_t WindowEnds[8] = {-10,38,45,52,59,66,2500,7500};


  Int_t V0 = 0;
  Int_t V1 = 0;

  Int_t TotalEvents   = 0;
  Int_t SkippedEvents = 0;
  Bool_t SubEvent = false;

  Float_t Fast[2];
  Float_t Slow[2];

  Char_t * TextBuffer = new Char_t[1000];


  if(argc < 5)
    {
      cout << argv[0] << " FPromptMin FPromptMax OutFile Infiles" << endl;
      return(0);
    }
  Int_t FirstFileArgv = 4;

  Float_t fPmin = atof(argv[1]);
  Float_t fPmax = atof(argv[2]);


  // Open and close first file to get the waveform length
  TFile * InFile   = new TFile(argv[FirstFileArgv],"READ","",9);
  TTree * tree     = (TTree*) InFile->Get("tree");

  Event * event    = 0;
  TextArray *RootText = 0;
  Int_t NEvents          = tree->GetEntries();
  tree->SetBranchAddress("Event branch",&event);
  tree->GetEntry(0);

  
  Int_t Size = event->GetSize(0);



  //Find Voltage

  TTree * TextTree = (TTree*) InFile->Get("TextTree");

  TextTree->SetBranchAddress("Run Notes",&RootText);

  TextTree->GetEntry(1);

  Char_t * TempPointer = RootText->GetArray();
  //  cerr << TempPointer << endl;
  string Text(TempPointer);

  Text.resize(  Text.rfind("</DAQ>",Text.size())+6);
  //cerr << Text << endl;
  
  Text.copy(TextBuffer,Text.size());
  cout << TextBuffer << endl;

  //  XMLNode DAQNode=XMLNode::parseString(TempPointer,"DAQ");
  XMLNode DAQNode=XMLNode::parseString(TextBuffer,"DAQ");

  V0 = atoi(DAQNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",0).getAttribute("PMTVoltage"));

  V1 = atoi(DAQNode.getChildNode("Setup").getChildNode("WFD",0).getChildNode("Channel",2).getAttribute("PMTVoltage"));

  cout << V0 << " " << V1 << endl;
  
  InFile->Close();
  delete InFile;

  //Close the fist file and now go on to the analysis. 


  Float_t * Channel1                   = new Float_t[Size];
  Float_t * Channel1b                  = new Float_t[Size];
  Float_t * Channel2                   = new Float_t[Size];
  Float_t * Channel2b                  = new Float_t[Size];

  sprintf(TextBuffer,"Summed PMT0 Waveform: %04d",V0);
  printf("%s\n",TextBuffer);
  TH2D * PMT0vE = new TH2D("PMT0vE",TextBuffer,WindowEnds[6] - WindowEnds[0],0,(WindowEnds[6] - WindowEnds[0])*2E-9,300,0,1.5E-8);
  sprintf(TextBuffer,"Summed PMT1 Waveform: %04d",V1);
  printf("%s\n",TextBuffer);
  TH2D * PMT1vE = new TH2D("PMT1vE",TextBuffer,WindowEnds[6] - WindowEnds[0],0,(WindowEnds[6] - WindowEnds[0])*2E-9,300,0,1.5E-8);

  TH1F * rPMT0fP = new TH1F("rPMT0fP","PMT0 fPrompt",100,0,1);
  TH1F * rPMT1fP = new TH1F("rPMT1fP","PMT1 fPrompt",100,0,1);
  TH1F * cPMT0fP = new TH1F("cPMT0fP","PMT0 fPrompt",100,0,1);
  TH1F * cPMT1fP = new TH1F("cPMT1fP","PMT1 fPrompt",100,0,1);
  
  TH2F * PMT0SPE = new TH2F("PMT0SPE","PMT0SPE",310,-10*C,300*C,300,0,300*Volts);
  PMT0SPE->GetXaxis()->SetTitle("Charge(C)");
  PMT0SPE->GetYaxis()->SetTitle("Volts");
  
  TH2F * PMT1SPE = new TH2F("PMT1SPE","PMT1SPE",310,-10*C,300*C,300,0,300*Volts);
  PMT1SPE->GetXaxis()->SetTitle("Charge(C)");
  PMT1SPE->GetYaxis()->SetTitle("Volts");

  

  for(int i = FirstFileArgv; i < argc && !stoprun;i++)
    {
      //Open file in of type "base"filenumber.root
      cout << "Opening file: " << argv[i] << endl;
      InFile                 = new TFile(argv[i],"READ","",9);
      tree                   = (TTree*) InFile->Get("tree");
      TTree * TextTree = (TTree*) InFile->Get("TextTree");
      event                  = 0;
      Int_t NEvents          = tree->GetEntries();
      tree->SetBranchAddress("Event branch",&event);




      //Loop over events in file
      for(int EventN = 0;EventN < NEvents && !stoprun; EventN++)
      	{
	  SubEvent = false;
	  TotalEvents++;
	  tree->GetEntry(EventN);
	  

	  bool FixGain[2]                      = {false,false};
	  Int_t ChannelSize                    = event->GetSize(0);
	  
	  Float_t Channel1_Offset              = 0;
	  Float_t Channel1b_Offset             = 0;
	  Float_t Channel2_Offset              = 0;
	  Float_t Channel2b_Offset             = 0;
	  
	  Float_t Sigma_Channel1_Offset        = 0;
	  Float_t Sigma_Channel1b_Offset       = 0;
	  Float_t Sigma_Channel2_Offset        = 0;
	  Float_t Sigma_Channel2b_Offset       = 0;
	  
	  
	  
	  Channel1_Offset  = Offset(event,0,1000,&Sigma_Channel1_Offset);
	  Channel1b_Offset = Offset(event,1,1000,&Sigma_Channel1b_Offset);
	  Channel2_Offset  = Offset(event,2,1000,&Sigma_Channel2_Offset);
	  Channel2b_Offset = Offset(event,3,1000,&Sigma_Channel2b_Offset);
	  
	  FixGain[0] = CopyWaveform(event,0,Channel1,ChannelSize,Channel1_Offset);
	  FixGain[1] = CopyWaveform(event,2,Channel2,ChannelSize,Channel2_Offset);	  

	  CopyWaveform(event,1,Channel1b,ChannelSize,Channel1b_Offset);
	  CopyWaveform(event,3,Channel2b,ChannelSize,Channel2b_Offset);

	  if(FixGain[0])
	    {
	      MergeWaveforms(ChannelSize,Channel1,Channel1b,Channel1_Offset,10.0);
	    }
	  if(FixGain[1])
	    {
	      MergeWaveforms(ChannelSize,Channel2,Channel2b,Channel2_Offset,10.0);
	    }

	  Int_t Peak[2] = {0,0};
	  Int_t SubPeak[2] ={0,0};
	  //Find waveform starts assuming they are in the first 6us of the waveform
	  Peak[0] = Find_Waveform_Start(3000,Channel1);
	  Peak[1] = Find_Waveform_Start(3000,Channel2);
	  
	  Float_t MaxHeights[2];
	  
	  //Find the maximum wave form heights in assuming it happens in the first 6us.
	  MaxHeights[0] = Channel1[Find_Waveform_Maximum(3000,Channel1)];
	  MaxHeights[1] = Channel2[Find_Waveform_Maximum(3000,Channel2)];
	  
	  	  
	  //Lets see if there is a second event in the waveform.
	  if((Channel1[Find_Waveform_Maximum(ChannelSize-3000,Channel1+3000)+3000] > 0.4*MaxHeights[0]) &&
	     (Channel2[Find_Waveform_Maximum(ChannelSize-3000,Channel2+3000)+3000] > 0.4*MaxHeights[1]))
	    {
	      if (fabs((Find_Waveform_Start(ChannelSize-3000,Channel1+3000)+3000) - (Find_Waveform_Start(ChannelSize-3000,Channel2+3000)+3000)) < 10)
		{
		  SubEvent = true;
		  SubPeak[0] = (Find_Waveform_Start(ChannelSize-3000,Channel1+3000)+3000);
		  SubPeak[1] = (Find_Waveform_Start(ChannelSize-3000,Channel2+3000)+3000);
		}
	    }
	  
	  //                 Integration windows
	  //                 -20-76ns  76-90  90-104 104-118 118-132 132-5us 5us-15us
	  Float_t PMT0[7] = {0        ,0     ,0     ,0      ,0      ,0       ,0}; 
	  Float_t PMT1[7] = {0,0,0,0,0,0,0};
	  
	  if(!SubEvent)
	    {
	      //If there is no Sub event integrate as normal and use the last 10 us for SPE study
	      
	      PMT0[0] = Integrate(Channel1,Peak[0] +WindowEnds[0]   ,Peak[0] +WindowEnds[1]    ,0,2,25,5);
	      PMT0[1] = Integrate(Channel1,Peak[0] +WindowEnds[1]   ,Peak[0] +WindowEnds[2]    ,0,2,25,5);
	      PMT0[2] = Integrate(Channel1,Peak[0] +WindowEnds[2]   ,Peak[0] +WindowEnds[3]    ,0,2,25,5);
	      PMT0[3] = Integrate(Channel1,Peak[0] +WindowEnds[3]   ,Peak[0] +WindowEnds[4]    ,0,2,25,5);
	      PMT0[4] = Integrate(Channel1,Peak[0] +WindowEnds[4]   ,Peak[0] +WindowEnds[5]    ,0,2,25,5);
	      PMT0[5] = Integrate(Channel1,Peak[0] +WindowEnds[5]   ,Peak[0] +WindowEnds[6]    ,0,2,25,5);


	      PMT1[0] = Integrate(Channel2,Peak[1] +WindowEnds[0]   ,Peak[1] +WindowEnds[1]    ,0,2,25,5);
	      PMT1[1] = Integrate(Channel2,Peak[1] +WindowEnds[1]   ,Peak[1] +WindowEnds[2]    ,0,2,25,5);
	      PMT1[2] = Integrate(Channel2,Peak[1] +WindowEnds[2]   ,Peak[1] +WindowEnds[3]    ,0,2,25,5);
	      PMT1[3] = Integrate(Channel2,Peak[1] +WindowEnds[3]   ,Peak[1] +WindowEnds[4]    ,0,2,25,5);
	      PMT1[4] = Integrate(Channel2,Peak[1] +WindowEnds[4]   ,Peak[1] +WindowEnds[5]    ,0,2,25,5);
	      PMT1[5] = Integrate(Channel2,Peak[1] +WindowEnds[5]   ,Peak[1] +WindowEnds[6]    ,0,2,25,5);
	      
	      if(MaxHeights[0] < 300)
		PMT0[6] = Integrate(Channel1,Peak[0] +WindowEnds[6]   ,Peak[0] +WindowEnds[7],PMT0SPE,2,25,5);
	      if(MaxHeights[1] < 300)
		PMT1[6] = Integrate(Channel2,Peak[1] +WindowEnds[6]   ,Peak[1] +WindowEnds[7],PMT1SPE,2,25,5);

	      Fast[0] = PMT0[0] + 
		PMT0[1];
	      Fast[1] = PMT1[0] + 
		PMT1[1];
	      Slow[0] = PMT0[2] + 
		PMT0[3] +
		PMT0[4] + 
		PMT0[5];
	      Slow[1] = PMT1[2] + 
		PMT1[3] +
		PMT1[4] + 
		PMT1[5];

	      Int_t PMT0EnergyBin = PMT0vE->GetYaxis()->FindBin(Fast[0] + Slow[0]);
	      Int_t PMT1EnergyBin = PMT1vE->GetYaxis()->FindBin(Fast[1] + Slow[1]);	 
	      //force both waveforms to independently be in the correct fPrompt range
	      //before adding either event to histogram.
	      if((Fast[0]/(Fast[0] + Slow[0]) > fPmin && Fast[0]/(Fast[0] + Slow[0]) < fPmax) &&
		 (Fast[1]/(Fast[1] + Slow[1]) > fPmin && Fast[1]/(Fast[1] + Slow[1]) < fPmax))// &&
		 //		 (PMT0EnergyBin < 300 && PMT1EnergyBin < 300))
		{
		  //Add waveform to 2d histogram
		  
		  for(int i = 0; i < WindowEnds[6] - WindowEnds[0];i++)
		    {
		      PMT0vE->Fill(PMT0vE->GetXaxis()->GetBinCenter(i),Fast[0] + Slow[0],Channel1[Peak[0] + WindowEnds[0] + i]);
		      PMT1vE->Fill(PMT1vE->GetXaxis()->GetBinCenter(i),Fast[1] + Slow[1],Channel2[Peak[1] + WindowEnds[0] + i]);
		      //		      PMT0vE->SetBinContent(i,PMT0EnergyBin,
		      //					    PMT0vE->GetBinContent(i,PMT0EnergyBin) +Channel1[Peak[0] + WindowEnds[0] + i]);
		      //		      PMT1vE->SetBinContent(i,PMT1EnergyBin,
		      //					    PMT1vE->GetBinContent(i,PMT1EnergyBin) +Channel2[Peak[1] + WindowEnds[0] + i]);
		    }
		  cPMT0fP->Fill(Fast[0] /(Fast[0] + Slow[0]));
		  cPMT1fP->Fill(Fast[1] /(Fast[1] + Slow[1]));
		}
	      else
		{
		  SkippedEvents++;
		  //		  printf("%08E %08f %08E %08f \n", Fast[0] + Slow[0],Fast[0]/(Fast[0] + Slow[0]),Fast[1] + Slow[1],Fast[1]/(Fast[1] + Slow[1]));
		  //		  rPMT0fP->Fill(Fast[0] /(Fast[0] + Slow[0]));
		  //		  rPMT1fP->Fill(Fast[1] /(Fast[1] + Slow[1]));
		}
	      
	      
	    }
	  else
	    {
	      SkippedEvents++;
	      //	      rPMT0fP->Fill(Fast[0] /(Fast[0] + Slow[0]));
	      //	      rPMT1fP->Fill(Fast[1] /(Fast[1] + Slow[1]));
	    }

	  rPMT0fP->Fill(Fast[0] /(Fast[0] + Slow[0]));
	  rPMT1fP->Fill(Fast[1] /(Fast[1] + Slow[1]));



	}

      InFile->Close("R");
      delete InFile;
    }
  delete [] Channel1;
  delete [] Channel1b;
  delete [] Channel2 ;
  delete [] Channel2b;

//  PMT0vE->Scale(1.0/(TotalEvents - SkippedEvents));
//  PMT1vE->Scale(1.0/(TotalEvents - SkippedEvents));
  
  TFile * OutFile = new TFile(argv[3],"RECREATE");
  cout << "Skipped " << SkippedEvents << " out of " << TotalEvents << " Total Events." << endl;
  //  PMT0->Write();
  //  PMT1->Write();

  TH1F * SPE0 = (TH1F*)PMT0SPE->ProjectionX();
  TH1F * SPE1 = (TH1F*)PMT1SPE->ProjectionX();

  SPEfit(SPE0,"SPE0Fit");
  SPEfit(SPE1,"SPE1Fit");

//  SPE0->Write();
//  SPE1->Write();
  PMT0SPE->Write();
  PMT1SPE->Write();

  PMT0vE->Write();
  PMT1vE->Write();
  PMT0vE->ProjectionX();//->Write();
  PMT1vE->ProjectionX();//->Write();
  rPMT0fP->Write();
  rPMT1fP->Write();
  cPMT0fP->Write();
  cPMT1fP->Write();
  OutFile->Write();
  OutFile->Close();
  return(0);
}
