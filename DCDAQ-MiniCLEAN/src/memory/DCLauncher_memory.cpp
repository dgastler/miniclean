#include <DCLauncher.h>

//Memory managers
#include <MemoryBlockManager.h>

#ifdef __RATDS__

#include <FEPCRATManager.h>
#include <DRPCRatManager.h>
#include <RATDiskBlockManager.h>
#include <RATCopyQueue.h>

#endif

uint64_t DCLauncher::SetupMemoryManagers(xmlNode * MMNode,
					 std::vector<MemoryManager*> & tempMM)
{
  //Check how many managers we are suppose to set up.
  uint32_t MMCount = NumberOfNamedSubNodes(MMNode,"MANAGER");  

  //Fail if it is over 48.
  if(MMCount > 48)
    return(MM_TOO_MANY);


  uint64_t ret  = 0;
  std::string errName("SetupMemoryManagers");
  //Loop over all MANAGER nodes in the XML stream 
  for(uint32_t node = 0; node < MMCount;node++)
    {
      //Load the node th element
      xmlNode* memoryManagerNode = FindIthSubNode(MMNode,"MANAGER",node);
      if(memoryManagerNode != NULL) // If this pointer isn't NULL
	{	  
	  //Get type of this manager
	  std::string memoryManagerType("");
	  if(FindSubNode(memoryManagerNode,"TYPE") == NULL)
	    {
	      ret = MM_XML_TYPE;
	      ret |= 0x1<<node;
	      break;
	    }
	  else
	    GetXMLValue(memoryManagerNode,"TYPE",errName.c_str(),memoryManagerType);	  
	  
	  //Get name of this manager
	  std::string memoryManagerName("");
	  if(FindSubNode(memoryManagerNode,"NAME") == NULL)
	    {
	      ret = MM_XML_NAME;
	      ret |= 0x1<<node;
	      break;
	    }
	  else
	    GetXMLValue(memoryManagerNode,"NAME",errName.c_str(),memoryManagerName);


	  //Allocate the correct memorymanager
	  bool KnownThread = true;	  
	  if(boost::iequals(memoryManagerType,"MemoryBlockManager"))
	    {
	      tempMM.push_back( new MemoryBlockManager(memoryManagerName));
	    }
	  //memory managers that require RAT
#ifdef __RATDS__
	  else if(boost::iequals(memoryManagerType,"FEPCRatManager"))
	    {
	      tempMM.push_back( new FEPCRATManager(memoryManagerName));
	    }
	  else if(boost::iequals(memoryManagerType,"DRPCRatManager"))
	    {
	      tempMM.push_back( new DRPCRATManager(memoryManagerName));
	    }
	  else if(boost::iequals(memoryManagerType,"RATDiskBlockManager"))
	    {
	      tempMM.push_back( new RATDiskBlockManager(memoryManagerName));
	    }
	  else if(boost::iequals(memoryManagerType,"RATCopyQueueManager"))
	    {
	      tempMM.push_back( new RATCopyQueue(memoryManagerName));
	    }
#endif
	  else
	    {
	      KnownThread = false;
	    }

	  //Set up the thread.
	  if(KnownThread)
	    {
	      //Check that the allocation went well
	      if(tempMM.back() == NULL)
		{
		  ret =  MM_NEW_FAIL;
		  ret |= (0x1<<node);
		  break;
		}

	      printf("Setting up memory manager %s(%s): ",
		     memoryManagerName.c_str(),
		     memoryManagerType.c_str());

	      if(!tempMM.back()->AllocateMemory(memoryManagerNode))
		{
		  printf("FAILED!\n");
		  ret = MM_ALLOC_FAIL;
		  ret = ret|(0x1 << node); //Set the nodeth bit of ret to 1
		  break;
		}
	      else
		{
		  printf("OK.\n");		  
		}
	    }
	  else
	    {	      
	      printf("Memory manager %d unknown name/type: %s/%s\n",
		     node,
		     memoryManagerName.c_str(),
		     memoryManagerType.c_str());
	      ret = MM_UNKNOWN;
	      ret |= (0x1 << node); //Set the nodeth bit of ret to 1
	      break;
	    }
	}
      else
	{
	  fprintf(stderr,"Bad memory manager node # %d\n",node);	 
	  ret = MM_NOT_FOUND;
	  ret |= (0x1<<node);
	  break;
	}
    }
  return(ret);
}
