#ifndef __SPEEDTEST__
#define __SPEEDTEST__

#include <DCThread.h>
#include <string>

#include <xmlHelper.h>

//DO NOT MODEL YOUR THREAD AFTER THIS ONE!
//NEVER, EVER!  THIS MEANS YOU!
//So says Dan Gastler


//struct for holding XML values
struct Setting {
  uint32_t Wait_time;
  uint32_t Pulse_count;
  uint32_t Frequency;
};


class SpeedTest : public DCThread 
{
 public:
  SpeedTest(std::string Name);
  ~SpeedTest();
  bool Setup(xmlNode * SetupNode,
	     std::map<std::string,MemoryManager*> &MemManager);
  virtual void MainLoop();
 private:

  //Handle incoming messages
  virtual bool ProcessMessage(DCMessage::Message &message);
  //Handle select timeout
  virtual void ProcessTimeout();

  unsigned char PPData(unsigned char TempFreq, unsigned char TempCount);
  unsigned char PPControl(unsigned char TempCount);
  bool Setup_port();
  

  //============================
  //Pulser settings
  //============================
  std::string Dev;                      //Name of parallel port device
  int32_t Fd;                           //File descriptor for PPort
  uint32_t Mode;                        //PPort mode
  std::vector <Setting> SettingVector;  //vector of pulser settings
  uint32_t DCDAQ_time;                  //Initial wait time.

  //============================
  //Pulser run variables
  //============================
  int32_t iRun;    
  bool DCDAQ_time_done;
  time_t currentTime;
  time_t lastTime;

  

  //Config file
  std::string Settings;
  xmlDoc * XMLDoc;
  xmlNode * BaseNode;
  xmlNode * SettingNode;
};


#endif
