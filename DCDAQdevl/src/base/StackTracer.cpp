#include <StackTracer.h>

/* 
example stolen from
http://www.gnu.org/software/libc/manual/html_node/Backtraces.html
*/

void print_trace() 
{
  void *array[10];
  size_t size;
  char **strings;
  size_t i;
  
  int status;
  char * demangled=NULL;

  size = backtrace (array, 10);
  strings = backtrace_symbols (array, size);
  
  printf ("Obtained %ld stack frames.\n", size);
  
  for (i = 0; i < size; i++)
    {
      demangled = abi::__cxa_demangle(strings[i], 0, 0, &status);
      printf (">    %s:%s (%d)\n", strings[i],demangled,status);
      free(demangled);
    }
  
  free (strings);
}
