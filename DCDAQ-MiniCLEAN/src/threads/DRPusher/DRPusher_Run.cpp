#ifdef __RATDS__
#include <DRPusher.h>

void DRPusher::MainLoop()
{
  //Check for a new connection
  CheckForNewConnection();

 
  //Check for a message from our DCNetThread
  if(DRAssembler.IsRunning())
    {
      if(IsReadReady(DRAssembler.GetMessageFD()))
	{
	  ProcessChildMessage();
	}
      if(IsReadReady(FEPC_mem->GetUnSentFDs()[0]))
	{
	  ProcessUnSentEvent();
	}
    }
}

bool DRPusher::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
      RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
      DeleteConnection();
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      //Remove the FEPCRATManager from select
      RemoveReadFD(FEPC_mem->GetUnSentFDs()[0]);
      SetupSelect();
      Loop = false;
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t timeTemp = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&timeTemp).c_str(),
	     GetName().c_str());
      
      //Register the WFD/EV block's free queue's FDs with select if
      //we have a network connection.
      if(DRAssembler.IsRunning())
	{
	  AddReadFD(FEPC_mem->GetUnSentFDs()[0]);      
	}
      else
	{
	  PrintWarning("No network connection! Not listening to the FEPC memory structure!");
	}
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }						
  return(ret);
}

void DRPusher::ProcessTimeout()
{
  //Set time.  
  currentTime = time(NULL);
  //Send updates back to DCDAQ
  if((currentTime - lastTime) > GetUpdateTime())
    {
      timeStepProcessedEvents = 0;

      //Setup next time
      lastTime = currentTime;
    }

  //Catch a timeout if we don't have a DCNetThread
  if((!DRAssembler.IsRunning()) 
     && (DRAssembler.GetSocketFD() != DRAssembler.badFD))
    {

      DRAssembler.Shutdown();
      StartNewConnection();
    }     
  //Check if we need to retry the connection
  if(DRAssembler.Retry())
    {
      //Start trying to connect again
      StartNewConnection();
    }
}

#endif
