#ifndef __NETHELPER__
#define __NETHELPER__

#include <iostream>
#include <vector>
#include <cstdio>
#include <stdint.h>
#include <errno.h>
#include <string.h>

//read/write
#include <sys/utsname.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/un.h>  // for AF_LOCAL
#include <fcntl.h> //For fcntl and misc

//CheckLocal
#include <net/if.h>
#include <sys/ioctl.h>

//DCAddress
#include <DCAddress.h>

#include <sstream>


namespace DCMessage
{
  class DCAddress;
}


int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint32_t size,
	       struct timeval timeout);

int32_t readN(int socketFD,
	       uint8_t * ptr,
	       uint32_t maxSize,
	       int & ret_errno);

struct ifconf Get_struct_ifconf(int fd);


bool AddrFullCompare(struct sockaddr_in *addr1,
		     struct sockaddr_in *addr2);
bool AddrAddressCompare(struct in_addr *addr1,
			struct in_addr *addr2);



bool AsyncConnectCheck(int & fd);

bool SetNonBlocking(int &fd,bool value);

DCMessage::DCAddress GetLocalAddress(int fd);
DCMessage::DCAddress GetRemoteAddress(int fd);

bool CloseSockFD(int fd);
bool SetSocketTTL(int fd, int time = 10);

#endif
