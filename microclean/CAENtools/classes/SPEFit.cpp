#include "SPEFit.h"

Double_t function(Double_t *x, Double_t *par)
{
  //SPE function to be fitted to the SPE spectrum. 
  Double_t N = par[0];
  Double_t m = par[1];
  Double_t a = par[2];
  Double_t qa = par[3];
  Double_t b = par[4];
  Double_t qb = par[5];
  Double_t c = par[6];
  Double_t d = par[7];
  Double_t ret = N * m/TMath::Gamma(m) * (
					  a * (pow(m*x[0]/qa,m-1)*exp(-1*m*x[0]/qa)) +
					  b * (pow(m*x[0]/qb,m-1)*exp(-1*m*x[0]/qb)) +
					  c * exp(-1*x[0]/d));
  return(ret);
}


TF1 * SPEfit(TH1F * h,Char_t * Name)
{
  //Take a 1D histogram of pulses and a name for the fit funtion.  
  //Calculate the fit function for this spectrum and return the function as a TF1.  
  //SPE is parameter 5 of the function.
  //Everything here is in C of charge. 
  //  TF1 * fit = new TF1(Name,function,h->GetBinCenter(h->GetMaximumBin()),1E-10,8);  
  TF1 * fit = new TF1(Name,"gaus(0) + gaus(3)",h->GetBinCenter(0),h->GetBinCenter(h->GetNbinsX()-1));  

  fit->SetNpx(1000);
  //Find a rough region to fit for the dark noise part of the spectrum. 

  TF1 * Pol = new TF1("Pol","pol2");
  Pol->SetParameters(h->GetMaximum(),(h->GetBinContent(2) - h->GetBinContent(1))/(h->GetBinCenter(2) - h->GetBinCenter(1)),0);

  Int_t NewMaxBin = 0;
  Float_t NewMax = 0;
  Int_t NewMinBin = 0;
  Float_t NewMin = 0;
  
  Float_t SPEguess = 0;
  Int_t SPEguessBin = 0;
  Float_t SPEHeight = 0;

  Int_t HalfMax = 0;

  for(int i = 0;i < h->GetNbinsX();i++)
    {
      h->Fit(Pol,"QF","",h->GetBinCenter(i - 2),h->GetBinCenter(i+2));
      if(Pol->GetParameter(2) < 0 && 
	 2.0*Pol->GetParameter(2) * h->GetBinCenter(i-1) + Pol->GetParameter(1) > 0 &&
	 2.0*Pol->GetParameter(2) * h->GetBinCenter(i+1) + Pol->GetParameter(1) < 0 &&
	 NewMaxBin == 0
	 )
	{
	  NewMax = Pol->GetParameter(2);
	  NewMaxBin = i;
	}
      if(NewMaxBin != 0 &&
	 Pol->GetParameter(2) > 0 && 
	 2.0*Pol->GetParameter(2) * h->GetBinCenter(i-1) + Pol->GetParameter(1) < 0 &&
	 2.0*Pol->GetParameter(2) * h->GetBinCenter(i+1) + Pol->GetParameter(1) > 0
	 )
	{
	  NewMin = Pol->GetParameter(2);
	  NewMinBin = i;
	  break;
	}
    }
  delete Pol;

  for(Int_t i = NewMaxBin;i > 0;i--)
    {
      if(2 * h->GetBinContent(i) < h->GetBinContent(NewMaxBin))
	{
	  HalfMax = i;
	  break;
	}
    }


  for(int i = NewMinBin; i < h->GetNbinsX();i++)
    {
      if(h->GetBinContent(i) > SPEHeight)
	{
	  SPEHeight = h->GetBinContent(i);
	  SPEguessBin = i;
	}
    }
  
  SPEguess = h->GetBinCenter(SPEguessBin);
  TF1 * Gaus = new TF1("Gaus","gaus",h->GetBinCenter(NewMinBin),
		       2.0*h->GetBinCenter(SPEguessBin) - h->GetBinCenter(NewMinBin));
		       
  Gaus->SetParameters(h->GetBinContent(SPEguessBin),
		      SPEguess,
		      SPEguess - h->GetBinCenter(NewMinBin));

  h->Fit(Gaus,"BQR");
  SPEguess = Gaus->GetParameter(1);

  Int_t SPEHalfMaxBin = 0;
  for(int i = SPEguessBin;i < h->GetNbinsX();i++)
    {
      if(2*h->GetBinContent(i) < h->GetBinContent(SPEguessBin))
	{
	  SPEHalfMaxBin = i;
	  break;
	}
    }
  
  //Fitting dark noise peak only
  fit->SetParameters(h->GetBinContent(NewMaxBin),
		     h->GetBinCenter(NewMaxBin),
		     h->GetBinCenter(NewMaxBin) - h->GetBinCenter(HalfMax),
		     0,
		     SPEguess,
		     1
		     );
//  fit->SetParameters(0,
//		     0,
//		     1,
//		     0,
//		     SPEguess,
//		     1
//		     );

  //  std::cerr << h->GetBinCenter(NewMaxBin) - h->GetBinCenter(HalfMax) << "asdf\n";

  
  fit->SetParLimits(0,0,2*fit->GetParameter(0));
  fit->SetParLimits(1,0,2*fit->GetParameter(1));
  fit->SetParLimits(2,0,2*fit->GetParameter(2));
  fit->FixParameter(3,0);
  fit->FixParameter(4,SPEguess);
  fit->FixParameter(5,1);
  //  h->Fit(fit,"QB","",h->GetBinCenter(0),h->GetBinCenter(NewMinBin));
  //  h->Fit(fit,"QBM","",h->GetBinCenter(0),h->GetBinCenter(NewMinBin));
  h->Fit(fit,"QB","",h->GetBinCenter(HalfMax),h->GetBinCenter(NewMinBin));
  //  h->Fit(fit,"QBM","",h->GetBinCenter(HalfMax),h->GetBinCenter(NewMinBin));

  //Fit SPE peak
  fit->FixParameter(0,fit->GetParameter(0));
  fit->FixParameter(1,fit->GetParameter(1));
  fit->FixParameter(2,fit->GetParameter(2));
  fit->SetParLimits(3,0,2*h->GetBinContent(h->GetXaxis()->FindBin(SPEguess)));
  fit->SetParLimits(4,0,2*SPEguess);
  fit->SetParLimits(5,0,h->GetBinCenter(SPEHalfMaxBin) - SPEguess);
  //  h->Fit(fit,"B","",h->GetBinCenter(NewMinBin),2*SPEguess - h->GetBinCenter(NewMinBin));
  //  h->Fit(fit,"BM","",h->GetBinCenter(NewMinBin),2*SPEguess - h->GetBinCenter(NewMinBin));
  h->Fit(fit,"QB","",h->GetBinCenter(NewMinBin),h->GetBinCenter(SPEHalfMaxBin));
  //  h->Fit(fit,"QBM","",h->GetBinCenter(NewMinBin),h->GetBinCenter(SPEHalfMaxBin));
  

  //Fit everything free

  fit->SetParLimits(0,0,2*fit->GetParameter(0));
  fit->SetParLimits(1,0,2*fit->GetParameter(1));
  fit->SetParLimits(2,0,2*fit->GetParameter(2));
  fit->SetParLimits(3,0,2*fit->GetParameter(3));
  fit->SetParLimits(4,0,2*fit->GetParameter(4));
  fit->SetParLimits(5,0,2*fit->GetParameter(5));
//  h->Fit(fit,"QB","",h->GetBinCenter(0),h->GetNbinsX());
//  h->Fit(fit,"QBM","",h->GetBinCenter(0),h->GetNbinsX());
//  h->Fit(fit,"QB","",h->GetBinCenter(0),h->GetBinCenter(SPEHalfMaxBin));
//  h->Fit(fit,"QBM","",h->GetBinCenter(0),h->GetBinCenter(SPEHalfMaxBin));
  h->Fit(fit,"QB","",h->GetBinCenter(HalfMax),h->GetBinCenter(SPEHalfMaxBin));
  //  h->Fit(fit,"QBM","",h->GetBinCenter(HalfMax),h->GetBinCenter(SPEHalfMaxBin));

  Gaus->SetParameters(
		      fit->GetParameter(0),
		      fit->GetParameter(1),
		      fit->GetParameter(2)
		      );
		      
  //  if(fit->GetParameter(3)/Gaus->Eval(fit->GetParameter(4)) < 3 || fit->GetParameter(4) < fit->GetParameter(2))
  if(fit->GetParameter(1) > 2E-12)
    {
      //      printf("Swapping peaks: %e\n",fit->GetParameter(3)/Gaus->Eval(fit->GetParameter(4)));
      printf("Swapping peaks: %e : %e\n",fit->GetParameter(1),fit->GetParameter(4));
      fit->SetParameter(0,fit->GetParameter(3));
      fit->SetParameter(1,fit->GetParameter(4));
      fit->SetParameter(2,fit->GetParameter(5));
 
      fit->SetParameter(3,Gaus->GetParameter(0));
      fit->SetParameter(4,Gaus->GetParameter(1));
      fit->SetParameter(5,Gaus->GetParameter(2));
    }

//  TLine * L1 = new TLine(h->GetBinCenter(NewMaxBin),0,h->GetBinCenter(NewMaxBin),h->GetMaximum());
//  TLine * L2 = new TLine(h->GetBinCenter(NewMinBin),0,h->GetBinCenter(NewMinBin),h->GetMaximum());
//  TLine * L3 = new TLine(SPEguess,0,SPEguess,h->GetMaximum());
//
//  L1->SetLineColor(kRed);
//  L2->SetLineColor(kBlue);
//  L3->SetLineColor(kGreen);
//
//  h->GetListOfFunctions()->AddLast(L1);
//  h->GetListOfFunctions()->AddLast(L2);
//  h->GetListOfFunctions()->AddLast(L3);

  return(fit);

}
