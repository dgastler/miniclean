#ifdef __RATDS__
#include <FEPCRATManager.h>

bool FEPCRATManager::GetUnSent(int32_t &index)
{
  bool ret = false;
  ret = UnSent.Get(index);
  return ret ;
}

int64_t FEPCRATManager::ReleaseStorageEvent(int32_t &index,
					    uint64_t &password)
{
  int64_t ret = returnDCVector::BAD_MUTEX;
  //Add time stamp
  RATEVBlock * ptr = GetFEPCBlock(index);
  if(ptr)
    ptr->AddTimeStamp(TimeStampCode::FE_DRPusher);
  //Update the storage event
  int64_t eventKey=GetKey(GetFEPCBlock(index));
  ret = StorageEvent.UpdateEntry(eventKey,
 				 index,
 				 password);
  if(ret == returnDCVector::OK)
    {
      ret = StorageEvent.ClearPassword(eventKey,
				       password);
    }
  else if(ret == returnDCVector::BAD_PASSWORD)
    {
      fprintf(stderr,"Error releasing event at index %"PRId32" with key %"PRId64" and password %"PRIu64"  (BAD_PASSWORD)\n",index,eventKey,password);
      //      fprintf(stderr,"Adding to event storage failed, so the event password was uncleared (but I just forced it)(error code %"PRId64")\n",ret);
      //      StorageEvent.ClearPassword(eventKey,password);
    }
  else
    {
      fprintf(stderr,"Error releasing event %"PRId64" (not BAD_PASSWORD)\n",eventKey);
      //      fprintf(stderr,"Adding to event storage failed, so the event password was uncleared (but I just forced it)(error code %"PRId64")\n",ret);
      //      StorageEvent.ClearPassword(eventKey,password);
    }

  return ret;
}

int64_t FEPCRATManager::GetStorageEvent(int64_t eventKey,
					int32_t &index,
					uint64_t &password)
{
  //Get the entry at Event
  return StorageEvent.GetEntry(eventKey,index,password);  
}


int64_t FEPCRATManager::ClearStorageEvent(int64_t eventKey,
					  uint64_t password)
{
  return StorageEvent.ClearEntry(eventKey,password);
}

int64_t FEPCRATManager::MoveStorageEventToBadQueue(int64_t eventKey,
						   uint64_t password,
						   Level::Type errorCode)
{  
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,
	  "FEPCRATManager::MoveStorageEventToBadQueue(eventKey=%zd,password=%zX,errorCode%d)\n",
	  eventKey,password,errorCode);
#endif 
  int64_t ret = returnDCVector::BAD_MUTEX;  
  int32_t index;
  //Get the index for this eventKey.
  ret = StorageEvent.GetEntry(eventKey,index,password);
  //Since we want to move whatever is at this entry, we could
  //get either the new or old eventKey.   This means that we should do the same
  //thing if we get either an OK or a collision
  if((ret == returnDCVector::OK)||(ret >= 0))
    {
      //Remove this event from the StorageEvent vector hash
      ret = StorageEvent.ClearEntry(eventKey,password);
      if(ret == returnDCVector::OK)
	//The event has been cleared from the vector hash
	{
	  //Add index to the Bad queue
	  BadEvent::sBadEvent badEvent;
	  badEvent.errorCode = errorCode;
	  badEvent.index = index;
	  badEvent.eventKey = eventKey;
	  //Add time stamp
	  RATEVBlock * ptr = GetFEPCBlock(index);
	  if(ptr)
	    ptr->AddTimeStamp(TimeStampCode::FE_BadEvent);
	  bool moveWorked = BadEvent.Add(badEvent);
	  if(moveWorked)
	    {
	      index = StorageEvent.GetBadValue();
	    }
	  else
	    {
	      ret = returnDCVector::EXTERNAL;
	    }
	}
      else
	//clear failed. 
	{
	  //We should just return the error we had
	}	
    }
  else
    {
      //Return the error we got from Get
    }
#ifdef __FUNCTION_DEBUG__
  fprintf(stderr,"\n");
#endif 
  return ret;
}
#endif
