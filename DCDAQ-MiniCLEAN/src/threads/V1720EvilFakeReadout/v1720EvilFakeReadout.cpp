#include <v1720EvilFakeReadout.h>

v1720EvilFakeReadout::v1720EvilFakeReadout(std::string Name)
{
  SetName(Name);
  SetType(std::string("v1720EvilFakeReadout"));
  blockIndex = FreeFullQueue::BADVALUE;
  block = NULL;
  Mem = NULL;  
}

v1720EvilFakeReadout::~v1720EvilFakeReadout()
{
  //clear pointer vector
  FakeEvent.clear();
  if(FakeWFD)
    {
      delete [] FakeWFD;
    }
  Mem->Shutdown();
}

void v1720EvilFakeReadout::SetPeriod(useconds_t _Period)
{
  Period = _Period;
//  SetSelectTimeout(
//		   ((Period*EventsPerBlock)/1000000),  //Seconds
//		   ((Period*EventsPerBlock)%1000000)   //useconds
//		   );
  SetDefaultSelectTimeout((Period*EventsPerBlock)/1000000, 
			 (Period*EventsPerBlock)%1000000);
}

bool v1720EvilFakeReadout::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Pausing thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      Loop = false;
      RemoveReadFD(Mem->GetFreeFDs()[0]);
      SetupSelect();
      if(Period!=0)
	{
	  SetDefaultSelectTimeout(60,0);
	}
    }
  else if(message.GetType() == DCMessage::GO)
    {
      DCtime_t tempTime = message.GetTime();
      printf("%s (%s): Starting up thread.\n",
	     GetTime(&tempTime).c_str(),
	     GetName().c_str());
      //Register the free queue's FD so we know when there are
      iEvent = 0;      
      if(Period == 0)
	{
	  AddReadFD(Mem->GetFreeFDs()[0]);
	}
      else
	{
	  SetPeriod(Period);
	}
      SetupSelect();
      Loop = true;
    }
  else
    {
      ret = false;
    }
  return(ret);
}
