#include "v1720.h"
#include "xmlHelper.h"

v1720::v1720()
{
  Handle = 0;
  BdType = cvV2718;
  Link = 0;
  BdNum = 0;
  VMEBaseAddress = 0;
  ChannelsConfig = 0;
  MemConfig = 0;
  ROCFPGA = 0;
  Ready = false;
}

v1720::~v1720()
{
  
  //Fix this!  We should close this handle, but we need to know it was set!
  //  CAENVME_End(Handle);
  Channels.clear();
  //clean up XML somethings.
}

int v1720::Start()
{
  int ret = 0;
  if(Ready)
    {
      //Make sure we don't mess up Acquisition control by
      //adding bit 2 to an already on bit2.
      ret += a32d32w(VMEBaseAddress + 0x8100,
		     (AcquisitionControl&0xFFFFFFFB) + 0x4);
    }
  else 
    {
      ret++;
    }
  return(ret);
}

int v1720::Stop()
{
  int ret = 0;
  if(Ready)
    {
      //Keep all bits the same except for bit 2
      ret += a32d32w(VMEBaseAddress + 0x8100,
		     AcquisitionControl&0xFFFFFFFB);
    }
  else 
    {
      ret++;
    }
  return(ret);
}

int v1720::SoftwareTrigger()
{
  int ret = 0;
  if(Ready)
    {
      ret += a32d32w(VMEBaseAddress + 0x8108,0x0);
    }
  else
    {
      ret++;
    }
  return(ret);
}

int v1720::Readout(MemoryBlock &block)
{
  int ret = 0;
  if(Ready)
    {
      uint32_t numberOfEvents = 0;
      uint32_t readoutSize = 0;
      ret += a32d32r(VMEBaseAddress + 0x812C,numberOfEvents);      
      readoutSize = numberOfEvents*EventSize;
      fprintf(stderr,"%d events\n",numberOfEvents);
      //    This might require some thought.  A larger memory buffer
      //    where we appened new data to old might be more efficient.
      if(readoutSize < block.allocatedSize)
	{
	  ret += a32MBLTr(VMEBaseAddress,
			  block.buffer,
			  block.allocatedSize,
			  block.dataSize);
	  //Figure out the difference between the MBLT and the FIFOMBLT
	  //Readout full amount and hopefully BERR ends transfer.
	  fprintf(stderr,"BERR after %05d bytes of %05d\n",block.dataSize,block.allocatedSize);
	}
      else
	{
	  fprintf(stderr,"Readout size larger than allocated buffer size.\n");
	  ret++;
	}
    }
  else
    {
      ret++;
    }
  return(ret);
}

bool v1720::TwoPack()
{
  bool ret = true;
  if((0x1 & (ChannelsConfig >> 11)) == 0x1)
    {
      ret = false;
    }
  return(ret);
}
