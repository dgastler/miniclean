#ifndef __PIPESELECT__
#define __PIPESELECT__

#include <stdlib.h>
#include <SelectValues.h>
#include <unistd.h>
#include <fcntl.h>
#include <StackTracer.h>

#define PIPE_ATTEMPTS 10;

class PipeSelect
{
 public:

  PipeSelect()
    {
      //Allocate our pipe
      int pipeReturn = pipe(Pipe);   
      if(pipeReturn == -1)           
	//something went wrong with our pipe
	{
	  printf("Warning:  Locked Queue failed to make FIFO! (error:%s)\n",
		 strerror(errno));	  
	  //Make sure the FIFOs are assigned to -1 if thing went wrong
	  Pipe[0] = -1;
	  Pipe[1] = -1;
	  //Cause code to fail
	  abort();
	}
      //Setup the pipes to be non-blocking
//      int currentFlags[2];
//      currentFlags[0] = fcntl(Pipe[0],F_GETFL,0);
//      currentFlags[1] = fcntl(Pipe[1],F_GETFL,0);
//      if((currentFlags[0] < 0 ) || (currentFlags[0] < 0))
//	{
//	  printf("Failed to get IO flags in PipeSelect Pipe (1)\n");
//	  abort();
//	}
//      //Set non-blocking bits
//      currentFlags[0] |= O_NONBLOCK;
//      currentFlags[1] |= O_NONBLOCK;           
//      currentFlags[0] = fcntl(Pipe[0],F_SETFL,currentFlags[0]);
//      currentFlags[1] = fcntl(Pipe[1],F_SETFL,currentFlags[1]);
//      //Error check
//      if((currentFlags[0] < 0 ) || (currentFlags[0] < 0))
//	{
//	  printf("Failed to get IO flags in PipeSelect Pipe (2)\n");
//	  abort();
//	}      
    }
  virtual ~PipeSelect()
    {
      //close our FIFO
      if(Pipe[1] != -1)
	{	
	  //unblock needed?
	  close(Pipe[1]);   //Close the pipe
	  Pipe[1] = -1;     //Make sure we think it's closed
	}
      if(Pipe[0] != -1)
	{
	  close(Pipe[0]);   //Close the pipe
	  Pipe[0] = -1;     //Make sure we think it's closed
	}
    }

  uint8_t Read()  
  {
    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"PipeSelect::Read()\n");
    #endif
    //Read one byte from the pipe to show that we got the message
    uint8_t readValue;
    
    //Do a read and return the read value
    int attemptsLeft = PIPE_ATTEMPTS;
    bool error = false;
    while(read(Pipe[0],&readValue,sizeof(readValue)) == -1)
      {
	//If this was a blocking issue, our pipe is oddly empty and return EMPTY
	if((errno == EAGAIN) ||
	   (errno == EWOULDBLOCK))
	  {
	    readValue=SelectValues::EMPTY;
	    error = true;
	    break;
	  }
	//If we have an interrupt that prevented the read, we have attemptsLeft retry, then a FAIL
	else if(errno == EINTR)
	  {
	    if(--attemptsLeft <= 0)
	      {
		readValue=SelectValues::FAIL;
		error = true;
		break;
	      }	    
	  }
	//Anything else, just fail
	else
	  {
	    readValue=SelectValues::FAIL;
	    error = true;
	    break;
	  }
      }
    if(error)
      {
	//If there is an error
	fprintf(stderr,"Error in pipe read: %s\n",strerror(errno));
      }
    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"PipeSelect::Read(): returned %d\n\n",readValue);
    #endif
    return readValue;
  }
  
  uint8_t Write()
  {
    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"PipeSelect::Write()\n");
    #endif
    uint8_t val = SelectValues::OK;
    //Write the val into the pipe and return how it went.
    int attemptsLeft = PIPE_ATTEMPTS;
    bool error = false;
    while(write(Pipe[1],&val,sizeof(val)) == -1)
      {
	//If this was a blocking issue, our pipe is full and return FULL
	if((errno == EAGAIN) ||
	   (errno == EWOULDBLOCK))
	  {
	    val=SelectValues::FULL;
	    error = true;
	    break;
	  }
	//If we have an interrupt that prevented the read, we have attemptsLeft retry, then a FAIL
	else if(errno == EINTR)
	  {
	    if(--attemptsLeft <= 0)
	      {
		val=SelectValues::FAIL;
		error = true;
		break;
	      }	    
	  }
	//Anything else, just fail
	else
	  {
	    val=SelectValues::FAIL;
	    error = true;
	    break;
	  }
      }
    if(error)
      {
	//We had an error
	fprintf(stderr,"Error in queue FIFO write: %s\n",strerror(errno));
	print_trace();
      }

    #ifdef __FUNCTION_DEBUG__
    fprintf(stderr,"PipeSelect::Write(): returned %d\n",val);
    #endif
    return val;
  }
  

  const int * GetFDs() {return(Pipe);}; //Return a constant pointer to the FDs
private:
  //internal pipe
  int Pipe[2];
};

#endif
