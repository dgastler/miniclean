#ifndef __TEST_BENCH_RAT_MANAGER__
#define __TEST_BENCH_RAT_MANAGER__

#include <MemoryManager.h>
#include <xmlHelper.h>
#include <TestBenchBlock.h>
#include "RAT/DS/EV.hh"

#include <DCVectorHash.h>
#include <DCQueue.h>


class TestBenchRATManager : public MemoryManager
{
 public:
  //================================================================
  //Memory manager setup
  //================================================================
  TestBenchRATManager(std::string Name);
  ~TestBenchRATManager();
  bool AllocateMemory(xmlNode * setupNode);
  
  virtual void WakeUp();
  virtual void Shutdown();

  //================================================================
  //Memory manager status
  //================================================================
  virtual void PrintStatus(int indent = 0);

  //================================================================
  //File descriptor access
  //================================================================
  const int * GetFreeFDs(){return(Free.GetFDs());};
  const int * GetDiskFDs(){return(Disk.GetFDs());};

  //================================================================
  //Memory access
  //================================================================
  //Get the defined bad value
  int32_t GetBadValue(){return badValue;};

  //Block access
  TestBenchBlock * GetTestBenchBlock(uint32_t iTestBench) 
  {
    if(iTestBench < Memory.size())
      {
	return(&(Memory[iTestBench]));
      }
    return(NULL);
  };  
  
  //================================================================
  //Index access
  //================================================================

  //================================================================
  //AssembledEvent access  (RATAssembler)
  //================================================================
  //Gets an event from the AssembledEvent vector-hash. Detects collisions
  int64_t GetAssembledEvent(int64_t eventID,
			    int32_t &index,
			    uint64_t &password);
  //Returns and clears PW for the event at index. (does not molest index if there is a collision)
  int64_t ReturnAssembledEvent(int32_t &index,uint64_t password);
  int64_t MoveAssembledEventToDiskQueue(int64_t eventID,uint64_t password);  
  uint64_t GetAssembledBlankPassword(){return AssembledEvent.GetBlankPassword();};
  //================================================================

  //================================================================
  //Disk queue
  //================================================================
  bool     GetDisk(int32_t &index);
  bool     AddDisk(int32_t &index);
  bool     AddFree(int32_t &index);
  //================================================================

 
 private:   
  //================================================================
  //Data storage and sizes
  //================================================================
  //Memory vector and it's size from the setup XML
  uint32_t NumTestBenchBlocks;
  std::vector<TestBenchBlock> Memory;
  //Size of AssembledEvent vectorhash
  uint32_t AssembledEventSize;
  //Shutdown status
  bool ShutdownStatus;

  int32_t badValue;
  int32_t emptyValue;

  //================================================================
  //QUEUES
  //================================================================
  //queue of indices of free
  DCQueue<int32_t,PipeSelect,LockedObject> Free;  
  //queue of indices of memory blocks to be written to disk
  DCQueue<int32_t,PipeSelect,LockedObject> Disk;  

  //================================================================
  //Vector hashes
  //================================================================
  //vector-hash of events being assembled
  DCVectorHash<int32_t,LockedObject> AssembledEvent; 

  //================================================================
  //Deallocation of all the data objects
  //================================================================  
  void Deallocate();
};

#endif
