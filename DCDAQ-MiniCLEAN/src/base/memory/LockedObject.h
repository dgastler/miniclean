#ifndef __LOCKEDOBJECT__
#define __LOCKEDOBJECT__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h> //abort()
#include <errno.h> //EBUSY
#include <pthread.h>

class LockedObject
{
 public:
  LockedObject();
  virtual          ~LockedObject();

  void     ShutdownLockWait(){Shutdown = true;};
  void     WakeUpSignal(){pthread_cond_broadcast(&cond);};  

  bool LockObject(bool wait = true);
  bool ConditionWait();
  bool UnlockObject()
  {
    bool ret = false;
    if(pthread_mutex_unlock(&mutex) == 0)
	ret = true;
    return ret;
  }
  bool GetShutdown(){return(Shutdown);};  

 private:
  volatile bool Shutdown;  
    
  //Mutex for this object
  pthread_mutex_t   mutex; //Mutex lock for vector access
  pthread_cond_t    cond; //Condition variable for the mutex

  //================================
  //Disallow coping of LockedObjects
  //================================
  LockedObject(const LockedObject&);              //Never implement these functions
  LockedObject& operator=(const LockedObject&);   //Never implement these functions
};

#endif
