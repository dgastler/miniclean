#ifndef CUTPARAMETERS
#define CUTPARAMETERS

#include <iostream>
#include <math.h>

#include "xmlParser.h"
#include "PEvent.h"


class Cut_Parameters 
{
 private:

 public:
  int   Type;

  float V0;
  float V1;

  float W0;
  float W1;
  
  float PMTLinearity1;
  float PMTLinearity2;
  
  int   PMTTiming;
  int   TOFmin;
  int   TOFmax;
  
  float PMT2psa0;
  float PMT2psa1;
  float PMT2psa2;
  float PMT2psa3;
  
  float TBRatio;
  float Fast;

  bool  bPMTLinearity1;
  bool  bPMTLinearity2;

  bool  bPMTTiming;
  bool  bTOFmin;
  bool  bTOFmax;
  
  bool  bPMT2psa;

  bool  bTBRatio;
  bool  bFast;

  Cut_Parameters();
  virtual ~Cut_Parameters() {};
  int SetCuts(XMLNode *xMainNode);
  bool PassCuts(PEvent * pevent);
};

#endif
