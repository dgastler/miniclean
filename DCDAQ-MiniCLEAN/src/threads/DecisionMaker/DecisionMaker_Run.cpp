#ifdef __RATDS__

#include <DecisionMaker.h>

void DecisionMaker::MainLoop()
{
  if(IsReadReady(DRPC_mem->GetUnProcessedFDs()[0]))
    {
      //Get the next unprocessed event. 
      int32_t index;
      if(DRPC_mem->GetUnProcessed(index))
	//got the next unprocessed
	{
	  //Check that the index returned doesn't give an error
	  if(index != DRPC_mem->GetBadValue())
	    {
	      //Get the RATEVBlock for this event
	      RATEVBlock * event = DRPC_mem->GetDRPCBlock(index);

	      //Check that the trigger box event ID matches the event header	  
	      int WFDCount = event->ev->GetBoardCount();
	      bool passedIDCheck = true;
	      bool passedWFDSyncCheck = true;
	      uint16_t wfdSyncCheck = 0;
	      for(int WFDIndex = 0; WFDIndex<WFDCount; WFDIndex++)
		{
		  RAT::DS::Board * board = event->ev->GetBoard(WFDIndex);
		  std::vector<uint32_t> boardHeader = board->GetHeader();	
		  uint8_t headerPatternGC = (boardHeader[1] & 0xFF00)>>8;
		  if(checkHeaders==1)
		    {
		      uint8_t headerPattern = DecodeGrayCode(headerPatternGC);
		      //Compare header pattern with event ID
		      if(headerPattern != (event->eventID & 0xFF))
			{
			  passedIDCheck = false;
			}

		      //Check LVDS values
		      if(WFDIndex == 0)
			wfdSyncCheck = (boardHeader[1] >> 8)&0xFFFF;
		      else
			{
			  if(((boardHeader[1] >> 8)&0xFFFF) != wfdSyncCheck)
			    {
			      passedWFDSyncCheck = false;
			    }
			}
		    }
		  event->ev->SetTriggerPattern(wfdSyncCheck,
					       event->eventTriggers);
		}
	      
	      //This is the end of the world
	      if(!passedWFDSyncCheck)
		{		 
		  std::stringstream ss;
		  ss << "WFD headers do not match! Event: "
		     << event->eventID <<std::endl
		     << "TriggerMask: " << std::hex << unsigned(event->ev->GetTriggerPattern()) << std::dec 
		     << "\nTrigger counts:";
		  for(uint8_t i = 0; i < 8;i++)
		    {
		      ss << " " << unsigned(i) << ":" << unsigned(event->ev->GetTriggerCount(i));
		    }
		  ss << std::endl;
		  int WFDCount = event->ev->GetBoardCount();
		  for(int WFDIndex = 0; WFDIndex<WFDCount; WFDIndex++)
		    {
		      std::vector<uint32_t> boardHeader = event->ev->GetBoard(WFDIndex)->GetHeader();	
		      int wfdID = event->ev->GetBoard(WFDIndex)->GetID();
		      uint16_t headerPattern = (boardHeader[1] & 0xFFFF00)>>8;
		      ss << "\t WFD(" <<  wfdID << ") Pattern:"
			 << std::hex << (unsigned)headerPattern 
			 << std::dec
			 << "      Time:" << std::hex << boardHeader[3]
			 << std::dec << "\n";
		    }
		    PrintWarning(ss.str().c_str());
		    //PrintError(ss.str().c_str());
#ifndef USE_EVENT_TIME_FOR_KEY
		    DRPC_mem->AddBadEvent(event->eventID,index,Level::LVDS_MISMATCH);
#endif
		}
	      //This is a bad event
	      else if(!passedIDCheck)
		{
		  std::stringstream ss;
		  ss << "WFD header does not match event number! Event: "
		     << event->eventID << "(" 
		     << unsigned((event->eventID)&0xFF) << ")" <<std::endl
		     << "Event time: " << std::hex << unsigned((event->eventTime)&0x7FFFFFFF) << std::endl
		     << "TriggerMask: " << std::hex << unsigned(event->ev->GetTriggerPattern()) << std::dec
		     << "\nTrigger counts:";
		  for(uint8_t i = 0; i < 8;i++)
		    {
		      ss << " " << unsigned(i) << ":" << unsigned(event->ev->GetTriggerCount(i));
		    }
		  ss << std::endl;
		  int WFDCount = event->ev->GetBoardCount();
		  for(int WFDIndex = 0; WFDIndex<WFDCount; WFDIndex++)
		    {
		      std::vector<uint32_t> boardHeader = event->ev->GetBoard(WFDIndex)->GetHeader();	
		      int wfdID = event->ev->GetBoard(WFDIndex)->GetID();
		      uint8_t headerPatternGC = (boardHeader[1] & 0xFF00)>>8;
		      ss << "\t WFD(" << wfdID << ") Pattern:"
			 << (unsigned)DecodeGrayCode(headerPatternGC) 
			 << "( Gray Code: " << (unsigned)headerPatternGC 
			 << "    Time: " << std::hex << unsigned((boardHeader[3])&0x7FFFFFFF) << std::dec << ")\n";
		    }
#ifndef USE_EVENT_TIME_FOR_KEY
		  PrintWarning(ss.str().c_str());		  
		  DRPC_mem->AddBadEvent(event->eventID,index,Level::LVDS_MISMATCH);
#endif
		}
	      else
		{
		  //Check the trigger pattern of this event to decide what to do with it
		 
		  //Pass the block to the ComputeDecision function
		  ComputeDecision(event);	 
		  //Send event on to the respond queue
		  if(!DRPC_mem->AddRespond(index))
		    {
		      //We are probably shutting down in this case
		      PrintError("Can't add to respond. DCDAQ shutdown?\n");
		      SendStop();
		    }
		}
	    }
	  else
	    {
	      PrintError("Bad event number\n");
	      //We are probably shutting down in this case
	    }
	}
      else
	//We couldn't get the next unprocessed
	{
	  //We are probably shutting down in this case
	  PrintError("Can't get a UnProcessed event.  DCDAQ shutdown?\n");
	  SendStop();
	}
    }
}

bool DecisionMaker::ProcessMessage(DCMessage::Message &message)
{
  bool ret = true;
  if(message.GetType() == DCMessage::STOP)
    {
      Loop = false;
      Running = false;
    }
  else if(message.GetType() == DCMessage::PAUSE)
    {
      Loop = false;
      //We should no longer pay attention to the UnProcessed queue
      RemoveReadFD(DRPC_mem->GetUnProcessedFDs()[0]);
      SetupSelect();
    }
  else if(message.GetType() == DCMessage::GO)
    {
      Loop = true;
      //We now need to pay attention to the UnProcessed queue
      AddReadFD(DRPC_mem->GetUnProcessedFDs()[0]);
      SetupSelect();
    }
  else
    {
      ret = false;
    }
  return(ret);
}

#endif
