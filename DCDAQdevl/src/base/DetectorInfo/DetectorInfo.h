#ifndef __DETECTOR_INFO__
#define __DETECTOR_INFO__

#include <stdint.h>  //int*_t s

#include <vector>
#include <string>

#include <xmlHelper.h>

namespace InfoTypes 
{
  enum 
  {
    NONE           = 0x0000,
    
    WFD_COUNT      = 0x0001,
    BOARD_NODE     = WFD_COUNT << 1,
    CHANNEL_MAP    = BOARD_NODE << 1,
    PMT_OFFSET     = CHANNEL_MAP << 1,
    PMT_PRESAMPLES = PMT_OFFSET << 1,
    PMT_ID         = PMT_PRESAMPLES << 1,
    
    EVENT_START    = PMT_ID << 1,
    EVENT_END      = EVENT_START << 1,
    EVENT_PROMPT   = EVENT_END << 1
    
  };
};
class DetectorInfo
{
 public:
  DetectorInfo()
  {
    WFDCount = 0;
    ChannelMap.clear();
    PMTOffset.clear();
    PMTPreSampleCount.clear();
  };
  ~DetectorInfo(){};
  
  uint32_t Setup(xmlNode * SetupNode);

  //PMT offset interface
  double GetPMTOffset(size_t iPMT){return PMTOffset[iPMT];};
  const std::vector<double> & GetPMTOffset(){return PMTOffset;};
  //PMT presamples interface
  uint32_t GetPMTPreSamples(size_t iPMT){return PMTPreSampleCount[iPMT];};
  const std::vector<uint32_t> & GetPMTPreSamples(){return PMTPreSampleCount;};
  //WFDCount interface
  size_t GetWFDCount(){return WFDCount;};

  //PMT channel map interfaces
  const static int BAD_CHANNEL_ID = -1;
  const std::vector< std::vector< int16_t > > & GetChannelMap(){return ChannelMap;};
  const std::vector< int16_t > & GetChannelMap(size_t iWFD){return ChannelMap[iWFD];};
  int16_t GetChannelMap(size_t iWFD, size_t iChan){return ChannelMap[iWFD][iChan];};

  uint32_t GetEventStartTime() {return eventStartTime;};
  uint32_t GetEventEndTime(){return eventEndTime;};
  uint32_t GetPromptIntegrationEndTime(){return promptIntegrationEndTime;};


  uint32_t GetInfoTypesFound(){return infoTypesFound;};
  bool WasFound(uint32_t type){return ((infoTypesFound & type) == type);};

 private:

  uint32_t infoTypesFound;

  //================================
  //WFD layout data
  //================================
  struct PMTInfo 
  {
    uint8_t WFDPos;
    uint16_t PMTID;
    double ADCOffset;
    uint8_t PreSamples;
    PMTInfo()
    {
      WFDPos = 0xFF;
      PMTID = 0xFFFF;
      ADCOffset = 0;
      PreSamples = 0;
    }
  };
  //================================
  //WFD layout data
  //================================
  //WFDs per partial event. 
  size_t WFDCount;
  //ChannelMap[WFD board ID(0-31)][position on WFD(0-7)] 
  //gives the channel ID (BAD_CHANNEL_ID is bad)
  std::vector< std::vector< int16_t > > ChannelMap;
  //gives the used positions for each WFD
  //PMTOffset[PMTID] gives the ADC offset value
  std::vector<double>  PMTOffset;
  //PMTPreSampleCount[PMTID] gives the number of ZLE presamples
  std::vector<uint32_t> PMTPreSampleCount;
  //================================
  //Event property data
  //================================
  //Start time for physics data in an event
  //This is also the prompt windown start time. (clock ticks)
  uint32_t eventStartTime;
  //End time for physics data in an event  (clock ticks)
  uint32_t eventEndTime;
  //End of the Prompt window (clock ticks)
  uint32_t promptIntegrationEndTime;


  //================================
  //XML parsing functions
  //================================
  uint32_t ProcessWFDNode(xmlNode * wfdNode);
  uint32_t ProcessPMTNode(xmlNode * wfdNode,std::vector<PMTInfo> & pmtInfo,uint16_t boardID);
  template<class T>
  bool GetPMTValue(xmlNode *node,std::string name,T & value);
  uint32_t ProcessEventData(xmlNode * SetupNode);
};
#endif
