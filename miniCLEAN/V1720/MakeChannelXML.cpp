#include <libxml/parser.h>
#include <libxml/tree.h>

int main(int argc, char ** argv)
{
  if(argc < 2)
    {
      printf("Usage: %s XML-file\n",argv[0]);
      return(0);
    }

  xmlDoc *doc = NULL;
  xmlNode *root_element = NULL;

  //Open and parse file.
  //  doc = xmlReadFile(argv[1], NULL, 0);
  doc = xmlNewDoc((xmlChar*)"1.0");
  if (doc == NULL) 
    {
      printf("error: could not create new xmlstream\n");
      //      printf("error: could not parse file %s\n", argv[1]);
    }
  //  root_element = xmlNewDocRawNode(doc,NULL,(xmlChar*)"WFD",NULL);
  root_element = xmlNewNode(NULL,(xmlChar*)"WFD");
  xmlDocSetRootElement(doc,root_element);
  xmlNewChild(root_element,NULL,(xmlChar*)"STUFF",(xmlChar*)"1");

  
  xmlSaveFormatFile(argv[1],doc,1);

  return(0);
}
