--This module serves as a wrapper for RAM that stores incoming packets.
--Packets are differentiated from one another by maintaining an array of
--their start addresses. Once MAC_RXPACKETOK strobes high, signalling the
--successful reception of a packet, this module updates the array 
--accordingly and indicates (via PACKET_RXREADY) that a packet is present.
--PACKET_RXREADY drops back to low after PACKET_RXDONE strobes high.
--Original code by Jeremy Mans.
--Translation from Verilog into VHDL along with support for 100Mbps
--speed by Boston University Electronics Design Facility--Conor DuBois.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity gbe_rxpacketbuffer is Port(
	MAC_RXD : in STD_LOGIC_VECTOR(7 downto 0);
	MAC_RXDV : in STD_LOGIC;
	MAC_CKE : in STD_LOGIC;
	SEND_CKE : in STD_LOGIC;
	RESET : in STD_LOGIC;
	MAC_RXPACKETOK : in STD_LOGIC;
	MAC_RXPACKETBAD : in STD_LOGIC;
	MAC_CLK : in STD_LOGIC;
	PACKET_RXD : out STD_LOGIC_VECTOR(7 downto 0);
	PACKET_RXW : out unsigned(31 downto 0);
	PACKET_RXA : in unsigned(10 downto 0);
	PACKET_RXREADY : out STD_LOGIC;
	PACKET_RXDONE : in STD_LOGIC);
end gbe_rxpacketbuffer;

architecture Behavioral of gbe_rxpacketbuffer is
	signal packet_len_acc : unsigned(10 downto 0);
	--store up to eight incoming packets at once
	type multiple is ARRAY(7 downto 0) of unsigned(10 downto 0);
	signal packet_len_fifo : multiple;
	signal packet_len_wp : unsigned(2 downto 0);
	signal packet_len_rp : unsigned(2 downto 0);
	signal inByteBuffer : STD_LOGIC_VECTOR(7 downto 0);
	signal was_packetok : STD_LOGIC;
	signal was_rxdone : STD_LOGIC;
	signal was_dv : STD_LOGIC;
	signal buffer_write_base : unsigned(11 downto 0) := x"002";
	signal buffer_read_base : unsigned(11 downto 0) := x"002";
	signal buffer_addr_write : unsigned(11 downto 0);
	signal buffer_addr_read : unsigned(11 downto 0);
	signal or_dv : STD_LOGIC;
	signal packet_l : unsigned(10 downto 0);

	component dpbr port(
		RESET : in STD_LOGIC;
		CLKA : in STD_LOGIC;
		WEA : in STD_LOGIC;
		ADDRA : in STD_LOGIC_VECTOR(11 downto 0);
		DINA : in STD_LOGIC_VECTOR(7 downto 0);
		CLKB : in STD_LOGIC;
		CLKEN : in STD_LOGIC;
		ADDRB : in STD_LOGIC_VECTOR(11 downto 0);
		DOUTL : out STD_LOGIC_VECTOR(7 downto 0);
		DOUTB : out STD_LOGIC_VECTOR(31 downto 0));
	end component;
begin
	thebuffer : dpbr port map(
		RESET => RESET,
		CLKA => MAC_CLK,
		WEA => or_dv,
		ADDRA => STD_LOGIC_VECTOR(buffer_addr_write),
		DINA => inByteBuffer,
		CLKB => MAC_CLK,
		CLKEN => SEND_CKE,
		ADDRB => STD_LOGIC_VECTOR(buffer_addr_read),
		DOUTL => PACKET_RXD,
		unsigned(DOUTB) => PACKET_RXW);

	or_dv <= (MAC_RXDV or was_dv) and MAC_CKE;
	PACKET_RXREADY <= '0' when packet_len_wp = packet_len_rp else '1';
	buffer_addr_read <= buffer_read_base + ('0' & PACKET_RXA);

	process(MAC_CLK)
	begin
		if MAC_CLK'event and MAC_CLK = '1' then
			was_rxdone <= PACKET_RXDONE;
			packet_l <= packet_len_fifo(to_integer(packet_len_rp));
			if RESET = '1' then
				buffer_write_base <= x"002";--pointer to each packet start ends in "10" to help with UDP parsing
				packet_len_wp <= "000";
				packet_len_acc <= "000" & x"00";
				packet_len_rp <= "000";
				buffer_read_base <= x"002";
			else
				if (PACKET_RXDONE = '1') and (was_rxdone = '0') then
					packet_len_rp <= packet_len_rp + "001";
					buffer_read_base <= buffer_read_base + (('0' & packet_l) and x"FFC") + x"004";
				end if;
				if MAC_CKE = '1' then
					buffer_addr_write <= buffer_write_base + ('0' & packet_len_acc);
					inByteBuffer <= MAC_RXD;
					was_dv <= MAC_RXDV;
					was_packetok <= MAC_RXPACKETOK;
					if (MAC_RXPACKETOK = '1') and (was_packetok = '0') then
						--keep the packet
						packet_len_fifo(to_integer(packet_len_wp)) <= packet_len_acc;
						buffer_write_base <= buffer_write_base + (('0' & packet_len_acc) and x"FFC") + x"004";
					elsif (MAC_RXPACKETOK = '0') and (was_packetok = '1') then
						--keep the packet (inc count)
						packet_len_wp <= packet_len_wp + "001";
						packet_len_acc <= "000" & x"00";
					elsif MAC_RXPACKETBAD = '1' then
						--drop the packet (forget the length)
						packet_len_acc <= "000" & x"00";
					elsif MAC_RXDV = '1' then
						packet_len_acc <= packet_len_acc + ("000" & x"01");
					end if;
				end if;
			end if;
		end if;
	end process;
end Behavioral;
