//Function to run thread.
#include <pthread.h>

#include <ThreadStart.h>


void * start_thread(void * _pointer)
{
  //Call the run function from the thread class.
  DCThread * pointer = (DCThread *) _pointer;
  return(pointer->Run(NULL));
}
