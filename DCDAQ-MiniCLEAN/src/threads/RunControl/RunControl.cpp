#include <RunControl.h>

RunControl::RunControl(std::string Name)
{
  Ready = false;
  shuttingDownStopAddresses = false;
  SetType("RunControl");
  SetName(Name);  
  //  lastTime = 0;
  gettimeofday(&lastTime,NULL);
  waitTime.tv_sec  = 0;
  waitTime.tv_usec =  100000;
  waitTimeBase.tv_sec  = waitTime.tv_sec  ;
  waitTimeBase.tv_usec = waitTime.tv_usec ;
  maxThreadShutdownCount = 1000000;
  run_error = false;
  inShutdownScript = false;
}

bool RunControl::Setup(xmlNode * SetupNode,
		       std::vector<MemoryManager*> &MemManager)
{
  bool ret = true;

  //Check if thre is a route error node and set the ErrorRoute if it exists
  xmlNode * eRouteNode;
  if(NULL != (eRouteNode = FindSubNode(SetupNode,"ROUTEERRORS")))
    {
      ErrorRoute.Set(eRouteNode);
    }
  //Check if thre is a route Warning node and set the WarningRoute if it exists
  xmlNode * wRouteNode;
  if(NULL != (wRouteNode = FindSubNode(SetupNode,"ROUTEWARNINGS")))
    {
      WarningRoute.Set(wRouteNode);
    }

  //Autostart
  if(NULL != FindSubNode(SetupNode,"GO"))
    {
      SendStartMessage();
    }

  //Auto shutdown
  autostop = false;
  if(NULL != FindSubNode(SetupNode,"AUTOSTOP"))
    {
      autostop = true;
    }

  //Uppdate RUNLOCK when the run is over (blank if none exists)
  std::string update_runlock_name("");
  if(NULL != FindSubNode(SetupNode,"UPDATE_RUNLOCK_NAME"))
    {
      GetXMLValue(SetupNode,"UPDATE_RUNLOCK_NAME","RunControl",update_runlock_name);
      //      updateRUNLOCK.Set(FindSubNode(SetupNode,"UPDATE_RUNLOCK"));
    }
  //Uppdate RUNLOCK when the run is over (blank if none exists)
  std::string update_runlock_addr("");
  if(NULL != FindSubNode(SetupNode,"UPDATE_RUNLOCK_ADDR"))
    {      
      GetXMLValue(SetupNode,"UPDATE_RUNLOCK_ADDR","RunControl",update_runlock_addr);
    }

  updateRUNLOCK.Set(update_runlock_name,update_runlock_addr);

  //Load opBlock nodes
  size_t opBlockCount = NumberOfNamedSubNodes(SetupNode,"OPBLOCK");
  for(size_t iOpBlock = 0; iOpBlock < opBlockCount;iOpBlock++)
    {
      xmlNode * opBlockNode = FindIthSubNode(SetupNode,"OPBLOCK",iOpBlock);
      if(opBlockNode != NULL)
	{
	  //Add new OpBlock to the vector
	  OpBlock opBlock;
	  opBlock.Clear();
	  if(opBlock.Load(opBlockNode))
	    {
	      setupBlocks.push_back(opBlock);
	    }
	  else
	    {
	      std::stringstream ss;
	      ss << "Failed on OpBlock: " << iOpBlock << std::endl;
	      Print(ss.str());
	      ret = false;
	      iOpBlock = opBlockCount;
	      break;
	    }
	}
    }  
  
  //Load shutdown opblock rules
  xmlNode * stopOpBlocksNode = FindSubNode(SetupNode,"STOPOPBLOCKS");
  if(stopOpBlocksNode != NULL)
    {
      //      StopOpBlocks=true;
      //Load stop opBlock nodes
      size_t stopOpBlockCount = NumberOfNamedSubNodes(stopOpBlocksNode,"OPBLOCK");
      for(size_t iOpBlock = 0; iOpBlock < stopOpBlockCount;iOpBlock++)
	{
	  xmlNode * opBlockNode = FindIthSubNode(stopOpBlocksNode,"OPBLOCK",iOpBlock);
	  if(opBlockNode != NULL)
	    {
	      //Add new OpBlock to the vector
	      OpBlock opBlock;
	      opBlock.Clear();
	      if(opBlock.Load(opBlockNode))
		{
		  StopBlocks.push_back(opBlock);
		}
	      else
		{
		  std::stringstream ss;
		  ss << "Failed on OpBlock: " << iOpBlock << std::endl;
		  Print(ss.str());
		  ret = false;
		  iOpBlock = stopOpBlockCount;
		  break;
		}
	    }
	}        
    }
  else
    {
      //      StopOpBlocks=false;
    }

  //We start in the RunSetupComplete false state.
  RunSetupComplete = false;

  Ready = ret;
  return ret;  
}

void RunControl::ProcessNetwork(DCMessage::Message &message)
{
  std::stringstream ss;
  ss << "Got network from " << message.GetSource().GetStr() ;
  NetworkStatus::Type status = NetworkStatus::BLANK;
  if(message.GetData(status))
    {
      ss << ": " << NetworkStatus::GetTypeName(status) << std::endl;
    }
  Print(ss.str().c_str());

  if((status == NetworkStatus::DISCONNECTED ) ||
     (status == NetworkStatus::RETRY )||
     (status == NetworkStatus::FAILED ) ||
     (status == NetworkStatus::BLANK ))
    {
      if((!shuttingDownStopAddresses)&&(!inShutdownScript))
	{	  
	  PrintWarning("ProcessNetwork: Calling Stop with error = true");
	  Stop(true);
	}
    }
}

void RunControl::ProcessError(DCMessage::Message &message)
{
  std::stringstream ss;
  ss << "Got error from " << message.GetSource().GetStr() ;
  DCMessage::SingleUINT64 singleUINT64;
  if(message.GetDataStruct(singleUINT64))
    {
      ss << ": " << singleUINT64.text << std::endl;
    }
  Print(ss.str().c_str());

  if(!ErrorRoute.IsBlank())
    {
      message.SetDestination(ErrorRoute);
      ForwardMessageOut(message);
    }
  if(!shuttingDownStopAddresses)
    {
      PrintWarning("ProcessError: Calling Stop with error = true; " + ss.str());
      Stop(true);
    }
}

void RunControl::ProcessWarning(DCMessage::Message &message)
{
  if(!WarningRoute.IsBlank())
    {
      message.SetDestination(WarningRoute);
      ForwardMessageOut(message);
    }
}
