#include <DRSender.h>

bool DRSender::ProcessNewConnection()
{
  //create a DCNetThread for this new connection
  if(server.ProcessConnection(managerNode))
    {
      //Get the FDs for the free queue of the outgoing packet manager
      //Add the incoming packet manager fds to list
      AddReadFD(server.Back().thread->GetInPacketManager()->GetFullFDs()[0]);
      
      //Get and add the outgoing message queue of this 
      //DCNetThread to select list
      AddReadFD(server.Back().thread->GetMessageOutFDs()[0]);      
      server.Back().thread->Start(NULL);
      printf("FEPC::DRGetter connection(%d) from: %s\n",
	     int(server.Size()),
	     server.Back().remoteIP.c_str());
      SetupSelect();
    }
 else
   {
     //Connection creation failed
     return(false);
   }
  return(true);
}

void DRSender::DeleteConnection(unsigned int iConn)
{
  //Check that iConn is physical
  if(iConn >= server.Size())
    return;
 
  //Making sure that the thread actually exists
  if(server[iConn].thread != NULL)
    {
      //Remove the incoming packet manager fds from list
      RemoveReadFD(server[iConn].thread->GetInPacketManager()->GetFullFDs()[0]);
      //Remove the outgoing message queue from the DCThread
      RemoveReadFD(server[iConn].thread->GetMessageOutFDs()[0]);          
    }
  SetupSelect();
  server.DeleteConnection(iConn);
}

void DRSender::ProcessChildMessage(size_t iConn)
{
  //Get the new message from connection iConn
  DCMessage::Message message = server[iConn].thread->GetMessageOut(true);

  //delete the connection of it has gone bad
  if(message.GetType() == DCMessage::ERROR)
    {
      DeleteConnection(iConn);
    }
  //We don't really care about statistics on this thread
  //if you do change this to true
  else if(message.GetType() == DCMessage::STATISTIC)
    {
      //      SendMessageOut(message,true);
    }
  else
    {
      char * buffer = new char[1000];
      sprintf(buffer,"Unknown message (%d) from DRAssembler's DCNetThread\n",message.GetType());
      PrintWarning(buffer);
      delete [] buffer;
    }
}
