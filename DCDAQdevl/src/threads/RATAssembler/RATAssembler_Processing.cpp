#include <RATAssembler.h>
#include <fstream>
#include <iostream>

void RATAssembler::ProcessZLEWindow(RAT::DS::Raw * raw,
				    RAT::DS::RawWaveform * rawWaveform,
				    int PMTID)
{
  //Find the offset for this PMT channel
  double offset  = detectorInfo.GetPMTOffset(PMTID);
  size_t preSamples = detectorInfo.GetPMTPreSamples(PMTID);
  size_t preSamplesUsed = 0;
  
  //Integral and baseline values (integer and double)
  double dIntegral = 0;
  uint32_t iIntegral = 0;

  double dBaseline = 0;
  double dBaselineRMS = 0;
  uint32_t iBaseline = 0;
  uint32_t iBaselineSquared = 0;

  
  //Get the parsed 12bit samples (packed in 16bit ints)
  std::vector<uint16_t> samples = rawWaveform->GetSamples();
  for(size_t iSample = 0; iSample < samples.size();iSample++)
    {
      //Baseline calculation
      if(iSample < preSamples)
	{
	  iBaseline        += uint32_t(samples[iSample]);
	  iBaselineSquared += (uint32_t(samples[iSample])*
			       uint32_t(samples[iSample]));
	  preSamplesUsed++;
	}
      //Integral calculation 
      iIntegral += uint32_t(samples[iSample]);
    }
  
  //Calculate the ZLE baseline
  dBaseline = double(iBaseline)/preSamplesUsed;
  dBaselineRMS = sqrt((preSamplesUsed * iBaselineSquared) - 
		      (iBaseline * iBaseline))/(preSamplesUsed*preSamplesUsed);
  
  //Calculate the integral
  dIntegral = (double(iIntegral) - (offset*samples.size()))*(-1);

  //  filestr<<"PMTOffset: "<<offset<<std::endl;
  //  filestr<<"Samples_size: "<<samples.size()<<std::endl;
  //  filestr<<"iIntegral: "<<iIntegral<<std::endl;
  //  filestr<<"dIntegral: "<<dIntegral<<std::endl;

  //Adda new raw integral structure and fill it. 
  RAT::DS::RawIntegral * rawIntegral = raw->AddNewRawIntegral();
  rawIntegral->SetIntegral(dIntegral);
  rawIntegral->SetStartTime(rawWaveform->GetTimeOffset());
  rawIntegral->SetWidth(samples.size());
  rawIntegral->SetBaseline(dBaseline);
  rawIntegral->SetBaselineRMS(dBaselineRMS);
}

int RATAssembler::ProcessOneEvent(v1720Event & V1720Event)
{
  int ret = 0;
  int32_t evIndex;
  FEPCBlock * evBlock = NULL;
  //Now try to get the vector hash event for this event
  uint64_t password = 0;
  int64_t eventID = 0x00FFFFFF & V1720Event.GetHeaderWord(2);
  int64_t retFEPCmem = FEPC_mem->GetAssembledEvent(eventID,evIndex,password);
  if(retFEPCmem == returnDCVectorHash::OK)		     
    //We got the index from the vector hash and it's now locked to us. 
    {      
      //Get our evBlock
      evBlock = FEPC_mem->GetFEPCBlock(evIndex);
      //Set IDs    
      evBlock->ev->SetEventID(0x00FFFFFF & V1720Event.GetHeaderWord(2));
      //fprintf(stderr,"Event ID set \n");
      //Loop over the WFD channels.
      size_t numberOfChannels = V1720Event.GetChannelCount();

      //Set the window header.
      WindowHeaderVector.clear(); //clears, but does not deallocate
      //Set the header for this PMT
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(0));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(1));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(2));
      WindowHeaderVector.push_back(V1720Event.GetHeaderWord(3));
      
      //Get the board ID
      uint32_t boardID = ((WindowHeaderVector[1] >> 27)&0x1F);

      //Loop over channels
      //fprintf(stderr,"Let's loop over some channels now \n");
      for(size_t iChannel = 0; iChannel < numberOfChannels;iChannel++)
	{
	  //Allocate a new PMT
	  RAT::DS::PMT * eventPMT = evBlock->ev->AddNewPMT();
	    
	  //Get the parsed channel structure.
	  v1720EventChannel currentParsedChannel = V1720Event.GetChannel(iChannel);
	  eventPMT->SetID(currentParsedChannel.GetChannelID());
	  eventPMT->SetPMTType(1);
	  //Get this PMT's raw event.
	  //RAT::DS::Raw * RATRaw = RATPMT->AddRaw();
	  RAT::DS::Raw * eventRaw = eventPMT->GetRaw();
	  
	  eventRaw->SetHeader(WindowHeaderVector);
	  
    
	  //Loop over the zero suppressed windows.
	  size_t numberOfWindows = currentParsedChannel.GetWindowCount();
	  for(size_t iWindow = 0; iWindow < numberOfWindows;iWindow++)
	    {
	      RAT::DS::RawWaveform * eventRawWaveform = NULL;
	      if(iWindow >= ((unsigned int) eventRaw->GetRawWaveformCount()))
		eventRawWaveform = eventRaw->AddNewRawWaveform();
	      else
		eventRawWaveform = eventRaw->GetRawWaveform(int(iWindow));
	      
	      //Get the current parsed window in this channel
	      v1720EventWindow currentParsedWindow = currentParsedChannel.GetWindow(iWindow);
	      
	      //For each window show the window's time offset to the event time stamp
	      eventRawWaveform->SetTimeOffset(currentParsedWindow.GetTimeOffset());
	      //Set the data samples for this window
	      WindowDataVector.clear();//clears, but does not deallocate
	      uint32_t windowSize = currentParsedWindow.GetSampleCount();
	      //Unpack the samples from this window and enter them in to the RAT window
	      for(uint32_t iSample = 0;iSample < windowSize;iSample++)
		{
		  WindowDataVector.push_back(currentParsedWindow.GetSample(iSample));
		}
	      eventRawWaveform->SetSamples(WindowDataVector);

	      //Calculate the ZLE window integral for the DRPC
	      ProcessZLEWindow(eventRaw, eventRawWaveform, 
			       detectorInfo.GetChannelMap(boardID,iChannel));
	
	    }//Window/RAW loop
	}//Channel loop

      //Push this WFD into the WFD vector
      evBlock->WFD.push_back(boardID);
      //Setup evBlock values if this is the first WFD added to the event
      if (evBlock->WFD.size() == 1)
	{
	  //This is the first event in this evblock, so we should
	  //set its status
	  evBlock->isSent = false;
	  evBlock->state = EventState::UNFINISHED;
	  evBlock->eventID = eventID;
	}
      
      //Determine what to do with the evBLock depending on how many WFDs it has in it
      if(evBlock->WFD.size() == detectorInfo.GetWFDCount())
	{
	  //The event is full and we need to ship it off
	  evBlock->isSent = false;
	  evBlock->state = EventState::READY;
	  retFEPCmem = FEPC_mem->MoveAssembledEventToUnSentQueue(evBlock->eventID,
								 password);
	  //fprintf(stderr,"Moved event %ld to unsent queue\n",eventID);

	  //check for MoveEventToSendQueue errors
	  if(retFEPCmem == returnDCVectorHash::OK)
	    {
	      ret = OK;
		}
	  else
	    {
	      //There should be no way we can anything other than
	      //OK.   If we do, then someone isn't following the
	      //standard rules and we should fail.
	      char * buffer = new char[100];
# if __WORDSIZE == 64
	      sprintf(buffer,"MoveEventToSendQueue returned %ld\n",retFEPCmem);
# else
	      sprintf(buffer,"MoveEventToSendQueue returned %lld\n",retFEPCmem);
#endif
	      PrintError(buffer);
	      delete [] buffer;
	      //DCMessage::Message message;      
	      //message.SetType(DCMessage::PAUSE);
	      //SendMessageOut(message,true);
	      ret = FATAL_ERROR;
	    }
	}
      else if(evBlock->WFD.size() > 0)
	{
	  //Put back the event.
	  ret = OK;
	  retFEPCmem = FEPC_mem->ReturnAssembledEvent(evIndex,password);
	}
      else if (evBlock->WFD.size() > detectorInfo.GetWFDCount())
	{	      
	  //THis should never happen, but if it does we
	  //have something very strange going on and we'll
	  //pass it on to the Badqueue
	  char * buffer = new char[100];
# if __WORDSIZE == 64
	  sprintf(buffer,"WFD count(%lu) exceeds maxWFDCount(%lu)!\n"  ,evBlock->WFD.size(),detectorInfo.GetWFDCount());
# else	  
	  sprintf(buffer,"WFD count(%u) exceeds maxWFDCount(%u)!\n",evBlock->WFD.size(),detectorInfo.GetWFDCount());
# endif

	  PrintError(buffer);
	  delete [] buffer;
	  evBlock->state = EventState::TOO_MANY_WFDS;
	  retFEPCmem = FEPC_mem->MoveAssembledEventToBadQueue(
							      evBlock->eventID,
							      password,
							      BadEvent::RATASSEMBLER_TOO_MANY_WFDS);
	  if(retFEPCmem == returnDCVectorHash::OK)	
	    {
	      //We successfully moved the bad event to the bad event queue
	      //We will not process this event now, but wait for the
	      //clean up loop to catch it. 
	      ret = WFD_NOT_PROCESSED;
	    }
	  else
	    {
	      //There should be no way we can anything other than
	      //OK.   If we do, then someone isn't following the
	      //standard rules and we should fail.
	      char * buffer = new char[100];
# if __WORDSIZE == 64
	      sprintf(buffer,"MoveEventToBadQueue (maxEventWFDCount %lu:%lu) returned %ld\n",
		      evBlock->WFD.size(),
		      detectorInfo.GetWFDCount(),
		      retFEPCmem
		      );
# else
	      sprintf(buffer,"MoveEventToBadQueue (maxEventWFDCount %u:%u) returned %lld\n",
		      evBlock->WFD.size(),
		      detectorInfo.GetWFDCount(),
		      retFEPCmem
		      );
#endif
	      
	      //PrintError(buffer);
	      delete [] buffer;
	      ret = FATAL_ERROR; 
	    }  
	}
	  
    }

 else if( retFEPCmem == returnDCVectorHash::BAD_PASSWORD)
    {	
      ret = LOCK_BLOCKED;
    }
 else if( retFEPCmem == returnDCVectorHash::NO_FREE)
    {
      ret = OUT_OF_MEMORY;
    }
 else if (retFEPCmem >= 0)
   {
      char * buffer = new char[100];
# if __WORDSIZE == 64
      sprintf(buffer,"Collision! %ld %ld\n",retFEPCmem,eventID);
# else
      sprintf(buffer,"Collision! %lld %lld\n",retFEPCmem,eventID);
# endif
      FEPC_mem->PrintStatus();
      PrintError(buffer);
 

      delete [] buffer;



      //The data in the vector hash is for another event. 
      //We have a lock on it though!
      //We will move this event to the bad queue
      //Action concerning this bad event will be left to
      //another thread.
      int64_t retCollision = FEPC_mem->MoveAssembledEventToBadQueue(
					retFEPCmem,
					password,
					BadEvent::RATASSEMBLER_COLLISION);
      
      if(retCollision == returnDCVectorHash::OK)	
	{
	  ret = COLLISION;
	}
      else 
	{
	  //There should be no way we can anything other than
	  //OK.   If we do, then someone isn't following the
	  //standard rules and we should fail.
	  char * buffer = new char[100];
# if __WORDSIZE == 64
	  sprintf(buffer,"MoveEventToBadQueue(collision) returned %ld\n",retFEPCmem);
# else
	  sprintf(buffer,"MoveEventToBadQueue(collision) returned %lld\n",retFEPCmem);
#endif 
	  PrintError(buffer);
	  delete [] buffer;
	  ret = FATAL_ERROR; 	
	}
    }
  else if(retFEPCmem == returnDCVectorHash::BAD_MUTEX)
    {
      //This should never happen since we will block on the mutex.
      //If this happens, then DCDAQ is probably shutting down.
      ret = FATAL_ERROR; 
      //      ret = WFD_NOT_PROCESSED;
    } 
  return(ret);


}
