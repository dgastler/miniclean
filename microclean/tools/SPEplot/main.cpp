//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>

//ROOT includes
#include "TFile.h"
#include "TGraph.h"
#include "TF1.h"
#include "TH1D.h"
#include "TCanvas.h"

int main(int argc, char ** argv)
{
  if(argc < 3)
    {
      std::cout << argv[0] << " outFile inFiles" << std::endl;
      return(0);
    }
  

  std::vector<float> PMT0SPE;
  std::vector<float> PMT0Voltage;
  std::vector<float> PMT1SPE;
  std::vector<float> PMT1Voltage;
  std::vector<int> Run;

  for(int i = 2; i < argc;i++)
    {
      std::string RunID(argv[i]);
      Run.push_back(atoi(
			 (RunID.substr(RunID.find("run_")+4,12)).c_str()
			 ));

      TFile* InFile = TFile::Open(argv[i],"READ");
      TH1D * hptr = (TH1D*) InFile->Get("PMT0SPE_Fit");
      if(!hptr)
	{
	  std::cout << "Bad File" << std::endl;
	  return(0);
	}
      std::string Title(hptr->GetTitle());
      PMT0SPE.push_back(atof((Title.substr(Title.find("SPE:")+4,					  
					   Title.find("C Height") - (Title.find("SPE:")+4)
					   )).c_str()));
      PMT0Voltage.push_back(atof((Title.substr(Title.find("Voltage:")+8,
					       Title.find("V SPE") - (Title.find("Voltage:")+8)
					       )).c_str()));
      hptr = (TH1D*) InFile->Get("PMT1SPE_Fit");
      if(!hptr)
	{
	  std::cout << "Bad File" << std::endl;
	  return(0);
	}
      Title = hptr->GetTitle();
      PMT1SPE.push_back(atof((Title.substr(Title.find("SPE:")+4,					  
					   Title.find("C Height") - (Title.find("SPE:")+4)
					   )).c_str()));
      PMT1Voltage.push_back(atof((Title.substr(Title.find("Voltage:")+8,
					       Title.find("V SPE") - (Title.find("Voltage:")+8)
					       )).c_str()));
      printf("Run: %08d  PMT0SPE = %fpC  PMT1SPE %fpC\n",
	     Run[Run.size()-1],
	     PMT0SPE[PMT0SPE.size() - 1]*1e12,
	     PMT1SPE[PMT1SPE.size() - 1]*1e12);
      InFile->Close();
      delete InFile;
    }

  float * fPMT0SPE     = new float[PMT0SPE.size()];
  float * fPMT0Voltage = new float[PMT0Voltage.size()];
  float * fPMT1SPE     = new float[PMT1SPE.size()];
  float * fPMT1Voltage = new float[PMT1Voltage.size()];

  int NumberOfPoints = PMT0SPE.size();

  for(unsigned int i = 0; i < NumberOfPoints;i++)
    {
      fPMT0SPE[i] = PMT0SPE[i];
      fPMT0Voltage[i] =	PMT0Voltage[i];
      fPMT1SPE[i] = PMT1SPE[i];
      fPMT1Voltage[i] =	PMT1Voltage[i];
      //      std::cout << Run[i] << " " << fPMT0SPE[i] << std::endl;
    }
  
  TFile * OutFile = TFile::Open(argv[1],"CREATE");
  if(OutFile == 0)
    {
      return(0);
    }

  TF1 * Fit0 = new TF1("Fit0","expo");
  TF1 * Fit1 = new TF1("Fit1","expo");

  TCanvas * c1 = new TCanvas("c1");
  TGraph * PMT0 = new TGraph(NumberOfPoints,fPMT0Voltage,fPMT0SPE);
  PMT0->SetName("PMT0SPE");
  PMT0->GetXaxis()->SetTitle("PMT0 Voltage (volts)");
  PMT0->GetYaxis()->SetTitle("PMT0 SPE (C)");
  PMT0->Fit(Fit0,"Q");
  PMT0->Draw("A*");
  PMT0->Write();
  //OutFile->WriteObject(PMT0,PMT0->GetTitle());
  //  c1->Write();

  TGraph * PMT1 = new TGraph(NumberOfPoints,fPMT1Voltage,fPMT1SPE);
  PMT1->SetName("PMT1SPE");
  PMT1->GetXaxis()->SetTitle("PMT1 Voltage (volts)");
  PMT1->GetYaxis()->SetTitle("PMT1 SPE (C)");
  PMT1->Fit(Fit1,"Q");
  PMT1->Draw("A*");
  PMT1->Write();
  //  OutFile->WriteObject(PMT1,PMT1->GetTitle());
  //  c1->Write();

  OutFile->Close();

  delete [] fPMT0SPE;
  delete [] fPMT0Voltage;
  delete [] fPMT1SPE;
  delete [] fPMT1Voltage;

  
  //Lets find the average for each PMT. 
  //PMT0
  std::vector<float> SPE0Vs;
  std::vector<std::vector<float> > SPE0s;
  for(int i = 0; i < PMT0SPE.size();i++)
    {
      for(int j = 0; j < SPE0Vs.size();j++)
	{
	  if(PMT0Voltage[i] == SPE0Vs[j])
	    {
	      SPE0s[j].push_back(PMT0SPE[i]);
	      break;
	    }
	  else if(j + 1 == SPE0Vs.size ())
	    {
	      SPE0s.push_back(std::vector<float>());
	      SPE0s[SPE0s.size()-1].push_back(PMT0SPE[i]);
	      SPE0Vs.push_back(PMT0Voltage[i]);
	      break;
	    }
	}
      if(SPE0Vs.size() == 0)
	{
	  SPE0s.push_back(std::vector<float>());
	  SPE0s[SPE0s.size()-1].push_back(PMT0SPE[i]);
	  SPE0Vs.push_back(PMT0Voltage[i]);
	}
    }
  printf("SPE0 Values:\n");
  for(int i = 0; i < SPE0Vs.size();i++)
    {
      printf("   %f Volts: ",SPE0Vs[i]);
      float Sum = 0;
      float Sum2 = 0;
      for(int j = 0; j < SPE0s[i].size();j++)
	{
	  Sum += SPE0s[i][j];
	  Sum2 += (SPE0s[i][j])*(SPE0s[i][j]);
	}
      float mean = Sum/SPE0s[i].size();
      float sigma = sqrt((Sum2 - Sum*Sum/SPE0s[i].size())/(SPE0s[i].size() -1));
      printf("%f +/- %f pC\n",mean*1e12,sigma*1e12);
    }

  //Lets find the average for each PMT. 
  //PMT1
  std::vector<float> SPE1Vs;
  std::vector<std::vector<float> > SPE1s;
  for(int i = 0; i < PMT1SPE.size();i++)
    {
      for(int j = 0; j < SPE1Vs.size();j++)
	{
	  if(PMT1Voltage[i] == SPE1Vs[j])
	    {
	      SPE1s[j].push_back(PMT1SPE[i]);
	      break;
	    }
	  else if(j + 1 == SPE1Vs.size ())
	    {
	      SPE1s.push_back(std::vector<float>());
	      SPE1s[SPE1s.size()-1].push_back(PMT1SPE[i]);
	      SPE1Vs.push_back(PMT1Voltage[i]);
	      break;
	    }
	}
      if(SPE1Vs.size() == 0)
	{
	  SPE1s.push_back(std::vector<float>());
	  SPE1s[SPE1s.size()-1].push_back(PMT1SPE[i]);
	  SPE1Vs.push_back(PMT1Voltage[i]);
	}
    }
  printf("SPE1 Values:\n");
  for(int i = 0; i < SPE1Vs.size();i++)
    {
      printf("   %f Volts: ",SPE1Vs[i]);
      float Sum = 0;
      float Sum2 = 0;
      for(int j = 0; j < SPE1s[i].size();j++)
	{
	  Sum += SPE1s[i][j];
	  Sum2 += (SPE1s[i][j])*(SPE1s[i][j]);
	}
      float mean = Sum/SPE1s[i].size();
      float sigma = sqrt((Sum2 - Sum*Sum/SPE1s[i].size())/(SPE1s[i].size() -1));
      printf("%f +/- %f pC\n",mean*1e12,sigma*1e12);
    }


  return(0);
}
