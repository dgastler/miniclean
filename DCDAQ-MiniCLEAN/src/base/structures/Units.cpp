#include <Units.h>
#include <cstdio>
#include <string>
#include <boost/algorithm/string.hpp>

static const char * const TypeName[Units::COUNT] = 
  {
    "NONE",
    "NUMBER",
    "PACKETS",
    "BYTES",
    "KBYTES",
    "MBYTES",      
  };  

const char * Units::GetTypeName(Units::Type type)
{
  if(type < Units::COUNT)
    return TypeName[type];
  return NULL;
} 

Units::Type Units::GetType(const char * name)
{
  std::string searchString(name);  
  Units::Type ret = Units::Type(0);
  for(Units::Type type = Units::Type(0);
      type < Units::COUNT;
      type = Units::Type(((int)type) + 1))
    {
      if(boost::algorithm::iequals(searchString,
				   std::string(GetTypeName(type))))
	{
	  ret = type;
	  break;
	}
    }

  return ret;
}
